import sbt.url


ThisBuild / scalaVersion := Version.scalaVersion
/*ThisBuild / version := Version.synaptixVersion*/
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"
ThisBuild / useCoursier := false
ThisBuild / onChangedBuildSource := ReloadOnSourceChanges

lazy val ddfWiktionnaireAnnotator = (project in file(".")).
  settings(
    name := "ddf-wiktionnaire-annotator",
    organization := "org.ddf",
    scalaVersion := Version.scalaVersion,
    version := Version.synaptix,
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    developers := List(
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lhérison",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
        Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
    ),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),

    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.sonatypeRepo("public"),
      Resolver.typesafeRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
      "gitlab-maven-template-replacer" at "https://gitlab.com/api/v4/projects/35329974/packages/maven"
    ),
    libraryDependencies ++= Dependencies.core
  )