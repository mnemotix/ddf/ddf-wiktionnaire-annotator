package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class SourceModelParserSpec extends AnyFlatSpec with Matchers {
  behavior of "Source Model Parser"

  val soureModelParser = new SourceModelParser

  it should "parse a citation with {{nom w pc}} and {{ws}}" in {
    val lesHautsDeHurlevent = "{{source|{{nom w pc|Emily|Brontë}}, ''{{ws|Les Hauts de Hurlevent (trad. Delebecque)|Les Hauts de Hurlevent}}'', 1847, traduit par {{nom w pc|Frédéric|Delebecque}} en 1925}}"
    println(TemplateReplacer.replaceTemplates(lesHautsDeHurlevent))
    soureModelParser.parser(lesHautsDeHurlevent) shouldEqual """Emily Brontë, <i>Les Hauts de Hurlevent (trad. Delebecque) Les Hauts de Hurlevent</i>, 1847, traduit par Frédéric Delebecque en 1925""".stripMargin
  }

  it should "parse a citation with one {{nom w pc}}" in {
    val lesCompagnonsDeLEscopette = """{{source|{{nom w pc|Victor|Méric}}, ''Les Compagnons de l’Escopette'', Éditions de l’Épi, Paris, 1930}}"""
    soureModelParser.parser(lesCompagnonsDeLEscopette) shouldEqual """Victor Méric, <i>Les Compagnons de l’Escopette</i>, Éditions de l’Épi, Paris, 1930"""
  }

  it should "parse a citation with a link" in {
    val journalDeLorenzo = """{{source|Journal de Lorenzo Létourneau (1899), [http://www.qualigram.ca/qualigram/index.php?id=12276 ''17 Eldorado''], Qualigram/Linguatech, Montréal, 2006}}"""
    soureModelParser.parser(journalDeLorenzo) shouldEqual """Journal de Lorenzo Létourneau (1899), <a href="http://www.qualigram.ca/qualigram/index.php?id=12276"><i>17 Eldorado</i></a>, Qualigram/Linguatech, Montréal, 2006"""
  }

  it should "parse a citation withe a link and with {{pc|" in {
    val comete= "{{source|Lucy {{pc|Embark}}, [https://www.ouest-france.fr/normandie/saint-lo-50000/des-coiffes-du-xixe-siecle-exposees-au-musee-du-bocage-saint-lo-5003013 « Des coiffes du {{siècle2|XIX}} siècle exposées au musée du Bocage à Saint-Lô »], www.ouest-france.fr, 20 mai 2017}}"
    println(soureModelParser.parser(comete))
    soureModelParser.parser(comete) shouldEqual """Lucy Embark, <a href="https://www.ouest-france.fr/normandie/saint-lo-50000/des-coiffes-du-xixe-siecle-exposees-au-musee-du-bocage-saint-lo-5003013">« Des coiffes du (XIXe siècle) siècle exposées au musée du Bocage à Saint-Lô »</a>, www.ouest-france.fr, 20 mai 2017""".stripMargin
  }

  it should "source auteur url titre editateur en ligne le" in {
    val source = """{{source|{{Lien web |auteur=''Le Monde'' |url=https://www.lemonde.fr/politique/article/2020/08/27/contre-l-avis-de-la-federation-nationale-des-chasseurs-l-elysee-decide-d-interdire-la-chasse-a-la-glu-cette-annee_6050106_823448.html |titre=Contre l’avis de la Fédération des chasseurs, Emmanuel Macron interdit la chasse à la glu cette année |éditeur=''Le Monde'' |en ligne le=27 août 2020}}}}"""
    // (Le Monde, Contre l’avis de la Fédération des chasseurs, Emmanuel Macron interdit la chasse à la glu cette année, Le Monde. Mis en ligne le 27 août 2020
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """<i>Le Monde</i>, <a href="https://www.lemonde.fr/politique/article/2020/08/27/contre-l-avis-de-la-federation-nationale-des-chasseurs-l-elysee-decide-d-interdire-la-chasse-a-la-glu-cette-annee_6050106_823448.html">Contre l’avis de la Fédération des chasseurs, Emmanuel Macron interdit la chasse à la glu cette année</a> <i>Le Monde</i>. Mis en ligne le 27 août 2020""".stripMargin
  }

  it should "check prenom nom titre sous-titre editateur" in {
    val source = """{{source|{{ouvrage |prénom=André |nom=Courvoisier |titre=Le Réseau Heckler |sous-titre=De Lyon à Londres |éditeur=France-Empire |lieu=Paris |année=1984}}, page 41}}"""

    val source2 = """John Nada (pseudonyme), <i>[http://www.nanarland.com/Chroniques/Main.php?id_film=metalman Chronique de Metalman]</i> sur <i>Nanarland</i> (nanarland.com)"""
    // (André Courvoisier, Le Réseau Heckler : De Lyon à Londres, France-Empire, Paris, 1984, page 41)
    println(soureModelParser.parser(source))
    println(soureModelParser.parser(source2))

    soureModelParser.parser(source) shouldEqual """André  Courvoisier, Le Réseau Heckler : De Lyon à Londres, France-Empire. Paris, 1984, page 41"""
    soureModelParser.parser(source2) shouldEqual """John Nada (pseudonyme), <i><a href="http://www.nanarland.com/Chroniques/Main.php?id_film=metalman">Chronique de Metalman</a></i> sur <i>Nanarland</i> (nanarland.com)"""
  }

  it should "parse model with {{ere}}" in {
    val source = """{{source|D. de Prat, ''Nouveau manuel complet de filature; 1{{ère}} partie: Fibres animales & minérales'', Encyclopédie Roret, 1914}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """D. de Prat, <i>Nouveau manuel complet de filature; 1ère partie: Fibres animales & minérales</i>, Encyclopédie Roret, 1914"""
  }

  it should "parse the {{in|2}}" in {
    val source = """{{source|Sophie {{pc|Barrial}}, interrogée par {{nom w pc|Denis|Cheissoux}}, ''{{w|CO{{in|2}} mon amour}}'', {{w|France Inter}}, 17 février 2018}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Sophie Barrial, interrogée par Denis Cheissoux, <i>CO<sub>2</sub> mon amour</i>, France Inter, 17 février 2018"""
  }

  it should "parse the {{smcp" in {
    val source = """{{source|Robert {{smcp|Gaumont}}, Article « {{smcp|Mimétisme}}, zoologie », [http://www.universalis.fr/encyclopedie/mimetisme-zoologie/ www.universalis.fr], 2016}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Robert Gaumont, Article « Mimétisme, zoologie », <a href="http://www.universalis.fr/encyclopedie/mimetisme-zoologie/">www.universalis.fr</a>, 2016"""
  }

  it should "parse the {{nobr|" in {
    val source = """{{source|« Le vieux poulet couvait un nid de fachos », ''{{w|Le Canard enchaîné}}'', 27 juin 2018, {{nobr|page 3}}.}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """« Le vieux poulet couvait un nid de fachos », <i>Le Canard enchaîné</i>, 27 juin 2018, page 3."""
  }

  it should "parse numero model" in {
    val source = """{{source|État-Major des armées, ''Instruction {{numéro}}2467/DEF/DCSCA/SD_REJ/BREG relative à l’emploi et à la fourniture des timbres officiels dans les armées'' du 30 avril 2013}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """État-Major des armées, <i>Instruction n°2467/DEF/DCSCA/SD_REJ/BREG relative à l’emploi et à la fourniture des timbres officiels dans les armées</i> du 30 avril 2013"""
  }

  it should "parse a source with w:" in {
    val source = """{{source|[[w:Pierre Rousseau (vulgarisateur)|Pierre Rousseau]], ''La Terre, ma Patrie'', collection "Savoir', librairie Arthème Fayard, 1947, page 444}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Pierre Rousseau, <i>La Terre, ma Patrie</i>, collection "Savoir', librairie Arthème Fayard, 1947, page 444"""
  }

  it should "parse a source with internal link [[]]" in {
    val source  = """{{source|Patrick Rousselle, Yvon Robert et Jean-Claude Crosnier, ''La pomme de terre : production, amélioration, ennemis et maladies, utilisations'', [[INRA]], 1996}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Patrick Rousselle, Yvon Robert et Jean-Claude Crosnier, <i>La pomme de terre : production, amélioration, ennemis et maladies, utilisations</i>, <a href="/form/INRA">INRA</a>, 1996"""
  }

  it should "parse a source from exemple model" in {
    val source = """|source={{périodique|auteur=Lucile Quillet|titre=Les femmes, ces criminelles (presque) comme les hommes|journal={{w|Slate (magazine)|Slate}}|date=4 mars 2019|url=http://www.slate.fr/story/174153/societe/les-femmes-et-le-crime-episode-1-criminelles-comme-hommes}}|lang=fr}}""".stripMargin
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Lucile Quillet, <a href="http://www.slate.fr/story/174153/societe/les-femmes-et-le-crime-episode-1-criminelles-comme-hommes">Les femmes, ces criminelles (presque) comme les hommes</a>, dans le Slate (magazine). Slate 4 mars 2019"""
  }

  it should "parse another source frome exemple model" in {
    val source = """|source={{périodique|auteur=Harold Grand|titre=Les données personnelles de 100 millions d’Américains exposées dans un piratage géant|journal={{w|Le Figaro}}|date=30 juillet 2019|url=https://www.lefigaro.fr/secteur/high-tech/les-donnees-personnelles-de-100-millions-d-americains-exposees-dans-un-piratage-geant-20190730}}|lang=fr}}""".stripMargin
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Harold Grand, <a href="https://www.lefigaro.fr/secteur/high-tech/les-donnees-personnelles-de-100-millions-d-americains-exposees-dans-un-piratage-geant-20190730">Les données personnelles de 100 millions d’Américains exposées dans un piratage géant</a>, dans le Le Figaro. 30 juillet 2019"""
  }

  it should "parse a source with an ISBN model" in {
    val source = """{{source|{{w|Maëster}}, {{w|Sœur Marie-Thérèse des Batignolles}}, ''La Guère Sainte'', Drugstore, 2008, {{ISBN|978-2226175601}}}}""".stripMargin
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual """Maëster, Sœur Marie-Thérèse des Batignolles, <i>La Guère Sainte</i>, Drugstore, 2008, ISBN 978-2226175601""".stripMargin
  }

  it should "parse a source with a lien web" in {
    val source = """{{source|{{lien web|url=https://www.philosophie.ch/fr/philosophie-fr/domaines/philosophie-theorique/philosophie-transcendantale-et-idealisme-allemand|site=philosophie.ch|titre=Philosophie transcendantale|consulté le=22 février 2019}}}}"""
    println(soureModelParser.parser(source))
    soureModelParser.parser(source) shouldEqual("""sur philosophie.ch <a href="https://www.philosophie.ch/fr/philosophie-fr/domaines/philosophie-theorique/philosophie-transcendantale-et-idealisme-allemand">Philosophie transcendantale</a> consulté le 22 février 2019""".stripMargin)
  }

  it should "parse a source with periodique" in {
    val source = """{{source|{{périodique|prénom1=Janine|nom1=Lanza|titre=Les veuves dans les corporations parisiennes au {{siècle2|18}} siècle|journal={{w|Revue d'histoire moderne et contemporaine|Revue d’histoire moderne et contemporaine}}|date=2009|volume=3|numéro=56|pages=92-122|texte=https://doi.org/10.3917/rhmc.563.0092}}}}"""
    println(soureModelParser.parser(source))
    //soureModelParser.parser(source) shouldEqual """Janine Lanza, Les veuves dans les corporations parisiennes au XVIIIe siècle, dans Revue d’histoire moderne et contemporaine, 2009, p. 92-122 [texte intégral]"""
  }

  it should "parse a source with the citation model" in {
    val source = """{{source|{{Citation/Jean Rogissart/Hurtebise aux griottes/1954|142}}}}""".stripMargin
    println(soureModelParser.parser(source))

  }

  it should "parce a source with the citation model simple" in {
    val source = """{{source|{{Citation/Gustave Flaubert/Madame Bovary/1857}}}}"""
    val source2 = """{{source|{{Citation/Michel Zévaco/Le Capitan/1907}}}}"""
    println(soureModelParser.parser(source))
    println(soureModelParser.parser(source2))
  }

  it should "parse a source with url" in {
    val source = "{{source|{{périodique|prénom1=Claire|nom1=Devarrieux|titre=Faites entrer la préfète|journal={{w|Libération (journal)|Libération}}|numéro=11658|date=22 novembre 2018|page=27|url=https://www.liberation.fr/livres/2018/11/21/faites-entrer-la-prefete_1693455/}}}}"
    println(soureModelParser.parser(source))
  }

  it should "parse a source with many inside model" in {
    val source = "{{source|{{Légifrance|base=JORF|numéro=ACVX8900143L|texte=Art. 1{{er}} de la loi {{n°}} 89-1013}} du 31 décembre 1989 portant création du statut de prisonnier du Viet-Minh.}}"
    println(soureModelParser.parser(source))
  }

  it should "parse a source with #* {{exemple|lang=fr}}" in {
    val source = "{{exemple|lang=fr}}"
    println(soureModelParser.parser(source))
  }
}

//Guy Millière, <i>[//www.les4verites.com/La-passivite-est-une-incitation-a-la-barbarie-2303.html La passivité est une incitation à la barbarie]</i>, 21 avril 2009, <www.les4verites.com>
// [http://www.futura-sciences.com/fr/news/t/medecine/d/sodas-lights-pas-de-calorie-mais-des-soucis_31172/ Sodas lights : pas de calorie mais des soucis !]<i> Futura-Sciences, 1{{er juillet 2011
//Molière, <i>Les femmes savantes</i>, II, 6. [cité par Littré]
//Abbé d’Aubignac [François Hédelin], dans <i>Mistère du siége d’Orléans</i>, appendice, p. 798
//<i>Projet de loi concernant les aliénations, […], demandées par diverses communes</i>, séance du 13 janvier 1810, Corps législatif, extrait du registre des minutes de la Secrétairie d’État, 7 février 1810, p.19
// John Nada (pseudonyme), <i>[http://www.nanarland.com/Chroniques/Main.php?id_film=metalman Chronique de Metalman]</i> sur <i>Nanarland</i> (nanarland.com)