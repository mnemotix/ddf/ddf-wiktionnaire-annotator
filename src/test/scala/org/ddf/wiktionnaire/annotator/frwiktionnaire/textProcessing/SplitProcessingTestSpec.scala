package org.ddf.wiktionnaire.annotator.frwiktionnaire.textProcessing

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.{RegexExtractors, SplitProcessing}
import org.ddf.wiktionnaire.annotator.model.{LexicalEntrySection, Section}
import org.ddf.wiktionnaire.annotator.services.DDFModelsSetter
import org.ddf.wiktionnaire.annotator.test.data.{WikitextAnanas, WikitextBulkData, WikitextChicha, WikitextConne, WikitextCorbeau, WikitextSolvions, WikitextTest}
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class SplitProcessingTestSpec extends WiktionnaireAnnotatorTestSpec {
  it should "split by lang" in {
    SplitProcessing.splitByLang(WikitextAnanas.ananas) shouldEqual
      """=== {{S|étymologie}} ===
        |: {{date|1578}} Du {{étyl|tpw|fr}}-{{étyl|gn|fr|naná}}. {{date|1554}} ''[[nana]]''.
        |
        |=== {{S|nom|fr}} ===
        |{{fr-inv|a.na.nɑ|pron2=a.na.nas|sp=1}}
        |[[Image:Pineapple Oahu.jpg|thumb|Un '''ananas'''.]]
        |[[Image:Ananas comosus Victoria P1190421.jpg|thumb|Fruits d’'''ananas'''.]]
        |'''ananas''' {{pron|a.na.nɑ|fr}} ou {{pron|a.na.nas|fr}} {{m}}, {{sp}}
        |# {{plantes|fr}} Plante [[tropical]]e originaire de l’[[Amérique du Sud]], qu’on cultive en [[Europe]] dans des [[serre]]s chaudes, dont les [[tige]]s courtes portent une [[rosette]] de [[feuille]]s [[épaisse]]s, [[sessile]]s, étroitement [[imbriquer|imbriquées]] et munies de [[piquant]]s parfois [[acéré]]s.
        |# [[faux-fruit|Faux-fruit]] [[comestible]] de cette plante de forme [[conique]] à la peau [[écailleux|écailleuse]] et épaisse et à la [[chair]] jaune et [[sucré]]e.
        |#* ''S’il restait encore un doute sur la supériorité de l’Inde, par rapport à ses fruits, l’'''ananas''' seul ferait pencher la balance en faveur de son heureuse patrie : il réunit toutes les bonnes qualités et tous les agrémens épars çà et là dans les autres : élégance de la forme, parfum fin et délicat, saveur exquise, suc abondant et distribué également dans tout le fruit, la nature a versé sur lui tous ses trésors.'' {{source|Père M. Perrin, ''Voyage dans l’Indostan'', 1807}}
        |#* ''L’'''ananas''' est essentiellement cultivé pour son fruit consommé au naturel ou mis en conserve (tranches, morceaux, jus). Les feuilles peuvent être utilisées pour leurs fibres et dans l’alimentation du bétail. La plante entière peut être réduite en farine pour le bétail. On en extrait de l’amidon et de la broméline, mélange d’enzymes utilisé dans l’industrie pharmaceutique.'' {{source|Cirad/Gret/MAE, ''Mémento de l’agronome'', Cirad/Gret/Ministère des Affaires étrangères, Paris, 2002, page 945}}
        |# {{militaire|fr}} {{term|argot des tranchées}} [[mine|Mine]] explosive de fabrication britannique, pendant la Première Guerre mondiale.
        |#* ''En réponse, je piquai dans la tranchée anglaise quelques « '''ananas''' », c’est-à-dire des mines qui faisaient songer par leur forme à ce fruit exquis.'' {{source|{{nom w pc|Ernst|Jünger}}, ''{{w|Orages d’acier}}'', 1961 ; traduit de l’allemand par {{nom w pc|Henri|Plard}}, 1970, p. 88}}
        |
        |==== {{S|dérivés}} ====
        |{{(}}
        |* [[ananas bouteille]]
        |* [[ananas de mer]]
        |* [[ananas des bois]]
        |* [[ananas fossile]]
        |* [[fraise ananas]]
        |* [[ananas-bois]]
        |* [[ananeraie]]
        |{{)}}
        |
        |==== {{S|traductions}} ====
        |{{trad-début|Plante}}
        |* {{T|conv}} : ''{{trad+|conv|Ananas}}''
        |* {{T|en}} : {{trad-|en|pineapple tree}}
        |* {{T|eu}} : {{trad-|eu|anana}}
        |* {{T|ca}} : {{trad-|ca|ananàs|m}}
        |* {{T|zh}} : {{trad+|zh|菠萝蜜|tr=bōluómì|tradi=菠蘿蜜}}
        |* {{T|es}} : {{trad+|es|ananás}}, {{trad+|es|piña|f}}
        |* {{T|ja}} : {{trad-|ja|パイナップル|R=painappuru}}
        |* {{T|avk}} : {{trad--|avk|ksago}}
        |* {{T|nl}} : {{trad+|nl|ananas|m}}
        |* {{T|pl}} : {{trad+|pl|ananas}}
        |* {{T|pt}} : {{trad+|pt|abacaxi|m}}, {{trad+|pt|ananás|m}}
        |* {{T|zdj}} : {{trad--|zdj|mnanasi}}
        |{{trad-fin}}
        |
        |{{trad-début|Fruit}}
        |* {{T|af}} : {{trad+|af|ananas}}, {{trad+|af|pynappel}}
        |* {{T|sq}} : {{trad+|sq|ananas}}
        |* {{T|de}} : {{trad+|de|Ananas|m}}
        |* {{T|en}} : {{trad+|en|pineapple}}
        |* {{T|ar}} : {{trad-|ar|اناناس|R={{transliterator|ar|اناناس}}}}
        |* {{T|bci}} : {{trad--|bci|ablɛlɛ}}
        |* {{T|eu}} : {{trad-|eu|anana}}
        |* {{T|be}} : {{trad+|be|ананас|R=ananas}}
        |* {{T|be-x-old}} : {{trad--|be-x-old|ананас|R=ananas}}
        |* {{T|bs}} : {{trad+|bs|ananas}}
        |* {{T|bg}} : {{trad+|bg|ананас|R=ananas}}
        |* {{T|ca}} : {{trad-|ca|pinya}}
        |* {{T|shy}} : {{trad--|shy|lananaṣ}}
        |* {{T|zh}} : {{trad+|zh|菠萝|tradi=菠蘿|R=bōluó}}, {{trad+|zh|凤梨|tradi=鳳梨|R=fènglí}}
        |* {{T|ko}} : {{trad+|ko|파인애플|R=painaepeul}}
        |* {{T|hr}} : {{trad+|hr|ananas}}
        |* {{T|da}} : {{trad+|da|ananas}}
        |* {{T|es}} : {{trad+|es|ananás}}, {{trad+|es|piña}}
        |* {{T|eo}} : {{trad+|eo|ananaso}}
        |* {{T|et}} : {{trad+|et|ananass}}
        |* {{T|fi}} : {{trad+|fi|ananas}}
        |* {{T|el}} : {{trad+|el|ανανάς|R=ananás}}
        |* {{T|hi}} : {{trad+|hi|अनन्नास|R=anannāsa}}
        |* {{T|hu}} : {{trad+|hu|ananász}}
        |* {{T|id}} : {{trad+|id|nanas}}
        |* {{T|is}} : {{trad+|is|ananas}}
        |* {{T|it}} : {{trad+|it|ananas}}
        |* {{T|ja}} : {{trad-|ja|パイナップル|R=painappuru}}, {{trad-|ja|パイン|R=pain}}
        |* {{T|lv}} : {{trad+|lv|ananass}}
        |* {{T|ln}} : {{trad-|ln|ananasi}}
        |* {{T|lt}} : {{trad+|lt|ananasas}}
        |* {{T|mk}} : {{trad+|mk|ананас|R=ananas}}
        |* {{T|ms}} : {{trad+|ms|nanas}}
        |* {{T|gv}} : {{trad-|gv|annane}}
        |* {{T|nl}} : {{trad+|nl|ananas|m}}
        |* {{T|no}} : {{trad+|no|ananas}}
        |* {{T|fa}} : {{trad+|fa|آناناس|R=ânânâs}}
        |* {{T|pl}} : {{trad+|pl|ananas|m}}
        |* {{T|pt}} : {{trad+|pt|abacaxi}}, {{trad+|pt|ananás}}
        |* {{T|ro}} : {{trad+|ro|ananas}}
        |* {{T|ru}} : {{trad+|ru|ананас|R=ananas}}
        |* {{T|sr}} : {{trad+|ru|ананас|R=ananas}}
        |* {{T|sh}} : {{trad+|sh|ananas}}
        |* {{T|sk}} : {{trad+|sk|ananás}}
        |* {{T|sl}} : {{trad+|sl|ananas}}
        |* {{T|sv}} : {{trad+|sv|ananas}}
        |* {{T|ta}} : {{trad-|ml|அன்னாசி|tr=aṉṉāçi}}
        |* {{T|cs}} : {{trad+|cs|ananas}}
        |* {{T|th}} : {{trad+|th|สับปะรด|R=sàpbpàrót}}
        |* {{T|tr}} : {{trad+|tr|ananas}}
        |* {{T|uk}} : {{trad+|uk|ананас|R=ananas}}
        |* {{T|vi}} : {{trad+|vi|dứa}}
        |* {{T|gdr}} : {{trad--|gdr|aitörang}}
        |{{trad-fin}}
        |
        |=== {{S|prononciation}} ===
        |* {{pron|a.na.nas|fr}} ou {{pron|a.na.na|fr}}
        |** {{écouter|France|ɛ̃.n‿a.na.nas|titre=un ananas|audio=Fr-ananas.ogg|lang=fr}}
        |** {{écouter|Canada {{soutenu|nocat=1}}|a.na.nɑ|lang=fr}}
        |** {{écouter|Canada {{informel|nocat=1}}|a.na.nɔ|audio=Qc-ananas.ogg|lang=fr}}
        |* {{écouter||a.na.nɑ|lang=fr|audio=LL-Q150 (fra)-Mathieu Denel WMFr-ananas.wav}}
        |* {{note}} En France métropolitaine et en Suisse romande, le « S » de ''ananas'' est généralement prononcé, tandis qu’aux Antilles françaises, à la Réunion, en Belgique, en Haïti et au Québec, ce n’est pas le cas (information en provenance de {{WP}}).
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-PJLC-Wiki-ananas.wav}}
        |* {{écouter|lang=fr|Suisse (Genève)|audio=LL-Q150 (fra)-Nattes à chat-ananas.wav}}
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines1-ananas.wav}}
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines2-ananas.wav}}
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines3-ananas.wav}}
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines5-ananas.wav}}
        |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Aemines4-ananas.wav}}
        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines6-ananas.wav}}
        |
        |=== {{S|voir aussi}} ===
        |* {{WP}}
        |
        |=== {{S|références}} ===
        |* {{R:TLFi}}
        |* {{R:Meyer}}
        |
        |[[Catégorie:Fruits en français]]
        |[[Catégorie:Broméliacées en français]]
        |[[Catégorie:-s prononcés /s/ en français]]""".stripMargin
  }

  it should "split by H3" in {
    SplitProcessing.splitByH3(WikitextCorbeau.corbeau).size shouldEqual 7
    SplitProcessing.splitByH3(WikitextTest.test).size shouldEqual 8
    SplitProcessing.splitByH3(WikitextSolvions.solvions).size shouldEqual 2
  }

  it should "split by H" in {
    SplitProcessing.splitByH(WikitextCorbeau.corbeau).size shouldEqual 15
  }

  it should "split by LexicalEntry" in {
    val lexicalEntries = SplitProcessing.splitByLexicalEntry(WikitextCorbeau.corbeau, "corbeau")
    val lexicalEntriesTest = SplitProcessing.splitByLexicalEntry(WikitextTest.test, "test")

    lexicalEntriesTest.size shouldEqual 3
    lexicalEntries.size shouldEqual 2
    lexicalEntries(0).sections.size shouldEqual 8
  }

  it should "split by LexicalEntry with flexion" in {
    val lexicalEntriesSolvions = SplitProcessing.splitByLexicalEntry(WikitextSolvions.solvions, "solvions")
    lexicalEntriesSolvions.foreach { lexicalEntrie =>
      println(lexicalEntrie.isInflection + " " + lexicalEntrie.lexicalEntryType)
    }
    println(lexicalEntriesSolvions.size)
    lexicalEntriesSolvions.size shouldEqual 2
    val lexicalEntriesConnes = SplitProcessing.splitByLexicalEntry(WikitextConne.conne, "conne")
    lexicalEntriesConnes.foreach { lexicalEntrie =>
      println(lexicalEntrie.isInflection + " " + lexicalEntrie.lexicalEntryType)
    }
    println(lexicalEntriesConnes.size)
    lexicalEntriesConnes.size shouldEqual 2
  }

  it should "split Etymology By Post with form ([[#Nom commun D|''Nom D'']])" in {

    val ddfModelSetter = new DDFModelsSetter()

    val lexicalEntriesSection: Seq[LexicalEntrySection] = SplitProcessing.splitByLexicalEntry(WikitextChicha.chicha, "chicha")
    val sectionsWordType: Seq[Section] = ddfModelSetter.wordTypeSection(ddfModelSetter.lexicalEntriesAllSection(lexicalEntriesSection))

    val etymologyText = """: ([[#Nom commun 1|''Nom 1'']]) {{date|lang=fr}} De l’{{étyl|arz|fr|mot=شيشة|tr=šīša}}, issu du {{étyl|tr|fr|mot=şişe}}, lui-même issu du {{étyl|fa|fr|mot=شیشه|tr=šīša|sens=[[verre]]}}.
                          |: ([[#Nom commun 2|''Nom 2'']]) {{date|lang=fr}} De l’{{étyl|es|fr|mot=chicha|sens=[[pois chiche]]}}, lui-même du latin ''[[cicer#la|cicer]]''.""".stripMargin

    val lexicalEntryType = "=== {{S|nom|fr|num=1}} ==="
    val lexicalEntryType2= "=== {{S|nom|fr|num=2}} ==="

    SplitProcessing.splitEtymologyByPost(Some(etymologyText), lexicalEntryType, sectionsWordType).get shouldEqual "{{date|lang=fr}} De l’{{étyl|arz|fr|mot=شيشة|tr=šīša}}, issu du {{étyl|tr|fr|mot=şişe}}, lui-même issu du {{étyl|fa|fr|mot=شیشه|tr=šīša|sens=[[verre]]}}."
    SplitProcessing.splitEtymologyByPost(Some(etymologyText), lexicalEntryType2, sectionsWordType).get shouldEqual "{{date|lang=fr}} De l’{{étyl|es|fr|mot=chicha|sens=[[pois chiche]]}}, lui-même du latin ''[[cicer#la|cicer]]''."
  }

  // todo berlot with etymology with term
  /*
  : {{term|Nom 1}} Possiblement dérivé de ''[[berline]]'', avec pour variante ''brelot''<ref>Honoré Beaugrand, François Ricard, ''La chasse-galerie et autres récits'', 1989</ref> peut-être le {{compos|brêler|sens1=attacher|-ot|sens=[[attelage]]|lang=fr}} ; voir ''{{lien|bérot|fr}}''.
  : {{term|Nom 2}} De l’{{étyl|fro|fr|belin||}}, ''{{lien|berlin|fro}}'', de même origine que ''{{lien|bélier|fr}}''.
  : {{term|Nom 3}} {{louchébem}} {{ébauche-étym|fr}}
  : {{term|Adjectif}} Voir ''{{lien|Berlot|fr}}''.
  */

  it should "split Etymology By Post with form (''Nom D'')" in {

    val ddfModelSetter = new DDFModelsSetter()

    val lexicalEntriesSection: Seq[LexicalEntrySection] = SplitProcessing.splitByLexicalEntry(WikitextTest.test, "test")
    val sectionsWordType: Seq[Section] = ddfModelSetter.wordTypeSection(ddfModelSetter.lexicalEntriesAllSection(lexicalEntriesSection))

    val etymologyText = """: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
                          |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
                          |: ''(Verbe)'' Apocope de ''[[tester]]''.""".stripMargin

    val lexicalEntryType = "=== {{S|nom|fr|num=1}} ==="
    val lexicalEntryType2= "=== {{S|nom|fr|num=2}} ==="
    val lexicalEntryVerb = "=== {{S|verbe|fr}} ==="

    SplitProcessing.splitEtymologyByPost(Some(etymologyText), lexicalEntryType, sectionsWordType).get shouldEqual "{{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}."
    SplitProcessing.splitEtymologyByPost(Some(etymologyText), lexicalEntryType2, sectionsWordType).get shouldEqual """{{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».""".stripMargin
    SplitProcessing.splitEtymologyByPost(Some(etymologyText), lexicalEntryVerb, sectionsWordType).get shouldEqual "Apocope de ''[[tester]]''."
  }

  it should "split by #" in {
    SplitProcessing.splitByDefinition(WikitextBulkData.griboullis).toSeq.foreach(println(_))


    SplitProcessing.splitByDefinition(WikitextBulkData.griboullis).toSeq.size shouldEqual 1
    SplitProcessing.splitByDefinition(WikitextBulkData.mazagran).toSeq.size shouldEqual 2
    SplitProcessing.splitByDefinition(WikitextBulkData.affaireeAdj).toSeq.size shouldEqual 1
    SplitProcessing.splitByDefinition(WikitextBulkData.francophonieMAJ).toSeq.size shouldEqual 2
  }
}