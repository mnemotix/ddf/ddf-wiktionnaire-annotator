/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

// todo close DDFS-214

class FchimParserSpec extends AnyFlatSpec with Matchers {

  it should "parse NH3" in {
    val ammoniac = "[[composé|Composé]] [[chimique]] de [[formule]] {{fchim|NH|3}}"
    TemplateReplacer.replaceTemplates(ammoniac) shouldEqual "[[composé|Composé]] [[chimique]] de [[formule]] NH<sub>3</sub>"
  }

  it should "parse a definition with 4 fchim" in {
    val acideTellureux = "[[acide|Acide]] analogue à l’[[acide sulfureux]], de formule {{fchim|H|2|TeO|3}}, moins oxygéné que l’[[acide tellurique]] ({{fchim|H|2|TeO|4}}), et dont les sels sont nommés [[hydrogénotellurite]]s ({{fchim|HTeO|3}}{{e|-}}) et [[tellurite]]s ({{fchim|TeO|3}}{{e|2-}})."
    TemplateReplacer.replaceTemplates(acideTellureux) shouldEqual "[[acide|Acide]] analogue à l’[[acide sulfureux]], de formule H<sub>2</sub>TeO<sub>3</sub>, moins oxygéné que l’[[acide tellurique]] (H<sub>2</sub>TeO<sub>4</sub>), et dont les sels sont nommés [[hydrogénotellurite]]s (HTeO<sub>3</sub><sup>-</sup>) et [[tellurite]]s (TeO<sub>3</sub><sup>2-</sup>)."
  }
}
