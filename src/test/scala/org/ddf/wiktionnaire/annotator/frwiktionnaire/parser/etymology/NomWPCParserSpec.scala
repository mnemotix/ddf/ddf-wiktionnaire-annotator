/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class NomWPCParserSpec extends AnyFlatSpec with Matchers{

/*
// hobbesien = {{nom w pc|Thomas|<i>Hobbes</i>}}
// berzélianite = {{nom w pc|Jöns Jacob|Berzélius}}
// selin = {{nom w pc|Jean-Baptiste de|Lamarck}}
// ringwoodite = {{pc|Binns}}, {{pc|Davis}}
// thénardite = {{nom w pc|lang=es|José Luis|Casaseca}}
// brucite = (1824) Nommée par le minéralogiste français {{nom w pc|François Sulpice|Beudant}} en l’honneur du minéralogiste américain {{nom w pc|Archibald|<i>Bruce</i>|lang=en|Archibald Bruce (mineralogist)}} (1777–1818).
// {{pc|Barnais}} [Georges Auguste Charles {{pc|Guibourg}}, dit {{nom w pc||Georgius}}
*/

  it should "parse a nom w pc sentence with an html tag" in {
    val wpcParser = new NomWPCParser()
    val result = wpcParser.parser("{{nom w pc|Thomas|<i>Hobbes</i>}}")
    println(result)
    result shouldEqual("Thomas <i>Hobbes</i>")
  }

  it should "parse a nom w pc sentence with 3 parts" in {
    val wpcParser = new NomWPCParser()
    val result = wpcParser.parser("{{nom w pc|Jöns Jacob|Berzélius}}")
    println(result)
    result shouldEqual("Jöns Jacob Berzélius")
  }

  it should "parse a nom w pc sentence with an -" in {
    val wpcParser = new NomWPCParser()
    val result = wpcParser.parser("{{nom w pc|Jean-Baptiste de|Lamarck}}")
    println(result)
    result shouldEqual("Jean-Baptiste de Lamarck")
  }

  it should "parse a nom w pc with an lang=es" in {
    val wpcParser = new NomWPCParser()
    val result = wpcParser.parser("{{nom w pc|lang=es|José Luis|Casaseca}}")
    println(result)
    result shouldEqual "José Luis Casaseca"
  }

  it should "parse a phrase with multiple nom w pc" in {
    val wpcParser = new NomWPCParser()
    val result = wpcParser.parser("(1824) Nommée par le minéralogiste français {{nom w pc|François Sulpice|Beudant}} en l’honneur du minéralogiste américain {{nom w pc|Archibald|<i>Bruce</i>|lang=en|Archibald Bruce (mineralogist)}} (1777–1818).")
    println(result)
    result shouldEqual "(1824) Nommée par le minéralogiste français François Sulpice Beudant en l’honneur du minéralogiste américain Archibald <i>Bruce</i> Archibald Bruce (mineralogist) (1777–1818)."
  }

  it should "parse a phrase with multiple pc" in {
    val wpcParser = new NomWPCParser
    val result = wpcParser.parser("{{pc|Barnais}} [Georges Auguste Charles {{pc|Guibourg}}, dit {{nom w pc||Georgius}}")
    println(result)
    result shouldEqual "Barnais [Georges Auguste Charles Guibourg, dit Georgius"
  }

  it should "parse pasquerette" in {
    val wpcParser = new NomWPCParser
    val result = wpcParser.parser(""": {{date|lang=fr|1553}}{{R|TLFi}} Du {{étyl|frm|fr|pasquerette}}, ''{{lien|pasquette|fro}}'' en {{étyl|fro|fr}}{{R|Godefroy|pasquette}}. L’étymologie du premier mot est divisée en deux propositions :
                                    |:* Le ''Trésor de la Langue Française''{{R|TLFi}}, le ''Dictionnaire étymologique et historique du français''{{RÉF|6}}, le ''Dictionnaire de l’Académie Française''{{R|DAF9}} et le ''Petit Robert''{{RÉF|8}} indiquent que le terme vient de ''{{lien|Pâques|fr}}'', parce que c’est la date du début de sa floraison, via l’adjectif du {{étyl|frm|fr|pasqueret}}{{RÉF|6}}{{R|DMF|pasqueret}} qui se rapporte à cette fête.
                                    |:* {{w|François Noël}} donne dans son ''Dictionnaire étymologique, critique, historique, et anecdotique de la langue française''{{RÉF|7}} une autre origine, reprise par {{w|Émile Littré}}{{R|Littré}} qui réfute la précédente ainsi : « Mais ce ne peut être la cause du nom de la plante, puisqu’elle fleurit à peu près toute l’année{{R|Littré}}. » Il indique en forme originelle l’{{étyl|fro|fr|pasquierette}} qui vient de {{lien|''pasquier''|fro|sens=[[pâquis]], [[pâturage]]}}, avec le suffixe ''{{lien|-ette|fr}}''. La fleur serait ainsi nommée parce qu’elle parsème nos prairies.""".trim)
    println(result)
  }
}