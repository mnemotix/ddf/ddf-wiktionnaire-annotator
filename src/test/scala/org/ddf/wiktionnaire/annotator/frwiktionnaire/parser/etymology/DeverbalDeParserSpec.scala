package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DeverbalDeParserSpec extends AnyFlatSpec with Matchers{
  val deverbalDe = new DeverbalDeParser

  it should "convert a simple model with two words" in {
    val accueillir = "{{déverbal de|accueillir|lang=fr}}."
    deverbalDe.parser(accueillir) shouldEqual "déverbal de [[accueillir]]."
  }

  it should "convert a simple model with renvier" in {
    val contrebrandier = "{{déverbal de|renvier|fr}} ; voir ''{{lien|envi|fro}}''."
    deverbalDe.parser(contrebrandier) shouldEqual "déverbal de [[renvier]] ; voir ''{{lien|envi|fro}}''."
  }

  it should "parse déverbal sans suffixe" in {
    val grigne = "{{déverbal sans suffixe|grigner|fr}}"
    deverbalDe.parser(grigne) shouldEqual "déverbal de [[grigner]]"
  }

  it should "parse déverbal sans suffixe with m=1 and de=" in {
    val file = "{{déverbal sans suffixe|de=filer|fr|m=1}}"
    deverbalDe.parser(file) shouldEqual "déverbal de [[filer]]"
  }
}