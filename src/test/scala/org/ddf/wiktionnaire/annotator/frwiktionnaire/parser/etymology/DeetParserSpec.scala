package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DeetParserSpec extends AnyFlatSpec with Matchers {

  val deetParser = new DeetParser

  it should "convert a simple model with two words" in {
    val grandBretagne = "{{deet|grand|Bretagne|lang=fr}}."
    TemplateReplacer.replaceTemplates(grandBretagne) shouldEqual "composé de grand et de Bretagne."
  }

  it should "convert a model with a suffixe" in {
    val contrebrandier = "{{deet|contrebande|-ier|lang=fr}}."
    TemplateReplacer.replaceTemplates(contrebrandier) shouldEqual "dérivé de contrebande avec le suffixe -ier."
  }

  it should "convert a model with a suffixe francouzsky" in {
    val contrebrandier = "{{deet|Francouz|-ský|lang=cs}}."
    TemplateReplacer.replaceTemplates(contrebrandier) shouldEqual "dérivé de Francouz avec le suffixe -ský."
  }

  it should "convert a model with a prefixe" in {
    val antiquark = "{{deet|anti-|quark|lang=en}}."
    TemplateReplacer.replaceTemplates(antiquark) shouldEqual "dérivé de quark avec le préfixe anti-."
  }

  it should "convert a model with a prefixe auto-mysophobie" in {
    val automysophobie = "{{deet|auto-|mysophobie|lang=fr|issu=grc}}."
    TemplateReplacer.replaceTemplates(automysophobie) shouldEqual "dérivé de mysophobie avec le préfixe auto-."
  }

  it should "convert a model with three words" in {
    val abc = "{{deet|A|B|C|lang=en}}."
    TemplateReplacer.replaceTemplates(abc) shouldEqual "composé de A, B et de C."
  }
}