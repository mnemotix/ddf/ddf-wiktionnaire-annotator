/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class FormatNumParserSpec extends AnyFlatSpec with Matchers {

  it should "parse an definition with a  formatnum" in {
    val definition = """''Ce sont ainsi {{formatnum:62826}} et {{formatnum:50650}} couples (''littéral anglais, littéral français'') que nous avons extrait des '''Wiktionnaires''' anglais et français respectivement, et pas moins de {{formatnum:286822}} couples à partir de la Wikipedia anglaise.''"""
    val parser = new FormatNumParser
    println(TemplateReplacer.replaceTemplates(definition))
    parser.parser(definition) shouldEqual(
      """''Ce sont ainsi 62 826 et 50 650 couples (''littéral anglais, littéral français'') que nous avons extrait des '''Wiktionnaires''' anglais et français respectivement, et pas moins de 286 822 couples à partir de la Wikipedia anglaise.''""".stripMargin
    )
  }

  it should "parse an definition with a formatnum with an." in {
    val definition = """''Le Fit-worari Birro fit son entrée à la tête de plus de {{formatnum:6000}} hommes, et, avec un '''appareil''' militaire qui éveilla les jalousies des grands vassaux du Ras, mais qui flatta l’orgueil de sa belle-mère […]''
                       | {{Royaume-Uni|fr}} {{Commonwealth|fr}} [[mesure|Mesure]] [[britannique]] de [[capacité]], pour les [[liquide]]s, valant {{formatnum:4.54609}} [[litre]]s.""".stripMargin
    val parser = new FormatNumParser
    println(parser.parser(definition))
  }

  it should "parse a definition with a long number" in {
    val definition = """''D’ailleurs l’actuel chef de l’État et certains membres du staff dirigeant de l’armée traînent une « '''casserole''' » (arriérés de soldes, à l’issue de conventions et promesses) de près de {{formatnum:7000000000}} €, '''casserole''' qui était, petit à petit, entrain de se muer en Épée de Damoclès.''""".stripMargin
    val parser = new FormatNumParser
    val nbr = "7000000000"
    println(nbr.toLong)
    println(parser.parser(definition))
  }

  it should "parse a definition with an expr" in {
    val definition = """ {{unités|fr}} {{lexique|informatique|fr}} [[unité de mesure|Unité de mesure]] de [[quantité]] d’[[information]] [[numérique]], valant {{formatnum:{{#expr:2 ^ 30}}}} (2{{e|30}}) [[octet]]s, et dont le [[symbole]] est '''[[Go]]'''. {{usage}} Cet usage de ''[[giga-]]'' est critiqué par le SI mais encore très fréquent.""".stripMargin
    val parser = new FormatNumParser
    println(parser.parser(definition))
  }

  it should "parse a definition with a long nbr" in {
    val definition = """{{formatnum:1000000000000}}"""
    val parser = new FormatNumParser
    println(parser.parser(definition))
  }
}
