package org.ddf.wiktionnaire.annotator.frwiktionnaire.helper

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StringCleanerSpec extends AnyFlatSpec with Matchers with BeforeAndAfterAll with ScalaFutures {

  behavior of "TextCleaners"

  it should "clean the beginning of string" in {
    val first = ", Serpillière."
    val second = "(, « Ne pleure plus », album Les ronds de carotte)"
    val third = "(, Cambrinus)"

    println(TextCleaners.cleanStartOfString(first))
    println(TextCleaners.cleanStartOfString(second))
    println(TextCleaners.cleanStartOfString(third))

  }

}
