package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DateModelParserSpec extends AnyFlatSpec with Matchers {
  behavior of "DataModelParsing"

  val datemodelParser = new DateModelParser

  it should "convert the date model of wiktionnaire in french" in {
    val definition = "{{siècle|Milieu XVII|fin XVIII}} blablabla et reblabla"
    val definition2 = "{{siècle|XVII|doute=oui}} que du blabla"
    val definition3 = "{{siècle|Vers le XI av. J.-C.}} encore du bla"
    val definition4 = "{{siècle|XIX}} un peu de blabla"
    val definition5 = "{{siècle|Fin XIX}} vous avez dit blabla en {{siècle|Vers le XI av. J.-C.}} avec du {{base je ne suis pas siècle}}"
    val definition6 = "{{date|lang=fr}} je ne connais pas cette date"
    val definition7 = "{{date|lang=fr|1823}} Nom donné par le minéralogiste Kupffer à partir du nom du topotype :Lles {{w|Monts Ilmen}} dans l'[[Oural]]."
    val definition8 = "{{siècle|XVII}}"
    val definition9 = "{{siècle2|XIX}}"
    val definition10 = "{{siècle|lang=fr|?}} Du {{étyl|la|fr|mot=itinerans|sens=voyageant}} ; ''{{lien|itinérer|fr}}'' semble être un emprunt postérieur."

    println(TemplateReplacer.replaceTemplates(definition))
    println(TemplateReplacer.replaceTemplates(definition2))
    println(TemplateReplacer.replaceTemplates(definition3))
    println(TemplateReplacer.replaceTemplates(definition4))
    println(TemplateReplacer.replaceTemplates(definition5))
    println(TemplateReplacer.replaceTemplates(definition6))
    println(TemplateReplacer.replaceTemplates(definition7))
    println(TemplateReplacer.replaceTemplates(definition8))
    println(TemplateReplacer.replaceTemplates(definition9))
    println(TemplateReplacer.replaceTemplates(definition10))

    datemodelParser.parser(definition) shouldEqual "(Milieu XVIIe siècle fin XVIIIe siècle) blablabla et reblabla"
    datemodelParser.parser(definition2) shouldEqual "(XVIIe siècle ?) que du blabla"
    datemodelParser.parser(definition3) shouldEqual "(Vers le XIe siècle av. J.-C.) encore du bla"
    datemodelParser.parser(definition4) shouldEqual "(XIXe siècle) un peu de blabla"
    datemodelParser.parser(definition5) shouldEqual "(Fin XIXe siècle) vous avez dit blabla en (Vers le XIe siècle av. J.-C.) avec du {{base je ne suis pas siècle}}"
    datemodelParser.parser(definition6) shouldEqual "je ne connais pas cette date"
    datemodelParser.parser(definition7) shouldEqual "(1823) Nom donné par le minéralogiste Kupffer à partir du nom du topotype :Lles {{w|Monts Ilmen}} dans l'[[Oural]]."
    datemodelParser.parser(definition8) shouldEqual "(XVIIe siècle)"
    datemodelParser.parser(definition9) shouldEqual "(XIXe siècle)"
    datemodelParser.parser(definition10) shouldEqual "Du {{étyl|la|fr|mot=itinerans|sens=voyageant}} ; ''{{lien|itinérer|fr}}'' semble être un emprunt postérieur."
  }

  it should "parse a siecle model on second line" in {
    val etym  = """: De l’[[algonquin]] ''tabaguia'' (« festin »), le sens ayant évolué sous l’influence de ''[[tabac]]''. {{date|1603}} Avec le sens de « festin, banquet chez les [[Algonquins]] » ([[w:Samuel de Champlain|Samuel de Champlain]], ''Des Sauvages'').
                  |: {{siècle|Milieu du XVII}} « lieu où l’on se réunissait pour fumer et boire ».""".stripMargin

    println(datemodelParser.parser(etym))
  }
}
