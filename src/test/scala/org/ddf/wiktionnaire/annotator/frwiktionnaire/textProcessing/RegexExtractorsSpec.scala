package org.ddf.wiktionnaire.annotator.frwiktionnaire.textProcessing


import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.RegexExtractors
import org.ddf.wiktionnaire.annotator.test.data.WikitextBulkData
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RegexExtractorsSpec extends AnyFlatSpec with Matchers {

  it should "extract Skos Definition" in {

      RegexExtractors.extractSKOSDefinition(WikitextBulkData.griboullis).toSeq.size shouldEqual 1
      RegexExtractors.extractSKOSDefinition(WikitextBulkData.mazagran).toSeq.size shouldEqual 2
      RegexExtractors.extractSKOSDefinition(WikitextBulkData.affaireeAdj).toSeq.size shouldEqual 1
      RegexExtractors.extractSKOSDefinition(WikitextBulkData.francophonieMAJ).toSeq.size shouldEqual 2
      RegexExtractors.extractSKOSDefinition(WikitextBulkData.avoir).toSeq.size shouldEqual  16


      RegexExtractors.extractSKOSDefinition(WikitextBulkData.moelle).toSeq.foreach { regex =>
        regex
      }

    RegexExtractors.extractSKOSDefinition(WikitextBulkData.autrice).toSeq.size shouldEqual(3)

    RegexExtractors.extractSKOSDefinition(WikitextBulkData.blanc).toSeq.size shouldEqual(20)
    //println(rgx)
  }

  it should "extract col definition" in {
    val rs = RegexExtractors.extractSKOSDefinition(WikitextBulkData.col)
    rs.toSeq.size shouldEqual 7
  }

  it should "extract citations" in {
    val citations =
      """|#* ''On ne distingue rien dans ce '''gribouillis'''.''
         |#* ''Je t'envoie sous ce pli le papier sur lequel je prenais des notes pendant cette visite. Ce n'est qu'un '''gribouillis''' illisible. Mais garde-le toute ta vie pour l'amour de moi.'' {{source|Victor Hugo, ''{{ws|Correspondance de Victor Hugo/1840|Correspondance de Victor Hugo}}'', 1947, page 582.}}
      """.stripMargin

    val citations2 =
      """#*''Celles-là venaient dès le crépuscule, s’installaient dans une anfractuosité bien éclairée, réclamaient, plutôt par contenance que par besoin réel, un petit verre de vespetro ou un « '''mazagran''' », puis surveillaient le passant d’un œil méticuleux.'' {{source|{{w|Auguste de Villiers de L'Isle-Adam}}, ''[[s:Les Demoiselles de Bienfilâtre|Les Demoiselles de Bienfilâtre]]'', dans les ''{{w|Contes cruels}}'', 1883, éd. J. Corti, 1954, vol. 1, p. 4}}
        |#* ''Gavard commanda deux autres '''mazagrans'''. Rose se hâta de servir les trois consommations, sous les yeux sévères de Logre.'' {{source|{{Citation/Émile Zola/Le Ventre de Paris/1878}}}}
        |#* ''Vous allez prendre quelque chose avec nous, comme on dit, ce qu’on appelait autrefois un '''mazagran''' ou un gloria, boissons qu’on ne trouve plus, comme curiosités archéologiques, que dans les pièces de Labiche et les cafés de Doncières.'' {{source|{{w|Marcel Proust}}, ''{{ws|Sodome_et_Gomorrhe/Partie_2_-_chapitre_3|Sodome et Gomorrhe}}'', 1921}}""".stripMargin

    RegexExtractors.extractCitation(citations).toSeq.size shouldEqual 2
    RegexExtractors.extractCitation(citations2).toSeq.size shouldEqual 3
  }

  it should "extract source in citation" in {
    val citation = "#* ''Gavard commanda deux autres '''mazagrans'''. Rose se hâta de servir les trois consommations, sous les yeux sévères de Logre.'' {{source|{{Citation/Émile Zola/Le Ventre de Paris/1878}}}}"
    RegexExtractors.extractSource(citation).toSeq.size shouldEqual 1
    RegexExtractors.extractSource(citation).toSeq(0).toString() shouldEqual "{{source|{{Citation/Émile Zola/Le Ventre de Paris/1878}}}}"
  }

  it should "extract phonetics" in {
    val wordType = """{{fr-inv|a.na.nɑ|pron2=a.na.nas|sp=1}}
                     |[[Image:Pineapple Oahu.jpg|thumb|Un '''ananas'''.]]
                     |[[Image:Ananas comosus Victoria P1190421.jpg|thumb|Fruits d’'''ananas'''.]]
                     |'''ananas''' {{pron|a.na.nɑ|fr}} ou {{pron|a.na.nas|fr}} {{m}}, {{sp}}
                     |# {{plantes|fr}} Plante [[tropical]]e originaire de l’[[Amérique du Sud]], qu’on cultive en [[Europe]] dans des [[serre]]s chaudes, dont les [[tige]]s courtes portent une [[rosette]] de [[feuille]]s [[épaisse]]s, [[sessile]]s, étroitement [[imbriquer|imbriquées]] et munies de [[piquant]]s parfois [[acéré]]s.
                     |# [[faux-fruit|Faux-fruit]] [[comestible]] de cette plante de forme [[conique]] à la peau [[écailleux|écailleuse]] et épaisse et à la [[chair]] jaune et [[sucré]]e.
                     |#* ''S’il restait encore un doute sur la supériorité de l’Inde, par rapport à ses fruits, l’'''ananas''' seul ferait pencher la balance en faveur de son heureuse patrie : il réunit toutes les bonnes qualités et tous les agrémens épars çà et là dans les autres : élégance de la forme, parfum fin et délicat, saveur exquise, suc abondant et distribué également dans tout le fruit, la nature a versé sur lui tous ses trésors.'' {{source|Père M. Perrin, ''Voyage dans l’Indostan'', 1807}}
                     |#* ''L’'''ananas''' est essentiellement cultivé pour son fruit consommé au naturel ou mis en conserve (tranches, morceaux, jus). Les feuilles peuvent être utilisées pour leurs fibres et dans l’alimentation du bétail. La plante entière peut être réduite en farine pour le bétail. On en extrait de l’amidon et de la broméline, mélange d’enzymes utilisé dans l’industrie pharmaceutique.'' {{source|Cirad/Gret/MAE, ''Mémento de l’agronome'', Cirad/Gret/Ministère des Affaires étrangères, Paris, 2002, page 945}}
                     |# {{militaire|fr}} {{term|argot des tranchées}} [[mine|Mine]] explosive de fabrication britannique, pendant la Première Guerre mondiale.
                     |#* ''En réponse, je piquai dans la tranchée anglaise quelques « '''ananas''' », c’est-à-dire des mines qui faisaient songer par leur forme à ce fruit exquis.'' {{source|{{nom w pc|Ernst|Jünger}}, ''{{w|Orages d’acier}}'', 1961 ; traduit de l’allemand par {{nom w pc|Henri|Plard}}, 1970, p. 88}}""".stripMargin

    val affaireADJ = """{{fr-accord-rég|ms=affairé|a.fe.ʁe|pron2=a.fɛ.ʁe}}
                       |'''affairé''' {{pron|a.fe.ʁe|fr}} ou {{pron|a.fɛ.ʁe|fr}}
                       |# Qui a bien des [[affaire]]s, qui est [[occupé]].
                       |#* ''Osman-Bey paraissait très-'''affairé'''. Des courriers lui arrivaient presque à chaque instant, porteurs de dépêches qu’il se faisait lire par un secrétaire et auxquelles il répondait immédiatement.'' {{source|{{nom w pc|Adolphe|Blanqui}}, ''Voyage en Bulgarie 1841'', chapitre X, 1845}}
                       |#* ''Il conservait son sans-gêne tranquille d’homme '''affairé''', chez lequel des accidents pareils ne produisent plus d’autre sensation qu’un désir de se débarrasser au plus vite d’un devoir qui manque de gaîté.'' {{source|{{nom w pc|Ivan|Tourgueniev}}, ''L’Exécution de Troppmann'', avril 1870, traduction française d’{{nom w pc|Isaac|Pavlovsky}}, publiée dans ses ''Souvenirs sur Tourguéneff'', Savine, 1887}}
                       |#* ''Près de Maidstone, ils tombèrent sur une rangée de onze canons automobiles de construction spéciale, autour desquels des artilleurs '''affairés''' surveillaient avec des jumelles une sorte de retranchement qu’on établissait sur la crête de la colline.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|54}}}}
                       |""".stripMargin

    RegexExtractors.extractphonetique(wordType).toSeq.size shouldEqual 2
    RegexExtractors.extractphonetique(affaireADJ).toSeq.size shouldEqual 2
  }

  it should "extract words for synonyms" in {
    val synonyms = """* [[noir corbeau]]
                     |* [[aile de corbeau]]
                     |* [[bleu corbeau]]""".stripMargin

    RegexExtractors.extractRelationWords(synonyms).toSeq.size shouldEqual 3
  }

  it should "extract title of section" in {
    val section = """=== {{S|étymologie}} ===
                    |: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
                    |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
                    |: ''(Verbe)'' Apocope de ''[[tester]]''.""".stripMargin

    RegexExtractors.extractSectionTitle(section) shouldEqual "=== {{S|étymologie}} ==="
  }

  it should "extract content of section" in {
    val section = """=== {{S|étymologie}} ===
                    |: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
                    |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
                    |: ''(Verbe)'' Apocope de ''[[tester]]''.""".stripMargin
    RegexExtractors.extractSectionContent(section) shouldEqual """: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
                                                                 |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
                                                                 |: ''(Verbe)'' Apocope de ''[[tester]]''.""".stripMargin
  }

  it should "extract etymology by post" in {
    val etym = """: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
                 |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
                 |: ''(Verbe)'' Apocope de ''[[tester]]''.""".stripMargin

    val etym2 = """:{{ébauche-étym|fr}}"""

    val etym3 = """: De ''[[pomme]]'' (dans le sens de « [[fruit]] ») et ''[[terre]]''.
                  |: « Pomme de terre » est une expression figée, calquée sur le {{étyl|la|fr|malum terrae}}, qui est attestée en français depuis 1488 pour désigner diverses plantes à tubercules ou bulbes<ref>''Le Robert, dictionnaire historique de la langue française'' sous la direction d’Alain Rey, éditions Le Robert, Paris 19921, {{ISBN|2-85036-187-9}}, p. 1574.</ref>. Elle a désigné ensuite le [[topinambour]] sous l’influence du {{étyl|nl|fr|mot=aardappel}}, littéralement « pomme de terre ». Par la suite, le topinambour a pris son nom actuel avec l’exhibition à Paris d’Amérindiens de la tribu des Tupis et le nom de pomme de terre s’est définitivement appliqué à ''[[Solanum tuberosum]]'' notamment sous l’action de popularisation de ce tubercule entreprise par Parmentier à partir de 1773.""".stripMargin

    val etym4 = """: ([[#Nom commun 1|''Nom 1'']]) {{date|lang=fr}} De l’{{étyl|arz|fr|mot=شيشة|tr=šīša}}, issu du {{étyl|tr|fr|mot=şişe}}, lui-même issu du {{étyl|fa|fr|mot=شیشه|tr=šīša|sens=[[verre]]}}.
                  |: ([[#Nom commun 2|''Nom 2'']]) {{date|lang=fr}} De l’{{étyl|es|fr|mot=chicha|sens=[[pois chiche]]}}, lui-même du latin ''[[cicer#la|cicer]]''.""".stripMargin

    val etym5 = """: ''([[#fr-nom|Nom]])'' Du {{étyl|frm|fr|mot=corbeau}}, de l’{{étyl|fro|fr|mot=corbel}}, dérivé de {{lien|''corb''|fro}}, {{lien|''corp''|fro}}, du latin populaire de Gaule {{recons|corbus}}, du {{étyl|la|fr}} classique {{lien|''corvus''|la}}, de même sens. <br/>Le sens d’« auteur de lettres anonymes » remonte à l’affaire Angèle Laval qui inonda Tulle de lettres anonymes en 1920. L’affaire inspira le film ''[[w:Le Corbeau (film, 1943)|Le Corbeau]]'' de {{w|Henri-Georges Clouzot}} dans lequel une série de lettres anonymes signées « Le Corbeau » s’abat sur une petite ville française.
                  |: ''([[#fr-adj|Adjectif]])'' En référence à la couleur des plumes du [[grand corbeau]].""".stripMargin

    val etym6 = """: {{term|adjectif 1}} Du {{étyl|la|fr|mot=nominabilis}}.
                  |: {{term|adjectif 2}} Mot {{compos|nominer|-able|lang=fr}}.""".stripMargin

    RegexExtractors.extractEtymologyByPOS(etym).size shouldEqual 3
    RegexExtractors.extractEtymologyByPOS(etym2).size shouldEqual 0
    RegexExtractors.extractEtymologyByPOS(etym3).size shouldEqual 0
    RegexExtractors.extractEtymologyByPOS(etym4).size shouldEqual 2
    RegexExtractors.extractEtymologyByPOS(etym5).size shouldEqual 2
    //RegexExtractors.extractEtymologyByPOS(etym6).size shouldEqual 2
  }

  it should "extract Lexique" in {
    val lexical2 = "{{lexique|agriculture|élevage|pharmacie|fr}} [[substance|Substance]] [[phytosanitaire]] et [[pharmaceutique]]"
    val lexical = "{{lexique|équitation|sport|fr}} equitation et sport"
    RegexExtractors.extractLexique(lexical).foreach { regex =>
      val s = regex.group(2)
      println(s)
    }

    RegexExtractors.extractLexique(lexical2).foreach { regex =>
      val s = regex.group(2)
      println(s)
    }
  }

  //
  //

  it should "extract inflection of" in {
    RegexExtractors.extractInflectionOf("''Première personne du pluriel de l’imparfait de'' [[soudre]].").map{ rgex =>
      println(rgex.group(2))
    }
    RegexExtractors.extractInflectionOf("''Pluriel de'' [[jalouse#fr|jalouse]]").map{ rgex =>
      println(rgex.group(2))
    }

    RegexExtractors.extractInflectionOf("''Deuxième personne du singulier de l’impératif présent de'' [[plager]].").map{ rgex =>
      println(rgex.group(2))
    }

    RegexExtractors.extractInflectionOf("Pluriel de ''[[le#fr-art-déf|le]]'' ou ''[[la#fr-flex-art-déf|la]]''.").map{ rgex =>
      println(rgex.group(2))
    }
  }

  it should "extract lexinfo for affaire" in {
    val affairee = """{{fr-accord-rég|ms=affairé|a.fe.ʁe|pron2=a.fɛ.ʁe}}
                     |'''affairé''' {{pron|a.fe.ʁe|fr}} ou {{pron|a.fɛ.ʁe|fr}}
                     |# Qui a bien des [[affaire]]s, qui est [[occupé]].
                     |#* ''Osman-Bey paraissait très-'''affairé'''. Des courriers lui arrivaient presque à chaque instant, porteurs de dépêches qu’il se faisait lire par un secrétaire et auxquelles il répondait immédiatement.'' {{source|{{nom w pc|Adolphe|Blanqui}}, ''Voyage en Bulgarie 1841'', chapitre X, 1845}}
                     |#* ''Il conservait son sans-gêne tranquille d’homme '''affairé''', chez lequel des accidents pareils ne produisent plus d’autre sensation qu’un désir de se débarrasser au plus vite d’un devoir qui manque de gaîté.'' {{source|{{nom w pc|Ivan|Tourgueniev}}, ''L’Exécution de Troppmann'', avril 1870, traduction française d’{{nom w pc|Isaac|Pavlovsky}}, publiée dans ses ''Souvenirs sur Tourguéneff'', Savine, 1887}}
                     |#* ''Près de Maidstone, ils tombèrent sur une rangée de onze canons automobiles de construction spéciale, autour desquels des artilleurs '''affairés''' surveillaient avec des jumelles une sorte de retranchement qu’on établissait sur la crête de la colline.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|54}}}}
                     |{{fr-rég|ma.za.ɡʁɑ̃}}
                     |[[Fichier:Mazagran 1.jpg|thumb|'''Mazagran''' en faïence. (1)]]""".stripMargin
    val res = "(?m)^'''.*$".r.findFirstIn(affairee)
    res.get shouldEqual("'''affairé''' {{pron|a.fe.ʁe|fr}} ou {{pron|a.fɛ.ʁe|fr}}")
  }

  it should "extact lexinfo for many words" in {
    val mazagran = RegexExtractors.extractLexicalInfo(WikitextBulkData.mazagran)
    val gribouillis = RegexExtractors.extractLexicalInfo(WikitextBulkData.griboullis)
    val francophonyMaj = RegexExtractors.extractLexicalInfo(WikitextBulkData.francophonieMAJ)
    val col = RegexExtractors.extractLexicalInfo(WikitextBulkData.col)
    val pomme = RegexExtractors.extractLexicalInfo(WikitextBulkData.pomme)
    val feu = RegexExtractors.extractLexicalInfo(WikitextBulkData.feu)

    mazagran.get shouldEqual "'''mazagran''' {{pron|ma.za.ɡʁɑ̃|fr}} {{m}}"
    gribouillis.get shouldEqual "'''gribouillis''' {{pron|ɡʁi.bu.ji|fr}} {{m}} {{sp}}"
    francophonyMaj.get shouldEqual "'''Francophonie''' {{pron|fʁɑ̃.kɔ.fɔ.ni|fr}} {{f}}, {{au singulier uniquement|fr}}"
    col.get shouldEqual "'''col''' {{pron|kɔl|fr}} {{m}}"
    pomme.get shouldEqual "'''pomme''' {{pron|pɔm|fr}} {{f}}"
    feu.get shouldEqual "'''feu''' {{pron|fø|fr}} {{m}}"
  }

}
