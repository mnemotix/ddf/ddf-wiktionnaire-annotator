package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.ddf.wiktionnaire.annotator.frwiktionnaire.helper.EtymologyHelper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class CalqueDeParserSpec extends AnyFlatSpec with Matchers {

  it should "parse simple calque du" in {
    val interdetroit = "calque du {{calque|zh|fr}} [[海峡两岸]] (simplifié)"
    //calqueDu.convert(interdetroit) shouldEqual "calque du chinois [[海峡两岸]] (simplifié)"
    TemplateReplacer.replaceTemplates(interdetroit) shouldEqual "calque du chinois [[海峡两岸]] (simplifié)"
  }

  it should "parse with mot= in model" in {
    val IJselHollandais = "Calque du {{calque|nl|fr|mot=Hollandse IJssel}}."
    //calqueDu.convert(IJselHollandais) shouldEqual "Calque du néerlandais Hollandse IJssel."
    TemplateReplacer.replaceTemplates(IJselHollandais) shouldEqual "Calque du néerlandais Hollandse IJssel."

  }

  it should "parse without the mot= in model" in {
    val saintPaul = "Calque du {{calque|pt|fr|São Paulo}}."
    //calqueDu.convert(saintPaul) shouldEqual "Calque du portugais São Paulo."
    TemplateReplacer.replaceTemplates(saintPaul) shouldEqual "Calque du portugais São Paulo."
  }

  it should "parse with 5 params" in {
    val fenggong = "calque partiel du {{calque|zh|fr|風鑼|fēngluó}}"
    //calqueDu.convert(fenggong) shouldEqual "calque partiel du chinois 風鑼 fēngluó"
    TemplateReplacer.replaceTemplates(fenggong) shouldEqual "calque partiel du chinois 風鑼 fēngluó"
  }

  it should "parse with 6 params" in {
    val filsduciel = "Calque du {{calque|zh|fr|天子|tiānzǐ|fils du ciel}}"
    //calqueDu.convert(filsduciel) shouldEqual "Calque du chinois 天子 tiānzǐ (« fils du ciel »)"
    TemplateReplacer.replaceTemplates(filsduciel) shouldEqual "Calque du chinois 天子 tiānzǐ (« fils du ciel »)"
  }

  it should "parse with the model m=1" in {
    val eau2chaude = """Calque de l’{{calque|en|fr|m=1}}  ''[[to be in hot water]]'' ou ''[[to get into hot water]]'', de même sens."""
    TemplateReplacer.replaceTemplates(eau2chaude) shouldEqual
      """Calque de l’anglais ''[[to be in hot water]]'' ou ''[[to get into hot water]]'', de même sens.""".stripMargin
  }
}
