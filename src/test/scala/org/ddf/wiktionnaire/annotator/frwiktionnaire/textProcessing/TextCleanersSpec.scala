package org.ddf.wiktionnaire.annotator.frwiktionnaire.textProcessing

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class TextCleanersSpec extends AnyFlatSpec with Matchers {
  it should "cleanWordSectionTitle" in {
    val toclean = "=== {{S|nom|fr|num=1}} ==="
    TextCleaners.cleanWordSectionTitle(toclean) shouldEqual "nom"
    val tocleanSimple = "=== {{S|verbe|fr}} ==="
    TextCleaners.cleanWordSectionTitle(tocleanSimple) shouldEqual "verbe"
  }

  it should "clean Section to a recognizable" in {
    //  to === {{S|nom|fr}} ===
    val toclean = "=== {{S|nom|fr|num=1}} ==="
    val toclean2 = "=== {{S|nom|fr|num=3}} ==="
    val toclean3 = "=== {{S|nom|fr|num=389}} ==="

    TextCleaners.cleanWordTitle(toclean) shouldEqual "=== {{S|nom|fr}} ==="
    TextCleaners.cleanWordTitle(toclean2) shouldEqual "=== {{S|nom|fr}} ==="
    TextCleaners.cleanWordTitle(toclean3) shouldEqual "=== {{S|nom|fr}} ==="

  }

  it should "clean flexion to a recognizable" in {
    val toClean = "=== {{S|verbe|fr|flexion}} ==="
    val toclean2 = "=== {{S|verbe|fr|flexion|num=1}} ==="

    TextCleaners.cleanWordTitle(toClean) shouldEqual "=== {{S|verbe|fr}} ==="
    TextCleaners.cleanWordTitle(toclean2) shouldEqual "=== {{S|verbe|fr}} ==="
  }

  it should "clean a definition with ? model" in {
    val definition = """# {{lexique|informatique|fr}} Emplacement connectant plusieurs machines. {{?|la référence dit qu'un noeud est une machine|fr}}
                       |""".stripMargin
    val rs = TextCleaners.cleanDefinition(definition)
    println(rs)
  }

  it should "clean a definition with Image" in {
    val definition = """# {{lexique|physique|fr}} Point où l’[[amplitude]] de l’[[onde stationnaire]] est toujours nulle. [[Image:Standing wave.gif|vignette|Les points rouges représentent les '''nœuds''' (13) de l’onde stationnaire.]]"""
    val rs = TextCleaners.cleanDefinition(definition)
    println(rs)
  }

  it should "clean a definition in noeud" in {
    val definition = """{{lexique|littérature|fr}} {{particulier}} [[incident|Incident]] qui détermine la [[marche]] de la [[pièce de théâtre]], d’où [[dépendre|dépend]] l’[[intrigue]] d’une [[action]] [[dramatique]], {{cf|lang=fr|nœud de l’action}}."""
    val rs = TextCleaners.cleanDefinition(definition)
    println(rs)
  }

  it should "clean an etymology" in {
    val etym = """: {{siècle|XIII}}{{R|TLFi}} De l’{{étyl|es|fr|mot=carmesí}}{{R|Littré}} ou de l’{{étyl|it|fr|mot=chermisi}}{{R|Littré}}{{R|TLFi}}, ''{{lien|cremisi|it}}'', eux-mêmes de l’{{étyl|ar|fr}} {{ar-mot|qirmiziy²ũ}} : carmin, cramoisi, écarlate ; voir ''{{lien|kermès|fr}}''."""
    val definition = """D’une [[couleur]] [[rouge]] [[foncé]], [[tirer|tirant]] sur le [[violet]].{{couleur|#DC143C}}"""
    val rs = TextCleaners.cleanDefinition(etym)
    println(rs)
  }

  // todo close DDFS-188
  it should "clean a definition with [[Annexe:.....|..{{e|...}}]]" in {
    val definition = "{{info lex|métrologie}} {{lexique|physique|fr}} [[unité de mesure|Unité de mesure]] d’[[intensité lumineuse]] du [[Système international]] (SI), valant '''[[Annexe:Principales puissances de 10|10{{e|&minus;18}}]]''' '''[[candela#fr|candela]]''', et dont le [[symbole]] est '''[[acd#conv|acd]]'''."
    TextCleaners.cleanDefinition(definition) shouldEqual "<a href=\"/form/unité de mesure\">Unité de mesure</a> d’<a href=\"/form/intensité lumineuse\">intensité lumineuse</a> du <a href=\"/form/Système international\">Système international</a> (SI), valant <b>10<sup>&minus;18</sup></b> <b><a href=\"/form/candela#fr\">candela</a></b>, et dont le <a href=\"/form/symbole\">symbole</a> est <b><a href=\"/form/acd#conv\">acd</a></b>."
  }
}