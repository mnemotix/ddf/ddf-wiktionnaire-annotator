package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WModelParserSpec extends AnyFlatSpec with Matchers {
  // {{w|Martin Luther}}

  val  wModelParser = new WModelParser()

  it should "replace W Matin Luther" in {
    val luther =  "{{w|Martin Luther}}"
    println(TemplateReplacer.replaceTemplates(luther))
    wModelParser.parser(luther) shouldEqual "[[Martin Luther]]"
  }

  // {{w|zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique}}
  it should "replace  W in zone naturelle d’intérêt écologique faunistique et floristique" in {
    val znieff = "{{w|zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique}}"
    wModelParser.parser(znieff) shouldEqual "[[zone naturelle d’intérêt écologique faunistique et floristique|Zone naturelle d’intérêt écologique faunistique et floristique]]"
  }

  it should "replace w in long phrases" in {
    val cheval = """(Début du XIIe siècle) Du latin populaire ''[[caballus]]'', du gaulois <sup>*</sup>[[caballos}}]] qui signifiait au départ « cheval de trait ». Ce mot s’imposa très vite
                   |en toute la Romania (aires linguistiques qui allaient donner naissance aux langues romanes) et supplanta le classique ''[[equus]]''
                   |probablement avant le milieu du (IIIe siècle) siècle : le roumain ''[[cal]]'' renvoie évidemment à ''[[caballus]]'' ; or, la séparation
                   |linguistique de la {{w|Dacie}} """.stripMargin

    println(wModelParser.parser(cheval))
  }
}
