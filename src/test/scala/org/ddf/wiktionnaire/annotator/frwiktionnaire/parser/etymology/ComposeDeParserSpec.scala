package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ComposeDeParserSpec extends AnyFlatSpec with Matchers {

  val composeDeparser = new ComposeDeParser

  it should "parse troisième" in {
    val troisieme = "{{compos|trois|-ième|lang=fr|m=1}}."
    TemplateReplacer.replaceTemplates(troisieme) shouldEqual "Dérivé de trois avec le suffixe -ième."
  }

  it should "parse compose de simple" in {
    val longusaevum = "Mot {{compos|longus|aevum|lang=la}}."
    TemplateReplacer.replaceTemplates(longusaevum) shouldEqual "Mot composé de longus et de aevum."
  }

  it should "parse compose de in Berlot with sens" in {
    val berlot = """Possiblement dérivé de ''[[berline]]'', avec pour variante ''brelot''<ref>Honoré Beaugrand, François Ricard, ''La chasse-galerie et autres récits'', 1989</ref> peut-être le {{compos|brêler|sens1=attacher|-ot|sens=[[attelage]]|lang=fr}} ;""".stripMargin

    TemplateReplacer.replaceTemplates(berlot) shouldEqual
      """Possiblement dérivé de ''[[berline]]'', avec pour variante ''brelot''<ref>Honoré Beaugrand, François Ricard, ''La chasse-galerie et autres récits'', 1989</ref> peut-être le dérivé de brêler (« attacher »), avec le suffixe -ot, littéralement « [[attelage]] » ;""".stripMargin
  }

  it should "parse compose de with two sens" in {
    val sinogramme = "{{compos|m=1|lang=fr|sino-|sens1=relatif à la Chine|gramme|sens2=relatif à l’écriture}}."
    val vivusPario = "{{compos|vivus|sens1=vivant|pario|sens2=engendrer|lang=la}}."

    TemplateReplacer.replaceTemplates(sinogramme) shouldEqual "Dérivé de gramme (« relatif à l'écriture »), avec le préfixe sino- (« relatif à la Chine »),."
  }

  it should "parse compose de in bendejunois with a suffixe" in {
    val bendejunois = "{{compos|Bendejun|-ois|lang=fr|m=oui}}."
    TemplateReplacer.replaceTemplates(bendejunois) shouldEqual "Dérivé de Bendejun avec le suffixe -ois."
  }

  it should "parse compose de with one sens" in {
    val neerlandais = "{{compos|Néerlande|sens1=[[Pays-Bas]]|-ais|lang=fr|m=1}}."
    TemplateReplacer.replaceTemplates(neerlandais) shouldEqual "Dérivé de Néerlande (« [[Pays-Bas]] »), avec le suffixe -ais."
  }

  it should "parse compose de in larvicide with a suffixe" in {
    val larvicide = "{{compos|m=1|larve|-cide|lang=fr}}"
    TemplateReplacer.replaceTemplates(larvicide) shouldEqual "Dérivé de larve avec le suffixe -cide"
  }

  it should "parse compose de in hélas with a lang=fr" in {
    val helas = "{{compos|hé|las|lang=fr}}, au sens ancien de « malheureux »."
    TemplateReplacer.replaceTemplates(helas) shouldEqual "composé de hé et de las, au sens ancien de « malheureux »."
  }

  it should "parse compose de in ostéomancie with sens\\d, tr\\d" in {
    val osteomancie = "Du grec ancien, {{composé de|ὀστέον|tr1=ostéon|sens1=[[os]]|μαντεία|tr2=manteia|sens2=divination|lang=grc}}."
    TemplateReplacer.replaceTemplates(osteomancie) shouldEqual "Du grec ancien, composé de ὀστέον ''ostéon'' (« [[os]] »), μαντεία ''manteia'' (« divination »)."
  }

  it should "parse compose de in jabisen with sens\\d, tr\\d not in order" in {
    val jabisen = "{{compos|蛇|皮|線|tr1=hebi|tr2=hi|tr3=sen|sens1=serpent|sens2=peau|sens3=corde|lang=ja}}."
    TemplateReplacer.replaceTemplates(jabisen) shouldEqual "composé de 蛇 ''hebi'' (« serpent »), 皮 ''hi'' (« peau »), 線 ''sen'' (« corde »)."
  }

  it should "parse compose de in longusaevum with sens\\d not in order" in {
    val longusaevum = "Mot {{compos|longus|aevum|sens=long temps|lang=la}}."
    TemplateReplacer.replaceTemplates(longusaevum) shouldEqual "Mot composé de longus et de aevum, littéralement (« long temps »)."
  }

  it should "parse compose de in mésoxérophile with a prefixe" in {
    val mesoxerophile = ": Mot {{compos|lang=fr|méso-|xérophile}}"
    TemplateReplacer.replaceTemplates(mesoxerophile) shouldEqual ": Mot dérivé de xérophile avec le préfixe méso-"
  }

  it should "test deriveDe to see if it should change to derive de" in {
    val compo1 = "{{compos|蛇|皮|線|tr1=hebi|tr2=hi|tr3=sen|sens1=serpent|sens2=peau|sens3=corde|lang=ja}}"
    val compo2 = "{{compos|lang=fr|méso-|xérophile}}"
    val compo3 = "{{compos|m=1|larve|-cide|lang=fr}}"
    val compo4 = "composé de|m=1|hygro-|-phobie|lang=fr"

    val compo5 = "{{compos|brêler|sens1=attacher|-ot|sens=[[attelage]]|lang=fr}}"

    TemplateReplacer.replaceTemplates(compo1)
    TemplateReplacer.replaceTemplates(compo2)
    TemplateReplacer.replaceTemplates(compo3)
    TemplateReplacer.replaceTemplates(compo4)
    TemplateReplacer.replaceTemplates(compo5)
  }

  it should "test compose de model of conjonction de coordination" in {
    val compo = "{{composé de|m=1|conjonction|coordination|lang=fr}}."
    TemplateReplacer.replaceTemplates(compo) shouldEqual "Composé de conjonction et de coordination."
  }

  it should "test compose de model for statalisme" in {
    val compo = "Concept décrit par Jacques Pohl, « ''Le statalisme'' », Travaux de linguistique et de littérature, 1984, t. XXII, no 1, p. 251-264</ref>, {{composé de|statal|-isme|lang=fr}}."
    TemplateReplacer.replaceTemplates(compo) shouldEqual
      "Concept décrit par Jacques Pohl, « ''Le statalisme'' », Travaux de linguistique et de littérature, 1984, t. XXII, no 1, p. 251-264</ref>, dérivé de statal avec le suffixe -isme."
  }
}