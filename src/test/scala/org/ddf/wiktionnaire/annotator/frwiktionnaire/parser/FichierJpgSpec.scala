/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

// todo close DDFS-213

class FichierJpgSpec extends AnyFlatSpec with Matchers {

  it should "parse [[Images:...|vignette|...]]" in {
    val definition = "# {{lexique|physique|fr}} Point où l’[[amplitude]] de l’[[onde stationnaire]] est toujours nulle. [[Image:Standing wave.gif|vignette|Les points rouges représentent les '''nœuds''' (13) de l’onde stationnaire.]]"
     TextCleaners.cleanDefinition(definition) shouldEqual "Point où l’<a href=\"/form/amplitude\">amplitude</a> de l’<a href=\"/form/onde stationnaire\">onde stationnaire</a> est toujours nulle."
  }

  it should "parse [[File:...|...|thumb]]" in {
    val definition = "# [[côté|Côté]] [[vers]] [[lequel]] une [[personne]] ou une [[chose]] se dirige, est dirigée ou [[tourner|tournée]] ; [[voie]] suivie pour [[arriver]] à un point [[donner|donné]]. [[File:Etigny-FR-89-la gare-a1.jpg|thumb]]"
    TextCleaners.cleanDefinition(definition) shouldEqual "<a href=\"/form/côté\">Côté</a> <a href=\"/form/vers\">vers</a> <a href=\"/form/lequel\">lequel</a> une <a href=\"/form/personne\">personne</a> ou une <a href=\"/form/chose\">chose</a> se dirige, est dirigée ou <a href=\"/form/tourner\">tournée</a> ; <a href=\"/form/voie\">voie</a> suivie pour <a href=\"/form/arriver\">arriver</a> à un point <a href=\"/form/donner\">donné</a>."
  }

}
