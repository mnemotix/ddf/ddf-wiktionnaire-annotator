/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UniteSpec extends AnyFlatSpec with Matchers{
  it should "parse unite" in {
    val definition = "{{sports|fr|de combat}} [[sport de combat|Sport de combat]] opposant deux adversaires équipés chacun d’une [[canne]] en bois de [[châtaignier]] longue de {{unité|95|cm}}."
   TextCleaners.cleanDefinition(definition) shouldEqual " <a href=\"/form/sport de combat\">Sport de combat</a> opposant deux adversaires équipés chacun d’une <a href=\"/form/canne\">canne</a> en bois de <a href=\"/form/châtaignier\">châtaignier</a> longue de 95 cm."
  }
}
