package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class SimpleParserSpec  extends AnyFlatSpec with Matchers {

  val simpleParser = new SimpleParser

  it should "remove <ref> </ref> and all char between the two tags" in {
    val universite = "{{vieilli|fr}} [[commune|Commune]], [[communauté]], [[corps]], [[compagnie]], [[collège]], [[corporation]], [[assemblée]]<ref>voir Dictionnaire Godefroy [http://www.micmap.org/dicfro/search/dictionnaire-godefroy/universit%C3%A9]</ref><ref>voir CNRTL [http://www.cnrtl.fr/etymologie/universit%C3%A9]</ref>. Ne subsiste plus que dans l'expression [[université d’été]] au sens d'assemblée."
    println(TemplateReplacer.replaceTemplates(universite))
    simpleParser.convertSimple(universite) shouldEqual "{{vieilli|fr}} [[commune|Commune]], [[communauté]], [[corps]], [[compagnie]], [[collège]], [[corporation]], [[assemblée]]. Ne subsiste plus que dans l'expression [[université d’été]] au sens d'assemblée."
  }

  it should "parse agglutination model" in {
    val auparavant = "{{agglutination|fr|m=1}} de la locution [[au]] [[par]] [[avant]]."
    println(TemplateReplacer.replaceTemplates(auparavant))

    simpleParser.convertSimple(auparavant) shouldEqual "agglutination de la locution [[au]] [[par]] [[avant]]."
  }
}

/*
Amélioration du parsing du wiki-ML notamment pour les définitions, sources, exemples, et étymologie.

L’idée, mais à valider, serait d’utiliser la librairie Parsoid: https://www.mediawiki.org/wiki/Parsoid#Converting_simple_wikitext. Il s’agirait donc de la faire tourner dans un docker https://www.mediawiki.org/wiki/Parsoid/Setup#Docker et d’utiliser l’API https://www.mediawiki.org/wiki/Parsoid/API au moment du parsing du texte du wiktionnaire.
 */