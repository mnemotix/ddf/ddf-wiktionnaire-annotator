package org.ddf.wiktionnaire.annotator.helpers

import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StringCleanerSpec extends AnyFlatSpec with Matchers {

  it should "replace ’ by '" in {
    val txt = "aujourd’hui"
    TextCleaners.replaceApostrophe(txt) shouldEqual "aujourd'hui"
  }

  it should "remove html tags" in {
    val txt = "<div><b>Geeks for Geeks</b></div>"
    TextCleaners.removeHTMLTags(txt) shouldEqual "Geeks for Geeks"
  }

  it should "test the template replacer" in {
    val tr = ": {{lien-ancre-étym|fr|nom|1}} {{siècle|XVI}} Le mot est passé du {{étyl|la|fr}} au [[français]] via le {{étyl|grc|fr}}, l’{{étyl|ar|fr}}, l’{{étyl|es|fr}} et le {{étyl|ca|fr}}. Les Romains le nommaient ''{{lien|praecoquum|la}}'', c’est-à-dire « (le fruit) [[précoce]] ». Les Grecs l’empruntèrent aux Romains sous la forme {{polytonique|[[πραικόκιον]]|praikókion}}. Les Arabes l’empruntèrent aux Grecs sous la forme [[ألبرقوق|أَلْبَرْقُوق]] ''ʾāl-barqūq'' (étant donné qu’il n’y a pas de son {{pron|p|fr}} en arabe, celui-ci a été remplacé par {{pron|b|fr}}, qui en est le son le plus proche.) Les Espagnols l’empruntèrent aux Arabes sous la forme ''[[albaricoque]]'' en agglutinant l’article défini au substantif et les Catalans sous la forme ''[[albercoc]]''{{R|MAKKI}}. La forme française vient de l'emprunt espagnol ou portugais."
    val tr2 = ": {{lien-ancre-étym|fr|adjectif}} {{siècle|XIX}} Formé par une [[métonymie]] de ''abricot'' en lien avec la couleur du fruit."
    val tr3 = ": {{lien-ancre-étym|fr|nom|2}} [[substantivation|Substantivation]] de l’adjectif."

     println(TemplateReplacer.replaceTemplates(tr2))
  }
}

