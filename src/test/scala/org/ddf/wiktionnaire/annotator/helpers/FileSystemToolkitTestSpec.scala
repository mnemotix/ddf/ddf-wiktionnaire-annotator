package org.ddf.wiktionnaire.annotator.helpers

import java.io.File
import java.net.URL

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.scalatest.BeforeAndAfterEach

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class FileSystemToolkitTestSpec extends WiktionnaireAnnotatorTestSpec with BeforeAndAfterEach {

  override def beforeAll(): Unit = {
    createTxtFiles
  }

  def createTxtFiles = {
    val classLoader = getClass().getClassLoader()
    val dir = new File(classLoader.getResource("data/fileSystemSpec/").getFile)
    val tmpFileScala = File.createTempFile("scala", ".txt", dir)
    val tmpFileJava = File.createTempFile("java", ".txt", dir)
    val tmpFilePython = File.createTempFile("python", ".py", dir)

    tmpFileScala.deleteOnExit()
    tmpFileJava.deleteOnExit()
    tmpFilePython.deleteOnExit()
  }

  it should "get a list of file base on the extension" in {
    val dir = new File(getClass.getClassLoader.getResource("data/fileSystemSpec").getPath)
    FileSystemToolKit.getListOfFiles(dir, List(".txt")).size shouldBe(2)
  }

  it should "unzip a bz2 file" in {
    val classLoader = getClass().getClassLoader()
    val file = new File(classLoader.getResource("data/fileSystemSpec/open_source.doc.bz2").getFile)
    val future = FileSystemToolKit.unbzipFile(file.getAbsolutePath)
    val rs = Await.result(future, Duration.Inf)
    rs shouldBe(0)
  }

  it should "zip a file" in {
    val dir = new File(getClass.getClassLoader.getResource("data/fileSystemSpec/").getPath)
    val tmpFilePython = File.createTempFile("empty", ".doc", dir)
    tmpFilePython.deleteOnExit()
    val future = FileSystemToolKit.zipFile(tmpFilePython.getAbsolutePath, "empty.doc.zip")
    val res = Await.result(future, Duration.Inf)
    res shouldBe(0)
  }

  it should "return the dir in a folder" in {
    val classLoader = getClass().getClassLoader()
    val path =  new File(classLoader.getResource("data/fileSystemSpec/rdf/").getPath)
    val res = FileSystemToolKit.listOfDir(path)
    res.foreach(r => println(r.getName))
    res.size shouldBe(4)
  }

  it should "return the last dir of rdf annotations" in {
    val classLoader = getClass().getClassLoader()
    val path = new File(classLoader.getResource("data/fileSystemSpec/rdf/").getPath)
    val res = FileSystemToolKit.listOfDir(path)
    val r =  FileSystemToolKit.lastAnnotationsDir(res)
    println(r)
    r shouldBe "01Nov2019"
  }
}