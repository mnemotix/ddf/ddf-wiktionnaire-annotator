package org.ddf.wiktionnaire.annotator.helpers

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireUriFactoryTestSpec extends AnyFlatSpec with Matchers {
  behavior of "Wiktionnaire URI Factory"

  it should "recreate the URI of a frWiktionnary word" in {
    val uri = WiktionnaireUriFactory.createWiktionnaireURL("affairé")
    uri shouldEqual "https://fr.wiktionary.org/wiki/affairé"
  }

  it should "create a URI for lexicog entry with a md5" in {
    val uri = WiktionnaireUriFactory.lexicogEntry("affairé")
    uri shouldEqual WiktionnaireUriFactory.lexicogEntry("affairé")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/entry/38054ceab06e55ac36aaba70f453a67d"
  }

  it should "create a URI for an Inflectable POS with a md5" in {
    val uri = WiktionnaireUriFactory.ddfInflectablePOS("affairé", "adjectif")
    uri shouldEqual WiktionnaireUriFactory.ddfInflectablePOS("affairé", "adjectif")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/inflectable/23e5e35a9e98c4dad70bc2595b6df61e"
  }

  it should "create a URI for an ddf Verb with a md5" in {
    val uri = WiktionnaireUriFactory.ddfVerb("bâfrer", "verbe")
    uri shouldEqual WiktionnaireUriFactory.ddfVerb("bâfrer", "verbe")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/verb/154d5c25ac4840d101c8b5a393f75b8"
  }

  it should "create a URI for an Invariable POS" in {
    val uri = WiktionnaireUriFactory.ddfInvariablePOS("carrément", "invariable")
    uri shouldEqual WiktionnaireUriFactory.ddfInvariablePOS("carrément", "invariable")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/invariable/ef316ce2b246498fbdf076e792c2980d"
  }

  it should "create a URI for an Multi Word Expression" in {
    val uri = WiktionnaireUriFactory.ontolexMultiWordExpression("en conséquence de", "préposition")
    uri shouldEqual WiktionnaireUriFactory.ontolexMultiWordExpression("en conséquence de", "préposition")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/multi-word/43b445f73342d6a8a7d46f289c2bbdd6"
  }

  it should "create a URI for a Lexical Sense" in {
    val uri = WiktionnaireUriFactory.ontolexLexicalSense("Qui a bien des affaires, qui est occupé.", "www.lexicalEntry.com")
    uri shouldEqual WiktionnaireUriFactory.ontolexLexicalSense("Qui a bien des affaires, qui est occupé.", "www.lexicalEntry.com")
  }

  it should "create a URI for an Ontolex Form" in {
    val uri = WiktionnaireUriFactory.ontolexForm("affairé")
    uri shouldEqual WiktionnaireUriFactory.ontolexForm("affairé")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/form/e85a5ee5f35701a2d25880a730ca8710"
  }

  it should "create a URI for an PostContent" in {
    val uri = WiktionnaireUriFactory.siocPost("un contenu de poste content")
    uri shouldEqual WiktionnaireUriFactory.siocPost("un contenu de poste content")
    uri shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/sioc-post/fa0e9992a765dad10be3ef583ade7390"
  }

  it should "create a URI for a Semantic Relation" in {
    val uri = WiktionnaireUriFactory.ddfSemanticRelation("http://data.dictionnairedesfrancophones.org/dict/wikt/entry/38054ceab06e55ac36aaba70f453a67d",
      WiktionnaireUriFactory.ontolexLexicalSense("http://data.dictionnairedesfrancophones.org/dict/wikt/entry/dea5c51fd030bedc21234b202dd2132f", "www.lexicalEntry.com"),
      "http://data.dictionnairedesfrancophones.org/authority/sense-relation/soundRelation"
    )
    uri shouldEqual WiktionnaireUriFactory.ddfSemanticRelation("http://data.dictionnairedesfrancophones.org/dict/wikt/entry/38054ceab06e55ac36aaba70f453a67d",
      WiktionnaireUriFactory.ontolexLexicalSense("http://data.dictionnairedesfrancophones.org/dict/wikt/entry/dea5c51fd030bedc21234b202dd2132f", "www.lexicalEntry.com"),
      "http://data.dictionnairedesfrancophones.org/authority/sense-relation/soundRelation"
    )
  }
}