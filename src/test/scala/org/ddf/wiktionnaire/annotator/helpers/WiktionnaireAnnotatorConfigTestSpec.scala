package org.ddf.wiktionnaire.annotator.helpers

import com.mnemotix.synaptix.rdf.client.RDFClientConfiguration
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


class WiktionnaireAnnotatorConfigTestSpec extends AnyFlatSpec with Matchers {

  it should "retreive the good spec" in {
    WiktionnaireAnnotatorConfig.ddfFilePath shouldBe("https://gitlab.com/mnemotix/dicofr/raw/master/ddf-repository")
    WiktionnaireAnnotatorConfig.ddfModels.size() shouldBe(10)
    WiktionnaireAnnotatorConfig.ddfVocabulary.size() shouldBe(22)
    WiktionnaireAnnotatorConfig.wiktionnairePrefixUri shouldBe("http://data.dictionnairedesfrancophones.org/dict/wikt/")

    WiktionnaireAnnotatorConfig.ontotextGraphDBhost shouldBe "http://localhost:7200"
    WiktionnaireAnnotatorConfig.ontotextGraphDBUser shouldBe "admin"
    WiktionnaireAnnotatorConfig.ontotextGraphDBPassword shouldBe "root"
    WiktionnaireAnnotatorConfig.ddfRepoName shouldBe "ddf"

  }
}