package org.ddf.wiktionnaire.annotator.services

import java.io.File
import akka.Done
import akka.stream.scaladsl.Sink
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig.wiktionnaireFileLocation
import org.ddf.wiktionnaire.annotator.model.Resource
import org.ddf.wiktionnaire.annotator.model.ddf.Entry

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireAnnotatorSpec extends WiktionnaireAnnotatorTestSpec with LazyLogging {
  val path = new File(s"${wiktionnaireFileLocation}")
  //val path = new File("src/test/resources/data/wiktionnaireSpec/frwiktionary-test-2.xml")

  it should "annotate only words" in  {
    val startGlobal = System.currentTimeMillis()
    val annotator = new WiktionnaireAnnotator(path.getAbsolutePath)
    annotator.init

    val future: (Future[Done], Future[Done]) = annotator.entry
    val result: Future[Seq[Done]] = Future.sequence(Seq(future._1, future._2))

    result.onComplete {
      case Success(value) => {
        println(s"first passage is finished with value = $value")
        annotator.shutdown
      }
      case Failure(e) => {
        println(s"error : $e")
        e.printStackTrace
      }
    }
    val finalResult = Await.result(result, Duration.Inf)
    println(finalResult)


    val endFirst = System.currentTimeMillis()

    println(s"1st process took ${endFirst - startGlobal} ms")
  }

  it should "annotate only relations" in   {
    val startGlobal = System.currentTimeMillis()
    val annotator2 = new WiktionnaireAnnotatorRelation(path.getAbsolutePath)
    annotator2.init
    val future2 = annotator2.relation
    future2.onComplete {
      case Success(value) => {
        println(s"second passage is finished with value = $value")
        annotator2.shutdown
      }
      case Failure(e) => e.printStackTrace
    }
    val finalResult2 = Await.result(future2, Duration.Inf)
    println(finalResult2)

    val endGlobal = System.currentTimeMillis()
    println(s"2nd process took ${endGlobal - startGlobal} ms")
  }

  it should "annotate only flexions" in  {
    val startGlobal = System.currentTimeMillis()
    val annotator = new WiktionnaireAnnotator(path.getAbsolutePath)
    annotator.init
    val future: (Future[Done], Future[Done]) = annotator.flexion
    val result: Future[Seq[Done]] = Future.sequence(Seq(future._1, future._2))

    result.onComplete {
      case Success(value) => {
        println(s"first passage is finished with value = $value")
        annotator.shutdown
      }
      case Failure(e) => {
        println(s"error : $e")
        e.printStackTrace
      }
    }
    val finalResult = Await.result(result, Duration.Inf)
    println(finalResult)

    val endFirst = System.currentTimeMillis()

    println(s"1st process took ${endFirst - startGlobal} ms")
  }
}