/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.ddf.wiktionnaire.annotator.model.VocabularyEntity
import org.ddf.wiktionnaire.annotator.services.WiktionnaireRdfReader
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.ExecutionContext

class VocsRecognizerSpec extends AnyFlatSpec with Matchers {
  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext =  ExecutionContext.global

  lazy val wiktionnaireVocabularyLoader = new WiktionnaireRdfReader
  wiktionnaireVocabularyLoader.init

  val vocs: Seq[VocabularyEntity] = wiktionnaireVocabularyLoader.vocabulariesBlocked
  lazy val vocsRecognizer = new VocsRecognizer(vocs)

  it should "recognize lexique " in {
    val definition = "{{lexique|politique|fr}} [[affaire|Affaire]] [[considérable]] [[soumettre|soumise]] à l’[[examen]] du [[gouvernement]], d’une [[assemblée]], du [[public]]."
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
   annotations.foreach(println(_))
    annotations.size shouldEqual(1)
  }

  it should "recognize another lexique" in {
    val definition = "{{lexique|linguistique|fr}} [[dictionnaire|Dictionnaire]] spécialisé servant à expliquer le [[vocabulaire]] d'un domaine [[technique]]."
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
   annotations.foreach(println(_))
  }

  it should "recognize another liste of lexique" in {
    val definition = "{{lexique|équitation|sport|fr}} equitation et sport"
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
   annotations.foreach(println(_))
    annotations.size shouldEqual(2)

  }

  it should "recognize a familier Vocabulaire Entity" in {
    val definition = "{{familier|fr}} Tête, visage d’une personne."
    println(definition.contains("{{familier|fr}}"))
    val definition2 = "{{familier|fr}} Par extension, accompagné d’un adjectif possessif : forme pronominale désignant la personne elle-même. Synonymes : [[moi]] ([[mézigue]], [[bibi]]), [[toi]], [[lui]], etc."
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
  }

  it should "recognize a lexique of form info lex" in {
    val definition = "{{info lex|cuisine}} {{ellipse|fr}} [[pomme de terre|Pomme de terre]]."
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
   annotations.foreach(println(_))
    annotations.size shouldEqual(1)
  }

  it should "recognize a figure lexique" in {
    val definition = "{{figuré|fr}} Prix de beauté, par allusion au mythologique jugement de [[Pâris]]."
    val annotations = vocsRecognizer.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", definition)
    annotations.distinct.foreach(println(_))
  }

  it should "recognize a canada" in {
    val definition = "{{Canada|fr}} {{term|Généralement péjoratif de la part des Amérindiens}} Personne [[amérindien]]ne [[acculturer|acculturée]] par les [[Blancs]]."
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
  }

  it should "recognize a gender based on label" in {
    val definition = "''Féminin pluriel de'' [[jaloux#fr|jaloux]]."
    val gender2 = vocsRecognizer.recogFromVocabWithLabel(definition.toLowerCase(), "http://data.dictionnairedesfrancophones.org/authority/gender")
    gender2.foreach(println(_))
  }

  it should "recognize a three values" in {
    val definition = """{{lexique|agriculture|élevage|pharmacie|fr}} [[substance|Substance]] [[phytosanitaire]] et [[pharmaceutique]], de formule brute {{fchim|C|19|H|30|O|5}}, [[synergiste]] d’[[insecticide]]s par blocage du [[métabolisme]] de [[détoxification]] des insectes."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    val pharma = vocsRecognizer.extractFromDomainVocabulary("http://data.dictionnairedesfrancophones.org/authority/domain", "{{pharmacie|fr}}")
    //pharma.foreach(println(_))
    annotations.foreach(println(_))
    annotations.size shouldEqual(3)
  }

  it should "recognize psychologie and sociologie" in {
    val definition = """{{lexique|psychologie|sociologie|fr}} Comportement [[chroniquement]] [[téméraire]] et [[irréfléchi]] pouvant avoir des [[conséquence]]s [[néfaste]]s. Ce terme est souvent associé aux [[adolescent]]s."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
    annotations.size shouldEqual(2)
  }

  it should "recognize probabilités and statistiques" in {
    val definition = """{{lexique|probabilités|statistiques|fr}} [[méthode|Méthode]] [[mathématique]] d’[[évaluation]] du [[sens]] de [[variation]] de [[deux]] [[variable]]s ([[moyenne]] de leurs [[produit]]s [[centré]]s sur leurs [[espérance]]s mathématiques<!-- E((X - E(X))(Y - E(Y))) -->, ou encore différence entre la moyenne de leurs produits et le produit de leurs moyennes<!-- E(XY) - E(X)E(Y) -->) et permettant de [[qualifier]] leur [[indépendance]]."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
    annotations.size shouldEqual(2)
  }

  it should "recognize linguistique sociologie" in {
    val definition = """{{lexique|linguistique|sociologie|fr}} [[correspondance|Correspondance]] entre l’[[appartenance]] à une [[certain]]e [[classe sociale]] et un certain [[parler]] [[inhérent]] à cette [[condition sociale]]."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
    annotations.size shouldEqual(2)

  }

  it should "recognize sociologie" in {
    val definition = """{{lexique|sociologie|fr}} Qualité de ce qui est au [[service]] de l'[[être humain]], adapté à la [[nature]] [[humain]]e, à [[dimension]] humaine, en particulier un [[outil]]."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
    annotations.size shouldEqual(1)
  }

  it should "recognize informatique telecommications" in {
    val definition = """{{lexique|informatique|télécommunications|fr}} Qualité de l'[[interface utilisateur]] d’un [[outil]] [[informatique]] dont l'[[usage]] est [[aisé]] et [[plaisant]]."""
    val annotations = vocsRecognizer.lexicalQualificationGenerate(definition)
    annotations.foreach(println(_))
    annotations.size shouldEqual(2)
  }


}