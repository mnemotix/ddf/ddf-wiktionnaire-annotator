package org.ddf.wiktionnaire.annotator.services

import java.io.File
import java.net.URI
import com.mnemotix.synaptix.cache.RocksDBStore
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.model.ddf.{CanonicalForm, Entry, LexicalEntry, Sense, UsageExample}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireDBCacheManagerSpec extends WiktionnaireAnnotatorTestSpec {
  val usageEx = new UsageExample("an exemple", Some("Citation"))
  val sense1 = new Sense(Some("Definition"), Some(Seq(usageEx)))
  val sense2 = new Sense(Some("Definition"), None)
  val canonicalForm = new CanonicalForm("entry1", None)
  val canonicalForm2 = new CanonicalForm("entry2", None)

  val entry1 = new Entry(URI.create("www.entry.test"), "entry1", "text",
    Some(Seq(new LexicalEntry(URI.create("www.entrylexical.test"), "tt","adj" ,Some(Seq(sense1)), None,Some(canonicalForm), Some("etym"), None, None, Some("many"),false),
    )))
  val toInsert = WiktionnaireDBCacheManager.transformerCacheEntry(entry1)

  val entry1Update = new Entry(URI.create("www.entry.test"), "entry1", "text",
    Some(Seq(new LexicalEntry(URI.create("www.entrylexical.test"), "tt2","adj" ,Some(Seq(sense1, sense2)),None, Some(canonicalForm), Some("etym"), None, None, Some("many"), false))))

  val newEntry = new Entry(URI.create("www.entrynew.test"), "entry2", "text",
    Some(Seq(new LexicalEntry(URI.create("www.entrylexical23.test"), "tt3","pronom" ,Some(Seq(sense2)),None, Some(canonicalForm2), Some("etymology de tout"), None, None, Some("many"), false))))
  val toInsert2 = WiktionnaireDBCacheManager.transformerCacheEntry(newEntry)


  val tmpFile = File.createTempFile("rocksdb", ".db")
  val tmpFileName = tmpFile.getAbsolutePath
  tmpFile.delete
  val store  = new RocksDBStore(tmpFileName)
  store.init

  it should "filter entry" in {
    val future = store.put(toInsert._1, toInsert._2)

    WiktionnaireDBCacheManager.filterEntryCache(entry1, store) shouldEqual true
    WiktionnaireDBCacheManager.filterEntryCache(entry1Update, store) shouldEqual true
    WiktionnaireDBCacheManager.filterEntryCache(newEntry, store) shouldEqual true
  }

  it should "filter entry after add" in {
    val future = store.put(toInsert._1, toInsert._2)
    val future2 = store.put(toInsert2._1, toInsert2._2)
    WiktionnaireDBCacheManager.filterEntryCache(entry1, store) shouldEqual true
    WiktionnaireDBCacheManager.filterEntryCache(entry1Update, store) shouldEqual true
    WiktionnaireDBCacheManager.filterEntryCache(newEntry, store) shouldEqual true
  }
}