package org.ddf.wiktionnaire.annotator.services.recognizer

import akka.stream.IOResult
import akka.stream.scaladsl.{Sink, Source}
import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig.wiktionnaireFileLocation
import org.ddf.wiktionnaire.annotator.model.Resource
import org.ddf.wiktionnaire.annotator.model.ddf.Entry
import org.ddf.wiktionnaire.annotator.services.{DDFModelsSetter, WiktionnaireWikidataExtractor}
import org.ddf.wiktionnaire.annotator.test.data.{WikitextConne, WikitextSolvions}
import org.ddf.wiktionnaire.annotator.test.data.liftyResource.{Absorbant, Blanc, Bouteille, Bureau, Bureaux, ConvergenceEvolutive, Cramoisi, Decaper, Drole, En, FrancophonieMaj, Gematrie, Latin, LettreDeChateau, Ouvrage, Peintre, Philosophe, Pre, Reunion, Roneoteuse, Somme, VaccinoSceptique, Vacuite, Violette, Yeux, noeud}

import java.io.File
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFAnnotationsGeneratorTestSpec extends WiktionnaireAnnotatorTestSpec {

  val ddfModelSetter = new DDFModelsSetter()

  it should "generate Annotations for an Absorbant" in {
    val absorbantEntry = ddfModelSetter.entry(Absorbant.resource)

    val m = absorbantEntry.toDDFModel()
    val m2 = absorbantEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate Annotations for an Blanc" in {
    val blancEntry = ddfModelSetter.entry(Blanc.resource)

    //blancEntry.lentries.toList.flatten.foreach(l => println(l.isInflection))

    val m = blancEntry.toDDFModel()
    val m2 = blancEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate Annotations for Somme" in {
    val sommeEntry = ddfModelSetter.entry(Somme.resource)
    val m = sommeEntry.toDDFModel()
    val m2 = sommeEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate Annotation for lettre de château" in {
    val lettreDeChateau = ddfModelSetter.entry(LettreDeChateau.resource)
    val m = lettreDeChateau.toDDFModel()
    val m2 = lettreDeChateau.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "create annotations for pré-" in {
    val pre = ddfModelSetter.entry(Pre.resource)
    val m = pre.toDDFModel()
    val m2 = pre.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate Annotations for an entry" in {
    val francophonieMajEntry: Entry = ddfModelSetter.entry(FrancophonieMaj.resource)
    val m = francophonieMajEntry.toDDFModel()
    val m2 = francophonieMajEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotations for bouteille" in {
    val bouteilleEntry = ddfModelSetter.entry(Bouteille.resource)
    val m = bouteilleEntry.toDDFModel()
    val m2 = bouteilleEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotations for decaper" in {
    val decaperEntry = ddfModelSetter.entry(Decaper.resource)
    println(decaperEntry.lentries.get(0).wordSectionContent.get)
    val m = decaperEntry.toDDFModel()
    val m2 = decaperEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotations for reunion" in {
    val reunionEntry = ddfModelSetter.entry(Reunion.resource)
    val m = reunionEntry.toDDFModel()
    val m2 = reunionEntry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate Annotations for an conne" in {
    val resource = new Resource("conne", "https://fr.wiktionary.org/wiki/", "entry", WikitextConne.conneRes)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate Annotations for an solvions" in {
    val resource = new Resource("solvions", "https://fr.wiktionary.org/wiki/", "entry", WikitextSolvions.solvionsRes)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate Annotations for an Yeux" in {
    val entry = ddfModelSetter.entry(Yeux.resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for noeud" in {
    val entry = ddfModelSetter.entry(noeud.resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for en" in {
    val entry = ddfModelSetter.entry(En.resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  // todo to close DDFS-176
  it should "generate annotation for cramoisi" in {
    val entry = ddfModelSetter.entry(Cramoisi.resource)
    val m = entry.toDDFModel()
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for peintre" in {
    val resource = new Resource("peintre", "https://fr.wiktionary.org/wiki/", "entry", Peintre.peintre)
    val entry = ddfModelSetter.entry(resource)

    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for philosophe" in {
    val resource = new Resource("philosophe", "https://fr.wiktionary.org/wiki/", "entry", Philosophe.Philosophe)
    val entry = ddfModelSetter.entry(resource)

    TemplateReplacer.replaceTemplates(TextCleaners.cleanDefinition(""": Du {{étyl|grc|fr|φιλόσοφος|philósophos|celui qui aime la [[sagesse]]}}.
                                                                     |
                                                                     |* {{siècle|XVII|lang=fr}} {{exemple
                                                                     ||Il faut dire, cette femme eſt ''Poëte'', eſt '''''Philoſophe''''', eſt ''Medecin'', eſt ''Auteur'', eſt ''Peintre'' ; & non ''Poëteſſe'', ''Philoſopheſſe'', ''Medecine'', ''Autrice'', ''Peintreſſe'', ''&c''.
                                                                     ||sens=Il faut dire, cette femme est ''poète'', est '''''philosophe''''', est ''médecin'', est ''auteur'', est ''peintre'' ; et non ''poétesse'', ''philosophesse'', ''médecine'', ''autrice'', ''peintresse'', ''etc''.
                                                                     ||source={{ouvrage|prénom=Nicolas|nom=Andry de Boisregard|titre=Reflexions ſur l’uſage préſent de la Langue Françoiſe ou Remarques Nouvelles & Critiques touchant la politeſſe du Langage|éditeur={{w|Laurent d’Houry}}|date=1692|passage=163-164|année première édition=1689|url={{Citation/Books.Google|l2tubIwXURgC|PA163|surligne=Philosophesse}}}}|tête=*|lang=fr}}""".stripMargin))

    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for Viollette" in {
    val resource = new Resource("Viollette", "https://fr.wiktionary.org/wiki/", "entry", Violette.violette)

    val entry = ddfModelSetter.entry(resource)


    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotation for drole" in {
    val resource = new Resource("drole", "https://fr.wiktionary.org/wiki/", "entry", Drole.drole)

    val entry = ddfModelSetter.entry(resource)

    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotation for gématrie" in {
    val resource = new Resource("gematrie", "https://fr.wiktionary.org/wiki/", "entry", Gematrie.gematrie)

    val entry = ddfModelSetter.entry(resource)

    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotation for vacuite" in {
    val resource = new Resource("vacuite", "https://fr.wiktionary.org/wiki/", "entry", Vacuite.vacuite)

    val entry = ddfModelSetter.entry(resource)

    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }

  it should "generate annotation for trilliard" in {
    val resource = new Resource("trilliard", "https://fr.wiktionary.org/wiki/", "entry", Vacuite.vacuite)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for ronéoteuse" in {
    val resource = new Resource("ronéoteuse", "https://fr.wiktionary.org/wiki/", "entry", Roneoteuse.roneoteuse)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for vaccinoSceptique" in {
    val resource = new Resource("vaccino-sceptique", "https://fr.wiktionary.org/wiki/", "entry", VaccinoSceptique.vaccinoSceptique)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for convergenceEvolutive" in {
    val resource = new Resource("convergenceEvolutive", "https://fr.wiktionary.org/wiki/", "entry", ConvergenceEvolutive.convergenceEvolutive)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for ouvrage" in {
    val resource = new Resource("ouvrage", "https://fr.wiktionary.org/wiki/", "entry", Ouvrage.ouvrage)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for bureaux" in {
    val resource = new Resource("bureaux", "https://fr.wiktionary.org/wiki/", "entry", Bureaux.bureaux)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for bureau" in {
    val resource = new Resource("bureau", "https://fr.wiktionary.org/wiki/", "entry", Bureau.bureau)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for vaccinosceptique" in {
    val resource = new Resource("vaccinosceptique", "https://fr.wiktionary.org/wiki/", "entry", VaccinoSceptique.vaccinoSceptique)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  it should "generate annotation for latin" in {
    val resource = new Resource("latin", "https://fr.wiktionary.org/wiki/", "entry", Latin.latin)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)
  }

  val r = """{{voir|Boug}}
            |
            |== {{langue|fr}} ==
            |=== {{S|étymologie}} ===
            |: {{lien-ancre-étym|fr|nom|1}} Des créoles antillais ou réunionnais, signifiant « gars ».
            |: {{lien-ancre-étym|fr|nom|2}} Du {{étyl|ja|fr|盆|bon}}.
            |
            |=== {{S|nom|fr|num=1}} ===
            |{{fr-rég|buɡ}}
            |'''boug''' {{pron|buɡ|fr}} {{m}}
            |# {{argot|fr}} [[gars|Gars]] ; [[mec]] ; [[type]].
            |#* ''Il n'avait pas dû être le seul à se lâcher sur le net. Il avait dû en avoir des '''''bougs''''' comme lui, énervés par le Député-Maire, à cracher leur fiel sur leur mur virtuel...'' {{source|Matheo Matahahrit, ''INSTITuTUEUR: Premier(s) jet(s)'', Publibook, 2014, p. 62}}
            |#* ''Je parle même pas des '''bougs''' camouflés en honnêtes travailleurs, et qui zieutent ceux qui ont de la maille en attendant le bon moment. C'est ça qui me vénèr le plus, gros.'' {{source|Geovani Martins, « ''P'tite virée'' », dans le recueil de nouvelles ''Le soleil sur ma tête'', traduit du portugais (Brésil) par Mathieu Dosse, NRF/Gallimard, 2019}}
            |#* ''— Bah, vas-y alors ! En vrai, t'auras qu'à faire le canard et l'appeler Bébé. J'en vois des '''bougs''' dans le bus, ils font genre les bonhommes et sur leur iPhone c'est marqué « Bébé » avec l'émoji cœur, tu connais ?'' {{source|Mathieu Palain, ''Sale Gosse'', Paris : Éditions L'Iconoclaste, 2019}}
            |#* ''On compte sur lui hein, le '''boug''' a fait ses preuves, et on souhaite donc aux Spurs d’apprécier les Playoffs à la télé, Top 3 des phrases les plus inattendues de l’année.'' {{source|Giovanni Marriette, ''[https://trashtalk.co/2020/08/15/zoom-sur-la-bulle-des-spurs-la-marche-etait-sans-doute-trop-haute-mais-gregg-popovich-nest-pas-venu-pour-rien/ Zoom sur la bulle des Spurs : la marche était sans doute trop haute, mais Gregg Popovich n’est pas venu pour rien]'', trashtalk.co, 15 août 2020}}
            |
            |=== {{S|nom|fr|num=2}} ===
            |'''boug''' {{pron|bɔ̃|fr}} {{m}} {{invar}}
            |# {{lexique|religion|fr}} {{désuet|fr}} ''Erreur de transcription de'' [[Bong]] (« festival japonais en été »)<ref>[//books.google.com/books/about/%E3%83%84%E3%83%B3%E3%83%99%E3%83%AB%E3%82%B0%E6%97%A5%E6%9C%AC%E7%B4%80%E8%A1%8C.html?id=icjO6BGb0E0C&pg=PA353 ''Voyages de C. P. Thunberg, au Japon, par le Cap de Bonne-Espérance, les isles de la Sonde, &c.''], 1796, traduction en japonais de Tamaki Yamada, 1928</ref>.
            |#* ''La fête des lanternes ou des lampes, nommée '''boug''', se célèbre à la fin d’août; elle dure trois jours ; mais la plus grande solennité a lieu le soir et la nuit du second jour : elle a été établie en l'honneur des morts qui viennent, disent-ils, visiter leurs parents et leurs amis ; […].'' {{source|1={{w|Carl Peter Thunberg}}, [//books.google.com/books?id=pyIQZV1QrOoC&pg=PA276&lpg=PA276&dq=boug&source=bl#v=onepage&q=boug ''Voyages de C. P. Thunberg, au Japon, par le Cap de Bonne-Espérance, les isles de la Sonde, &c.''], traduction de L. Langlès, 1796, p. 276}}
            |
            |=== {{S|prononciation}} ===
            |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-boug.wav}}
            |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-boug.wav}}
            |
            |=== {{S|références}} ===
            |{{Références}}
            |* {{R:Rivarol}}
            |* {{w|Napoléon Landais}}, ''Dictionnaire général et grammatical des dictionnaires français'', 11{{e}} édition, tome 1, Paris : chez Didier, 1852, p. 252
            |
            |== {{langue|gcf}} ==
            |=== {{S|étymologie}} ===
            |: Du {{étyl|fr|gcf|bougre}} utilisé dans l’argot de la marine et signifiant (« homme »).
            |
            |=== {{S|nom|gcf}} ===
            |'''boug''' {{pron||gcf}} {{m}}
            |# [[homme|Homme]], [[individu]].
            |
            |=== {{S|prononciation}} ===
            |* {{écouter|lang=gcf|France (La Ferté-sous-Jouarre)|audio=LL-Q3006280 (gcf)-Palé Kréyol-boug.wav}}
            |
            |=== {{S|références}} ===
            |* [http://www.freelang.com/enligne/creole_antillais.php?lg=fr ''Dictionnaire en ligne Créole antillais-Français, Français-Créole antillais'']
            |
            |== {{langue|gcr}} ==
            |=== {{S|étymologie}} ===
            |: Du {{étyl|fr|gcr|bougre}}, mot utilisé dans l'argot de la marine.
            |
            |=== {{S|nom|gcr}} ===
            |'''boug''' {{pron||gcr}}
            |# [[homme|Homme]], [[individu]].
            |
            |== {{langue|ht}} ==
            |=== {{S|étymologie}} ===
            |: Du {{étyl|fr|ht|bougre}}, mot utilisé dans l'argot de la marine.
            |
            |=== {{S|nom|ht}} ===
            |'''boug'''
            |#[[homme|Homme]], [[individu]].
            |
            |== {{langue|gcf-mq}} ==
            |=== {{S|étymologie}} ===
            |: Du {{étyl|fr|gcf-mq|bougre}}, mot utilisé dans l'argot de la marine.
            |
            |=== {{S|nom|gcf-mq}} ===
            |'''boug''' {{pron||gcf-mq}}
            |# [[homme|Homme]], [[individu]], [[type]].
            |
            |=== {{S|références}} ===
            |* {{R:PinalieBernabé|p=10}}
            |
            |== {{langue|rcf}} ==
            |=== {{S|étymologie}} ===
            |: Du {{étyl|fr|rcf|bougre}}, mot utilisé dans l'argot de la marine.
            |
            |=== {{S|nom|rcf}} ===
            |'''boug'''
            |#[[homme|Homme]], [[individu]].
            |
            |=== {{S|références}} ===
            |*[http://zorz.free.fr/Soubou/Zorz%20diksyoner.htm ''Lexique Créole Reunionnais-français, anglais, italien, allemand, espagnol.'']
            |*[https://web.archive.org/web/20130601222434/http://pedagogie2.ac-reunion.fr/clglasaline/disciplines/creole/lexiquecreole.htm Site Académie Réunion, clg la saline.]""".stripMargin

  it should "print a LiftyResource" in {
    val resource = new Resource("ouvrage", "https://fr.wiktionary.org/wiki/", "entry", r)
    val entry = ddfModelSetter.entry(resource)
    val m = entry.toDDFModel()
    //println(RDFSerializationUtil.toTriG(m))
    val m2 = entry.toInflexion()
    val merged = RDFSerializationUtil.merge(m, m2)
    val trig = RDFSerializationUtil.toTriG(merged)
    println(trig)

  }
}