package org.ddf.wiktionnaire.annotator.services

import java.io.{File, FileOutputStream}
import akka.{Done, NotUsed}
import akka.stream.{ClosedShape, IOResult}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.synaptix.core.utils.RandomNameGenerator
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig.{previousFileDir, wiktionnaireFileLocation}
import org.ddf.wiktionnaire.annotator.helpers.{WiktionnaireAnnotatorConfig, WiktionnaireUriFactory}
import org.ddf.wiktionnaire.annotator.model.Resource
import org.ddf.wiktionnaire.annotator.services.cache.MemoryCacheSystem
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.{IRI, Model}
import org.eclipse.rdf4j.rio.{RDFFormat, RDFHandlerException, Rio, UnsupportedRDFormatException}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireWikidataExtractorSpec extends WiktionnaireAnnotatorTestSpec {
  val classLoader = getClass().getClassLoader()
  val path =  new File(classLoader.getResource("data/wiktionnaireSpec/frwiktionary-test.xml").getFile)
  val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)

  it should "extract a seq of LiftyResource" in {
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    val sinkCount = Sink.fold[Int, Resource](0)((acc, _) => acc + 1)
    val number = Await.result(resources.runWith(sinkCount), Duration.Inf)
    number shouldBe 13
  }

  implicit val htmlCrawler = new WiktionnaireHTMLCrawler()
  lazy val topHeadSink = Sink.ignore
  val ddfModelSetter = new DDFModelsSetter()
  lazy val recognizer = new WiktionnaireRecognizer()
  lazy val wiktionnaireFileOutput = new WiktionnaireFileOutput()

  def nameSpace(builder: ModelBuilder) = {
    builder.setNamespace("wik", "http://data.dictionnairedesfrancophones.org/resource/wiktionnaire")
      .setNamespace("owl", "http://www.w3.org/2002/07/owl#")
      .setNamespace("ddf","http://data.dictionnairedesfrancophones.org/ontology/ddf#")
      .setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#")
      .setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
      .setNamespace("lexinfo", "http://www.lexinfo.net/ontology/2.0/lexinfo#")
      .setNamespace("core", "http://www.w3.org/2004/02/skos/core#")
      .setNamespace("lemon", "http://lemon-model.net/lemon#")
      .setNamespace("dct", "http://purl.org/dc/terms/")
      .setNamespace("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#")
      .setNamespace("xml", "http://www.w3.org/XML/1998/namespace")
      .setNamespace("lexicog", "http://www.w3.org/ns/lemon/lexicog#")
      .setNamespace("ontolex", "http://www.w3.org/ns/lemon/ontolex#")
      .setNamespace("prov", "http://www.w3.org/ns/prov-o-20130430#")
      .setNamespace("sioc", "http://rdfs.org/sioc/ns#").build()
  }

  it should "print a LiftyResource" in {
    val path = new File(s"${wiktionnaireFileLocation}")
    val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    val fut: Source[Resource, Future[IOResult]] = resources.filter(p => p.id == "boug")
    val pr = fut.runForeach(i => println(i.content))
    Await.result(pr, Duration.Inf)
  }

  it should "calculer le nombre de LiftyResource" in {
    val startTime = System.currentTimeMillis()
    val path = new File(s"${wiktionnaireFileLocation}")
    val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    val fut = Await.result(resources.runWith(Sink.seq), Duration.Inf)
    println(s"le nombre de resource est ${fut.size}")
    logger.info(">> Time took to extract the liftyResource => {} s", (System.currentTimeMillis() - startTime)/1000)
  }

  it should "print a LiftyResource with a d%" in {
    val path = new File(s"${wiktionnaireFileLocation}")
    val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    val ids = resources.filter(p => p.id contains "/").map(_.id)
    val allIds = Await.result(ids.runWith(Sink.seq), Duration.Inf)
    val s = ids.runForeach(i => println(i))

    Await.result(s, Duration.Inf)

    println(allIds.size)

    //val fut: Source[LiftyResource, Future[IOResult]] = resources.filter(p => p.id == "champ")
    //val pr = fut.runForeach(i => println(i.content))
    //Await.result(pr, Duration.Inf)
  }

  /*
Wiktionnaire:Bêtisier
Wiktionnaire:Wikidémie/septembre 2017
Wiktionnaire:Proposer un mot/juin 2020
Wiktionnaire:Proposer un mot/septembre 2016
Wiktionnaire:Proposer un mot/novembre 2016
   */

  it should "print Wiktionnaire:Wikidémie/septembre 2017 resource" in {
    val path = new File(s"${wiktionnaireFileLocation}")
    val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    // val ids = resources.filter(p => p.id contains "%").map(_.id)
    // val s = ids.runForeach(i => println(i))
    // Await.result(s, Duration.Inf)
    val fut: Source[Resource, Future[IOResult]] = resources.filter(p => p.id == "Wiktionnaire:Proposer un mot/juin 2020")
    val pr = fut.runForeach(i => println(i.content))
    Await.result(pr, Duration.Inf)
  }

  it should "print a resource" in {
    val toFilter = "bureau"
    val path = new File(s"${wiktionnaireFileLocation}")
    val extractor = new WiktionnaireWikidataExtractor(path.getAbsolutePath)
    val resources: Source[Resource, Future[IOResult]] = extractor.extract
    // val ids = resources.filter(p => p.id contains "%").map(_.id)
    // val s = ids.runForeach(i => println(i))
    // Await.result(s, Duration.Inf)
    val fut: Source[Resource, Future[IOResult]] = resources.filter(p => p.id == s"$toFilter")
    val pr = fut.runForeach(i => println(i.content))
    Await.result(pr, Duration.Inf)
  }
}
