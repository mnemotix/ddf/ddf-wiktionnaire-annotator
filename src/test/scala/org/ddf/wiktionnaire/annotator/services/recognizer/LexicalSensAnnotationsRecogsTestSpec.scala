package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.model.ddf.{Sense, UsageExample}
import org.ddf.wiktionnaire.annotator.services.DDFModelsSetter
import org.ddf.wiktionnaire.annotator.test.data.liftyResource.{Pareil, Scenario}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LexicalSensAnnotationsRecogsTestSpec extends WiktionnaireAnnotatorTestSpec {

  it should "generate an uri for a lexical sens" in {
    val definition = "voici un defintion"
    val usageExamples = None
    val usageExamples2 = Some(Seq(new UsageExample("exemple", None)))
    val usageExample3 = Some(Seq(new UsageExample("exemple", None), new UsageExample("exemple", None)))

  }

  it should "generate an uri for an exemple" in {
    val exemple = "exemple"
    val biblio = Some("biblio")
    val biblio2 = None

    //uri1.toString == uri2.toString shouldBe false
  }

  it should "generate annotation for lexical sens" in {
    val uriLexicogEntry = new URI("www.lexicogEntry.com")
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
  }

  it should "generate annotation for definition" in {
    val uriLexicalSens = new URI("www.lexicalSens.com")
    val definition = "definition"
  }

  it should "generate annotation exemple" in {
    val uriLexicalSens = new URI("www.lexicalSens.com")
    val uriExemple = new URI("www.exemple.com")
    val exemple = "exemple"
  }

  it should "generate bibliographical citation" in {
    val uriExemple = new URI("www.exemple.com")
    val source = "source"
  }

  it should "recognize vocs concept" in {
    val uriLexicalSens = new URI("www.lexixalSens.com")
    val definition = "{{par ext}} {{agri|fr}} La [[plante]] [[elle-même]] dont la pomme de terre constitue le tubercule."
  }

  it should "recognize relation" in {
    val senses = Seq(new Sense(
      Some("{{cuisine|nocat=1}} [[légume|Légume]] [[féculent]] très commun, [[tubercule]] de forme [[plus ou moins]] [[sphérique]] et [[cabossé]]e, souvent un peu [[allongée]], de la famille des [[solanacées]]."),
      Some(Seq(new UsageExample("''Et quel festin ! De l’oie rôtie, des '''pommes de terre''' bouillies, du pain, du fromage !''", Some("{{w|Émile Zola}}, ''La débâcle''}}")),
        new UsageExample("''Faites cuire des '''pommes de terre''' à l’eau salée, ôtez la pelure, coupez les en tranches.''", Some("Marcel Butler, ''La bonne cuisine pour tous, ou L’art de bien vivre à bon marché'', 1885}}"))))
    ),
      new Sense(Some("{{par ext}} {{agri|fr}} La [[plante]] [[elle-même]] dont la pomme de terre constitue le tubercule."),
        Some(Seq(new UsageExample("''Nous sommes redevables à la lutte contre le Doryphore par les insecticides d’avoir pu maintenir la production de la '''pomme de terre''' dans une très grande partie de notre territoire, en dépit de l'invasion doryphorique.''", Some("René Guy Busnel, ''Études physiologiques sur le ''Leptinotarsa decemlineata'' Say, Paris : libr. E. Le François, 1939, page 1}}")))))
    ,new Sense(Some("# {{Canada|fr}} [[airelle|Airelle]] rouge, fruit proche de la [[myrtille]]<ref>[https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/dictionnaires/terminologie_fruits/airelle.html Lexique des fruits rouges de l’OQLF]</ref>."),
        None))

    val resourceTitle = "pomme de terre"
    val lexicogEntryURI = new URI("www.uriLexicogEntry.fr")
    //LexicalSensAnnotationsRecogs.sensRelationGenerate(senses, resourceTitle, lexicogEntryURI, "www.lexicalEntry.com").size shouldEqual(4)
  }

  it should "recognize relation in pareil" in {
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(Pareil.resource)
    /*
    val res = entry.lentries.map { lentris =>
      lentris.filter { en =>
        en.sense.isDefined
      }.map { en =>
        LexicalSensAnnotationsRecogs.sensRelationGenerate(en.sense.get, entry.form, entry.uri, en.uri.toString)
      }
    }.toSeq.flatten.flatten
    res.foreach(println(_))

     */
  }

  it should "recognize relation in scenario" in {
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(Scenario.resource)
   /*
    val res = entry.lentries.map { lentris =>
      lentris.filter { en =>
        en.sense.isDefined
      }.map { en =>
        LexicalSensAnnotationsRecogs.sensRelationGenerate(en.sense.get, entry.form, entry.uri, en.uri.toString)
      }
    }.toSeq.flatten.flatten
    res.foreach(println(_))

    */
  }
}