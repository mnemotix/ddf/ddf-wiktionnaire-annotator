package org.ddf.wiktionnaire.annotator.services

import java.io.File

import akka.http.scaladsl.model.Uri
import org.apache.commons.io.FileUtils
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireDownloaderSpec extends WiktionnaireAnnotatorTestSpec {

  val wiktionnaireDownloader = new WiktionnaireDownloader()

  override def afterAll(): Unit = {
    FileUtils.deleteDirectory(new File(WiktionnaireAnnotatorConfig.latestFileDir))
    FileUtils.deleteDirectory(new File(WiktionnaireAnnotatorConfig.previousFileDir))
  }

  it should "init the wiktionnaire Downloader" in {
    wiktionnaireDownloader.init
    new File(s"${WiktionnaireAnnotatorConfig.latestFileDir}").isDirectory shouldBe(true)
  }

  it should "downloader the wiktionnaçre file" in {
    val fileLink = "https://gitlab.com/mnemotix/ddf/ddf-data/raw/master/wiktionnaire/wiktionnaire-test-data/frwiktionary-test.xml.bz2"
    val res = wiktionnaireDownloader.download(Uri(fileLink))
    Await.result(res, Duration.Inf)
    new File(s"${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionnaireFileName}").exists() shouldBe(true)
  }

  it should "unzip the file" in {
    val future = wiktionnaireDownloader.unzip
    val res = Await.result(future._1, Duration.Inf)
    res shouldEqual(0)
  }
}
