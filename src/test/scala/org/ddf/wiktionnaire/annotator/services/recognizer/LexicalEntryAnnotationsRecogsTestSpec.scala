package org.ddf.wiktionnaire.annotator.services.recognizer


import java.net.URI
import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.model.{Resource, VocabularyEntity}
import org.ddf.wiktionnaire.annotator.services.DDFModelsSetter
import org.ddf.wiktionnaire.annotator.test.data.{WikitextBulkData, WikitextChemineau}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LexicalEntryAnnotationsRecogsTestSpec extends WiktionnaireAnnotatorTestSpec {

  it should "create a lexical entry annotations" in {
    val uriLexicog = new URI("www.lexicogEntry.com")
    val uriLexicalEntry = new URI("www.lexicaEntry.com")
    //LexicalEntryAnnotationsRecogs.lexicalEntryAnnotations(uriLexicog, uriLexicalEntry).obj.stringValue() shouldEqual uriLexicalEntry.toString
  }

  it should "create annotations for form" in {
    val uriLexicalEntry = new URI("www.lexicaEntry.com")
   // LexicalEntryAnnotationsRecogs.formAnnotations(uriLexicalEntry, "Test Unitaire").size shouldBe 4
  }



  it should "extract class of form" in {
    val word1 = "betterave"
    val word2 = "sauve-qui-peut"
    val word3 = "point cardinal"

   // LexicalEntryAnnotationsRecogs.extractClassForm(word1).shouldEqual("http://www.w3.org/ns/lemon/ontolex#Word")
    //LexicalEntryAnnotationsRecogs.extractClassForm(word2).shouldEqual("http://www.w3.org/ns/lemon/ontolex#MultiWordExpression")
    //LexicalEntryAnnotationsRecogs.extractClassForm(word3).shouldEqual("http://www.w3.org/ns/lemon/ontolex#MultiWordExpression")
  }

  it should "create annotations for other form annotation" in {
    val uriLexicalEntry = new URI("www.lexicaEntry.com")
    val words = Seq("other", "word", "annotations")
   // LexicalEntryAnnotationsRecogs.otherFormAnnotations(words, uriLexicalEntry).foreach(println(_))
   // LexicalEntryAnnotationsRecogs.otherFormAnnotations(words, uriLexicalEntry).size shouldEqual 9
  }

  it should "create annotations for other form for the word Chemineau" in {
    val wikitextChamp = new Resource("chemineau", "nameSpace", "content", WikitextChemineau.chemineau)
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(wikitextChamp)
    val words = entry.lentries.toSeq.flatten.map(lentry => lentry.otherForms.toSeq.flatten)
    //val annotations = LexicalEntryAnnotationsRecogs.otherFormAnnotations(words.flatten, new URI("www.lexicaEntry.com"))
    //println(annotations.size)
    //annotations.foreach(println(_))
    //annotations.size shouldEqual 3
  }

  it should "create annotations for pronounciation" in {
    val uriLexicalEntry = new URI("www.lexicaEntry.com")
    val pronounciation = Seq("kɔʁ.bo")
    //LexicalEntryAnnotationsRecogs.pronounciationAnnotations("corbeau", pronounciation).size shouldEqual 1
  }

  it should "create annotations for etymology" in {
    val uriLexicalEntry = new URI("www.lexicaEntry.com")
    val etymologyTxt = "Etymology text"
    //LexicalEntryAnnotationsRecogs.etymologyAnnotations(uriLexicalEntry, Some(etymologyTxt)).size shouldEqual 4
  }

  it should "create annotations for gender or number or transivity" in {
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
    val wikitext = """{{fr-inv|fʁɑ̃.kɔ.fɔ.ni}}
                     |'''Francophonie''' {{pron|fʁɑ̃.kɔ.fɔ.ni|fr}} {{f}}, {{au singulier uniquement|fr}}
                     |# [[organisation|Organisation]] internationale qui rassemble les [[pays]] parlant ou promouvant la langue française.
                     |#* ''Macron s’engage par rapport à la '''Francophonie''' : il veut faire du français la troisième langue du monde. Il crée un centre de la francophonie dans un ancien château de l’Aisne.'' {{source|Dominique {{pc|Lebel}}, ''L’entre-deux-mondes'', Montréal, Boréal, 2019, p. 326}}
                     |# Ensemble des [[peuple]]s, pays et territoires parlant français.""".stripMargin
   // val res = LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(uriLexicalEntry, wikitext)
    //res.foreach(println(_))
    //LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(uriLexicalEntry, wikitext).size shouldEqual 2
  }

  it should "create annotations for unstableGender" in {
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
    val wikitext = """{{fr-rég|e.flyv}}
                     |'''effluve''' {{pron|e.flyv|fr}} {{mf ?|fr}}
                     |# {{lexique|médecine|fr}} {{vieilli|fr}} Substances organiques altérées, tenues en suspension dans l’air, principalement aux endroits marécageux, et donnant particulièrement lieu à des fièvres intermittentes, [[rémittente]]s et continues.
                     |# [[émanation|Émanation]] qui se [[dégager|dégage]] d’un [[corps]] [[quelconque]].
                     |#* ''Il ne reste qu’un tableau de Lebrun représentant la Pentecôte d’une façon qui étonnerait l’auteur des ''Actes des apôtres''. La Vierge y est au centre et reçoit pour son compte tout l’'''effluve''' du Saint-Esprit, qui, d’elle, se répand sur les apôtres.'' {{source|{{w|Ernest Renan}}, ''Souvenirs d’enfance et de jeunesse'', 1883, collection Folio, page 153.}}
                     |#* ''On entendait partout des chants d’oiseaux. Alors ma compagne se mit à courir en [[gambader|gambadant]], enivrée d’air et d’'''effluves''' champêtres. Et moi je courais derrière en sautant comme elle. Est-on bête, monsieur, par moments !'' {{source|{{nom w pc|Guy de Maupassant}}, ''Au printemps'', dans ''La maison Tellier'', 1891, collection Le Livre de Poche, page 213.}}
                     |#* ''Elle pourrait demeurer ici, l’assaillir d’invites, de chatteries, toute la nuit provoquer son désir, répandre ses '''effluves'''.'' {{source|{{Citation/Jean Rogissart/Passantes d’Octobre/1958|}}}}
                     |#* ''Lors des manifestations au foyer rural, il traînait toujours dans les environs de la salle, les '''effluves''' festives l'ayant averti qu'il se tramait quelque chose d'intéressant et, aussitôt les gens ou les invités partis, il entrait prêter ses mains noires pour aider à débarrasser.'' {{source|José Herbert, ''La vie privée de Joint de Culasse'', dans ''L'instituteur impertinent: récits'', Atria Témoignages/Primento, 2014}}
                     |#*''De chaque côté de la place, il y avait des tilleuls, qui pleuraient au printemps en nous arrosant de leurs '''effluves''' apaisants.''{{source| José Herbert, ''L’instituteur impertinent: Récit de vie'', 2016}}""".stripMargin
    //val res = LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(uriLexicalEntry, wikitext)
    //res.foreach(println(_))
    //LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(uriLexicalEntry, wikitext).size shouldEqual 2
  }

  it should "create annotations for gender and number or transitivity for feu" in {
    val uri = new URI("www.feu.com")
    val wikitext = WikitextBulkData.feu
    //val res = LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(uri, wikitext)
    //res.foreach(println(_))
  }

  it should "create annotations for posAnnotations" in {
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
    val sectionTitle = "=== {{S|nom|fr}} ==="
    val sectionTitle2 = "==== {{S|vocabulaire}} ===="
/*
    LexicalEntryAnnotationsRecogs.posAnnotations(uriLexicalEntry, sectionTitle).isDefined shouldBe true
    LexicalEntryAnnotationsRecogs.posAnnotations(uriLexicalEntry, sectionTitle).get.size shouldEqual 1

    LexicalEntryAnnotationsRecogs.posAnnotations(uriLexicalEntry, sectionTitle2).isDefined shouldBe false

 */
  }

  it should "create annotations for sens relation" in {
    val uriLexicogEntry = new URI("www.lexicogEntry.com")
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
    val voc = new VocabularyEntity("voc", "notation", "concept", "property", "conceptLabel")
    val uriRelationsWith = new URI("www.uriRelations.com")

    //LexicalEntryAnnotationsRecogs.sensRelationAnnotations(uriLexicogEntry, uriLexicalEntry, voc, uriRelationsWith).size shouldEqual 4
  }

  it should "create annotations for word type" in {
    val uriLexicalEntry = new URI("www.lexicalEntry.com")
    val sectionTitle = "=== {{S|nom|fr}} ==="
    /*
    val posTrue = LexicalEntryAnnotationsRecogs.posAnnotations(uriLexicalEntry, sectionTitle)
    val res = LexicalEntryAnnotationsRecogs.wordTypeAnnotations(uriLexicalEntry, posTrue.get)
    res.foreach(f => println(s"${f.resource.toString} -  ${f.property}  - ${f.obj.stringValue()}"))
    res.size shouldEqual 1

     */
  }
}