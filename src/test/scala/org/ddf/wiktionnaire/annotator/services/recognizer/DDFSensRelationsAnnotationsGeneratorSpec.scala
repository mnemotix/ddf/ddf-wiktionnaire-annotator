/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services.recognizer

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.model.Resource
import org.ddf.wiktionnaire.annotator.services.DDFModelsSetter
import org.ddf.wiktionnaire.annotator.services.cache.MemoryCacheSystem
import org.ddf.wiktionnaire.annotator.test.data.WikitextChemineau

class DDFSensRelationsAnnotationsGeneratorSpec extends WiktionnaireAnnotatorTestSpec {

  it should "annotate sens relation" in {
    lazy val lexicogEntryCache = MemoryCacheSystem.lexicogEntryTmpDB
    lexicogEntryCache.init
    val wikitextChamp = new Resource("chemineau", "nameSpace", "content", WikitextChemineau.chemineau)
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(wikitextChamp)
   // val annotations = DDFSensRelationsAnnotationsGenerator.generator(entry, lexicogEntryCache)
    //val annotationsSeq = annotations.toSeq.flatten.flatten.toSeq
    //println(annotationsSeq.size)
    //annotationsSeq.foreach(println(_))
  }

}
