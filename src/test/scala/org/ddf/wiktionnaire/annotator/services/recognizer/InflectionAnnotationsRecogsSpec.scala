/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec

class InflectionAnnotationsRecogsSpec extends WiktionnaireAnnotatorTestSpec {
  it should "create gender Number Transitivity Annotations" in {
    val definition = "''Féminin pluriel de'' [[jaloux#fr|jaloux]]."
    val wikitext = """'''jalouses''' {{pron|ʒa.luz|fr}}
                     |# ''Féminin pluriel de'' [[jaloux#fr|jaloux]].""".stripMargin
    val lexicalEntryType = "=== {{S|adjectif|fr|flexion}} ==="
    //val annotations = InflectionAnnotationsRecogs.genderNumberTransitivityAnnotations(new URI("wwww.jalouses.fr"), definition, wikitext,lexicalEntryType)
    //annotations.foreach(println(_))
  }

  it should "create verbal annotations" in {
    val definition = "''Première personne du pluriel de l’imparfait de l’indicatif de'' [[solver]]."
    val lexicalEntryType = "=== {{S|verbe|fr|flexion|num=2}} ==="
   // val annotations = InflectionAnnotationsRecogs.verbalInflection(new URI("wwww.solvions.fr"), definition,lexicalEntryType)
    //annotations.foreach(println(_))
  }
}
