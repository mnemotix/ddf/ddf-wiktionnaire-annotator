package org.ddf.wiktionnaire.annotator.services

import org.ddf.wiktionnaire.annotator.WiktionnaireAnnotatorTestSpec
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.SplitProcessing
import org.ddf.wiktionnaire.annotator.model.Resource
import org.ddf.wiktionnaire.annotator.test.data.liftyResource.{Cadeau, FrancophonieMaj, Pomme, Yeux}
import org.ddf.wiktionnaire.annotator.test.data.{WikitextAutrice, WikitextBulkData, WikitextChamp, WikitextChemineau, WikitextCorbeau, WikitextSomme}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFModelSetterSpec extends WiktionnaireAnnotatorTestSpec {

  val ddfModelSetter = new DDFModelsSetter()


  it should "convert a lifty Resource in a Entry" in {
    val francophonieMajEntry = ddfModelSetter.entry(FrancophonieMaj.resource)

    //println("result usage Examples : " + francophonieMajEntry.lentries.get(0).sense.get(0).usageExamples)

    francophonieMajEntry.uri.toString shouldEqual "http://data.dictionnairedesfrancophones.org/dict/wikt/entry/e01b4f69506578d7ba1cd2a481774f03"
    francophonieMajEntry.lentries.isDefined shouldBe true
    francophonieMajEntry.lentries.get.size shouldBe 1
    francophonieMajEntry.lentries.get(0).lexicalEntryType shouldEqual "=== {{S|nom|fr}} ==="
    francophonieMajEntry.lentries.get(0).etymology shouldEqual Some(":{{ébauche-étym|fr}}")
    francophonieMajEntry.lentries.get(0).canonicalForm.get.phoneticRep.isDefined shouldBe true
    francophonieMajEntry.lentries.get(0).canonicalForm.get.phoneticRep.get.size shouldEqual 1
    francophonieMajEntry.lentries.get(0).canonicalForm.get.phoneticRep.get(0) shouldEqual "fʁɑ̃.kɔ.fɔ.ni"
    francophonieMajEntry.lentries.get(0).canonicalForm.get.label shouldEqual "Francophonie"
    francophonieMajEntry.lentries.get(0).sense.isDefined shouldBe true
    francophonieMajEntry.lentries.get(0).sense.get.size shouldBe 2
    francophonieMajEntry.lentries.get(0).sense.get(1).definition.isDefined shouldBe true
    francophonieMajEntry.lentries.get(0).sense.get(1).definition.get shouldEqual "Ensemble des [[peuple]]s, pays et territoires parlant français."
    francophonieMajEntry.lentries.get(0).sense.get(1).usageExamples.isDefined shouldBe false
    francophonieMajEntry.lentries.get(0).wordSectionContent shouldEqual Some("""{{fr-inv|fʁɑ̃.kɔ.fɔ.ni}}
                                                              |'''Francophonie''' {{pron|fʁɑ̃.kɔ.fɔ.ni|fr}} {{f}}, {{au singulier uniquement|fr}}
                                                              |# [[organisation|Organisation]] internationale qui rassemble les [[pays]] parlant ou promouvant la langue française.
                                                              |#* ''Macron s’engage par rapport à la '''Francophonie''' : il veut faire du français la troisième langue du monde. Il crée un centre de la francophonie dans un ancien château de l’Aisne.'' {{source|Dominique {{pc|Lebel}}, ''L’entre-deux-mondes'', Montréal, Boréal, 2019, p. 326}}
                                                              |# Ensemble des [[peuple]]s, pays et territoires parlant français.""".stripMargin)
  }

  it should "convert a lifty resource in a Entry - wikitextChamp" in {
    val wikitextChamp = new Resource("champ", "nameSpace", "content", WikitextChamp.champ)
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(wikitextChamp)
    val entries = entry.lentries.toSeq.flatten.toSeq
    //println(entries.size)
    entries.map { entry =>
      val forms = entry.otherForms.toSeq.flatten
      println(forms.size)
      println(forms.mkString(" "))
    }
  }

  it should "convert a lifty resource in a Entry - wikiChemineau" in {
    val wikitextChamp = new Resource("chemineau", "nameSpace", "content", WikitextChemineau.chemineau)
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(wikitextChamp)
    val entries = entry.lentries.toSeq.flatten.toSeq
    //println(entries.size)
    entries.map { entry =>
      val forms = entry.otherForms.toSeq.flatten
      println(forms.size)
      println("other form : " + forms.mkString(" "))
    }
  }

  it should "convert a lifty resource in a Entry - wikiTextAutrice" in {
    val wikitextAutrice = new Resource("autrice", "nameSpace", "content", WikitextAutrice.autrice)
    val ddfModelsSetter = new DDFModelsSetter()
    val entry = ddfModelsSetter.entry(wikitextAutrice)
    val entries = entry.lentries.toSeq.flatten.toSeq
    //println(entries.size)
  }

  it should "get sens object" in {
    val francophonie = WikitextBulkData.francophonieMAJ
    val sensObj = ddfModelSetter.sense(Some(francophonie))
    sensObj.isDefined shouldBe true
    sensObj.get.size shouldBe 2
  }

  it should "get cannonicalForm object" in {
    val francophonie = WikitextBulkData.francophonieMAJ

    val cannonicalFormObj = ddfModelSetter.cannonicalForm(Some(francophonie), "Francophonie")
    cannonicalFormObj.get.label shouldBe "Francophonie"
    cannonicalFormObj.get.phoneticRep.isDefined shouldBe true
    cannonicalFormObj.get.phoneticRep.get.size shouldBe 1
    cannonicalFormObj.get.phoneticRep.get.last shouldBe "fʁɑ̃.kɔ.fɔ.ni"
  }

  it should "get usage exemple for a specific {{exemple}} model" in {
    val exemple = """#* {{exemple|Cette situation familiale, prise en compte dans le processus de personnalisation des peines au même titre que la personnalité de l’auteur ou de l’'''autrice''', la gravité et les circonstances de l’infraction, fait que, dans bien des cas, la criminelle n’est pas seule à la barre|source={{périodique|auteur=Lucile Quillet|titre=Les femmes, ces criminelles (presque) comme les hommes|journal={{w|Slate (magazine)|Slate}}|date=4 mars 2019|url=http://www.slate.fr/story/174153/societe/les-femmes-et-le-crime-episode-1-criminelles-comme-hommes}}|lang=fr}}
                    |#* {{exemple|L’'''autrice''' présumée de l’attaque a été arrêtée à Seattle.|source={{périodique|auteur=Harold Grand|titre=Les données personnelles de 100 millions d’Américains exposées dans un piratage géant|journal={{w|Le Figaro}}|date=30 juillet 2019|url=https://www.lefigaro.fr/secteur/high-tech/les-donnees-personnelles-de-100-millions-d-americains-exposees-dans-un-piratage-geant-20190730}}|lang=fr}}""".stripMargin

    val autrice = ddfModelSetter.usageExample(exemple)

    autrice.get.foreach(ex => println(ex.bibliographicalCitation.get))

    autrice.isDefined shouldBe true
    autrice.get.size shouldBe 2
    autrice.get.last.example shouldBe "#* {{exemple|L’'''autrice''' présumée de l’attaque a été arrêtée à Seattle."
  }

  it should " get usage exemple for corbeau" in {
    val citationExemple = """#* ''[…] un '''corbeau''' croassait à la dernière branche desséchée d’un chêne, et les archers montrèrent en riant ce but à Othon, mais le jeune homme répondit que le '''corbeau''' était un animal immonde, dont les plumes étaient indignes d’orner la toque d’un franc archer.'' {{source|{{w|Alexandre Dumas}}, ''[[s:Othon l’archer|Othon l’archer]]'', 1839}}
                            |#* ''[…] ; un '''corbeau''' déjà dressé et comment (il buvait du vin), avait jugé bon néanmoins de renoncer aux bienfaits de la civilisation et de reprendre la clé des bois ; […].'' {{source|{{w|Louis Pergaud}}, ''[[s:La Traque aux nids|La Traque aux nids]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                            |#* ''Dans dix ans, parmi les vestiges de Beaumat, les souffles du vent, les croassements des '''corbeaux''' et la chute des pierres retentiront seuls ; nulle oreille humaine ne les entendra et la cloche du village elle-même se taira, fatiguée de tinter seulement pour les morts.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}""".stripMargin
    val corbeauDEF1UsageExample = ddfModelSetter.usageExample(citationExemple)

    corbeauDEF1UsageExample.isDefined shouldBe true
    corbeauDEF1UsageExample.get.size shouldBe 3
    corbeauDEF1UsageExample.get.last.example shouldBe("""#* ''Dans dix ans, parmi les vestiges de Beaumat, les souffles du vent, les croassements des '''corbeaux''' et la chute des pierres retentiront seuls ; nulle oreille humaine ne les entendra et la cloche du village elle-même se taira, fatiguée de tinter seulement pour les morts.''""".stripMargin)
  }

  it should "get etymology Section" in {
    val etymSection = ddfModelSetter.etymologySection(WikitextCorbeau.corbeau)
    etymSection.get.title shouldEqual "=== {{S|étymologie}} ==="
    etymSection.get.content shouldEqual """: ''([[#fr-nom|Nom]])'' Du {{étyl|frm|fr|mot=corbeau}}, de l’{{étyl|fro|fr|mot=corbel}}, dérivé de {{lien|''corb''|fro}}, {{lien|''corp''|fro}}, du latin populaire de Gaule {{recons|corbus}}, du {{étyl|la|fr}} classique {{lien|''corvus''|la}}, de même sens. <br/>Le sens d’« auteur de lettres anonymes » remonte à l’affaire Angèle Laval qui inonda Tulle de lettres anonymes en 1920. L’affaire inspira le film ''[[w:Le Corbeau (film, 1943)|Le Corbeau]]'' de {{w|Henri-Georges Clouzot}} dans lequel une série de lettres anonymes signées « Le Corbeau » s’abat sur une petite ville française.
                                      |: ''([[#fr-adj|Adjectif]])'' En référence à la couleur des plumes du [[grand corbeau]].""".stripMargin
  }

  it should "get lexical Entry Section" in {
    val lexicalEntries = SplitProcessing.splitByLexicalEntry(WikitextCorbeau.corbeau, "corbeau")
    val sections = lexicalEntries(0).sections
    val lexicalEntry =  ddfModelSetter.wordTypeSection(sections)
    lexicalEntry(0).title shouldEqual "=== {{S|nom|fr}} ==="
    lexicalEntry(0).content shouldEqual """{{fr-rég-x|kɔʁ.bo}}
                                          |[[Fichier:Corvus corax in Akureyri 3.jpeg|vignette|Un '''corbeau''' sur un arbre ''(1)'']]
                                          |[[Fichier:Corbeau.de.bois.png|vignette|Un '''corbeau''' de bois ''(6)'']]
                                          |[[Fichier:Corvus.svg|vignette|Schéma de principe d’un '''corbeau''' ''(7)'']]
                                          |[[Fichier:Blason ville fr Fourcès (Gers).svg|vignette|120px|Armoiries avec 2 '''corbeaux''' ''(sens héraldique)'']]
                                          |'''corbeau''' {{pron|kɔʁ.bo|fr}} {{m|équiv=corbelle}}
                                          |# {{ornithol|nocat}} Nom [[vernaculaire]] du [[grand corbeau]] (''[[Corvus corax]]''), gros [[oiseau]] [[passereau]] [[carnassier]] à [[plumage]] [[noir]] et au [[bec]] [[fort]] et noir.
                                          |#* ''[…] un '''corbeau''' croassait à la dernière branche desséchée d’un chêne, et les archers montrèrent en riant ce but à Othon, mais le jeune homme répondit que le '''corbeau''' était un animal immonde, dont les plumes étaient indignes d’orner la toque d’un franc archer.'' {{source|{{w|Alexandre Dumas}}, ''[[s:Othon l’archer|Othon l’archer]]'', 1839}}
                                          |#* ''[…] ; un '''corbeau''' déjà dressé et comment (il buvait du vin), avait jugé bon néanmoins de renoncer aux bienfaits de la civilisation et de reprendre la clé des bois ; […].'' {{source|{{w|Louis Pergaud}}, ''[[s:La Traque aux nids|La Traque aux nids]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                                          |#* ''Dans dix ans, parmi les vestiges de Beaumat, les souffles du vent, les croassements des '''corbeaux''' et la chute des pierres retentiront seuls ; nulle oreille humaine ne les entendra et la cloche du village elle-même se taira, fatiguée de tinter seulement pour les morts.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
                                          |# {{par ext}} {{zoologie|nocat=1}} Oiseau d’[[allure]] [[proche]] du [[grand corbeau]] (1) ([[freux]], [[corneille]], [[choucas]], etc.) pouvant appartenir aux genres ''Corvus'' et ''Pyrrhocorax''.
                                          |#* ''Toute la journée, un vent aigre a soufflé de l’Ouest ; le ciel est resté bas et triste, et j’ai vu passer des vols de '''corbeaux'''…'' {{source|{{w|Octave Mirbeau}}, ''La tête coupée'',}}
                                          |#* ''Au moment de repartir, notre attention est attirée par une nuée de '''corbeaux''' s'agitant au-dessus d'un groupe d’êtres humains.'' {{source|{{w|Frédéric {{smcp|Weisgerber}}}}, ''Trois mois de campagne au Maroc : étude géographique de la région parcourue'', Paris : Ernest Leroux, 1904, p. 54}}
                                          |#* ''Quelques années plus tard, deux autres répulsifs pour '''corbeaux''' sont découverts : la diphénylguanidine et le triacétate de guazatine. Ces produits sont assez strictement limités aux corvidés.'' {{source|Jean Lhoste, Pierre Grison, ''La phytopharmacie française: chronique historique'', page 29, INRA, 1989}}
                                          |# {{histoire|fr}} En temps d’[[épidémie]], personne chargée par les autorités de sortir de la ville les [[cadavre]]s des [[pestiféré]]s afin de les réunir dans des [[fosse]]s.
                                          |#* ''Ce ne sont pas des fossoyeurs. […] Ces hommes en [[casaquin]] sont des '''corbeaux''', ainsi nommés à cause de leur [[croc]], par quoi ils crochent dans les morts pour non pas les approcher trop.'' {{source|{{w|Robert Merle}}, [[w:Fortune de France (Roman)|''Fortune de France'']], IX, 1977}}
                                          |#* ''Ce qu'il y avait de terrible au milieu de cette agonie de tout un peuple, c'était l'hilarité, la joie, l'allégresse de ces hommes chargés de réunir les morts, et qu'on avait baptisés du nom expressif de '''''corbeaux'''''.'' {{source|{{nom w pc|Alexandre|Dumas}}, ''Le comte de Moret'' (''Le sphinx rouge''), 1865, IV, 16}}
                                          |# {{par extension|fr}} {{désuet|fr}} {{populaire|fr}} Nom donné quelquefois aux [[porteur]]s des morts.
                                          |# {{désuet|fr}} {{péjoratif|fr}} Nom donné quelquefois aux [[prêtre]]s à cause de leurs vêtements noirs.
                                          |# {{figuré|fr}} {{familier|fr}} [[auteur|Auteur]] de [[lettre anonyme|lettres]] ou de [[coup de téléphone|coups de téléphone]] [[anonyme]]s.
                                          |#* ''Les juges chargés de faire la lumière sur les envois anonymes adressés en 2004 à leur collègue Renaud Van Ruymbeke, ont découvert un deuxième '''corbeau''' dans l’affaire Clearstream.'' {{source|''Le Monde'', 13 juillet 2006, Gérard Davet, Hervé Gattegno, lemonde.fr}}
                                          |# {{architecture|fr}} {{maçonnerie|fr}} [[pierre|Pierre]], [[pièce]] de [[bois]] ou de [[métal]], façonnée en [[console]], moindre en hauteur qu’en saillie, [[encastré]]e dans la [[maçonnerie]] et placée en [[saillie]] du [[nu]] d’un [[mur]], servant à [[supporter]] une [[charge]] ([[poutre]], [[corniche]], [[balcon]], etc.).
                                          |#* ''Puis, à mesure que la charpente métallique montait, (,,,) les consoles et les '''corbeaux''' se chargeaient de sculptures.'' {{source|{{w|Émile Zola}}, ''{{w|Au Bonheur des Dames}}'', 1883}}
                                          |#* ''Bâtière d’ardoises à coyau sur '''corbeaux''' calcaires en quart-de-rond.'' {{source|''Le patrimoine monumental de la Belgique : Wallonie'', tome 16, page 177, Ministère de la Région Wallonne, ‎1992, Éditions Mardaga}}
                                          |# {{antiquité|fr}} Espèce de [[grappin]] d’[[abordage]] et de [[pont]] [[volant]], utilisé par les [[Romain]]s.
                                          |#* ''Les Romains suppléèrent à cet inconvénient [la supériorité des Carthaginois dans la manœuvre des vaisseaux] par une machine qui fut inventée sur-le-champ et que depuis on a appelée '''corbeau''', par le moyen de laquelle ils accrochaient les vaisseaux des ennemis, passaient dedans malgré eux et en venaient aussitôt aux mains'' {{source|Rollin, Hist. anc. t. I, page 311, dans {{smcp|Pougens}}, cité dans ''Littré'', article ''corbeau''}}
                                          |# [[cépage|Cépage]] rouge de la [[Savoie]].{{R|plantgrape}}
                                          |#* ''En 1970, l’ampélographe français Paul Truel découvrit, lors d’un voyage à Mendoza, que ce cépage [bonarda] était en fait le '''corbeau''', appelé aussi douce noire et charbonneau.'' {{source|Wikipedia, article {{w|Bonarda}}, 2013}}
                                          |# {{meubles héraldiques|fr}} [[meuble|Meuble]] représentant l’animal du même nom dans les [[armoiries]]. Il est généralement représenté de [[profil]], debout sur ses [[patte]]s et [[de sable]]. À rapprocher de [[corneille]].
                                          |#* ''Écartelé d’or au lion de gueules et d’argent au '''corbeau''' de sable, becqué et membré de gueules, qui est de Fourcès du Gers'' {{cf|lang=fr}} illustration « armoiries avec 2 corbeaux »
                                          |# [[jeu|Jeu]] traditionnel de village.
                                          |#* ''Les uns se répandaient dans les cabarets pour digérer en fumant une pipe. D’autres jouaient aux quilles ou au '''corbeau''', ou bien encore au bricotiau.'' {{source|Charles Deulin, « Cambrinus, roi de la bière », ''{{ws|Cambrinus|Cambrinus et autres Contes}}'', Librairie internationale, 1868, page 25.}}""".stripMargin
  }

  it should "retrieve sense of col" in {
    val sense = ddfModelSetter.sense(Some(WikitextBulkData.col))
    sense.get.foreach(s => println {
      s.definition + " :" + " ex: " + s.usageExamples.get.size
    })

    sense.get.foreach { s =>
      println(s.definition.get)
      s.usageExamples.get.foreach(us => println(us.example))
    }

    sense.isDefined shouldBe(true)
    sense.get.size shouldEqual(7)

  }

  it should "retreive sense Relation of pomme" in {
    val pommeEntry = ddfModelSetter.entry(Pomme.resource)
    pommeEntry.lentries.get(0).sensRelation.isDefined shouldBe(true)
    pommeEntry.lentries.get(0).sensRelation.get.foreach(r => println(r.notationSkos))

    pommeEntry.lentries.get(0).sensRelation.get.foreach(r => println(r.relationWord))

    pommeEntry.lentries.get(0).sensRelation.get.size shouldBe(5)
  }

  it should "get the Sense Relation of corbeau" in {
    val lexicalEntries = SplitProcessing.splitByLexicalEntry(WikitextCorbeau.corbeau, "corbeau")
    val senseRelation = ddfModelSetter.senseRelation(lexicalEntries(0))
    senseRelation.isDefined shouldBe true
    senseRelation.get.size shouldBe 6
  }

  it should "get the Sense Relation of Somme" in {
    val lexicalEntries = SplitProcessing.splitByLexicalEntry(WikitextSomme.somme, "somme")
    val senseRelation = ddfModelSetter.senseRelation(lexicalEntries(0))
    lexicalEntries.foreach { le =>
      println(s"${le.uriLexicalEntry.toString} - ${le.lexicalEntryType} - ${le.sections.size}")
    }
    senseRelation.isDefined shouldBe true
    senseRelation.get.size shouldBe 3
  }

/*
=== {{S|nom|fr|num=1}} ===
{{fr-rég|sɔm}}
'''somme''' {{pron|sɔm|fr}} {{f}}

=== {{S|nom|fr|num=2}} ===
{{fr-rég|sɔm}}
'''somme''' {{pron|sɔm|fr}} {{m}}

=== {{S|nom|fr|num=3}} ===
{{fr-rég|sɔm}}
'''somme''' {{pron|sɔm|fr}} {{f}}
*/

  it should "get an entry for Cadeau" in {
    val cadeauEntry = ddfModelSetter.entry(Cadeau.resource)
    cadeauEntry.lentries.isDefined shouldBe true
    cadeauEntry.lentries.get.size shouldBe 1
    cadeauEntry.lentries.get(0).lexicalEntryType shouldEqual "=== {{S|nom|fr}} ==="
  }

  it should "get an entry for Yeux" in {
    val yeuxEntry = ddfModelSetter.entry(Yeux.resource)
    yeuxEntry.lentries.isDefined shouldBe false
  }
}