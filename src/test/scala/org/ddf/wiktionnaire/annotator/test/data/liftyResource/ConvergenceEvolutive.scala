/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object ConvergenceEvolutive {
  def convergenceEvolutive = {
    """== {{langue|fr}} ==
      |=== {{S|étymologie}} ===
      |:{{date|1862}} attesté dans la première édition française de l'ouvrage fondateur de {{w|Charles Darwin}} ''{{w|L'Origine des espèces|L’Origine des espèces}}'' sous la forme « convergence des caractères »{{R|Darwin&Royer|{{w|Charles Darwin}} (traduction en français : {{w|Clémence Royer}}), ''De l'origine des espèces ou des lois du progrès chez les êtres organisés}'' (titre original : ''On the Origin of Species''), Éditions chez Guillaumin et Victor Masson, 1862 }}.
      |:Mot {{composé de|convergence|évolutif|lang=fr}}
      |
      |=== {{S|nom|fr}} ===
      |{{fr-rég|kɔ̃.vɛʁ.ʒɑ̃.s‿e.vo.ly.tiv|p=convergences évolutives}}
      |[[File:Homology.jpg|vignette|L'apparition de l'aile dans des groupes [[phylogénétique]]ment très différents comme les ptérodactyles, les chauve-souris et les oiseaux est une '''convergence évolutive'''.]]
      |'''convergence évolutive''' {{pron|kɔ̃.vɛʁ.ʒɑ̃.s‿e.vo.ly.tiv|fr}} {{f}}
      |# {{lexique|biologie|fr}} [[mécanisme|Mécanisme]] [[évolutif]] ayant conduit des [[organisme]]s [[vivant]]s à développer, indépendamment les uns des autres, des [[adaptation]]s [[physiologique]]s, [[morphologique]]s, [[social]]es ou [[comportemental]]es similaires face à des contraintes [[écologique]]s comparables sans les avoir [[hériter|héritées]] d'un [[ancêtre]] commun.
      |#* {{exemple|Les phénomènes de '''convergence évolutive''' sont particulièrement nombreux dans l'ordre des Marsupicarnivora. Ils sont la conséquence des diverses radiations adaptatives effectuées indépendamment sur plusieurs aires géographiques.|lang=fr|source=Jean-Yves Crochet, ''Les marsupiaux du tertiaire d'Europe'', 1979}}
      |#* {{exemple|Quoi qu'il en soit, avec la foi du converti, {{w|Conway Morris}} devint le principal promoteur de l'idée selon laquelle la '''convergence évolutive''' était la clé de l'histoire de la vie au-delà de sa diversité. « La '''convergence évolutive''' est omniprésente, affirme-t-il : Où que vous portiez le regard, vous la trouverez. » En conclusion, il ajoute : « Rejouez le film de la vie autant de fois que vous voudrez, le résultat final sera toujours le même. » |lang=fr|source=Jonathan B. Losos (traduction française : Benjamin Watkins), ''Destinées improbables. Le hasard, la nécessité et l'avenir de l'évolution.'', La Découverte, 2021}}
      |
      |==== {{S|synonymes}} ====
      |* [[évolution convergente]]
      |
      |==== {{S|hyperonymes}} ====
      |* [[homoplasie]]
      |
      |==== {{S|antonymes}} ====
      |* [[divergence évolutive]]
      |* [[symplésiomorphie]]
      |* [[homologie]]
      |
      |==== {{S|vocabulaire}} ====
      |* [[analogie]]
      |
      |==== {{S|traductions}} ====
      |{{trad-début}}
      |* {{T|de}} : {{trad-|de|konvergente Evolution|f}}
      |* {{T|en}} : {{trad+|en|convergent evolution}}
      |* {{T|es}} : {{trad-|es|evolución convergente|f}}, {{trad-|es|convergencia evolutiva|f}}
      |* {{T|it}} : {{trad-|it|convergenza evolutiva|f}}
      |* {{T|ru}} : {{trad-|ru|конвергентная эволюция}}
      |{{trad-fin}}
      |
      |=== {{S|voir aussi}} ===
      |* {{WP}}
      |
      |=== {{S|références}} ===
      |{{références}}""".stripMargin
  }
}
