package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Yeux {
  val resource = Resource("yeux","https://fr.wiktionary.org/wiki/", "entry", """== {{langue|fr}} ==
                                                                                    |=== {{S|nom|fr|flexion}} ===
                                                                                    |{{en-nom|œil|yeux|œj|jø}}
                                                                                    |'''yeux''' {{pron|jø|fr}} {{m}}
                                                                                    |# ''Pluriel de'' [[œil]]. {{note}} Souvent prononcé {{pron|zjø|fr}} par [[pataquès]].
                                                                                    |#* ''La jeune nourrice obéit à Bras-Rouge mais, un jour qu’elle oignait le nourrisson, elle se frotta machinalement les '''yeux''' avec ses doigts encore enduits de pommade magique.'' {{source|Robert Colle, ''Légendes et contes d’Aunis et Saintonge'', éditions Rupella, 1979, page 114}}
                                                                                    |
                                                                                    |==== {{S|variantes}} ====
                                                                                    |* [[z’yeux]] ''(non standard)''
                                                                                    |
                                                                                    |==== {{S|synonymes}} ====
                                                                                    |*{{l|mirettes|fr}} {{familier|nocat}}
                                                                                    |
                                                                                    |==== {{S|dérivés}} ====
                                                                                    |{{(}}
                                                                                    |* [[avoir de la peau de saucisson devant les yeux]]
                                                                                    |* [[avoir les yeux plus gros que le ventre]]
                                                                                    |* [[faire des yeux de velours]]
                                                                                    |{{)}}
                                                                                    |
                                                                                    |=== {{S|prononciation}} ===
                                                                                    |* {{pron|jø|fr}}
                                                                                    |** {{écouter|lang=fr|France (Paris)|le.zjø|titre=les yeux|audio=Fr-yeux.ogg|fr}}
                                                                                    |** {{écouter|Paris (France)|jø|lang=fr|audio=Fr-Paris--yeux.ogg}}
                                                                                    |* {{écouter|lang=fr|France (Muntzenheim)|audio=LL-Q150 (fra)-0x010C-yeux.wav}}
                                                                                    |* {{écouter|lang=fr|France (Quimper)|audio=LL-Q150 (fra)-Vaulque-yeux.wav}}
                                                                                    |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-yeux.wav}}
                                                                                    |
                                                                                    |[[Catégorie:Pluriels irréguliers en français]])""".stripMargin)
}
