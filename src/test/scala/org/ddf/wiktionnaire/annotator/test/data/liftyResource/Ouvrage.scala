/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Ouvrage {
  def ouvrage = """== {{langue|fr}} ==
                  |=== {{S|étymologie}} ===
                  |: {{siècle|fin XIII}} Emprunté au {{étyl|la|fr|mot=synonymon|dif=synōnymon}} ou ''[[synonymum#la|synōnymum]]'', du grec ancien {{polytonique|συνώνυμον|sunōnumon}}, neutre singulier de {{polytonique|[[συνώνυμος]]|sunōnumos}}, même sens, composé de {{polytonique|[[σύν]]}} (« avec ») et de {{polytonique|[[ὄνομα]]}} (« nom »).
                  |
                  |=== {{S|adjectif|fr}} ===
                  |{{fr-rég|mf=oui|si.nɔ.nim}}
                  |'''synonyme''' {{pron|si.nɔ.nim|fr}} {{mf}}
                  |# {{lexique|linguistique|fr}} Qui a le même [[sens]] qu’un autre mot ou une [[signification]] [[presque]] [[semblable]].
                  |#* ''Les troubles du comportement alimentaire (TCA) sont souvent '''synonymes''' d’anorexie mentale et de boulimie.'' {{source|France Mutuelle Magazine, n° 174, octobre-novembre-décembre 2022, page 25}}
                  |#* {{exemple|La bioclimatologie ou biométéorologie humaine - on conviendra, au moins provisoirement, de considérer les deux termes comme '''synonymes''' - a pour objet l'étude des rapports existant entre le temps qu'il fait ou le climat et le fonctionnement de l'organisme humain, dans l'état de santé comme dans la maladie.|source=Jean-Pierre Besancenot, « ''Introduction'' » à ''Risques pathologiques, rythmes et paroxysmes climatiques'', Paris : Éditions John Libbey Eurotext, 1992, p. 2|lang=fr}}
                  |#*{{exemple|lang=fr|Jusqu'au {{siècle2|XVII}} siècle, les termes chimie et alchimie sont '''synonymes'''. Au début du {{siècle2|XVIII}} siècle, apparaît une opposition entre une nouvelle chimie, d'inspiration mécaniste, et l’ancienne. C’est alors que les mécanistes utilisent volontiers le mot alchimie pour critiquer leurs adversaires, les accusant de perpétuer une chimie dépassée.|source=Propos recueillis par Luc Allemand, [https://www.larecherche.fr/bernard-joly-%C2%AB-une-science-pratiqu%C3%A9e-au-grand-jour-%C2%BB Bernard Joly : « Une science pratiquée au grand jour »], ''La Recherche'', février 2008}}
                  |
                  |==== {{S|synonymes}} ====
                  |{{(}}
                  |* [[analogue]]
                  |* [[approchant]]
                  |* [[équivalent]]
                  |* [[identique]]
                  |* [[pareil]]
                  |* [[semblable]]
                  |* [[similaire]]
                  |{{)}}
                  |
                  |==== {{S|quasi-synonymes}} ====
                  |* [[allonyme]] ''(en toponymie)''
                  |
                  |==== {{S|dérivés}} ====
                  |{{(}}
                  |* [[quasi-synonyme]]
                  |* [[quasi-synonymie]]
                  |* [[synonymie]]
                  |* [[synonymisation]]
                  |* [[synonymique]]
                  |* [[synonymiser]]
                  |* [[synonymologie]]
                  |{{)}}
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début|}}
                  |* {{T|en}} : {{trad+|en|synonymous}}
                  |* {{T|br}} : {{trad+|br|heñvelster}}
                  |* {{T|hr}} : {{trad+|hr|sinonim}}
                  |* {{T|es}} : {{trad+|es|sinónimo|m}}, {{trad-|es|sinónima|f}}
                  |* {{T|ja}} : {{trad+|ja|同義|tr=dōgi}}
                  |* {{T|oc}} : {{trad+|oc|sinonim}}
                  |* {{T|ro}} : {{trad+|ro|sinonim}}
                  |{{trad-fin}}
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég|si.nɔ.nim}}
                  |'''synonyme''' {{pron|si.nɔ.nim|fr}} {{m}}
                  |# {{lexique|linguistique|fr}} Mot ou terme qui a un [[sens]] identique ou [[voisin]] à celui d’un autre mot ou d’un autre terme.
                  |#* ''Mais il y a plus : ce besoin de synonymiser s'est incrusté si profondément dans l'âme du traducteur qu'il choisira toute de suite un '''synonyme''' : il traduira «mélancolie» si dans le texte original il y a «tristesse», il traduira «tristesse» là où il y a «mélancolie».'' {{source|Jean Delisle, ''La traduction raisonnée : Manuel d'initiation à la traduction professionnelle de l'anglais vers le français'', Presses de l'Université d'Ottawa, 2{{e}} édition, 2003, p. 460}}
                  |#* ''Téléviseur et poste de télévision sont deux '''synonymes'''. — Long, large, vaste, haut sont des '''synonymes''' de grand.''
                  |#*{{exemple|lang=fr|La langue française est tellement facétieuse que ce qui peut être considéré comme antonyme (ou contraire) dans un contexte peut devenir '''synonyme''' (ou semblable) dans un autre.|source= Béatrice Pothier, ''Contribution de la linguistique à l’enseignement du français'', 2012}}
                  |==== {{S|quasi-synonymes}} ====
                  |''Mot du même sens'' :
                  |* [[allonyme]] ''(en toponymie)''
                  |* [[quasi-synonyme]]
                  |
                  |==== {{S|antonymes}} ====
                  |''Mot du même sens'' :
                  |* [[antonyme]]
                  |
                  |==== {{S|apparentés}} ====
                  |{{(|Mot du même sens}}
                  |* [[antonyme]]
                  |* [[holonyme]]
                  |* [[homonyme]]
                  |* [[hyperonyme]]
                  |* [[hyponyme]]
                  |* [[méronyme]]
                  |* [[paronyme]]
                  |{{)}}
                  |
                  |==== {{S|vocabulaire}} ====
                  |* [[datisme]]
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début|Mot avec le même sens (1)}}
                  |* {{T|sq}} : {{trad+|sq|sinonim}}
                  |* {{T|de}} : {{trad+|de|Synonym|n}}
                  |* {{T|en}} : {{trad+|en|synonym}}
                  |* {{T|ar}} : {{trad-|ar|مُتَرَادِف|tr=mutarādef}}, {{trad-|ar|مُرَادِف|tr=murādef}}
                  |* {{T|hy}} : {{trad+|hy|հոմանիշ|tr=homaniš}}
                  |* {{T|dsb}} : {{trad|dsb|synonym}}
                  |* {{T|br}} : {{trad+|br|heñvelster|m}}, {{trad-|br|sinonim}}
                  |* {{T|bg}} : {{trad+|bg|синоним|tr=sinonim}}
                  |* {{T|ca}} : {{trad+|ca|sinònim|m}}
                  |* {{T|shy}} : {{trad|shy|agdawal}}
                  |* {{T|zh}} : {{trad+|zh|同义词|tradi=同義詞|tr=tóngyìcí}}
                  |* {{T|ko}} : {{trad+|ko|동의어|tradi=同義語|tr=donguieo}}
                  |* {{T|hr}} : {{trad+|hr|istoznačnica|f}}, {{trad+|hr|sinonim|m}}
                  |* {{T|da}} : {{trad+|da|synonym|n}}
                  |* {{T|es}} : {{trad+|es|sinónimo|m}}
                  |* {{T|eo}} : {{trad-|eo|sinonimo}}
                  |* {{T|fi}} : {{trad+|fi|synonyymi}}
                  |* {{T|ga}} : {{trad+|ga|comhchiallach|m}}
                  |* {{T|cy}} : {{trad+|cy|cyfystyr|m}}
                  |* {{T|el}} : {{trad+|el|συνώνυμο|tr=synónymo|n}}
                  |* {{T|hsb}} : {{trad+|hsb|synonym}}
                  |* {{T|hu}} : {{trad+|hu|szinonima}}
                  |* {{T|io}} : {{trad+|io|sinonimo}}
                  |* {{T|id}} : {{trad+|id|sinonim}}
                  |* {{T|ia}} : {{trad-|ia|synonymo}}
                  |* {{T|is}} : {{trad+|is|samheiti|n}}
                  |* {{T|it}} : {{trad+|it|sinonimo|m}}
                  |* {{T|ja}} : {{trad+|ja|同義語|tr=dōgigo}}
                  |* {{T|kab}} : {{trad|kab|aknaw}}
                  |* {{T|crg}} : {{trad|crg|moon paray}}
                  |* {{T|nl}} : {{trad+|nl|synoniem|n}}
                  |* {{T|pes}} : {{trad|pes|مترادف}}
                  |* {{T|pl}} : {{trad+|pl|synonim|m}}
                  |* {{T|pt}} : {{trad+|pt|sinônimo}}
                  |* {{T|ro}} : {{trad+|ro|sinonim|n}}
                  |* {{T|ru}} : {{trad+|ru|синоним}} (sinonim) {{m}}
                  |* {{T|sr}} : {{trad+|sr|poveznica|f}}
                  |* {{T|sh}} : {{trad+|sh|sinonim}}
                  |* {{T|sl}} : {{trad+|sl|sopomenka|f}}, {{trad+|sl|sinonim|m}}
                  |* {{T|sv}} : {{trad+|sv|synonym}}
                  |* {{T|cs}} : {{trad+|cs|synonymum|n}}
                  |* {{T|uk}} : {{trad+|uk|синонім}} (synonim)
                  |* {{T|vi}} : {{trad-|vi|từ đồng nghĩa}}
                  |* {{T|wa}} : {{trad+|wa|sinonime|m}}, {{trad+|wa|piron-parey}}
                  |{{trad-fin}}
                  |
                  |=== {{S|prononciation}} ===
                  |* {{pron-rimes|si.nɔ.nim|fr}}
                  |* {{écouter|lang=fr|France (Île-de-France)|si.nɔ.nim|audio=Fr-Paris--synonyme.ogg}}
                  |* {{écouter|lang=fr|Suisse (canton du Valais)|audio=LL-Q150 (fra)-DSwissK-synonyme.wav}}
                  |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-synonyme.wav}}
                  |
                  |==== {{S|homophones|fr}} ====
                  |* [[cynonyme]]
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{Annexe|Mots ayant le plus de synonymes en français}}
                  |* [[Aide:Synonymes et antonymes|Page d’aide du Wiktionnaire sur les synonymes]]
                  |* {{WP}}
                  |* {{Le Dico des Ados}}
                  |
                  |=== {{S|références}} ===
                  |* {{R:DAF8}}
                  |* {{R:TLFi}}
                  |
                  |[[Catégorie:Mots autologiques en français]]
                  |""".stripMargin
}
