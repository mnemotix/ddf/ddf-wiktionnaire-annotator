package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextClimatologie {

  val climatologie = """== {{langue|fr}} ==
=== {{S|étymologie}} ===
: {{ébauche-étym|fr}}

=== {{S|nom|fr}} ===
{{fr-rég|kli.ma.tɔ.lɔ.ʒi}}
'''climatologie''' {{pron|kli.ma.tɔ.lɔ.ʒi|fr}} {{f}}
# [[science|Science]] qui a pour [[objet]] l’[[étude]] des [[climat]]s.
#* ''Une préoccupation importante de la '''climatologie''' consiste à identifier, à classifier, à décrire et à expliquer les climats actuels ou passés.'' {{source|Raymond {{pc|Gervais}}, ‎Richard {{pc|Leduc}}, ''La Météo en Photos'', 1986}}

==== {{S|traductions}} ====
{{trad-début}}
* {{T|sq}} : {{trad-|sq|klimatologjia}}
* {{T|de}} : {{trad+|de|Klimatologie}}
* {{T|en}} : {{trad+|en|climatology}}
* {{T|eu}} : {{trad+|eu|klimatologia}}
* {{T|bg}} : {{trad-|bg|климатология}}
* {{T|ca}} : {{trad-|ca|climatologia}}
* {{T|hr}} : {{trad+|hr|klimatologija}}
* {{T|da}} : {{trad+|da|klimatologi}}
* {{T|es}} : {{trad+|es|climatología}}
* {{T|fi}} : {{trad+|fi|klimatologia}}
* {{T|gl}} : {{trad-|gl|climatoloxía}}
* {{T|he}} : {{trad+|he|קלימטולוגיה}}
* {{T|hu}} : {{trad+|hu|klimatológia}}
* {{T|io}} : {{trad+|io|klimatologio}}
* {{T|id}} : {{trad+|id|klimatologi}}
* {{T|is}} : {{trad-|is|veðurfarsfræði}}
* {{T|it}} : {{trad+|it|climatologia}}
* {{T|lt}} : {{trad-|lt|klimatologija}}
* {{T|lmo}} : {{trad--|lmo|climatulugía}}
* {{T|ms}} : {{trad-|ms|klimatologi}}
* {{T|nl}} : {{trad+|nl|klimatologie}}
* {{T|no}} : {{trad-|no|klimatologi}}
* {{T|nn}} : {{trad-|no|klimatologi}}
* {{T|pl}} : {{trad+|pl|klimatologia}}
* {{T|pt}} : {{trad+|pt|climatologia}}
* {{T|ru}} : {{trad+|ru|климатология}}
* {{T|sr}} : {{trad-|sr|климатологија}}
* {{T|sh}} : {{trad+|sh|klimatologija}}
* {{T|scn}} : {{trad-|scn|climatoluggìa}}
* {{T|sk}} : {{trad-|sk|klimatológia}}
* {{T|sv}} : {{trad+|sv|klimatologi}}
* {{T|tg}} : {{trad-|tg|иқлимшиносӣ}}
* {{T|cs}} : {{trad-|cs|klimatologie}}
* {{T|tsolyáni}} : {{trad--|tsolyáni|srikàtlmongékh}}
* {{T|tr}} : {{trad-|tr|iklimbilim}}
* {{T|uk}} : {{trad+|uk|кліматологія|f|tr=klimatologija}}
* {{T|vec}} : {{trad-|vec|climatołogia}}
{{trad-fin}}

=== {{S|voir aussi}} ===
* {{WP}}

=== {{S|références}} ===
*{{Import:DAF8}}

== {{langue|it}} ==
{{it-accord-ae|climatologi|kli.ma.to.lo.ˈɡi}}
'''climatologie''' {{pron|kli.ma.to.lo.ˈɡie|it}} {{f}}
# ''Pluriel de ''[[climatologia#it|climatologia]]""".stripMargin
}
