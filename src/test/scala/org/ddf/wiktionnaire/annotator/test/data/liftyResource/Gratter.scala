package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Gratter {
  val resource = Resource("gratter", "https://fr.wiktionary.org/wiki/", "entry", """== {{langue|fr}}==
                                                                                        |=== {{S|étymologie}} ===
                                                                                        |: De l'ancien français ''grater'', par un intermédiaire attesté en latin médiéval, ''cratare'', emprunté à l'ancien bas {{étyl|frk|fr}} {{recons|krattōn}} « frotter en raclant », qui s'apparente au moyen bas-allemand ''kratten'', à l'allemand ''kratzen'', à l'anglais régional ''scrat'' ; {{cf|kratzen}}. Du français sont tirés ''gratar'' en occitan et espagnol et ''grattare'' en italien.
                                                                                        |
                                                                                        |=== {{S|verbe|fr}} ===
                                                                                        |'''gratter''' {{pron|ɡʁa.te|fr}} {{t|fr}} {{conj|grp=1|fr}} {{lien pronominal}}
                                                                                        |# [[racler|Racler]] pour [[nettoyer]], pour [[effacer]] ou pour [[polir]].
                                                                                        |#* '''''Gratter''' une muraille.''
                                                                                        |#* '''''Gratter''' des souliers avec un couteau pour en enlever la boue.''
                                                                                        |#* '''''Gratter''' un parchemin un papier pour enlever l’écriture.''
                                                                                        |#* ''Ce mot a été '''gratté'''.''
                                                                                        |#* '''''Grattez''' plus fort.''
                                                                                        |# {{par ext|fr}} [[remuer|Remuer]] avec ses [[ongle]]s.
                                                                                        |#* ''Un mandoliniste '''grattait''' son instrument comme dans un café de province où les habitués sont demeurés de mœurs paisibles.'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
                                                                                        |#* ''Ses petits-enfants soufflottaient sur le divan ; elle '''grattait''' doucement la plante de leurs pieds roses pour améliorer les ris de leurs rêves enfantins.'' {{source|Vasiliĭ Aksenov, ''Recherche d'un genre: première version'', traduit du russe (URSS) par Lily Denis, éd. Gallimard, 1979, p. 116}}
                                                                                        |#* ''— J'en pense qu'une fille qui habite Le Faouët peut très bien faire ses courses à Gourin, si elle travaille quelque part entre les deux communes, voire même à Gourin, répliqua le retraité de sa grosse voix, '''en grattant''' sa barbe poivre et sel.'' {{source|Bernard Larhant, ''La Madone du Faouët: Un roman policier en pays breton'', Éditions Alain Bargain, 2017, chap. 2}}
                                                                                        |# {{populaire|fr}} [[doubler|Doubler]], [[dépasser]] [[quelqu’un]] ou [[quelque chose]].
                                                                                        |#* ''Il m’'''a gratté''' au démarrage avec sa nouvelle voiture.''
                                                                                        |# {{populaire|fr}} [[récupérer|Récupérer]], [[obtenir]].
                                                                                        |#* ''Des gens ne viennent à certains conférences que pour '''gratter''' des cadeaux.''
                                                                                        |# {{argot|fr}} ''(Vendée, Bretagne, Normandie, Île-de-France)'' {{scolaire|fr}} [[tricher|Tricher]] à l’aide de [[gratte]] lors d’un [[examen]].
                                                                                        |# {{intransitif|fr}} {{argot|fr}} [[travailler|Travailler]].
                                                                                        |#* ''Comme il avait une poule dans la même boite où que j’'''grattais''', il m’obligeait à partir avec elle le soir en y tenant le bras […].'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
                                                                                        |# {{intransitif|fr}} {{figuré|fr}} Creuser ou [[approfondir]] sa recherche ou son [[enquête]].
                                                                                        |#* ''Y a des gens, au début, ' sont sympas. Mais après, quand on '''gratte''', on voit qu'ils sont pas sympas.'' {{source|{{w|Stupeflip}}, ''Non o gen pasimpa''}}
                                                                                        |#* ''Je repensais à ses paroles dans le bar, comme quoi il allait falloir '''gratter''' profond. Leveaux pris en flag chez le maire pendant son kidnapping... Et quand on '''gratte''' profond, Fowler, ça saigne.'' {{source|François & Emmanuel Hollman, ''2032 - Livre 1: la Société : roman policier'',  Éditions Publibook, 2014, p. 226}}
                                                                                        |# {{pronl|fr}} {{particulier}} [[passer|Passer]] les [[ongle]]s ou quelque chose de [[semblable]], un peu [[fortement]] et à [[plusieurs]] [[reprise]]s, sur quelque [[endroit]] du [[corps]].
                                                                                        |#* ''Il '''se gratte''' le front qu’un prurit tenace ne cesse de taquiner, et où fleurissent des taches rougeâtres.'' {{source|{{Citation/Victor Méric/Les Compagnons de l’Escopette/1930|257}}}}
                                                                                        |#* ''Un cheval qui se '''gratte''' contre la muraille.''
                                                                                        |#* ''Deux ânes qui se '''grattent''' l’un contre l’autre.''
                                                                                        |
                                                                                        |==== {{S|dérivés}} ====
                                                                                        |{{(}}
                                                                                        |* [[carte à gratter]]
                                                                                        |* [[entre-gratter]]
                                                                                        |* [[grattage]]
                                                                                        |* [[gratte-bottes]]
                                                                                        |* [[gratte-ciel]]
                                                                                        |* [[gratte-cul]]
                                                                                        |* [[gratte-dos]]
                                                                                        |* [[gratte-langue]]
                                                                                        |* [[gratte-menton]]
                                                                                        |* [[gratteler]]
                                                                                        |* [[grattement]]
                                                                                        |* [[gratte-paille]]
                                                                                        |* [[gratte-papier]]
                                                                                        |* [[gratte-pieds]]
                                                                                        |* [[gratte-pierre]]
                                                                                        |* [[gratter du papier]] ''(gagner sa vie en faisant des écritures)''
                                                                                        |* [[gratter la terre]] ''(se dit par analogie d’un labourage qui ne fait que l’effleurer)''
                                                                                        |* [[gratter l’amitié]] ''(expression populaire pour désigner une personne qui cherche à devenir ami avec quelqu'un ou à se réconcilier avec une personne après une dispute)''
                                                                                        |* [[gratter où ça démange]]
                                                                                        |* [[gratter sur la dépense]] ''(économiser sur une dépense)''
                                                                                        |* [[gratter un ballon]] ''(récupérer un ballon dans un regroupement)'' (rugby)
                                                                                        |* [[gratterie]]
                                                                                        |* [[gratteur]]
                                                                                        |* [[gratteux]]
                                                                                        |* [[grattoir]]
                                                                                        |* [[grattouiller]]
                                                                                        |* [[poil à gratter]]
                                                                                        |* [[regratter]]
                                                                                        |* [[se gratter l’oreille]]
                                                                                        |* [[se gratter la fesse]] {{Réunion|nocat=1}} (''ne rien faire'')
                                                                                        |{{)}}
                                                                                        |
                                                                                        |==== {{S|phrases}} ====
                                                                                        |{{(}}
                                                                                        |* [[après la fête, on gratte sa tête]]
                                                                                        |* [[ce sont deux ânes qui se grattent]] ''(se dit, par dérision, de deux personnes qui se flattent l’une l’autre)''
                                                                                        |* [[chaque poule vit de ce qu’elle gratte]]
                                                                                        |* [[qui se sent galeux se gratte]]
                                                                                        |* [[trop gratter cuit, trop parler nuit]] ''(si l’on dépasse la mesure, il en résulte un mal au lieu d’un bien)''
                                                                                        |* [[tu peux toujours te gratter]] ''(pas la peine d’espérer quoi que ce soit)''
                                                                                        |{{)}}
                                                                                        |
                                                                                        |==== {{S|traductions}} ====
                                                                                        |{{trad-début}}
                                                                                        |* {{T|af}} : {{trad-|af|krap}}
                                                                                        |* {{T|de}} : {{trad+|de|jucken}}, {{trad+|de|schaben}}, {{trad+|de|kratzen}}, {{trad+|de|radieren}}, {{trad-|de|schrapen}}, {{trad+|de|ritzen}}
                                                                                        |* {{T|en}} : {{trad+|en|itch}}, {{trad+|en|scrape}}, {{trad+|en|scratch}}, {{trad+|en|squawk}}
                                                                                        |* {{T|br}} : {{trad+|br|skrabañ}}, {{trad-|br|kravat}}, {{trad-|br|krafignat}}
                                                                                        |* {{T|ca}} : {{trad-|ca|carriquejar}}, {{trad+|ca|escarbotar}}, {{trad+|ca|grinyolar}}, {{trad+|ca|pelar}}, {{trad+|ca|raspar}}, {{trad+|ca|raure}}, {{trad+|ca|gratar}}, {{trad+|ca|rascar}}
                                                                                        |* {{T|co}} : {{trad-|co|grattà}}, {{trad-|co|rasticcià}}, {{trad-|co|ruspà}} (les poules)
                                                                                        |* {{T|dmr}} : {{trad--|dmr|neˈwur}}
                                                                                        |* {{T|da}} : {{trad-|da|skrabe}}, {{trad-|da|kradse}}
                                                                                        |* {{T|es}} : {{trad+|es|rascar}}, {{trad+|es|raspar}}, {{trad+|es|raer}}
                                                                                        |* {{T|eo}} : {{trad-|eo|skrapi}}, {{trad-|eo|grati}}
                                                                                        |* {{T|fo}} : {{trad-|fo|klóra}}, {{trad-|fo|skava}}
                                                                                        |* {{T|fi}} : {{trad-|fi|raapia}}, {{trad+|fi|kynsiä}}
                                                                                        |* {{T|io}} : {{trad+|io|gratar}}, {{trad+|io|skrapar}}
                                                                                        |* {{T|id}} : {{trad+|id|menggaruk}}
                                                                                        |* {{T|it}} : {{trad+|it|grattare}}, {{trad-|it|raschiare}}
                                                                                        |* {{T|la}} : {{trad-|la|scabere}}
                                                                                        |* {{T|yua}} : {{trad--|yua|la’achik}}
                                                                                        |* {{T|nl}} : {{trad+|nl|krassen}}, {{trad+|nl|schrapen}}, {{trad+|nl|schrappen}}, {{trad+|nl|klauwen}}, {{trad+|nl|krabben}}
                                                                                        |* {{T|oc}} : {{trad-|oc|gratar}}, {{trad-|oc|gratussar}}, {{trad+|oc|raspar}}
                                                                                        |* {{T|pap}} : {{trad--|pap|huña}}
                                                                                        |* {{T|myp}} : {{trad--|myp|xaxahói}}
                                                                                        |* {{T|pt}} : {{trad+|pt|arranhar}}, {{trad+|pt|rapar}}, {{trad+|pt|raspar}}, {{trad+|pt|coçar}}
                                                                                        |* {{T|se}} : {{trad--|se|faskut}}, {{trad--|se|ruvvet}}, {{trad--|se|gazzut}}
                                                                                        |* {{T|ses}} : {{trad--|ses|kaaji}}
                                                                                        |* {{T|sv}} : {{trad+|sv|klå}}, {{trad+|sv|klösa}}, {{trad+|sv|krafsa}}, {{trad+|sv|riva}}
                                                                                        |{{trad-fin}}
                                                                                        |{{trad-début|Racler pour nettoyer, pour effacer ou pour polir|1}}
                                                                                        |* {{T|co}} : {{trad-|co|rasticcià}}
                                                                                        |* {{T|eo}} : {{trad-|eo|grati}}
                                                                                        |* {{T|kk}} : {{trad-|kk|қыру|tr=qıruw}}
                                                                                        |* {{T|se}} : {{trad--|se|ruohkastit}}
                                                                                        |* {{T|zdj}} : {{trad--|zdj|-pvaya|dif=upvaya}}
                                                                                        |{{trad-fin}}
                                                                                        |{{trad-début|Passer les ongles ou quelque chose de semblable, un peu fortement et à plusieurs reprises, sur quelque endroit du corps}}
                                                                                        |* {{T|ba}} : {{trad--|ba|тырнау}}
                                                                                        |* {{T|eo}} : {{trad-|eo|skrapi}}
                                                                                        |* {{T|sah}} : {{trad--|sah|тарбаа}}
                                                                                        |* {{T|krc}} : {{trad--|krc|къа­шыргъа}}
                                                                                        |* {{T|kk}} : {{trad-|kk|қасу|tr=qasuw}}, {{trad-|kk|тырнау|tr=tırnaw}}
                                                                                        |* {{T|ky}} : {{trad+|ky|тырмоо}}, {{trad-|ky|кашуу}}
                                                                                        |* {{T|kum}} : {{trad--|kum|хашымакъ}}
                                                                                        |* {{T|swb}} : {{trad--|swb|ukua}}
                                                                                        |* {{T|se}} : {{trad--|se|sakŋidit}}, {{trad--|se|ruohkastit}}
                                                                                        |* {{T|crh}} : {{trad--|crh|qaşımaq}}
                                                                                        |* {{T|tt}} : {{trad-|tt|кашу}}
                                                                                        |* {{T|cv}} : {{trad--|cv|хыç}}
                                                                                        |* {{T|tk}} : {{trad-|tk|gaşamak}}
                                                                                        |{{trad-fin}}
                                                                                        |
                                                                                        |=== {{S|prononciation}} ===
                                                                                        |* {{pron|ɡʁa.te|fr}}
                                                                                        |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|ɡʁa.te|audio=Fr-gratter.ogg}}
                                                                                        |
                                                                                        |[[Catégorie:Lexique en français de l’illégalité]]
                                                                                        |* {{écouter||ɡʁa.te|lang=fr|audio=LL-Q150 (fra)-Guilhelma-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Roll-Morton-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Massy)|audio=LL-Q150 (fra)-X-Javier-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-GrandCelinien-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|Suisse (Genève)|audio=LL-Q150 (fra)-Nattes à chat-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Penegal-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Paris)||audio=LL-Q150 (fra)-Ash Crow-gratter.wav}}
                                                                                        |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-gratter.wav}}""".stripMargin)
}
