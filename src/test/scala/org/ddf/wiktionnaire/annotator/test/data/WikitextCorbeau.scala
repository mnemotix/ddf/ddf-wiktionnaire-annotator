package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextCorbeau {

  val corbeau = """=== {{S|étymologie}} ===
                  |: ''([[#fr-nom|Nom]])'' Du {{étyl|frm|fr|mot=corbeau}}, de l’{{étyl|fro|fr|mot=corbel}}, dérivé de {{lien|''corb''|fro}}, {{lien|''corp''|fro}}, du latin populaire de Gaule {{recons|corbus}}, du {{étyl|la|fr}} classique {{lien|''corvus''|la}}, de même sens. <br/>Le sens d’« auteur de lettres anonymes » remonte à l’affaire Angèle Laval qui inonda Tulle de lettres anonymes en 1920. L’affaire inspira le film ''[[w:Le Corbeau (film, 1943)|Le Corbeau]]'' de {{w|Henri-Georges Clouzot}} dans lequel une série de lettres anonymes signées « Le Corbeau » s’abat sur une petite ville française.
                  |: ''([[#fr-adj|Adjectif]])'' En référence à la couleur des plumes du [[grand corbeau]].
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég-x|kɔʁ.bo}}
                  |[[Fichier:Corvus corax in Akureyri 3.jpeg|vignette|Un '''corbeau''' sur un arbre ''(1)'']]
                  |[[Fichier:Corbeau.de.bois.png|vignette|Un '''corbeau''' de bois ''(6)'']]
                  |[[Fichier:Corvus.svg|vignette|Schéma de principe d’un '''corbeau''' ''(7)'']]
                  |[[Fichier:Blason ville fr Fourcès (Gers).svg|vignette|120px|Armoiries avec 2 '''corbeaux''' ''(sens héraldique)'']]
                  |'''corbeau''' {{pron|kɔʁ.bo|fr}} {{m|équiv=corbelle}}
                  |# {{ornithol|nocat}} Nom [[vernaculaire]] du [[grand corbeau]] (''[[Corvus corax]]''), gros [[oiseau]] [[passereau]] [[carnassier]] à [[plumage]] [[noir]] et au [[bec]] [[fort]] et noir.
                  |#* ''[…] un '''corbeau''' croassait à la dernière branche desséchée d’un chêne, et les archers montrèrent en riant ce but à Othon, mais le jeune homme répondit que le '''corbeau''' était un animal immonde, dont les plumes étaient indignes d’orner la toque d’un franc archer.'' {{source|{{w|Alexandre Dumas}}, ''[[s:Othon l’archer|Othon l’archer]]'', 1839}}
                  |#* ''[…] ; un '''corbeau''' déjà dressé et comment (il buvait du vin), avait jugé bon néanmoins de renoncer aux bienfaits de la civilisation et de reprendre la clé des bois ; […].'' {{source|{{w|Louis Pergaud}}, ''[[s:La Traque aux nids|La Traque aux nids]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                  |#* ''Dans dix ans, parmi les vestiges de Beaumat, les souffles du vent, les croassements des '''corbeaux''' et la chute des pierres retentiront seuls ; nulle oreille humaine ne les entendra et la cloche du village elle-même se taira, fatiguée de tinter seulement pour les morts.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
                  |# {{par ext}} {{zoologie|nocat=1}} Oiseau d’[[allure]] [[proche]] du [[grand corbeau]] (1) ([[freux]], [[corneille]], [[choucas]], etc.) pouvant appartenir aux genres ''Corvus'' et ''Pyrrhocorax''.
                  |#* ''Toute la journée, un vent aigre a soufflé de l’Ouest ; le ciel est resté bas et triste, et j’ai vu passer des vols de '''corbeaux'''…'' {{source|{{w|Octave Mirbeau}}, ''La tête coupée'',}}
                  |#* ''Au moment de repartir, notre attention est attirée par une nuée de '''corbeaux''' s'agitant au-dessus d'un groupe d’êtres humains.'' {{source|{{w|Frédéric {{smcp|Weisgerber}}}}, ''Trois mois de campagne au Maroc : étude géographique de la région parcourue'', Paris : Ernest Leroux, 1904, p. 54}}
                  |#* ''Quelques années plus tard, deux autres répulsifs pour '''corbeaux''' sont découverts : la diphénylguanidine et le triacétate de guazatine. Ces produits sont assez strictement limités aux corvidés.'' {{source|Jean Lhoste, Pierre Grison, ''La phytopharmacie française: chronique historique'', page 29, INRA, 1989}}
                  |# {{histoire|fr}} En temps d’[[épidémie]], personne chargée par les autorités de sortir de la ville les [[cadavre]]s des [[pestiféré]]s afin de les réunir dans des [[fosse]]s.
                  |#* ''Ce ne sont pas des fossoyeurs. […] Ces hommes en [[casaquin]] sont des '''corbeaux''', ainsi nommés à cause de leur [[croc]], par quoi ils crochent dans les morts pour non pas les approcher trop.'' {{source|{{w|Robert Merle}}, [[w:Fortune de France (Roman)|''Fortune de France'']], IX, 1977}}
                  |#* ''Ce qu'il y avait de terrible au milieu de cette agonie de tout un peuple, c'était l'hilarité, la joie, l'allégresse de ces hommes chargés de réunir les morts, et qu'on avait baptisés du nom expressif de '''''corbeaux'''''.'' {{source|{{nom w pc|Alexandre|Dumas}}, ''Le comte de Moret'' (''Le sphinx rouge''), 1865, IV, 16}}
                  |# {{par extension|fr}} {{désuet|fr}} {{populaire|fr}} Nom donné quelquefois aux [[porteur]]s des morts.
                  |# {{désuet|fr}} {{péjoratif|fr}} Nom donné quelquefois aux [[prêtre]]s à cause de leurs vêtements noirs.
                  |# {{figuré|fr}} {{familier|fr}} [[auteur|Auteur]] de [[lettre anonyme|lettres]] ou de [[coup de téléphone|coups de téléphone]] [[anonyme]]s.
                  |#* ''Les juges chargés de faire la lumière sur les envois anonymes adressés en 2004 à leur collègue Renaud Van Ruymbeke, ont découvert un deuxième '''corbeau''' dans l’affaire Clearstream.'' {{source|''Le Monde'', 13 juillet 2006, Gérard Davet, Hervé Gattegno, lemonde.fr}}
                  |# {{architecture|fr}} {{maçonnerie|fr}} [[pierre|Pierre]], [[pièce]] de [[bois]] ou de [[métal]], façonnée en [[console]], moindre en hauteur qu’en saillie, [[encastré]]e dans la [[maçonnerie]] et placée en [[saillie]] du [[nu]] d’un [[mur]], servant à [[supporter]] une [[charge]] ([[poutre]], [[corniche]], [[balcon]], etc.).
                  |#* ''Puis, à mesure que la charpente métallique montait, (,,,) les consoles et les '''corbeaux''' se chargeaient de sculptures.'' {{source|{{w|Émile Zola}}, ''{{w|Au Bonheur des Dames}}'', 1883}}
                  |#* ''Bâtière d’ardoises à coyau sur '''corbeaux''' calcaires en quart-de-rond.'' {{source|''Le patrimoine monumental de la Belgique : Wallonie'', tome 16, page 177, Ministère de la Région Wallonne, ‎1992, Éditions Mardaga}}
                  |# {{antiquité|fr}} Espèce de [[grappin]] d’[[abordage]] et de [[pont]] [[volant]], utilisé par les [[Romain]]s.
                  |#* ''Les Romains suppléèrent à cet inconvénient [la supériorité des Carthaginois dans la manœuvre des vaisseaux] par une machine qui fut inventée sur-le-champ et que depuis on a appelée '''corbeau''', par le moyen de laquelle ils accrochaient les vaisseaux des ennemis, passaient dedans malgré eux et en venaient aussitôt aux mains'' {{source|Rollin, Hist. anc. t. I, page 311, dans {{smcp|Pougens}}, cité dans ''Littré'', article ''corbeau''}}
                  |# [[cépage|Cépage]] rouge de la [[Savoie]].{{R|plantgrape}}
                  |#* ''En 1970, l’ampélographe français Paul Truel découvrit, lors d’un voyage à Mendoza, que ce cépage [bonarda] était en fait le '''corbeau''', appelé aussi douce noire et charbonneau.'' {{source|Wikipedia, article {{w|Bonarda}}, 2013}}
                  |# {{meubles héraldiques|fr}} [[meuble|Meuble]] représentant l’animal du même nom dans les [[armoiries]]. Il est généralement représenté de [[profil]], debout sur ses [[patte]]s et [[de sable]]. À rapprocher de [[corneille]].
                  |#* ''Écartelé d’or au lion de gueules et d’argent au '''corbeau''' de sable, becqué et membré de gueules, qui est de Fourcès du Gers'' {{cf|lang=fr}} illustration « armoiries avec 2 corbeaux »
                  |# [[jeu|Jeu]] traditionnel de village.
                  |#* ''Les uns se répandaient dans les cabarets pour digérer en fumant une pipe. D’autres jouaient aux quilles ou au '''corbeau''', ou bien encore au bricotiau.'' {{source|Charles Deulin, « Cambrinus, roi de la bière », ''{{ws|Cambrinus|Cambrinus et autres Contes}}'', Librairie internationale, 1868, page 25.}}
                  |
                  |==== {{S|synonymes}} ====
                  |{{(|Grand oiseau noir}}
                  |* [[grand corbeau]]
                  |* [[corbac]] {{argot|nocat=1}}
                  |* [[corbin]] {{désuet|nocat=1}}
                  |* [[gniasse]] {{région|nocat=1}}
                  |* [[grolle]] {{région|nocat=1}}
                  |* [[agrolle]] {{région|nocat=1}}
                  |{{)}}
                  |{{(|Genre de gros oiseau noir}}
                  |* [[corvidé]]
                  |{{)}}
                  |{{(|Auteur de lettre anonyme}}
                  |* [[anonymographe]]
                  |{{)}}
                  |{{(|Pièce maçonnée en saillie d’un mur servant à supporter une charge}}
                  |* [[console]]
                  |* [[coussinet]]
                  |{{)}}
                  |{{(|Cépage de Savoie}}
                  |* [[douce noire]]
                  |* [[charbonneau]]
                  |{{)}}
                  |
                  |==== {{S|quasi-synonymes}} ====
                  |; Pièce maçonnée en saillie d’un mur servant à supporter une charge
                  |* [[modillon]]
                  |
                  |==== {{S|dérivés}} ====
                  |{{(}}
                  |* [[aile de corbeau]]
                  |* [[assise de corbeau]]
                  |* [[bec de corbeau]]
                  |* [[bleu corbeau]]
                  |* [[corbeau à collier]]
                  |* [[corbeau à cou blanc]]
                  |* [[corbeau à gros bec]]
                  |* [[corbeau à nuque blanche]]
                  |* [[corbeau à queue courte]]
                  |* [[corbeau blanc]]
                  |* [[corbeau brun]]
                  |* [[corbeau calédonien]]
                  |* [[corbeau corbivau]]
                  |* [[corbeau d’Australie]]
                  |* [[corbeau d’Édith]]
                  |* [[corbeau de fer]]
                  |* [[corbeau de Levaillant]]
                  |* [[corbeau de mer]]
                  |* [[corbeau de nuit]]
                  |* [[corbeau de Tasmanie]]
                  |* [[corbeau de Torres]]
                  |* [[corbeau des Bismarck]]
                  |* [[corbeau du désert]]
                  |* [[corbeau familier]]
                  |* [[corbeau freux]]
                  |* [[corbeau indien]]
                  |* [[corbeau-pêcheur]]
                  |* [[corbeau pie]]
                  |* [[corbillat]]
                  |* [[corbillot]]
                  |* [[corbineau]]
                  |* [[grand corbeau]]
                  |* [[nid de corbeau]]
                  |* [[noir comme un corbeau]]
                  |* [[noir corbeau]]
                  |* [[petit corbeau]]
                  |* [[ravitaillé par les corbeaux]]
                  |* [[servir de pâture aux corbeaux]]
                  |{{)}}
                  |
                  |==== {{S|apparentés}} ====
                  |* [[encorbellement]]
                  |
                  |==== {{S|phrases}} ====
                  |* [[les corbeaux ne font pas d’agasses]]
                  |
                  |==== {{S|vocabulaire}} ====
                  |* [[croasser]]
                  |* {{voir thésaurus|fr|corbeau}}
                  |; Oiseaux ressemblant au corbeau
                  |* [[choucas]]
                  |* [[corneille]]
                  |* {{voir thésaurus|fr|police}}
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début|Oiseau}}
                  |* {{T|conv}} : ''{{trad+|conv|Corvus corax}}'' (L.)
                  |* {{T|abx}} : {{trad--|abx|wakwak}}
                  |* {{T|af}} : {{trad+|af|raaf}}
                  |* {{T|aht}} : {{trad--|aht|saghani}}, {{trad--|aht|dotsonʼ}}
                  |* {{T|ain}} : {{trad--|ain|オンネパスクル|R=[[onne-paskuru]]}}
                  |* {{T|akz}} : {{trad--|akz|lakcha}}
                  |* {{T|sq}} : {{trad+|sq|korb}}
                  |* {{T|gsw-fr}} : {{trad--|gsw-fr|Kràpp}}
                  |* {{T|ale}} : ''dialecte atkan d’Alaska'' : {{trad--|ale|qanglaax̂}}, {{trad--|ale|qanglaaĝix̂}} ; ''dialecte attuan d’Alaska'' : {{trad--|ale|qaglaax̂}}, {{trad--|ale|qalgaax̂}} ; ''dialecte oriental'' : {{trad--|ale|qalngaax̂}}
                  |* {{T|alq}} : {{trad--|alq|kakaki}}
                  |* {{T|de}} : {{trad+|de|Rabe}} {{m}} (1) ; {{trad+|de|Rabenvogel}} {{m}} (2)
                  |* {{T|ems}} : ''dialecte chugach'' : {{trad--|ems|qanitiirpak}} ; ''dialecte koniag'' : {{trad--|ems|qalngaʼaq}}
                  |* {{T|adx}} : {{trad--|adx|kha ta}}
                  |* {{T|njm}} : {{trad--|njm|süzha}}
                  |* {{T|en}} : {{trad+|en|raven}} ; {{trad+|en|crow}}
                  |* {{T|ang}} : {{trad-|ang|crawe}}
                  |* {{T|apw}} : {{trad--|apw|gaagé}}
                  |* {{T|apk}} : {{trad--|apk|ghaal}}
                  |* {{T|ar}} : {{trad-|ar|غراب|m|tr=ghorab}}
                  |* {{T|hy}} : {{trad+|hy|ագռավ|R=agŕav}}
                  |* {{T|rup}} : {{trad--|rup|corbu}}
                  |* {{T|atj}} : {{trad--|atj|kakakiw}}
                  |* {{T|az}} : {{trad+|az|quzğun}}
                  |* {{T|dsb}} : {{trad--|dsb|rapak}}
                  |* {{T|taa}} : {{trad--|taa|dotronʼ}}
                  |* {{T|eu}} : {{trad+|eu|erroi}}
                  |* {{T|be}} : {{trad-|be|крумкач}}
                  |* {{T|bs}} : {{trad-|bs|gavran}}
                  |* {{T|pcc}} : {{trad--|pcc|dezal}}
                  |* {{T|br}} : {{trad+|br|bran}}
                  |* {{T|bg}} : {{trad+|bg|гарван}}
                  |* {{T|chl}} : {{trad--|chl|’álwet}}
                  |* {{T|mlc}} : {{trad--|mlc|tu³¹ kɑ⁴²}}
                  |* {{T|krl}} : {{trad--|krl|korppi}}, {{trad--|krl|varoi}}
                  |* {{T|ca}} : {{trad+|ca|corb}}
                  |* {{T|shy}} : {{trad--|shy|jaref}}, {{trad--|shy|tabaɣla}}
                  |* {{T|chr}} : {{trad-|chr|ᎪᎳᏅ|R=golanv}}
                  |* {{T|chy}} : {{trad--|chy|ókohke}}
                  |* {{T|cic}} : {{trad--|cic|fala}}
                  |* {{T|zh}} : {{trad+|zh|乌鸦|R=wūyā|tradi=烏鴉}}
                  |* {{T|cjs}} : {{trad--|cjs|қарға}}
                  |* {{T|ko}} : {{trad-|ko|큰까마귀|R=keunggamagwi}}, {{trad+|ko|까마귀|R=kkamagwi}}
                  |* {{T|kw}} : {{trad-|kw|bran}}
                  |* {{T|co}} : {{trad+|co|corbu|m}}
                  |* {{T|crk}} : {{trad--|crk|kahkâkiw}}
                  |* {{T|hr}} : {{trad+|hr|gavran}}
                  |* {{T|dak}} : {{trad--|dak|kȟaŋǧí}}, {{trad--|dak|khaŋǧí}}
                  |* {{T|da}} : {{trad+|da|ravn|c}}
                  |* {{T|ing}} : {{trad--|ing|yixgitsiy}}
                  |* {{T|tfn}} : ''dialecte de Upper Inlet'' : {{trad--|tfn|delgga}} ; ''dialecte d’Inland'' : {{trad--|tfn|chulyin}} ; ''dialecte d’Outer Inlet'' : {{trad--|tfn|ggugguyni}}
                  |* {{T|es}} : {{trad+|es|cuervo}}
                  |* {{T|eo}} : {{trad+|eo|korvo}}
                  |* {{T|et}} : {{trad-|et|kaaren}}
                  |* {{T|eya}} : {{trad--|eya|ch’iileh}}
                  |* {{T|fo}} : {{trad-|fo|krákufuglur}}, {{trad+|fo|ravnur}}
                  |* {{T|fi}} : {{trad+|fi|korppi}}
                  |* {{T|frp}} : {{trad--|frp|courbàss}}
                  |* {{T|fy}} : {{trad-|fy|krie}}, {{trad-|fy|raven}}
                  |* {{T|gd}} : {{trad-|gd|fitheach}} {{m}}
                  |* {{T|ga}} : {{trad-|ga|fiach}} {{m}}
                  |* {{T|gl}} : {{trad+|gl|corvo|m}}
                  |* {{T|cy}} : {{trad-|cy|cigfran}} {{f}}, {{trad+|cy|brân}}
                  |* {{T|pwg}} : {{trad--|pwg|oghogha}}
                  |* {{T|gaulois}} : {{trad--|gaulois|branos}}
                  |* {{T|ka}} : {{trad-|ka|ყორანი}}
                  |* {{T|got}} : {{trad--|got|𐌷𐍂𐌰𐌱𐌽𐍃|R=hrabns}} {{m}}
                  |* {{T|el}} : {{trad+|el|κόρακας|R=kórakas}} {{m}}
                  |* {{T|grc}} : {{trad--|grc|κόραξ|R=kórax}}
                  |* {{T|griko}} : {{trad--|griko|kràvulo}}
                  |* {{T|gwi}} : {{trad--|gwi|deetrinʼ}}
                  |* {{T|hai}} : {{trad--|hai|yáahl}}
                  |* {{T|haa}} : {{trad--|haa|tätrą̀ʼ}}
                  |* {{T|hsb}} : {{trad-|hsb|rapak}}
                  |* {{T|tau}} : {{trad--|tau|taatsaanʼ}}
                  |* {{T|yuf-hav}} : {{trad--|yuf-hav|qsaq}}
                  |* {{T|haw}} : {{trad--|haw|kolaka}}
                  |* {{T|he}} : {{trad-|he|עורב שחור}}
                  |* {{T|hbo}} : {{trad--|hbo|ערב|dif=עֹרֵב}} {{m}}
                  |* {{T|hoi}} : {{trad--|hoi|yixgitsiy}}
                  |* {{T|hu}} : {{trad+|hu|holló}}
                  |* {{T|hop}} : {{trad--|hop|angwusi}}
                  |* {{T|sah}} : {{trad--|sah|суор}}
                  |* {{T|io}} : {{trad+|io|korvo}}, {{trad+|io|korbelo}}
                  |* {{T|ifk}} : {{trad--|ifk|gawwang}}
                  |* {{T|id}} : {{trad+|id|gagak}}, {{trad+|id|burung gagak}}
                  |* {{T|inh}} : {{trad--|inh|хьаргӏа}}
                  |* {{T|ikt}} : {{trad--|ikt|tulugaq}}
                  |* {{T|iu}} : {{trad-|iu|ᑐᓗᒐᖅ|R=[[tulugaq]]}}
                  |* {{T|ik}} : {{trad--|ik|tulugaq}}
                  |* {{T|is}} : {{trad+|is|hrafn}}
                  |* {{T|it}} : {{trad+|it|corvo}}
                  |* {{T|ja}} : {{trad+|ja|鴉|R=karasu}}
                  |* {{T|kab}} : {{trad--|kab|agerfiw}}, {{trad--|kab|tagerfa}}
                  |* {{T|kl}} : {{trad-|kl|tuluaq}}, {{trad-|kl|tulugaq}}
                  |* {{T|kls}} : {{trad--|kls|krụ̃}}
                  |* {{T|fla}} : {{trad--|fla|scaʔáʔ}}
                  |* {{T|kjb}} : {{trad--|kjb|joj}}
                  |* {{T|kn}} : {{trad-|kn|ಕಾರ್ಗೊರಲಕಾಗೆ}}
                  |* {{T|krc}} : {{trad--|krc|къаргъа}}
                  |* {{T|kkz}} : {{trad--|kkz|mésg}}
                  |* {{T|ktw}} : {{trad--|ktw|da teañʼ}}
                  |* {{T|kk}} : {{trad-|kk|қарға|tr=qarğa}}
                  |* {{T|kee}} : {{trad--|kee|sč̓ír̓á}}
                  |* {{T|kjq}} : {{trad--|kjq|š̓úta}}
                  |* {{T|kjh}} : {{trad--|kjh|хусхун}}
                  |* {{T|kg}} : {{trad--|kg|kwakwa}}
                  |* {{T|kuu}} : {{trad--|kuu|dotronʼ}}
                  |* {{T|kv}} : {{trad--|kv|рака}}
                  |* {{T|avk}} : {{trad--|avk|razama}}
                  |* {{T|koy}} : {{trad--|koy|dotsonʼ}}
                  |* {{T|kus}} : {{trad--|kus|gãaʋk}}
                  |* {{T|lld}} : ''dialecte Cadorino'' : {{trad--|lld|crò}} ; ''dialecte Fassano'' : {{trad--|lld|corf}}
                  |* {{T|lkt}} : {{trad--|lkt|kȟaŋǧí}}
                  |* {{T|gmq}} : {{trad--|gmq|ᚺᚨᚱᚨᛒᚨᚾᚨᛉ|tr=harabanaz}}
                  |* {{T|la}} : {{trad-|la|corax}}, {{trad-|la|corvus}}
                  |* {{T|lv}} : {{trad-|lv|krauklis}}
                  |* {{T|lt}} : {{trad+|lt|kranklys}}
                  |* {{T|lnd}} : {{trad--|lnd|kook}}
                  |* {{T|lut}} : {{trad--|lut|kʼákʼaʔ}}
                  |* {{T|lb}} : {{trad-|lb|ramm}}
                  |* {{T|mk}} : {{trad-|mk|гавран}}
                  |* {{T|mg}} : {{trad+|mg|goaika}}, {{trad+|mg|goaky}}, {{trad+|mg|voronkahaka}}
                  |* {{T|mt}} : {{trad-|mt|għarab}}
                  |* {{T|gv}} : {{trad-|gv|feeagh}}, {{trad-|gv|feeagh mooar}}
                  |* {{T|msm}} : {{trad--|msm|uwak}}
                  |* {{T|mns}} : {{trad--|mns|хулах}}
                  |* {{T|mwl}} : {{trad--|mwl|cuorbo}}
                  |* {{T|mn}} : {{trad-|mn|хар хэрээ|tr=khar kheree}}, {{trad+|mn|хэрээ|tr=kheree}}
                  |* {{T|moe}} : {{trad--|moe|kakatshu}}
                  |* {{T|nci}} : {{trad--|nci|cacalōtl}}
                  |* {{T|nsk}} : {{trad--|nsk|ᑲᑲᒍᐤ‎|R=[[kaahkaachuw]]}}
                  |* {{T|nv}} : {{trad--|nv|zhį́ʼii}}
                  |* {{T|nl}} : {{trad+|nl|kraai}}, {{trad+|nl|raaf}}
                  |* {{T|niy}} : {{trad--|niy|àkʉ̌rʉ̀}}
                  |* {{T|nog}} : {{trad--|nog|карга}}
                  |* {{T|nol}} : {{trad--|nol|kaˑk}}
                  |* {{T|no}} : {{trad+|no|ravn}}
                  |* {{T|oc}} : {{trad+|oc|còrb}}
                  |* {{T|one}} : {{trad--|one|ká·ka}}
                  |* {{T|osa}} : {{trad--|osa|hkáɣe}}
                  |* {{T|pwn}} : {{trad--|pwn|qaqa}}
                  |* {{T|pwi}} : ''dialectes de Kabalmem Hill et du river patwin :'' {{trad--|pwi|kaˑk}} ; ''dialecte de Kabalmem Hill :'' {{trad--|pwi|koˑk}}
                  |* {{T|pcd}} : {{trad--|pcd|corbiau}}, {{trad--|pcd|cornalle}}
                  |* {{T|pdt}} : {{trad--|pdt|Rowe}}
                  |* {{T|poitevin-saintongeais}} : {{trad--|poitevin-saintongeais|grole}}
                  |* {{T|pl}} : {{trad+|pl|kruk}}
                  |* {{T|caf}} : {{trad--|caf|datsancho}} (1), {{trad--|caf|datsan}} (2)
                  |* {{T|pt}} : {{trad+|pt|corvo}}
                  |* {{T|qua}} : {{trad--|qua|kkáxe}}
                  |* {{T|rm}} : {{trad--|rm|corv}}
                  |* {{T|ro}} : {{trad+|ro|corb}}
                  |* {{T|ru}} : {{trad+|ru|ворон|R=voron}}
                  |* {{T|sia}} : {{trad--|sia|karnes}}
                  |* {{T|sjd}} : {{trad--|sjd|ка̄рьнэсь}}
                  |* {{T|smj}} : {{trad--|smj|gárránis}}
                  |* {{T|sjt}} : {{trad--|sjt|karnas}}
                  |* {{T|smn}} : {{trad--|smn|káránâs}}
                  |* {{T|se}} : {{trad--|se|garjá}}, {{trad--|se|gáranas}}
                  |* {{T|sma}} : {{trad--|sma|gaarenes}}, {{trad--|sma|goerhpe}}, {{trad--|sma|krungke}}
                  |* {{T|sju}} : {{trad--|sju|gaarànis}}
                  |* {{T|sms}} : {{trad--|sms|käärnõs}}
                  |* {{T|sa}} : {{trad+|sa|वायस}}
                  |* {{T|sco}} : {{trad--|sco|corbie}}
                  |* {{T|xsr}} : {{trad--|xsr|कलक}}
                  |* {{T|zdj}} : {{trad--|zdj|gawa}}
                  |* {{T|sk}} : {{trad+|sk|havran}}
                  |* {{T|sl}} : {{trad+|sl|krokar}}
                  |* {{T|ses}} : {{trad--|ses|gaaru-gaaru}}
                  |* {{T|sv}} : {{trad+|sv|korp}}
                  |* {{T|sw}} : {{trad+|sw|kunguru}}
                  |* {{T|tg}} : {{trad+|tg|зоғ}}
                  |* {{T|ta}} : {{trad+|ta|காக்கை|R=kākkai}}
                  |* {{T|tcb}} : {{trad--|tcb|taatsą́ąʼ}}
                  |* {{T|crh}} : {{trad--|crh|quzğun}}
                  |* {{T|cs}} : {{trad+|cs|havran}}, {{trad+|cs|krkavec}}
                  |* {{T|ckt}} : {{trad--|ckt|валвийӈын}}
                  |* {{T|cv}} : {{trad--|cv|çăхăн}}
                  |* {{T|tli}} : {{trad--|tli|yéil}}
                  |* {{T|tyv}} : {{trad--|tyv|кускун}}
                  |* {{T|tsi}} : {{trad--|tsi|g̱aax}}
                  |* {{T|tr}} : {{trad+|tr|kuzgun}}
                  |* {{T|bvy}} : {{trad--|bvy|uyák}}
                  |* {{T|vi}} : {{trad+|vi|quạ}}
                  |* {{T|goh}} : {{trad--|goh|raban}}
                  |* {{T|non}} : {{trad--|non|hrafn}}
                  |* {{T|orv}} : {{trad--|orv|воронъ|R=voronŭ}}
                  |* {{T|wnw}} : {{trad--|wnw|qaˑqat}}
                  |* {{T|esu}} : {{trad--|esu|tulukaruk}} ; ''dialecte de Nunivak :'' {{trad--|esu|tulukarug}}
                  |* {{T|zun}} : {{trad--|zun|kʼwalashi}}
                  |{{trad-fin}}
                  |
                  |{{trad-début|Auteur de lettres ou de coups de téléphone anonymes}}
                  |* {{T|en}} : {{trad-|en|poison pen letter writer}}
                  |* {{T|nl}} : {{trad+|nl|klokkenluider}}
                  |{{trad-fin}}
                  |
                  |{{trad-début|Élément en saillie d’un mur}}
                  |* {{T|de}} : {{trad+|de|Kragstein}}
                  |* {{T|en}} : {{trad-|en|bracket-corbel}}
                  |* {{T|it}} : {{trad+|it|mensola}}
                  |{{trad-fin}}
                  |
                  |{{trad-début|Espèce de grappin d’abordage et de pont volant}}
                  |* {{T|de}} : {{trad-|de|Corvus}}
                  |* {{T|en}} : {{trad+|en|corvus}}, {{trad+|en|harpago}}
                  |* {{T|es}} : {{trad-|es|corvus}}
                  |* {{T|it}} : {{trad+|it|corvo}}
                  |* {{T|nl}} : {{trad+|nl|corvus}}
                  |* {{T|no}} : {{trad-|no|corvus}}
                  |* {{T|pt}} : {{trad-|pt|corvus}}
                  |* {{T|sr}} : {{trad-|sr|корвус}}
                  |{{trad-fin}}
                  |
                  |=== {{S|adjectif|fr}} ===
                  |{{fr-inv|kɔʁ.bo}}
                  |[[Image:Deepika Padukone (face).jpg|vignette|Des cheveux '''corbeau'''.]]
                  |'''corbeau''' {{pron|kɔʁ.bo|fr}} {{mf}} {{invar}}
                  |# Qui est du [[noir]] le plus [[sombre]], avec parfois des [[reflet]]s [[bleuté]]s. {{couleur|#000000}}
                  |#* ''Un feutre assorti à la robe, crânement relevé, couronne ses cheveux '''corbeau''' coiffés en arrière, dégageant le front lisse.'' {{source|{{w|Yvonne de Bremond d’Ars}}, ''Le Journal d’une antiquaire : Le Marquis de Caracas'', page 171, 1967, Hachette}}
                  |#* ''[…] d’étonnantes dates, Lourdes, Fatima, Maria Goretti, reliées aux chapeaux '''corbeau''' des vieux curés plutôt qu’au petit béret des vicaires motocyclistes.'' {{source|''La Parisienne'', journal, page 432, janvier 1954}}
                  |
                  |==== {{S|synonymes}} ====
                  |* [[noir corbeau]]
                  |* [[aile de corbeau]]
                  |* [[bleu corbeau]]
                  |
                  |=== {{S|prononciation}} ===
                  |* {{pron|kɔʁ.bo|fr}}
                  |** {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|kɔʁ.bo|audio=Fr-corbeau.ogg}}
                  |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Ltrlg-corbeau.wav}}
                  |
                  |=== {{S|anagrammes}} ===
                  |* [[caroube]]
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{thésaurus|fr|corbeau}}
                  |* {{WP|Grand corbeau}}
                  |* {{WP|Corbeau (système d'abordage)}}
                  |* {{WP|Aile de corbeau|Aile de corbeau}}
                  |* {{thésaurus|fr|couleur}}
                  |
                  |=== {{S|références}} ===
                  |* {{Import:DAF8}}
                  |* {{Import:Littré}}
                  |* Jean de Vigan, ''Le petit Dicobat'', éd. Arcature, Paris, 2003.
                  |* {{R:Morisot1814}}
                  |* {{R:TLFi|corbeau}}
                  |* [http://www.pourquois.com/francais/pourquoi-auteur-lettre-anonyme-est-corbeau.html Source]
                  |
                  |<references>
                  |{{R|plantgrape|[http://plantgrape.plantnet-project.org/cepage/Corbeau%20N plantgrape.plantnet-project.org]}}
                  |</references>
                  |
                  |[[Catégorie:Corbeaux en français]]
                  |[[Catégorie:Couleurs en français]]
                  |
                  |{{Bonne entrée|français}}""".stripMargin

}
