package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Dabord {
  val resource = Resource("d’abord","https://fr.wiktionary.org/wiki/","entry","""== {{langue|fr}} ==
                                                                                     |=== {{S|étymologie}} ===
                                                                                     |: {{compos|m=1|lang=fr|de|abord}}.
                                                                                     |
                                                                                     |=== {{S|adverbe|fr|locution=oui}} ===
                                                                                     |'''d’abord''' {{pron|d‿a.bɔʁ|fr}}
                                                                                     |# {{littéraire|fr}} Lors du [[premier]] [[contact]].
                                                                                     |#* ''Jamalou, le concierge, s’apercevant que la voûte se trouvait éclairée, alla se rendre compte du fait et ne vit rien '''d’abord''' qui l’étonnât.'' {{source|{{Citation/Francis Carco/L’Homme de minuit/1938}}}}
                                                                                     |# {{littéraire|fr}} Dès que.
                                                                                     |#* '' Dans un étroit sentier, un étranger, qu’à son costume on aurait pris pour un chasseur, rencontre le vinaigrier ; et '''d’abord''' qu’il voit l’âne, il éclate de rire.'' {{source|{{w|Prosper Mérimée}}, ''Lettres d’Espagne'', 1832, rééd. Éditions Complexe, 1989, page 100}}
                                                                                     |# Au [[commencement]] ; [[premièrement]].
                                                                                     |#* '''''D'abord''' réservées aux scientifiques, les Pyrénées ont ensuite accueilli un tourisme prudent en attendant les pyrénéistes, ivres de conquêtes.'' {{source|André Lasserre, ''Petite histoire de la Bigorre'', Éditions Cairn, 2015}}
                                                                                     |#* ''Nous aborderons '''d’abord''' les aspects théoriques, puis nous passerons à la pratique.''
                                                                                     |# {{par ext}} {{populaire|fr}} [[déjà|Déjà]] ; premièrement — {{note}} Il s’utilise alors pour donner de l’importance à une affirmation.
                                                                                     |#* ''Moi, '''d’abord''', je n’essaie pas de truander.''
                                                                                     |# [[prioritairement|Prioritairement]].
                                                                                     |#* ''Les femmes et les enfants '''d’abord'''.''
                                                                                     |# [[auparavant|Auparavant]]. — {{note}} Il introduit une [[précondition]].
                                                                                     |#* ''On ne s’improvise pas usurier, il faut parvenir '''d’abord''' à une méritoire insertion dans la mafia des gens bien placés pour exploiter leur prochain.'' {{source|Jean-Paul Desaive, ''Délits sexuels et archives judiciaires (1690-1750)'', Communications, 1987, vol.46, {{n°|46}}, p.121}}
                                                                                     |# [[principalement|Principalement]].
                                                                                     |#* ''Dès l’instant où, à l’occasion de cette réédition, je me suis retourné vers les premières pages que j’ai écrites, c’est cela '''d’abord''', que j’ai eu envie de consigner ici.'' {{source|{{nom w pc|Albert|Camus}}, ''L’Envers et l’Endroit'', Gallimard, 1958, préface, p.33}}
                                                                                     |# {{QC|fr}} {{familier|fr}} Puisque c’est comme ça.
                                                                                     |#* ''Eh bien, je vais y aller '''d’abord'''.''
                                                                                     |
                                                                                     |==== {{S|dérivés}} ====
                                                                                     |{{(}}
                                                                                     |* [[d’abord et avant tout]]
                                                                                     |* [[d’abord et d’une]]
                                                                                     |* [[d’abord que]]
                                                                                     |* [[frappe-d’abord]]
                                                                                     |* [[tout d’abord]]
                                                                                     |{{)}}
                                                                                     |
                                                                                     |==== {{S|traductions}} ====
                                                                                     |{{trad-début|Lors du premier contact}}
                                                                                     |* {{T|de}} : {{trad+|de|zuerst}}, {{trad+|de|zunächst}}, {{trad+|de|anfangs}}, {{trad+|de|Anfang|dif=am Anfang}}
                                                                                     |* {{T|en}} : {{trad+|en|at first}}
                                                                                     |* {{T|es}} : {{trad-|es|en un principio}}
                                                                                     |* {{T|ru}} : {{trad+|ru|сначала|R=snachala}}
                                                                                     |* {{T|se}} : {{trad--|se|vuos}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Au commencement, premièrement}}
                                                                                     |* {{T|de}} : {{trad+|de|zuerst}}
                                                                                     |* {{T|en}} : {{trad+|en|at first}}
                                                                                     |* {{T|az}} : {{trad-|az|qabaqca}}, {{trad-|az|əvvəlcə}}, {{trad-|az|əvvəla}}
                                                                                     |* {{T|es}} : {{trad+|es|primeramente}}
                                                                                     |* {{T|fi}} : {{trad+|fi|ensin}}
                                                                                     |* {{T|vls}} : {{trad--|vls|aistn}}
                                                                                     |* {{T|gallo}} : {{trad--|gallo|d’abord}}
                                                                                     |* {{T|swb}} : {{trad--|swb|rabuzi}}
                                                                                     |* {{T|poitevin-saintongeais}} : {{trad--|poitevin-saintongeais|d’abord}}, {{trad--|poitevin-saintongeais|déjha}}
                                                                                     |* {{T|ru}} : {{trad+|ru|сначала|R=snachala}}
                                                                                     |* {{T|se}} : {{trad--|se|vuos}}, {{trad--|se|álggos}}
                                                                                     |* {{T|sw}} : {{trad+|sw|kwanza}}
                                                                                     |* {{T|tr}} : {{trad+|tr|önce}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Déjà, premièrement}}
                                                                                     |* {{T|de}} : {{trad+|de|dagegen}}
                                                                                     |* {{T|en}} : {{trad+|en|first of all}}
                                                                                     |* {{T|gallo}} : {{trad--|gallo|d’abord}}
                                                                                     |* {{T|poitevin-saintongeais}} : {{trad--|poitevin-saintongeais|d’abord}}, {{trad--|poitevin-saintongeais|déjha}}
                                                                                     |* {{T|se}} : {{trad--|se|vuos}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Prioritairement}}
                                                                                     |* {{T|de}} : {{trad+|de|zuerst}}
                                                                                     |* {{T|en}} : {{trad+|en|first}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Auparavant}}
                                                                                     |* {{T|de}} : {{trad+|de|zuerst}}, {{trad+|de|zunächst}}
                                                                                     |* {{T|en}} : {{trad+|en|first}}
                                                                                     |* {{T|zdj}} : {{trad--|zdj|rangu}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Principalement}}
                                                                                     |* {{T|en}} : {{trad+|en|mainly}} ; {{trad+|en|principally}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Puisque c’est comme ça}}
                                                                                     |* {{T|de}} : {{trad+|de|zuerst}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |{{trad-début|Traductions à trier suivant le sens}}
                                                                                     |* {{T|he|trier}} : {{trad-|he|קודם|R=qodem}}
                                                                                     |* {{T|ja|trier}} : {{trad+|ja|まず|R=mazu}}, {{trad-|ja|始めに|R=hajimeni}}
                                                                                     |* {{T|ses|trier}} : {{trad--|ses|jina}}
                                                                                     |{{trad-fin}}
                                                                                     |
                                                                                     |=== {{S|prononciation}} ===
                                                                                     |* {{écouter|lang=fr|France|d‿a.bɔʁ}}
                                                                                     |* {{écouter|lang=fr|Québec {{soutenu|nocat=1}}|d‿a.bɔʁ}}
                                                                                     |* {{écouter|lang=fr|Québec {{populaire|nocat=1}}|d‿a.bɑɔ̯ʁ}}
                                                                                     |* {{écouter|lang=fr|Québec {{familier|nocat=1}}|d‿ə.bɑɔ̯ʁ}} ''(définition 3 et 7)''
                                                                                     |* {{écouter|lang=fr|Québec {{familier|nocat=1}}|d‿ə.bɔʁ}} ''(définition 3 et 7)''
                                                                                     |
                                                                                     |{{clé de tri|dabord}}
                                                                                     |
                                                                                     |== {{langue|gallo}} ==
                                                                                     |=== {{S|étymologie}} ===
                                                                                     |: {{ébauche-étym|gallo}}
                                                                                     |
                                                                                     |=== {{S|adverbe|gallo|locution=oui}} ===
                                                                                     |{{gallo-inv}}
                                                                                     |'''d’abord''' {{pron||gallo}} {{gallo-graphie ABCD}}
                                                                                     |# [[d’abord#fr|D’abord]], [[a priori]], [[premièrement]], [[primo]].
                                                                                     |
                                                                                     |=== {{S|références}} ===
                                                                                     |* {{R:PMatao|p=70}}
                                                                                     |== {{langue|poitevin-saintongeais}} ==
                                                                                     |
                                                                                     |=== {{S|étymologie}} ===
                                                                                     |: {{ébauche-étym|poitevin-saintongeais}}
                                                                                     |
                                                                                     |=== {{S|adverbe|poitevin-saintongeais|locution=oui}} ===
                                                                                     |'''d’abord''' {{pron||poitevin-saintongeais}} {{poit-saint-normalisé}}
                                                                                     |# [[d’abord#fr|D’abord]].
                                                                                     |
                                                                                     |=== {{S|références}} ===
                                                                                     |* {{R:Dicopoitsaint|abord|consulté le=9 avril 2019}}""".stripMargin)

}
