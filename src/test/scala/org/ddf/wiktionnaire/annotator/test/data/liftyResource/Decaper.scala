/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Decaper {

  val resource = Resource("decaper", "https://fr.wiktionary.org/wiki/","entry","""== {{langue|fr}} ==
                                                                                       |=== {{S|étymologie}} ===
                                                                                       |:{{term|verbe 1}} {{composé de|dé-|cape|-er|lang=fr}}, la crasse étant assimilée à une cape, une couverture qu’il faut ôter.
                                                                                       |:{{term|verbe 2}} {{composé de|dé-|cap|-er|lang=fr}}.
                                                                                       |
                                                                                       |=== {{S|verbe|fr|num=1}} ===
                                                                                       |{{illustration souhaitable}}
                                                                                       |'''décaper''' {{t|fr}} {{pron|de.ka.pe|fr}} {{conj|grp=1|fr}}
                                                                                       |# {{lexique|art|fr}} [[débarrasser|Débarrasser]] un [[métal]] de la [[rouille]], de l’[[oxyde]] qui s’est formé à sa [[surface]].
                                                                                       |#* ''C'est cette industrie privée qui fondra les métaux , les alliera entre eux , les laminera , les '''décapera''' , les découpera, etc. , et la monnaie de Paris viendra seulement leur imprimer le sceau de l’État; c'est cependant de la fonte et de toutes les opérations qui la suivent que dépend l'identité de la monnaie.'' {{source|{{w|Jean-François Terme}}, ''Discours sur les monnaies: prononcé dans la séance de la chambre des députés du 1{{er}} juin 1843'', p. 15}}
                                                                                       |#* ''Quelquefois on '''décape''' l’argent à l'acide azotique, mais il faut employer de l’acide bien exempt de chlore, qui formerait du chlorure d’argent à la surface de l’objet. Le zinc '''se décape''' assez bien lorsqu'il n'est pas accompagné d’étain ou de plomb.'' {{source|A. De Plazanet, « ''Hydroplastie (Électro-chimie - Galvanoplastie) ''», chap. 38 des ''Études sur l'Exposition de 1867 ou Les archives de l'Industrie au XIX{{e}} siècle : description générale, encyclopédique, méthodique et raisonnée de l’État actuel des arts des sciences de l'industrie et de l'agriculture chez toutes les nations'', édité par Eugène Lacroix, Paris, 1867, p. 423}}
                                                                                       |# {{par ext}} Débarrasser la [[surface]] d'un [[objet]] de sa [[couche]] [[supérieur]]e.
                                                                                       |#* ''Au fond, dans la pénombre, Bern était accroupi et '''décapait''' un vieux vaisselier au chalumeau. Des couches de peinture faisaient des cloques noires sous la flamme bleue. L'odeur était un mélange de bois carbonisé et de métal.'' {{source|Peter Temple, ''Séquelles'', traduit de l'anglais (Australie) par Mireille Vignol, Éditions Gallimard, 2016}}
                                                                                       |#* ''De gros scrapers '''décapaient''' le sol et stockaient les roches, d'autres ratissaient et aplanissaient la surface dégagée pour délimiter nettement le trou circulaire que les ingénieurs avaient décidé de creuser pour atteindre le minerai.'' {{source|Jean Darmen, ''P900'', tome 3: ''Les Pirates'', Numeriklivres, 2016, chap. 4}}
                                                                                       |# [[attaquer|Attaquer]] [[violemment]] un [[organe]].
                                                                                       |#* ''Ici on fume sans filtre, sans ''Tampax'' comme disent les puristes, la règle est de rechercher le contact direct de la langue sur le tabac noir qui '''décape''' les bronches, à condition de cloper vraiment, de ne pas crapoter comme les gonzesses […].'' {{source|Philippe Delepierre, ''Sous les pavés l'orage'', éd. Liana Levi,, 2008, page 145}}
                                                                                       |#* ''Rozan prévint son ami et tous deux se ruèrent sur la douche afin d'éviter que cet étonnant produit ne leur '''décapât''' complètement la peau !'' {{source|Philippe Durant, ''[[w:Jean-Paul Belmondo|Belmondo]]'', nouvelle édition revue & augmentée, éd. Robert Laffont, 2011}}
                                                                                       |
                                                                                       |==== {{S|dérivés}} ====
                                                                                       |* {{lien|décapage|fr}}
                                                                                       |* {{lien|décapeur|fr}}
                                                                                       |* {{lien|décapeuse|fr}}
                                                                                       |
                                                                                       |==== {{S|traductions}} ====
                                                                                       |{{trad-début|Débarrasser un métal de la rouille}}
                                                                                       |* {{T|de}} : {{trad+|de|entrosten}}
                                                                                       |* {{T|en}} : {{trad+|en|scour}}, {{trad+|en|strip}}
                                                                                       |* {{T|hr}} : {{trad-|hr|istrugati}}, {{trad+|hr|otapati}}, {{trad-|hr|spaliti}}
                                                                                       |* {{T|el}} : {{trad+|el|ξεσκουριάζω}}
                                                                                       |* {{T|pt}} : {{trad-|pt|decapar}}
                                                                                       |* {{T|se}} : {{trad--|se|beiset}}
                                                                                       |* {{T|cs}} : {{trad+|cs|brousit}}
                                                                                       |{{trad-fin}}
                                                                                       |
                                                                                       |=== {{S|verbe|fr|num=2}} ===
                                                                                       |'''décaper''' {{t|fr}} {{pron|de.ka.pe|fr}} {{conj|grp=1|fr}}
                                                                                       |# {{lexique|marine|fr}} Passer un [[cap]], sortir d’une baie, d’un golfe, en dedans duquel on naviguait.
                                                                                       |#* {{ébauche-exe|fr}}
                                                                                       |
                                                                                       |=== {{S|références}} ===
                                                                                       |*{{R:TLFi}}
                                                                                       |*{{Import:DAF8}}
                                                                                       |""".stripMargin)

}
