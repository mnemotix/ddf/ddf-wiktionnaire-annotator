/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextSolvions {
  val solvions = """=== {{S|verbe|fr|flexion|num=1}} ===
                   |{{fr-verbe-flexion|grp=3|soudre|ind.i.1p=oui|sub.p.1p=oui}}
                   |'''solvions''' {{pron|sɔl.vjɔ̃|fr}}
                   |# ''Première personne du pluriel de l’imparfait de'' [[soudre]].
                   |# ''Première personne du pluriel du présent du subjonctif de'' [[soudre]].
                   |
                   |=== {{S|verbe|fr|flexion|num=2}} ===
                   |{{fr-verbe-flexion|solver|ind.i.1p=oui|sub.p.1p=oui}}
                   |'''solvions''' {{pron|sɔl.vjɔ̃|fr}}
                   |# ''Première personne du pluriel de l’imparfait de l’indicatif de'' [[solver]].
                   |# ''Première personne du pluriel du présent du subjonctif de'' [[solver]].""".stripMargin

  val solvionsRes = """== {{langue|fr}} ==
                      |=== {{S|verbe|fr|flexion|num=1}} ===
                      |{{fr-verbe-flexion|grp=3|soudre|ind.i.1p=oui|sub.p.1p=oui}}
                      |'''solvions''' {{pron|sɔl.vjɔ̃|fr}}
                      |# ''Première personne du pluriel de l’imparfait de'' [[soudre]].
                      |# ''Première personne du pluriel du présent du subjonctif de'' [[soudre]].
                      |
                      |=== {{S|verbe|fr|flexion|num=2}} ===
                      |{{fr-verbe-flexion|solver|ind.i.1p=oui|sub.p.1p=oui}}
                      |'''solvions''' {{pron|sɔl.vjɔ̃|fr}}
                      |# ''Première personne du pluriel de l’imparfait de l’indicatif de'' [[solver]].
                      |# ''Première personne du pluriel du présent du subjonctif de'' [[solver]].
                      |""".stripMargin
}
