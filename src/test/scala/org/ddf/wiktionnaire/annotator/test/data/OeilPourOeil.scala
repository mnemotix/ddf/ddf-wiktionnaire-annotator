package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object OeilPourOeil {
 val oeilPourOeil = """== {{langue|fr}} ==
                      |=== {{S|étymologie}} ===
                      |: ''[[loi du talion|Loi du ''talion'']] ; du latin ''[[talis]]'' (« tel ») : telle la faute, tel le châtiment.
                      |: Résumé très simplifié des préceptes des chapitres XXI et XXII du livre de l’Exode. S’exprime le plus nettement au § XXI.22 : « Quand des hommes se disputent une femme enceinte, si le fœtus sort et que la femme survit, une amende sera exigée selon ce que demandera le mari de la femme, et le coupable paiera au taux usuel. Mais si la femme meurt, tu paieras '''œil pour œil, dent pour dent''', main pour main, pied pour pied, brûlure pour brûlure, blessure pour blessure, plaie pour plaie. » Voir également Lévitique, XXIV, 20 et Deutéronome, XIX, 21)
                      |
                      |=== {{S|locution-phrase|fr}} ===
                      |'''œil pour œil, dent pour dent''' {{pron|œj puʁ œj dɑ̃ puʁ dɑ̃|fr}}
                      |# [[précepte|Précepte]] de la [[peine]] du [[talion]] qui [[consiste]] à [[traiter]] un [[coupable]] de la même [[façon]] qu’il a traité ou voulu traiter les [[autre]]s.
                      |#* ''Ainsi les Sadducéens admettaient à la lettre les dispositions pénales édictées par le Pentateuque, au sujet des blessures corporelles : '''œil pour œil, dent pour dent'''. Ils acquirent, de la sorte, une réputation de sévérité excessive dans l’exercice du droit pénal, tandis que les Pharisiens, invoquant dans ces cas des interprétations traditionnelles, usaient d’indulgence et se bornaient à imposer des indemnités pécuniaires.'' {{source|Hirsch Graëtz, ''[[s:Histoire des Juifs|Histoire des Juifs]]'', Deuxième période, Chapitre X. Les institutions et les sectes}}
                      |#* ''Ces blessures que vous me voyez, qui me les a faites ? le Cœur-Loyal ! Nous sommes ennemis, sa mère est en mon pouvoir, je pourrais de suite l’attacher au poteau des tortures, ce serait mon droit. […]. La loi des prairies dit '''œil pour œil, dent pour dent''', ….'' {{source|{{Citation/Gustave Aimard/Les Trappeurs de l’Arkansas/1858}}}}
                      |#* ''« '''Œil pour œil, dent pour dent'''. » Le désir de vengeance est une cause majeure de la violence.'' {{source|{{nom w pc|Mathieu|Ricard}}, ''Plaidoyer pour l'altruisme'', NiL, Paris, 2013, p. 391}}
                      |
                      |==== {{S|notes}} ====
                      |: Dans le sens courant, cette locution est prise dans le sens d’une justice impitoyable, alors que les commentateurs de la Bible insistent sur le sens modérateur qu’avait l’expression en contexte : il s’agissait de limiter la portée de la vengeance, de fonder une [[proportionnalité]] des peines, par opposition à la coutume précédente : « Caïn sera vengé sept fois, et Lémec soixante-dix-sept fois. » (Ge 4:24).
                      |
                      |==== {{S|synonymes}} ====
                      |* [[œil pour œil]] {{ellipse|nocat=1}}
                      |
                      |==== {{S|vocabulaire}} ====
                      |* [[talion]]
                      |
                      |==== {{S|antonymes}} ====
                      |*[[tendre l'autre joue]]
                      |
                      |==== {{S|traductions}} ====
                      |{{trad-début|}}
                      |* {{T|de}} : {{trad+|de|Auge um Auge, Zahn um Zahn}}
                      |* {{T|en}} : {{trad+|en|an eye for an eye, a tooth for a tooth}}
                      |* {{T|ar}} : {{trad-|ar|العين بالعين و السن بالسن والأذن بالأذن و الحرمات قصاص|tr=El-Ainou bi elaini wa es-sinou bi es-sini wa el-oudhounou bi el-oudhouni w el-houroumatou qissass}}
                      |* {{T|eu}} : {{trad+|eu|begia begiagatik, hortza hortzagatik}}
                      |* {{T|ca}} : {{trad-|ca|ull per ull, dent per dent}}
                      |* {{T|zh}} : {{trad-|zh|以眼还眼，以牙还牙 (Yǐ yǎn hái yǎn, yǐyáháiyá)}}
                      |* {{T|ko}} : {{trad-|ko|눈에는 눈, 이에는 이|tr=nuneneun nun, ieneun i}}
                      |* {{T|es}} : {{trad-|es|ojo por ojo, diente por diente}}
                      |* {{T|eo}} : {{trad-|eo|okulon pro okulo, denton pro dento}}
                      |* {{T|fi}} : {{trad-|fi|silmä silmästä, hammas hampaasta}}
                      |* {{T|it}} : {{trad-|it|occhio per occhio, dente per dente}}
                      |* {{T|ja}} : {{trad+|ja|目には目を、歯には歯を|tr=me ni wa me o, ha ni wa ha o}}
                      |* {{T|la}} : {{trad-|la|oculum pro oculo dentem pro dente}}
                      |* {{T|nb}} : {{trad-|nb|øye for øye, tann for tann}}
                      |* {{T|pl}} : {{trad+|pl|oko za oko, ząb za ząb}}
                      |* {{T|ro}} : {{trad-|ro|ochi pentru ochi si dinte pentru dinte|m}}
                      |* {{T|ru}} : {{trad-|ru|око за око, зуб за зуб|tr=Óko za óko, zub zá zub}}
                      |* {{T|sv}} : {{trad+|sv|öga för öga, tand för tand}}
                      |* {{T|cs}} : {{trad-|cs|oko zo oko, zub za zub}} ; {{ellipse|nocat=1}} {{trad-|cs|zub za zub}}
                      |* {{T|tr}} : {{trad-|tr|dişe diş göze göz}}, {{trad+|tr|kısasa kısas}}
                      |{{trad-fin}}
                      |
                      |{{cf|lang=fr|œil pour œil}}
                      |
                      |{{clé de tri|oeil pour oeil dent pour dent}}""".stripMargin
}
