/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextJalouses {
  val jalousesRes = """== {{langue|fr}} ==
                   |=== {{S|adjectif|fr|flexion}} ===
                   |'''jalouses''' {{pron|ʒa.luz|fr}}
                   |# ''Féminin pluriel de'' [[jaloux#fr|jaloux]].
                   |
                   |=== {{S|nom|fr|flexion}} ===
                   |{{fr-rég|s=jalouse|ʒa.luz}}
                   |'''jalouses''' {{pron|ʒa.luz|fr}} {{f}}
                   |# ''Pluriel de'' [[jalouse#fr|jalouse]].
                   |
                   |=== {{S|verbe|fr|flexion}} ===
                   |{{fr-verbe-flexion|jalouser|ind.p.2s=oui|sub.p.2s=oui}}
                   |'''jalouses''' {{pron|ʒa.luz|fr}}
                   |# ''Deuxième personne du singulier du présent de l’indicatif de'' [[jalouser]].
                   |# ''Deuxième personne du singulier du présent du subjonctif de'' [[jalouser]].""".stripMargin
}
