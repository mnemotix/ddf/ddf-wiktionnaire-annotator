/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextChemineau {
  val chemineau = """== {{langue|fr}} ==
                    |=== {{S|étymologie}} ===
                    |: ''([[#fr-nom-1|Nom commun 1]])'' {{cf|lang=fr|cheminer}}.
                    |: ''([[#fr-nom-2|Nom commun 2]])'' {{cf|lang=fr|cheminée}}.
                    |: ''([[#fr-nom-3|Nom commun 3]])'' Peut-être du {{étyl|la|fr|mot=simila|tr=|sens=fleur de farine}} par l'intermédiaire du bas latin ''similellus'' puis du {{fro}} ''siminel («sorte de gateau»).
                    |
                    |=== {{S|nom|fr|num=1}} ===
                    |{{fr-rég-x|ʃə.mi.no|pron2=ʃmi.no}}
                    |'''chemineau''' {{pron|ʃə.mi.no|fr}}, {{pron|ʃmi.no|fr}} {{m}}
                    |# {{vieilli|fr}} {{désuet|fr}} [[ouvrier|Ouvrier]] qui [[parcourir|parcourt]] les [[campagne]]s.
                    |#* ''Comme le '''chemineau''' chemine, le [[trottin]] trottine.'' {{source|{{w|Germaine Acremant}}, ''Ces dames aux chapeaux verts'', Plon, 1922, collection Le Livre de Poche, page 342.}}
                    |#* ''Notre voisin, enrhumé et doux, secoué d’éternuements en série, ne manquait pas de déguiser ses cerisiers en vieux '''chemineaux''' et coiffait ses groseilliers de [[gibus]] poilus.'' {{source|{{w|Colette}}, ''Sido'', 1930, Fayard, page 35.}}
                    |#* ''Il est à moitié mort de fatigue et de froid, car il a voyagé pendant plusieurs jours à la manière d’un '''chemineau''', avec un paquet de hardes et des outils sur l’épaule.'' {{source|{{nom w pc|Léon|Frapié}}, ''La régularisation'', dans ''Les contes de la maternelle'', éditions Self, 1945, pages 81-82}}
                    |# {{vieilli|fr}} {{péjoratif|fr}} [[vagabond|Vagabond]], [[rôdeur]], qui vit de petites [[besogne]]s, d’[[aumône]]s ou de [[larcin]]s.
                    |#* ''Il ferma la porte avec fracas et ils l’entendirent distinctement dire à sa femme en se servant du mot dont on désigne les '''chemineaux''' :<br
                    |/>– C’est un roulant avec sa roulante.'' {{source|{{nom w pc|Charles-Louis|Philippe}}, ''Dans la petite ville'', 1910, réédition Plein Chant, page 91}}
                    |#* ''Dans l’antichambre, l’homme mal rasé, au veston sale, me dévisagea, comme un chien de garde soupçonneux regarde passer un '''chemineau'''.'' {{source|{{nom w pc|Maurice|Dekobra}}, ''{{w|La Madone des sleepings}}'', 1925, réédition Le Livre de Poche, page 105}}
                    |#* ''Un '''chemineau''', éconduit à la nuit tombante par notre cuisinière, refusait de s’éloigner, glissait son gourdin entre les battants de la porte d’entrée, jusqu’à l’arrivée de mon père…'' {{source|{{nom w pc||Colette}}, ''La maison de Claudine'', Hachette, 1922, coll. Livre de Poche, 1960, page 25.}}
                    |
                    |==== {{S|synonymes}} ====
                    |{{cf|SDF}}
                    |* [[roulant]]
                    |* [[roulante]]
                    |
                    |=== {{S|nom|fr|num=2}} ===
                    |'''chemineau''' {{pron|ʃə.mi.no|fr}}, {{pron|ʃmi.no|fr}} {{m}}
                    |# Autre graphie de [[cheminot]].
                    |# [[cheminée|Cheminée]] [[portatif|portative]] en [[terre cuite]].
                    |
                    |==== {{S|traductions}} ====
                    |{{trad-début}}
                    |{{trad-fin}}
                    |
                    |=== {{S|nom|fr|num=3}} ===
                    |{{fr-rég-x|ʃə.mi.no|pron2=ʃmi.no}}
                    |'''chemineau''' {{pron|ʃə.mi.no|fr}}, {{pron|ʃmi.no|fr}} {{m}}
                    |# {{lexique|cuisine|fr}}{{région|Ouest de la France}} [[gâteau|Gâteau]] à pâte lourde, compacte.
                    |# {{Rouen|fr}} Petit pain en forme de [[turban]], qui autrefois était mangé durant le [[carême]] avec du beurre. Genre d'[[échaudé]]. (Note: dans Madame Bovary, Flaubert parle d'aller manger des [[cheminot#fr-nom-2|cheminots]] à Rouen pour la fête des Rois)
                    |
                    |==== {{S|variantes orthographiques}} ====
                    |* [[cheminot#fr-nom-2|cheminot]]
                    |
                    |=== {{S|références}} ===
                    |* Claude Blum (sous la direction de), ''Les thématiques Littré : Le vocabulaire du français de province'', Éd. Garnier, 2007.
                    |* {{R:Littré}}
                    |
                    |=== {{S|prononciation}} ===
                    |
                    |* {{écouter|lang=fr|France (Saint-Maurice-de-Beynost)|audio=LL-Q150 (fra)-Benoît Prieur-chemineau.wav}}
                    |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-chemineau.wav}}
                    |
                    |
                    |
                    |==== {{S|homophones|fr}} ====
                    |* [[cheminot]]
                    |* [[cheminaux]]
                    |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Penegal-chemineau.wav}}
                    |
                    |=== {{S|références}} ===
                    |*{{Import:DAF8}}
                    |
                    |[[Catégorie:Lexique en français de l’errance]]
                    |""".stripMargin
}
