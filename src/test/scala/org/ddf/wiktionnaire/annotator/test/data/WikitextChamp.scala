/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextChamp {

  val champ = """|== {{langue|fr}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|fr|mot=campus|sens=[[terrain]] plat, [[plaine]] », « [[campagne]] cultivée, champ, terrain, [[territoire]] », {{absolument}} « le champ de Mars à Rome », « [[champ de bataille]], [[lice]], [[carrière]], [[champ d’action]], [[théâtre]]}}. Ce mot est le [[doublet]] étymologique de ''{{lien|camp|fr}}''.
                |
                |=== {{S|nom|fr|num=1}} ===
                |{{fr-rég|ʃɑ̃}}
                |[[Image:Field Hamois Belgium Luc Viatour.jpg|thumb|Un '''champ''' en été.]]
                |[[Image:AIDE HELP FRENCH WIKTIONARY Rechercher.JPG‎|thumb|Un '''champ''', élément de base d’une base de données (ici avec complètement automatique).]]
                |'''champ''' {{pron|ʃɑ̃|fr}} {{m}}
                |# {{lexique|agronomie|biogéographie|écologie|ethnobiologie|fr}} [[parcelle|Parcelle]] de [[terre]] utilisée pour l’[[agriculture]].
                |#* ''Les '''champs''' d’avoine sont, certaines années, envahis par des plantes nuisibles telles que les chardons, les sanves, moutardes sauvages, ravenelles.'' {{source|''Bulletin de la Société des lettres, sciences et arts du département de la Lozère'', 1903, page 24}}
                |#* ''Monsieur le juge, comment serait-il possible que je possédasse une vache tachetée ou pas tachetée, n’ayant ni étable pour la loger, ni '''champ''' pour la nourrir.'' {{source|{{w|Octave Mirbeau}}, ''La vache tachetée'', 1918}}
                |#* ''Là, un '''champ''' labouré n’avait pas été ensemencé ; ici, une pièce de blé était trépignée par les bêtes.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|418}}}}
                |#* ''Parmi la maigre littérature, en particulier francophone, concernant la malherbologie, il manquait une synthèse taxonomique sur la flore des '''champs''' cultivés.'' {{source|Philippe Jauzein, ''Flore des champs cultivés '', page 7, Éditions Quae, 2011}}
                |#* ''En plein '''champ''','' au milieu des champs, de la campagne.
                |## {{par analogie|fr}} [[étendue|Étendue]] de [[plante]]s non-cultivées.
                |##* ''Les '''champs''' d'algues et les zones intertidales contribuent à la qualité de l'eau, facilitent la décomposition des matières organiques et constituent des sites de frai et d’alevinage.'' {{source|« ''Examen des pêcheries dans les pays de l'OCDE : Politiques et statistiques de base'' », [[OCDE]], 2003, chap. III-20 (Japon)}}
                |## {{au pluriel}} [[campagne|Campagne]], terres [[labourable]]s, [[prés]], [[bois]], [[bruyère]]s, etc.
                |##* ''El Mouria, c’est toute la forêt : il en connaît tous les sentiers, à des lieues, les cantons à muguet, les '''champs''' de myrtilles, les placers de girolles, de pieds rouges, de charbonniers.'' {{source|{{Citation/Jean Rogissart/Passantes d’Octobre/1958|}}}}
                |##* ''Mener les vaches, les brebis aux '''champs'''.''
                |##* ''Fleurs des '''champs'''.''
                |##* ''Se promener dans les '''champs'''.''
                |## {{au pluriel}} ''Antonyme de'' [[ville]].
                |##* ''Maison des '''champs'''.''
                |##* ''Il est allé aux '''champs'''.''
                |## {{lexique|militaire|fr}} ''Ancien synonyme de'' [[campagne]].
                |# [[recul|Recul]], [[espace]].
                |#* ''Se donner du '''champ''','' donner ou se donner de l’espace.
                |## {{par ext}} [[domaine|Domaine]] d’[[étude]], espace [[virtuel]].
                |##* ''En proposant le passage, incessant, d'un discours ordinaire au '''champ''' de la science, en devenant un « agitateur de mots », le vulgarisateur se donne un joli rôle : celui de l’intermédiaire, du « passeur de mots ».'' {{source|Sabine Pétillon-Boucheron, ''Les détours de la langue : étude sur la parenthèse et le tiret double'', page 299, éditions Peeters, 2002}}
                |##* ''[…], néanmoins l'émergence des premières sociétés à caractère compagnonnique se situe bien dans ce '''champ''' de l'histoire où le triptyque corporations-cathédrales-croisades contribue fondamentalement à forger la première identité compagnonnale.'' {{source|François Icher, ''Les compagnons ou l'amour de la belle ouvrage, 1995, Découvertes Gallimard n°255, 2005, page 29}}
                |## {{lexique|physique|fr}} [[espace|Espace]] où s’exercent des [[forces]].
                |##* ''Un '''champ''' gravitationnel, magnétique, etc.''
                |## {{lexique|sociologie|fr}} [[espace|Espace]] [[social]] [[métaphorique]], [[relationnel]] et [[concurrentiel]] où s’exercent des forces, des [[enjeu]]x de pouvoirs.
                |##* ''Le '''champ''' politique, médiatique, etc.''
                |## {{lexique|mathématiques|fr}} [[espace|Espace]] occupé ou non par un objet [[mathématique]].
                |##* ''Le '''champ''' scalaire, vectoriel, matriciel, etc.''
                |## {{lexique|informatique|fr}} Espace [[éditable]] dans un [[formulaire]].
                |## {{lexique|bases de données|fr}} Colonne de [[table]] de [[base de données]], avec un [[type]].
                |#* '''''Champ''' alpha-numérique, '''champ''' numérique, '''champ''' texte, '''champ''' dates, '''champ''' booléens, '''champ''' compteur automatique, etc.''
                |# {{figuré|fr}} [[carrière|Carrière]], [[sujet]], [[occasion]].
                |#* ''On lui a donné, on lui a ouvert un beau '''champ''' pour acquérir de la gloire.''
                |#* ''Un vaste '''champ''' s’ouvre devant nous.''
                |#* ''Il a un beau '''champ''' pour paraître avec avantage.''
                |#* ''Voilà un beau '''champ''' pour étaler son éloquence, son érudition.''
                |# [[liberté|Liberté]], [[champ libre]].
                |#* ''Donner du '''champ''' à quelqu’un.''
                |#* ''Avoir encore du '''champ''' devant soi,'' Avoir encore des ressources.
                |#* ''Être à bout de '''champ''','' N’avoir plus de ressources.
                |# Étendue plane.
                |## [[fond|Fond]] sur lequel on [[peindre|peint]], on [[grave]], on [[représenter|représente]] quelque chose.
                |##* ''Le '''champ''' de ce tableau est trop clair.''
                |##* ''Ses armes sont un lion d’or en '''champ''' d’azur, sur '''champ''' d’azur.''
                |##* ''Le '''champ''' de cette pièce de monnaie est terne.''
                |## Étendue qu’[[embrasse]] la vue, [[champ de vision]].
                |##* ''Cette lunette a trop peu de '''champ'''.''
                |##* ''Le '''champ''' visuel.''
                |##* ''Diminution du '''champ''' visuel.''
                |### {{analogie|fr}} [[portion|Portion]] de l’espace [[visible]] à travers l’[[objectif]] d’une [[caméra]] et limité par le [[cadre]].
                |###* ''La maîtrise du '''champ''' est un des éléments-clé du coût d’un film : un élément parasite se trouvant accidentellement dans le '''champ''' nécessite de refilmer la même scène.''
                |## {{ellipse|fr}} Étendue de [[nuance]]s et de [[colori]]s que couvre une [[couleur]] [[dominant]]e, [[champ chromatique]].
                |##* ''Appartiennent à ce '''champ''' : albâtre, blanc d'argent, argile, blanchâtre.'' {{source|[http://www.soa-home-deco.fr/glossaire-decoration.php http://www.soa-home-deco.fr]}}
                |# [[Fichier:Blason Nouvelle-église.svg|vignette|120px|Tour et église ouvertes du '''champ''' ''(sens héraldique)'']] {{lexique|héraldique|fr}} [[fond|Fond]] de l’[[écu]]. [[surface|Surface]] la plus [[basse]] des [[armoiries]], [[délimité]]e par un [[périmètre]]. Seule sa [[couleur]] est [[blasonné]]e. Quand un [[blasonnement]] fait [[référence]] au champ, c’est pour [[éviter]] la [[répétition]] de la couleur utilisée pour le champ.
                |#* ''Taillé, au premier d’or à la tour de sable, ouverte et maçonnée du '''champ''', au second de sable à l’église d’or, ouverte et ajourée du '''champ''', qui est de Nouvelle-église.'' {{cf|lang=fr}} illustration « tour et église ouvertes du champ »
                |#* ''L’un porte ses rayures noires sur un fond rosé comme le plumage de la tourterelle, l’autre n’est, des oreilles à la queue, que zébrures pain brûlé sur '''champ''' marron très clair, comme une fleur de giroflée.'' {{source|{{nom w pc||Colette}}, ''Chats'', dans ''La maison de Claudine'', Hachette, 1922, collection Livre de Poche, 1960, page 141.}}
                |# {{lexique|cartographie|fr}} {{term|Rédaction cartographique}} [[étendue|Étendue]] géographique représentée sur une carte, à l’intérieur de l’[[orle]] si celui-ci existe ou se confondant avec la [[surface]] cartographiée dans le cas contraire<ref>{{Import:CFC}}</ref>.
                |
                |==== {{S|synonymes}} ====
                |''Terrain de campagne'' :
                |* [[pré]]
                |''Colonne de table de base de données'' :
                |* [[attribut]]
                |
                |==== {{S|dérivés}} ====
                |{{(|}}
                |* [[agaric des champs]]
                |* [[aller en champ]]
                |* [[alouette des champs]]
                |* [[armoise des champs]]
                |* [[à tout bout de champ]]
                |* [[aux champs]]
                |* [[bartramie des champs]]
                |* [[aux champs|battre aux champs]]
                |* [[bourdon des champs]]
                |* [[brachypode des champs]]
                |* [[champ anal]]
                |* [[champ B]]
                |* [[champ BEHHGK]]
                |* [[champ chromatique]]
                |* [[champ clos]]
                |* [[champ d’action]]
                |* [[champ d’arbres]]
                |* [[champ d’aviation]], aérodrome.
                |* [[champ d’honneur]]
                |* [[champ de bataille]]
                |* [[champ de bits]]
                |* [[champ de Brout-Englert-Higgs-Hagen-Guralnik-Kibble]]
                |* [[champ de course]] (espace où se font des courses de chevaux)
                |* [[champ de foire]]
                |* [[champ de force]]
                |* [[champ de gravité]]
                |* [[champ de Higgs]]
                |* [[champ de jauge]]
                |* [[champ de manœuvres]] (vaste espace plat où s’exécutent les manœuvres militaires)
                |* [[Champ de Mars]]
                |* [[champ de mines]]
                |* [[champ de navet]], [[Champ-des-Navets]] ''(argot)'' cimetière.
                |* [[champ de pesanteur]]
                |* [[champ de pression]]
                |* [[champ de saisie]] (espace éditable dans un formulaire électronique)
                |* [[champ de tir]] (terrain disposé pour les exercices de tir à la cible)
                |* [[champ de vecteurs]]
                |* [[champ de vision]]
                |* [[champ des morts]], [[champ du repos]] (cimetière)
                |* [[champ d’observation]]
                |* [[champ du cinabre]]
                |* [[champ électrique]]
                |* [[champ électromagnétique]]
                |* [[champ extérieur]]
                |* [[champ gazier]]
                |* [[champ gravitationnel]]
                |* [[champ H]]
                |* [[champ intérieur]]
                |* [[champ jugal]]
                |* [[champ lexical]]
                |* [[champ libre]]
                |* [[champ magnétique]]
                |* [[champ opératoire]] {{info lex|médecine}}, région circonscrite du corps sur laquelle est effectuée une opération.
                |* [[champ pétrolifère]]
                |* [[champ réduit]] ''(Paris hippiques)''
                |* [[champ scalaire]]
                |* [[champ sémantique]]
                |* [[champ tensoriel]]
                |* [[champ total]] ''(Paris hippiques)''
                |* [[champ vectoriel]]
                |* [[champ vif]]
                |* [[champ visuel]]
                |* [[champs Élysées]], lieu de séjour réservé, d’après les anciens, à ceux qui ont mené une vie vertueuse.
                |* [[clé des champs]]
                |* [[clé du champ de tir]]
                |* [[clyte des champs]]
                |* [[communication en champ proche]]
                |* [[de champ]]
                |* [[être dans le champ]]
                |* [[gagner les champs]]
                |* [[joueur de champ extérieur]]
                |* [[joueur de champ intérieur]]
                |* [[longicorne des champs]]
                |* [[maubèche des champs]]
                |* [[mélampyre des champs]]
                |* [[moineau des champs]]
                |* [[moutarde des champs]]
                |* [[musaraigne des champs]]
                |* [[nielle des champs]]
                |* [[œillet des champs]]
                |* [[panicaut des champs]]
                |* [[passerage des champs]]
                |* [[pensée des champs]]
                |* [[plein champ]]
                |* [[pomme de terre en robe des champs]]
                |* [[poser de champ]]
                |* [[poser sur champ]]
                |* [[prendre à travers champs]]
                |* [[rat des champs]]
                |* [[aux champs|sonner aux champs]]
                |* [[souci des champs]]
                |* [[thé des champs]]
                |* [[torilis des champs]]
                |* [[violette des champs]]
                |* [[voix dans le champ]]
                |* [[voix hors champ]]
                |{{)}}
                |
                |==== {{S|apparentés}} ====
                |{{(|}}
                |* [[Champagne]], [[champagne]]
                |* [[champan]]
                |* [[champayer]]
                |* [[champéage]]
                |* [[champêtre]]
                |* [[champi]]
                |* [[champmètre]]
                |* [[contrechamp]]
                |* [[échampir]], [[réchampir]]
                |* [[hors-champ]]
                |{{)}}
                |
                |==== {{S|phrases}} ====
                |* [[à tout bout de champ]] : à chaque instant, à tout propos
                |* [[à travers champs]] (hors des routes battues)
                |* [[courir les champs]] (se promener, errer dans les champs, ou vagabonder en parlant de l’esprit, de l’imagination vagabonde)
                |* [[mettre aux champs]] (se fâcher ou s’inquièter aisément)
                |* [[prendre du champ]] (prendre de l’espace, du recul pour mieux fournir sa carrière)
                |* [[sur-le-champ]]
                |
                |==== {{S|vocabulaire}} ====
                |* [[agraire]]
                |* [[agriculture]]
                |* [[arable]]
                |* [[aratoire]]
                |
                |==== {{S|traductions}} ====
                |{{trad-début|Terrain de campagne}}
                |* {{T|af}} : {{trad+|af|veld}}, {{trad+|af|akker}}
                |* {{T|sq}} : {{trad+|sq|fushë}}
                |* {{T|de}} : {{trad+|de|Feld}}, {{trad+|de|Flur}}, {{trad+|de|Land}}, {{trad+|de|Acker}}, {{trad+|de|Parzelle}}
                |* {{T|en}} : {{trad+|en|field}}, {{trad+|en|farmland}}
                |* {{T|ar}} : {{trad-|ar|حقل|tr=Haql}}
                |* {{T|ang}} : {{trad-|ang|feld}}, {{trad-|ang|æcer}}
                |* {{T|hy}} : {{trad+|hy|դաշտ|tr=dašt}}
                |* {{T|bm}} : {{trad--|bm|foro}}
                |* {{T|eu}} : {{trad-|eu|elge}}, {{trad-|eu|alor}}, {{trad-|eu|zelai}}
                |* {{T|bg}} : {{trad+|bg|нива|tr=niva}}, {{trad+|bg|поле|tr=pole}}
                |* {{T|ca}} : {{trad+|ca|camp}}
                |* {{T|shy}} : {{trad--|shy|aṛeqquc}}
                |* {{T|zh}} : {{trad+|zh|田|tr=tián}}
                |* {{T|cim}} : {{trad--|cim|akhar}}
                |* {{T|ko}} : {{trad+|ko|밭|tr=bat}}
                |* {{T|da}} : {{trad+|da|mark|c}}
                |* {{T|es}} : {{trad+|es|campo}}, {{trad+|es|agro}}
                |* {{T|eo}} : {{trad-|eo|kampo}}
                |* {{T|et}} : {{trad-|et|väli}}
                |* {{T|ewo}} : {{trad--|ewo|afup}}
                |* {{T|fo}} : {{trad-|fo|bøur}}, {{trad-|fo|fløttur}}
                |* {{T|fi}} : {{trad+|fi|pelto}}, {{trad+|fi|viljelys}}
                |* {{T|fc}} : {{trad--|fc|tchaimp|m}}
                |* {{T|fy}} : {{trad+|fy|fjild}}, {{trad-|fy|eker}}
                |* {{T|gd}} : {{trad-|gd|achadh}}, {{trad-|gd|machair}}
                |* {{T|ga}} : {{trad+|ga|páirc}}
                |* {{T|gallo}} : {{trad--|gallo|cloz}}
                |* {{T|cy}} : {{trad+|cy|maes}}
                |* {{T|gqu}} : {{trad--|gqu|zəɯ¹³}}
                |* {{T|gaulois}} : {{trad--|gaulois|acito-}}
                |* {{T|el}} : {{trad+|el|αγρός|tr=agrós}}
                |* {{T|grc}} : {{trad--|grc|ἀγρόσ|tr=agros}}
                |* {{T|hbo}} : {{trad--|hbo|שדה}}
                |* {{T|hu}} : {{trad+|hu|mező}}
                |* {{T|io}} : {{trad+|io|agro}}
                |* {{T|id}} : {{trad+|id|ladang}}
                |* {{T|is}} : {{trad+|is|völlur}}, {{trad+|is|akur}}
                |* {{T|it}} : {{trad+|it|campo}}
                |* {{T|ja}} : {{trad+|ja|畑|tr=hatake}}, {{trad+|ja|野|tr=no}}, {{trad+|ja|場|tr=ba}}
                |* {{T|kab}} : {{trad--|kab|iger}}
                |* {{T|kog}} : {{trad--|kog|téʒi}}
                |* {{T|ku}} : {{trad+|ku|zevî|f}}
                |* {{T|la}} : {{trad-|la|arvum}}, {{trad+|la|ager}}
                |* {{T|lv}} : {{trad+|lv|lauks}}
                |* {{T|lt}} : {{trad-|lt|laukas}}
                |* {{T|swb}} : {{trad--|swb|shamba}}, {{trad--|swb|mundra}}
                |* {{T|yua}} : {{trad--|yua|kol}}
                |* {{T|nl}} : {{trad+|nl|land}}, {{trad+|nl|veld}}, {{trad+|nl|akker}}
                |* {{T|fra-nor}} : {{trad--|fra-nor|pyiche}}, {{trad--|fra-nor|clios}}, {{trad--|fra-nor|cllosaée}}, {{trad--|fra-nor|âcre}}
                |* {{T|no}} : {{trad-|no|leir}}
                |* {{T|oc}} : {{trad+|oc|camp}}, {{trad+|oc|champ}}
                |* {{T|os}} : {{trad--|os|быдыр}}, {{trad--|os|будур}}
                |* {{T|ug}} : {{trad-|ug|ئېتىز|tr=Étiz}}
                |* {{T|pln}} : {{trad--|pln|monte}}, {{trad--|pln|monte}}
                |* {{T|pap}} : {{trad--|pap|vèlt}}, {{trad--|pap|kunuku}}
                |* {{T|ff}} : {{trad--|ff|ngesa}}
                |* {{T|pcd}} : {{trad--|pcd|canp}}
                |* {{T|myp}} : {{trad--|myp|xogaí}}
                |* {{T|pl}} : {{trad+|pl|pole}}
                |* {{T|pt}} : {{trad+|pt|agro}}, {{trad+|pt|campo}}
                |* {{T|ro}} : {{trad+|ro|câmp}}, {{trad-|ro|ogorul}}
                |* {{T|ru}} : {{trad+|ru|поле|tr=pole}}
                |* {{T|saa}} : {{trad--|saa|bərsa}}
                |* {{T|se}} : {{trad--|se|bealdu}}, {{trad--|se|gieddi}}
                |* {{T|zdj}} : {{trad--|zdj|shiunga}}, {{trad--|zdj|shamɓa}}, {{trad--|zdj|mnda}} (champ cultivé)
                |* {{T|sk}} : {{trad-|sk|pole}}
                |* {{T|sl}} : {{trad+|sl|polje}}
                |* {{T|ses}} : {{trad--|ses|faari}}
                |* {{T|srn}} : {{trad--|srn|firi}}, {{trad--|srn|gron}}, {{trad--|srn|sabana}}
                |* {{T|sv}} : {{trad+|sv|fält}}, {{trad+|sv|åker}}
                |* {{T|sw}} : {{trad+|sw|shamba}}, {{trad+|sw|konde}}
                |* {{T|ta}} : {{trad+|ta|வயல்|tr=vayal}}
                |* {{T|cs}} : {{trad+|cs|pole}}
                |* {{T|dje}} : {{trad--|dje|fari}}
                |{{trad-fin}}
                |
                |{{trad-début|Recul, liberté, espace, domaine}}
                |* {{T|ar}} : {{trad-|ar|ميدان|tr=maydèn}}, {{trad-|ar|ساحة|tr=sèHa}}
                |* {{T|eu}} : {{trad-|eu|eremu}}
                |* {{T|ca}} : {{trad+|ca|espai}}
                |* {{T|fi}} : {{trad+|fi|tila}}, {{trad+|fi|ala}}, {{trad-|fi|liikkumatila}}, {{trad-|fi|liikkuma-ala}}
                |* {{T|nl}} : {{trad+|nl|terrein}}
                |* {{T|ug}} : {{trad-|ug|مەيدان|tr=meydan}}, {{trad-|ug|دائىرە|tr=da'ire}}, {{trad-|ug|ساھە|tr=sahe}}
                |{{trad-fin}}
                |
                |{{trad-début|Espace (mathématique, sociologie, etc.)}}
                |* {{T|de}} : {{trad+|de|Feld}}, {{trad-|de|Eingabefeld}}, {{trad-|de|Anzeigefeld}}
                |* {{T|en}} : {{trad+|en|field}}, {{trad+|en|area}}
                |* {{T|eu}} : {{trad-|eu|eremu}}
                |* {{T|ca}} : {{trad+|ca|camp}}, {{trad+|ca|àrea}}
                |* {{T|da}} : {{trad-|da|felt|n}}
                |* {{T|fi}} : {{trad+|fi|kenttä}}, {{trad+|fi|ala}}, {{trad+|fi|alue}}
                |* {{T|nl}} : {{trad+|nl|veld}}
                |{{trad-fin}}
                |
                |{{trad-début|Espace éditable dans un formulaire}}
                |* {{T|en}} : {{trad-|en|input field}}, {{trad+|en|box}}
                |* {{T|ca}} : {{trad+|ca|camp}}
                |* {{T|da}} : {{trad-|da|felt|n}}
                |* {{T|fi}} : {{trad+|fi|kenttä}}
                |* {{T|gallo}} : {{trad--|gallo|cloz}}
                |* {{T|nl}} : {{trad+|nl|veld}}
                |* {{T|se}} : {{trad--|se|gieddi}}
                |* {{T|cs}} : {{trad+|cs|pole}}
                |{{trad-fin}}
                |
                |{{trad-début|Occasion|3}}
                |* {{T|zdj}} : {{trad--|zdj|nafasi}}
                |{{trad-fin}}
                |
                |=== {{S|nom|fr|num=2}} ===
                |'''champ''' {{pron|ʃɑ̃p|fr}} {{m}}
                |# {{indénombrable|fr}} {{variante ortho de|champ’|fr}}.
                |#* ''Soirée de réveillon à la sauce vaudeville : à la suite d’un concours de circonstances, une bourgeoise et une prolo se retrouvent contraintes à partager la dinde et le '''champ'''.'' {{source|''{{w|Télérama}}'' {{numéro}} 2783}}
                |
                |=== {{S|prononciation}} ===
                |* ''([[#fr-nom-1|Nom commun 1]])'' {{pron|ʃɑ̃|fr}}
                |** {{écouter|lang=fr|France|ɛ̃ ʃɑ̃|titre=un champ|audio=Fr-champ.ogg}}
                |* Français méridional : {{pron|ʃaŋ|fr}}
                |* Canada : {{pron|ʃã|fr}}
                |* ''([[#fr-nom-2|Nom commun 2]])'' {{pron|ʃɑ̃p|fr}}
                |** {{écouter|lang=fr|France (Yvelines)|audio=LL-Q150 (fra)-Jules78120-champ.wav}}
                |* {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Fhala.K-champ.wav}}
                |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-GrandCelinien-champ.wav}}
                |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-champ.wav}}
                |* {{écouter|||lang=fr|audio=LL-Q150 (fra)-WikiLucas00-champ.wav}}
                |* {{écouter|lang=fr|France (Muntzenheim)|audio=LL-Q150 (fra)-0x010C-champ.wav}}
                |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-champ.wav}}
                |* {{écouter|lang=fr|||audio=LL-Q150 (fra)-ClasseNoes-champ.wav}}
                |* {{écouter|lang=fr|Suisse (Lausanne)||audio=LL-Q150 (fra)-Eihel-champ.wav}}
                |
                |==== {{S|homophones|fr}} ====
                |''[[#fr-nom-1|Nom commun 1]]'' :
                |* [[Champs]]
                |* [[chant]]
                |
                |=== {{S|voir aussi}} ===
                |* {{Thésaurus|fr|héraldique}}
                |* {{WP}}
                |* {{Le Dico des Ados}}
                |
                |=== {{S|références}} ===
                |{{Références}}
                |
                |[[Catégorie:Vins en français]]
                |[[Catégorie:Homographes non homophones en français]]
                |
                |== {{langue|en}} ==
                |=== {{S|étymologie}} ===
                |: ''(Nom 1)'' {{apocope|m=1|en}} de ''{{l|champion|en}}''.
                |: ''(Nom 2)'' {{apocope|m=1|en}} de ''{{l|champagne|en}}''.
                |
                |=== {{S|nom|en|num=1}} ===
                |{{en-nom-rég|ˈtʃæmp}}
                |'''champ''' {{pron|ˈtʃæmp|en}}
                |# {{popu|en}} [[champion|Champion]].
                |
                |=== {{S|nom|en|num=2}} ===
                |{{en-nom-rég|ˈʃæmp}}
                |'''champ''' {{pron|ˈʃæmp|en}}
                |# {{popu|en}} [[champagne|Champagne]].
                |
                |=== {{S|prononciation}} ===
                |* {{écouter|lang=en|Royaume-Uni (Sud de l'Angleterre)||audio=LL-Q1860 (eng)-Vealhurl-champ.wav}}
                |
                |=== {{S|voir aussi}} ===
                |* {{WP|lang=en}}
                |[[Catégorie:Vins en anglais]]
                |[[Catégorie:Homographes non homophones en anglais]]
                |""".stripMargin
}