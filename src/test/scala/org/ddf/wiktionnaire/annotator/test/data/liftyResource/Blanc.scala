/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource


object Blanc {

  val resource = Resource("blanc", "https://fr.wiktionary.org/wiki/", "entry",
  """{{voir|Blanc}}
    |
    |== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: Le bas-latin {{recons|lang-mot-vedette=fr|blancus}} est postulé par les langues romanes{{R|Littré}} : ''{{lien|blanco|es}}'' en espagnol, ''{{lien|bianco|it}}'' en italien, ''{{lien|branco|pt}}'' en portugais, du {{étyl|frk|fr}} {{recons|lang-mot-vedette=fr|[[blank]]}} (« [[clair]] » {{cf|bleu|lang=fr}}), qui a remplacé le latin ''[[albus#la|albus]], {{lien|candidus|la}}'' {{cf|blond|brun|fauve|gris|lang=fr}} pour d’autres noms de couleur empruntés au germanique.
    |
    |=== {{S|adjectif|fr}} ===
    |{{fr-accord-mixte|ms=blanc|fs=blanche|pm=blɑ̃|pf=blɑ̃ʃ}}
    |'''blanc''' {{pron|blɑ̃|fr}} {{m}}
    |# D’une [[couleur]] comme celle des [[os]], de la [[craie]] ou de l’[[écume]] entre autre. — {{note}} Comme l’adjectif [[noir]] ou [[gris]], qui ne sont que des différences de [[ton]] du [[#fr-nom-1|blanc]], il implique souvent une notion d’absence de couleur, de teinte.{{couleur|#FFFFFF}}
    |#* ''On ne pouvait voir de fille plus fraîche, plus riante ; elle était blonde, avec de beaux yeux bleus, des joues roses et des dents '''blanches''' comme du lait ; […].'' {{source|{{w|Erckmann-Chatrian}}, ''[[s:Histoire d’un conscrit de 1813/1|Histoire d’un conscrit de 1813]]'', J. Hetzel, 1864}}
    |#* ''Il neige dur. La place est toute '''blanche'''.'' {{source|Albert Vidalie, ''C’était donc vrai'', éditions René Julliard, 1952, page 34}}
    |#* ''La salle des fêtes était habillée pour une grande cérémonie, de somptueux rideaux '''blancs''' et pourpres qui tombaient du haut plafond jusqu'au bas des portes et des fenêtres.'' {{source|Maboa Bebe, ''Ewande Amours, peurs, espoir'', L’Harmattan Cameroun, 2014, page 7}}
    |# {{par extension|fr}} D’une couleur appartenant au [[champ chromatique]] du [[#fr-nom-1|blanc]], c’est-à-dire légèrement différente de la couleur originelle mais lié à celle-ci.{{couleur|#FFFFFF}}{{couleur|#FFFFD4}}{{couleur|#FDE9E0}}{{couleur|#FDE1B8}}{{couleur|#F2FFFF}}{{couleur|#EFEFEF}}
    |# Très peu coloré, ou moins coloré que, en parlant de choses qui ne sont pas tout à fait '''blanches''' (1), pour les [[distinguer]] de celles de même espèce qui le sont moins, ou qui sont d’une autre couleur.
    |#* ''Il y a des betteraves '''blanches''', il y en a de jaunes, de rouges, et de marbrées, et quelquefois la pellicule est rouge et la chair est '''blanche'''.'' {{source|{{w|Jean-Antoine Chaptal}}, ''[[s:Mémoire sur le sucre de betterave|Mémoire sur le sucre de betterave]]'', Mémoires de l’Académie des sciences, tome 1, 1818 (pages 347-388)}}
    |#* ''Bois''' blanc''', métal '''blanc''', mûrier '''blanc''', pain '''blanc''', poisson '''blanc''', poivre '''blanc''', raisin '''blanc''', sauce '''blanche''', sel '''blanc''', viande '''blanche''', vin '''blanc'''.''
    |#* ''En sabots, les manches relevées sur leurs bras '''blancs''', les filles balayaient le devant des boutiques''. {{source|{{Citation/Francis Carco/Brumes/1935|158}}}}
    |#* ''Avoir le teint '''blanc''', la gorge '''blanche''', les mains '''blanches''', la peau '''blanche'''.''
    |#* ''Gelée '''blanche''','' Gelée qui, le matin, se forme de la rosée ou du brouillard congelé.
    |#* ''Faire des lotions avec de l’eau '''blanche'''.''
    |#* ''Ver '''blanc''','' Larve du hanneton.''
    |# {{lexique|sociologie|fr}} Qui appartient au groupe (ethnique, social ou biologique, selon le point du vue) des [[Blancs]].
    |#* ''Betty Bonifassi, selon cette logique, ne pouvait chanter ce qu'elle avait contribué à sortir de l'oubli : les chants d'esclaves noirs. Elle est '''blanche'''.'' {{source|{{w|Marie-France Bazzo}}, ''Nous méritons mieux'', Boréal, 2020, page 170}}
    |#* ''Geoffroy, Maxime, Frédéric, Jérémy et Billy sont cinq hommes '''blancs''' dans la trentaine, travaillant dans le secteur manuel et originaires de l'Essonne.''{{source|www.francetvinfo.fr, ''[https://www.francetvinfo.fr/economie/transports/gilets-jaunes/ils-n-ont-meme-pas-manifeste-et-les-voila-en-garde-a-vue-apres-les-violences-a-paris-le-defile-des-gilets-jaunes-au-tribunal_3083317.html Le défilé des gilets jaunes au tribunal]'', 04/12/2018}}
    |#* ''DiAngelo est une femme '''blanche''' qui a passé des dizaines d'années délibérément embourbée dans la question de la race.'' {{source|Bérengère Viennot, [http://www.slate.fr/story/185459/etats-unis-sociologie-fragilite-blanche-robin-diangelo-racisme-progressisme-1 ''« La Fragilité blanche » brise les idées reçues sur les progressistes''], slate.fr, 2019, page 38}}
    |# {{lexique|sociologie|fr}} Relatif à la [[race]] blanche, au groupe social des [[Blanc]]s.
    |#* ''C’est dans cette perspective que nous nous situerons, en nous inscrivant notamment dans le sillage des travaux conduits dans le champ anglophone des critical white studies, qui étudie la construction sociale des identités et subjectivités '''blanches'''.'' {{source|Maxime Cervulle, [https://www.cairn.info/revue-cahiers-du-genre-2012-2-page-37.htm « La conscience dominante. Rapports sociaux de race et subjectivation »], n° 53, ''Cahiers du genre'', 2012, page 38}}
    |#* ''Cela étant dit, le privilège '''blanc''' n’est pas un sujet facile à aborder.'' {{source|Kevin Sweet, [https://ici.radio-canada.ca/nouvelle/1507954/mois-histoire-noirs-privilege-blanc-reflexion ''L'éveil du Blanc privilégié et sa fragilité''], ici.radio-canada.ca, 2020}}
    |# {{spécialement|fr}} {{Canada|fr}} Qui est allochtone, non [[amérindien]]ne.
    |#* ''[Mme] Hunter […] a donné naissance à une fille ce matin, le premier enfant '''blanc''' dans Teslin. Ah ! Pourtant non, pas '''blanc''', car c’est une famille de Noirs, et pourtant on s’[[obstiner|obstine]] ici à dire que c’est la naissance du premier enfant '''blanc'''. On veut sans doute dire que c’est le premier enfant né à Teslin de parents civilisés.'' {{source|''17 Eldorado'', Journal de Lorenzo Létourneau (1899), Qualigram/Linguatech, Montréal, 2006}}
    |# [[vierge#fr|Vierge]], sur lequel il n’y a rien d’écrit ou d’imprimé.
    |#* ''Un feuillet de papier '''blanc'''.''
    |#* ''Une page '''blanche'''.''
    |#* ''Un cahier '''blanc'''.''
    |#* ''Livre '''blanc''','' livre dont tous les feuillets sont blancs.
    |# [[vierge|Vierge]].
    |#* ''Bal '''blanc''','' bal où ne sont invitées que des jeunes filles.
    |#* ''Mariage '''blanc''','' mariage qui n’a pas été consommé.
    |# {{par ext|fr}} Vide de contenu.
    |#* ''Pour certains, ce taf était juste un moyen commode de remplir un '''blanc''' dans leur CV en attendant qu’un membre de leur famille leur trouve une meilleure place.'' {{source|{{w|David Graeber}}, traduit par Élise Roy, ''Bullshit jobs'', {{W|Les liens qui libèrent}}, 2018, {{ISBN|979-10-209-0633-5}}}}
    |# {{lexique|militaire|fr}} À lame d'[[acier]].
    |#* ''Armes '''blanches''','' armes offensives, comme épées, sabres, baïonnettes, etc., par opposition aux armes à feu. On appelait autrefois aussi ''armes '''blanches''''' les armes défensives qui n’étaient ni gravées, ni dorées, ni bronzées.
    |#* ''Se battre à l’arme '''blanche'''.''
    |#* ''Combat à l’arme '''blanche'''.''
    |# [[clair#fr|Clair]].
    |#* ''Voix '''blanche''','' voix claire mais qui manque de timbre.
    |#* ''Il prenait pour parler une voix '''blanche''', doucereuse, déclamait des bouts d’idées ramassées un peu partout, sur les droits de l’ouvrier, la tyrannie du capital.'' {{source|{{w|Alphonse Daudet}}, ''Arthur'', dans ''Contes du lundi'', 1873, Fasquelle, collection Le Livre de Poche, 1974, page 168}}
    |#* ''Argent '''blanc''', Bâton '''blanc''', Bulletin '''blanc''', Houille '''blanche''', Magie '''blanche''', Nuit '''blanche''', Sauce '''blanche''', Viande '''blanche'''.'' Voyez « argent », « bâton », « bulletin », etc.
    |# {{lexique|jeux|fr}} [[coup|Coup]] qui ne [[produit]] [[rien]].
    |#* ''Amener '''blanc''','' à certains jeux de Dés, se dit lorsque tous les dés présentent la face qui n’est marquée d’aucun point.
    |#* ''Coup '''blanc''','' coup nul.
    |#* ''Opération '''blanche''','' Opération qui n’a pas donné de résultat.
    |#* {{figuré|fr}} ''Vers '''blancs''','' en termes de Versification, Vers non rimés.''
    |#* ''Le Paradis perdu " de Milton est en vers '''blancs'''.''
    |# {{lexique|foresterie|fr}} Plusieurs termes d'[[exploitation]] [[forestier|forestière]] recourent à des [[locution nominale|locutions nominales]] employant l'adjectif '''blanc''' telles que
    |## ''Couper une forêt, faire une coupe à '''blanc''' estoc'' ou
    |## ''À '''blanc''' être,'' ou simplement
    |## ''À '''blanc''': ces expressions signifient toutes ''en couper tout le bois, sans y laisser de baliveaux.''
    |##* On dit dans le même sens: une '''''coupe blanche''''', qui en est le résultat.
    |##* ''Couper un arbre à '''blanc''' estoc'' : le couper au pied sur la souche.
    |# {{proverbial|fr}} L'adjectif ''blanc'' s'emploie aussi dans des [[locution verbale|locutions verbales]] issues du vocabulaire des [[duel]]s [[judiciaire]]s du [[Moyen Âge]] et parfois utilisées dans le français moderne:
    |## ''Se faire tout '''blanc''' de son épée, se faire '''blanc''' de son épée,'' : répondre à une accusation, se justifier d’une accusation par l’épée, par la force.
    |## {{figuré|fr}} {{par extension|fr}} ''Faire '''blanc''' de son épée'': s'emploie au figuré pour signifier ''se prévaloir de sa force, se vanter de faire quelque chose en se supposant un pouvoir ou un crédit qu’on n’a pas.''
    |# Qui est [[propre]], par opposition à [[sale]].
    |#* ''Linge '''blanc'''.''
    |#* ''Ces draps ont servi, ils ne sont pas '''blancs'''.''
    |#* ''Chemise '''blanche'''.''
    |#* ''Nappe '''blanche'''.''
    |#* ''Serviette '''blanche'''.''
    |#* '''''Blanc''' de lessive'' se dit du Linge propre, tel qu’il est au sortir de la lessive.
    |#* ''Ces draps, ces rideaux sont '''blancs''' de lessive.''
    |#* {{figuré|fr}} {{familier|fr}} ''Sortir d’une accusation, d’une affaire '''blanc''' comme neige,'' ou simplement
    |#* ''Sortir '''blanc''' d’une affaire,'' être déclaré innocent, être acquitté par un arrêt ou un jugement, en matière criminelle ou correctionnelle.
    |#* ''Vouer un enfant au '''blanc''','' Faire vœu qu’un enfant sera entièrement vêtu de '''blanc''', jusqu’à tel âge, en l’honneur de la sainte Vierge.''
    |#* ''Se vouer au '''blanc'''.''
    |
    |==== {{S|synonymes}} ====
    |''Relatif à la race blanche'' :
    |* [[caucasien]]
    |* [[caucasoïde]]
    |* [[europoïde]]
    |
    |==== {{S|antonymes}} ====
    |''Non amérindien'' :
    |* [[amérindien]]
    |* [[autochtone]]
    |* [[rouge]] {{désuet|nocat=1}}
    |* [[sauvage]] {{désuet|nocat=1}}
    |
    |==== {{S|dérivés}} ====
    |{{(}}
    |* [[à blanc estoc]]
    |* [[à marquer d’une pierre blanche]]
    |* [[acacia blanc]]
    |* [[adénostyle à feuilles blanches]]
    |* [[Afrique blanche]]
    |* [[aigle à tête blanche]]
    |* [[aigle marin à queue blanche]]
    |* [[aigrette blanche]]
    |* [[albatros à cape blanche]]
    |* [[alcool blanc]]
    |* [[alisier blanc]]
    |* [[alisier blanc de neige]]
    |* [[amazone à front blanc]]
    |* [[amour blanc]]
    |* [[angine blanche]]
    |* [[année blanche]]
    |* [[arbre blanc]]
    |* [[arc blanc]]
    |* [[arc-en-ciel blanc]]
    |* [[argent blanc]] ''(monnaie d’argent, par opposition aux autres métaux)''
    |* [[arme blanche]]
    |* [[asclépiade blanche]]
    |* [[asphodèle blanc]]
    |* [[autour blanc]]
    |* [[autour à ventre blanc]]
    |* [[avoir carte blanche]]
    |* [[baleine blanche]]
    |* [[baptisie blanche]]
    |* [[bâton blanc]]
    |* [[bâton-blanc ramifié]]
    |* [[bécasseau à croupion blanc]]
    |* [[bécasseau cul-blanc]]
    |* [[béret blanc]]
    |* [[berger blanc suisse]]
    |* [[beurre blanc]]
    |* [[bière blanche]]
    |* [[black-blanc-beur]]
    |* [[blanc-bec]]
    |* [[blanc-bleu]]
    |* [[blanc-boc]]
    |* [[blanc-bois]]
    |* [[blanc bonnet]]
    |* [[blanc cassé]]
    |* [[blanc-cassis]]
    |* [[blanc comme neige]]
    |* [[blanc comme un cachet d’aspirine]]
    |* [[blanc comme un cygne]]
    |* [[blanc comme un linge]]
    |* [[blanc comme un navet]]
    |* [[blanc-cul]]
    |* [[blanc de peur]]
    |* [[blanc-étoc]], [[blanc-estoc]]
    |* [[blanc-gris]]
    |* [[blanc-manger]]
    |* [[blanc-manteau]]
    |* [[Blancs-Manteaux]]
    |* [[blanc navet]]
    |* [[blanc-nez]]
    |* [[blanc-ployant]]
    |* [[blanc-poudré]]
    |* [[blanc-russe]]
    |* [[blanc-seing]], [[blanc-signé]]
    |* [[blanc-soudant]]
    |* [[blancard]]
    |* [[blanchaille]]
    |* [[blanchard]]
    |* [[blanchâtre]]
    |* [[blanche]]
    |* [[blanche-coiffe]]
    |* [[Blanche-Neige]]
    |* [[blanche-queue]]
    |* [[blanche-raie]]
    |* [[blanchement]]
    |* [[blancher]]
    |* [[blancherie]]
    |* [[blanchet]]
    |* [[blanchette]]
    |* [[blancheur]]
    |* [[blanchiment]]
    |* [[blanchir]]
    |* [[blanchis]]
    |* [[blanchissage]]
    |* [[blanchissement]]
    |* [[blanchisserie]]
    |* [[blanchisseur]]
    |* [[blanchité]]
    |* [[blanchitude]]
    |* [[blanchœuvrier]]
    |* [[blanchon]]
    |* [[blanchot]]
    |* [[blanchoyer]]
    |* [[blanque]]
    |* [[blanquet]]
    |* [[blanquette]]
    |* [[blanquier]]
    |* [[bois blanc]]
    |* [[boite blanche]] {{ortho1990}}
    |* [[boîte blanche]]
    |* [[boudin blanc]]
    |* [[bouillon blanc]], [[bouillon-blanc]]
    |* [[bouillon blanc commun]]
    |* [[bouillon blanc sinué]]
    |* [[bouleau blanc]]
    |* [[bourdon à queue blanche]]
    |* [[brème blanche]]
    |* [[brouille-blanche]]
    |* [[bruant à calotte blanche]]
    |* [[bruant blanc]]
    |* [[bruit blanc]]
    |* [[bruyère blanche]]
    |* [[bulletin blanc]]
    |* [[busautour aux yeux blancs]]
    |* [[C-blanc]]
    |* [[cacatoès à rectrices blanches]]
    |* [[cacatoès blanc]]
    |* [[café blanc]]
    |* [[caillé-blanc]]
    |* [[caïque à ventre blanc]]
    |* [[canard à front blanc]]
    |* [[canne blanche]]
    |* [[carré blanc]]
    |* [[carte blanche]]
    |* [[caryer blanc]]
    |* [[ceinture blanche]]
    |* [[ceinture rouge et blanche]]
    |* [[chaméléon blanc]]
    |* [[charbon blanc]]
    |* [[chêne blanc]]
    |* [[chêne blanc d’Amérique]]
    |* [[chêne blanc de Californie]]
    |* [[chêne blanc de Provence]]
    |* [[chevalier blanc]]
    |* [[chevalier culblanc]]
    |* [[chèvre allemande blanche]]
    |* [[chien français blanc et noir]]
    |* [[chien français blanc et orange]]
    |* [[chionis blanc]]
    |* [[chocolat blanc]]
    |* [[cigogne blanche]]
    |* [[ciguë blanche]]
    |* [[cigüe blanche]] {{ortho1990}}
    |* [[circaète jean-le-blanc]]
    |* [[ciste blanc]]
    |* [[clématite vigne-blanche]]
    |* [[clitocybe blanc]]
    |* [[clitocybe blanc d’ivoire]]
    |* [[clown blanc]]
    |* [[cocarde blanche]]
    |* [[code blanc]]
    |* [[col blanc]]
    |** [[criminel en col blanc]]
    |* [[connu comme le loup blanc]]
    |* [[conure à col blanc]]
    |* [[conure à oreillons blancs]]
    |* [[coprin blanc et noir]]
    |* [[corbeau blanc]]
    |* [[corégone blanc]]
    |* [[cortinaire blanc violacé]]
    |* [[cotique blanc]]
    |* [[coucoumelle blanche]]
    |* [[coupe à blanc]]
    |* [[cousu de fil blanc]]
    |* [[crime en col blanc]]
    |* [[crocidure blanche]]
    |* [[cul-blanc]]
    |* [[cul-blanc des rivages]]
    |* [[damalisque à front blanc]]
    |* [[damalisque à queue blanche]]
    |* [[dame blanche]]
    |* [[dauphin à bec blanc]]
    |* [[dauphin à flancs blancs]]
    |* [[dauphin à nez blanc]]
    |* [[dauphin blanc]]
    |* [[dauphin bleu et blanc]]
    |* [[déblanchir]]
    |* [[délinquance en col blanc]]
    |* [[demi-lune blanche]]
    |* [[diarrhée blanche des poussins]]
    |* [[dictame blanc]]
    |* [[dindon blanc de Beltsville]]
    |* [[diotis blanc]]
    |* [[docteur à quatre boules blanches]]
    |* [[donner carte blanche]]
    |* [[double-blanc]]
    |* [[drapeau blanc]]
    |* [[eau blanche]]
    |* [[échasse blanche]]
    |* [[écrevisse à pattes blanches]]
    |* [[écriture blanche]]
    |* [[élanion blanc]]
    |* [[éléphant blanc]]
    |* [[ellébore blanc]]
    |* [[épine blanche]]
    |* [[épinette blanche]]
    |* [[érismature à tête blanche]]
    |* [[esturgeon blanc]]
    |* [[érynge blanc]]
    |* [[éryngion blanc]]
    |* [[être blanc comme un navet]]
    |* [[être blanc de peur]]
    |* [[être connu comme le loup blanc]]
    |* [[examen blanc]]
    |* [[facteur blanc]]
    |* [[faire chou blanc]]
    |* [[fardeau de l’homme blanc]]
    |* [[fer-blanc]]
    |* [[fer sulfuré blanc]]
    |* [[feuille blanche]]
    |* [[flanc blanc]]
    |* [[flétan blanc]]
    |* [[folle-blanche]], [[folle blanche]]
    |* [[fonio blanc]]
    |* [[fontaine blanche]]
    |* [[français blanc et orange]] (''Race de chien'')
    |* [[frêne blanc]]
    |* [[fromage blanc]]
    |* [[fuligule à dos blanc]]
    |* [[fumeur blanc]]
    |* [[gardon blanc]]
    |* [[geai à face blanche]]
    |* [[geai à gorge blanche]]
    |* [[gelao blanc de Pudi]]
    |* [[gelée blanche]]
    |* [[génépi blanc]]
    |* [[genêt blanc]]
    |* [[germandrée blanc-grisâtre]]
    |* [[globule blanc]]
    |* [[gnou à queue blanche]]
    |* [[goéland à ailes blanches]]
    |* [[gorge blanche]]
    |* [[grain blanc]] {{info lex|météorologie}}
    |* [[graisse blanche]]
    |* [[grallaire à gorge blanche]]
    |* [[grallaire à ventre blanc]]
    |* [[grand anglo-français blanc et noir]] (''Race de chien'')
    |* [[grand anglo-français blanc et orange]] ''(Race de chien)''
    |* [[grand porc blanc anglais]]
    |* [[grand requin blanc]]
    |* [[gros-blanc]]
    |* [[grue à cou blanc]]
    |* [[grue blanche]]
    |* [[guifette à ailes blanches]]
    |* [[haricot blanc]]
    |* [[hélianthème blanc]]
    |* [[herbe blanche]]
    |* [[herminie de la vigne-blanche]]
    |* [[héron blanc]]
    |* [[hocheur blanc-nez]]
    |* [[hocheur blanc-nez du Bénin]]
    |* [[homme blanc]]
    |* [[houille blanche]]
    |* [[huile blanche]]
    |* [[jambon blanc]]
    |* [[jean-le-blanc]]
    |* [[jeu blanc]]
    |* [[la fée blanche]]
    |* [[lagénorhynque à flancs blancs]]
    |* [[lagopède à queue blanche]]
    |* [[laisser carte blanche]]
    |* [[lamier blanc]]
    |* [[ligne blanche]]
    |* [[lingue blanche]]
    |* [[lion blanc]]
    |* [[livre blanc]]
    |* [[lori à nuque blanche]]
    |* [[lumière blanche]]
    |* [[lupin blanc]]
    |* [[magie blanche]]
    |* [[makaire blanc]]
    |* [[makaire blanc de l’Atlantique]]
    |* [[mal blanc]]
    |* [[maladie des boyaux blancs]]
    |* [[manakin à ailes blanches]]
    |* [[manakin à col blanc]]
    |* [[manakin à tête blanche]]
    |* [[manakin à ventre blanc]]
    |* [[manchot à ailerons blancs]]
    |* [[manger son pain blanc]]
    |* [[manger son pain blanc le premier]]
    |* [[maquereau blanc]]
    |* [[marche blanche]]
    |* [[mariage blanc]]
    |* [[Marie-blanque]]
    |* [[marlin blanc]]
    |* [[marque blanche]]
    |* [[marquer d’une pierre blanche]]
    |* [[marrube à poils blancs]]
    |* [[marsouin blanc]]
    |* [[martinet à croupion blanc]]
    |* [[martinet à ventre blanc]]
    |* [[martre blanche]]
    |* [[matière blanche]]
    |* [[mauve blanche]]
    |* [[méliphage à front blanc]]
    |* [[méliphage à gorge blanche]]
    |* [[mer Blanche]]
    |* [[mérier-blanc]]
    |* [[merle blanc]]
    |* [[mérou blanc]]
    |* [[métal blanc]]
    |* [[millet blanc]]
    |* [[moine blanc]] ''(dominicain)''
    |* [[molène blanche]]
    |* [[molène bouillon-blanc]]
    |* [[mont Blanc]]
    |* [[mont-blanc]]
    |* [[Mont-Blanc]]
    |* [[montrer patte blanche]]
    |* [[mouette blanche]]
    |* [[mouron blanc]]
    |* [[mousseron blanc]]
    |* [[moustelle blanche]]
    |* [[murier blanc]] {{ortho1990}}
    |* [[mûrier blanc]]
    |* [[musaraigne à dents blanches]]
    |* [[naine blanche]]
    |* [[nègre blanc]]
    |* [[Nil Blanc]]
    |* [[noir et blanc]]
    |* [[non-blanc]]
    |* [[normalement blanc]]
    |* [[noyer blanc d’Amérique]]
    |* [[nuit blanche]]
    |* [[oie à front blanc]]
    |* [[oie blanche]]
    |* [[oie blanche du Bourbonnais]]
    |* [[oie blanche du Poitou]]
    |* [[oie bourbonnaise blanche]]
    |* [[oiseau blanc]]
    |* [[opération blanche]]
    |* [[opossum à oreilles blanches]]
    |* [[opossum à oreilles blanches des Andes]]
    |* [[or blanc]]
    |* [[oronge blanche]]
    |* [[orpin blanc]]
    |* [[ortie blanche]]
    |* [[oryx blanc]]
    |* [[osier blanc]]
    |* [[osyride blanche]]
    |* [[osyris blanc]]
    |* [[ours blanc]]
    |* [[PACS blanc]]
    |* [[page blanche]]
    |* [[pain blanc]]
    |* [[papillon blanc]]
    |* [[pavillon blanc]]
    |* [[pêche blanche]]
    |* [[pélican blanc]]
    |* [[perdrix blanche]]
    |* [[père blanc]]
    |* [[péridot blanc]]
    |* [[Périgord blanc]]
    |* [[pertes blanches]]
    |* [[peste blanche]]
    |* [[petite oie blanche]]
    |* [[pétrel à menton blanc]]
    |* [[peuplier blanc]]
    |* [[phalène blanche]]
    |* [[phosphore blanc]]
    |* [[phyllanthe à gorge blanche]]
    |* [[pian à oreilles blanches]]
    |* [[pic à dos blanc]]
    |* [[pierre blanche]]
    |* [[pieuvre blanche]]
    |* [[pin blanc]]
    |* [[pin blanc de Corée]]
    |* [[pin blanc de Provence]]
    |* [[pintade à poitrine blanche]]
    |* [[pione à couronne blanche]]
    |* [[piste blanche]]
    |* [[pizza blanche]]
    |* [[plectrophane blanc]]
    |* [[plombe blanche]]
    |* [[point blanc]]
    |* [[poisson blanc]]
    |* [[poisson cardinal blanc]]
    |* [[porc blanc de l’Ouest]]
    |* [[pou à poche blanche]]
    |* [[poulpe blanc]]
    |* [[produit blanc]]
    |* [[puffin à face blanche]]
    |* [[puffin à menton blanc]]
    |* [[pygargue à queue blanche]]
    |* [[pyrite blanche]]
    |* [[pyroxène blanc]]
    |* [[quebracho blanc]]
    |* [[québracho blanc]] {{ortho1990}}
    |* [[racine-blanche]]
    |* [[radis blanc]]
    |* [[rage blanche]]
    |* [[raie blanche]]
    |* [[raie blanche bordée]]
    |* [[raie blanche douce]]
    |* [[raie blanche lissée]]
    |* [[rascasse blanche]]
    |* [[reblanchir]]
    |* [[reine blanche]]
    |* [[requin à aileron blanc]]
    |* [[requin à pointes blanches]]
    |* [[requin blanc]]
    |* [[requin-chabot à taches blanches]]
    |* [[requin-marteau aile blanche]]
    |* [[requin pointes blanches du large]]
    |* [[réséda blanc]]
    |* [[rhum blanc]]
    |* [[rognon blanc]] (testicule de mouton)
    |* [[roquette blanche]]
    |* [[rouge-queue à front blanc]]
    |* [[rougequeue à front blanc]]
    |* [[rouvet blanc]]
    |* [[russe blanc]]
    |* [[Russe blanc]]
    |* [[Russie blanche]]
    |* [[russule blanc ocre]]
    |* [[russule ocre et blanche]]
    |* [[Ruthénie blanche]]
    |* [[salamandre blanche]]
    |* [[salle blanche]]
    |* [[sapin blanc]]
    |* [[santoline blanche]]
    |* [[sardinelle blanche]]
    |* [[sauce blanche]]
    |* [[saule blanc]]
    |* [[scaphirhynque blanc]]
    |* [[schorl blanc]]
    |* [[se faire des cheveux blancs]]
    |* [[sédum blanc]]
    |* [[setter irlandais rouge et blanc]]
    |* [[sittelle à poitrine blanche]]
    |* [[sizerin blanc]]
    |* [[sizerin à croupion blanc]]
    |* [[souris blanche]]
    |* [[spatule blanche]]
    |* [[stilton blanc]]
    |* [[substance blanche]]
    |* [[sucre blanc]]
    |* [[syncope blanche]]
    |* [[tableau blanc]]
    |* [[tableau blanc interactif]] ([[TBI]])
    |* [[tenue blanche]]
    |* [[tenue blanche fermée]]
    |* [[tenue blanche ouverte]]
    |* [[terrier blanc West Highland]]
    |* [[terrier blanc West-Highland]]
    |* [[thon blanc]]
    |* [[tigre blanc]]
    |* [[tinamou à gorge blanche]]
    |* [[tissu adipeux blanc]]
    |* [[tornade blanche]]
    |* [[touille blanche]]
    |* [[tout-blanc]]
    |* [[tricholome noir et blanc]]
    |* [[trou blanc]]
    |* [[truffe blanche]]
    |* [[truffe blanche d’été]]
    |* [[turbith blanc]]
    |* [[ugni blanc]]
    |* [[vache figure blanche]]
    |* [[vanneau à queue blanche]]
    |* [[vautour blanc d’Égypte]]
    |* [[veine à sang blanc]]
    |* [[ver blanc]]
    |* [[vers blancs]]
    |* [[viande blanche]]
    |* [[vigne-blanche]]
    |* [[vin blanc]]
    |* [[voile blanc]]
    |* [[voir blanc]]
    |* [[voir en blanc]]
    |* [[voix blanche]]
    |* [[vote blanc]]
    |* [[zone blanche]]
    |* [[zorille à nuque blanche]]
    |* [[zurichois à queue blanche]]
    |{{)}}
    |
    |==== {{S|phrases}} ====
    |* [[bonnet blanc, blanc bonnet]]
    |* [[la bave du crapaud n’atteint pas la blanche colombe]]
    |* [[peu importe que le chat soit noir ou blanc pourvu qu’il attrape les souris]]
    |* [[rouge au soir et blanc au matin, c’est la journée du pèlerin]] ''(quand le ciel est rouge le soir et '''blanc''' le matin, c’est ordinairement un indice qu’il fera beau temps)''
    |
    |==== {{S|vocabulaire}} ====
    |* {{voir thésaurus|fr|blanc|boisson|vin}}
    |
    |==== {{S|traductions}} ====
    |{{trad-début|blanc (sens général)}}
    |* {{T|af}} : {{trad-|af|blank}}, {{trad+|af|wit}}
    |* {{T|sq}} : {{trad+|sq|bardhë}}
    |* {{T|de}} : {{trad+|de|weiß}}
    |* {{T|zgh}} : {{trad--|zgh|ⴰⵎⵍⵍⴰⵍ}}
    |* {{T|angevin}} : {{trad--|angevin|bianc}}
    |* {{T|en}} : {{trad+|en|white}}
    |* {{T|ang}} : {{trad-|ang|hwit}}
    |* {{T|ar}} : {{trad+|ar|أبيض|R=abyadh}}
    |* {{T|hy}} : {{trad+|hy|սպիտակ|tr=spitak}}
    |* {{T|az}} : {{trad+|az|ağ}}
    |* {{T|vbb}} : {{trad--|vbb|ˈwowor}}
    |* {{T|ba}} : {{trad--|ba|аҡ}}
    |* {{T|bm}} : {{trad--|bm|farajɛ}}
    |* {{T|eu}} : {{trad-|eu|zuri}}
    |* {{T|bourguignon}} : {{trad--|bourguignon|blanc}}, {{trad--|bourguignon|bianc}}, {{trad--|bourguignon|blainche|f}}
    |* {{T|brabançon}} : {{trad--|brabançon|witte}}
    |* {{T|br}} : {{trad+|br|gwenn}}
    |* {{T|bg}} : {{trad+|bg|бяло}}
    |* {{T|ca}} : {{trad+|ca|blanc}}
    |* {{T|shy}} : {{trad--|shy|amellal}}
    |* {{T|zh}} : {{trad+|zh|白色|tr=báisè}}
    |* {{T|cjs}} : {{trad--|cjs|ақ}}
    |* {{T|ko}} : {{trad+|ko|희다|tr=huida}}, {{trad+|ko|하얗다|tr=hayata}}
    |* {{T|kw}} : {{trad-|kw|gwyn}}
    |* {{T|co}} : {{trad+|co|biancu|m}}, {{trad+|co|bianca|f}}
    |* {{T|ht}} : {{trad--|ht|blan}}
    |* {{T|hr}} : {{trad-|hr|bela}}
    |* {{T|dmr}} : {{trad--|dmr|wawaˈrah}}
    |* {{T|da}} : {{trad+|da|hvid}}
    |* {{T|es}} : {{trad+|es|blanco}}, {{trad+|es|cano}}
    |* {{T|eo}} : {{trad+|eo|blanka}}
    |* {{T|et}} : {{trad+|et|valge}}
    |* {{T|fo}} : {{trad-|fo|hvítur}}
    |* {{T|fi}} : {{trad+|fi|valkea}}
    |* {{T|fc}} : {{trad--|fc|biainc}}
    |* {{T|frp}} : {{trad--|frp|blanc}}, {{trad--|frp|blanche|f}}
    |* {{T|fy}} : {{trad+|fy|wyt}}
    |* {{T|gd}} : {{trad-|gd|bàn}}, {{trad-|gd|geal}}
    |* {{T|ga}} : {{trad+|ga|bán}}
    |* {{T|gag}} : {{trad--|gag|biyaz}}, {{trad--|gag|ak}}
    |* {{T|cy}} : {{trad+|cy|gwyn|m}}, {{trad+|cy|gwen|f}}
    |* {{T|gaulois}} : {{trad--|gaulois|alba}}, {{trad--|gaulois|albos}}, {{trad--|gaulois|albios}}, {{trad--|gaulois|albanos}}, {{trad--|gaulois|uindos}}
    |* {{T|ka}} : {{trad+|ka|თეთრი|R=t’et’ri}}
    |* {{T|got}} : {{trad--|got|𐍈𐌴𐌹𐍄𐍃|R=ƕeits}}, {{trad--|got|𐌱𐌻𐌰𐌲𐌺𐍃|R=blagks}}, {{trad--|got|𐌱𐌰𐌻𐍃|R=bals}}
    |* {{T|el}} : {{trad+|el|λευκό|R=levkó}}, {{trad+|el|άσπρο|R=áspro}}
    |* {{T|grc}} : {{trad--|grc|λευκός|m}}
    |* {{T|gu}} : {{trad+|gu|સફેદ|R=safed}}
    |* {{T|hbo}} : {{trad--|hbo|לבן|dif=לָבָן|m}}
    |* {{T|hi}} : {{trad+|hi|सफेद|R=safed}}
    |* {{T|hu}} : {{trad+|hu|fehér}}
    |* {{T|sah}} : {{trad--|sah|үрүҥ}}, {{trad--|sah|маҥан}}
    |* {{T|id}} : {{trad+|id|putih}}
    |* {{T|ia}} : {{trad+|ia|blanc}}
    |* {{T|iu}} : {{trad-|iu|ᖃᑯᖅᑐᖅ|R=[[qakuqtuq]]}}
    |* {{T|is}} : {{trad+|is|hvítur}}
    |* {{T|ist}} : {{trad--|ist|bianco}}
    |* {{T|ruo}} : {{trad--|ruo|åb}}
    |* {{T|it}} : {{trad+|it|bianco|m}}, {{trad+|it|bianca|f}}
    |* {{T|ja}} : {{trad+|ja|白い|tr=shiroi}}
    |* {{T|krc}} : {{trad--|krc|акъ}}
    |* {{T|kk}} : {{trad+|kk|ақ|tr=aq}}
    |* {{T|kjh}} : {{trad--|kjh|ах}}
    |* {{T|km}} : {{trad+|km|ស|tr=saa}}
    |* {{T|ki}} : {{trad--|ki|mwerũ}}
    |* {{T|ky}} : {{trad+|ky|ак}}
    |* {{T|kog}} : {{trad--|kog|abúʧi}}
    |* {{T|avk}} : {{trad--|avk|batakaf}}
    |* {{T|kum}} : {{trad--|kum|акъ}}
    |* {{T|lo}} : {{trad+|lo|ສີຂາວ}}
    |* {{T|la}} : {{trad+|la|albus}}, {{trad+|la|candidus}}, {{trad+|la|canus}}
    |* {{T|lv}} : {{trad+|lv|balts}}
    |* {{T|lt}} : {{trad+|lt|balta}}
    |* {{T|lb}} : {{trad-|lb|wäiss}}
    |* {{T|ms}} : {{trad+|ms|putih}}
    |* {{T|mg}} : {{trad+|mg|fotsy}}
    |* {{T|gv}} : {{trad-|gv|bane}}, {{trad-|gv|vane}}
    |* {{T|yua}} : {{trad--|yua|sak}}
    |* {{T|mayennais}} : {{trad--|mayennais|bianc}}
    |* {{T|mn}} : {{trad+|mn|цагаан|R=tsagaan}}
    |* {{T|nl}} : {{trad+|nl|wit}}, {{trad+|nl|blank}}
    |* {{T|nog}} : {{trad--|nog|ак}}
    |* {{T|fra-nor}} : {{trad--|fra-nor|bllaunc}}, {{trad--|fra-nor|bllaunche|f}}
    |* {{T|nb}} : {{trad+|nb|hvit}}
    |* {{T|nn}} : {{trad-|no|kvit}}
    |* {{T|oc}} : {{trad+|oc|blanc}}
    |* {{T|uz}} : {{trad+|uz|oq}}
    |* {{T|pap}} : {{trad--|pap|blanko}}, {{trad--|pap|blanku}}
    |* {{T|fa}} : {{trad+|fa|سفید|tr=sefid}}
    |* {{T|pcd}} : {{trad--|pcd|blanc}}
    |* {{T|pl}} : {{trad+|pl|biały}}
    |* {{T|pt}} : {{trad+|pt|branco}}
    |* {{T|ro}} : {{trad+|ro|alb}}
    |* {{T|ru}} : {{trad+|ru|белый|R=biélyï}}
    |* {{T|se}} : {{trad--|se|vielgat}}
    |* {{T|sc}} : {{trad--|sc|albu}}
    |* {{T|sr}} : {{trad+|sr|бела}}
    |* {{T|zdj}} : {{trad--|zdj|-eu}}
    |* {{T|sk}} : {{trad+|sk|biely}}
    |* {{T|sl}} : {{trad+|sl|bel|m}}, {{trad-|sl|bela|f}}
    |* {{T|ses}} : {{trad--|ses|ikaray}}
    |* {{T|srn}} : {{trad--|srn|weti}}
    |* {{T|sv}} : {{trad+|sv|vit}}
    |* {{T|sw}} : {{trad-|sw|-eupe}}
    |* {{T|tl}} : {{trad-|tl|putî}}
    |* {{T|ta}} : {{trad+|ta|வெள்ளை|R=veḷḷai}}
    |* {{T|crh}} : {{trad--|crh|beyaz}}, {{trad--|crh|aq}}
    |* {{T|tt}} : {{trad+|tt|ак}}
    |* {{T|cs}} : {{trad+|cs|bílý}}
    |* {{T|cv}} : {{trad--|cv|шурă}}
    |* {{T|th}} : {{trad+|th|สีขาว}}, {{trad+|th|ขาว}}
    |* {{T|tox}} : {{trad--|tox|vusivus}}
    |* {{T|kim}} : {{trad--|kim|аӄ}}
    |* {{T|tpi}} : {{trad-|tpi|wait}}
    |* {{T|tyv}} : {{trad--|tyv|ак}}
    |* {{T|tsolyáni}} : {{trad--|tsolyáni|abásun}}
    |* {{T|tr}} : {{trad+|tr|beyaz}}, {{trad+|tr|ak}}
    |* {{T|tk}} : {{trad+|tk|ak}}
    |* {{T|uk}} : {{trad+|uk|білий}}
    |* {{T|uum}} : {{trad--|uum|бийаз}}
    |* {{T|vi}} : {{trad+|vi|trắng}}
    |* {{T|wa}} : {{trad+|wa|blanc|m}}
    |* {{T|zu}} : {{trad+|zu|-mhlophe}}
    |{{trad-fin}}
    |
    |{{trad-début|Qui appartient à une race indo-européenne}}
    |* {{T|id}} : {{trad-|id|kulit putih}}
    |* {{T|nb}} : {{trad+|nb|hvit}}
    |* {{T|nn}} : {{trad-|no|kvit}}
    |* {{T|sv}} : {{trad+|sv|vit}}
    |* {{T|sw}} : {{trad-|sw|-a kizungu}}
    |* {{T|vi}} : {{trad-|vi|da trắng}}
    |* {{T|wa}} : {{trad+|wa|blanc|m}}
    |{{trad-fin}}
    |
    |=== {{S|nom|fr|num=1}} ===
    |{{fr-rég|blɑ̃}}
    |[[Image:Color icon white.svg|vignette|Le '''blanc''' (2) et ses divers '''blancs''' (3).]]
    |'''blanc''' {{pron|blɑ̃|fr}} {{m}}
    |# {{au singulier}} La [[couleur]] comme celle des [[os]], de la [[craie]] ou de l’[[écume]] entre autre.{{couleur|#FFFFFF}}
    |#* ''L’Alcazar de Séville et la mosquée de Cordoue, maintenant la cathédrale, étaient couvertes du haut en bas d’arabesques de couleur ; maintenant on a recouvert tout cela d’une couche de plâtre ; c’est l’usage de peindre tout en '''blanc''', c’est la seule propreté d’un pays où l’on mange des mouches dans la soupe dans les meilleures maisons. Par le même amour pour le '''blanc''', ils nettoyent avec du sable des statues antiques et les rendent aussi éclatantes que les figures d’albâtre des pendules que vous voyez dans la rue de Richelieu.'' {{source|{{w|Prosper Mérimée}}, ''Lettres d’Espagne'', 1832, réédition Éditions Complexe, 1989, page 141}}
    |#* ''Le Beerenberg apparut fier et arrogant dans une brusque et étroite déchirure des nuées ; le '''blanc''' immaculé de son sommet éclairé par le soleil se détachait dans une auréole d’azur.'' {{source|{{w|Jean-Baptiste Charcot}}, ''Dans la mer du Groenland'', 1928}}
    |#* ''La façade était peinte en '''blanc''' et les arêtes de la corniche se rehaussaient d’un filet rouge qui en accentuait le profil.'' {{source|{{Citation/Francis Carco/L’Homme de minuit/1938}}}}
    |#* ''Il déboucha bientôt devant plusieurs chalets, construits en planches comme le dernier, avec chacun une sorte de véranda mal peinte en '''blanc''', […].'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|381}}}}
    |# {{au singulier}} {{par ext}} [[terme|Terme]] générique qualifiant un des onze [[champ chromatique|champs chromatiques]] français, couvrant les différentes [[variante]]s de la couleur [[#fr-adj|blanche]].{{couleur|#FFFFFF}}{{couleur|#FFFFD4}}{{couleur|#FDE9E0}}{{couleur|#FDE1B8}}{{couleur|#F2FFFF}}{{couleur|#EFEFEF}}
    |#* ''Voilà que le '''blanc''' regagne la faveur et suscite la ferveur quand il se teinte de tons chauds, de jaunes, de rouges et d’orangés.'' {{source|[//www.sico.ca/fr/Couleur_Interieur_Blanc.asp www.sico.ca]}}
    |# {{au pluriel}} Les couleurs de ces variantes [[appartenir|appartenant]] au champ chromatique du blanc.
    |#* ''L’avantage de cette diversification est de pouvoir marier et opposer les '''blancs''', de jouer avec les effets, et de les accorder au mieux avec les couleurs.'' {{source|Olivier Waché, ''[//www.cotemaison.fr/atelier-deco/mur-blanc-et-couleurs-comment-reveiller-votre-interieur_9443.html, Mur blanc et couleurs : comment réveiller votre intérieur]'' article paru dans ''Maison Magazine'' de mars-avril 2011}}
    |#* ''Les '''blancs''' subtils, légèrement colorés, s’annoncent prédominants en véritables leaders des courants émergents.'' {{source|[//www.sico.ca/fr/Couleur_Interieur_Blanc.asp www.sico.ca]}}
    |# {{particulier}} {{lexique|maçonnerie|peinture|fr}} {{info lex|cuisine}} Couleur ou [[matière]] blanche que les [[peintre]]s, les [[maçon]]s, etc., emploient pour [[rendre]] une [[surface]] blanche.
    |#* '''''Blanc''' de plomb (sous-carbonate de plomb), de zinc (oxyde de zinc), de céruse, de lait, de perles.''
    |#* ''Nuance du '''blanc''' semblable à celle du lait, des perles.
    |#* ''Broyer, peindre du '''blanc'''.''
    |#* ''Une couche de '''blanc'''.''
    |#* '''''Blanc''' sale : couleur blanche dont l’apparence est terne, sans éclat.''
    |#* ''Cette étoffe, ce papier sont d’un '''blanc''' sale.''
    |#* ''Mets au '''blanc''' : mets accommodé à une sauce blanche.''
    |#* ''Un plat de cardons, des laitues, un poulet au '''blanc'''.''
    |# [[silence|Silence]] au cours d’une [[conversation]].
    |#* ''Seuls les poumons d’un train ou d’un bateau auraient pu lancer le cri des joies qu’avaient soulevées dans nos ventres les éclats de sa petite voix fatiguée et les halètements doux de ses '''blancs''' de parole.'' {{source|{{w|Réjean Ducharme}}, ''L’hiver de force'', Gallimard, 1973, page 85}}
    |# [[lacune|Lacune]], oubli, absence, vide.
    |#* ''Il faut dire qu’il y a eu un '''blanc''' dans son mode d’emploi de la vie.'' {{source|{{w|Annie Ernaux}}, ''La femme gelée'', 1981, réédition Quarto Gallimard, page 344}}
    |# [[espace|Espace]] [[vide]], sur une [[feuille]] de [[papier]], sur un [[formulaire]] réservé pour être rempli plus tard ou [[intervalle]] sur une [[page]].
    |#* ''Le notaire a laissé des '''blancs''' dans le contrat pour y mettre les noms des contractants et la somme dont ils conviendront.''
    |#* ''Laisser beaucoup de '''blanc''' entre le titre et la matière.''
    |#* ''Devancé par tous les autres clercs, il s’empara bravement d’une ''déclaration'' très difficile à rédiger et à laquelle personne n’avait voulu mordre; mais il n’avait pas encore parcouru la moitié des titres qu’il fallait analyser, que son imagination prit encore une fois la clé des champs, et lorsqu’après une heure de travail, M. Dumon vint regarder par-dessus son épaule, afin de voir comment il se tirait d’affaire, […].<br>— Tiens, s’écria le patron, vous m’avez fait l’ouvrage d’un '''''blanc'''''.<br>— C’est que M. Guérin ne travaille pas comme un ''nègre'', observa malicieusement le premier clerc.'' {{source|{{w|Pierre-Joseph-Olivier Chauveau}}, ''[[w:Charles Guérin (roman)|Charles Guérin]]'', G.H. Cherrier, éditeur, Montréal, 1853, I, 5, page 74}}
    |# {{ellipse|fr}} {{lexique|œnologie|fr}} {{familier|fr}} [[vin blanc|Vin blanc]] ou un [[verre]] de ce [[vin]]
    |#* ''Le vin rouge succéda au vin blanc et fut loué congrûment lui aussi, puis le '''blanc''' refit de nouveau son apparition, mais cette fois sous la forme d’une bouteille cachetée.'' {{source|{{w|Louis Pergaud}}, ''[[s:Joséphine est enceinte (suite et fin)|Joséphine est enceinte]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
    |#* ''Au comptoir, perché sur un tabouret, on déguste un verre de '''blanc''', un café, avec une farandole de tapas et chipoteries, «grignoteries» en tout genre.'' {{source|Nathalie Nallet, ''De bouches à oreilles'', chez l'auteur/Éditions Books on Demand, 2021, page 118}}
    |# {{info lex|cuisine}} [[chair|Chair]] cuite de la [[poitrine]] de certains oiseaux.
    |#* ''Un '''blanc''' de poulet, de dinde, de chapon, de perdrix.''
    |#* '' ''10 décembre 42'' – On avait reçu un poulet ce matin et Lamangue est venu déjeuner ; au grand dam de Benoîte qui refusait catégoriquement que maman l’invite si je n’avais pas l’intention de l’épouser. Elle trouvait que c’était du gâchis de sacrifier nos '''blancs''' pour quelqu’un qui n’était pas de la famille.'' {{source|{{w|Benoîte Groult|Benoîte}} et {{w|Flora Groult|Flora}} Groult, ''Journal à quatre mains'', Denoël, 1962, page 250}}
    |# [[colorant|Colorants]] de couleur blanche.
    |#* '''Blanc''' de céruse, '''blanc''' de plomb, '''blanc''' de Troyes, '''blanc''' de bourre, etc.''
    |# {{désuet|fr}} {{Canada|fr}} [[linge|Linge]] de fil ou de coton blanc<ref>Cet usage est encore courant au [[Québec]].</ref>.
    |#* ''Magasin, vente de '''blanc'''.''
    |# {{lexique|botanique|fr}} Nom vulgaire de certains [[champignon]]s parasites (''Oïdium'') produisant des taches blanches à la surface des plantes, ainsi qu’à la maladie elle-même.
    |# {{vieilli|fr}} Anciennement, petite [[monnaie]] qui valait cinq deniers.
    |# [[albumen|Albumen]], ellipse de [[blanc d’œuf]].
    |#* ''[[battre les blancs en neige|Battre les '''blancs''' en neige]].''
    |# {{méton|fr}} [[correcteur liquide|Correcteur liquide]].
    |# {{figuré|fr}} Par [[exagération]], extrême opposé à noir
    |#* ''Si vous lui dites '''blanc''', il répondra noir, il se plaît à contredire.''
    |# {{ellipse|fr}} Blanc de [[fard]] : sorte de fard, de cosmétique qui fait paraître la peau blanche.
    |#* ''Cette femme met du '''blanc''', a du '''blanc'''.''
    |# {{lexique|agriculture|fr}} [[maladie|Maladie]] de végétaux [[caractériser|caractérisée]] par des [[moisissure]]s.
    |# {{lexique|sport|fr}} [[partie|Partie]] d’une [[cible]] la plus [[rapprocher|rapprochée]] du [[centre]], [[partir|partie]] qui est [[peindre|peinte]] en '''blanc'''.
    |#* ''Tirer au '''blanc'''.''
    |#* ''Donner, mettre dans le '''blanc'''.''
    |#* ''Tirer de but en '''blanc'''.
    |# {{lexique|militaire|fr}} Interruption périodique et très brève d’une émission de brouillage afin de contrôler l’émission de l’adversaire pour suivre ses variations de fréquence ou interrompre le brouillage en temps voulu.
    |# ''(Dominos)'' [[zéro|Zéro]], le [[domino]] vide : <big><big><big>&#x1F031;</big></big></big>
    |# {{lexique|typographie|fr}} {{familier|fr}} [[espace|Espace]] entre deux mots.
    |# {{lexique|militaire|relations internationales|fr}} [[note blanche|Note blanche]].
    |# Document vierge, revêtu d’une signature.
    |#* ''Voici encore des '''''blancs''''' du duc d’Orléans que vous remplirez comme vous le voudrez.'' {{source|{{w|Alfred de Vigny}}, ''{{ws|Cinq-Mars/XIX|Cinq-Mars}}'', chapitre XIX, 1826}}
    |
    |==== {{S|dérivés}} ====
    |{{(}}
    |* aller, passer, [[changer du blanc au noir]] ''(passer d’une opinion à l’opinion contraire, passer d’une extrémité à l’autre)''
    |* [[battre les blancs en neige]]
    |* [[blanc cassé]]
    |* [[blanc d’Espagne]]
    |* [[blanc d’œuf]]
    |* [[blanc de baleine]]
    |* [[blanc de Beltsville]] ''(Race de dindons)''
    |* [[blanc de blancs]]
    |* [[blanc de bourre]]
    |* [[blanc de céruse]]
    |* [[blanc de champignon]] ''(mycélium du champignon de Paris, utilisé pour sa culture)''
    |* [[blanc de chaux]] ''(eau dans laquelle on a délayé de la chaux et dont on peint les murailles)''
    |* [[blanc de Hotot]] ''(Race de lapins)''
    |* [[blanc de l’œil]] ''(partie de l’œil qui paraît blanche, cf. sclérotique)''
    |* [[blanc de l’Ouest]] ''(Race de porcs)''
    |* [[blanc de Meudon]]
    |* [[blanc de plomb]]
    |* [[blanc de poulet]]
    |* [[blanc de Saturne]]
    |* [[blanc de Troyes]]
    |* [[blanc de Vienne]] ''(Race de lapins)''
    |* [[blanc limé]]
    |* [[blanc souligné]]
    |* [[blanc tacheté de l’Est]] ''(Espèce de papillon)''
    |* [[chauffer à blanc]]
    |* [[chèque en blanc]]
    |* [[cheval qui boit dans son blanc]]
    |* [[coupe à blanc]]
    |* [[de but en blanc]]
    |* [[double-blanc]]
    |* [[en noir et blanc]]
    |* [[grand blanc]] ''(grand requin blanc)''
    |* [[gros blanc]]
    |* [[gros-blanc]]
    |* [[jusqu’au blanc des yeux]]
    |* [[mettre du noir sur du blanc]] ''(écrire, composer)''
    |* [[mettre noir sur blanc]]
    |* [[mouvement en blanc]] ''(mouvement d’horlogerie non monté dans sa boîte)''
    |* [[noir et blanc]]
    |* [[poudré à blanc]] ''(extrêmement poudré, de manière que la poudre cache entièrement la couleur des cheveux)''
    |* [[poule au blanc]]
    |* [[procuration en blanc]] ''(procuration où le nom de celui qui doit en être chargé est laissé en blanc)''
    |* [[regarder quelqu’un dans le blanc des yeux]] ''(regarder quelqu'un fixement, avec assurance ou impudence)''
    |* [[rougir jusqu’au blanc des yeux]] ''(rougir beaucoup)''
    |* [[saigner à blanc]]
    |* [[se manger le blanc des yeux]] ''(se quereller fortement)''
    |* [[se regarder dans le blanc des yeux]]
    |* [[six-blancs]]
    |{{)}}
    |
    |==== {{S|hyponymes}} ====
    |* [[incane]]
    |
    |==== {{S|traductions}} ====
    |{{trad-début|Couleur, colorant de couleur blanche}}
    |* {{T|de}} : {{trad+|de|Weiß|n}}
    |* {{T|en}} : {{trad+|en|white}}
    |* {{T|eu}} : {{trad-|eu|zuri}}
    |* {{T|br}} : {{trad+|br|gwenn}}
    |* {{T|bg}} : {{trad+|bg|бял|m}}
    |* {{T|zh}} : {{trad+|zh|白色|tr=báisè}}
    |* {{T|ko}} : {{trad+|ko|흰색|tradi=흰色|tr=huinsaek}}, {{trad+|ko|하얀색|tradi=하얀色|tr=hayansaek}}, {{trad+|ko|하양|tr=hayang}}, {{trad+|ko|화이트|tr=hwaiteu}}
    |* {{T|es}} : {{trad+|es|blanco}}
    |* {{T|io}} : {{trad+|io|blanka}}
    |* {{T|it}} : {{trad+|it|bianco|m}}
    |* {{T|ja}} : {{trad+|ja|白|tr=shiro}}, {{trad+|ja|白色|tr=hakushoku}}, {{trad+|ja|ホワイト|tr=howaito}}
    |* {{T|kl}} : {{trad+|kl|qaqortoq}}
    |* {{T|avk}} : {{trad--|avk|batake}}
    |* {{T|fsl}} : {{wikisign}}
    |* {{T|ln}} : {{trad-|ln|mpɛ́mbɛ́}}
    |* {{T|nl}} : {{trad+|nl|wit}}
    |* {{T|fra-nor}} : {{trad--|fra-nor|bllaunc}}
    |* {{T|ngh}} : {{trad--|ngh|ǃuria}}
    |* {{T|myp}} : {{trad--|myp|kobiaí}}
    |* {{T|pl}} : {{trad+|pl|biały}}
    |* {{T|pt}} : {{trad+|pt|branco}}
    |* {{T|ru}} : {{trad+|ru|белое|n}}
    |* {{T|se}} : {{trad--|se|vielgat}}
    |* {{T|ses}} : {{trad--|ses|ikaray}}, {{trad--|ses|annasaara}} (Ar)
    |* {{T|sv}} : {{trad+|sv|vitt}}
    |* {{T|ta}} : {{trad+|ta|வெள்ளை|R=veḷḷai}}
    |* {{T|tsolyáni}} : {{trad--|tsolyáni|abásukh}}
    |* {{T|wa}} : {{trad+|wa|blanc|m}}
    |{{trad-fin}}
    |
    |{{trad-début|Espace vide, sur une feuille de papier}}
    |* {{T|de}} : {{trad-|de|Leerraum|m}}
    |* {{T|en}} : {{trad+|en|blank}}, {{trad+|en|space}} (''white ~; floating ~, forced ~'')
    |* {{T|avk}} : {{trad--|avk|vlardaca}}
    |{{trad-fin}}
    |
    |{{cf|lang=fr|blanc d’œuf}}
    |
    |{{trad-début|19}}
    |* {{T|en}} : {{trad-|en|look-through}}
    |{{trad-fin}}
    |
    |=== {{S|nom|fr|num=2}} ===
    |{{fr-rég|blɑ̃}}
    |'''blanc''' {{pron|blɑ̃|fr}} {{m}} {{équiv-pour|lang=fr|une femme|blanche}}
    |# [[personne|Personne]] dont la peau est [[clair]]e, par opposition à [[noir]], [[jaune]], [[peau-rouge]], etc.
    |#* ''Il n’y a point ici de '''blancs''' et il n’y a point de noirs ; il y a des Africains du Nord, tous coiffés de la chéchia.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
    |#* ''Le long des boutiques éclairées, des ombres se succédaient sans hâte. Il y avait des nègres, quelques Chinois, des '''blancs'''. Tous reluquaient les filles.'' {{source|{{Citation/Francis Carco/Brumes/1935|41}}}}
    |#* ''Il y a présentement le risque d’une mythologie du nègre en soi qui n’est qu'une évasion du '''blanc''' devant ses propres problèmes. Il existe le danger d'une négrophilie philistine, d’une négrophilie sans obligation ni sanction ; […].'' {{source|Georges Mounin, Émile Dermemghem et Magdeleine Paz, « ''Premières réponses à l'enquête sur le « Mythe du Nègre »'' », dans ''Présence Africaine'' 1948/1 (n° 2), page 197}}
    |#* ''Comme le montre une étude récente, pendant longtemps, aux États-Unis, les immigrés juifs, irlandais ou italiens ne furent pas considérés comme '''"blancs"'''. C’est seulement une fois intégrés économiquement et culturellement à la bourgeoisie américaine qu’ils devinrent, dans l’imaginaire américain, '''"blancs"'''.'' {{source|Paul Klein, ''La reine d’Angleterre et autres fictions'', {{w|''Charlie Hebdo''}}, 7 janvier 2015}}
    |
    |==== {{S|dérivés}} ====
    |* [[traite des blanches]], racolage de femmes ou de jeunes filles en vue de la prostitution
    |
    |==== {{S|traductions}} ====
    |{{trad-début|Personne à la peau claire}}
    |* {{T|en}} : {{trad+|en|white}}
    |* {{T|bm}} : {{trad--|bm|farajɛ}}, {{trad--|bm|tubabu}}
    |* {{T|it}} : {{trad+|it|bianco|m}}, {{trad+|it|bianca|f}}
    |* {{T|avk}} : {{trad--|avk|aftalmik}}
    |* {{T|ngh}} : {{trad--|ngh|ǀhûusi}}
    |* {{T|ff}} : {{trad--|ff|daneejo}}
    |* {{T|zdj}} : {{trad--|zdj|mzungu}}, {{trad--|zdj|mndru wa rangi nyinkuɗu|tr=mndru wa rangi nyinkudu}}
    |{{trad-fin}}
    |
    |=== {{S|prononciation}} ===
    |* {{pron-rimes|blɑ̃|fr}}
    |* {{pron|blɑ̃|fr}}
    |** {{écouter|lang=fr|France|blɑ̃|audio=Fr-blanc.ogg}}
    |** {{écouter||blɑ̃|lang=fr|audio=LL-Guilhelma-fr-blanc.wav}}
    |** {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Roll-Morton-blanc.wav}}
    |* Français méridional : {{pron|blaŋ|fr}}
    |* Canada : {{pron|blã|fr}}
    |* {{écouter|lang=fr|France (Massy)|audio=LL-Q150 (fra)-X-Javier-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-GrandCelinien-blanc.wav}}
    |* {{écouter|lang=fr|France (Occitanie)|audio=LL-Q150 (fra)-Arthur Crbz-blanc.wav}}
    |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-blanc.wav}}
    |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Penegal-blanc.wav}}
    |* {{écouter|lang=fr|France (Grenoble)|audio=LL-Q150 (fra)-GAllegre-blanc.wav}}
    |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Ltrlg-blanc.wav}}
    |* {{écouter|||lang=fr|audio=LL-Q150 (fra)-WikiLucas00-blanc.wav}}
    |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Justforoc-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines1-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines2-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines3-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines5-blanc.wav}}
    |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Aemines4-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines6-blanc.wav}}
    |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-blanc.wav}}
    |* {{écouter|lang=fr|France (Paris)||audio=LL-Q150 (fra)-Ash Crow-blanc.wav}}
    |* {{écouter|lang=fr|France (Saint-Étienne)||audio=LL-Q150 (fra)-Touam-blanc.wav}}
    |* {{écouter|lang=fr|||audio=LL-Q150 (fra)-Madehub-blanc.wav}}
    |* {{écouter|lang=fr|Canada (Shawinigan)||audio=LL-Q150 (fra)-DenisdeShawi-blanc.wav}}
    |* {{écouter|lang=fr|France (Hérault)||audio=LL-Q150 (fra)-T. Le Berre-blanc.wav}}
    |* {{écouter|lang=fr|Suisse (Lausanne)||audio=LL-Q150 (fra)-Eihel-blanc.wav}}
    |* {{écouter|lang=fr|||audio=LL-Q150 (fra)-Kaderousse-blanc.wav}}
    |* {{écouter|lang=fr|France (Metz)||audio=LL-Q150 (fra)-val (Eavq)-blanc.wav}}
    |* {{écouter|lang=fr|France||audio=LL-Q150 (fra)-Mecanautes-blanc.wav}}
    |* {{écouter|lang=fr|France||audio=LL-Q150 (fra)-Julien Baley-blanc.wav}}
    |* {{écouter|lang=fr|||audio=LL-Q150 (fra)-Fabe56-blanc.wav}}
    |* {{écouter|lang=fr|France (Toulouse)||audio=LL-Q150 (fra)-Loumaju-blanc.wav}}
    |* {{écouter|lang=fr|Tchad (N'Djaména)||audio=LL-Q150 (fra)-Matarimi1-blanc.wav}}
    |
    |=== {{S|voir aussi}} ===
    |* {{Le Dico des Ados}}
    |* {{Vikidia}}
    |* {{Thésaurus|fr|blanc|couleur}}
    |* [[Bourg-Blanc]] (Bretagne)
    |* [[Cap Blanc]] (Tunisie, Mauritanie, Maroc, Île Miquelon)
    |* [[Lac Blanc]] (Vosges, Russie)
    |* [[Le Blanc]] (Indre)
    |* [[mer Blanche]]
    |* [[Mont-Blanc]]
    |
    |=== {{S|références}} ===
    |* {{R:FranceTerme}}
    |* {{R:DAF8}}
    |* {{RÉF}} {{R:TLFi}}
    |* {{R:FranceTerme}}
    |{{Références}}
    |
    |[[Catégorie:Vins en français]]
    |[[Catégorie:Couleurs blanches en français]]
    |[[Catégorie:Couleurs de robes de chevaux en français]]
    |[[Catégorie:Ethnonymes informels en français]]
    |
    |== {{langue|fro}} ==
    |=== {{S|étymologie}} ===
    |: Du {{étyl|frk|fro}} {{recons|lang-mot-vedette=fro|blank}}.
    |
    |=== {{S|adjectif|fro}} ===
    |{{fro-adj|mss=blans|mrp=blans|fs=blanche|fp=blanches}}
    |'''blanc''' {{pron|blãnk|fro}}
    |# [[blanc#fr|Blanc]].
    |#* ''Ses haubers est coverz de sanc:<br>De roge i a plus que de '''blanc'''.'' {{source|[[w:Chrétien de Troyes|Chrétien de Troyes]], ''[[s:Érec et Énide|Érec et Énide]]'', 1170}}
    |
    |=== {{S|nom|fro}} ===
    |{{fro-nom-m|ss=blans|rp=blans}}
    |'''blanc''' {{pron|blãnk|fro}}
    |# [[blanc#fr|Blanc]].
    |
    |==== {{S|dérivés autres langues}} ====
    |* {{L|en}} : {{lien|blank|en}}
    |* {{L|frm}} : {{lien|blanc|frm}}
    |** {{L|fr}} : {{lien|blanc|fr}}
    |
    |[[Catégorie:Couleurs en ancien français]]
    |
    |== {{langue|pro}} ==
    |=== {{S|étymologie}} ===
    |: Du {{étyl|frk|pro}} {{recons|lang-mot-vedette=pro|blank}}.
    |
    |=== {{S|adjectif|pro}} ===
    |'''blanc'''
    |# [[blanc#fr|Blanc]].
    |
    |==== {{S|variantes}} ====
    |* [[blanx]]
    |* [[blanch]]
    |
    |=== {{S|nom|pro}} ===
    |'''blanc''' {{m}}
    |# [[blanc#fr|Blanc]], sorte de monnaie.
    |
    |=== {{S|références}} ===
    |* {{R:Raynouard}}
    |
    |== {{langue|ca}} ==
    |=== {{S|étymologie}} ===
    |: Du {{étyl|frk|ca}} {{recons|lang-mot-vedette=ca|blank}}.
    |
    |=== {{S|adjectif|ca}} ===
    |{{ca-accord-c|blan|pron=ˈblaŋk}}
    |'''blanc''' {{pron|ˈblaŋk|ca}}
    |# [[#fr-adj|Blanc]].
    |
    |=== {{S|nom|ca}} ===
    |'''blanc''' {{pron|ˈblaŋk|ca}}
    |# [[but|But]], [[dessein]].
    |
    |==== {{S|synonymes}} ====
    |* [[fi]]
    |* [[finalitat]]
    |* [[objectiu]]
    |
    |=== {{S|prononciation}} ===
    |* {{écouter|lang=ca|Espagne (Barcelone)|ˈblaŋk|audio=LL-Q7026 (cat)-Toniher-blanc.wav}}
    |* {{écouter|lang=ca|Espagne (Villarreal)|ˈblaŋk|audio=LL-Q7026 (cat)-Millars-blanc.wav}}
    |
    |[[Catégorie:Couleurs en catalan]]
    |
    |== {{langue|oc}} ==
    |=== {{S|étymologie}} ===
    |: De l'{{étyl|pro|oc|blanc}}.
    |
    |=== {{S|adjectif|oc}} ===
    |'''blanc''' {{oc-norme classique}}
    |# [[blanc#fr-adj|Blanc]].
    |
    |=== {{S|nom|oc}} ===
    |'''blanc''' {{oc-norme classique}}
    |# [[blanc#fr-nom|Blanc]].
    |
    |=== {{S|prononciation}} ===
    |* {{écouter|lang=oc|France (Béarn)|audio=LL-Q14185 (oci)-Davidgrosclaude-blanc.wav}}
    |
    |=== {{S|références}} ===
    |* {{R:Cantalausa}}
    |
    |[[Catégorie:Couleurs en occitan]]
    |""".stripMargin
  )

}
