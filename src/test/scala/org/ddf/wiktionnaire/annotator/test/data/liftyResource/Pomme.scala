package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Pomme {
  val resource = Resource("pomme","https://fr.wiktionary.org/wiki/","entry","""== {{langue|fr}} ==
                                                                                   |=== {{S|étymologie}} ===
                                                                                   |: {{siècle|XII|lang=fr}} Du {{étyl|frm|fr|pomme}}{{R|DMF}}, de l’{{étyl|fro|fr|pome}}, {{lien|''pume''|fro}}{{R|DÉCT|pome}}, du {{étyl|bas latin|fr|mot=poma|sens=fruit du pommier}}, du latin {{lien|''poma''|la}}, pluriel neutre de {{lien|''pomum''|sens=fruit [à pépin ou noyau]|la}}, dont le sens dérive en « fruit du pommier » en bas latin en Gaule et dans le nord de l’Italie et qui remplace le latin classique {{lien|malum|dif=''mālum''|sens=fruit du pommier|la}}{{R|TLFi}}. Cognat du catalan {{lien|''poma''|sens=pomme|ca}}.
                                                                                   |
                                                                                   |=== {{S|nom|fr}} ===
                                                                                   |{{fr-rég|pɔm}}
                                                                                   |[[Fichier:Red Apple.jpg|vignette|Une '''pomme''' ''(sens 1)'']]
                                                                                   |[[Fichier:Watering Can Spout.jpg|vignette|Une '''pomme''' d’arrosoir ''(sens 2)'']]
                                                                                   |'''pomme''' {{pron|pɔm|fr}} {{f}}
                                                                                   |# [[fruit|Fruit]] [[comestible]] du [[pommier]] à la [[chair]] [[croquant]]e et à la [[peau]] [[colorée]] et [[variée]] suivant les [[variété]]s. — {{note}} C’est un [[faux-fruit]] au sens botanique.
                                                                                   |#* '' La '''pomme''' est un fruit charnu à 5 loges cartilagineuses, issues des 5 carpelles du pistil floral et contenant les pépins, anciens ovules fécondés et futures graines. '' {{source|Delahaye Thierry, Vin Pascal, ''Le Pommier'', Actes Sud, Le Nom de l’arbre, 1997, page 33}}
                                                                                   |#* '' Comment se priver d’un fruit riche en vitamines, en sucres, en sels minéraux et en acides organiques… ? […] « Une '''pomme''' chaque jour éloigne le médecin ». '' {{source|Delahaye Thierry, Vin Pascal, ''Le Pommier'', Actes Sud, Le Nom de l’arbre, 1997, page 63}}
                                                                                   |#* ''Un jour ils le mandèrent au salon et lui dirent, avec toute la douceur et la bienveillance possible, que la veille, dînant au château voisin, ils avaient mangé des '''pommes''' et des poires si parfumées, si savoureuses, si exquises, que tous les convives en avaient exprimé leur admiration.'' {{source|{{w|Hans Christian Andersen}}, ''[[s:Le Jardinier et ses maîtres|Le Jardinier et ses maîtres]]}}
                                                                                   |#* ''Je vais m’étendre dans la prairie, sous un pommier aux '''pommes''' vertes et dures. Je peux dormir au-dessous d’elles, je peux les contempler sans crainte, et même sans l’appréhension d’avoir à inventer, l’une tombant, les lois du monde.'' {{source|{{w|Jean Giraudoux}}, ''Retour d’Alsace - Août 1914'', 1916}}
                                                                                   |#* ''[…] mais l’on n’était qu’en juin et, sauf pour les poires de moisson qui mûrissent en août, il fallait encore attendre longtemps avant de savourer concurremment les '''pommes''' du verger et la vengeance désirée.'' {{source|{{w|Louis Pergaud}}, ''[[s:Une revanche|Une revanche]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                                                                                   |# Tout objet [[en forme de]] pomme.
                                                                                   |## Partie d’un [[arrosoir]], souvent [[amovible]], par laquelle l’eau s’[[échappe]] en [[pluie]].
                                                                                   |##* ''La '''pomme''' d’arrosoir.''
                                                                                   |## Partie d’une [[douche]], percée et arrondie, fixée au bout d’un [[flexible]] de douche, pour se laver.
                                                                                   |## {{architecture|fr}} Motif décoratif en forme de pomme.
                                                                                   |## {{botanique|nocat=1}} Cœur d’un [[chou]], d’une [[laitue]].
                                                                                   |# Fruit de plusieurs végétaux.
                                                                                   |#* '''''Pomme''' de cajou, '''pomme''' épineuse.''
                                                                                   |# Fruit interdit du [[paradis]] terrestre.
                                                                                   |#* ''Eve croqua la '''pomme'''.''
                                                                                   |#* ''Il faut avouer, que notre mère Ève était bien gourmande, d’avoir mangé de la '''pomme'''.'' {{source|{{w|Jeanne-Marie Leprince de Beaumont}}, ''[[s:La Curiosité|La Curiosité]]''}}
                                                                                   |# {{cuisine|nocat=1}} {{ellipse|fr}} [[pomme de terre|Pomme de terre]].
                                                                                   |#* '''''Pommes''' frites.''
                                                                                   |# {{familier|fr}} Tête, visage d’une personne.
                                                                                   |#* ''Il s’est bien payé ma '''pomme'''.''
                                                                                   |# {{familier|fr}} Par extension, accompagné d’un adjectif possessif : forme pronominale désignant la personne elle-même. Synonymes : [[moi]] ([[mézigue]], [[bibi]]), [[toi]], [[lui]], etc.
                                                                                   |#* ''Et tu sais qui a gagné au final ? Ma '''pomme''' !''
                                                                                   |#* ''OK, je veux bien écraser le champignon, mais les [[amende]]s ce sera pour ta '''pomme'''.''
                                                                                   |# {{familier|fr}} Personne [[naïf|naïve]], dont on se moque facilement.
                                                                                   |#* ''C’est la reine des '''pommes''' !''
                                                                                   |# {{figuré|fr}} Prix de beauté, par allusion au mythologique jugement de [[Pâris]].
                                                                                   |#* ''Elle mérite la '''pomme'''.''
                                                                                   |# {{Canada|fr}} {{term|Généralement péjoratif de la part des Amérindiens}} Personne [[amérindien]]ne [[acculturer|acculturée]] par les [[Blancs]].
                                                                                   |#* ''Car bien que transplantée dans le terreau d'une famille ukrainienne aimante, Sandy n'en a pas moins, de sa jeunesse jusqu'à sa vie adulte, vécu cet héritage comme une malédiction la confinant à la haine de soi. Devenue une « '''pomme''' » au fil des ans, terme utilisé pour qualifier ces Amérindiens rouges à l'extérieur et blancs à l'intérieur, javellisés par la pression d'avoir à correspondre à la culture majoritaire, la journaliste, épaulée par son ami Kyle et le guide spirituel Joe Bush Sr., troquera sa pelure pour une peau d'ours, éminent symbole de sa reconversion.'' {{source|''[[w:Nuit blanche (revue)|Nuit blanche]]'', n° 152, automne 2018, page 48}}
                                                                                   |# [[Fichier:Vemmenhög vapen.svg|vignette|120px|Armoiries avec 3 '''pommes''' ''(sens héraldique)'']] {{meubles héraldiques|fr}} {{rare|fr}} [[meuble|Meuble]] représentant un disque de couleur verte dans les [[armoiries]]. Il s’agit d’un ancien nom utilisé pour le tourteau [[de sinople]]. À rapprocher de [[besant]], [[besant-tourteau]], [[buse]], [[gulpe]], [[guse]], [[heurte]], [[œil de faucon]], [[ogoesse]], [[plate]], [[somme]], [[tourteau]], [[tourteau-besant]] et [[volet]].
                                                                                   |#* ''D'argent à trois tourteaux de sinople ('''pommes''') mal-ordonnés, qui est du Hundred Vemmenhög en Suède'' {{cf|lang=fr}} illustration « armoiries avec 3 pommes »
                                                                                   |
                                                                                   |==== {{S|synonymes}} ====
                                                                                   |''Tête (5)'' :
                                                                                   |* [[tête]]
                                                                                   |* [[visage]]
                                                                                   |
                                                                                   |''Héraldique'' :
                                                                                   |* [[somme#héraldique|somme]]
                                                                                   |* [[volet#héraldique|volet]]
                                                                                   |
                                                                                   |==== {{S|dérivés}} ====
                                                                                   |{{(}}
                                                                                   |* [[bonne pomme]]
                                                                                   |* [[croquer la pomme]]
                                                                                   |* [[comparer des pommes et des bananes]]
                                                                                   |* [[cueillir la pomme]]
                                                                                   |* [[être dans les pommes]]
                                                                                   |* [[Grosse Pomme]]
                                                                                   |* [[haut comme trois pommes]]
                                                                                   |* [[jus de pomme]]
                                                                                   |* [[la Grosse Pomme]]
                                                                                   |* [[ma pomme]]
                                                                                   |* [[marque à la pomme]]
                                                                                   |* [[passe-pomme]]
                                                                                   |* [[pomiforme]]
                                                                                   |* [[pommage]]
                                                                                   |* [[pommé]]
                                                                                   |* [[pomme à cidre]]
                                                                                   |* [[pomme à couteau]], [[pomme de couteau]]
                                                                                   |* [[pomme à cuire]]
                                                                                   |* [[pomme cajou]]
                                                                                   |* [[pomme cannelle]]
                                                                                   |* [[Pomme Clochard]]
                                                                                   |* [[pomme d’abricot]]
                                                                                   |* [[pomme d’Adam]]
                                                                                   |* [[pomme d’amour]]
                                                                                   |* [[pomme de Calville]]
                                                                                   |* [[pomme de discorde]]
                                                                                   |* [[pomme de l’air]]
                                                                                   |* [[pomme de Médée]]
                                                                                   |* [[pomme de merveille]]
                                                                                   |* [[pomme de Paradis]]
                                                                                   |* [[pomme de table]]
                                                                                   |* [[pomme de terre]]
                                                                                   |* [[pomme de terre en robe de chambre]]
                                                                                   |* [[pomme de terre en robe des champs]]
                                                                                   |* [[pomme de pin]], [[pomme-de-pin]]
                                                                                   |* [[pomme épineuse]]
                                                                                   |* [[pomme-grenade]] {{botan|nocat=1}} {{Québec|nocat=1}}
                                                                                   |* [[pommeau]]
                                                                                   |* [[pomme liane]]
                                                                                   |* [[pomme-liane]]
                                                                                   |* [[pomme paille]]
                                                                                   |* [[pommer]]
                                                                                   |* [[pommeraie]]
                                                                                   |* [[pommes frites]]
                                                                                   |* [[pomme tapée]]
                                                                                   |* [[pommette]]
                                                                                   |* [[pommier]]
                                                                                   |* [[pomo]]
                                                                                   |* [[sa pomme]], [[ma pomme]], [[ta pomme]]
                                                                                   |* [[se payer la pomme]] de quelqu’un
                                                                                   |* [[sucer la pomme]]
                                                                                   |* [[tomber dans les pommes]]
                                                                                   |* [[vert pomme]]
                                                                                   |* [[vide-pomme]]
                                                                                   |{{)}}
                                                                                   |
                                                                                   |==== {{S|vocabulaire}} ====
                                                                                   |* {{voir thésaurus|fr|fruit|graine}}
                                                                                   |
                                                                                   |==== {{S|phrases}} ====
                                                                                   |* [[chaque jour une pomme conserve son homme]] ; [[une pomme chaque matin éloigne le médecin]] ({{T|en}} : ''{{trad+|en|an apple a day keeps the doctor away}}'' ; {{T|it}} : ''{{trad-|it|una mela al giorno toglie il medico di torno}}'' ; ''{{trad-|it|una mela al giorno leva il medico di torno}}'' ; {{T|pt}} : ''{{trad-|pt|uma maçã por dia traz saúde e alegria}}'')
                                                                                   |* [[la pomme ne tombe jamais loin de l’arbre]]
                                                                                   |
                                                                                   |==== {{S|traductions}} ====
                                                                                   |{{trad-début|Fruit}}
                                                                                   |* {{T|af}} : {{trad+|af|appel}}
                                                                                   |* {{T|sq}} : {{trad+|sq|mollë}}
                                                                                   |* {{T|de}} : {{trad+|de|Apfel}} {{m}}
                                                                                   |* {{T|en}} : {{trad+|en|apple}}
                                                                                   |* {{T|ang}} : {{trad+|ang|æppel}}
                                                                                   |* {{T|ar}} : {{trad-|ar|تفاحة}}
                                                                                   |* {{T|arq}} : {{trad--|arq|تفاح}}
                                                                                   |* {{T|hy}} : {{trad+|hy|խնձոր|tr=xnjor}}
                                                                                   |* {{T|az}} : {{trad+|az|alma}}
                                                                                   |* {{T|ba}} : {{trad--|ba|алма}}
                                                                                   |* {{T|eu}} : {{trad+|eu|sagar}}
                                                                                   |* {{T|bar}} : {{trad--|bar|Opfi|m}}
                                                                                   |* {{T|be}} : {{trad-|be|яблык|m|tr=iablik}}
                                                                                   |* {{T|br}} : {{trad+|br|aval}}
                                                                                   |* {{T|bg}} : {{trad+|bg|ябълка}} {{f}}
                                                                                   |* {{T|ca}} : {{trad+|ca|poma}}
                                                                                   |* {{T|shy}} : {{trad--|shy|aḍeffu}}
                                                                                   |* {{T|zh}} : {{trad+|zh|苹果|R=píngguǒ|tradi=蘋果}}
                                                                                   |* {{T|ko}} : {{trad+|ko|사과|R=sagwa}}
                                                                                   |* {{T|kw}} : {{trad+|kw|aval}}
                                                                                   |* {{T|co}} : {{trad-|co|mela}}
                                                                                   |* {{T|ht}} : {{trad--|ht|pòm}}
                                                                                   |* {{T|hr}} : {{trad+|hr|jabuka}}
                                                                                   |* {{T|da}} : {{trad+|da|æble}}
                                                                                   |* {{T|es}} : {{trad+|es|manzana}} {{f}}
                                                                                   |* {{T|eo}} : {{trad+|eo|pomo}}
                                                                                   |* {{T|et}} : {{trad+|et|õun}}
                                                                                   |* {{T|fo}} : {{trad+|fo|súrepli}}
                                                                                   |* {{T|fi}} : {{trad+|fi|omena}}
                                                                                   |* {{T|nld}} : {{trad--|nld|abbel}}, {{trad--|nld|abbele}}
                                                                                   |* {{T|francique mosellan}} : {{trad--|francique mosellan|Aapel}}
                                                                                   |* {{T|fy}} : {{trad+|fy|apel}}
                                                                                   |* {{T|gd}} : {{trad-|gd|ubhal}}
                                                                                   |* {{T|ga}} : {{trad+|ga|úll}}
                                                                                   |* {{T|gag}} : {{trad--|gag|alma}}
                                                                                   |* {{T|gl}} : {{trad+|gl|mazá}}
                                                                                   |* {{T|gallo}} : {{trad--|gallo|pome|f}}
                                                                                   |* {{T|cy}} : {{trad+|cy|afal}}
                                                                                   |* {{T|gaulois}} : {{trad--|gaulois|Reconstruction:gaulois/*abalo-|dif=*abalo-}}
                                                                                   |* {{T|ka}} : {{trad+|ka|ვაშლი|tr=vašli}}
                                                                                   |* {{T|el}} : {{trad+|el|μήλο|R=mílo}} {{n}}
                                                                                   |* {{T|griko}} : {{trad--|griko|milo}} {{n}}
                                                                                   |* {{T|he}} : {{trad+|he|תפוח}}
                                                                                   |* {{T|hi}} : {{trad+|hi|सेब|tr=seb}}
                                                                                   |* {{T|hu}} : {{trad+|hu|alma}}
                                                                                   |* {{T|io}} : {{trad+|io|pomo}}
                                                                                   |* {{T|id}} : {{trad+|id|apel}}
                                                                                   |* {{T|ia}} : {{trad-|ia|pomo}}
                                                                                   |* {{T|is}} : {{trad+|is|epli}}
                                                                                   |* {{T|it}} : {{trad+|it|mela}} {{f}}
                                                                                   |* {{T|ja}} : {{trad+|ja|林檎|tr=ringo}}, {{trad+|ja|りんご|tr=ringo}}
                                                                                   |* {{T|krc}} : {{trad--|krc|алма}}
                                                                                   |* {{T|kk}} : {{trad+|kk|алма|tr=alma}}
                                                                                   |* {{T|ky}} : {{trad+|ky|алма}}
                                                                                   |* {{T|kum}} : {{trad--|kum|алма}}
                                                                                   |* {{T|ku}} : {{trad+|ku|sêv}}
                                                                                   |* {{T|la}} : {{trad+|la|malum|dif=mālum|n}}
                                                                                   |* {{T|lv}} : {{trad+|lv|ābols}}
                                                                                   |* {{T|lt}} : {{trad+|lt|obuolys}}
                                                                                   |* {{T|lb}} : {{trad+|lb|Apel}}
                                                                                   |* {{T|ms}} : {{trad-|ms|buah epal}}, {{trad-|ms|apel}}
                                                                                   |* {{T|mg}} : {{trad+|mg|paoma}}
                                                                                   |* {{T|mia}} : {{trad--|mia|mihšiimina}}
                                                                                   |* {{T|mn}} : {{trad+|mn|алим|tr=alim|tradi=ᠠᠯᠢᠮ᠎ᠠ}}
                                                                                   |* {{T|nl}} : {{trad+|nl|appel}} {{m}}
                                                                                   |* {{T|nog}} : {{trad--|nog|алма}}
                                                                                   |* {{T|no}} : {{trad+|no|eple}}
                                                                                   |* {{T|oc}} : {{trad+|oc|poma}}, {{trad+|oc|pom|m}}, {{trad+|oc|pòm|m}}
                                                                                   |* {{T|ug}} : {{trad+|ug|ئالما}}
                                                                                   |* {{T|pi}} : {{trad--|pi|sītāphala}}
                                                                                   |* {{T|pap}} : {{trad--|pap|apel}}, {{trad--|pap|aplo}}
                                                                                   |* {{T|paw}} : {{trad--|paw|ápuʾ}}
                                                                                   |* {{T|fa}} : {{trad+|fa|سیب|tr=sib}}
                                                                                   |* {{T|pl}} : {{trad+|pl|jabłko}} {{n}}
                                                                                   |* {{T|pt}} : {{trad+|pt|maçã}}
                                                                                   |* {{T|proto-celtique}} : {{trad--|proto-celtique|Reconstruction:proto-celtique/*abalom|dif=*abalom}}
                                                                                   |* {{T|rm}} : {{trad--|rm|mail}}
                                                                                   |* {{T|rom}} : {{trad--|rom|phabaj}}
                                                                                   |* {{T|ro}} : {{trad+|ro|măr}}
                                                                                   |* {{T|ru}} : {{trad+|ru|яблоко|R=iabloko}} {{n}}
                                                                                   |* {{T|se}} : {{trad--|se|eappel}}
                                                                                   |* {{T|sr}} : {{trad+|sr|јабука|f|tr=iabuka}}
                                                                                   |* {{T|zdj}} : {{trad--|zdj|pomu}}
                                                                                   |* {{T|scn}} : {{trad+|scn|pumu|m}}
                                                                                   |* {{T|sk}} : {{trad+|sk|jablko|n}}
                                                                                   |* {{T|sl}} : {{trad+|sl|jabolko|n}}
                                                                                   |* {{T|sv}} : {{trad+|sv|äpple}}
                                                                                   |* {{T|sw}} : {{trad-|sw|tofaa}}, {{trad-|sw|tunda la kizungu}}
                                                                                   |* {{T|tg}} : {{trad-|tg|cеб|R=seb}}
                                                                                   |* {{T|tl}} : {{trad+|tl|mansanas}}
                                                                                   |* {{T|crh}} : {{trad--|crh|alma}}
                                                                                   |* {{T|tt}} : {{trad+|tt|алма}}
                                                                                   |* {{T|cs}} : {{trad+|cs|jablko}} {{n}}
                                                                                   |* {{T|ckt}} : {{trad--|ckt|яплок}}
                                                                                   |* {{T|cv}} : {{trad--|cv|улма}}
                                                                                   |* {{T|th}} : {{trad-|th|แอปเปิล|tr=àep bpern}}
                                                                                   |* {{T|bo}} : {{trad--|bo|ཀུ་ཤུ།|tr=ku shu}}
                                                                                   |* {{T|tr}} : {{trad+|tr|elma}}
                                                                                   |* {{T|tk}} : {{trad+|tk|alma}}
                                                                                   |* {{T|uk}} : {{trad+|uk|яблуко|tr=iabluko}}
                                                                                   |* {{T|uum}} : {{trad--|uum|алма}}
                                                                                   |* {{T|vi}} : {{trad+|vi|táo}}
                                                                                   |* {{T|zu}} : {{trad-|zu|ihabhula}}, {{trad-|zu|ilihabhula}}
                                                                                   |{{trad-fin}}
                                                                                   |
                                                                                   |{{trad-début|Héraldique}}
                                                                                   |* {{T|en}} : {{trad+|en|pomme}}
                                                                                   |* {{T|es}} : {{trad-|es|roele de sinople}}
                                                                                   |{{trad-fin}}
                                                                                   |
                                                                                   |==== {{S|hyponymes}} ====
                                                                                   |* [[golden]]
                                                                                   |* [[jazz]]
                                                                                   |* [[gala]]
                                                                                   |** [[royal gala]]
                                                                                   |
                                                                                   |=== {{S|adjectif|fr}} ===
                                                                                   |{{fr-rég|pɔm}}
                                                                                   |'''pomme''' {{pron|pɔm|fr}}
                                                                                   |# {{familier|fr}} Qui manque de discernement ; stupide, sot.
                                                                                   |#* ''Quand je pense, Debedeux, que je renaudais pour venir là, en paradis, fallait que je sois '''pomme''' et racorni !'' {{source|{{w|René Fallet}}, ''Le Beaujolais nouveau est arrivé'', chapitre IX ; Éditions Denoël, Paris, 1975}}
                                                                                   |
                                                                                   |=== {{S|verbe|fr|flexion}} ===
                                                                                   |{{fr-verbe-flexion|pommer|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
                                                                                   |'''pomme''' {{pron|pɔm|fr}}
                                                                                   |# ''Première personne du singulier de l’indicatif présent de'' [[pommer]].
                                                                                   |# ''Troisième personne du singulier de l’indicatif présent de'' [[pommer]].
                                                                                   |# ''Première personne du singulier du subjonctif présent de'' [[pommer]].
                                                                                   |# ''Troisième personne du singulier du subjonctif présent de'' [[pommer]].
                                                                                   |# ''Deuxième personne du singulier de l’impératif présent de'' [[pommer]].
                                                                                   |
                                                                                   |==== {{S|paronymes}} ====
                                                                                   |* [[paume]]
                                                                                   |
                                                                                   |=== {{S|prononciation}} ===
                                                                                   |* {{pron|pɔm|fr}}
                                                                                   |** {{écouter|lang=fr|France
                                                                                   |<!-- précisez svp la ville ou la région -->
                                                                                   ||yn̪ pɔm|titre=une pomme|audio=Fr-pomme.ogg}}
                                                                                   |** {{écouter|lang=fr|France|pɔm|audio=LL-Q150 (fra)-Fhala.K-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pɔm|audio=LL-Q150 (fra)-GrandCelinien-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|Suisse (canton du Valais)|pɔm|audio=LL-Q150 (fra)-DSwissK-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pɔm|audio=LL-Q150 (fra)-Aemines1-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pɔm|audio=LL-Q150 (fra)-Aemines2-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pɔm|audio=LL-Q150 (fra)-Aemines3-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pɔm|audio=LL-Q150 (fra)-Aemines5-pomme.wav}}
                                                                                   |** {{écouter|lang=fr||pɔm|audio=LL-Q150 (fra)-Aemines4-pomme.wav}}
                                                                                   |** {{écouter|lang=fr|France (Paris)|pʌm|audio=LL-Q150 (fra)-Aemines6-pomme.wav}}
                                                                                   |* Français méridional : {{pron|ˈpɔ.mə|fr}}
                                                                                   |* Français du Missouri : {{pron|põm|fr}}
                                                                                   |
                                                                                   |=== {{S|voir aussi}} ===
                                                                                   |* {{WP}}
                                                                                   |* {{Thésaurus|fr|héraldique}}
                                                                                   |* {{WP|Liste des meubles héraldiques}}
                                                                                   |
                                                                                   |=== {{S|références}} ===
                                                                                   |{{Références}}
                                                                                   |* {{R:DAF9}}
                                                                                   |
                                                                                   |[[Catégorie:Pommes en français|*]]
                                                                                   |[[Catégorie:Couleurs en français]]
                                                                                   |[[Catégorie:Mots féminins en français pouvant désigner des hommes]]""".stripMargin)
}
