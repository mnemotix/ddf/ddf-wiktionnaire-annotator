/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Latin {
  def latin = """{{voir/latin}}
                |== {{langue|fr}} ==
                |=== {{S|étymologie}} ===
                |: {{lien-ancre-étym|fr|adj}} Du {{étyl|la|fr|mot=Latinus|sens=du [[Latium]], latin}}.
                |: {{lien-ancre-étym|fr|nom}} Du {{étyl|la|fr|mot=Latinum|dif=Latīnum|sens=langue latine}}.
                |
                |=== {{S|adjectif|fr}} ===
                |{{fr-accord-in|la.t}}
                |'''latin''' {{pron|la.tɛ̃|fr}}
                |# {{lexique|histoire|fr}} Qui est [[originaire]] ou se [[rapporte]] aux [[Latins]], les habitants du [[Latium]].
                |#* ''La mythologie '''latine'''.''
                |# {{par extension}} [[romain|Romain]], relatif à l’empire romain.
                |#* ''Le droit '''latin'''.''
                |#* ''Empire '''latin''' d’Orient.''
                |# {{info lex|linguistique}} Relatif au [[latin#fr#Nom Commun|latin]], la langue.
                |#* ''La littérature française du moyen-âge n'a guère que des antécédens '''latins'''. Les poésies celtique et germanique n’y ont laissé que de rares et douteux vestiges ; la culture antérieure est purement '''latine'''. C’est du sein de cette culture '''latine''' que le moyen-âge français est sorti, comme la langue française elle-même a émané de la langue '''latine'''.'' {{source|{{w|Jean-Jacques Ampère}}, ''{{ws|La Littérature française au moyen-âge}}'', Revue des Deux Mondes, 1839, tome 19}}
                |#* ''L’homme ne doit savoir littérairement que deux langues, le '''latin''' et la sienne ; mais il doit comprendre toutes celles dont il a besoin pour ses affaires ou son instruction.'' {{source|{{w|Ernest Renan}}, ''Souvenirs d’enfance et de jeunesse'', 1883, collection Folio, pages 166-167}}
                |#* ''« Votre castillan et notre français, et l’italien encore ne sont que des dialectes issus du '''latin''' parlé », poursuivit Joanny, récitant malgré lui sa grammaire ; « ce sont des langues vulgaires, d'anciens patois de paysans. Un temps viendra, vous dis-je, où de nouveau on enseignera le '''latin''' dans toutes les écoles de l'Empire, le '''latin''' classique, et où tous les vulgaires seront oubliés.'' {{source|{{w|Valery Larbaud}}, ''Fermina Márquez'', 1911, réédition Le Livre de Poche, pages 143-144}}
                |#* ''Toute la nuit, il avait rêvé qu’il récitait un discours '''latin''' en présence de l’Archevêque, et il lui avait semblé prononcer, ''ore rotundo'', un nombre infini de belles terminaisons et de nobles désinences : ''abunt'', ''arentur'', ''ibus'', ''arum''…'' {{source|{{w|Valery Larbaud}}, ''Fermina Márquez'', 1911, réédition Le Livre de Poche, page 210}}
                |#* ''Les plus énormes bêtises qui sortaient de la bouche de Johnson prenaient en effet je ne sais quel accent de véracité qui tenait sans doute au style de l’orateur, à la parfaite symétrie de ses phrases et à son redoutable arsenal de mots savants à désinences '''latines'''.'' {{source|{{w|Julien Green}}, '' Samuel Johnson'', dans ''Suite anglaise'', 1972, Le Livre de Poche, page 12}}
                |#* ''Le jeune jésuite chargé de la classe de '''latin''' lit à haute voix les thèmes des élèves sur un ton de persiflage, pour faire rire. Michel en particulier, nouveau venu expulsé depuis peu par une institution laïque, sert de cible.<br
                |/>– Voici, Messieurs, du '''latin''' de lycée.<br
                |/>– Cela vous changera de votre '''latin''' de sacristie.'' {{source|{{w|Marguerite Yourcenar}}, ''{{w|Archives du Nord}}'', Gallimard, 1977, page 225}}
                |# Qui s’exprime dans cette langue.
                |#* ''Les auteurs '''latins'''.''
                |#* ''– Je prenais sur mes nuits pour parfaire ma connaissance des maîtres '''latins'''. Car je les préfère aux Grecs. Ceux-ci sont plus élégants, peut-être. Mais les autres sont plus vigoureux, et ont davantage le sens du droit.'' {{source|{{w|Pierre Benoit}}, ''{{w|Le lac salé}}'', 1921, réédition Le Livre de Poche, page 203}}
                |# {{lexique|religion|fr}} Relatif à l’[[Église catholique romaine]].
                |#* ''L’église '''latine''' ; de rite '''latin'''.''
                |# Qui a certaines caractéristiques des habitants du Latium, des anciens Romains et, par extension, des peuples qu’ils ont longtemps et durablement colonisés et influencés.
                |#* ''Il releva ses cheveux. Sa beauté était célèbre à Tolède : nez aquilin, yeux très grands, le masque conventionnel de beauté '''latine'''.'' {{source|{{w|André Malraux}}, ''L’Espoir'', 1937, page 622}}
                |# Qui a pour origine la langue latine, qui à l’origine parlait le latin.
                |#* ''Les peuples '''latins'''.''
                |# {{en particulier}} [[espagnol|Espagnol]], [[portugais]] et [[italien]] par opposition à [[anglo-saxon]].
                |#* ''[[Amérique latine|Amérique '''latine''']].''
                |
                |==== {{S|dérivés}} ====
                |{{(|colonnes=4}}
                |* [[antilatin]], [[anti-latin]]
                |* [[bas latin]], [[bas-latin]]
                |* [[carré latin]]
                |* [[celtolatin]], [[celto-latin]]
                |* [[croix latine]]
                |* [[en perdre son latin]]
                |* [[grécolatin]], [[gréco-latin]]
                |* [[hellénolatin]], [[helléno-latin]]
                |* [[hellénicolatin]], [[hellénico-latin]]
                |* [[latinage]]
                |* [[latinasserie]]
                |* [[latinément]], [[latinement]]
                |* [[latin impérial]]
                |* [[latinisant]]
                |* [[latinisation]]
                |* [[latiniser]]
                |* [[latinisme]]
                |* [[latiniste]]
                |* [[latinité]]
                |* [[latino]]
                |* [[Latino-Américain]], [[latino-américain]]
                |* [[latinomanie]]
                |* [[latinophile]]
                |* [[latinophobe]]
                |* [[latinophone]]
                |* [[néolatin]], [[néo-latin]]
                |* [[nom latin]]
                |* [[perdre son latin]]
                |* [[protolatin]], [[proto-latin]]
                |* [[Quartier latin]]
                |* [[relatiniser]]
                |* [[vieilles latines]]
                |* [[voile latine]]
                |{{)}}
                |
                |==== {{S|traductions}} ====
                |{{trad-début|Relatif à la langue parlée par les Romains}}
                |* {{T|af}} : {{trad+|af|Latinis}}
                |* {{T|de}} : {{trad+|de|lateinisch}}
                |* {{T|en}} : {{trad+|en|Latin}}
                |* {{T|bm}} : {{trad-|bm|latin}}
                |* {{T|eu}} : {{trad-|eu|latindun}}
                |* {{T|bs}} : {{trad-|bs|latinski}}
                |* {{T|br}} : {{trad+|br|latin}}
                |* {{T|ca}} : {{trad+|ca|llatí}}
                |* {{T|da}} : {{trad+|da|latinsk}}
                |* {{T|es}} : {{trad+|es|latino}}
                |* {{T|eo}} : {{trad+|eo|latina}}
                |* {{T|kk}} : {{trad-|kk|латын|R=latın}}
                |* {{T|avk}} : {{trad|avk|latinavaf}}
                |* {{T|nl}} : {{trad+|nl|Latijns}}
                |* {{T|oc}} : {{trad+|oc|latin}}
                |* {{T|pap}} : {{trad|pap|latino}}
                |* {{T|pl}} : {{trad+|pl|łaciński}}
                |* {{T|pt}} : {{trad+|pt|latino}}
                |* {{T|ro}} : {{trad+|ro|latin}}
                |* {{T|ru}} : {{trad+|ru|латинский}}
                |* {{T|sv}} : {{trad+|sv|latinsk}}
                |* {{T|ta}} : {{trad-|ta|இலத்தீனிய|R=ilattīniya}}
                |* {{T|cs}} : {{trad+|cs|latinský}}
                |* {{T|non}} : {{trad|non|bók}}
                |{{trad-fin}}
                |
                |=== {{S|nom|fr}} ===
                |{{fr-inv|la.tɛ̃}}
                |'''latin''' {{pron|la.tɛ̃|fr}} {{m}}, {{au singulier uniquement|fr}}
                |# [[langue|Langue]] [[indo-européen]]ne dont sont [[originaire]]s les [[langue romane|langues romanes]].
                |#* {{exemple|lang=fr|Oh ! mon Dieu, le premier qui me fit commencer le '''latin''' (''rosa'', la rose ; ''cornu'', la corne ; ''tonitru'', le tonnerre), un grand vieux voûté, mal tenu, triste à regarder comme une pluie de novembre !|source={{w|Pierre Loti}}, ''Le Roman d'un enfant'', 1890}}
                |#* ''Le bruit courut que les trois frères avaient été empoisonnés, et le public n’eut pas besoin de savoir le '''latin''' pour cherchez à découvrir le coupable pas l’application du vieil axiome « is fecit cui prodest ».'' {{source|{{w|Frédéric Weisgerber}}, ''Au seuil du Maroc Moderne'', Institut des Hautes Études Marocaines, Rabat : Les éditions de la porte, 1947, page 121}}
                |#* ''La mauvaise foi des « curés » était d’ailleurs prouvée par l’usage du '''latin''', langue mystérieuse, et qui avait, pour les fidèles ignorants, la vertu perfide des formules magiques.'' {{source|{{w|Marcel Pagnol}}, ''{{w|La gloire de mon père}}'', 1957, collection Le Livre de Poche, pages 21-22}}
                |#* ''« Et puis, au lycée, dit l’oncle, tu apprendras le '''latin''', et je te promets que ça va te passionner ! Moi, du '''latin''', j’en faisais même pendant les vacances, pour le plaisir ! »'' {{source|{{w|Marcel Pagnol}}, ''{{w|Le château de ma mère}}'', 1958, collection Le Livre de Poche, page 94}}
                |#* ''On s’est plu à qualifier le '''latin''' de langue de communication universelle. Or, la multiplicité de ses prononciations et la vanité des efforts pour établir une prononciation unique invitent, pour l’époque moderne, à tenir des propos plus nuancés et, dans le temps, à mesurer, à partir d’un exemple précis – celui de la prononciation –, l’écart entre les discours élaborés sur le latin et la réalité des faits.'' {{source|{{w|Françoise Waquet}}, ''[//www.persee.fr/web/revues/home/prescript/article/mefr_1123-9891_1996_num_108_1_4433 Parler latin dans l’Europe moderne. L’épreuve de la prononciation]'', 1996, page 279}}
                |#* ''Dans cette étonnante maison j’entends affirmer que sans la clé magique du '''latin''' il est inutile d’espérer ouvrir les portes du monde et qu’on demeure à jamais aveugle, sourd, invalide (le médecin, du reste, entreprend, dans cette année d’avant la sixième, d’enseigner les déclinaisons à mon amie et à moi et de nous initier à l’épitomé).'' {{source|{{w|Mona Ozouf}}, {{w|''Composition française''}}, Gallimard, 2009, collection Folio, pages 131-132}}
                |#* ''[…] le '''latin''' est une langue de la famille [[indo-européen]]ne appartenant au groupe des langues [[italique]]s, qui comporte également quelques langues moins répandues, comme par exemple l’osque ou l’ombrien, qui ont laissé peu de traces, alors que le '''latin''' a réussi à se faire une place de choix parmi les langues du monde au cours de sa longue histoire.'' {{source|{{w|Henriette Walter}}, ''Minus, lapsus et mordicus : nous parlons tous latin sans le savoir'', Robert Laffont, Paris, 2014, page 129}}
                |# [[alphabet|Alphabet]] [[utilisé]] par ces langues.
                |
                |==== {{S|notes}} ====
                |
                |* {{note code langue}}
                |
                |==== {{S|abréviations}} ====
                |* [[la]] (ISO 639-1)
                |* [[lat]] (ISO 639-2 et -3)
                |
                |==== {{S|synonymes}} ====
                |* [[langue latine]]
                |* [[langue de Cicéron]] (périphrase)
                |
                |==== {{S|dérivés}} ====
                |{{(}}
                |* [[alphabet latin]]
                |* [[bas latin]]
                |* [[en perdre son latin]]
                |* [[être au bout de son latin]] (ne plus savoir où on en est, ne plus savoir que dire, que faire)
                |* [[latin archaïque]]
                |* [[latin classique]]
                |* [[latin de cuisine]]
                |* [[latin contemporain]]
                |* [[latin ecclésiastique]]
                |* [[latin médiéval]]
                |* [[latin macaronique]]
                |* [[latin moderne]]
                |* [[latin populaire]]
                |* [[latin scientifique]]
                |* [[latin tardif]]
                |* [[latin vivant]]
                |* [[latin vulgaire]]
                |* [[latinage]]
                |* [[néolatin]]
                |{{)}}
                |
                |==== {{S|hyponymes}} ====
                |{{(}}
                |* [[latin archaïque]]
                |* [[latin classique]]
                |* [[latin d’Église]] ou [[latin ecclésiastique]]
                |* [[latin médiéval]]
                |* [[latin moderne]]
                |* [[latin postclassique]]
                |* [[latin scientifique]]
                |* [[latin tardif]]
                |* [[latin vulgaire]]
                |* [[néolatin]]
                |{{)}}
                |
                |==== {{S|traductions}} ====
                |{{trad-début|langue parlée par les Romains}}
                |* {{T|af}} : {{trad+|af|Latyn}}
                |* {{T|de}} : {{trad+|de|Latein}}
                |* {{T|en}} : {{trad+|en|Latin}}
                |* {{T|ang}} : {{trad-|ang|lǣden}}
                |* {{T|ar}} : {{trad+|ar|لاتينية|dif=لاتِينِيّة|R={{transliterator|ar|لاتيني}}}}
                |* {{T|an}} : {{trad+|an|latín}}
                |* {{T|az}} : {{trad-|az|latın dili}}
                |* {{T|bm}} : {{trad-|bm|latin}}
                |* {{T|nds-nl}} : {{trad|nds-nl|latien}}
                |* {{T|dsb}} : {{trad|dsb|łatyńšćina}}
                |* {{T|eu}} : {{trad+|eu|latin}}
                |* {{T|bar}} : {{trad|bar|latein}}
                |* {{T|br}} : {{trad+|br|latin}}
                |* {{T|ca}} : {{trad+|ca|llatí}}
                |* {{T|ceb}} : {{trad|ceb|linatin}}
                |* {{T|ko}} : {{trad+|ko|라틴어|tr=Ratineo|tradi=라틴語}}
                |* {{T|kw}} : {{trad-|kw|latin}}
                |* {{T|gcr}} : {{trad|gcr|laten}}
                |* {{T|ht}} : {{trad|ht|laten}}
                |* {{T|da}} : {{trad+|da|latin}}
                |* {{T|diq}} : {{trad-|diq|latinki}}
                |* {{T|eml}} : {{trad|eml|latèin}}
                |* {{T|es}} : {{trad+|es|latín}}
                |* {{T|eo}} : {{trad+|eo|latino}}
                |* {{T|fi}} : {{trad+|fi|latina}}
                |* {{T|vls}} : {{trad|vls|latyn}}
                |* {{T|ksh}} : {{trad|ksh|latting}}
                |* {{T|fy}} : {{trad+|fy|Latyn}}
                |* {{T|stq}} : {{trad|stq|latiensq}}
                |* {{T|gd}} : {{trad-|gd|laideann}}
                |* {{T|cy}} : {{trad-|cy|lladin}}
                |* {{T|el}} : {{trad+|el|λατινικά|R=latiniká}}
                |* {{T|hsb}} : {{trad-|hsb|laćonšćina}}
                |* {{T|he}} : {{trad+|he|לטינית|R=latinit}}
                |* {{T|hi}} : {{trad+|hi|लातिनी|R=lātinī}}
                |* {{T|hif}} : {{trad-|hif|latin}}
                |* {{T|is}} : {{trad+|is|latína}}
                |* {{T|it}} : {{trad+|it|latino}}
                |* {{T|ja}} : {{trad+|ja|ラテン語|tr=Ratengo}}
                |* {{T|lad}} : {{trad|lad|latin}}
                |* {{T|kab}} : {{trad|kab|talatint}}
                |* {{T|kl}} : {{trad-|kl|latiinerisut}}
                |* {{T|kk}} : {{trad-|kk|латынша|R=latınşa}}
                |* {{T|kg}} : {{trad|kg|kilatini}}
                |* {{T|avk}} : {{trad|avk|Latinava}}
                |* {{T|ku}} : {{trad+|ku|latînî}}
                |* {{T|la}} : {{trad-|la|lingua latina}}
                |* {{T|li}} : {{trad-|li|latien}}
                |* {{T|ln}} : {{trad-|ln|latina}}
                |* {{T|lmo}} : {{trad+|lmo|latin}}
                |* {{T|lb}} : {{trad+|lb|Latäin}}
                |* {{T|gv}} : {{trad-|gv|ladjyn}}
                |* {{T|nl}} : {{trad+|nl|Latijn}}
                |* {{T|no}} : {{trad+|no|latin}}
                |* {{T|nn}} : {{trad+|no|latin}}
                |* {{T|ny}} : {{trad|ny|chilatini}}
                |* {{T|oc}} : {{trad+|oc|latin}}
                |* {{T|pfl}} : {{trad|pfl|latein}}
                |* {{T|pap}} : {{trad|pap|latin}}
                |* {{T|fa}} : {{trad+|fa|لاتین|R=lạty̰n}}
                |* {{T|pih}} : {{trad|pih|leten}}
                |* {{T|pl}} : {{trad+|pl|łacina}}
                |* {{T|pt}} : {{trad+|pt|latim|m}}
                |* {{T|rm}} : {{trad-|rm|latin}}
                |* {{T|ru}} : {{trad+|ru|латынь|R=latyn’}}
                |* {{T|se}} : {{trad|se|láhtengiella}}
                |* {{T|sco}} : {{trad|sco|laitin}}
                |* {{T|szl}} : {{trad|szl|łacina}}
                |* {{T|sl}} : {{trad+|sl|latinščina}}
                |* {{T|sv}} : {{trad+|sv|latin}}
                |* {{T|sw}} : {{trad-|sw|kilatini}}
                |* {{T|ta}} : {{trad+|ta|இலத்தீன்|R=ilattīn}}
                |* {{T|cs}} : {{trad+|cs|latina}}
                |* {{T|tr}} : {{trad+|tr|Latince}}
                |* {{T|wa}} : {{trad+|wa|latén}}
                |* {{T|war}} : {{trad|war|linatin}}
                |* {{T|yi}} : {{trad-|yi|לאטיין|tr=lateyn}}
                |* {{T|zea}} : {{trad|zea|latijn}}
                |{{trad-fin}}
                |
                |=== {{S|prononciation}} ===
                |* {{pron-rimes|la.tɛ̃|fr}}
                |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|la.tɛ̃|audio=Fr-latin.ogg}}
                |* {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Fhala.K-latin.wav}}
                |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-GrandCelinien-latin.wav}}
                |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-Poslovitch-latin.wav}}
                |
                |==== {{S|paronymes}} ====
                |* [[ladin]]
                |
                |=== {{S|anagrammes}} ===
                |{{voir anagrammes|fr}}
                |
                |=== {{S|voir aussi}} ===
                |* {{Catégorie langue|spéc1=du Vatican|spéc2=éteintes}}
                |* {{interwiktionnaire|la}}
                |* {{WP}}
                |* {{WB|Catégorie:Latin}}
                |
                |=== {{S|références}} ===
                |* {{R:DAF8}}
                |* {{R:TLFi}}
                |
                |== {{langue|fro}} ==
                |=== {{S|étymologie}} ===
                |: ''(Nom)'' Du {{étyl|la|fro|mot=Latinum|sens=langue latine}}.
                |: ''(Adjectif)'' Du {{étyl|la|fro|mot=Latinus}}, ''-a'', ''-um''.
                |
                |=== {{S|nom|fro}} ===
                |'''latin''' {{pron||fro}} {{m}}
                |# {{langues|fro}} [[latin#fr-nom|Latin]].
                |#* ''De greu le torna en '''latin''''' {{source|1=''Le Roman de Troie'', édition de Constans, tome I, [http://gallica.bnf.fr/ark:/12148/bpt6k5094q/f25.image page 8], <span title="circa">c.</span> 1165}}
                |# [[langue|Langue]], [[parole]].
                |#* ''E parolent plusurs '''latins''''' {{source|''{{w|Partonopeus de Blois}}'', manuscrit de la [https://digi.vatlib.it/view/MSS_Pal.lat.1971 Bibliothèque apostolique vaticane]. 1175-1200. Fol. 21v.}}
                |# [[ramage|Ramage]] des [[oiseau]]x.
                |# Ce qu’on a à dire, [[pensée]].
                |#* ''Avez vous dit votre '''latin''' ?''
                |
                |=== {{S|adjectif|fro}} ===
                |{{fro-adj}}
                |'''latin''' {{pron||fro}} {{m}}
                |# [[latin#fr|Latin]], [[romain]].
                |#* ''Comment li roys '''latins''' donna<br>sa fille a Eneas qui ne l’avoit onques veue.'' {{source|1=''Roman d’Eneas'', [[manuscrit|ms.]] [http://gallica.bnf.fr/ark:/12148/btv1b9059221b/f154.item.zoom 60 français] de la {{w|BnF}}, f. 162r. c., rubrique (en gris sur le site de la BnF)}}
                |
                |==== {{S|apparentés}} ====
                |* [[latiner#fro|latiner]]
                |* [[latinier#fro|latinier]]
                |* [[latineur#fro|latineur]]
                |
                |=== {{S|références}} ===
                |* {{R:Godefroy}}
                |
                |== {{langue|pro}} ==
                |{{ébauche|pro}}
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|pro|Latinum}}.
                |
                |=== {{S|adjectif|pro}} ===
                |'''latin''' {{m}}
                |# [[latin#fr|Latin]].
                |#* ''Legir audi sotz eiss un pin<br/>del vell temps un libre '''latin''''' {{source|{{w|Chanson de sainte Foy d'Agen|Cançon de Santa Fe}}, transcription adaptée de {{w|Robert Lafont (occitaniste)|Robert Lafont}}, ''Histoire et Anthologie de la littérature occitane'', Tome I « L’Âge classique - 1000-1520 », Les Presses du Languedoc, 1997, Montpellier, page 23}}
                |#*: J’entendis lire, sous un pin, un livre latin du vieux temps. {{source|[[s:Chanson de sainte Foy|Traduction par Antoine Thomas]]}}
                |
                |=== {{S|nom|pro}} ===
                |'''latin''' {{pron||pro}} {{m}}
                |# {{langues|pro}} [[latin#fr-nom|Latin]] (langue).
                |# Latin (peuple).
                |
                |=== {{S|références}} ===
                |* {{R:Raynouard}}
                |
                |== {{langue|eu}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|eu|mot=Latinus}}.
                |
                |=== {{S|nom|eu}} ===
                |'''latin''' {{pron|la.t̻i̻n̻|eu}}
                |# {{langues|eu}} [[latin#fr|Latin]], langue latine.
                |
                |==== {{S|dérivés}} ====
                |* {{lien|latindar|eu|sens=Latin}}
                |* {{lien|latindun|eu|sens=latin}}
                |
                |=== {{S|prononciation}} ===
                |* {{écouter|lang=eu|Espagne (Saint-Sébastien)|audio=LL-Q8752 (eus)-Xabier Cañas-latin.wav}}
                |
                |=== {{S|voir aussi}} ===
                |* {{WP|lang=eu}}
                |
                |=== {{S|références}} ===
                |* {{R:Elhuyar}}
                |
                |== {{langue|br}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|br|mot=Latinum|sens=langue latine}}.
                |
                |=== {{S|nom|br}} ===
                |'''latin''' {{pron|ˈlatːĩn|br}} {{m}}
                |# {{langues|br}} [[latin#fr-nom|Latin]] (langue).
                |#* ''Ha gwir eo ez an bemdez, goude ar skol, d’ar presbital da zeskiñ '''latin'''.'' {{source|{{Citation/Youenn Drezen/Skol-louarn Veig Trebern/1972-1974|2|104}}}}
                |#*: Et c’est vrai que je vais tous les jours au presbytère, après l’école, pour apprendre le latin.
                |
                |==== {{S|dérivés}} ====
                |{{(}}
                |* {{lien|latinaat|br}}
                |* {{lien|latinadur|br}}
                |* {{lien|latinat|br}}
                |* {{lien|latinek|br}}
                |* {{lien|latineg|br}}
                |* {{lien|latinelezh|br}} = {{lien|latiniezh|br}}
                |* {{lien|latiner|br}}
                |* {{lien|latinerezh|br}}
                |* {{lien|latinist|br}}
                |* {{lien|latinour|br}}
                |{{)}}
                |
                |== {{langue|mfe}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|fr|mfe|latin}}, du {{étyl|la|mfe|Latinum}}.
                |
                |=== {{S|nom|mfe}} ===
                |'''latin''' {{pron||mfe}}
                |# {{langues|mfe}} [[latin#fr|Latin]].
                |#* ''Si nu kapav donn enn lekzanp konkre ki pe al konpletman kont seki nn fek afirme : zis bizin observ ki franse inn transform u '''latin''' an ü franse zordi, alorski kreol morisyin inn fer ekzaktman linvers, inn transform bann ü an [u] u [i]. '' {{source|1=Emmanuel Richon, ''[https://kiltir.com/francais/b0027/download/langaz-kreol-langaz-marron.pdf Langaz kreol, langaz maron, Etimolozi, “lang-baz”, de konsep kolonyal]'', page 142}}
                |#*: {{trad-exe|mfe}}
                |
                |== {{langue|oc}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|oc|mot=Latinum|dif=Lătīnum|sens=langue latine}}.
                |
                |=== {{S|nom|oc}} ===
                |'''latin''' {{msing}} {{oc-norme classique}}
                |# {{langues|oc}} [[latin#fr-nom|Latin]] (langue).
                |
                |=== {{S|adjectif|oc}} ===
                |'''latin''' {{oc-norme classique}} {{m}}
                |# [[latin#fr-nom|Latin]].
                |
                |==== {{S|dérivés}} ====
                |* [[latinejar]]
                |
                |=== {{S|prononciation}} ===
                |* languedocien : {{phon|laˈti|oc}}
                |* provençal : {{phon|laˈtĩᵑ|oc}}
                |** Marseille, Istres, Aix-en-Provence : {{phon|laˈtẽᵑ|oc}}
                |* {{écouter|lang=oc|France (Béarn)|audio=LL-Q14185 (oci)-Davidgrosclaude-latin.wav}}
                |
                |=== {{S|références}} ===
                |* {{R:Cantalausa}}
                |
                |== {{langue|pap}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|la|pap|mot=Latinum|dif=Lătīnum|sens=langue latine}}.
                |
                |=== {{S|nom|pap}} ===
                |'''latin''' {{pron||pap}} {{msing}}
                |# {{langues|pap}} [[latin#fr-nom|Latin]] (langue).
                |
                |== {{langue|sv}} ==
                |=== {{S|étymologie}} ===
                |: {{ébauche-étym|sv}}
                |
                |=== {{S|nom|sv}} ===
                |{{sv-nom-n-ind}}
                |'''latin''' {{pron||sv}} {{n}}
                |# {{langues|sv}} [[latin#fr|Latin]] (langue).
                |""".stripMargin
}
