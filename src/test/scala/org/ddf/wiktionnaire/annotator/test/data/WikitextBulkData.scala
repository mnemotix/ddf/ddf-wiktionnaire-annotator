package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextBulkData {

  val affaireeAdj = """{{fr-accord-rég|ms=affairé|a.fe.ʁe|pron2=a.fɛ.ʁe}}
                      |'''affairé''' {{pron|a.fe.ʁe|fr}} ou {{pron|a.fɛ.ʁe|fr}}
                      |# Qui a bien des [[affaire]]s, qui est [[occupé]].
                      |#* ''Osman-Bey paraissait très-'''affairé'''. Des courriers lui arrivaient presque à chaque instant, porteurs de dépêches qu’il se faisait lire par un secrétaire et auxquelles il répondait immédiatement.'' {{source|{{nom w pc|Adolphe|Blanqui}}, ''Voyage en Bulgarie 1841'', chapitre X, 1845}}
                      |#* ''Il conservait son sans-gêne tranquille d’homme '''affairé''', chez lequel des accidents pareils ne produisent plus d’autre sensation qu’un désir de se débarrasser au plus vite d’un devoir qui manque de gaîté.'' {{source|{{nom w pc|Ivan|Tourgueniev}}, ''L’Exécution de Troppmann'', avril 1870, traduction française d’{{nom w pc|Isaac|Pavlovsky}}, publiée dans ses ''Souvenirs sur Tourguéneff'', Savine, 1887}}
                      |#* ''Près de Maidstone, ils tombèrent sur une rangée de onze canons automobiles de construction spéciale, autour desquels des artilleurs '''affairés''' surveillaient avec des jumelles une sorte de retranchement qu’on établissait sur la crête de la colline.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|54}}}}""".stripMargin

  val mazagran = """{{fr-rég|ma.za.ɡʁɑ̃}}
                   |[[Fichier:Mazagran 1.jpg|thumb|'''Mazagran''' en faïence. (1)]]
                   |'''mazagran''' {{pron|ma.za.ɡʁɑ̃|fr}} {{m}}
                   |# Profonde tasse [[conique]] sans anse mais dotée d’un [[pied]], souvent en [[terre cuite]], en [[grès]] ou en porcelaine.
                   |#* ''C’est un très beau service de '''mazagran''' en faïence.''
                   |# {{vieilli|fr}} [[breuvage|Breuvage]] servi dans un verre profond, à base de [[café]] noir, de sucre et d’[[eau-de-vie]].
                   |#*''Celles-là venaient dès le crépuscule, s’installaient dans une anfractuosité bien éclairée, réclamaient, plutôt par contenance que par besoin réel, un petit verre de vespetro ou un « '''mazagran''' », puis surveillaient le passant d’un œil méticuleux.'' {{source|{{w|Auguste de Villiers de L'Isle-Adam}}, ''[[s:Les Demoiselles de Bienfilâtre|Les Demoiselles de Bienfilâtre]]'', dans les ''{{w|Contes cruels}}'', 1883, éd. J. Corti, 1954, vol. 1, p. 4}}
                   |#* ''Gavard commanda deux autres '''mazagrans'''. Rose se hâta de servir les trois consommations, sous les yeux sévères de Logre.'' {{source|{{Citation/Émile Zola/Le Ventre de Paris/1878}}}}
                   |#* ''Vous allez prendre quelque chose avec nous, comme on dit, ce qu’on appelait autrefois un '''mazagran''' ou un gloria, boissons qu’on ne trouve plus, comme curiosités archéologiques, que dans les pièces de Labiche et les cafés de Doncières.'' {{source|{{w|Marcel Proust}}, ''{{ws|Sodome_et_Gomorrhe/Partie_2_-_chapitre_3|Sodome et Gomorrhe}}'', 1921}}
                   |""".stripMargin

  val griboullis = """[[Fichier:Child scribble age 1y10m.jpg|alt=Traits de plusieurs couleurs sans forme.|vignette|Un '''gribouillis''' coloré.]]
                     |'''gribouillis''' {{pron|ɡʁi.bu.ji|fr}} {{m}} {{sp}}
                     |# Dessin [[confus]], sans forme reconnaissable, qui résulte du [[gribouillage]].
                     |#* ''On ne distingue rien dans ce '''gribouillis'''.''
                     |#* ''Je t'envoie sous ce pli le papier sur lequel je prenais des notes pendant cette visite. Ce n'est qu'un '''gribouillis''' illisible. Mais garde-le toute ta vie pour l'amour de moi.'' {{source|Victor Hugo, ''{{ws|Correspondance de Victor Hugo/1840|Correspondance de Victor Hugo}}'', 1947, page 582.}}""".stripMargin

  val francophonieMAJ = """{{fr-inv|fʁɑ̃.kɔ.fɔ.ni}}
                          |'''Francophonie''' {{pron|fʁɑ̃.kɔ.fɔ.ni|fr}} {{f}}, {{au singulier uniquement|fr}}
                          |# [[organisation|Organisation]] internationale qui rassemble les [[pays]] parlant ou promouvant la langue française.
                          |#* ''Macron s’engage par rapport à la '''Francophonie''' : il veut faire du français la troisième langue du monde. Il crée un centre de la francophonie dans un ancien château de l’Aisne.'' {{source|Dominique {{pc|Lebel}}, ''L’entre-deux-mondes'', Montréal, Boréal, 2019, p. 326}}
                          |# Ensemble des [[peuple]]s, pays et territoires parlant français.""".stripMargin

  val col = """{{fr-rég|kɔl}}
                 |[[Image:Henri III Versailles.jpg|thumb|right|150px|Portrait d’Henri III avec '''col haut'''.]]
                 |'''col''' {{pron|kɔl|fr}} {{m}}
                 |#[[partie|Partie]] du [[vêtement]] qui [[entourer|entoure]] le [[cou]].
                 |#* ''Avec le surplus de ses gains, il achetait des cigarettes, des '''cols''' et des cravates sensationnels […]'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|11}}}}
                 |#* ''Jusqu’aux cravates, au petit nœud suavement bloqué par une épingle dans l’échancrure du '''col''', jusqu’au feu d’un vrai diam’ et au cuir mat du bracelet-montre, on sentait ces messieurs soucieux de leur mise.'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
                 |#* ''On patinait autour de lui. Des enfants, le '''col''' entouré de cache-nez écarlates, blancs, verts, bleus, glissaient et tournoyaient.'' {{source|{{Citation/Francis Carco/Brumes/1935|95}}}}
                 |# [[pièce|Pièce]] de [[lingerie]], de [[dentelle]] ou d’[[étoffe]] qui [[garnit]] le [[haut]] d’un [[corsage]] de [[femme]].
                 |#* ''Un '''col''' de mousseline. - Un '''col''' de velours.''
                 |# {{vieilli|fr}} [[cou|Cou]]. — {{usage}} On le dit quelquefois encore devant une [[voyelle]].
                 |#* ''Celui-là, tué d’hier, les loups lui on déchiqueté la chair sur le '''col''' en si longues aiguillettes qu’on le dirait paré […] d’une touffe de rubans rouges.'' {{source|{{w|Aloysius Bertrand}}, ''{{ws|Gaspard de la nuit}}'', 1842}}
                 |#* ''Ils se tortillaient sur leur siège, haussaient le '''col''', se penchaient les uns sur les autres pour discuter de la qualité des prix exposés.'' {{source|{{nom w pc|Henri|Troyat}}, ''Le mort saisit le vif'', 1942, réédition Le Livre de Poche, page 177}}
                 |#* ''J'étais un grand bateau descendant la Garonne<br
                 |/>Farci de contrebande et bourré d'Espagnols<br
                 |/>Les gens qui regardaient saluaient la Madone<br
                 |/>Que j'avais attachée en poupe par le '''col'''.'' {{source|{{nom w pc|Léo|Ferré}}, extrait des paroles de la chanson ''Le bateau espagnol'', 1950}}
                 |#* ''Quand il se dresse face à quelque « vieille écorce », chêne, frêne, ou hêtre, […] Arsène André éprouve une virile volupté. Sa chair se durcit, son '''col''' se gonfle, le sang lui afflue aux tempes à coups précipités'' {{source|{{Citation/Jean Rogissart/Passantes d’Octobre/1958|}}}}
                 |# {{lexique|anatomie|fr}} {{analogie|fr}} Désigne [[différentes]] [[extrémité]]s du [[corps]].
                 |#* ''C'est en 1875 que Copeman a proposé la dilatation du '''col''' par l’introduction du doigt contre l’hyperemesis gravidarum ; l'auteur publie deux faits favorables à la méthode.'' {{source|''Lyon médical'', 1880, vol. 33, page 289}}
                 |#* ''Le '''col''' de la vessie'' : ce qui est comme l’embouchure de la vessie.
                 |#* ''Le '''col''' d’un os'' : le rétrécissement qui se remarque au-dessous de la tête ou de quelque autre partie de certains os.
                 |#* ''Le '''col''' de l’utérus. - Le '''col''' du fémur. - Le '''col''' de l’humérus, du radius, etc.''
                 |# [[partie|Partie]] [[long]]ue et [[étroit]]e par [[laquelle]] on [[remplit]] et on [[vider|vide]] une [[bouteille]] ou un [[vase]].
                 |#* ''Le '''col''' d’une bouteille, d’un matras, etc.''
                 |## {{par ext|fr}} {{lexique|conditionnement|fr}} [[récipient|Récipient]] d’[[emballage]] qui [[possède]] un tel col.
                 |##* ''La Bouteillerie, qui représente en tonnage la part essentielle de la production du Verre Creux, a réalisé, à elle seule, une production de 3 200 000 tonnes […]. Les ventes de bouteilles en 1998 ont représenté près de 8 milliards de '''col'''s.'' {{source|Fédération des Chambres syndicales de l’industrie du verre, rapport d’activité 1998}}
                 |# {{lexique|géographie|fr}} Point de [[passage]] privilégié entre [[deux]] [[versant]]s, du fait d'une altitude plus basse que les environs.
                 |#* ''Aux Pyrénées centrales et occidentales, par exemple, ce sont les '''cols''' de la chaîne d’altitude inférieure à 2300 mètres qui paraissent avoir permis le passage au versant Nord de plantes semiméditerranéennes, venues du bassin de l’Èbre.'' {{source|{{w|Henri Gaussen}}, ''Géographie des Plantes'', Armand Colin, 1933, p. 83}}
                 |#* ''Le franchissement du '''col''' du Lautaret avait nécessité l'usage des chaînes à partir de Villar d'Arène et nous avions passé le sommet entre deux hauts murs de neige.'' {{source|Robert Falize, L'Ombre et la lumière, Éditions Publibook, 2006, page 354}}
                 |""".stripMargin

  val pomme = """{{fr-rég|pɔm}}
                |[[Fichier:Red Apple.jpg|vignette|Une '''pomme''' ''(sens 1)'']]
                |[[Fichier:Watering Can Spout.jpg|vignette|Une '''pomme''' d’arrosoir ''(sens 2)'']]
                |[[Fichier:POMME DE TERRE AU SAHARA.jpg|alt=Champ |vignette|Un champ de '''pommes''' (sens 5).]]
                |'''pomme''' {{pron|pɔm|fr}} {{f}}
                |# [[fruit|Fruit]] du [[pommier]], de forme ronde, à la [[chair]] [[ferme]] et de [[saveur]] [[diverse]]. — {{note}} C’est un [[faux-fruit]] au sens botanique.
                |#* '' La '''pomme''' est un fruit charnu à 5 loges cartilagineuses, issues des 5 carpelles du pistil floral et contenant les pépins, anciens ovules fécondés et futures graines. '' {{source|Delahaye Thierry, Vin Pascal, ''Le Pommier'', Actes Sud, Le Nom de l’arbre, 1997, page 33}}
                |#* '' Comment se priver d’un fruit riche en vitamines, en sucres, en sels minéraux et en acides organiques… ? […] « Une '''pomme''' chaque jour éloigne le médecin ». '' {{source|Delahaye Thierry, Vin Pascal, ''Le Pommier'', Actes Sud, Le Nom de l’arbre, 1997, page 63}}
                |#* ''Un jour ils le mandèrent au salon et lui dirent, avec toute la douceur et la bienveillance possible, que la veille, dînant au château voisin, ils avaient mangé des '''pommes''' et des poires si parfumées, si savoureuses, si exquises, que tous les convives en avaient exprimé leur admiration.'' {{source|{{w|Hans Christian Andersen}}, ''[[s:Le Jardinier et ses maîtres|Le Jardinier et ses maîtres]]}}
                |#* ''Je vais m’étendre dans la prairie, sous un pommier aux '''pommes''' vertes et dures. Je peux dormir au-dessous d’elles, je peux les contempler sans crainte, et même sans l’appréhension d’avoir à inventer, l’une tombant, les lois du monde.'' {{source|{{w|Jean Giraudoux}}, ''Retour d’Alsace - Août 1914'', 1916}}
                |#* ''[…] mais l’on n’était qu’en juin et, sauf pour les poires de moisson qui mûrissent en août, il fallait encore attendre longtemps avant de savourer concurremment les '''pommes''' du verger et la vengeance désirée.'' {{source|{{w|Louis Pergaud}}, ''[[s:Une revanche|Une revanche]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                |# Tout objet [[en forme de]] pomme.
                |## Partie d’un [[arrosoir]], souvent [[amovible]], par laquelle l’eau s’[[échappe]] en [[pluie]].
                |##* ''La '''pomme''' d’arrosoir.''
                |## Partie d’une [[douche]], percée et arrondie, fixée au bout d’un [[flexible]] de douche, pour se laver.
                |## {{lexique|architecture|fr}} Motif décoratif en forme de pomme.
                |## {{info lex|botanique}} Cœur d’un [[chou]], d’une [[laitue]].
                |# Fruit de plusieurs végétaux.
                |#* '''''Pomme''' de cajou, '''pomme''' épineuse.''
                |# Fruit interdit du [[paradis]] terrestre.
                |#* ''Eve croqua la '''pomme'''.''
                |#* ''Il faut avouer, que notre mère Ève était bien gourmande, d’avoir mangé de la '''pomme'''.'' {{source|{{w|Jeanne-Marie Leprince de Beaumont}}, ''[[s:La Curiosité|La Curiosité]]''}}
                |# {{info lex|cuisine}} {{ellipse|fr}} [[pomme de terre|Pomme de terre]].
                |#* '''''Pommes''' frites.''
                |# {{familier|fr}} Tête, visage d’une personne.
                |#* ''Il s’est bien payé ma '''pomme'''.''
                |# {{familier|fr}} Par extension, accompagné d’un adjectif possessif : forme pronominale désignant la personne elle-même. Synonymes : [[moi]] ([[mézigue]], [[bibi]]), [[toi]], [[lui]], etc.
                |#* ''Et tu sais qui a gagné au final ? Ma '''pomme''' !''
                |#* ''OK, je veux bien écraser le champignon, mais les [[amende]]s ce sera pour ta '''pomme'''.''
                |# {{familier|fr}} Personne [[naïf|naïve]], dont on se moque facilement.
                |#* ''C’est la reine des '''pommes''' !''
                |#* ''C'est le carreau-loupe qui vient de disparaître, hé, '''pomme''' !'' {{source|{{w|Hervé Bazin}}, {{w|''Cri de la chouette''}}, Grasset, 1972, réédition Le Livre de Poche, page 222}}
                |# {{figuré|fr}} Prix de beauté, par allusion au mythologique jugement de [[Pâris]].
                |#* ''Elle mérite la '''pomme'''.''
                |# {{Canada|fr}} {{term|Généralement péjoratif de la part des Amérindiens}} Personne [[amérindien]]ne [[acculturer|acculturée]] par les [[Blancs]].
                |#* ''Car bien que transplantée dans le terreau d'une famille ukrainienne aimante, Sandy n'en a pas moins, de sa jeunesse jusqu'à sa vie adulte, vécu cet héritage comme une malédiction la confinant à la haine de soi. Devenue une « '''pomme''' » au fil des ans, terme utilisé pour qualifier ces Amérindiens rouges à l'extérieur et blancs à l'intérieur, javellisés par la pression d'avoir à correspondre à la culture majoritaire, la journaliste, épaulée par son ami Kyle et le guide spirituel Joe Bush Sr., troquera sa pelure pour une peau d'ours, éminent symbole de sa reconversion.'' {{source|''[[w:Nuit blanche (revue)|Nuit blanche]]'', n° 152, automne 2018, page 48}}
                |# [[Fichier:Vemmenhög vapen.svg|vignette|120px|Armoiries avec 3 '''pommes''' ''(sens héraldique)'']] {{meubles héraldiques|fr}} {{rare|fr}} [[meuble|Meuble]] représentant un disque de couleur verte dans les [[armoiries]]. Il s’agit d’un ancien nom utilisé pour le tourteau [[de sinople]]. À rapprocher de [[besant]], [[besant-tourteau]], [[buse]], [[gulpe]], [[guse]], [[heurte]], [[œil de faucon]], [[ogoesse]], [[plate]], [[somme]], [[tourteau]], [[tourteau-besant]] et [[volet]].
                |#* ''D'argent à trois tourteaux de sinople ('''pommes''') mal-ordonnés, qui est du Hundred Vemmenhög en Suède'' {{cf|lang=fr}} illustration « armoiries avec 3 pommes »""".stripMargin

  val feu = """{{fr-rég-x|fø}}
              |[[Image:New Orleans Fire 2005-09-02.jpg|vignette|upright|La maison est en '''feu'''. (3)]]
              |[[File:Nailsea and Backwell railway station MMB 99.jpg|vignette|'''Feu''' de signalisation (13)]]
              |[[Image:11-08-05-volkswagen-by-RalfR-37.jpg|vignette|'''Feux''' arrière de voiture. (15)]]
              |'''feu''' {{pron|fø|fr}} {{m}}
              |# [[phénomène|Phénomène]] [[calorifique]] et [[lumineux]] [[produire|produit]] par la [[combustion]] d'une matière [[inflammable]] ; un des [[quatre]] [[éléments]] des [[Anciens]]. {{note}} Utilisé avec l'article défini : « le '''feu''' ».
              |#* ''La maîtrise du '''feu''' est souvent considérée, avec celle de l’outil, comme une des caractéristiques qui distinguent l’homme de l’animal.'' {{source|{{nom w pc|Jacques|Collina-Girard}}, ''Le feu avant les allumettes'', Paris, éditions de la Maison des sciences de l’homme, 1998, page 1}}
              |# [[brasier|Brasier]] de petite [[envergure]] [[allumer|allumé]] [[à dessein]] pour éclairer, chauffer ou cuire.
              |#* ''Lorsqu’une bande de baleines est signalée, la nouvelle se répand aussitôt dans tout l’archipel au moyen de '''feux''' allumés sur les montagnes ; […].'' {{source|Jules {{pc|Leclercq}}, ''La Terre de glace, Féroë, Islande, les geysers, le mont Hékla'', Paris : E. Plon & Cie, 1883, page 37}}
              |#*''Ils arrivèrent à la grotte, tout le monde se les gelait sérieusement. Ils décidèrent de faire un '''feu''' pour la nuit.'' {{source|Eva Kavian, ''L'Homme que les chiens aimaient'', ONLIT Éditions, 2016, page 98}}
              |#* ''Ils allumèrent des '''feux''' qu’ils entretinrent nuit et jour, et les hommes qu’on envoyait à la corvée du bois aux environs devaient tenir les loups en respect.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|271}}}}
              |#* ''Je m’habillai hâtivement et redescendis jusqu’à la salle à manger, la température était glaciale, le '''feu''' n’avait pas pris, je n’avais jamais été doué pour le '''feu''', je ne comprenais rien à l’assemblage de bûches et de brindilles qu’il convenait de bâtir, c’était un des nombreux points qui me séparaient de l’homme de référence – mettons pour situer Harrison Ford – que j’aurais souhaité d’être, enfin pour l’instant la question n’était pas là.'' {{source|{{w|Michel Houellebecq}}, ''Sérotonine'', Flammarion, 2019, page 280.}}
              |# {{part}} [[incendie|Incendie]].
              |#* ''En saison sèche, les Garde-bœuf suivent les '''feux''' de forêt et de savane, pour manger les animaux mis à nu par la disparition de la couverture herbacée.'' {{source|Jean Claude {{pc|Ruwet}}, ''Les Oiseaux des plaines et du lac-barrage de la Lufira supérieure (Katanga méridional) : reconnaissance écologique et éthologique'', Université de Liège & Fondation pour les recherches scientifiques en Afrique centrale, 1965, page 41}}
              |# {{familier|fr}} {{méton|fr}} Dispositif [[autonome]] destiné à produire une petite [[flamme]] ([[briquet]], [[allumette]], [[allume-cigare]]...).
              |#* ''Prête-moi ton '''feu'''.''
              |#* ''Avez-vous du '''feu''' ?''
              |#* ''Y a-t-il du '''feu''' dans la voiture ?''
              |# {{vieux|fr}} [[bougie|Bougie]] allumée pour [[indiquer]] le [[temps]] durant une mise aux [[enchère]]s [[publique]]s.
              |#* ''Deux antres '''feux''' successivement allumés sur cette dernière somme s’étant éteints sans enchère, la maison dont il s’agit, a été définitivement adjugée à bail à M. Nicolas Lhérault, sus-nommé, […].'' {{source|F. M. {{pc|Sellier}}, ''Le manuel des notaires'', Paris : Librairie Cotillon, 1841, vol. 1, page 135}}
              |# {{méton|fr}} {{vieilli|fr}} [[torche|Torche]].
              |#* ''Il nous guidait grâce au '''feu''' qu’il portait dans la main.''
              |# [[source|Source]] de chaleur pour la [[transformation]] des [[aliments]].
              |#* ''Laissez cuire vingt minutes à '''feu''' doux''.
              |# Appareil de [[cuisson]] des aliments, [[cuisinière]].
              |#* ''Des racines coupées et des feuilles de betteraves, bouillant dans une marmite sur le '''feu''', mêlaient leur parfum âcre à l’odeur de renfermé qui semblait stagner dans les encoignures.'' {{source|{{nom w pc|Louis|Pergaud}}, ''[[s:La Vengeance du père Jourgeot|La Vengeance du père Jourgeot]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
              |#* ''Si tu veux, j’ai encore de la soupe sur le '''feu'''.
              |# {{méton|fr}} [[endroit|Endroit]] où l’on [[faire|fait]] du feu.
              |#* ''— Cependant la nuit que tu viens de passer a dû te donner à réfléchir ?<br
              |/>— Bien certainement, j’aimerais mieux un bon lit et le coin du '''feu'''.<br
              |/>— Le veux-tu, le coin du '''feu''' et le bon lit, avec le travail bien entendu ?'' {{source|Hector Malot, ''{{ws|Sans famille/Dentu, 1887/Texte entier|Sans famille}}'', 1878}}
              |#* ''[…] et l’on court se mettre en pantoufles et en robe de chambre pour popoter avec lui au coin du '''feu'''.'' {{source|Octave {{pc|Uzanne}}, ''Les Zigzags d’un curieux'', Maison Quantin, 1888, page 218}}
              |# {{méton|fr}} {{vieilli|fr}} [[foyer|Foyer]] ; [[famille]].
              |#* ''Viennent ensuite les pays dont la population a été déterminée par plusieurs méthodes indirectes, telles que l’énumération de toutes les personnes sujettes à un impôt quelconque ; celle des familles ou '''feux''' ; celle des maisons, qu’il ne faut pas confondre, comme on le fait souvent, avec la précédente ''[…] {{source|{{nom w pc|Adriano (Adrien)|Balbi|Adriano Balbi}}, ''{{ws|La Population des deux mondes}}'', Revue des Deux Mondes, tome 1, 1829}}
              |#* ''La paroisse compte une trentaine de '''feux''', assez peu de fidèles.'' {{source|{{Citation/Jean Rogissart/Passantes d’Octobre/1958|}}}}
              |#* ''Leur population totale, qui en 1504 ne dépassait pas 69 '''feux''' ou ménages, comme le précise un compte de cette époque, atteignait 175 '''feux''' en 1789 (100 pour Vaux, 40 pour Euilly et 35 pour Tétaigne).'' {{source|Georges {{pc|Hubrecht}}, ''Le Bailliage de Mouzon à la veille de la Révolution et ses Cahiers de doléances'', Bière, 1969, page 65}}
              |#* ''Le rôle des tailles de Barques pour 1719 porte 41 '''feux''', dont 33 masculins ; celui de Marques : 66 '''feux''', dont 56 masculins.'' {{source|Jacques Dupâquier, ''Statistiques démographiques du bassin parisien, 1636-1720'', éd. Gauthier-Villars, 1977, page 530}}
              |# {{méton|fr}} {{au pluriel}} [[lumière|Lumière]].
              |#* ''Des '''feux''' tantôt roses, tantôt d’un bleu acide qui tournait au vert pomme, scintillaient à l’extérieur des ''Folies. {{source|{{Citation/Francis Carco/L’Homme de minuit/1938}}}}
              |#* ''L’hypothèse a été avancée d’une extinction des '''feux''' de la piste d’atterrissage au moment de l’approche de l’avion ; mais cette version n’est pas validée, les feux ayant, semble-t-il, été éteints après le crash, dans un mouvement de panique.'' {{source|{{nom w pc|Paul|Quilès}}, {{nom w pc|Bernard|Cazeneuve}} et {{nom w pc|Pierre|Brana}}, ''Enquête sur la tragédie rwandaise (1990-1994)'', éd. Assemblée nationale (rapport), décembre 1998, pages 216-217}}
              |# {{métaphore|fr}} [[signal|Signal]] lumineux.
              |#* ''Un officier de police traverse la rue pour donner une contravention à une dame n'ayant pas immobilisé son véhicule au '''feu''' rouge.'' {{source|Veronica {{pc|Brathwaite}}, ''Arrête! Le feu est rouge!: Traverser en toute sécurité'', Xlibris Corporation, 2013}}
              |#* ''Les signaux A45 et A47 sont accompagnés de '''feux''' clignotants rouges ou blancs.'' {{source|Claude {{pc|De Bruyn}}, ''Feu vert pour le permis de conduire : Je réussis l'examen'', De Boeck, 2008, page 80}}
              |# {{méton|fr}} {{familier|fr}} (''souvent au'' {{p}}) [[dispositif|Dispositif]] [[électrique]] qui [[réguler|régule]] le [[flux]] des [[véhicule]]s et des [[piétons]] à l’[[aide]] de lumière de différentes [[couleur]]s.
              |#* ''Arrête-toi aux '''feux''' !''
              |#* ''[[brûler un feu|Brûler un '''feu''']].''
              |# {{méton|fr}} [[phare|Phare]] maritime.
              |#* ''Le vendredi 13 mars, j’aurais dû apercevoir dans la soirée le '''feu''' de Sombrero, sur lequel je voulais atterrir […]'' {{source|{{nom w pc|Alain|Gerbault}}, ''À la poursuite du soleil'' ; tome 1 : ''De New-York à Tahiti'', 1929}}
              |#* ''Le ciel, clair au départ de Saint-Louis, devient brumeux vers Dakar. Nous apercevons dans un trou le '''feu''' de la pointe du Cap-Vert, à 4 heures.'' {{source|{{nom w pc|Jean|Mermoz}}, ''Mes Vols'', Flammarion, 1937, page84}}
              |# {{méton|fr}} [[dispositif|Dispositif]] [[lumineux]] permettant de signaler la présence d'un [[véhicule]] ou d'un [[aéronef]], ses [[changement]]s de direction, ses [[arrêt]]s .
              |#* ''Le troisième '''feu''' stop est maintenant obligatoire.''
              |#* ''Les '''feux''' de croisement sont les seuls à servir en ville et sur route.''
              |#* ''Les '''feux''' de navigation équipent tous les avions, du petit appareil d'aéroclub au plus gros des avions de commerce.''
              |# {{métaphore|fr}} {{poétique|fr}} [[étoile|Étoile]], corps [[céleste]] [[brillant]].
              |#* ''Les '''feux''' de la nuit.''
              |# {{métaphore|fr}} [[éclat|Éclat]], [[scintillement]].
              |#* ''Dans l’azur à peine noirci du couchant, l’étoile du berger brillait d’un '''feu''' paisible, sans un scintillement ; l’air était calme  […]'' {{source|Louis {{pc|Pergaud}}, ''{{ws|Un satyre}}'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
              |#* ''Jusqu’aux cravates, au petit nœud suavement bloqué par une épingle dans l’échancrure du col, jusqu’au '''feu''' d’un vrai diam’ et au cuir mat du bracelet-montre, on sentait ces messieurs soucieux de leur mise.'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
              |# {{métaphore|fr}} [[saveur|Saveur]] [[brûlante]], en particulier d’un [[alcool]].
              |#* ''Le '''feu''' de l’absinthe.''
              |# {{littéraire|fr}} [[excitation|Excitation]] ; [[passion]].
              |#* ''[…] le prince Jean, monté sur un palefroi gris plein de '''feu''', caracola dans la lice à la tête de sa suite joviale […]'' {{source|{{Citation/Walter Scott/Ivanhoé/1820|VII}}}}
              |#* ''Cette Gourguin était, comme l'on dit chez nous, un vrai chat noir, qui n’avait que la peau et les os ; toutefois, un grand '''feu''' d'esprit, et les plus beaux yeux, avec des manières hautaines : dangereuse, artificieuse, accusée de beaucoup de noirceurs ; […].'' {{source|{{w|Élémir Bourges}}, « ''Prologue : Le mémoire d'Ivan Manès'' », avril 1871, dans ''Les Oiseaux s'envolent: et les Fleurs tombent'', en feuilleton dans ''La Revue hebdomadaire : romans, histoire, voyages'', Paris : Librairie Plon, novembre 1892 (A1 - T6), p. 56 & Éditions Ligaran, 2015}}
              |#* ''Deux ou trois images de femmes rencontrées lui apparaissaient à l’imagination et il les suivait, d’abord pour se délasser. Puis tout le '''feu''' de ses vingt ans s’animait, tous ses sens sentaient (sic) ce que contient une femme qui passe.'' {{source|{{nom w pc|Charles-Louis|Philippe}}, ''Bubu de Montparnasse'', 1901, réédition Garnier-Flammarion, page 60}}
              |#* ''L’âme de Wesley avait besoin d’un '''feu''' plus vif. Un jour (24 mai 1738), dans une sorte d’illumination, il entrevit la vraie foi qui est un lien vivant et non une opération de la raison.'' {{source|André {{smcp|Maurois}}, ''Histoire de l’Angleterre'', 1937}}
              |#* ''Les cerfs, bien qu’imbelles, se livrent des combats lorsque les '''feux''' de l'amour les y poussent.'' {{source|''Les études classique''s, vol. 65, Université de Namur, 1997, page 124}}
              |#* ''Il y a, ô brahmane, trois sortes de '''feu''' qu’il faut abandonner, qu’il faut éloigner, qu’il faut éviter. Quels sont ces trois '''feux''' ? Ce sont le '''feu''' de l’avidité, le '''feu''' de la haine et le '''feu''' de l’illusion.'' {{source|{{smcp|Bouddha}}}}
              |# Source d’excitation, de passion.
              |#* ''Les disciples ont reproduit l'expérience du Sinaï : la Parole est un '''feu'''!'' {{source|{{nom w pc|Bruno|Chenu}}, ''Disciples d'Emmaüs'', Bayard, Paris, 2003, page 81}}
              |# {{littéraire|fr}} Passion, [[amour]].
              |#* ''Brûlé de plus de '''feux''' que je n'en allumai (…)'' {{source|{{smcp|Racine}}, ''Andromaque'', 1667}}
              |#* ''De mes '''feux''' mal éteints je reconnus la trace, <br
              |/>Je sentis que ma haine allait finir son cours,<br
              |/>Ou plutôt je sentis que je l’aimais toujours. (ibid.)''
              |# {{argot|fr}} {{ellipse|fr}} [[arme à feu|Arme à feu]], [[pistolet]].
              |# {{lexique|militaire|fr}} [[tirs|Tirs]], lors d’un [[combat]].
              |#* ''Notre régiment était presque à couvert du '''feu''' des Russes par un pli de terrain. '' {{source|{{nom w pc|Prosper|Mérimée}}, ''{{w|L’Enlèvement de la redoute}}''}}
              |#* ''Un capitaine réglait le '''feu''' : « À vous, Judin! » Le petit lignard visait soigneusement, lâchait la détente.'' {{source|{{w|Paul Margueritte|Paul}} et {{nom w pc|Victor|Margueritte}}, ''Le Désastre'', Plon-Nourrit & C{{e|ie}}, 86{{e}} éd., page 168}}
              |#* ''M. de la Ferté s’était séparé de la première demi-section, qui avait obliqué vers la gauche, avec ordre pour Saumabère, dès qu’il entendrait les premiers coups de fusil, d’exécuter un '''feu''' à [[cartouche]]s comptées, afin de donner à l’adversaire qu’il était tourné, ce qui en forêt est le prélude immédiat de la débâcle.'' {{source|{{w|Pierre Benoit}}, ''Monsieur de la Ferté'', Albin Michel, 1934, Cercle du Bibliophile, page 244.}}
              |#* ''Le '''feu''' des fusils Mauser commença ; ce fut un vacarme épouvantable dans lequel on percevait le ronflement des obus à la lydite, le crépitement des fusils Metford, les éclats des mitrailleuses boers et les sifflements des Mauser.'' {{source|''Le Mémorial d’Aix'', 25 janvier 1900, page 1}}
              |#* ''En cueillant dans les blés le coquelicot rouge<br
              |/>La blanche marguerite et le bleuet si bleu<br
              |/>– Ces couleurs du drapeau ! – parmi le blé qui bouge,<br
              |/>Songez à nos soldats qui sont tombés au '''feu''' !'' {{source|{{nom w pc|Marie|Cassabois}}, ''Les Chants de l’écolier'', Voix des tombes ; Édouard Privat, Toulouse, 1934, page 94}}
              |#* ''Il a sorti son '''feu''' et il a fait feu.'' {{source|Jean-Marc {{pc|Agrati}}, ''Le chien a des choses à dire, On foutait que dalle''}}
              |# {{métaphore|fr}} {{argot|fr}} [[enthousiasme|Enthousiasme]], excitation.
              |#* ''L’autre jour les MCs ont mis le '''feu''' à la salle.''
              |#* ''Allumer le '''feu''' et faire danser les diables et les dieux.'' {{source|Pierre {{pc|Jaconelli}}, Allumer le feu}}
              |# {{lexique|athlétisme|fr}} {{ellipse|fr}} [[coup de feu|Coup de feu]], signal [[sonore]] qui autorise le [[départ]] d’une course. Départ d’une course.
              |#* ''Dans les starting-blocks les coureurs attendaient que le '''feu''' retentisse pour partir.''
              |#* ''Il est parti dès le '''feu''', sans attendre.''
              |# {{métaphore|fr}} [[urgence|Urgence]], [[presse]].
              |#* ''Y a pas l’'''feu'''.''
              |#* {{familier|fr}} ''Il a le '''feu''' aux fesses.''
              |# {{lexique|astrologie|fr}} Élément astrologique qui comprend les signes [[Bélier]], [[Lion]] et [[Sagittaire]].
              |# {{familier|fr}} Utilisé pour exprimer que quelque chose est [[génial]].
              |#* ''C’est le '''feu''' les licornes !''
              |# [[Fichier:Blason ville fr Robert-Espagne 55.svg|vignette|120px|Armoiries avec un '''feu''' ''(sens héraldique)'']] {{meubles héraldiques|fr}} [[meuble|Meuble]] représentant un ensemble de flammes dans les [[armoiries]]. Il est toujours représenté flammes vers le [[chef]]. Certains auteurs [[blasonner|blasonne]] le nombre de flammes (trois d’ordinaire). Il peut être meuble seul ou utilisé en complément d’un autre (marmite par exemple). On le trouve également dans des meubles composites mais il porte alors un nom différent : [[immortalité]] pour le phénix, [[patience]] pour la [[salamandre]]. À rapprocher de [[flamme]].
              |#* ''Écartelé : au 1 d’azur à la tête de loup d’or, au 2 d’or au '''feu''' de gueules, au 3 d’or à l’enclume de gueules et au 4 d’azur à la tête de lion d’or, qui est de la commune de Robert-Espagne de la Meuse'' {{cf|lang=fr}} illustration « armoiries avec un feu »
              |# {{term|Régional}} [[brûlure|Brûlure]] de la [[peau]].
              |#* ''— Va-t’en à l'Hardilas, dit-elle, et ramène Julie la Rouge qui traite pour le '''feu'''. Il faut tout essayer.'' {{source|{{w|Ernest Pérochon}}, ''{{ws|Nêne/Texte entier|Nêne}}'', 1920}}""".stripMargin

  def avoir = """'''avoir''' {{pron|a.vwaʁ|fr}} {{tr-dir|fr}} {{conjugaison|fr|groupe=3}}
                |# Être en [[relation]] [[possessif|possessive]], soit [[concret|concrète]] ou [[abstrait]]e, soit [[permanent]]e ou [[occasionnel]]le, dont le [[possesseur]] est le sujet et le [[possédé]] est le [[complément d’objet direct]].
                |#* ''Et les cédilles, ça ressemblait à rien ces zigouigouis, quant au verbe '''avoir''' qui s’accordait tantôt avec le complément d’objet direct tantôt pas, c’était un cauchemar.'' {{source|Akli Tadjer, ''Alphonse'', Jean-Claude Lattès, 2005}}
                |#* ''Pierre '''a''' deux filles.'' ''Cadet Rouselle '''a''' trois maisons.''
                |#* ''La Mongolie n’'''a''' que deux voisins : la Russie et la Chine.''
                |#* ''L’antilope '''a''' plusieurs prédateurs.'' ''J’'''ai''' un beau chapeau.''
                |#* ''J’'''ai''' un très bon emploi.''
                |#* ''Nous '''avons''' un logement très agréable.''
                |#* ''Hélas ! Il mourra donc ! Il n''''a''' pour sa défense<br
                |/>Que les pleurs de sa mère et que son innocence.'' {{source|Racine, ''Andromaque'', 1667}}
                |#* ''Je n’'''ai''' plus beaucoup d’argent.''
                |## {{term|le sujet est un être animé, l’objet est inanimé}}
                |##* ''Il '''avait''' ce jour-là un costume bleu.'' ([[porter]])
                |##* ''Vous '''avez''' de la boue sur vos habits.''
                |##* ''Ce bébé '''a''' de très beaux yeux.''
                |##* ''J’'''ai''' un bras cassé.''
                |## {{term|le sujet est un être animé, l’objet est une [[qualité]], caractéristique physique ou matérielle}}
                |##* '''''Avoir''' affaire à quelqu’un ou avec quelqu’un.''
                |##* '''''Avoir''' l’âge de raison, '''avoir''' vingt ans.''
                |##* '''''Avoir''' de l’agilité.''
                |##* '''''Avoir''' quelque chose en aversion, sur le cœur.''
                |##* '''''Avoir''' peur, '''avoir''' la crainte de quelque chose.''
                |##* '''''Avoir''' des douleurs, '''avoir''' de la fièvre, la grippe, une angine, le sida, la tuberculose…''
                |##* '''''Avoir''' de la force,'' être fort.
                |##* '''''Avoir''' soin de faire quelque chose.''
                |##* '''''Avoir''' froid, chaud, '''avoir''' faim, soif…''
                |##* ''J’'''ai''' mal à la tête.''
                |##* ''Cet oiseau '''a''' un très beau chant.''
                |## {{term|le sujet est un être animé, l’objet est une caractéristique morale}}
                |##* '''''Avoir''' de l’ascendant, de l’autorité sur quelqu’un.''
                |##* '''''Avoir''' la confiance de quelqu’un.''
                |##* ''Tu peux '''avoir''' confiance en lui, je le connais.''
                |##* '''''Avoir''' l’estime de quelqu’un, '''avoir''' de l’estime pour quelqu’un.''
                |##* '''''Avoir''' des qualités, '''avoir''' des vertus, des vices…''
                |##* ''J’ai '''eu''' beaucoup de peine en apprenant son décès.''
                |## {{term|le sujet est un être animé, l’objet est une caractéristique abstraite}}
                |##* '''''Avoir''' [la] foi (en quelque chose ou en quelqu’un)''
                |##* '''''Avoir''' honte (de quelque chose)''
                |##* '''''Avoir''' pitié (de quelqu’un)''
                |##* '''''Avoir''' raison, avoir tort.''
                |##* '''''Avoir''' des sentiments (pour quelqu’un)''
                |##* '''''Avoir''' du temps devant soi, '''avoir''' le temps…''
                |##* ''Cet enfant '''a''' peur du loup.''
                |##* ''Vous n’'''avez''' pas le droit de faire ça.''
                |## {{term|L’objet n’a aucune valeur spécifique}}
                |##* '''''Avoir''' de la pluie, du beau temps…
                |##* ''Nous allons sûrement '''avoir''' beau temps cet après-midi.''
                |## {{term|le sujet est un être animé, l’objet est un être animé}}
                |##* '''''Avoir''' un père, une mère, une femme, des enfants, une nombreuse parenté…''
                |##* '''''Avoir''' un médecin, un notaire, un avocat…''
                |##* '''''Avoir''' un maître, des domestiques, un chef…''
                |##* '''''Avoir''' des élèves, des auditeurs…''
                |##* '''''Avoir''' des poux, des puces…''
                |##* '''''Avoir''' des convives, des hôtes…''
                |##* '''''Avoir''' des amis, des ennemis…''
                |##* '''''Avoir''' des correspondants, des associés, des complices…''
                |##* '''''Avoir''' des gens à sa suite.''
                |##* '''''Avoir''' quelqu’un chez soi.''
                |##* '''''Avoir''' une personne à dîner, à déjeuner…''
                |## {{term|le sujet est un être inanimé, l’objet est un être inanimé matériel ou abstrait}}
                |##* ''Cette ville '''a''' de beaux édifices, des rues larges, de vastes promenades.''
                |##* ''Votre château '''a''' un parc magnifique.''
                |##* ''Ma maison '''a''' cinq étages, a une belle vue.''
                |##* ''Cette table '''a''' deux tiroirs.''
                |##* ''Cette plante '''a''' de très belles fleurs.''
                |##* ''Cette planche '''a''' six pieds de long.''
                |##* ''Ce fruit '''a''' une forme allongée, une belle couleur.''
                |##* ''L’architecture de cet édifice '''a''' un caractère imposant.''
                |##* ''Cette poésie '''a''' de la douceur et de la grâce.''
                |##* ''Ce rêve '''a''' quelque chose d’effrayant.''
                |##* ''Cette pièce '''a''' beaucoup de succès.''
                |##* ''Un tel accident peut '''avoir''' des suites.''
                |##* ''Les plaisirs '''ont''' leurs dangers.''
                |## {{term|le sujet est un être inanimé, l’objet est un être animé}}
                |##* ''Cette maison '''a''' vingt locataires.''
                |##* ''Cette ville '''a''' dix mille habitants.''
                |##* ''Cet ouvrage '''a''' pour auteur un écrivain distingué.''
                |##* ''Cette doctrine '''a''' de nombreux partisans.''
                |## {{term|Avoir + article défini + nom + adjectif}} Son … est …, comme caractère personnel.
                |##* ''Il '''a''' l’œil bleu.''
                |##* ''Il '''a''' les cheveux blonds.''
                |## {{term|Avoir pour + nom + de + infinitif}} C’est son … de faire cela.
                |##* ''Si le monopole '''a''' pour but d’enrichir l’état, il '''a''' pour devoir de satisfaire à toutes les exigences de la consommation, […]'' {{source|Maxime Du Camp, ''[[s:Les Manufactures de Tabac : les établissemens du Gros-Caillou et de Reuilly|Les Manufactures de Tabac : les établissemens du Gros-Caillou et de Reuilly]]'', dans ''Revue des Deux Mondes'', tome 76, 1868}}
                |##* ''Ce canal, ouvert sur la rive gauche du Rhône, '''a''' pour but d’offrir à la navigation une voie indépendante des accidents du fleuve.'' {{source|{{w|Pierre-Joseph Proudhon}}, ''Canal d’arles à bouc'', dans ''[[s:Manuel du Spéculateur à la Bourse/Texte entier|Manuel du Spéculateur à la Bourse]]'', 1875}}
                |# {{term|utilisé [[intransitivement]], sans objet}} [[se passer|Se passer]], [[arriver]].
                |#* ''Qu’est-ce que tu '''as''' ? Tu te sens mal ?''
                |# [[se procurer|Se procurer]], [[obtenir]] quelque chose.
                |#* ''Il a '''eu''' tout ce qu’il demandait.''
                |#* ''J’ai '''eu''' ce cheval à très bon marché.''
                |#* ''Puis-je '''avoir''' la parole ?''
                |# {{familier|fr}} [[tromper|Tromper]], [[berner]], [[leurrer]] quelqu’un{{R|DicoHayard|8}}.
                |#* ''J’ai l’impression que je me suis fait '''avoir'''.''
                |#* ''Je t’ai '''eu'''.''
                |# {{term|J’ai … qui …}} {{popu|fr}} Introduit une nouvelle information concernant le locuteur sans relation [[thème]]-[[rhème]].
                |#* ''Oui, j’'''ai''' mon amie qui m’attend.'' {{source|{{w|Michèle {{pc|Rakotoson}}}}, ''Elle, au printemps'', 1996, {{ISBN|2907888641}}, page 22}}
                |#* ''J’'''ai''' ma mère qui est malade.''
                |# {{auxiliaire|fr}} [[auxiliaire|Auxiliaire]] servant à former les [[temps composé]]s, signifiant le [[passé]], la [[complétion]] ou l’[[expérience]]. {{note}} Certains verbes demandent ''[[être]]'' comme auxiliaire. En français moderne, le [[passé composé]] est beaucoup plus courant que le [[passé simple]].
                |#* ''J’'''ai''' bien mangé.''
                |#* ''Nous '''avons''' bien ri à lire toutes ces finesses, mais c’est beau l’instruction, quand même.''""".stripMargin

  val moelle = """[[File:Moellebw legendefr.svg|vignette|Schéma d'une section de la colonne vertébrale, où figure la '''moelle''' (3a).]]
                 |'''moelle''' {{pron|mwal|fr}} {{f}}
                 |# {{lexique|anatomie|fr}} [[moelle osseuse|Moelle osseuse]], [[moelle des os]].
                 |# {{lexique|biologie|histologie|fr}} [[substance|Substance]] [[mou|molle]] contenue dans les os. Il en existe deux types : la moelle osseuse rouge et la moelle osseuse jaune. La moelle osseuse rouge produit les cellules sanguines. La moelle jaune est du tissu adipeux.
                 |# {{lexique|boucherie|cuisine|fr}} De la moelle jaune [[cuisiner|cuisinée]].
                 |#* ''Dans les siècles des siècles, mes ancêtres paysans avaient respecté les œufs et la '''moelle''', nourritures de nababs. On donnait des œufs aux malades. Plus précieux encore dans le calice du coquetier. Quant à la '''moelle''', on la retirait parfois, le dimanche, à la pointe de la fourchette, d’un os de bœuf du pot-au-feu. On se partageait quelques miettes de cette substance tremblotante.'' {{source|{{nom w pc|Paul|Guth}}, ''Le mariage du Naïf'', 1957, réédition Le Livre de Poche, page 115}}
                 |#* ''Dans l’humérus du bœuf se cache un trésor de gras : l’os à '''moelle'''.'' {{source|Charles Patin O’Coohoon, [https://www.lexpress.fr/styles/saveurs/bouillon-bone-broth-fonds-de-sauce-les-os-s-invitent-dans-nos-assiettes_1857548.html Bouillon, bone broth, fonds de sauce… les os s'invitent dans nos assiettes], ''L’Express'', 9 janvier 2017}}
                 |#* ''Détailler la '''moelle''' de bœuf en petits dés, ainsi que les ailerons des chipirons.''{{source|Hélène Darroze, [http://madame.lefigaro.fr/recettes/chipirons-sautes-au-chorizo-et-riz-noir-010917-133774 Chipirons sautés au chorizo et riz noir], ''Figaro Madame'', 31 août 2017}}
                 |# {{lexique|cosmétologie|fr}} Moelle de bœuf.
                 |#* ''Ses cheveux noirs, lustrés à la '''moelle''' de bœuf, formaient des crochets sur ses tempes.'' {{source|{{w|Guy de Maupassant}}, ''La maison Tellier'', Albin Michel, 1881, collection Le Livre de Poche, page 13}}
                 |# {{figuré|fr}} L’[[essence]] d’un [[être]], d’une [[entité]].
                 |#* ''En supposant même qu’ils se prêtassent à un départ en masse, qui répugne au fond de leur nature et déchirerait toutes les fibres de leur cœur, qui pourrait voir s’éloigner sans effroi cette pépinière de nos armées, cette '''moelle''', cette substance de notre force nationale ?'' {{source|{{nom w pc|Albert|de Broglie}}, « [[s:Une Réforme administrative en Afrique/03|Une Réforme administrative en Afrique]] », ''Revue des Deux Mondes'', tome 25, 1860}}
                 |# {{figuré|fr}} Ce qu’il y a de plus [[instructif]], d’[[essentiel]], dans un [[ouvrage]] de l’[[esprit]].
                 |#* ''Ce livre est excellent, il faut savoir en tirer la substantifique '''moelle'''.''
                 |#* ''Nourri comme il l’était de la plus pure '''moelle''' de ''L’Encyclopédie'', il ne se bornait pas à parquer les humains à tel degré, tant de minutes et tant de secondes de latitude et de longitude. Il s’occupait de leur bonheur, hélas !'' {{source|{{w|Anatole France}}, ''Le crime de Sylvestre Bonnard'', Calmann-Lévy ; réédition Le Livre de Poche, 1967, page 131}}
                 |# {{lexique|anatomie|fr}} [[moelle épinière|Moelle épinière]].
                 |#* ''La '''moelle''' véhicule les faisceaux nerveux pour l’ensemble du corps.'' {{source|[http://sante.lefigaro.fr/sante/organe/moelle/quest-ce-que-cest Moelle : Qu’est-ce que c’est ?], ''Le Figaro Santé'', 2012}}
                 |# {{lexique|botanique|fr}} [[cylindre|Cylindre]] de [[tissu]] [[cellulaire]], [[tendre]], qui [[exister|existe]] au [[centre]] des [[tige]]s des [[plante]]s.
                 |#* ''Donefer et al. (1975) ont découvert aux Antilles que les bouts blancs de canne à sucre ensilés ajoutés à la '''moelle''' de canne à sucre entraînaient chez les bovins des gains de poids nettement supérieurs à ceux produits par l'alimentation basée sur la consommation de '''moelle''' à canne à sucre ou ''Digitaria decubens'' seulement.'' {{source|''L'élevage des petits ruminants dans les régions tropicales humides'', 1983}}
                 |#* ''La '''moelle''' utilisée pour fabriquer les feuilles de papyrus contient 54-58% de cellulose et 24-32% de lignine.''{{source|Brink, M. & Achigan-Dako, E.G., '' Ressources végétales de lAfrique tropicale 16 : Plantes à fibres'', fondation PROTA, 2012, page 130}}
                 |#* ''Des compagnons de Cook ayant remarqué que les Aborigènes mangeaient des « graines » (en réalité des prégraines pour un botaniste) et la '''moelle''' d’un Cycas, des membres de l’équipage voulurent en récolter et y goûter.'' {{source|Guillaume {{pc|Jean}}, ''Ils ont domestiqué plantes et animaux : Prélude à la civilisation'', 2013, page 283}}""".stripMargin

  val autrice =  """# {{lien|femme|fr|dif=Femme}} à l’{{lien|origine|fr}} de {{lien|quelque chose|fr}}.
                   |#* {{exemple
                   ||Cette situation familiale, prise en compte dans le processus de personnalisation des peines au même titre que la personnalité de l’auteur ou de l’'''autrice''', la gravité et les circonstances de l’infraction, fait que, dans bien des cas, la criminelle n’est pas seule à la barre
                   ||source={{périodique|auteur=Lucile Quillet|titre=Les femmes, ces criminelles (presque) comme les hommes|journal={{w|Slate (magazine)|Slate}}|date=4 mars 2019|url=http://www.slate.fr/story/174153/societe/les-femmes-et-le-crime-episode-1-criminelles-comme-hommes}}
                   ||lang=fr
                   |}}
                   |#* {{exemple
                   ||L’'''autrice''' présumée de l’attaque a été arrêtée à Seattle.
                   ||source={{périodique|auteur=Harold Grand|titre=Les données personnelles de 100 millions d’Américains exposées dans un piratage géant|journal={{w|Le Figaro}}|date=30 juillet 2019|url=https://www.lefigaro.fr/secteur/high-tech/les-donnees-personnelles-de-100-millions-d-americains-exposees-dans-un-piratage-geant-20190730}}
                   ||lang=fr
                   |}}
                   |# {{lexique|littérature|fr}} {{lien|femme|fr|dif=Femme}} qui a rédigé un {{lien|ouvrage|fr}} de {{lien|littérature|fr}}, de {{lien|science|fr}} ou d’{{lien|art|fr}}. {{absolument}} {{lien|écrivaine|fr|dif=Écrivaine}}.
                   |#* {{exemple
                   ||Au Carouzel de nôtre Roy<br/>La jeune '''Autrice''' de Torquate<br/>Piéce charmante et délicate<br/>A fait, en ſtile net & fin,<br/>Vn Carouzel pour le Daufin
                   ||source={{ouvrage|prénom=Jean|nom=Loret|titre=La Muse historique du 27 mai 1662|date=1665|passage=78|url={{Citation/Books.Google|5r_aFST4AYUC|PA78|surligne=Autrice}}}}
                   ||lang=fr
                   |}}
                   |#*: Au carrousel de notre roi<br/>La jeune '''autrice''' de Torquate<br/>Pièce charmante et délicate<br/>A fait, en style net et fin,<br/>Un carrousel pour le dauphin
                   |#* {{exemple
                   ||Corrigez le mot dont vous uſez.<br/>En fait de bel Eſprit vous parlez en Novice.<br/>Un Homme eſt un Autheur, une Femme eſt '''Autrice'''.<br/>Appelez-donc Madame '''Autrice''', & non Autheur,<br/>Et parlons d’autre choſe.
                   ||source={{ouvrage|prénom=Gaspard|nom=Abeille|titre=Crispin bel esprit|lieu=Paris|date=1696|année première édition=1681|passage=56|url=https://gallica.bnf.fr/ark:/12148/bpt6k3042306t/f39}}
                   ||lang=fr
                   |}}
                   |#*: Corrigez le mot dont vous usez.<br/>En fait de bel esprit vous parlez en novice.<br/>Un homme est un auteur, une femme est '''autrice'''.<br/>Appelez-donc Madame '''autrice''', et non auteur,<br/>Et parlons d’autre chose.
                   |#* {{exemple
                   ||Si l’on ne dit pas une femme '''Autrice''', c’est qu’une femme qui fait un livre est une femme extraordinaire ; mais il est dans l’ordre qu’une femme aime les spectacles, la poésie, etc. comme il est dans l’ordre qu’elle soit Spectatrice.
                   ||source=M. Hilscher, cité dans {{ouvrage|prénom=Louis-Sébastien|nom=Mercier|titre=Néologie, ou vocabulaire des mots nouveaux, à renouveler, ou pris dans des acceptions nouvelles|lieu=Paris|éditeur=Moussard, Maradan|date=1801|tome=1|passage=26|url={{Citation/Books.Google|5pQPAAAAQAAJ|PA26|surligne=autrice}}}}
                   ||lang=fr
                   |}}
                   |#* {{exemple
                   ||Un journal discourait naguère sur ''authoresse'', et, le proscrivant avec raison, le voulait exprimer par ''auteur''. Pourquoi cette réserve, cette peur d’user des forces linguistiques ? Nous avons fait ''actrice'', ''cantatrice'', ''bienfaitrice'', et nous reculons devant '''autrice''' […] Autant avouer que nous ne savons plus nous servir de notre langue
                   ||source={{ouvrage|prénom=Remy|nom=de Gourmont|titre=Esthétique de la langue française|éditeur={{w|Mercure de France}}|lieu=Paris|date=1899|passage=37|url=https://gallica.bnf.fr/ark:/12148/bpt6k295676/f90}}
                   ||lang=fr
                   |}}
                   |#* {{exemple
                   ||Exemple de vocable de la liste précédente employé par une '''autrice''' contemporaine : […]
                   ||source={{périodique|prénom=Jacques|nom=Damourette|titre=Le lexique du Dictionnaire de l’Académie française (Huitième édition)|journal={{w|Le Français moderne}}|no=1|date=1935|pages=71|url=https://fr.calameo.com/read/0009039476be3a5c7e240}}
                   ||lang=fr
                   |}}
                   |#* {{exemple
                   ||Parce que le mot sonne clair, affirme sa féminité, s’appuie sur l’histoire et la proximité d’''actrice'', les ''Livres du Soir'' diront, dorénavant, '''''autrice'''''.
                   ||source={{périodique|auteur=Jean-Claude Vantroyen|titre=Auteur, auteure ou autrice ? « Le Soir » choisit autrice|journal={{w|Le Soir}}|date=5 janvier 2019|url=https://plus.lesoir.be/198562/article/2019-01-05/auteur-auteure-ou-autrice-le-soir-choisit-autrice}}
                   ||lang=fr
                   |}}
                   |# {{plus rare}} {{lien|génitrice|fr|dif=Génitrice}} ; {{lien|ascendante|fr}} ({{note}} ce sens n’est plus guère utilisé que sous la forme de la locution « {{lien|autrice de mes jours|fr}} »).
                   |#* {{exemple
                   ||Au commencement de cette lettre l’'''Autrice''' de l’Impetrant a taché de capter la bienveillance de ſa belle Sœur, en lui voulant procurer un Docteur pour la guerir de ſon chancre, dont elle eſt morte peu de tems après.
                   ||source={{ouvrage|titre=Reflexions paratitlaires pour la Dame Marie Joſephe Comteſſe Douariere… contre Meſſire Frederic Comte d’Eynatten Seigneur…|date=1728|passage=12|url={{Citation/Books.Google|zf9CAAAAcAAJ|PA12|surligne=autrice}}}}
                   ||lang=fr
                   |}}
                   |#*: Au commencement de cette lettre l’'''autrice''' de l’impétrant a tâché de capter la bienveillance de sa belle-sœur, en lui voulant procurer un docteur pour la guérir de son chancre, dont elle est morte peu de temps après.""".stripMargin

  val blanc = """{{fr-accord-mixte|ms=blanc|fs=blanche|pm=blɑ̃|pf=blɑ̃ʃ}}
                |'''blanc''' {{pron|blɑ̃|fr}} {{m}}
                |# D’une [[couleur]] comme celle des [[os]], de la [[craie]] ou de l’[[écume]] entre autre. — {{note}} Comme l’adjectif [[noir]] ou [[gris]], qui ne sont que des différences de [[ton]] du [[#fr-nom-1|blanc]], il implique souvent une notion d’absence de couleur, de teinte.{{couleur|#FFFFFF}}
                |#* ''On ne pouvait voir de fille plus fraîche, plus riante ; elle était blonde, avec de beaux yeux bleus, des joues roses et des dents '''blanches''' comme du lait ; […].'' {{source|{{w|Erckmann-Chatrian}}, ''[[s:Histoire d’un conscrit de 1813/1|Histoire d’un conscrit de 1813]]'', J. Hetzel, 1864}}
                |#* ''Il neige dur. La place est toute '''blanche'''.'' {{source|Albert Vidalie, ''C’était donc vrai'', éditions René Julliard, 1952, page 34}}
                |#* ''La salle des fêtes était habillée pour une grande cérémonie, de somptueux rideaux '''blancs''' et pourpres qui tombaient du haut plafond jusqu'au bas des portes et des fenêtres.'' {{source|Maboa Bebe, ''Ewande Amours, peurs, espoir'', L’Harmattan Cameroun, 2014, page 7}}
                |# {{par extension|fr}} D’une couleur appartenant au [[champ chromatique]] du [[#fr-nom-1|blanc]], c’est-à-dire légèrement différente de la couleur originelle mais lié à celle-ci.{{couleur|#FFFFFF}}{{couleur|#FFFFD4}}{{couleur|#FDE9E0}}{{couleur|#FDE1B8}}{{couleur|#F2FFFF}}{{couleur|#EFEFEF}}
                |# Très peu coloré, ou moins coloré que, en parlant de choses qui ne sont pas tout à fait '''blanches''' (1), pour les [[distinguer]] de celles de même espèce qui le sont moins, ou qui sont d’une autre couleur.
                |#* ''Il y a des betteraves '''blanches''', il y en a de jaunes, de rouges, et de marbrées, et quelquefois la pellicule est rouge et la chair est '''blanche'''.'' {{source|{{w|Jean-Antoine Chaptal}}, ''[[s:Mémoire sur le sucre de betterave|Mémoire sur le sucre de betterave]]'', Mémoires de l’Académie des sciences, tome 1, 1818 (pages 347-388)}}
                |#* ''Bois''' blanc''', métal '''blanc''', mûrier '''blanc''', pain '''blanc''', poisson '''blanc''', poivre '''blanc''', raisin '''blanc''', sauce '''blanche''', sel '''blanc''', viande '''blanche''', vin '''blanc'''.''
                |#* ''En sabots, les manches relevées sur leurs bras '''blancs''', les filles balayaient le devant des boutiques''. {{source|{{Citation/Francis Carco/Brumes/1935|158}}}}
                |#* ''Avoir le teint '''blanc''', la gorge '''blanche''', les mains '''blanches''', la peau '''blanche'''.''
                |#* ''Gelée '''blanche''','' Gelée qui, le matin, se forme de la rosée ou du brouillard congelé.
                |#* ''Faire des lotions avec de l’eau '''blanche'''.''
                |#* ''Ver '''blanc''','' Larve du hanneton.''
                |# {{lexique|sociologie|fr}} Qui appartient au groupe (ethnique, social ou biologique, selon le point du vue) des [[Blancs]].
                |#* ''Betty Bonifassi, selon cette logique, ne pouvait chanter ce qu'elle avait contribué à sortir de l'oubli : les chants d'esclaves noirs. Elle est '''blanche'''.'' {{source|{{w|Marie-France Bazzo}}, ''Nous méritons mieux'', Boréal, 2020, page 170}}
                |#* ''Geoffroy, Maxime, Frédéric, Jérémy et Billy sont cinq hommes '''blancs''' dans la trentaine, travaillant dans le secteur manuel et originaires de l'Essonne.''{{source|www.francetvinfo.fr, ''[https://www.francetvinfo.fr/economie/transports/gilets-jaunes/ils-n-ont-meme-pas-manifeste-et-les-voila-en-garde-a-vue-apres-les-violences-a-paris-le-defile-des-gilets-jaunes-au-tribunal_3083317.html Le défilé des gilets jaunes au tribunal]'', 04/12/2018}}
                |#* ''DiAngelo est une femme '''blanche''' qui a passé des dizaines d'années délibérément embourbée dans la question de la race.'' {{source|Bérengère Viennot, [http://www.slate.fr/story/185459/etats-unis-sociologie-fragilite-blanche-robin-diangelo-racisme-progressisme-1 ''« La Fragilité blanche » brise les idées reçues sur les progressistes''], slate.fr, 2019, page 38}}
                |# {{lexique|sociologie|fr}} Relatif à la [[race]] blanche, au groupe social des [[Blanc]]s.
                |#* ''C’est dans cette perspective que nous nous situerons, en nous inscrivant notamment dans le sillage des travaux conduits dans le champ anglophone des critical white studies, qui étudie la construction sociale des identités et subjectivités '''blanches'''.'' {{source|Maxime Cervulle, [https://www.cairn.info/revue-cahiers-du-genre-2012-2-page-37.htm « La conscience dominante. Rapports sociaux de race et subjectivation »], n° 53, ''Cahiers du genre'', 2012, page 38}}
                |#* ''Cela étant dit, le privilège '''blanc''' n’est pas un sujet facile à aborder.'' {{source|Kevin Sweet, [https://ici.radio-canada.ca/nouvelle/1507954/mois-histoire-noirs-privilege-blanc-reflexion ''L'éveil du Blanc privilégié et sa fragilité''], ici.radio-canada.ca, 2020}}
                |# {{spécialement|fr}} {{Canada|fr}} Qui est allochtone, non [[amérindien]]ne.
                |#* ''[Mme] Hunter […] a donné naissance à une fille ce matin, le premier enfant '''blanc''' dans Teslin. Ah ! Pourtant non, pas '''blanc''', car c’est une famille de Noirs, et pourtant on s’[[obstiner|obstine]] ici à dire que c’est la naissance du premier enfant '''blanc'''. On veut sans doute dire que c’est le premier enfant né à Teslin de parents civilisés.'' {{source|''17 Eldorado'', Journal de Lorenzo Létourneau (1899), Qualigram/Linguatech, Montréal, 2006}}
                |# [[vierge#fr|Vierge]], sur lequel il n’y a rien d’écrit ou d’imprimé.
                |#* ''Un feuillet de papier '''blanc'''.''
                |#* ''Une page '''blanche'''.''
                |#* ''Un cahier '''blanc'''.''
                |#* ''Livre '''blanc''','' livre dont tous les feuillets sont blancs.
                |# [[vierge|Vierge]].
                |#* ''Bal '''blanc''','' bal où ne sont invitées que des jeunes filles.
                |#* ''Mariage '''blanc''','' mariage qui n’a pas été consommé.
                |# {{par ext|fr}} Vide de contenu.
                |#* ''Pour certains, ce taf était juste un moyen commode de remplir un '''blanc''' dans leur CV en attendant qu’un membre de leur famille leur trouve une meilleure place.'' {{source|{{w|David Graeber}}, traduit par Élise Roy, ''Bullshit jobs'', {{W|Les liens qui libèrent}}, 2018, {{ISBN|979-10-209-0633-5}}}}
                |# {{lexique|militaire|fr}} À lame d'[[acier]].
                |#* ''Armes '''blanches''','' armes offensives, comme épées, sabres, baïonnettes, etc., par opposition aux armes à feu. On appelait autrefois aussi ''armes '''blanches''''' les armes défensives qui n’étaient ni gravées, ni dorées, ni bronzées.
                |#* ''Se battre à l’arme '''blanche'''.''
                |#* ''Combat à l’arme '''blanche'''.''
                |# [[clair#fr|Clair]].
                |#* ''Voix '''blanche''','' voix claire mais qui manque de timbre.
                |#* ''Il prenait pour parler une voix '''blanche''', doucereuse, déclamait des bouts d’idées ramassées un peu partout, sur les droits de l’ouvrier, la tyrannie du capital.'' {{source|{{w|Alphonse Daudet}}, ''Arthur'', dans ''Contes du lundi'', 1873, Fasquelle, collection Le Livre de Poche, 1974, page 168}}
                |#* ''Argent '''blanc''', Bâton '''blanc''', Bulletin '''blanc''', Houille '''blanche''', Magie '''blanche''', Nuit '''blanche''', Sauce '''blanche''', Viande '''blanche'''.'' Voyez « argent », « bâton », « bulletin », etc.
                |# {{lexique|jeux|fr}} [[coup|Coup]] qui ne [[produit]] [[rien]].
                |#* ''Amener '''blanc''','' à certains jeux de Dés, se dit lorsque tous les dés présentent la face qui n’est marquée d’aucun point.
                |#* ''Coup '''blanc''','' coup nul.
                |#* ''Opération '''blanche''','' Opération qui n’a pas donné de résultat.
                |#* {{figuré|fr}} ''Vers '''blancs''','' en termes de Versification, Vers non rimés.''
                |#* ''Le Paradis perdu " de Milton est en vers '''blancs'''.''
                |# {{lexique|foresterie|fr}} Plusieurs termes d'[[exploitation]] [[forestier|forestière]] recourent à des [[locution nominale|locutions nominales]] employant l'adjectif '''blanc''' telles que
                |## ''Couper une forêt, faire une coupe à '''blanc''' estoc'' ou
                |## ''À '''blanc''' être,'' ou simplement
                |## ''À '''blanc''': ces expressions signifient toutes ''en couper tout le bois, sans y laisser de baliveaux.''
                |##* On dit dans le même sens: une '''''coupe blanche''''', qui en est le résultat.
                |##* ''Couper un arbre à '''blanc''' estoc'' : le couper au pied sur la souche.
                |# {{proverbial|fr}} L'adjectif ''blanc'' s'emploie aussi dans des [[locution verbale|locutions verbales]] issues du vocabulaire des [[duel]]s [[judiciaire]]s du [[Moyen Âge]] et parfois utilisées dans le français moderne:
                |## ''Se faire tout '''blanc''' de son épée, se faire '''blanc''' de son épée,'' : répondre à une accusation, se justifier d’une accusation par l’épée, par la force.
                |## {{figuré|fr}} {{par extension|fr}} ''Faire '''blanc''' de son épée'': s'emploie au figuré pour signifier ''se prévaloir de sa force, se vanter de faire quelque chose en se supposant un pouvoir ou un crédit qu’on n’a pas.''
                |# Qui est [[propre]], par opposition à [[sale]].
                |#* ''Linge '''blanc'''.''
                |#* ''Ces draps ont servi, ils ne sont pas '''blancs'''.''
                |#* ''Chemise '''blanche'''.''
                |#* ''Nappe '''blanche'''.''
                |#* ''Serviette '''blanche'''.''
                |#* '''''Blanc''' de lessive'' se dit du Linge propre, tel qu’il est au sortir de la lessive.
                |#* ''Ces draps, ces rideaux sont '''blancs''' de lessive.''
                |#* {{figuré|fr}} {{familier|fr}} ''Sortir d’une accusation, d’une affaire '''blanc''' comme neige,'' ou simplement
                |#* ''Sortir '''blanc''' d’une affaire,'' être déclaré innocent, être acquitté par un arrêt ou un jugement, en matière criminelle ou correctionnelle.
                |#* ''Vouer un enfant au '''blanc''','' Faire vœu qu’un enfant sera entièrement vêtu de '''blanc''', jusqu’à tel âge, en l’honneur de la sainte Vierge.''
                |#* ''Se vouer au '''blanc'''.''""".stripMargin

}