/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Drole {
  val drole = """{{voir|drolé|drole}}
                |
                |== {{langue|fr}} ==
                |=== {{S|étymologie}} ===
                |: Du {{étyl|nl|fr|mot=drolle}}, ''[[drol]]'' qui désigne au sens propre un [[lutin]] et au sens figuré un être joyeux et bon vivant, apparenté à ''[[troll]]''.
                |: {{note}} {{refnec|lang=fr|En Normandie le lutin est appelé ''drol'' ou ''drôle.''}}
                |
                |=== {{S|adjectif|fr}} ===
                |{{fr-rég|dʁol|mf=oui}}
                |'''drôle''' {{pron|dʁol|fr}} {{mf}} ({{superlatif}} : [[drôlissime]])
                |# [[comique|Comique]] ; [[marrant]] ; [[rigolo]].
                |#* ''Quelques alcoolos faisaient un cirque vaseux. Les alcoolos m’emmerdent, ils ne sont jamais '''drôles'''.'' {{source|{{w|Claude Courchay}}, ''Les Américains sont de grands enfants'', Flammarion, 1979, page 136}}
                |#*''''On a beaucoup ri, il est vraiment très '''drôle'''. — Un conte fort '''drôle'''. — Est '''drôle''' quiconque qui se fait rire.''
                |# [[bizarre|Bizarre]], [[inhabituel]], qui [[sortir|sort]] du [[commun]].
                |#* ''C’est '''drôle''', mais c’est comme ça…'' {{source|{{w|Guy de Maupassant}}, ''[[s:Madame Baptiste|Madame Baptiste]]'', 1882}}
                |#* ''Nous allions monter en voiture, quand une espèce de petit bonhomme tout '''drôle''', pas très vieux, mais pas extraordinairement jeune non plus, fort sec, nous demanda poliment si nous rentrions à Honfleur.'' {{source|{{w|Alphonse Allais}}, ''[[s:À l’œil|À l’œil]]'', ''[[s:Phares|Phares]]'', 1921}}
                |#* ''C’est '''drôle''' qu’il se fût plutôt entiché de la gamine… Céline Thiébault pourtant était plus en rapport d’âge avec lui. Ce n’eût été ni moins décent, ni moins excusable.'' {{source|{{Citation/Jean Rogissart/Hurtebise aux griottes/1954|92}}}}
                |#* ''Mythologie du village français ? Oui, complètement ! C’est même '''drôle''' à quel point nous recouvrons la réalité par des métaphores et des mythes. Actuellement, ce sont l’image de la guerre et de l’abri qui dominent.'' {{source|{{w|Michel Eltchaninoff}}, « ''Carnet de la drôle de guerre'' », dans la [[newsletter]] du 21/03/220 de ''{{w|Philosophie Magazine}}''.}}
                |#* {{exemple|lang=fr|Les détenteurs de robots cuiseurs multifonction forment une '''drôle''' de tribu enjouée, bavarde et prosélyte dont les membres se reconnaissent à leurs propos extatiques : « Ça change la vie, honnêtement, je ne m’en passerais plus. » De cette addiction électroménagère, le marché français témoigne, année après année.|source= Pascale Krémer, « Le Thermomix, j’en parle tout le temps » : comment les robots-cuiseurs ont envahi les foyers français, Le Monde, 11 février 2022 <!-- https://www.lemonde.fr/m-le-mag/article/2022/02/11/des-amis-se-sont-separes-la-question-s-est-posee-qui-garde-le-thermomix-les-proselytes-du-robot-cuiseur_6113323_4500055.html --> }}
                |
                |==== {{S|synonymes}} ====
                |* [[comique]]
                |* [[désopilant]]
                |* [[hilarant]]
                |* [[rigolo]]
                |
                |==== {{S|dérivés}} ====
                |* [[c’est encore drôle]] {{Québec|nocat=1}}
                |* [[drôlatique]] (1)
                |* [[drôle de]] (2)
                |* [[drôle de guerre]]
                |* [[drôlement]] (2)
                |* [[drôlerie]]
                |* [[drôlet]]
                |
                |==== {{S|faux-amis}} ====
                |* {{F|fr|en}} : [[droll#en|droll]]
                |
                |==== {{S|traductions}} ====
                |{{trad-début|Comique, marrant, rigolo (sens général)|1}}
                |* {{T|af}} : {{trad+|af|eienaardig}}, {{trad-|af|amusant}}
                |* {{T|de}} : {{trad+|de|ulkig}}, {{trad+|de|lustig}} (amusant), {{trad+|de|spaßig}}, {{trad+|de|amüsant}}, {{trad+|de|drollig}}, {{trad+|de|unterhaltend}}, {{trad+|de|unterhaltsam}}, {{trad+|de|ergötzlich}}, {{trad+|de|belustigend}}, {{trad+|de|kurzweilig}}
                |* {{T|en}} : {{trad+|en|funny}}
                |* {{T|br}} : {{trad+|br|farsus}}, {{trad+|br|c’hoarzhus}}, {{trad+|br|fentus}}
                |* {{T|ca}} : {{trad+|ca|estrany}}, {{trad+|ca|còmic}}
                |* {{T|hr}} : {{trad+|hr|smiješan}}
                |* {{T|da}} : {{trad+|da|gal}}, {{trad-|da|sær}}, {{trad-|da|tosset}}, {{trad-|da|underlig}}, {{trad-|da|komisk}}, {{trad+|da|sjov}}, {{trad-|da|morsom}}
                |* {{T|es}} : {{trad+|es|gracioso}}, {{trad+|es|chistoso}}, {{trad+|es|chusco}}, {{trad+|es|cómico}}, {{trad+|es|jocoso}}, {{trad+|es|divertido}}
                |* {{T|eo}} : {{trad-|eo|ŝerca}}, {{trad-|eo|stranga}}, {{trad-|eo|ridiga}}, {{trad-|eo|komika}}, {{trad-|eo|gajiga}}, {{trad-|eo|amuza}}
                |* {{T|fo}} : {{trad-|fo|serur}}, {{trad-|fo|egin}}, {{trad-|fo|løgin}}, {{trad-|fo|løgin}}, {{trad-|fo|margháttligur}}, {{trad-|fo|láturligur}}
                |* {{T|fi}} : {{trad+|fi|kummallinen}}
                |* {{T|vls}} : {{trad|vls|fienn}}
                |* {{T|fy}} : {{trad-|fy|frjemd}}, {{trad-|fy|komysk}}, {{trad-|fy|fermaaklik}}
                |* {{T|el}} : {{trad+|el|αστείος|R=asteíos}}
                |* {{T|hu}} : {{trad+|hu|tréfás}}, {{trad+|hu|vicces}}, {{trad-|hu|vidámmá tevő}}, {{trad+|hu|szórakoztató}}
                |* {{T|io}} : {{trad+|io|drola}}
                |* {{T|is}} : {{trad+|is|skrýtinn}}, {{trad+|is|vitlaus}}
                |* {{T|it}} : {{trad+|it|strano}}, {{trad-|it|bizzaro}}, {{trad+|it|comico}}, {{trad+|it|divertente}}
                |* {{T|crg}} : {{trad|crg|drool}}
                |* {{T|nl}} : {{trad+|nl|boertig}}, {{trad+|nl|luimig}}, {{trad+|nl|schertsend}}, {{trad+|nl|eigenaardig}}, {{trad+|nl|gek}}, {{trad+|nl|raar}}, {{trad+|nl|vreemd}}, {{trad+|nl|vreemdsoortig}}, {{trad+|nl|wonderlijk}}, {{trad+|nl|lollig}}, {{trad+|nl|lachwekkend}}, {{trad+|nl|grappig}}, {{trad+|nl|koddig}}, {{trad+|nl|komisch}}, {{trad+|nl|moppig}}, {{trad+|nl|amusant}}, {{trad+|nl|aardig}}, {{trad+|nl|amusant}}, {{trad+|nl|leuk}}, {{trad+|nl|vermakelijk}}
                |* {{T|no}} : {{trad+|no|gal}}, {{trad+|no|rar}}, {{trad-|no|sprø}}
                |* {{T|oc}} : {{trad+|oc|estranh}}, {{trad+|oc|risolièr}}, {{trad+|oc|dròlle}}, {{trad+|oc|plasent}}
                |* {{T|pap}} : {{trad|pap|strabagante}}, {{trad|pap|straño}}, {{trad|pap|kómiko}}, {{trad|pap|chaskero}}, {{trad|pap|chistoso}}, {{trad|pap|dibertido}}
                |* {{T|pl}} : {{trad+|pl|zabawny}}
                |* {{T|pt}} : {{trad+|pt|engraçado}}, {{trad+|pt|cómico}}, {{trad+|pt|cômico}}, {{trad+|pt|divertido}}, {{trad+|pt|recreativo}}
                |* {{T|ro}} : {{trad+|ro|caraghios}}, {{trad+|ro|nostim}}, {{trad+|ro|haios}}
                |* {{T|ru}} : {{trad+|ru|смешной}}
                |* {{T|se}} : {{trad|se|somá}}
                |* {{T|zdj}} : {{trad|zdj|outsésa}}
                |* {{T|srn}} : {{trad|srn|prisiri}}
                |* {{T|sv}} : {{trad+|sv|lustig}}, {{trad+|sv|löjlig}}
                |* {{T|tsolyáni}} : {{trad|tsolyáni|kháichan}}
                |* {{T|tr}} : {{trad+|tr|gülünç}}
                |{{trad-fin}}
                |
                |{{trad-début|Bizarre, inhabituel, qui sort du commun.|2}}
                |* {{T|de}} : {{trad+|de|komisch}} (bizarre), {{trad+|de|eigenartig}}, {{trad+|de|merkwürdig}}, {{trad+|de|sonderbar}}
                |* {{T|en}} : {{trad+|en|funny}} {{figuré|nocat=1}}, {{trad+|en|strange}}, {{trad+|en|odd}}
                |* {{T|hr}} : {{trad-|hr|čudan}}
                |* {{T|es}} : {{trad+|es|extraño}}, {{trad+|es|curioso}}, {{trad+|es|raro}}
                |* {{T|oc}} : {{trad+|oc|dròlle}}
                |* {{T|pms}} : {{trad|pms|drolo}}, {{trad|pms|drola}}
                |* {{T|pt}} : {{trad+|pt|estranho}}, {{trad+|pt|bizarro}}, {{trad+|pt|esquisito}}, {{trad+|pt|singular}}, {{trad+|pt|diferente}}
                |* {{T|ro}} : {{trad+|ro|caraghios}}, {{trad+|ro|ridicol}}
                |* {{T|sv}} : {{trad+|sv|lustig}}, {{trad+|sv|konstig}}
                |* {{T|tr}} : {{trad+|tr|acayip}}, {{trad+|tr|tuhaf}}
                |* {{T|uk}} : {{trad-|uk|дивний}}
                |{{trad-fin}}
                |
                |{{trad-début|Traductions à trier}}
                |* {{T|en|trier}} : {{trad+|en|wacky}}, {{trad+|en|cheering}}, {{trad+|en|entertaining}}, {{trad+|en|enjoyable}}, {{trad+|en|pleasure}}, {{trad+|en|strange}}, {{trad+|en|weird}}, {{trad+|en|odd}}
                |{{trad-fin}}
                |
                |=== {{S|nom|fr}} ===
                |{{fr-rég|dʁol}}
                |'''drôle''' {{pron|dʁol|fr}} {{m}} {{équiv-pour|lang=fr|une femme|drôlesse|drôlière}}
                |# {{vieilli|fr}} {{péjoratif|fr}} [[personne|Personne]] [[roué]]e, mauvais sujet, dont il faut se [[méfier]].
                |#* ''Or j’avons vu tantôt passer ce méchant '''drôle'''.'' {{source|{{pc|{{w|Marivaux}}}}, ''Le Père prudent et équitable''}}
                |#* […]'' les mynheers d’Erchin ''[…]'' fumaient leurs pipes en silence au « Bon Couvet », quand voilà qu’un grand '''drôle''', vêtu, comme un Jean Potage, d’une veste de velours brodée de paillons, s’arrêta devant le cabaret.'' {{source|{{w|Charles Deulin}}, ''{{ws|Les Muscades de la Guerliche}}''}}
                |#* {{exemple|lang=fr|— Ah ! vous voilà ? dit-il en allant donner une poignée de main à Léon et à Bixiou. '''Drôles''' !… que venez-vous faire dans le sanctuaire des lois ?|source={{w|Honoré de Balzac}}, ''{{w|Les Comédiens sans le savoir}}'', 1846}}
                |#* ''En vrai soldat de l’école impériale, Dumay ''[…]'' se représentait un poète comme un '''drôle''' sans conséquence, un farceur à refrains.'' {{source|{{Citation/Honoré de Balzac/Modeste Mignon/1855}}}}
                |#* ''Qu’est-ce que cela signifie ? murmura le capitaine avec colère ; mes '''drôles''' se sont-ils donc laissé surprendre ?'' {{source|{{Citation/Gustave Aimard/Les Trappeurs de l’Arkansas/1858}}}}
                |#* ''Dans les nuits d’été, les marches de granit du théâtre sont couvertes d’un tas de '''drôles''' qui n’ont pas d’autre asile. Chacun a son degré qui est comme son appartement, où l’on est toujours sûr de le retrouver.'' {{source|{{w|Théophile Gautier}}, ''{{ws|Voyage en Espagne/XI|Voyage en Espagne}}'', Charpentier, 1859}}
                |#* ''Je croyais tenir mon gaillard. Il m’a glissé des pattes… C’est un '''drôle'''.'' {{source|{{Citation/Francis Carco/Messieurs les vrais de vrai/1927}}}}
                |#* {{exemple|Mais grâce à vous, mes enfants, le butin du '''drôle''' a été récupéré.|source=Greg et Alain Saint-Ogan, ''Zig et Puce – Le voleur fantôme'', éditions du Lombard, 1974, page 46|lang=fr}}
                |# {{région|Sud et sud-ouest de la France}} [[enfant|Enfant]] ; [[marmot]] ; [[gamin]] quand il s’agit d’un garçon. Pour une fille on dira drôle ou [[drôlière]], [[drôlesse]] ayant un sens légèrement péjoratif.
                |#* […]'' et puis, je me suis un peu détourné de la ligne droite pour aller jeter à la rivière un affreux enfant qui criait : À bas les papistes, vive l’amiral ! Malheureusement, je crois que le '''drôle''' savait nager.'' {{source|{{Citation/Alexandre Dumas/La Reine Margot/1886|I|IX}}}}
                |
                |==== {{S|variantes orthographiques}} ====
                |* {{lien|drole|fr}}
                |
                |==== {{S|synonymes}} ====
                |; Personne louche {{cf|lang=fr|salaud}}
                |
                |==== {{S|traductions}} ====
                |{{trad-début|{{vieilli|nocat=1}} {{péjoratif|nocat=1}} Personne dont le comportement suscite de la méfiance.|1}}
                |* {{T|en}} : '''un drôle de''' : {{trad+|en|funny||dif=funny kind of}} ; '''un drôle''' : {{trad+|en|funny|dif=funny kind of person}}
                |* {{T|oc}} : {{trad-|oc|un dròlle de}}
                |{{trad-fin}}
                |
                |===== {{S|traductions à trier}} =====
                |{{trad-début}}
                |* {{T|de|trier}} : {{trad+|de|Schurke}}, {{trad+|de|Strolch}}, {{trad+|de|Gauner}}
                |* {{T|es|trier}} : {{trad+|es|bribón|m}}, {{trad+|es|truhán|m}}
                |{{trad-fin}}
                |
                |=== {{S|prononciation}} ===
                |* {{pron|dʁol|fr}}
                |** {{écouter|lang=fr|France|dʁol|audio=Fr-drôle.ogg}}
                |* {{term|Sud de la France}} {{term|français méridional}} {{pron|dʁɔl|fr}}
                |* {{CA|nocat=1}} {{pron|dʁou̯l|fr}}
                |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Penegal-drôle.wav}}
                |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-drôle.wav}}
                |
                |=== {{S|voir aussi}} ===
                |* [http://www.tv5monde.com/cms/chaine-francophone/lf/Merci-Professeur/p-17081-Drole.htm?episode=437 Explication de Bernard Cerquiglini en images]
                |
                |=== {{S|références}} ===
                |* {{R:Larousse15vol1991}}
                |* {{R:Index-FEW|15/2|72|mot=drol|ref=oui}}
                |* {{R:TLFi}}
                |
                |== {{langue|en}} ==
                |=== {{S|adjectif|en}} ===
                |'''drôle''' {{pron||en}}
                |# {{cf|drole|lang=en}}
                |""".stripMargin
}
