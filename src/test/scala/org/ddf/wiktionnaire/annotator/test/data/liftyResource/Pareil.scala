/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Pareil {
  val resource = Resource("pareil", "https://fr.wiktionary.org/wiki/", "entry",
  """== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: {{date|lang=fr}} Du {{étyl|la|fr|mot=pariculus|sens=semblable, égal}}.
    |
    |=== {{S|adjectif|fr}} ===
    |{{fr-accord-cons|pa.ʁɛj||l}}
    |'''pareil''' {{pron|pa.ʁɛj|fr}}
    |# Qui [[présenter|présente]] une [[fort]]e [[ressemblance]] ou [[similitude]] ; qui est [[égal]] ou [[semblable]], en parlant de [[deux]] [[personne]]s ou de deux [[chose]]s.
    |#* ''Et de son côté, le récent hiver a roussi les oliviers, dépenaillé les grands mimosas, '''pareils''' à des mâts dont les haubans sont coupés, et où pendent des paquets de vieilles voilures déralinguées.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
    |#* ''Ce qu’elle chantait – ah ! la fatale et maudite chanson ! –, c’était une vieille romance larmoyante et tendre, '''pareille''' à celles que les aveugles nasillent dans les rues.'' {{source|{{Citation/Octave Mirbeau/La Chanson de Carmen/1990}}}}
    |#* ''Elle s’approcha sans bruit de Charles IX, qui donnait à ses chiens des fragments de gâteaux coupés en portions '''pareilles'''.'' {{source|{{Citation/Alexandre Dumas/La Reine Margot/1886|I|VI}}}}
    |#* ''Toutes les fois que nous atteignons le sommet d'une côte, nous n'apercevons devant nous qu'une autre côte '''pareille''', couverte comme elle de palmiers nains, auxquels de maigres champs labourés essayent de disputer le terrain; […].'' {{source|{{w|Frédéric {{smcp|Weisgerber}}}}, ''Trois mois de campagne au Maroc : étude géographique de la région parcourue'', Paris : Ernest Leroux, 1904, p. 27}}
    |# [[tel|Tel]], de [[cette]] [[nature]], de cette [[espèce]].
    |#* ''Un '''pareil''' crime, si près du commissariat, en bordure du passage où le meurtrier risquait à son insu d’être surpris, se présentait, de prime abord, comme l’acte d’un fou, […].'' {{source|{{Citation/Francis Carco/L’Homme de minuit/1938}}}}
    |#* ''Sous le porche, un boueux attendait l'arrivée de la voiture en songeant que, par un temps '''pareil''', elle ne viendrait sans doute pas.'' {{source|{{Citation/Francis Carco/Brumes/1935|62}}}}
    |# {{par ext}} S’utilise pour désigner un [[accord]], voire une [[affirmation]].
    |#* ''Si l’on me le propose, '''pareil''', ce n’est pas une impossibilité.''""".stripMargin)
}
