/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource


object LettreDeChateau {

  val resTxt = """== {{langue|fr}} ==
                 |=== {{S|étymologie}} ===
                 |: {{ébauche-étym|fr}}
                 |
                 |=== {{S|nom|fr}} ===
                 |{{fr-rég|lɛtʁ də ʃɑ.to|p=lettres de château}}
                 |'''lettre de château''' {{pron|lɛtʁ də ʃɑ.to|fr}} {{f}}
                 |# [[lettre|Lettre]] de [[remerciement]]s qu’on envoie à une personne après [[séjourner|avoir séjourné]] chez elle.
                 |#* ''Le lendemain matin, malheureusement, il a fallu raccompagner le visiteur à la gare. Depuis lors il m'a téléphoné une fois, […], et déclaré qu'il s'étonnait de penser tant à moi. Ces mots faisaient une '''lettre de château''' un peu trop appuyée, ils m'ont précipité dans des abîmes de complaisante rêverie. Mais à présent plus de nouvelles.'' {{source|{{w|Renaud Camus}}, ''Les Nuits de l'âme : Journal 1996'', Fayard, 2001}}
                 |#* ''Il possède également une très bonne plume. Pour l'heure, elle ne lui sert qu'à rédiger d'un stylo précieux d'inimitables « '''lettres de château''' » qu'il fait porter par coursier dès le lundi après-midi lorsque le week-end a été chic et confortable.'' {{source|{{w|Philippe Bouvard}}, ''Des femmes'', Éditions Flammarion, 2014}}
                 |#* ''La carte de visite s'utilise pour remercier (invitation, cadeau), féliciter (fiançailles, mariage, naissance), accompagner un cadeau, exprimer des vœux. En aucun cas, elle ne remplace une lettre quand celle-ci s'impose (condoléances, '''lettre de château''').'' {{source|''Savoir-vivre > Correspondance > Usuelle > Quelques généralités d'usage'', sur le sire du ''Bottin mondain'' (www.bottin-mondain.fr/) , consulté le 2 août 2016}}
                 |#* ''La '''lettre de château''' doit vous ressembler. […]. Rien de tel en cela que d’adresser cette '''lettre de château''' manuscrite, preuve de l’importance que vous apportez à ce geste.'' {{source|''La lettre de château est-elle réservée aux châtelains ?'' sur ''Le Blog de la Belle École : Art de vivre à la française'' (www.labelleecole.fr), le 31 juillet 2012}}
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début|}}
                 |* {{T|en}} : {{trad+|en|bread-and-butter letter}}, {{trad+|en|bread-and-butter note}}
                 |{{trad-fin}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-lettre de château.wav}}
                 |
                 |[[Catégorie:Documents en français]]""".stripMargin

  val resource = Resource("lettre de château","https://fr.wiktionary.org/wiki/","entry",resTxt)

}
