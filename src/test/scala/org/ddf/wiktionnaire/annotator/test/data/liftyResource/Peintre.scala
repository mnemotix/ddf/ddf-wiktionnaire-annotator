/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Peintre {

  val peintre = """{{voir|Peintre}}
                  |
                  |== {{langue|fr}} ==
                  |=== {{S|étymologie}} ===
                  |: {{date|lang=fr|vers 1165}}{{R|TLFi}} Du {{étyl|la|fr|mot=pictor}}{{R|Littré}}, devenu, en latin populaire {{recons|lang-mot-vedette=fr|pinctor}}. Cognat de l’espagnol ''{{lien|pintor|es}}'', de l’italien ''{{lien|pittore|it}}''.
                  |: L’ancien français ''{{lien|peintor|fro}}'' dérive de l’accusatif latin {{recons|lang-mot-vedette=fr|pinctorem}}{{R|TLFi}}.
                  |
                  |==== {{S|attestations}} ====
                  |* {{siècle|XVII|lang=fr}} {{exemple|Il faut dire, cette femme eſt ''Poëte'', eſt ''Philoſophe'', eſt ''Medecin'', eſt ''Auteur'', eſt '''''Peintre''''' ; & non ''Poëteſſe'', ''Philoſopheſſe'', ''Medecine'', ''Autrice'', ''Peintreſſe'', ''&c''.
                  ||sens=Il faut dire, cette femme est ''poète'', est ''philosophe'', est ''médecin'', est ''auteur'', est '''''peintre''''' ; et non ''poétesse'', ''philosophesse'', ''médecine'', ''autrice'', ''peintresse'', ''etc''.
                  ||source={{ouvrage|auteur={{w|Nicolas Andry de Boisregard}}|titre=Reflexions ſur l’uſage préſent de la Langue Françoiſe ou Remarques Nouvelles & Critiques touchant la politeſſe du Langage|éditeur={{w|Laurent d'Houry}}|date=1692|passage=163-164|année première édition=1689|url={{Citation/Books.Google|l2tubIwXURgC|PA163}}}}
                  ||tête=*
                  ||lang=fr
                  |}}
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég|pɛ̃tʁ|mf=oui}}
                  |[[Image:Adélaïde Labille-Guiard - Self-Portrait with Two Pupils - The Metropolitan Museum of Art.jpg|vignette|redresse|Une '''peintre''' se dépeignant (sens 1).]]
                  |'''peintre''' {{pron|pɛ̃tʁ|fr}} {{mf}} {{équiv-pour|lang=fr|une femme|peintre|peintresse}}
                  |# [[artiste|Artiste]] utilisant la [[peinture]] pour son art.
                  |#* ''Hazlitt avait d’abord été '''peintre''', mais il peignait si mal qu’il finit par s’en apercevoir, et il se mit à écrire.'' {{source|{{w|Julien Green}}, ''Charles Lamb'', dans ''Suite anglaise'', 1927, Le Livre de Poche, page 68}}
                  |#* ''Outre l’ascension du '''peintre''' refoulé devenu soldat fanatique puis sauveur vénéré d’une Allemagne en perdition, le musée abordait l’Holocauste et ses millions de victimes, photographies sans concession à l’appui, montrant comment la folie d’une poignée d’individus ultradéterminés avait mené le pays dans l’horreur.'' {{source|Gwenael Le Guellec, ''Exil pour l'enfer'', 2021}}
                  |#* ''La jeune Artemisia Gentileschi est une '''peintre''' remarquablement douée, qui dépasse rapidement la dextérité de son père, Ortazio Gentileschi''. {{source|Franko-Romanisten-Verband. Kongress, ‎Roswitha Böhm, ‎Andrea Grewe, Margarete Zimmermann, ''Siècle classique et cinéma contemporain'', 2009}}
                  |# [[artisan|Artisan]] dont le [[métier]] est de [[couvrir]] de [[couleur]] des [[muraille]]s, des [[lambris]], des [[plafond]]s, etc.
                  |#* ''Un '''peintre''' en bâtiments.''
                  |# {{figuré|fr}} Celui qui représente [[fidèlement]] les choses dont il parle, dont il [[traiter|traite]], soit en [[prose]], soit en [[vers]].
                  |#* ''Cet écrivain est un excellent '''peintre'''.''
                  |#* ''Molière est un grand '''peintre''' des vices et des travers de l’humanité.''
                  |
                  |==== {{S|notes}} ====
                  |: La forme féminine ''{{lien|peintresse|fr}}'' existait jadis, elle est plus rare aujourd'hui.
                  |
                  |==== {{S|synonymes}} ====
                  |''Art'' :
                  |* {{péjoratif|nocat=1}} [[barbouilleur]], [[gribouilleur]].
                  |* [[artiste peintre]]
                  |''Bâtiment'' :
                  |* [[peintre en bâtiment]]
                  |
                  |==== {{S|quasi-synonymes}} ====
                  |* [[peintresse]]
                  |* [[peintureur]]
                  |* [[peintureuse]]
                  |
                  |==== {{S|apparentés}} ====
                  |* [[peindre]]
                  |* [[peinture]]
                  |* [[pittoresque]]
                  |
                  |==== {{S|hyperonymes}} ====
                  |* [[artiste]]
                  |* [[artisan]]
                  |
                  |==== {{S|hyponymes}} ====
                  |{{(}}
                  |* [[miniaturiste]]
                  |* [[maniériste]]
                  |* [[paysagiste]]
                  |* [[enlumineur]]
                  |* [[portraitiste]]
                  |* [[ruiniste]]
                  |* [[zographos]]
                  |{{)}}
                  |
                  |==== {{S|dérivés}} ====
                  |* [[désespoir du peintre]]
                  |* [[peintre officiel de la Marine]]
                  |* [[peintre en lettres]]
                  |* [[peintre en bâtiment]]
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début}}
                  |* {{T|sq}} : {{trad+|sq|piktor}}
                  |* {{T|de}} : {{trad+|de|Maler|m}}
                  |* {{T|en}} : {{trad+|en|painter}}
                  |* {{T|ar}} : {{trad-|ar|رسام|tr=rasām|m}}
                  |* {{T|hy}} : {{trad+|hy|նկարիչ|tr=nkaričʿ|m}}
                  |* {{T|az}} : {{trad+|az|rəssam}}
                  |* {{T|ba}} : {{trad|ba|рәссам|tr=1}}
                  |* {{T|eu}} : {{trad-|eu|margolari}}
                  |* {{T|br}} : 1. {{trad-|br|livour|m}}, {{trad-|br|livourez|f}}, {{trad-|br|penter|m}} ; 2. {{trad+|br|liver|m}}
                  |* {{T|bg}} : {{trad+|bg|художник|tr=houdojnik}}
                  |* {{T|zh}} : {{trad+|zh|画家|R=huàjiā}}, {{trad+|zh|画师|R=huàshī}} (1) ; {{trad-|zh|油漆工|R=yóuqīgōng}} (2)
                  |* {{T|ko}} : {{trad+|ko|화가|tr=hwaga}}
                  |* {{T|co}} : {{trad-|co|pittore}}
                  |* {{T|hr}} : {{trad+|hr|slikar}}
                  |* {{T|da}} : {{trad+|da|maler}}, {{trad+|da|maler}}
                  |* {{T|es}} : {{trad+|es|pintor|m}}
                  |* {{T|eo}} : {{trad-|eo|farbisto}} (2) ; {{trad-|eo|pentristo}} (1)
                  |* {{T|et}} : {{trad-|et|maaler}}, {{trad-|et|maalija}}
                  |* {{T|fi}} : {{trad+|fi|maalari}}, {{trad+|fi|taidemaalari}}
                  |* {{T|frp}} : {{trad|frp|peintro|m}}, {{trad|frp|peintressi|f}}, {{trad|frp|pictor|m}}, {{trad|frp|pictrici|f}}
                  |* {{T|fy}} : {{trad-|fy|skilder}}, {{trad-|fy|ferver}}
                  |* {{T|gl}} : {{trad+|gl|pintor}}
                  |* {{T|ka}} : {{trad-|ka|მხატვარი|R=mxatvari}} (1), {{trad-|ka|მღებავი|tr=mḡebavi}} (2)
                  |* {{T|el}} : {{trad+|el|ζωγράφος|tr=zográfos}}
                  |* {{T|grc}} : {{trad|grc|γραφεύς|tr=grapheus}}
                  |* {{T|he}} : {{trad+|he|צבע}}
                  |* {{T|hu}} : {{trad+|hu|festő}}
                  |* {{T|io}} : {{trad+|io|piktisto}}
                  |* {{T|is}} : {{trad+|is|málari}}
                  |* {{T|it}} : {{trad+|it|pittore|m}}, {{trad+|it|pittrice|f}}
                  |* {{T|ja}} : {{trad+|ja|画家|R=がか, gaka}} (1) ; {{trad-|ja|ペンキ屋|R=ぺんきや, penkiya}} (2)
                  |* {{T|krc}} : {{trad|krc|суратчы|tr=1}}
                  |* {{T|kk}} : {{trad-|kk|суретші|tr=süwretşi (1)}}, {{trad-|kk|бояушы|tr=boyawşı (2)}}, {{trad-|kk|сырлаушы|tr=sırlawşı (2)}}, {{trad-|kk|сыршы|tr=sırşı (2)}}
                  |* {{T|ky}} : {{trad+|ky|сүрөтчү|tr=1}}
                  |* {{T|kum}} : {{trad|kum|суратчы|tr=1}}
                  |* {{T|ku}} : {{trad+|ku|nîgarvan}}
                  |* {{T|lv}} : {{trad-|lv|gleznotājs|m}}, {{trad-|lv|krāsotājs}}
                  |* {{T|lt}} : {{trad-|lt|dažytojas}}, {{trad-|lt|lakuotojas}}, {{trad-|lt|tapytojas}}
                  |* {{T|mk}} : {{trad-|mk|сликар|tr=slikar|m}}
                  |* {{T|ms}} : {{trad-|ms|pelukis}}
                  |* {{T|crg}} : {{trad|crg|payncheureur}}
                  |* {{T|nl}} : {{trad+|nl|schilder|m}} ; {{trad+|nl|kunstschilder|m}} ; {{trad+|nl|huisschilder|m}} ; {{trad+|nl|verver}}
                  |* {{T|nog}} : {{trad|nog|суьвретши|tr=1}}
                  |* {{T|nb}} : {{trad+|nb|maler}} ''(1 : artiste)'', {{trad+|nb|maler}} ''(2 : artisan)''
                  |* {{T|nn}} : {{trad-|no|målar}} ''(1 : artiste)'', {{trad-|no|målar}} ''(2 : artisan)''
                  |* {{T|oc}} : {{trad+|oc|pintre|m}}, {{trad+|oc|pintor|m}}
                  |* {{T|pap}} : {{trad|pap|pintor}} ; {{trad|pap|ferfdó}}
                  |* {{T|fa}} : {{trad+|fa|نقاش}}, {{trad+|fa|نگارگر}}
                  |* {{T|pl}} : {{trad+|pl|malarz|m}}
                  |* {{T|pt}} : {{trad+|pt|pintor|m}}
                  |* {{T|ro}} : {{trad+|ro|pictor|m}}
                  |* {{T|ru}} : {{trad+|ru|художник|tr=houdajnik}}, {{trad+|ru|живописец}}
                  |* {{T|se}} : {{trad|se|govvačeahppi}} (1)
                  |* {{T|sr}} : {{trad+|sr|сликар|tr=slikar}}
                  |* {{T|sk}} : {{trad+|sk|maliar|m}}
                  |* {{T|sl}} : {{trad+|sl|slikar|m}}
                  |* {{T|sv}} : {{trad+|sv|målare|m}} ; {{trad-|sv|skildrare}}
                  |* {{T|crh}} : {{trad|crh|ressam|tr=1}}
                  |* {{T|tt}} : {{trad+|tt|рәссам|tr=1}}
                  |* {{T|cs}} : {{trad+|cs|malíř|m}}
                  |* {{T|tyv}} : {{trad|tyv|чурукчу|tr=1}}
                  |* {{T|tr}} : {{trad+|tr|ressam}}
                  |* {{T|tk}} : {{trad-|tk|suratçy|tr=1}}
                  |* {{T|uk}} : {{trad-|uk|художник|tr=houdojnik|m}}
                  |{{trad-fin}}
                  |
                  |=== {{S|prononciation}} ===
                  |* {{pron|pɛ̃tʁ|fr}}
                  |** {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|pɛ̃tʁ|audio=Fr-peintre.ogg}}
                  |* Français méridional : {{pron|ˈpɛn.tʁə|fr}}
                  |* Canada : {{pron|pẽtʁ|fr}}
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{WP}}
                  |* {{Wikiquote|Catégorie:Femme peintre‎|lang=fr}}
                  |
                  |=== {{S|références}} ===
                  |* {{R:DAF8}}
                  |* {{R:Littré}}
                  |* {{R:TLFi}}
                  |
                  |[[Catégorie:Artistes en français]]
                  |[[Catégorie:Métiers du secteur secondaire en français]]
                  |""".stripMargin
}