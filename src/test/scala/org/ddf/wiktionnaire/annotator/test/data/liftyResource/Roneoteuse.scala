/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Roneoteuse {
  def roneoteuse = {
    """== {{langue|fr}} ==
      |=== {{S|étymologie}} ===
      |: {{date||lang=fr}} {{composé de|ronéoter|-euse|lang=fr|m=1}}.
      |
      |=== {{S|nom|fr}} ===
      |{{fr-rég|ʁɔ.ne.ɔ.tøz}}
      |'''ronéoteuse''' {{pron|ʁɔ.ne.ɔ.tøz|fr}} {{f}}
      |[[File:Machine à polycopier à alcool (4).JPG|thumb|Une '''ronéoteuse'''. (sens 1)]]
      |# {{vieilli|fr}} {{familier|fr}} [[machine|Machine]] à [[ronéoter]], [[ronéo]], [[polycopieur]] à alcool.
      |#* {{exemple|L’industrie privée aidant l’école publique, la dame riche tendant la main à la pauvresse, voilà qui me semblait plutôt moral. Mais la charité a ses limites ; je tirais l’essentiel de mes textes au lycée sur une '''ronéoteuse''', machine mérovingienne carburant à l’alcool, actionnée à la main, qui livrait une soixantaine de copies puantes, baveuses au début, puis trop pâles, également illisibles.
      ||source=Michel Volkovitch, « Feuilles volantes » in ''Mes écoles'', 2008
      ||lang=fr
      |}}
      |# {{vieilli|fr}} Celle qui [[ronéoter|ronéote]], qui fait [[fonctionner]] la [[ronéo]] {{équiv-pour|un homme|ronéoteur|lang=fr}}.
      |#* {{exemple|J’y étais entrée comme '''ronéoteuse''', autant dire tout en bas de l’échelle des salaires, et j’y avais retrouvé par pur hasard Emeric Deutsch, Juif hongrois réfugié qui venait manger chez ma mère le dimanche, après la guerre, et que j’avais depuis perdu de vue.
      || source = {{ouvrage|auteurs={{w|Marceline Loridan-Ivens}}, ‎{{w|Judith Perrignon}}|titre=L’amour après|éditeur=Grasset|date=2018|passage=6|url={{Citation/Books.Google|mK9CDwAAQBAJ|PT6|surligne=ronéoteuse}}}}
      || lang = fr
      |}}
      |
      |==== {{S|traductions}} ====
      |{{trad-début}}
      |{{ébauche-trad}}
      |{{trad-fin}}
      |
      |=== {{S|prononciation}} ===
      |* {{pron-rimes|ʁɔ.ne.ɔ.tøz|fr}}
      |* {{écouter|Cesseras (France)||lang=fr|audio=LL-Q150 (fra)-Guilhelma-ronéoteuse.wav}}
      |
      |=== {{S|références}} ===
      |* {{R:Cordial}}
      |
      |[[Catégorie:Machines en français]]
      |[[Catégorie:Métiers du secteur secondaire en français]]
      |[[Catégorie:Noms de métiers féminisés en français]]""".stripMargin
  }
}
