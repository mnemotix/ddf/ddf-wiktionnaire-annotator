/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextSomme {

  val somme = """== {{langue|fr}} ==
                |=== {{S|étymologie}} ===
                |: ''([[#fr-nom-1|Nom 1]])'' Du {{étyl|la|fr|mot=summa|sens= point le plus élevé}}, abréviation de ''summa linea'' (« ligne du haut »), car les Romains faisaient leurs additions en notant le résultat en haut, d'où le sens actuel.
                |: ''([[#fr-nom-2|Nom 2]])'' Du {{étyl|la|fr|mot=somnus}} (« [[sommeil]] ») qui donne l’{{étyl|fro|fr|som}} et dont sont dérivé ''[[somnambule]], [[somnifère]]'', etc.
                |: ''([[#fr-nom-3|Nom 3]])'' Du {{étyl|la|fr|mot=sagma}} qui donne l’{{étyl|fro|fr|some}}, l’occitan ''{{lien|sauma|oc}}''.
                |
                |=== {{S|nom|fr|num=1}} ===
                |{{fr-rég|sɔm}}
                |'''somme''' {{pron|sɔm|fr}} {{f}}
                |# {{lexique|mathématiques|fr}} [[résultat|Résultat]] de l’[[addition]] de [[plusieurs]] [[nombre]]s.
                |#* ''Cinq, la '''somme''' de deux et trois.''
                |#* ''Si l’on additionne tous ces chiffres, on arrive à une '''somme''' importante.''
                |# {{par ext}} [[quantité|Quantité]] d’[[argent]].
                |#* ''Avignon, Carpentras, Cavaillon, regorgent, pour la malédiction des familles, de tripots où de grosses '''sommes''' sont à chaque instant risquées.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
                |#* ''Le double objectif de la tontine est de permettre au membre de libérer sa cotisation (comme s'il faisait une épargne) et de pouvoir bénéficier d'une importante '''somme''' en vue de réaliser un investissement ou une dépense.'' {{source|Célestin Murengezi, ''Impacts du crédit dans la promotion des PME'', page 61, 2008}}
                |#* ''Jolie '''somme''' ! Mais restait à la toucher... Or, ici, commence un procès aux complications inénarrables, et qui dura dix ans.'' {{source|Henry Guy, ''Histoire de la poésie française au XVI{{e}} siècle'', 1910, vol.1, p.22.}}
                |#* ''Andréa fouillait das sa coiffeuse. Elle l'avait trouvée dans une salle des ventes à Saumur et arrachée de haute lutte à une équipe de brocanteurs rapaces pour la '''somme''' de trois cent cinquante francs.'' {{source|Patrick Cauvin, ''Haute-Pierre'', éditions Albin Michel, 1985}}
                |# {{figuré|fr}} [[ensemble|Ensemble]], [[totalité]].
                |#* ''Cela ne peut qu’ajouter à la '''somme''' de nos maux.''
                |# [[ensemble|Ensemble]] de [[livre]]s qui traitent en [[abrégé]] de toutes les [[partie]]s d’une [[science]], d’une [[doctrine]], etc.
                |#* ''Le premier, Abélard avait eu l’idée de composer une '''somme''' théologique suivant la méthode des Dialecticiens, […] ; mais le traité qu’il écrivit tout d’abord, […], comprend seulement l’étude de Dieu et de la Trinité.'' {{source|Louis Rougier, ''Histoire d’une faillite philosophique : la Scolastique'', 1925, éd. 1966}}
                |#* ''«Par pur hasard du calendrier», balaie le sociologue des religions Jean-Louis Schlegel, auteur d’une '''somme''' sur cette question,[[…]] .'' {{source|Samuel {{smcp|Lieven}}, ''Les «chrétiens de gauche» se cherchent un avenir - Journal La Croix, page 17, 17 mars 2015}}
                |# [[Fichier:Vemmenhög vapen.svg|vignette|120px|Armoiries avec 3 '''sommes''' ''(sens héraldique)'']] {{meubles héraldiques|fr}} {{rare|fr}} [[meuble|Meuble]] représentant un disque de couleur verte dans les [[armoiries]]. Il s’agit d’ancien nom utilisé pour le tourteau [[de sinople]]. À rapprocher de [[besant]], [[besant-tourteau]], [[buse]], [[gulpe]], [[guse]], [[heurte]], [[œil de faucon]], [[ogoesse]], [[plate]], [[pomme]], [[tourteau]], [[tourteau-besant]] et [[volet]].
                |#* ''D’argent à trois tourteaux de sinople ('''sommes''') mal-ordonnés, qui est du Hundred Vemmenhög en Suède'' {{cf|lang=fr}} illustration « armoiries avec 3 sommes »
                |
                |==== {{S|synonymes}} ====
                |''Héraldique'' :
                |* {{lien|pomme|fr|héraldique}}
                |* {{lien|volet|fr|héraldique}}
                |
                |==== {{S|dérivés}} ====
                |* [[en somme]]
                |* [[somme toute]]
                |* [[sommer]]
                |
                |==== {{S|apparentés}} ====
                |{{(}}
                |* [[sommaire]]
                |* [[sommairement]]
                |* [[sommet]]
                |* [[sommiste]]
                |* [[sommité]]
                |* [[summa divisio]]
                |{{)}}
                |
                |==== {{S|traductions}} ====
                |{{trad-début}}
                |* {{T|af}} : {{trad-|af|bedrag}}
                |* {{T|de}} : {{trad+|de|Summe}}, {{trad+|de|Betrag}}
                |* {{T|en}} : {{trad+|en|sum}}, {{trad+|en|amount}}, {{trad+|en|aggregate}}
                |* {{T|ar}} : {{trad-|ar|مَجْموع}}
                |* {{T|br}} : {{trad+|br|hollad}}
                |* {{T|ca}} : {{trad+|ca|suma}}, {{trad+|ca|summa}}
                |* {{T|ko}} : {{trad+|ko|합|R=hap}} (1), {{trad-|ko|합계|R=hapgye}}
                |* {{T|da}} : {{trad+|da|beløb}}, {{trad-|da|sum}}
                |* {{T|es}} : {{trad+|es|suma}}
                |* {{T|eo}} : {{trad-|eo|sumo}}
                |* {{T|fo}} : {{trad-|fo|samløga}}, {{trad-|fo|upphædd}}
                |* {{T|fi}} : {{trad+|fi|summa}}
                |* {{T|el}} : {{trad+|el|άθροισμα}}
                |* {{T|hu}} : {{trad+|hu|összeg}}
                |* {{T|io}} : {{trad+|io|sumo}}
                |* {{T|ia}} : {{trad-|ia|summa}}, {{trad-|ia|amonta}}
                |* {{T|it}} : {{trad+|it|somma}}, {{trad+|it|importo}}
                |* {{T|ja}} : {{trad+|ja|和|R=wa}} (1), {{trad+|ja|合計|R=gōkei}}
                |* {{T|kk}} : {{trad+|kk|қосынды|tr=qosındı (1)}}
                |* {{T|ms}} : {{trad-|ms|jumlah}}
                |* {{T|nl}} : {{trad+|nl|bedrag}}, {{trad+|nl|som}}, {{trad+|nl|somma}}, {{trad-|nl|summa}}, {{trad+|nl|totaal}}, {{trad+|nl|totaalbedrag}}, {{trad-|nl|totaalcijfer}}
                |* {{T|no}} : {{trad+|no|beløp}}
                |* {{T|oc}} : {{trad-|oc|soma|f}}
                |* {{T|fa}} : {{trad+|fa|مجموع}}
                |* {{T|pl}} : {{trad+|pl|suma}}, {{trad+|pl|ilość}}
                |* {{T|pt}} : {{trad+|pt|soma}}, {{trad+|pt|importância}}, {{trad+|pt|quantia}}, {{trad+|pt|total}}
                |* {{T|ro}} : {{trad+|ro|sumă|f}}, {{trad+|ro|total|n}}
                |* {{T|ru}} : {{trad+|ru|сумма|R=summa}}
                |* {{T|sv}} : {{trad+|sv|belopp}}, {{trad+|sv|summa}}
                |* {{T|sw}} : {{trad+|sw|hesabu}}
                |* {{T|tg}} : {{trad-|tg|ҳосили ҷамъ}} (1)
                |* {{T|cs}} : {{trad+|cs|suma}}
                |{{trad-fin}}
                |
                |=== {{S|nom|fr|num=2}} ===
                |{{fr-rég|sɔm}}
                |'''somme''' {{pron|sɔm|fr}} {{m}}
                |# [[temps|Temps]] de [[sommeil]].
                |#* ''Ma sieste habituelle n’avait jamais dépassé vingt-cinq minutes. Je me sentis donc suffisamment rassuré, et je m’arrangeai tout de suite pour faire mon '''somme'''.'' {{source|{{w|Edgar Poe}}, ''L’Ange du bizarre'', dans ''Histoires grotesques et sérieuses'', traduction de {{w|Charles Baudelaire}}}}
                |#* ''Or, le matin, jour de l’élection, Laugu du Moulin et Abel le Rat, après un petit '''somme''' pour cuver les cuites de la semaine, s’étant levés et de concert.'' {{source|{{w|Louis Pergaud}}, ''[[s:Deux Électeurs sérieux|Deux Électeurs sérieux]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                |
                |==== {{S|dérivés}} ====
                |* [[microsomme]]
                |
                |==== {{S|apparentés}} ====
                |{{(}}
                |* [[assommer]]
                |* [[sommeil]]
                |* [[sommeiller]]
                |* [[somnambule]]
                |* [[somnambulisme]]
                |* [[somnoler]]
                |{{)}}
                |
                |==== {{S|hyponymes}} ====
                |* [[microsomme]]
                |* [[sieste]]
                |
                |==== {{S|traductions}} ====
                |{{trad-début}}
                |* {{T|br}} : {{trad+|br|kousk|m}}, {{trad+|br|kouskadenn|f}}, {{trad+|br|morenn|f}}
                |* {{T|oc}} : {{trad+|oc|sòm|m}}
                |{{trad-fin}}
                |
                |=== {{S|nom|fr|num=3}} ===
                |{{fr-rég|sɔm}}
                |'''somme''' {{pron|sɔm|fr}} {{f}}
                |# [[bât#fr|Bât]].
                |#*''[[bête de somme#fr|Bête de '''somme''']].''
                |
                |==== {{S|apparentés}} ====
                |* {{lien|sommier|fr}}
                |
                |=== {{S|verbe|fr|flexion}} ===
                |{{fr-verbe-flexion|sommer|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
                |'''somme''' {{pron|sɔm|fr}}
                |# ''Première personne du singulier du présent de l’indicatif de'' [[sommer]].
                |# ''Troisième personne du singulier du présent de l’indicatif de'' [[sommer]].
                |# ''Première personne du singulier du présent du subjonctif de'' [[sommer]].
                |# ''Troisième personne du singulier du présent du subjonctif de'' [[sommer]].
                |# ''Deuxième personne du singulier de l’impératif de'' [[sommer]].
                |
                |=== {{S|prononciation}} ===
                |* {{écouter|lang=fr|France|titre=une somme|yn.sɔm|audio=Fr-somme.ogg}}
                |* {{écouter|Paris (France)|sɔm|lang=fr|audio=Fr-Paris--somme.ogg}}
                |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Skimel-somme.wav}}
                |* {{écouter|lang=fr|France (Canet)|audio=LL-Q150 (fra)-Ives (Guilhelma)-somme.wav}}
                |* {{écouter|lang=fr|France (Massy)|audio=LL-Q150 (fra)-X-Javier-somme.wav}}
                |* {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Fhala.K-somme.wav}}
                |* {{écouter|||lang=fr|audio=LL-Q150 (fra)-Guilhelma-somme.wav}}
                |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-LurKin-somme.wav}}
                |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-somme.wav}}
                |* {{écouter|lang=fr|Thaïlande|audio=LL-Q150 (fra)-Saubry-somme.wav}}
                |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-somme.wav}}
                |
                |==== {{S|homophones|fr}} ====
                |* [[Somme]]
                |
                |=== {{S|voir aussi}} ===
                |* {{Thésaurus|fr|héraldique}}
                |* {{WP|Liste des meubles héraldiques}}
                |
                |=== {{S|références}} ===
                |* {{R:DAF8}}
                |* {{R:TLFi}}
                |
                |[[Catégorie:Noms multigenres en français]]""".stripMargin

}
