/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource


object Bouteille {

  val resource = Resource("bouteille", "https://fr.wiktionary.org/wiki/","entry",
    """{{voir|bouteillé}}
      |== {{langue|fr}} ==
      |=== {{S|étymologie}} ===
      |: {{siècle|XII}} De l’{{étyl|fro|fr|mot=botele|sens=récipient}}, du {{étyl|bas latin|fr|mot=buticula}} diminutif de {{lien|''buttis''|sens=sorte de vase|la}} et attesté en latin médiéval au sens de « [[tonneau]] », « [[outre]] ».
      |
      |=== {{S|nom|fr}} ===
      |{{fr-rég|bu.tɛj}}
      |[[Image:Bouteille.jpg|vignette|'''Bouteille''' (1) de vin, de couleur vert '''bouteille''' (4).]]
      |'''bouteille''' {{pron|bu.tɛj|fr}} {{f}}
      |# [[récipient|Récipient]] à [[goulot]] [[étroit]], [[destiné]] à [[contenir]] un [[liquide]]; le plus [[souvent]] [[faire|fait]] de [[verre]] ou de [[plastique]] [[translucide]].
      |#* ''– Bon, il y a de la boustifaille ! <br/>[…], il entra et se mit en devoir d’explorer l’établissement. Il y découvrit plusieurs flacons de lait stérilisé, des '''bouteilles''' d’eau minérale, deux énormes boites de biscuits, un grand bocal de gâteaux éventés, […] pour plus de cinquante personnes.'' {{source|{{Citation/H. G. Wells/La Guerre dans les airs/1921|327}}}}
      |#* ''Ils s’attablent dans de bons restaurants, se font déboucher des '''bouteilles''' de Châteauneuf-du-Pape, dégustent des liqueurs.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
      |#* ''Une demi-heure plus tard, après avoir, suivant la tradition, placé dans une anfractuosité une '''bouteille''' cachetée contenant un document commémoratif que je leur passai, Moussard et Le François purent sauter de nouveau dans la baleinière […].'' {{source|{{w|Jean-Baptiste Charcot}}, ''Dans la mer du Groenland'', 1928}}
      |#* ''Mon lieutenant, faites-moi servir une '''bouteille''' de pommard… Du meilleur…'' {{source|{{Citation/Victor Méric/Les Compagnons de l’Escopette/1930|15}}}}
      |#* ''Devant une table, encombrée par un chanteau de pain, une demi-meule de gruyère et une innombrable quantité de '''bouteilles''' vides, les deux amis qu’on croyait morts devisaient paisiblement comme des sages.'' {{source|{{w|Louis Pergaud}}, ''[[s:La Disparition mystérieuse|La Disparition mystérieuse]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
      |# {{méton|fr}} Contenu d’une '''bouteille''' au sens premier.
      |#* ''Deux ou trois '''bouteilles''' d’eau suffiront pour arroser les jardinières.''
      |#* ''Une '''bouteille''' de bière, de vin, d’eau-de-vie, de rhum.''
      |#* {{vieilli|fr}} ''Une '''bouteille''' d’encre,'' dans ce sens, il signifie toujours d’une '''bouteille''' de vin.
      |#* ''Boire, vider une '''bouteille'''.''
      |#* ''Les pulsations des tempes s’alourdissaient. Des billes lui grouillaient dans la tête.<br>– J’ai soif. Que dirais-tu d’une '''bouteille''' de supérieur?<br>– Je t’en dirais le goût.'' {{source|{{w|Jules Romains}}, ''Les Copains'', page 123, 1922}}''
      |# Récipient [[rigide]] servant à [[stocker]] un [[gaz]] sous [[pression]].
      |#* ''Le plongeur s’équipe de ses '''bouteilles''' d’air comprimé.''
      |# Couleur [[vert]] [[sombre]], [[habituellement]] celle du [[verre]] d’une '''bouteille''' (sens 1). {{couleur|#096A09}}
      |#* ''C’était un sous-sol étranglé, écrasé, mal éclairé par un vitrage d’une transparence grossière et couleur '''bouteille'''.'' {{source|Maurice Talmeyr, ''Histoires joyeuses et funébres'', page 12, 1886, M. de Brunhoff}}
      |# {{lexique|histoire|marine|fr}} [[saillie|Saillie]] en [[charpente]], sur les [[coté]]s [[arrière]]s du [[navire]], et qui servaient de [[latrine]]s.{{réf|2}}
      |# {{lexique|botanique|fr}} {{vieilli|fr}} [[variété|Variété]] de [[courge]].{{réf|2}}
      |
      |==== {{S|variantes}} ====
      |* [[teille#Nom commun 2|teille]] ''(par [[aphérèse]])''
      |
      |==== {{S|synonymes}} ====
      |;Récipient à goulot, destiné à contenir un liquide
      |:[[champenoise]], [[quille]], [[téteille]]
      |;Son contenu
      |:[[champenoise]]
      |
      |==== {{S|dérivés}} ====
      |{{(}}
      |* [[aimer la bouteille]] : {{familier|nocat=1}} s’adonner à la boisson
      |* [[avoir de la bouteille]]
      |* [[bouteille à encre]]
      |* [[bouteille à la mer]]
      |* [[bouteille à l’encre]] : affaire obscure, embrouillée
      |* [[bouteille carrée]]
      |* [[bouteille consignée]]
      |* [[bouteille de Klein]]
      |* [[bouteille de Leyde]]
      |* [[bouteille de plongée]]
      |* [[caresser la bouteille]]
      |* [[casse-bouteille]]
      |* [[cul-de-bouteille]]
      |* [[demi-bouteille]]
      |* [[dessous-de-bouteille]]
      |* [[dive bouteille]]
      |* [[jeter une bouteille à la mer]] (appeler à l’aide avec l’espoir d’être entendu)
      |* [[la règle de bouteille]] (le résultat est toujours 2/3)
      |* [[laisser sa raison au fond de la bouteille]] (s’enivrer)
      |* [[faire le niveau à la bouteille]] (maçonner approximativement)
      |* [[mettre en bouteille]]
      |* [[mise en bouteille]]
      |* [[n’avoir rien vu que par le trou d’une bouteille]] (n’avoir aucune connaissance des choses du monde, aucun usage du monde)
      |* [[ouvre-bouteille]]
      |* [[porte-bouteille]]
      |* [[prendre de la bouteille]]
      |* [[rince-bouteille]]
      |* [[vert bouteille]]
      |* [[vide-bouteille]]
      |{{)}}
      |
      |==== {{S|apparentés}} ====
      |{{(}}
      |* [[bouteiller]]
      |* [[désembouteiller]]
      |* [[embouteillage]]
      |* [[embouteillé]]
      |* [[embouteiller]]
      |* [[embouteilleur]]
      |* [[embouteilleuse]]
      |* [[réembouteiller]]
      |* [[rembouteiller]]
      |{{)}}
      |
      |==== {{S|phrases}} ====
      |* [[avec des si on mettrait Paris en bouteille]] ''(avec des hypothèses, tout devient possible)''
      |
      |==== {{S|vocabulaire}} ====
      |* {{voir thésaurus|fr|bouteille}}
      |* [[lagéniforme]]
      |* [[boîte boisson]]
      |* [[brique]]
      |
      |* {{voir thésaurus|fr|bière (boisson)}}
      |
      |==== {{S|hyperonymes}} ====
      |;Récipient à goulot
      |:[[contenant]]
      |
      |==== {{S|hyponymes}} ====
      |{{(|Récipient à goulot}}
      |* [[clavelin]]
      |* [[demi-clavelin]]
      |* [[double magnum]]
      |* [[fiole]]
      |* [[flacon]]
      |* [[flasque]]
      |* [[mignonnette]]
      |{{)}}
      |{{Palette Contenance des bouteilles de vin}}
      |
      |==== {{S|méronymes}} ====
      |Du haut vers le bas de la '''bouteille''' :
      |* [[col]]
      |* [[cou]]
      |* [[ventre]], ou [[panse]]
      |* [[cul]]
      |
      |==== {{S|traductions}} ====
      |{{trad-début|Récipient destiné à contenir un liquide}}
      |* {{T|af}} : {{trad+|af|bottel}}
      |* {{T|sq}} : {{trad+|sq|shishe}}
      |* {{T|de}} : {{trad+|de|Flasche|f}}
      |* {{T|en}} : {{trad+|en|bottle}}
      |* {{T|ang}} : {{trad-|ang|flasce}}, {{trad-|ang|flaxe}}
      |* {{T|ar}} : {{trad-|ar|زُجَاجَة|f|tr=zujāja}}, {{trad+|ar|قِنِّينَة|f|tr=qinnīna}}
      |* {{T|hy}} : {{trad+|hy|շիշ|tr=šiš}}
      |* {{T|az}} : {{trad+|az|şüşə}}
      |* {{T|bm}} : {{trad--|bm|buteli}}
      |* {{T|eu}} : {{trad+|eu|botila}}
      |* {{T|bar}} : {{trad--|bar|Floschn|f}}
      |* {{T|bn}} : {{trad-|bn|বোতল|tr=botol}}
      |* {{T|be}} : {{trad-|be|бутэлька|tr=butélka}}
      |* {{T|my}} : {{trad+|my|ပုလင်း|tr=pu.lang:}}
      |* {{T|br}} : {{trad+|br|boutailh}}
      |* {{T|bg}} : {{trad-|bg|бутилка}}
      |* {{T|ca}} : {{trad+|ca|ampolla}}, {{trad+|ca|botella}}
      |* {{T|zh}} : {{trad+|zh|瓶|tr=píng}}, {{trad+|zh|瓶子|tr=píngzi}}
      |* {{T|ko}} : {{trad+|ko|병|tr=byeong|tradi=甁}}
      |* {{T|kw}} : {{trad-|kw|bottel}}
      |* {{T|co}} : {{trad-|co|buttiglia}}
      |* {{T|hr}} : {{trad+|hr|boca}}, {{trad-|hr|flaša}}
      |* {{T|da}} : {{trad+|da|flaske}}
      |* {{T|es}} : {{trad+|es|botella|f}}
      |* {{T|eo}} : {{trad-|eo|botelo}}
      |* {{T|et}} : {{trad-|et|pudel}}
      |* {{T|fo}} : {{trad-|fo|fløska}}
      |* {{T|fi}} : {{trad+|fi|pullo}}; {{trad+|fi|puteli}}
      |* {{T|fy}} : {{trad-|fy|flesse}}
      |* {{T|gd}} : {{trad-|gd|botul}}
      |* {{T|ga}} : {{trad+|ga|buidéal}}
      |* {{T|gl}} : {{trad-|gl|botella}}, {{trad+|gl|garrafa}}
      |* {{T|gallo}} : {{trad--|gallo|bótaylh}}
      |* {{T|cy}} : {{trad+|cy|potel}}
      |* {{T|ka}} : {{trad+|ka|ბოთლი|tr=bot’li}}
      |* {{T|el}} : {{trad+|el|φιάλη}}, {{trad+|el|μπουκάλι}}
      |* {{T|ha}} : {{trad-|ha|kwalba}}
      |* {{T|mey}} : {{trad--|mey|buuŝ}}
      |* {{T|he}} : {{trad+|he|בקבוק|m|tr=bakbúk}}
      |* {{T|hi}} : {{trad+|hi|शीशी|tr=śīśī}}, {{trad+|hi|बोतल|tr=botal}}
      |* {{T|mww}} : {{trad--|mww|hwj}}
      |* {{T|hu}} : {{trad+|hu|flaska}}, {{trad+|hu|üveg}}
      |* {{T|io}} : {{trad+|io|botelo}}
      |* {{T|id}} : {{trad+|id|botol}}
      |* {{T|is}} : {{trad+|is|flaska}}
      |* {{T|it}} : {{trad+|it|bottiglia}}
      |* {{T|ja}} : {{trad+|ja|瓶|tr=bin}}
      |* {{T|kk}} : {{trad+|kk|шөлмек|tr=şölmek}}
      |* {{T|km}} : {{trad+|km|ដប|tr=dɑɑp}}
      |* {{T|ky}} : {{trad+|ky|бөтөлкө}}
      |* {{T|avk}} : {{trad--|avk|tirac}}
      |* {{T|lo}} : {{trad-|lo|ຂວດ|tr=khūat}}, {{trad+|lo|ແກ້ວ|tr=kǣu}}
      |* {{T|la}} : {{trad-|la|lagoena}}
      |* {{T|lv}} : {{trad+|lv|pudele}}
      |* {{T|ln}} : {{trad-|ln|molangi}}
      |* {{T|lt}} : {{trad-|lt|butelis}}
      |* {{T|mk}} : {{trad-|mk|шише|n|tr=chiché}}
      |* {{T|ms}} : {{trad-|ms|botol}}
      |* {{T|mt}} : {{trad-|mt|flixkun}}
      |* {{T|gv}} : {{trad-|gv|boteil}}
      |* {{T|mn}} : {{trad+|mn|лонх|tr=lonkh|tradi=ᠯᠣᠩᠬᠤ}}
      |* {{T|mvf}} : {{trad+|mvf|ᠯᠣᠩᠬᠤ|tr=longqu}}
      |* {{T|nl}} : {{trad+|nl|fles}}
      |* {{T|no}} : {{trad+|no|flaske|f}}
      |*{{T|oc}} : {{trad+|oc|botelha}}
      |* {{T|ur}} : {{trad-|ur|بوتل|tr=botal}}, {{trad-|ur|شیشہ|tr=śīśa}}
      |* {{T|uz}} : {{trad+|uz|shisha}}
      |* {{T|pap}} : {{trad--|pap|bòter}}, {{trad--|pap|bòtro}}
      |* {{T|fa}} : {{trad+|fa|بطری|tr=botri}}, {{trad+|fa|شیشه|tr=šiše}}
      |* {{T|plodarisch}} : {{trad--|plodarisch|vlòsche|f}}
      |* {{T|pl}} : {{trad+|pl|butelka}}
      |* {{T|pt}} : {{trad+|pt|botelha}}, {{trad+|pt|garrafa}}
      |* {{T|ro}} : {{trad+|ro|sticlă}}
      |* {{T|ru}} : {{trad+|ru|бутылка}}
      |* {{T|se}} : {{trad--|se|boahtal}}, {{trad--|se|bohtal}}
      |* {{T|sc}} : {{trad--|sc|ampulla|f}}
      |* {{T|sr}} : {{trad+|sr|боца}}, {{trad+|sr|флаша}}
      |* {{T|zdj}} : {{trad--|zdj|falasika}}
      |* {{T|sk}} : {{trad+|sk|fľaša}}
      |* {{T|sl}} : {{trad+|sl|steklenica}}, {{trad-|sl|buteljka}}
      |* {{T|srn}} : {{trad--|srn|batra}}
      |* {{T|sv}} : {{trad+|sv|butelj|c}}, {{trad+|sv|flaska|c}}
      |* {{T|sw}} : {{trad+|sw|chupa}}
      |* {{T|tg}} : {{trad+|tg|шиша|tr=šiša}}
      |* {{T|tl}} : {{trad-|tl|bóte}}
      |* {{T|ta}} : {{trad+|ta|புட்டி|tr=puṭṭi}}
      |* {{T|tt}} : {{trad+|tt|шешә|tr=şeşä}}
      |* {{T|cs}} : {{trad+|cs|láhev}}
      |* {{T|te}} : {{trad+|te|సీసా}}
      |* {{T|th}} : {{trad+|th|ขวด|tr=khuaat}}
      |* {{T|tsolyáni}} : {{trad--|tsolyáni|izhúkh}} ({{p}} {{trad--|tsolyáni|izhúyal}})
      |* {{T|tr}} : {{trad+|tr|şişe}}
      |* {{T|tk}} : {{trad-|tk|çüýşe}}
      |* {{T|uk}} : {{trad-|uk|пляшка}}
      |* {{T|vi}} : {{trad+|vi|chai}}
      |* {{T|wym}} : {{trad--|wym|fłoś|f}}
      |* {{T|gdr}} : {{trad--|gdr|botol}}
      |* {{T|yi}} : {{trad-|yi|פֿלאַש|tr=flash}}
      |* {{T|zu}} : {{trad-|zu|ibhodlela}}, {{trad-|zu|ilibhodlela}}
      |{{trad-fin}}
      |{{trad-début|Son contenu}}
      |* {{T|br}} : {{trad+|br|boutailhad}}
      |* {{T|avk}} : {{trad--|avk|tiraccek}}
      |{{trad-fin}}
      |{{trad-début|Récipient pour stocker un gaz}}
      |* {{T|es}} : {{trad+|es|bombona|f}}, {{trad+|es|botella|f}}
      |* {{T|avk}} : {{trad--|avk|tieak}}
      |* {{T|pt}} : {{trad+|pt|botija|f}}
      |{{trad-fin}}
      |
      |=== {{S|verbe|fr|flexion}} ===
      |{{fr-verbe-flexion|bouteiller|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
      |'''bouteille''' {{pron|bu.tɛj|fr}}
      |# ''Première personne du singulier de l’indicatif présent du verbe'' [[bouteiller]].
      |# ''Troisième personne du singulier de l’indicatif présent du verbe'' [[bouteiller]].
      |# ''Première personne du singulier du subjonctif présent du verbe'' [[bouteiller]].
      |# ''Troisième personne du singulier du subjonctif présent du verbe'' [[bouteiller]].
      |# ''Deuxième personne du singulier de l’impératif du verbe'' [[bouteiller]].
      |
      |=== {{S|prononciation}} ===
      |* {{pron|bu.tɛj|fr}}
      |** {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|bu.tɛj|audio=Fr-bouteille.ogg}}
      |* Français méridional : {{pron|bu.ˈtɛ.jə|fr}}
      |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Ltrlg-bouteille.wav}}
      |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines1-bouteille.wav}}
      |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines2-bouteille.wav}}
      |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines3-bouteille.wav}}
      |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines5-bouteille.wav}}
      |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Aemines4-bouteille.wav}}
      |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines6-bouteille.wav}}
      |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-Poslovitch-bouteille.wav}}
      |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-bouteille.wav}}
      |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-bouteille.wav}}
      |
      |=== {{S|anagrammes}} ===
      |* [[beuillote]]
      |
      |=== {{S|voir aussi}} ===
      |* {{WP}}
      |* {{Vikidia}}
      |
      |=== {{S|références}} ===
      |* {{R:TLFi}}
      |* {{R:DAF8}}
      |* {{réf|2}}{{R:Rivarol}}
      |
      |[[Catégorie:Couleurs en français]]
      |[[Catégorie:Vie domestique en français]]
      |[[Catégorie:Lexique en français de l’œnologie]]""".stripMargin)

}