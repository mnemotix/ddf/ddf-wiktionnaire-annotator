package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextTest {
  val test = """=== {{S|étymologie}} ===
               |: ''(Nom 1)'' {{siècle|XII}} Forme collatérale de ''{{lien|têt|fr}}''{{R|TLFi}}, du {{étyl|la|fr|testum|sens=pot}}{{R|TLFi}}.
               |: ''(Nom 2)'' {{date|lang=fr|1686}} De l’{{étyl|en|fr|test}}{{R|TLFi}} emprunté à l’ancien français ''{{lien|test|fro}}'' (« pot »){{R|TLFi}} de même origine que le précédent. Voir le mot anglais ci-dessous pour l’évolution sémantique qui conduit de « pot » à « examen ».
               |: ''(Verbe)'' Apocope de ''[[tester]]''.
               |
               |=== {{S|nom|fr|num=1}} ===
               |{{fr-rég|tɛst}}
               |[[File:Colobocentrotus atratus MHNT Bali Test dos.jpg|thumb|Le '''test''' d’un oursin tortue]]
               |'''test''' {{pron|tɛst|fr}} {{m}}
               |# [[coquille#fr|Coquille]] [[externe]] [[dur]]e, [[calcaire]] ou [[chitineux|chitineuse]], de certains [[invertébré]]s.
               |#*'''''Test''' corné, osseux.''
               |#*''Ces parties dures : lorsqu’elles sont recouvertes par les muscles, elles portent le nom d’os ; lorsqu’elles les recouvrent, elles prennent ceux de '''test''', de coquille ou d’écaille, selon leur plus ou moins de consistance.'' {{source|Cuvier, ''Anat. comp''., t. 1, 1805}}
               |#*''Celles-ci [les infiltrations] ont dissous les coquilles, laissant à leur place un vide dans lequel on peut […] prendre l’empreinte du '''test''' disparu.'' {{source|Lapparent, ''Abr. géol.'', 1886}}
               |
               |==== {{S|synonymes}} ====
               |* {{lien|coque|fr}}
               |* {{lien|carapace|fr}}
               |
               |=== {{S|nom|fr|num=2}} ===
               |{{fr-rég|tɛst}}
               |'''test''' {{pron|tɛst|fr}} {{m}}
               |# [[essai|Essai]], [[opération]] que l’on fait pour [[vérifier]] la [[véracité]] d’une [[hypothèse]] ou d’un [[fait]].
               |#* ''Son '''test''' de dépistage était négatif.''
               |# {{éducation|fr}} [[examen|Examen]], [[concours]] ou [[épreuve]], [[évaluation]] des [[capacité]]s d’une personne.
               |#* ''Elle a réussi son '''test''' d’anglais.''
               |# {{technique|fr}} [[opération|Opération]] de [[vérification]] des propriétés réelles d’un [[produit]].
               |#* ''La viabilité de semences peut être déterminée par le célèbre '''test''' de chlorure de tetrazolium (TZ). Le '''test''' TZ est simple pour les besoins de la plupart des chercheurs en malherbologie, mais pour les technologues de semences le '''test''' peut être tout à fait complexe.'' {{source|''Gestion des mauvaises herbes pour les pays en développement'', addendum 1, Rome : FAO, 2005, page 12}}
               |
               |==== {{S|synonymes}} ====
               |* [[interrogation]]
               |* [[questionnaire]]
               |
               |==== {{S|dérivés}} ====
               |* [[banc de test]]
               |* [[développement piloté par les tests]] {{prog|nocat=1}}
               |* [[prétest]]
               |* [[test intégré]]
               |* [[test fonctionnel]]
               |* {{lien|tester|fr}}
               |** {{lien|testage|fr}}
               |* {{lien|testologie|fr}}
               |
               |==== {{S|traductions}} ====
               |{{trad-début|Test d’essai}}
               |* {{T|sq}} : {{trad+|sq|provë}}, {{trad-|sq|prova}}
               |* {{T|de}} : {{trad+|de|Probe}}, {{trad+|de|Test}}
               |* {{T|en}} : {{trad+|en|test}}
               |* {{T|ar}} : {{trad-|ar|اختبار|tr=’iχtibá:r|m}}
               |* {{T|zh}} : {{trad+|zh|试验|tradi=試驗|tr=shìyàn}}
               |* {{T|ko}} : {{trad+|ko|시험|tradi=試驗|tr=siheom}}, {{trad+|ko|테스트|tr=teseuteu}}
               |* {{T|es}} : {{trad+|es|prueba}}
               |* {{T|fi}} : {{trad+|fi|koe}}
               |* {{T|gallo}} : {{trad--|gallo|assai}}
               |* {{T|hu}} : {{trad+|hu|próba}}
               |* {{T|it}} : {{trad+|it|prova}}, {{trad+|it|verifica}}, {{trad+|it|test|m}}
               |* {{T|ja}} : {{trad+|ja|試験|tr=shiken}}, {{trad+|ja|テスト|tr=tesuto}}
               |* {{T|nl}} : {{trad+|nl|test}}, {{trad+|nl|proef}}
               |* {{T|fa}} : {{trad+|fa|آزمایش|R=âzmâyesh}}
               |* {{T|pt}} : {{trad+|pt|teste}}, {{trad+|pt|prova}}
               |* {{T|ru}} : {{trad+|ru|проба|R=próba}}, {{trad+|ru|тест}}
               |* {{T|se}} : {{trad--|se|iskkus}}
               |* {{T|sv}} : {{trad+|sv|prov}}, {{trad+|sv|prövning}}
               |* {{T|cs}} : {{trad+|cs|test}}, {{trad-|cs|testování}}
               |* {{T|tr}} : {{trad+|tr|test}}, {{trad+|tr|deneme}}
               |* {{T|uk}} : {{trad-|uk|тест}}, {{trad-|uk|випробування}}
               |{{trad-fin}}
               |
               |{{trad-début|Test technique}}
               |* {{T|en}} : {{trad+|en|test}}
               |* {{T|nl}} : {{trad+|nl|test}}
               |* {{T|sv}} : {{trad+|sv|test}}, {{trad-|sv|provning}}
               |* {{T|cs}} : {{trad+|cs|test}}
               |* {{T|te}} : {{trad+|te|పరీక్ష|R=pareeksha}}
               |{{trad-fin}}
               |
               |''Examen'' : {{cf|lang=fr|examen}}
               |
               |=== {{S|verbe|fr}} ===
               |'''test''' {{pron|tɛst|fr}} {{conj|grp=3|fr|déf=1}}
               |# {{argot|fr}} [[tester#fr-verb|Tester]].
               |#* ''Tu peux pas '''test''','' tu ne peux pas te mesurer à moi, tu ne m’arrives pas à la cheville.
               |
               |=== {{S|prononciation}} ===
               |* {{écouter|lang=fr|France (Muntzenheim)|audio=LL-Q150 (fra)-0x010C-test.wav}}
               |* {{écouter|lang=fr|Suisse (canton du Valais)|audio=LL-Q150 (fra)-DSwissK-test.wav}}
               |* {{écouter|||lang=fr|audio=LL-Q150 (fra)-WikiLucas00-test.wav}}
               |* {{écouter|lang=fr|France||audio=LL-Q150 (fra)-Adélaïde Calais WMFr-test.wav}}
               |
               |=== {{S|anagrammes}} ===
               |* [[têts]]
               |
               |=== {{S|voir aussi}} ===
               |* {{WP}}
               |
               |=== {{S|références}} ===
               |* {{Import:DAF8}}
               |{{Références}}
               |
               |[[Catégorie:Apocopes en français]]""".stripMargin
}
