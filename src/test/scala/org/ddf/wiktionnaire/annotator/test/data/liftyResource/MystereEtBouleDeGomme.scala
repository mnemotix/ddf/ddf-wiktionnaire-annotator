package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object MystereEtBouleDeGomme {
  val resource = Resource("mystère et boule de gomme","https://fr.wiktionary.org/wiki/","entry", """== {{langue|fr}} ==
                   |=== {{S|étymologie}} ===
                   |: Cette expression aurait été employée pour la première fois dans la littérature par [[w:Henry de Montherlant|Henry de Montherlant]] en 1949 dans sa pièce ''Demain il fera jour''.
                   |
                   |: Pour certains{{référence nécessaire|lang=fr}}, ce serait un [[jeu de mots]] construit à partir de « mystère de la Bégum » faisant référence au roman ''[[ w:Les Cinq Cents Millions de la Bégum|Les Cinq Cents Millions de la Bégum]]'' dont l’ébauche de Jean-François [[w:Paschal Grousset|Paschal Grousset]] a été remaniée par [[w:Jules Verne|Jules Verne]] (1879).
                   |
                   |=== {{S|interjection|fr}} ===
                   |'''mystère et boule de gomme''' {{pron|mis.tɛʁ e bul də ɡɔm|fr}} {{invariable}}
                   |# Interjection [[exprimer|exprimant]] le [[mystère]], l’[[incompréhension]].
                   |#*''Ce n’est pas croyable : il faut qu'il ait faim pour que l'appétit vous vienne ! Est-ce sublime ? Est-ce grotesque ? <br /> Gillou. — '''Mystère et boule de gomme'''.'' {{source|{{w|Henry de Montherlant}}, ''Demain il fera jour'', pièce en trois actes, Gallimard, 1949.}}
                   |#* ''Martin Baysse posa un doigt sur ses lèvres et cligna de l’œil : — Un truc à moi ! '''Mystère et boule de gomme''' !'' {{source|{{w|Henri Troyat}}, ''{{w|Les Semailles et les Moissons}}'', tome 3 : ''{{w|La Grive}}'', 1956.}}
                   |#* ''Comment, d’un déguisement parfait parce qu’il semble que sa porteuse n’a de réalité que déguisée, peut-on passer à une aussi parfaite absence de déguisement ? '''Mystère et boule de gomme''' : voilà ce que, présente, me soufflerait peut-être Wang Yuen-chen, à qui l’un de mes compagnons avait appris cette locution, dont elle usait un peu à tort et à travers sans cesser de s’en amuser.'' {{source|{{w|Michel Leiris}}, ''La Règle du jeu'', tome III : ''Fibrilles'', 1966.}}
                   |#* ''Comment il a fait pour grimper ces échelons, '''mystère et boule de gomme''' ! ajouta Mrs Sixsmith en souriant.'' {{source|{{w|Ronald Firbank}}, ''Œuvres romanesques'', tome 1, 1984 : nouvelle ''Caprice'' (publiée en anglais, sous le même titre, en 1917), dans une traduction de Gérard Joulié.}}
                   |
                   |=== {{S|nom|fr}} ===
                   |{{fr-rég|mis.tɛʁ e bul də gɔm|p=mystères et boules de gomme}}
                   |'''mystère et boule de gomme''' {{m}}
                   |#{{par ext}} {{plais|fr}} [[mystère|Mystère]].
                   |#* ''Diane » est un récit piégé toutes les trois lignes par un '''mystère et boule de gomme'''.'' <small>(Jeanne Bem, ''Le texte traversé'', 1991)</small>
                   |#* ''J’en avais assez de ses '''mystères et boules de gomme'''.'' <small>(Fernando Arrabal, ''L'extravagante croisade d'un castrat amoureux'', 1989)</small>
                   |
                   |==== {{S|traductions}} ====
                   |{{trad-début}}
                   |* {{T|de}} : {{trad-|de|ein Buch mit sieben Siegeln}}
                   |{{trad-fin}}
                   |
                   |=== {{S|prononciation}} ===
                   |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-mystère et boule de gomme.wav}}""".stripMargin)
}
