/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Philosophe {
  val Philosophe = """{{voir|philosophé}}
                     |
                     |== {{langue|fr}} ==
                     |=== {{S|étymologie}} ===
                     |: Du {{étyl|grc|fr|φιλόσοφος|philósophos|celui qui aime la [[sagesse]]}}.
                     |
                     |* {{siècle|XVII|lang=fr}} {{exemple
                     ||Il faut dire, cette femme eſt ''Poëte'', eſt '''''Philoſophe''''', eſt ''Medecin'', eſt ''Auteur'', eſt ''Peintre'' ; & non ''Poëteſſe'', ''Philoſopheſſe'', ''Medecine'', ''Autrice'', ''Peintreſſe'', ''&c''.
                     ||sens=Il faut dire, cette femme est ''poète'', est '''''philosophe''''', est ''médecin'', est ''auteur'', est ''peintre'' ; et non ''poétesse'', ''philosophesse'', ''médecine'', ''autrice'', ''peintresse'', ''etc''.
                     ||source={{ouvrage|prénom=Nicolas|nom=Andry de Boisregard|titre=Reflexions ſur l’uſage préſent de la Langue Françoiſe ou Remarques Nouvelles & Critiques touchant la politeſſe du Langage|éditeur={{w|Laurent d’Houry}}|date=1692|passage=163-164|année première édition=1689|url={{Citation/Books.Google|l2tubIwXURgC|PA163|surligne=Philosophesse}}}}|tête=*|lang=fr}}
                     |
                     |=== {{S|nom|fr}} ===
                     |{{fr-rég|fi.lɔ.zɔf|mf=oui}}
                     |'''philosophe''' {{pron|fi.lɔ.zɔf|fr}} {{mf}}
                     |# [[personne|Personne]] qui [[pratiquer|pratique]] la [[philosophie]].
                     |#* ''C’est une Savante, & une '''Philosophe''' de profession.'' {{source|''Le babillard, ou Le nouvelliste philosophe'', 1724, volume 1, page 413}}
                     |#* ''Les '''philosophes''', les théologiens et la plupart des héros d’arguments ont le génie de la nation française : ils attaquent vigoureusement, mais sont perdus s’ils sont réduits à la guerre défensive.'' {{source|[[w:Frédéric II de Prusse|Frédéric II]] et {{w|Voltaire}}, ''[[w:Anti-Machiavel|L'anti-Machiavel]]'', 1739, édition de 1947}}
                     |#* ''Je suis sûr que j’ai rencontré depuis deux jours dans les rues les '''philosophes''' les plus illustres et les fonctionnaires les plus influents. C’est mon désespoir de ne pas les connaître.'' {{source|{{Citation/Pierre Louÿs/Aphrodite/1896}}}}
                     |#* ''Ces personnes ont craint, en pénétrant trop avant dans le système de l’homme, de voir disparaître ses plus brillantes attributions; et, si elles ont été portées à applaudir à la sagacité du '''philosophe''', elles ont été sur le point de condamner la philosophie.'' {{source|''Journal de médecine, chirurgie, pharmacie, etc'', volume 35, 1816, page 376}}
                     |#* ''[…], et c’est seulement après 1967 que le '''philosophe''' laïque syrien Sadiq Jalal al-Azm refusa hautement de voir dans la religion une voie d’avenir efficace.'' {{source|Panayiotis Jerasimof Vatikiotis, ''L’Islam et l’État'', 1987, traduction d’Odette Guitard, 1992, pages 108-109}}
                     |#* ''La fonction du '''philosophe''' consiste exclusivement dans la ''profanation'' des idées. Aucune violence n’égale par ses effets la violence théorique. Plus tard, l’action vient…'' {{source|{{w|Paul Nizan}}, ''{{w|La Conspiration}}'', 1938, page 44}}
                     |#* ''Autrement dit : un '''philosophe''' n'est pas quelqu'un ''qui parle'' en '''philosophe''' mais ''qui vit'' en '''philosophe''', non pas un grand parleur, un habile conférencier, mais un être qui a décidé de mettre en pratique les pensées philosophiques qui sont les siennes. En regard de cette méthode, on comprend que Pierre Hadot prenne plaisir à citer souvent cette phrase de Thoreau : « Il y a de nos jours des professeurs de philosophie, mais pas de '''philosophes'''. »'' {{source|{{w|Michel Onfray}}, ''La résistance au nihilisme'', Grasset, 2020, p. 449}}
                     |# {{en particulier}} [[écrivain|Écrivain]] du [[dix-huitième]] [[siècle]] [[éprendre|épris]] d’[[idée]]s de [[réforme]] en [[matière]] [[religieux|religieuse]], [[politique]], [[social]]e et [[moral]]e.
                     |#* ''Les philosophes ont favorisé la publication de l’''Encyclopédie.
                     |#* {{term|par [[apposition]]}} — ''Frédéric II de Prusse se montra un roi '''philosophe'''
                     |# {{par ext}} Celui qui [[cultiver|cultive]] sa [[raison]], qui [[conformer|conforme]] sa [[conduite]] à des [[principe]]s et [[travailler|travaille]] à [[fortifier]] son âme [[contre]] les [[coup]]s du [[sort]] ; personne qui a un [[comportement]] [[inspirer|inspiré]] par la [[sagesse]].
                     |#* ''Il a pris cette mauvaise nouvelle en '''philosophe'''.''
                     |#* ''Il vécut et mourut en '''philosophe'''.''
                     |#* ''Un vrai '''philosophe''' sait résister à ses passions et se vaincre lui- même.''
                     |# [[homme|Homme]] qui [[mener|mène]] une vie [[tranquille]] et [[retirer|retirée]], [[hors]] de l’[[embarras]] des [[affaire]]s.
                     |#* ''Il s’est retiré pour toujours à la campagne; c’est un '''philosophe''', un vrai '''philosophe'''.''
                     |# [[étudiant|Étudiant]] qui [[étudier|étudie]] la philosophie.
                     |# {{péjoratif|fr}} [[personnage|Personnage]] [[rêveur]], qui manque du [[sens]] des [[réalité]]s.
                     |#* ''Les jurisconsultes civils donnèrent aussi leur avis et Valin dans son ''Commentaire sur l'Ordonnance de la Marine'' déclara « que les '''philosophes''' (sic) qui désapprouvaient la guerre de course étaient de mauvais citoyens. »'' {{source|{{w|Étienne Dupont}}, ''Le vieux Saint-Malo : Les Corsaires chez eux'', Édouard Champion, 1929, page 50}}
                     |
                     |==== {{S|notes}} ====
                     |: Le nom féminin ''{{lien|philosophesse|fr}}'' existait jadis, il est plus rare aujourd’hui.
                     |
                     |==== {{S|dérivés}} ====
                     |* [[antiphilosophe]]
                     |* [[bateaux des philosophes]]
                     |* [[philosophant]]
                     |* [[philosophard]]
                     |* [[philosophâtre]]
                     |* [[philosophesque]]
                     |
                     |==== {{S|apparentés}} ====
                     |* [[philosopher]]
                     |* [[philosophie]]
                     |* [[philosophique]]
                     |* [[philosophiquement]]
                     |
                     |==== {{S|traductions}} ====
                     |{{trad-début}}
                     |* {{T|de}} : {{trad+|de|Philosoph|m}}
                     |* {{T|en}} : {{trad+|en|philosopher}}
                     |* {{T|ast}} : {{trad-|ast|filósofu|m}}
                     |* {{T|ca}} : {{trad+|ca|filòsof|m}}
                     |* {{T|eo}} : {{trad-|eo|filozofo}}
                     |* {{T|et}} : {{trad+|et|filosoof|m}}
                     |* {{T|fi}} : {{trad+|fi|filosofi}}
                     |* {{T|el}} : {{trad+|el|φιλόσοφος|mf|tr=filósofos}}
                     |* {{T|io}} : {{trad+|io|filozofo}}
                     |* {{T|it}} : {{trad+|it|filosofo|m}}, {{trad+|it|filosofa|f}}
                     |* {{T|kk}} : {{trad-|kk|пәлсапашы|tr=pälsapaşı}}
                     |* {{T|pt}} : {{trad+|pt|filósofo|m}}
                     |* {{T|ru}} : {{trad+|ru|философ}}
                     |* {{T|uk}} : {{trad-|uk|мудрій|m}}
                     |{{trad-fin}}
                     |
                     |=== {{S|adjectif|fr}} ===
                     |{{fr-rég|fi.lɔ.zɔf|mf=oui}}
                     |'''philosophe''' {{pron|fi.lɔ.zɔf|fr}} {{mf}}
                     |# Qui [[résigner|est résigné]], qui [[supporter|supporte]] [[avec]] [[sagesse]] et [[force]] d’[[âme]] les [[épreuve]]s.
                     |#* ''Il s’est montré très '''philosophe''' en cette circonstance''.
                     |
                     |==== {{S|traductions}} ====
                     |{{trad-début}}
                     |* {{T|eo}} : {{trad-|eo|filozofa}}
                     |* {{T|it}} : {{trad+|it|filosofo|m}}
                     |* {{T|pt}} : {{trad+|pt|filósofo|m}}
                     |{{trad-fin}}
                     |
                     |=== {{S|verbe|fr|flexion}} ===
                     |{{fr-verbe-flexion|philosopher|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
                     |'''philosophe''' {{pron|fi.lɔ.zɔf|fr}}
                     |# ''Première personne du singulier du présent de l’indicatif de'' [[philosopher]].
                     |# ''Troisième personne du singulier du présent de l’indicatif de'' [[philosopher]].
                     |# ''Première personne du singulier du présent du subjonctif de'' [[philosopher]].
                     |# ''Troisième personne du singulier du présent du subjonctif de'' [[philosopher]].
                     |# ''Deuxième personne du singulier de l’impératif de'' [[philosopher]].
                     |
                     |=== {{S|prononciation}} ===
                     |* {{pron-rimes|fi.lɔ.zɔf|fr}}
                     |* {{écouter|France (Brétigny-sur-Orge)|fil̪ozɔf|lang=fr|audio=LL-Q150 (fra)-Pamputt-philosophe.wav}}
                     |* {{écouter|Suisse (canton du Valais)||lang=fr|audio=LL-Q150 (fra)-DSwissK-philosophe.wav}}
                     |* {{écouter|France (Vosges)||lang=fr|audio=LL-Q150 (fra)-Poslovitch-philosophe.wav}}
                     |
                     |=== {{S|voir aussi}} ===
                     |* {{WP}}
                     |* {{Wikiquote|Catégorie:Femme philosophe‎|lang=fr}}
                     |* {{Le Dico des Ados}}
                     |
                     |=== {{S|références}} ===
                     |* {{Import:DAF8}}
                     |* {{ouvrage|auteur=Jules Dessiaux|titre=Examen critique de la Grammaire des grammaires de M. Girault-Duvivier : avec des supplémens indispensables extraits des meilleurs grammairiens|lieu=Paris|éditeur=L. Hachette|date=1832|passage=279|url={{Citation/Books.Google|IvrcR44Kgi4C|RA1-PA279|surligne=philosophe}}}}
                     |* {{R:Littré}}
                     |* {{R:TLFi}}
                     |
                     |== {{langue|la}} ==
                     |=== {{S|étymologie}} ===
                     |: De {{lien|''philosophus''|la|adj|sens=philosophique}} avec le suffixe adverbial  {{lien|''-e''|la}}.
                     |
                     |=== {{S|adverbe|la}} ===
                     |{{Lang|la|'''philosophe'''}} {{pron||la}}
                     |# [[philosophiquement|Philosophiquement]], avec philosophie, en philosophe.
                     |
                     |=== {{S|références}} ===
                     |* {{R:Gaffiot}}
                     |""".stripMargin
}
