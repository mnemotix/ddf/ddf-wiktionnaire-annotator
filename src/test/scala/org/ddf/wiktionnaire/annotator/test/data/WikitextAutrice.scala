/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextAutrice {
  val autrice = """== {{langue|fr}} ==
                  |=== {{S|étymologie}} ===
                  |: {{date|1477-1478|lang=frm}} Attesté d’abord sous la forme ''{{lien|actrixe|frm}}''{{R|Leseur-1477}}, ensuite {{date|1503|lang=frm}} ''{{lien|auctrixe|frm}}''{{R|Delorme-1927}}, puis {{date|1524|lang=frm}} sous la forme ''{{lien|auctrice|frm}}'' dans une lettre de {{w|Guillaume Briçonnet (1470-1534)|Guillaume Briçonnet}} à {{w|Marguerite de Valois-Angoulême}}. Puis sous le pluriel ''{{lien|autrices|frm}}'' en 1554 dans ''La Claire'', de {{w|Louis Le Caron}}. Du {{étyl|la|fr|mot=auctrix|sens=agente, autrice, fondatrice, {{lien|instigatrice|fr}} », « {{lien|conseillère|fr}} », en droit « garante d’une vente}}{{R|Evain-2019|r=1}}. Emprunt savant au latin, l’évolution phonétique classique du ''c'' devant ''t'' (k implosif devant t : ''{{lien|factum|la}}'' qui donne ''{{lien|fait|fr}}'') en {{lien|roman|fr}} et en {{lien|ancien français|fr}} n’a pas joué, elle était déjà achevée en {{lien|moyen français|fr}}, bien avant les premières attestations du mot{{R|Laborderie-2015}}.
                  |
                  |: Le latin ''auctrix'' est d’abord défini dans les premiers lexiques et dictionnaires latin-moyen français comme synonyme d’{{lien|''accroisseresse''|frm|sens=celle qui accroit}} et d’{{lien|''augmenteresse''|frm|sens=celle qui augmente}}{{R|Evain-2019|r=1}}, et les premières occurrences {{lien|autrice|frm|dif=en moyen français}} gardent pour la plupart un sens restreint, pour décrire l’origine d’un mouvement ou d’une action. Alors que le mot ''{{lien|acteur|frm}}'' (dont le féminin n’a presque pas d’occurrence avant le {{siècle2|17}} siècle) est employé dans un sens large puisqu’il peut désigner un faiseur de livres, un croisement s’opère au cours du {{siècle2|17}} siècle : le sens d’''acteur'' se restreint à un synonyme de ''{{lien|comédien|fr}}'', il gagne pleinement le féminin ''{{lien|actrice|fr}}'', tandis que le sens d’''{{lien|auteur|fr}}'' s’accroit, englobe le sens de « rédacteur d’ouvrages », et gagne ainsi en prestige littéraire et social{{R|Evain-2019}}. Son féminin ''autrice'' est alors condamné par des grammairiens comme {{w|Jean-Louis Guez de Balzac}} et par l’{{w|Académie française}}{{R|Evain-2019|r=1}}.
                  |
                  |: Le nom, encore actif dans la première moitié du {{siècle2|20}} siècle, est en remontée{{R|Feo-2018}}{{R|Delvaux-Melancon-2019}}. Voir aussi la [[#Notes|note]].
                  |
                  |==== {{S|attestations}} ====
                  |[[Fichier:Marguerite d Autriche par Bernard van Orley vers 1518 huile sur bois.jpeg|vignette|redresse|{{w|Marguerite d'Autriche (1480-1530)|Marguerite d’Autriche}}, « '''auctrice''' de paix »|alt=Portrait de Marguerite d'Autriche (1480-1530)]]
                  |* {{siècle|XV|lang=frm}} {{Lang|frm|''Et, sur ce point, fera l’Acteur fin aux ditz de sa prose et donnera lieu à la très prudente et saige '''actrixe''' bonne Memoire, sa coadjuteure, pour retourner au paracompliment de l’exploracion, […]''}} {{source|{{ouvrage|auteur=Guillaume Leseur|titre=Histoire de Gaston IV, comte de Foix|sous-titre=Chronique française inédite du {{siècle2|15}} siècle|tome=II, 1477-1478|éditeur=Henri Courteault|date=1896|lieu=Paris – Renouard|passage=274|url=https://archive.org/details/histoiredegasto00courgoog/}}}}
                  |* {{siècle|XVI|lang=frm}} {{Lang|frm|''Savoir faisons à tous présens et à venir, que nous ayans agréable l’approbation faicte par l’Eglise et sainct Siège appostolicque de la vie religieuse en sexe femenyn dont nostre très chère et très amée cousine Jehanne de France, […] est '''auctrixe''' et fondatrixe soubz le nom de l’Anunciation et tiltre des dix plaisirs de la glorieuse Vierge Marie, […]''}} {{source|''Lettres de Louis XII portant approbation de la règle et monastère de l’Annonciade'', décembre 1503, citées dans {{périodique|auteur=Ferdinand Delorme|titre=Documents pour l’histoire du Bienheureux Gabriel-Maria|journal=La France franciscaine : mélanges d’archéologie, d’histoire et de littérature|date=janvier 1927|pages=220|url=https://gallica.bnf.fr/ark:/12148/bpt6k55124541/f224}}}}
                  |* {{siècle|XVI|lang=frm}} {{Lang|frm|''Sur quoy lesdits seigneur roy et dame m’ont escript, comme '''auctrice''' de paix, vouloir pourveoir et remedier, tant par les lettres à V. M. que à l’imperatrice, affin que mesdits seigneurs leurs enffans, pour austant qu’ilz ont encoires à demeurer en Espaigne, que ne sera longuement comme ilz esperent, soient bien traictez''}} {{source|{{w|Marguerite d'Autriche (1480-1530)|Marguerite d’Autriche}}, lettre du 11 octobre 1529, citée dans {{ouvrage|prénom=Edward|nom=Le Glay|titre=Négociations diplomatiques entre la France et l’Autriche durant les trente premières années du {{siècle2|XVI}} siècle|tome=2|lieu=Paris|date=1845|passage=710|url=https://gallica.bnf.fr/ark:/12148/bpt6k54074935/f719}}}}
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég|o.tʁis}}
                  |[[Fichier:Launch of IYA 2009, Paris - Grygar, Bell Burnell cropped.jpg|vignette|redresse|{{w|Jocelyn Bell}} est l’'''autrice''' ''({{nobr|sens 1}})'' de la découverte des pulsars|alt=Jocelyn Bell, astrophysicienne britannique, en 2009]]
                  |[[Fichier:Yōko Tawada signiert.jpg|vignette|redresse|Une '''autrice''' ''({{nobr|sens 2}})'' japonaise|alt=L’autrice japonaise Yōko Tawada signe un livre]]
                  |[[Fichier:Catherine-de-medici.jpg|vignette|redresse|{{w|Catherine de Médicis}} fut l’'''autrice''' ''({{nobr|sens 3}})'' de plusieurs reines et plusieurs rois|alt=Miniature de Catherine de Médicis]]
                  |'''autrice''' {{prononciation|o.tʁis|lang=fr}} {{f}} {{équiv-pour|un homme|auteur|lang=fr}}
                  |# {{lien|femme|fr|dif=Femme}} à l’{{lien|origine|fr}} de {{lien|quelque chose|fr}}.
                  |#* {{exemple
                  ||Cette situation familiale, prise en compte dans le processus de personnalisation des peines au même titre que la personnalité de l’auteur ou de l’'''autrice''', la gravité et les circonstances de l’infraction, fait que, dans bien des cas, la criminelle n’est pas seule à la barre
                  ||source={{périodique|auteur=Lucile Quillet|titre=Les femmes, ces criminelles (presque) comme les hommes|journal={{w|Slate (magazine)|Slate}}|date=4 mars 2019|url=http://www.slate.fr/story/174153/societe/les-femmes-et-le-crime-episode-1-criminelles-comme-hommes}}
                  ||lang=fr
                  |}}
                  |#* {{exemple
                  ||L’'''autrice''' présumée de l’attaque a été arrêtée à Seattle.
                  ||source={{périodique|auteur=Harold Grand|titre=Les données personnelles de 100 millions d’Américains exposées dans un piratage géant|journal={{w|Le Figaro}}|date=30 juillet 2019|url=https://www.lefigaro.fr/secteur/high-tech/les-donnees-personnelles-de-100-millions-d-americains-exposees-dans-un-piratage-geant-20190730}}
                  ||lang=fr
                  |}}
                  |# {{lexique|littérature|fr}} {{lien|femme|fr|dif=Femme}} qui a rédigé un {{lien|ouvrage|fr}} de {{lien|littérature|fr}}, de {{lien|science|fr}} ou d’{{lien|art|fr}}. {{absolument}} {{lien|écrivaine|fr|dif=Écrivaine}}.
                  |#* {{exemple
                  ||Au Carouzel de nôtre Roy<br/>La jeune '''Autrice''' de Torquate<br/>Piéce charmante et délicate<br/>A fait, en ſtile net & fin,<br/>Vn Carouzel pour le Daufin
                  ||source={{ouvrage|prénom=Jean|nom=Loret|titre=La Muse historique du 27 mai 1662|date=1665|passage=78|url={{Citation/Books.Google|5r_aFST4AYUC|PA78|surligne=Autrice}}}}
                  ||lang=fr
                  |}}
                  |#*: Au carrousel de notre roi<br/>La jeune '''autrice''' de Torquate<br/>Pièce charmante et délicate<br/>A fait, en style net et fin,<br/>Un carrousel pour le dauphin
                  |#* {{exemple
                  ||Corrigez le mot dont vous uſez.<br/>En fait de bel Eſprit vous parlez en Novice.<br/>Un Homme eſt un Autheur, une Femme eſt '''Autrice'''.<br/>Appelez-donc Madame '''Autrice''', & non Autheur,<br/>Et parlons d’autre choſe.
                  ||source={{ouvrage|prénom=Gaspard|nom=Abeille|titre=Crispin bel esprit|lieu=Paris|date=1696|année première édition=1681|passage=56|url=https://gallica.bnf.fr/ark:/12148/bpt6k3042306t/f39}}
                  ||lang=fr
                  |}}
                  |#*: Corrigez le mot dont vous usez.<br/>En fait de bel esprit vous parlez en novice.<br/>Un homme est un auteur, une femme est '''autrice'''.<br/>Appelez-donc Madame '''autrice''', et non auteur,<br/>Et parlons d’autre chose.
                  |#* {{exemple
                  ||Si l’on ne dit pas une femme '''Autrice''', c’est qu’une femme qui fait un livre est une femme extraordinaire ; mais il est dans l’ordre qu’une femme aime les spectacles, la poésie, etc. comme il est dans l’ordre qu’elle soit Spectatrice.
                  ||source=M. Hilscher, cité dans {{ouvrage|prénom=Louis-Sébastien|nom=Mercier|titre=Néologie, ou vocabulaire des mots nouveaux, à renouveler, ou pris dans des acceptions nouvelles|lieu=Paris|éditeur=Moussard, Maradan|date=1801|tome=1|passage=26|url={{Citation/Books.Google|5pQPAAAAQAAJ|PA26|surligne=autrice}}}}
                  ||lang=fr
                  |}}
                  |#* {{exemple
                  ||Un journal discourait naguère sur ''authoresse'', et, le proscrivant avec raison, le voulait exprimer par ''auteur''. Pourquoi cette réserve, cette peur d’user des forces linguistiques ? Nous avons fait ''actrice'', ''cantatrice'', ''bienfaitrice'', et nous reculons devant '''autrice''' […] Autant avouer que nous ne savons plus nous servir de notre langue
                  ||source={{ouvrage|prénom=Remy|nom=de Gourmont|titre=Esthétique de la langue française|éditeur={{w|Mercure de France}}|lieu=Paris|date=1899|passage=37|url=https://gallica.bnf.fr/ark:/12148/bpt6k295676/f90}}
                  ||lang=fr
                  |}}
                  |#* {{exemple
                  ||Exemple de vocable de la liste précédente employé par une '''autrice''' contemporaine : […]
                  ||source={{périodique|prénom=Jacques|nom=Damourette|titre=Le lexique du Dictionnaire de l’Académie française (Huitième édition)|journal={{w|Le Français moderne}}|no=1|date=1935|pages=71|url=https://fr.calameo.com/read/0009039476be3a5c7e240}}
                  ||lang=fr
                  |}}
                  |#* {{exemple
                  ||Parce que le mot sonne clair, affirme sa féminité, s’appuie sur l’histoire et la proximité d’''actrice'', les ''Livres du Soir'' diront, dorénavant, '''''autrice'''''.
                  ||source={{périodique|auteur=Jean-Claude Vantroyen|titre=Auteur, auteure ou autrice ? « Le Soir » choisit autrice|journal={{w|Le Soir}}|date=5 janvier 2019|url=https://plus.lesoir.be/198562/article/2019-01-05/auteur-auteure-ou-autrice-le-soir-choisit-autrice}}
                  ||lang=fr
                  |}}
                  |# {{plus rare}} {{lien|génitrice|fr|dif=Génitrice}} ; {{lien|ascendante|fr}} ({{note}} ce sens n’est plus guère utilisé que sous la forme de la locution « {{lien|autrice de mes jours|fr}} »).
                  |#* {{exemple
                  ||Au commencement de cette lettre l’'''Autrice''' de l’Impetrant a taché de capter la bienveillance de ſa belle Sœur, en lui voulant procurer un Docteur pour la guerir de ſon chancre, dont elle eſt morte peu de tems après.
                  ||source={{ouvrage|titre=Reflexions paratitlaires pour la Dame Marie Joſephe Comteſſe Douariere… contre Meſſire Frederic Comte d’Eynatten Seigneur…|date=1728|passage=12|url={{Citation/Books.Google|zf9CAAAAcAAJ|PA12|surligne=autrice}}}}
                  ||lang=fr
                  |}}
                  |#*: Au commencement de cette lettre l’'''autrice''' de l’impétrant a tâché de capter la bienveillance de sa belle-sœur, en lui voulant procurer un docteur pour la guérir de son chancre, dont elle est morte peu de temps après.
                  |
                  |==== {{S|notes}} ====
                  |: Malgré son emploi jusque dans la première moitié du {{siècle2|20}} siècle, le TLFi{{R|TLFi|auteur}} indique qu’''auteur'' n’a pas de forme féminine. Le mot ''autrice'' a fait cependant son retour, à partir de 1996 comme féminin d’{{lien|auteur|fr}} dans le {{R:PRobertF|1996}}, sous une forme plus développée à partir du {{R:PRobertF|2003}}, ainsi que dans le ''{{w|Dictionnaire Hachette}}'' et ''{{w|L'Officiel du jeu Scrabble|L’Officiel du jeu Scrabble}}'' (''ODS4'') à partir de 2004. En 2019, l’Académie française{{R|AF-2019}} juge qu’''autrice'' est le féminin d’''auteur'' « dont la formation est plus satisfaisante » et elle réhabilite le terme{{R|Lecaplain-2019}} (souvent perçu à tort comme un néologisme{{R|Lorenzo-2019}}), quarante ans après la recommandation de l’{{w|Office québécois de la langue française}}.
                  |: {{note-féminisation}}
                  |
                  |==== {{S|variantes orthographiques}} ====
                  |<!-- {{(}} -->
                  |* {{lien|auctrice|fr}} {{archaïsme|nocat=1}}
                  |* {{lien|authrice|fr}} {{archaïsme|nocat=1}}
                  |<!-- {{)}} -->
                  |
                  |==== {{S|synonymes}} ====
                  |{{(|Celle qui est à l’origine de quelque chose|1}}
                  |* {{lien|artisane|fr}}
                  |* {{lien|conceptrice|fr}}
                  |* {{lien|constructrice|fr}}
                  |* {{lien|créatrice|fr}}
                  |* {{lien|faiseuse|fr}}
                  |* {{lien|fondatrice|fr}}
                  |* {{lien|formulatrice|fr}}
                  |* {{lien|génératrice|fr}}
                  |* {{lien|inauguratrice|fr}}
                  |* {{lien|initiatrice|fr}}
                  |* {{lien|innovatrice|fr}}
                  |* {{lien|introductrice|fr}}
                  |* {{lien|inventrice|fr}}
                  |* {{lien|originatrice|fr}}
                  |* {{lien|perpétratrice|fr}}
                  |* {{lien|promotrice|fr}}
                  |{{)}}
                  |{{(|Celle qui a rédigé un ouvrage|2}}
                  |* {{lien|auteure|fr}}
                  |* {{lien|auteuresse|fr}} {{désuet|nocat=1}}
                  |* {{lien|auteuse|fr}} {{peu usité|nocat=1}}
                  |* {{lien|authoress|fr}} {{désuet|nocat=1}} {{anglicisme|nocat=1}}
                  |* {{lien|authoresse|fr}} {{désuet|nocat=1}} {{anglicisme|nocat=1}}
                  |* {{lien|autoresse|fr}} {{désuet|nocat=1}} {{anglicisme|nocat=1}}
                  |{{)}}
                  |{{(|Génitrice|3}}
                  |* {{lien|dabesse|fr}} {{argot|nocat=1}}
                  |* {{lien|daronne|fr}} {{argot|nocat=1}}
                  |* {{lien|dabuche|fr}} {{argot|nocat=1}}
                  |* {{lien|génitrice|fr}}
                  |* {{lien|maman|fr}} {{familier|nocat=1}}
                  |* {{lien|mater|fr}} {{argot|nocat=1}}
                  |* {{lien|mère|fr}}
                  |* {{lien|reum|fr}} {{term|verlan}}
                  |{{)}}
                  |
                  |==== {{S|quasi-synonymes}} ====
                  |* {{lien|auteur|fr}}
                  |
                  |==== {{S|antonymes}} ====
                  |{{(|Celle qui est à l’origine de quelque chose|1}}
                  |* {{lien|copiste|fr}}
                  |* {{lien|destructrice|fr}}
                  |* {{lien|défaiseuse|fr}}
                  |* {{lien|disciple|fr}}
                  |* {{lien|imitatrice|fr}}
                  |* {{lien|supportrice|fr}}
                  |* {{lien|suiveuse|fr}}
                  |{{)}}
                  |{{(|Celle qui a rédigé un ouvrage|2}}
                  |* {{lien|lectrice|fr}}
                  |* {{lien|spectatrice|fr}}
                  |{{)}}
                  |{{(|Génitrice|3}}
                  |* {{lien|enfant|fr}}
                  |{{)}}
                  |
                  |==== {{S|dérivés}} ====
                  |<!-- {{(}} -->
                  |* {{lien|autrice-compositrice|fr}}
                  |* {{lien|autrice-compositrice-interprète|fr}}
                  |* {{lien|autrice de bande dessinée|fr}}
                  |* {{lien|autrice dramatique|fr}}
                  |* {{lien|autrice-productrice|fr}}
                  |* {{lien|cantautrice|fr}} {{rare|nocat=1}}
                  |* {{lien|coautrice|fr}}
                  |* {{lien|grande autrice|fr}}
                  |<!-- {{)}} -->
                  |
                  |==== {{S|dérivés autres langues}} ====
                  |* {{L|nl}} : {{lien|autrice|nl}}
                  |
                  |==== {{S|vocabulaire}} ====
                  |* {{voir thésaurus|fr|autorat|dictionnaire}}
                  |
                  |==== {{S|phrases}} ====
                  |* {{lien|autrice de mes jours|fr}}
                  |
                  |==== {{S|hyperonymes}} ====
                  |{{(|Génitrice|3}}
                  |* {{lien|parent|fr}}
                  |{{)}}
                  |
                  |==== {{S|hyponymes}} ====
                  |{{(|Celle qui est à l’origine de quelque chose|1}}
                  |* {{lien|coupable|fr}}
                  |* {{lien|découvreuse|fr}}
                  |* {{lien|inventrice|fr}}
                  |* {{lien|instigatrice|fr}}
                  |* {{lien|responsable|fr}}
                  |{{)}}
                  |{{(|Celle qui a rédigé un ouvrage|2}}
                  |* {{lien|autobiographe|fr}}
                  |* {{lien|bardesse|fr}}
                  |* {{lien|bas-bleu|fr}} {{péjoratif|nocat=1}}
                  |* {{lien|bédéiste|fr}}
                  |* {{lien|biographe|fr}}
                  |* {{lien|blogueuse|fr}}
                  |* {{lien|boulevardière|fr}}
                  |* {{lien|chroniqueuse|fr}}
                  |* {{lien|codificatrice|fr}}
                  |* {{lien|coupletière|fr}} {{péjoratif|nocat=1}}
                  |* {{lien|diariste|fr}}
                  |* {{lien|didacticienne|fr}}
                  |* {{lien|dramaturge|fr}}
                  |* {{lien|échotière|fr}}
                  |* {{lien|écrivaine|fr}}
                  |* {{lien|éditorialiste|fr}}
                  |* {{lien|essayiste|fr}}
                  |* {{lien|fabuliste|fr}}
                  |* {{lien|félibresse|fr}}
                  |* {{lien|femme de lettres|fr}}
                  |* {{lien|feuilletoniste|fr}}
                  |* {{lien|hagiographe|fr}}
                  |* {{lien|librettiste|fr}}
                  |* {{lien|littératrice|fr}}
                  |* {{lien|madrigalière|fr}}
                  |* {{lien|mémorialiste|fr}}
                  |* {{lien|pamphlétaire|fr}}
                  |* {{lien|parémiographe|fr}}
                  |* {{lien|parolière|fr}}
                  |* {{lien|poétesse|fr}}
                  |* {{lien|prosatrice|fr}}
                  |* {{lien|romancière|fr}}
                  |* {{lien|satiriste|fr}}
                  |* {{lien|scénariste|fr}}
                  |* {{lien|versificatrice|fr}}
                  |{{)}}
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début|Celle qui est à l’origine de quelque chose|1}}
                  |* {{T|de}} : {{trad-|de|Urheberin|f}}
                  |* {{T|en}} : {{trad+|en|author|mf}}, {{trad+|en|perpetratrix}}, {{trad+|en|creatrix}} {{rare|nocat=1}}
                  |* {{T|ar}} : {{trad-|ar|مؤلفة|f|dif=مُؤَلِّفَةٌ|tr=mūʾallifa}}
                  |* {{T|br}} : {{trad-|br|oberourez|f}}
                  |* {{T|ca}} : {{trad+|ca|autora|f}}
                  |* {{T|da}} : {{trad-|da|ophavskvinde}}
                  |* {{T|es}} : {{trad+|es|autora|f}}
                  |* {{T|eo}} : {{trad-|eo|aŭtorino|f}}
                  |* {{T|id}} : {{trad-|id|pelaku wanita}}
                  |* {{T|it}} : {{trad+|it|autrice|f}}
                  |* {{T|la}} : {{trad-|la|auctrix|f}}
                  |* {{T|nl}} : {{trad+|nl|daderes|f}}
                  |* {{T|oc}} : {{trad+|oc|autora|f}}
                  |* {{T|pl}} : {{trad+|pl|autorka|f}}, {{trad+|pl|sprawczyni|f}}
                  |* {{T|pt}} : {{trad-|pt|autora|f}}
                  |* {{T|ro}} : {{trad+|ro|autoare|f}}
                  |* {{T|ru}} : {{trad+|ru|авторша|f|tr=avtorša}}
                  |* {{T|te}} : {{trad-|te|కారకురాలు|f|tr=kārakurālu}}
                  |{{trad-fin}}
                  |
                  |{{trad-début|Celle qui a rédigé un ouvrage|2}}
                  |* {{T|de}} : {{trad+|de|Autorin|f}}, {{trad+|de|Schriftstellerin|f}}, {{trad+|de|Verfasserin|f}}, {{trad-|de|Urheberin|f}}
                  |* {{T|en}} : {{trad+|en|author|mf}}, {{trad+|en|authoress|f}} {{vieilli|nocat=1}}
                  |* {{T|ar}} : {{trad-|ar|مؤلفة|f|dif=مُؤَلِّفَةٌ|tr=mūʾallifa}}
                  |* {{T|ast}} : {{trad-|ast|autora|f}}
                  |* {{T|bn}} : {{trad-|bn|লেখিকা|f|tr=lēkhikā}}
                  |* {{T|br}} : {{trad-|br|oberourez|f}}, {{trad-|br|aozerez|f}}
                  |* {{T|bg}} : {{trad-|bg|авторка|f|tr=avtorka}}, {{trad-|bg|писателка|f|tr=pisátelka}}
                  |* {{T|ca}} : {{trad+|ca|autora|f}}
                  |* {{T|zh}} : {{trad-|zh|女作者|tr=nǚ zuòzhě}}
                  |* {{T|ko}} : {{trad-|ko|여류 작가|tr=yeoryu jakga|tradi=女流作家}}
                  |* {{T|da}} : {{trad+|da|forfatterinde|c}}
                  |* {{T|es}} : {{trad+|es|autora|f}}
                  |* {{T|eo}} : {{trad-|eo|aŭtorino|f}}, {{trad-|eo|verkistino|f}}
                  |* {{T|et}} : {{trad-|et|naisautor}}
                  |* {{T|fi}} : {{trad-|fi|kirjailijatar}}
                  |* {{T|ga}} : {{trad-|ga|banúdar|f}}
                  |* {{T|gl}} : {{trad-|gl|autora|f}}
                  |* {{T|cy}} : {{trad+|cy|awdures|f}}
                  |* {{T|el}} : {{trad+|el|συγγραφέας|mf|tr=singraféas}}
                  |* {{T|he}} : {{trad+|he|מחברת|f|dif=מְחַבֶּֽרֶת|tr=m’khaberet}}
                  |* {{T|hi}} : {{trad+|hi|लेखिका|f|tr=lekhikā}}
                  |* {{T|hu}} : {{trad-|hu|szerzőnő}}, {{trad+|hu|írónő}}
                  |* {{T|io}} : {{trad-|io|autorino}}
                  |* {{T|id}} : {{trad-|id|pengarang wanita}}
                  |* {{T|it}} : {{trad+|it|autrice|f}}
                  |* {{T|ja}} : {{trad-|ja|女作者|dif={{Lang|ja|{{ruby|女作者|おんな さくしゃ}}}}|tr=onna sakusha}}
                  |* {{T|la}} : {{trad-|la|editrix|f}}, {{trad-|la|auctrix|f}}
                  |* {{T|lv}} : {{trad-|lv|autore|f}}
                  |* {{T|lt}} : {{trad-|lt|autorė|f}}
                  |* {{T|lb}} : {{trad-|lb|Autorin|f}}
                  |* {{T|mk}} : {{trad-|mk|авторка|f|tr=avtorka}}
                  |* {{T|ms}} : {{trad-|ms|pengarang wanita}}
                  |* {{T|mr}} : {{trad-|mr|लेखिका|f|tr=lekhikā}}
                  |* {{T|nl}} : {{trad+|nl|schrijfster|f}}, {{trad+|nl|autrice|f}}
                  |* {{T|nb}} : {{trad+|nb|forfatterinne|f}}
                  |* {{T|nn}} : {{trad+|nn|forfattarinne|f}}
                  |* {{T|oc}} : {{trad+|oc|autora|f}}
                  |* {{T|ur}} : {{trad-|ur|مصنفہ|f|dif=مُصنِّفہ|tr=muṣannifa}}
                  |* {{T|fa}} : {{trad-|fa|خانم نویسنده|tr=ḫānom-e nevisande}}
                  |* {{T|pl}} : {{trad+|pl|autorka|f}}, {{trad+|pl|pisarka|f}}
                  |* {{T|pt}} : {{trad-|pt|autora|f}}
                  |* {{T|rm}} : {{trad--|rm|autura|f}}
                  |* {{T|ro}} : {{trad+|ro|autoare|f}}
                  |* {{T|ru}} : {{trad+|ru|авторша|f|tr=avtorša}}, {{trad+|ru|писательница|f|tr=pisátelʼnica}}
                  |* {{T|sh}} : {{trad+|sh|autorica}}, {{trad+|sh|književnica}}
                  |* {{T|sk}} : {{trad-|sk|autorka|f}}
                  |* {{T|sl}} : {{trad-|sl|avtorica|f}}
                  |* {{T|sv}} : {{trad+|sv|författarinna|c}}
                  |* {{T|ta}} : {{trad-|ta|நுலாசிரியை|tr=nūl-āciriyai}}
                  |* {{T|cs}} : {{trad+|cs|autorka|f}}
                  |* {{T|te}} : {{trad-|te|కారకురాలు|f|tr=kārakurālu}}
                  |* {{T|th}} : {{trad-|th|ผู้เขียน|tr=p̄hū̂ k̄heīyn}}
                  |* {{T|tr}} : {{trad-|tr|kadın yazar}}
                  |* {{T|uk}} : {{trad+|uk|авторка|f|tr=avtorka}}
                  |* {{T|vi}} : {{trad-|vi|nữ tác giả}}
                  |{{trad-fin}}
                  |
                  |{{trad-début|Génitrice|3}}
                  |* {{T|en}} : {{trad+|en|genitrix}}
                  |* {{T|ca}} : {{trad+|ca|progenitora|f}}
                  |* {{T|es}} : {{trad+|es|progenitora|f}}
                  |* {{T|eo}} : {{trad-|eo|naskantino}}
                  |* {{T|el}} : {{trad+|el|γενέτειρα|f|tr=yenétira}}
                  |* {{T|grc}} : {{trad--|grc|γενέτειρα|f|tr=genéteira}}
                  |* {{T|it}} : {{trad+|it|genitrice|f}}
                  |* {{T|pt}} : {{trad+|pt|progenitora|f}}
                  |{{trad-fin}}
                  |
                  |=== {{S|prononciation}} ===
                  |* {{pron-rimes|o.tʁis|fr}}
                  |* {{écouter|France (Grenoble)|o.t̪ʁ̥is|lang=fr|audio=LL-Q150 (fra)-Opsylac-Autrice.wav}}
                  |* {{écouter|France (Paris)|oː.t̪ʁ̥is|lang=fr|audio=LL-Q150 (fra)-Avatea-autrice.wav}}
                  |* {{écouter|France (Toulouse)|ˈo.t̪ʁ̥i.sə|lang=fr|audio=LL-Q150 (fra)-Lepticed7-autrice.wav}}
                  |* {{écouter|France (Vosges)|o.t̪ʁ̥is|lang=fr|audio=LL-Q150 (fra)-Poslovitch-autrice.wav}}
                  |
                  |==== {{S|paronymes}} ====
                  |<!-- {{(}} -->
                  |* {{lien|actrice|fr}}
                  |* {{lien|fautrice|fr}}
                  |* {{lien|tutrice|fr}}
                  |<!-- {{)}} -->
                  |
                  |=== {{S|anagrammes}} ===
                  |<!-- {{(}} -->
                  |* {{lien|cuitera|fr}}
                  |* {{lien|curetai|fr}}
                  |* {{lien|curiate|fr}}
                  |* {{lien|cuterai|fr}}
                  |* {{lien|écurait|fr}}
                  |* {{lien|éructai|fr}}
                  |* {{lien|raucité|fr}}
                  |* {{lien|recuita|fr}}
                  |<!-- {{)}} -->
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{Wikipédia|Auteur|lang=fr}}
                  |* {{Wikipédia|Autrices et auteurs de Suisse|lang=fr}}
                  |* {{Wikisource|Catégorie:Autrices|lang=fr}}
                  |* {{Wikipédia|Catégorie:Femme de lettres|lang=fr}}
                  |* {{Wikiquote|Catégorie:Femme de lettres|lang=fr}}
                  |* {{Commons|Category:Female writers}}
                  |
                  |=== {{S|références}} ===
                  |==== {{S|sources}} ====
                  |<references>
                  |* {{R|AF-2019|{{lien web|auteur=L’Académie française|titre=La féminisation des noms de métiers et de fonctions|date=2019|url=http://www.academie-francaise.fr/sites/academie-francaise.fr/files/rapport_feminisation_noms_de_metier_et_de_fonction.pdf}}}}
                  |* {{R|Leseur-1477|{{ouvrage|auteur=Guillaume Leseur|titre=Histoire de Gaston IV, comte de Foix|sous-titre=Chronique française inédite du {{siècle2|15}} siècle|tome=II, 1477-1478|éditeur=Henri Courteault|date=1896|lieu=Paris – Renouard|passage=274|url=https://archive.org/details/histoiredegasto00courgoog/}}}}
                  |* {{R|Delorme-1927|''Lettres de Louis XII portant approbation de la règle et monastère de l’Annonciade'', décembre 1503, citées dans {{périodique|auteur=Ferdinand Delorme|titre=Documents pour l’histoire du Bienheureux Gabriel-Maria|journal=La France franciscaine : mélanges d’archéologie, d’histoire et de littérature|date=janvier 1927|pages=220|url=https://gallica.bnf.fr/ark:/12148/bpt6k55124541/f224}}}}
                  |* {{R|Evain-2019|{{ouvrage|prénom=Aurore|nom=Évain|titre=En compagnie. Histoire d’autrice de l’époque latine à nos jours|éditeur={{w|Éditions iXe|iXe}}|date=2019|passage=10-16|isbn=979-10-90062-47-4}}}}
                  |* {{R|Feo-2018|{{périodique|auteur=Agnès de Féo|titre=Pourquoi on n’a aucun mal à dire coiffeuse et beaucoup plus à dire professeuse|journal={{w|Slate (magazine)|Slate}}|date=1{{er}} février 2018|url=https://www.slate.fr/story/156221/feminisation-metiers-pouvoir}}}}
                  |* {{R|Laborderie-2015|{{ouvrage|auteur=Noëlle Laborderie|titre=Précis de phonétique historique|lieu=Paris|éditeur={{w|Armand Colin}}|date=2015|numéro d'édition=2|passage=86|isbn=978-2-200-60182-9}}.}}
                  |* {{R|Lecaplain-2019|{{périodique|auteur=Guillaume Lecaplain|titre=“Auteure” ou “autrice” ? “On ne trouve plus ces termes choquants comme dans les années 80”|journal={{w|Libération (journal)|Libération}}|date=20 février 2019|url=https://www.liberation.fr/france/2019/02/20/auteure-ou-autrice-on-ne-trouve-plus-ces-termes-choquants-comme-dans-les-annees-80_1710520}}}}
                  |* {{R|Lorenzo-2019|{{périodique|auteur=Sandra Lorenzo|titre=Autrice, le féminin qui gênait tant l’Académie française est tout sauf un néologisme|journal={{w|Le HuffPost}}|date=28 février 2019|url=https://www.huffingtonpost.fr/2019/02/28/autrice-le-feminin-qui-genait-tant-lacademie-francaise-est-tout-sauf-un-neologisme_a_23680379/}}}}
                  |* {{R|Delvaux-Melancon-2019|{{périodique|prénom1=Martine|nom1=Delvaux|prénom2=Benoit|nom2=Melançon|titre=Pour ou contre le mot “autrice” ?|journal={{w|Société Radio-Canada|Radio-Canada}}|date=21 aout 2019|url=https://ici.radio-canada.ca/premiere/emissions/plus-on-est-de-fous-plus-on-lit/segments/entrevue/129118/autrice-auteure-definition-histoire-dictionnaire}}}}
                  |</references>
                  |
                  |==== {{S|bibliographie}} ====
                  |* {{ouvrage|prénom=Bernard|nom=Cerquiglini|titre=Femme, j’écris ton nom – guide d’aide à la féminisation des noms de métiers, titres, grades et fonctions|éditeur={{w|Ministère de la Culture (France)|Ministère de la culture}}, Direction générale de la langue française|date=1999|passage=17, 25, 67|url=https://www.ladocumentationfrancaise.fr/var/storage/rapports-publics/994001174.pdf}}
                  |* {{périodique|prénom1=Jean-Pierre|nom1=Chambon|lien auteur1=Jean-Pierre Chambon (linguiste)|prénom2=Beatrice|nom2=Schmid|titre=Compte-rendu du LRL 5/2|journal=Revue de linguistique romane (RLiR)|no=56|date=1992|pages=200-206}}
                  |* {{périodique|prénom=Aurore|nom=Évain|titre=Histoire d’autrice, de l’époque latine à nos jours|journal=Sêméion : travaux de sémiologie|no=6|date=février 2008|url=http://siefar.org/wp-content/uploads/2015/09/Histoire-dautrice-A_-Evain.pdf}}
                  |* {{ouvrage|auteur=Audrey Lasserre|titre=La disparition : enquête sur la « féminisation » des termes auteur et écrivain|directeur=Johan Faerber, Mathilde Barraband, Aurélien Pigeat|titre volume=Le mot juste : des mots à l’essai aux mots à l’œuvre|lieu=Paris|éditeur={{w|Presses Sorbonne Nouvelle}}|date=2006|passage=51-68|url=https://books.openedition.org/psn/1296}}
                  |* {{ouvrage|prénom=Marina|nom=Yaguello|titre=Le sexe des mots|lieu=Paris|éditeur={{w|Belfond}}|date=1995|passage=31-33|url={{Citation/Books.Google|cieKQgAACAAJ|PA31|surligne=autrice}}}}
                  |* {{lien web|titre=autrice|site={{w|Office québécois de la langue française}}|url=http://bdl.oqlf.gouv.qc.ca/bdl/gabarit_bdl.asp?N=2&id=5469}}
                  |* {{R:GDT|fiche=2087968}}
                  |* {{R:Index-FEW|25|825|mot=auctrix|ref=oui}}
                  |* « autrice », dans {{R:Larousse1898}}, tome&nbsp;1, [https://archive.org/details/nouveaularoussei01laro/page/604 p.&nbsp;604]
                  |* {{R:Boiste|auteur}}
                  |* « auteur », dans {{R:Guérin}}, tome&nbsp;1, [https://gallica.bnf.fr/ark:/12148/bpt6k201375w/f728 p.&nbsp;692-693]
                  |* « autrice », dans {{R:Bescherelle}}, tome&nbsp;I, [https://fr.wikisource.org/wiki/Dictionnaire_national_ou_Dictionnaire_universel_de_la_langue_fran%C3%A7aise/Autrice p.&nbsp;301]
                  |* {{R:BHVF}}
                  |* {{R:DIF:1:1660|autrice|111|111}}
                  |* {{R:Féraud}}
                  |* « autrice », dans {{ouvrage|prénom=Pierre-François Guyot|nom=Desfontaines|titre=Dictionaire Néologique à l’usage des beaux esprits du siécle|date=1727|passage=16|url={{Citation/Books.Google|xQotAAAAYAAJ|PA16|surligne=autrice}}}}
                  |* « autrice », dans {{R:LarousseXIXe}}, tome&nbsp;1, [https://fr.wikisource.org/wiki/Grand_dictionnaire_universel_du_XIXe_si%C3%A8cle/AUTRICE p.&nbsp;1014]
                  |* « autrice », dans {{ouvrage|auteur=Adolphe Peschier|titre=Supplément au dictionnaire complet des langues française et allemande de l’abbé Mozin|lieu=Stuttgart|éditeur=J. G. Cotta|date=1859|passage=29|url={{Citation/Books.Google|cJkGAAAAQAAJ|PA29|surligne=autrice}}}}
                  |* « autrice », dans {{ouvrage|prénom=Charles|nom=de Pougens|titre=Archéologie française ou vocabulaire de mots anciens, tombés en désuétude, et propres à être restitués au langage moderne|tome=1|lieu=Paris|éditeur=Desoer|date=1821|passage=42|url={{Citation/Books.Google|UZQPAAAAQAAJ|RA2-PA42|surligne=autrice}}}}
                  |* « autrice », dans {{ouvrage|auteur={{w|Louis Remacle (philologue)|Louis Remacle}}|titre=Notaires de Malmedy, Spa et Verviers. Documents lexicaux|lieu=Paris|éditeur={{w|Les Belles Lettres}}|date=1977|passage=45|url={{Citation/Books.Google|1fHZNIHfSg8C|PA45|surligne=autrice}}}}
                  |* {{R:Trévoux}}
                  |* {{R:Trévoux|PEINTRE}}
                  |* « autrice », dans {{ouvrage|auteur=Otto Winkelman|titre=Dictionnaire françois-hollandois|lieu=Utrecht|éditeur=Barthelemy Wild|date=1783|passage=81|url={{Citation/Books.Google|OVsUAAAAQAAJ|PA81|surligne=autrice}}}}
                  |
                  |{{Très bonne entrée|français}}
                  |
                  |[[Catégorie:Mots en français suffixés avec -rice]]
                  |
                  |== {{langue|frm}} ==
                  |=== {{S|étymologie}} ===
                  |: {{date|1477-1478|lang=frm}} Attesté d’abord sous la forme ''{{lien|actrixe|frm}}''{{R|Leseur-1477}}, ensuite {{date|1503|lang=frm}} ''{{lien|auctrixe|frm}}''{{R|Delorme-1927}}, puis {{date|1524|lang=frm}} sous la forme ''{{lien|auctrice|frm}}'' dans une lettre de {{w|Guillaume Briçonnet (1470-1534)|Guillaume Briçonnet}} à {{w|Marguerite de Valois-Angoulême}}. Puis sous le pluriel ''{{lien|autrices|frm}}'' en 1554 dans ''La Claire'', de {{w|Louis Le Caron}}. Du {{étyl|la|fr|mot=auctrix|sens=agente, autrice, fondatrice, {{lien|instigatrice|fr}} », « {{lien|conseillère|fr}} », en droit « garante d’une vente}}{{R|Evain-2019|r=1}}.
                  |
                  |=== {{S|nom|frm}} ===
                  |{{frm-rég|o.tris(ə)}}
                  |'''autrice''' {{prononciation|o.tris(ə)|lang=frm}} {{f}} {{équiv-pour|un homme|auteur|lang=frm}}
                  |# {{lien|femme|fr|dif=Femme}} à l’{{lien|origine|fr}} de {{lien|quelque chose|fr}}.
                  |#* {{exemple
                  ||Car tous les maux d’Etolie, la calamité des Calidoniens, l’occiſion de tant de gens, voire la mort de Meleager : ils diſent que tout cela vient de Diane, qui en fut l’'''autrice''', s’eſtant fachee de ce qu’Oené ne l’auoit point inuitee en ſon ſacrifice : tant profondement s’eſtoit en ſon eſprit engrauee la faute commiſe en ceſte feſte.
                  ||sens=Car tous les maux d’Étolie, la calamité des Calédoniens, l’occision de tant de gens, voire la mort de Méléagre : ils disent que tout cela vient de Diane, qui en fut l’'''autrice''', s’étant fâchée de ce qu’Œnoé ne l’avait point invitée en son sacrifice : tant profondément s’était en son esprit engravée la faute commise en cette fête.
                  ||source={{ouvrage|prénom=Lucien|nom=de Samosate|trad=Filbert Bretin|titre=Les œuures de Lucian de Samoſate, philoſophe excellent, non moins vtiles que plaiſantes|lieu=Paris|éditeur={{w|Abel L'Angelier|Abel L’Angelier}}|date=1583|passage=136|url=https://gallica.bnf.fr/ark:/12148/bpt6k123117s/f158}}
                  ||lang=frm
                  |}}
                  |#* {{exemple
                  ||On l’a fort accuſé du maſſacre de Paris, ce ſont lettres cloiſes, pour quant à cela, car alors j’eſtois à noſtre embarquement de Broüage, mais j’ay bien ouy dire qu’elle n’en fut la premiere '''autrice'''.
                  ||sens=On l’a fort accusé du massacre de Paris, ce sont lettres closes, pour quant à cela, car alors j’étais à notre embarquement de Brouage, mais j’ai bien ouï dire qu’elle n’en fut la première '''autrice'''.
                  ||source={{ouvrage|auteur=Brantôme|lien auteur=Brantôme (écrivain)|titre=Memoires de Meſſire Pierre du Bourdeille, Seigneur de Brantome, contenant Les Vies des Dames Illuſtres de France de ſon temps|lieu=Leyde|éditeur=Jean Sambix le jeune|date=1699|passage=65|url={{Citation/Books.Google|VnzSK1AASlEC|PA65|surligne=autrice}}}}
                  ||lang=frm
                  |}}
                  |# {{lexique|littérature|frm}} {{lien|femme|fr|dif=Femme}} qui a rédigé un {{lien|ouvrage|fr}} de {{lien|littérature|fr}}, de {{lien|science|fr}} ou d’{{lien|art|fr}}.
                  |#* {{exemple
                  ||Quelqu’vn vous dira telles Dames n’eſtre '''autrices''' des conſtitutions, qui ſouz leur nom ſont publiées, ains leur conſeillers.
                  ||sens=Quelqu’un vous dira telles dames n’être '''autrices''' des constitutions [lois], qui sous leur nom sont publiées, ains [mais] leur conseillers.
                  ||source={{ouvrage|prénom=Louis|nom=Le Caron|titre=La Claire, ov De la prudence de droit, dialogue premier. Plus, La clarté amoureuſe|lieu=Paris|éditeur=Guillaume Cavellat|date=1554|passage=17b|url={{Citation/Books.Google|YKJXAAAAcAAJ|PA17-IA1|surligne=autrices}}}}
                  ||lang=frm
                  |}}
                  |
                  |==== {{S|variantes}} ====
                  |<!-- {{(}} -->
                  |* {{lien|actrixe|frm}} {{moins courant}}
                  |* {{lien|auctrice|frm}}
                  |* {{lien|auctrixe|frm}} {{moins courant}}
                  |<!-- {{)}} -->
                  |
                  |==== {{S|variantes orthographiques}} ====
                  |* {{lien|authrice|frm}}
                  |
                  |==== {{S|synonymes}} ====
                  |* ''Celle qui est à l’origine de quelque chose'' :
                  |** {{lien|accroisseresse|frm}}
                  |** {{lien|augmenteresse|frm}}
                  |* ''Celle qui a rédigé un ouvrage :''
                  |** {{lien|acteur|frm}}
                  |
                  |=== {{S|références}} ===
                  |==== {{S|sources}} ====
                  |<references>
                  |* {{R|Leseur-1477|{{ouvrage|auteur=Guillaume Leseur|titre=Histoire de Gaston IV, comte de Foix|sous-titre=Chronique française inédite du {{siècle2|15}} siècle|tome=II, 1477-1478|éditeur=Henri Courteault|date=1896|lieu=Paris – Renouard|passage=274|url=https://archive.org/details/histoiredegasto00courgoog/}}}}
                  |* {{R|Delorme-1927|''Lettres de Louis XII portant approbation de la règle et monastère de l’Annonciade'', décembre 1503, citées dans {{périodique|auteur=Ferdinand Delorme|titre=Documents pour l’histoire du Bienheureux Gabriel-Maria|journal=La France franciscaine : mélanges d’archéologie, d’histoire et de littérature|date=janvier 1927|pages=220|url=https://gallica.bnf.fr/ark:/12148/bpt6k55124541/f224}}}}
                  |* {{R|Evain-2019|{{ouvrage|prénom=Aurore|nom=Évain|titre=En compagnie. Histoire d’autrice de l’époque latine à nos jours|éditeur={{w|Éditions iXe|iXe}}|date=2019|passage=10-16|isbn=979-10-90062-47-4}}}}
                  |</references>
                  |
                  |==== {{S|bibliographie}} ====
                  |* {{R:DMF}}
                  |* {{R:Index-FEW|25|825|mot=auctrix|ref=oui}}
                  |* {{ouvrage|prénom=Edmond|nom=Huguet|titre=Dictionnaire de la langue française du seizième siècle|tome=I|lieu=Paris|éditeur=E. Champion|date=1925|passage=412|url=https://fr.wikisource.org/wiki/Dictionnaire_de_la_langue_fran%C3%A7aise_du_seizi%C3%A8me_si%C3%A8cle/Autheur}}
                  |
                  |[[Catégorie:Mots en moyen français suffixés avec -rice]]
                  |
                  |== {{langue|it}} ==
                  |=== {{S|étymologie}} ===
                  |* {{date|1385|lang=it}}{{R|Tesoro-lingua}} Dérivé du {{étyl|la|it|mot=auctrix|sens=agente, autrice, fondatrice, {{lien|instigatrice|fr}} », « {{lien|conseillère|fr}} », en droit « garante d’une vente}}<ref>{{ouvrage|titre=Lessico etimologico italiano|tome=3|passage=2237.49, auctor|url=https://www.lei-digitale.org/}}</ref>.
                  |
                  |==== {{S|attestations}} ====
                  |* {{siècle|XIV|lang=it}} {{Lang|it|''E di questo consiglio fu '''autrice''' e principale una ch’avea nome Polisso''}} {{source|{{ouvrage|prénom=Francesco|nom= di Bartolo|titre=Commento di Francesco da Buti sopra la Divina Commedia di Dante Allighieri pubblicato per cura di Crescentino Giannini|tome=I|éditeur=Fratelli Nistri|date=1858|année première édition=1385|lieu=Pise|passage=483|url={{Citation/Books.Google|YA5wfBuoCC8C|PA483|surligne=autrice}}}}}}
                  |*: Et l’autrice de ce conseil était celle qui se nommait Polisso.
                  |
                  |=== {{S|nom|it}} ===
                  |{{it-flexion|au.ˈtri.t͡ʃ}}
                  |[[Fichier:Casa de Venus Pompeya 01.jpg|vignette|{{w|Vénus (mythologie)|Vénus}}, '''autrice''' légendaire de la {{w|Iulii|''gens'' Iulia}} ''({{nobr|sens 3}})''|alt=Fresque de Vénus anadyomène dans la maison de Vénus à Pompéi]]
                  |'''autrice''' {{prononciation|au.ˈtri.t͡ʃe|lang=it}} {{f}} {{équiv-pour|un homme|autore|lang=it}}
                  |# {{lien|autrice|fr|dif=Autrice}}, {{lien|femme|fr}} ou figure féminine à l’{{lien|origine|fr}} de {{lien|quelque chose|fr}}.
                  |#* {{exemple
                  ||E per certo, essendo ornata della bellezza di colei, non è presuntuosa volendo vincere di splendore l’altre stelle, ma ancora potrebbe contendere con Febo e domandargli il suo carro, per essere lei '''autrice''' del giorno.
                  ||sens=Et certainement, étant parée de sa beauté, elle n’a pas la présomption de vouloir l’emporter sur la splendeur des autres étoiles, mais elle pourrait encore lutter avec Phébus et lui demander son char, pour être l’autrice du jour.
                  ||source={{ouvrage|prénom=Laurent|nom=de Médicis|titre={{Lang|it|Comento del magnifico Lorenzo de’ Medici sopra alcuni de’ suoi sonetti}}|tome=1|éditeur=Giuseppe Laterza & Figli|lieu=Bari|date=1913|année première édition=1480|passage=27|url=https://it.wikisource.org/wiki/Opere_(Lorenzo_de%27_Medici)/II._Comento_del_magnifico_Lorenzo_de%27_Medici_sopra_alcuni_de%27_suoi_sonetti}}
                  ||lang=it
                  |}}
                  |#* {{exemple
                  ||tu vedi, Platone, quanto o la natura o il fato o la necessitá, o qual si sia potenza '''autrice''' e signora dell’universo, è stata ed è perpetuamente inimica alla nostra specie.
                  ||sens=Tu vois, Platon, quelle ennemie éternelle notre espèce a toujours trouvée dans la nature, ou dans la nécessité, ou dans la destinée, ou dans la puissance, quelle qu’elle soit, autrice et maîtresse de l’univers.
                  ||source={{ouvrage|prénom=Giacomo|nom=Leopardi|titre={{Lang|it|Operette morali}}|éditeur=Laterza & Figli|lieu=Bari|date=1928|année première édition=1826|passage=193|url=https://it.wikisource.org/wiki/Operette_morali_(Leopardi_-_Donati)/Dialogo_di_Plotino_e_di_Porfirio}}
                  ||lang=it
                  |}}
                  |# {{lexique|littérature|it}} {{lien|autrice|fr|dif=Autrice}}, femme qui a créé une œuvre littéraire, artistique ou scientifique. {{absolument}} {{lien|écrivaine|fr|dif=Écrivaine}}.
                  |#* {{exemple
                  ||Chi compoſe poemi, e chi ſonetti :<br/>Chi di metri novelli fu l’'''autrice'''
                  ||sens=L’une a écrit des poèmes et des sonnets<br/>L’autre était l’autrice de vers nouveaux
                  ||source={{ouvrage|prénom=Giovanni Battista|nom=Fagiuoli|titre={{Lang|it|Rime piacevoli}}|tome=II|éditeur=Salvatore e Gian-Domen Marescandoli|lieu=Lucques|date=1733|année première édition=1730|passage=176|url={{Citation/Books.Google|aNYOv1xmhMsC|PA176|surligne=autrice}}}}
                  ||lang=it
                  |}}
                  |#*:
                  |#* {{exemple
                  ||E la nostra squisita poetessa, la gentile Olinda Mezzofanti Sassabetti, l’'''autrice''' di ''Casti palpiti'', avrà letto.
                  ||sens=Et notre exquise poétesse, l’aimable Odolinda Mezzofanti Sassabetti, l’autrice de ''Chastes palpitations'', que vous avez dû lire, bien sûr.
                  ||source={{ouvrage|prénom=Umberto|nom=Eco|titre={{Lang|it|Il pendolo di Foucault}}|éditeur={{w|Giunti Editore}}|lieu=Florence|date=2013|année première édition=1988|passage=323|url={{Citation/Books.Google|jGWgDQAAQBAJ|PT323|surligne=autrice}}}}
                  ||lang=it
                  |}}
                  |# {{lien|génitrice|fr|dif=Génitrice}} ; {{lien|ascendante|fr}}.
                  |#* {{exemple
                  ||Sì stupenda pittura dedicò Augusto nel tempio di Giulio Cesare, consacrando al padre l’origine e l’'''autrice''' di Casa Giulia.
                  ||sens=Un magnifique tableau dédié à Auguste dans le temple de Jules César, consacrant à son père l’origine et l’autrice de la gens Iulia [Vénus].
                  ||source={{ouvrage|prénom=Carlo Roberto|nom=Dati|titre={{Lang|it|Vite de’ pittori antichi}}|éditeur=Società Tipografica de’ Classici Italiani|lieu=Milan|date=1806|année première édition=1667|passage=168|url={{Citation/Books.Google|WeTGth65fVUC|PA168|surligne=autrice}}}}
                  ||lang=it
                  |}}
                  |
                  |==== {{S|synonymes}} ====
                  |{{(}}
                  |* {{lien|creatrice|it|sens={{lien|créatrice|fr}}}}
                  |* {{lien|inventrice|it|nom}}
                  |* {{lien|promotrice|it|nom}}
                  |* {{lien|responsabile|it|sens={{lien|responsable|fr}}}}
                  |{{)}}
                  |
                  |==== {{S|dérivés}} ====
                  |<!-- {{(}} -->
                  |* {{lien|cantautrice|it}}
                  |* {{lien|coautrice|it}}
                  |* {{lien|grande autrice|it}}
                  |<!-- {{)}} -->
                  |
                  |==== {{S|hyperonymes}} ====
                  |''Celle qui a créé une œuvre :''
                  |<!-- {{(}} -->
                  |* {{lien|artista|it|sens={{lien|artiste|fr}}}}
                  |<!-- {{)}} -->
                  |
                  |==== {{S|hyponymes}} ====
                  |{{(|''Celle qui a créé une œuvre :''}}
                  |* {{lien|architetta|it|sens={{lien|architecte|fr}}}}
                  |* {{lien|pittrice|it|sens={{lien|peintre|fr}}, {{lien|peintresse|fr}}}}
                  |* {{lien|poetessa|it|sens={{lien|poétesse|fr}}}}
                  |* {{lien|scrittrice|it|sens={{lien|écrivaine|fr}}}}
                  |* {{lien|scultrice|it|sens={{lien|sculptrice|fr}}}}
                  |{{)}}
                  |
                  |=== {{S|prononciation}} ===
                  |==== {{S|paronymes}} ====
                  |<!-- {{(}} -->
                  |* {{lien|altrice|it}}
                  |* {{lien|attrice|it}}
                  |* {{lien|fautrice|it}}
                  |* {{lien|nutrice|it}}
                  |* {{lien|tutrice|it}}
                  |<!-- {{)}} -->
                  |
                  |=== {{S|anagrammes}} ===
                  |<!-- {{(}} -->
                  |* {{lien|cauteri|it}}
                  |* {{lien|curiate|it}}
                  |* {{lien|reciuta|it}}
                  |* {{lien|rutacei|it}}
                  |<!-- {{)}} -->
                  |
                  |=== {{S|références}} ===
                  |==== {{S|sources}} ====
                  |<references>
                  |* {{R|Tesoro-lingua|{{lien web|titre=autrice|site=Tesoro della lingua italiana delle origini|éditeur=Opera del vocabolario italiano|url=http://tlio.ovi.cnr.it/TLIO/index.php?vox=015299.htm}}.}}
                  |</references>
                  |
                  |==== {{S|bibliographie}} ====
                  |* {{ouvrage|prénom=Salvatore|nom=Battaglia|titre=Grande dizionario della lingua italiana|lieu=Turin|date=1962|tome=1|passage=859|url=http://www.gdli.it/sala-lettura/vol/1?seq=868}}
                  |* {{R:Costèro - Lefebvre 1878|pages=italien-français page 58}}
                  |* {{R:SDAS}}
                  |* {{ouvrage|titre=Vocabolario degli accademici della Crusca|lieu=Venice|éditeur=Sarzina|date=1623|passage=98|url=http://www.lessicografia.it/Controller?lemma=AUTRICE_ed2}}
                  |* {{lien web|titre=autore|site=Dizionario Italiano Olivetti|éditeur=Olivetti Media Communication|url=https://www.dizionario-italiano.it/dizionario-italiano.php?lemma=AUTORE100}}
                  |* {{lien web|titre=autore|site=Garzanti Linguistica|url=https://www.garzantilinguistica.it/ricerca/?q=autore}}
                  |* {{lien web|prénom1=Francesco|nom1=Sabatini|prénom2=Vittorio|nom2=Coletti|titre=autore|site=Il Sabatini Coletti|éditeur=RCS Mediagroup|url=https://dizionari.corriere.it/dizionario_italiano/A/autore.shtml}}
                  |* {{lien web|titre=autore|site=Vocabolario Treccani|éditeur=Istituto dell’Enciclopedia Italiana|url=https://www.treccani.it/vocabolario/autore/}}
                  |* {{R:DIF:1:1660|autrice|111|idem}}
                  |* « autrice », dans {{R:Oudin}}, p. 50
                  |
                  |[[Catégorie:Mots en italien suffixés avec -rice]]
                  |
                  |== {{langue|nl}} ==
                  |=== {{S|étymologie}} ===
                  |: {{siècle|XVII|lang=nl}} Du {{étyl|fr|nl|autrice}}.
                  |
                  |==== {{S|attestations}} ====
                  |* {{siècle|XVII|lang=nl}} {{Lang|nl|''Sulckx datter ſelfs noch twyffel is geweeſt over de macht der voorſchreve Princeſſe Iſabella naer de doodt van haeren man den Aerts-Hertogh Albertus, niet tegenſtaende dat ſy in ſyn leven beneffens hem hadde gehadt de abſolute Souvereyniteyt van deſe Nederlanden, ende beneffens hem ſelfs was geweeſt de '''Autrice''' van het voorſz. eeuwigh Edict.''}} {{source|{{ouvrage|titre=Procès au sujet de la succession Jacques de Berges|date=1623|url={{Citation/Books.Google|ld9FAAAAcAAJ|RA2-PA10|surligne=Autrice}}}}}}.
                  |
                  |=== {{S|nom|nl}} ===
                  |{{nl-nom-s|t}}
                  |[[Fichier:George Charles Beresford - Virginia Woolf in 1902 - Restoration.jpg|vignette|redresse|L’'''autrice''' {{w|Virginia Woolf}}|alt=Portrait photographique de Virginia Woolf en 1902 par George Charles Beresford]]
                  |'''autrice''' {{prononciation|ɑuˈtri.sə|lang=nl}} {{f}} {{équiv-pour|un homme|auteur|lang=nl}}
                  |# {{lexique|littérature|nl}} {{rare|nl}} {{lien|autrice|fr|dif=Autrice}}, femme qui a créé une œuvre littéraire. {{absolument}} {{lien|écrivaine|fr|dif=Écrivaine}}.
                  |#* {{exemple
                  ||De op het oogenblik meest gelezen Engelsche schrijfster Virginia Woolf, '''autrice''' van den roman „de Vrouw van vijftig jaar” heeft een geschrift gepubliceerd naar aanleiding van een kort bezoek aan een Engelsche universiteitstad — zij noemt deze Oxbridge — en waarin zij een vergelijk maakt tusschen het weeldeleven der mannelijke studenten en de vaak armelijke omgeving waarin de vrouwelijke studenten leven.
                  ||sens=L’écrivaine anglaise la plus lue actuellement, Virginia Woolf, autrice du roman ''La femme de cinquante ans'', a publié un article à l’occasion d'une brève visite dans une ville universitaire anglaise — elle l’appelle Oxbridge — dans lequel elle fait une comparaison entre la vie luxueuse des étudiants masculins et l’environnement souvent pauvre dans lequel vivent les étudiantes.
                  ||source={{périodique|titre=De dichteres op het dakkamertje: waarom zijn de vrouwen onproductief?|journal=Nieuwe Apeldoornsche courant|date=15 mai 1930|url=https://www.kb.nl/historische-kalender/2019/15-mei-1930-de-dichteres-op-het-dakkamertje-waarom-zijn-de-vrouwen-onproductief}}
                  ||lang=nl
                  |}}
                  |#* {{exemple
                  ||Ze noemen namelijk een schrijfster een '''''autrice''''' en een leidsvrouw een ''mentrix''.
                  ||sens=Ils appellent une écrivaine une ''autrice'' et une enseignante une ''mentrix''.
                  ||source={{périodique|titre=Aanstelleritis|journal={{w|Genootschap Onze Taal|Onze Taal}}|date=1971|volume=40|url=https://www.dbnl.org/tekst/_taa014197101_01/_taa014197101_01.pdf}}
                  ||lang=nl
                  |}}
                  |#* {{exemple
                  ||Volgens een wat nuffige conventie wordt de leeftijd van de '''autrice''' van deze roman, Anne Landsman, niet genoemd; wel mogen we weten dat ze in Kaapstad studeerde en in New York lesgeeft in scenarioschrijven.
                  ||sens=Selon une convention quelque peu prude, l’âge de l’autrice de ce roman, Anne Landsman, n’est pas mentionné ; il faut savoir qu’elle a étudié au Cap et enseigne la scénarisation à New York.
                  ||source={{périodique|auteur=Jacq Vogelaar|titre=Roman van een mislukking|journal={{w|De Groene Amsterdammer}}|date=20 janvier 1999||url=https://www.groene.nl/artikel/roman-van-een-mislukking}}
                  ||lang=nl
                  |}}
                  |#* {{exemple
                  ||Na de drukbezochte en veel gewaardeerde tentoonstellingen ‘Wild in de schuur’, ‘Jan Strube & Jan Tankink’ en ‘Boer zoekt Kunst’ tonen we nu schitterende kunstwerken van twaalf kunstenaressen en een '''autrice'''.
                  ||sens=Après les expositions très fréquentées et très appréciées ''Wild in de schuur'', ''Jan Strube & Jan Tankink'' et ''Boer zoekt Kunst'', nous montrons maintenant de belles œuvres d’art de douze artistes et d’une autrice.
                  ||source={{périodique|titre=Kunsten Expositie met als titel Sterke Vrouwen|journal=Bredavandaag|date=29 aout 2020|url=https://www.bredavandaag.nl/activiteit/item/319375/kunsten-expositie-met-als-titel-sterke-vrouwen-}}
                  ||lang=nl
                  |}}
                  |
                  |==== {{S|dérivés}} ====
                  |* {{lien|co-autrice|nl}}
                  |
                  |==== {{S|hyponymes}} ====
                  |* {{lien|schrijfster|nl}}
                  |
                  |=== {{S|prononciation}} ===
                  |* {{écouter|Pays-Bas <!-- précisez svp la ville ou la région -->|ɑuˈtri.sə|lang=nl|audio=Nl-autrice.ogg}}
                  |
                  |==== {{S|paronymes}} ====
                  |* {{lien|actrice|nl}}
                  |
                  |=== {{S|références}} ===
                  |* {{lien web|titre=auteur|site=Woordenboek der Nederlandsche Taal|url=http://gtb.inl.nl/iWDB/search?actie=article&wdb=WNT&id=M004581}}
                  |
                  |[[Catégorie:Artistes en néerlandais]]
                  |[[Catégorie:Mots en néerlandais suffixés avec -rice]]
                  |""".stripMargin
}
