package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Cadeau {
  val resource = Resource("cadeau", "https://fr.wiktionary.org/wiki/", "entry", """== {{langue|fr}} ==
                                                                                       |=== {{S|étymologie}} ===
                                                                                       |: {{date|1416}}{{R|TLFi}} Apparait avec le sens de « lettre [[capitale]] ornée{{R|TLFi}} », en {{étyl|fro|fr|mot=cadel|sens=lettre capitale}}, de l'{{étyl|pro|fr|mot=capdel|sens=capital, capitaine}}{{R|TLFi}} ; du sens de « capitale ornée », il est passé à celui de « chose inutiles » puis à celui de « fête » et enfin le sens qu'on lui connaît.
                                                                                       |: Ou du {{étyl|la|fr|mot=catellus|sens=petite chaine}}{{R|Littré}} diminutif de ''{{lien|catena|la}}'' (« [[chaine]] »), à cause de la forme enchaînée des traits de plume. {{w|Gilles Ménage}} nous apprend{{R|Littré}} que « faire des cadeaux » s'est dit pour « faire des choses spécieuses mais inutiles », comparées métaphoriquement à ces traits de main des maîtres d'écriture. De là on passe sans peine à « cadeau » dans le sens de « [[divertissement]], [[fête]] » et, finalement, « [[présent]] ».
                                                                                       |: {{w|Antoine-Paulin Pihan}} le fait dériver de l’arabe {{polytonique|{{lien|هدية|ar}}|hadiyya|cadeau}}<ref>Antoine-Paulin Pihan, ''[https://archive.org/stream/bub_gb_NhhudmEk0HgC#page/n109/mode/2up Glossaire des mots français tirés de l'arabe, du persan et du turc]'', Paris, 1866, p. 81</ref>, ce qui ne correspond ni phonétiquement, ni sémantiquement au sens ancien de « lettre capitale ».
                                                                                       |
                                                                                       |=== {{S|nom|fr}} ===
                                                                                       |{{fr-rég-x|ka.do}}
                                                                                       |[[Image:Gifts xmas.jpg|thumb|'''Cadeaux''' placés sous un [[sapin de Noël]].]]
                                                                                       |'''cadeau''' {{pron|ka.do|fr}} {{m}}
                                                                                       |# {{vieilli|fr}} [[fête#fr|Fête]] que l’on donnait principalement à des femmes, [[partie]] de [[plaisir]].
                                                                                       |#*''Elles y ont reçu des '''cadeaux''' merveilleux de musique et de danse.'' {{source|{{nom w pc||Molière}}, ''Am. magn''. I, 1}}
                                                                                       |#*''Des promenades du temps,<br>Ou dîners qu’on donne aux champs,<br>Il ne faut point qu’elle essaye<br>Selon les prudents cerveaux,<br>Le mari dans ces '''cadeaux'''<br>Est toujours celui qui paye.'' {{source|{{nom w pc||Molière}}, ''L’École des femmes'', III, 2}}
                                                                                       |# [[présent#fr|Présent]], {{lien|don|fr}}.
                                                                                       |#* ''Je me représentais sa mine lorsqu’elle recevrait mon '''cadeau''', et je me demandais : « Qu’est-ce qu’elle dira ? »'' {{source|{{w|Erckmann-Chatrian}}, ''[[s:Histoire d’un conscrit de 1813/2|Histoire d’un conscrit de 1813]]'', J. Hetzel, 1864}}
                                                                                       |#* ''Je reçus de France un important matériel, un nouveau cinématographe, '''cadeau''' de mon ami Pierre Albaran, et un phonographe envoyé par Jean Borotra, le champion de tennis.'' {{source|{{w|Alain Gerbault}}, ''À la poursuite du soleil''; tome 1 : ''De New-York à Tahiti'', 1929}}
                                                                                       |#* ''Le « gros » l'entretenait parcimonieusement de repas et de '''cadeaux''' mesquins et lui payait sa chambre, au coin de la rue Grange-Batelière.'' {{source|{{Citation/Victor Méric/Les Compagnons de l’Escopette/1930|88}}}}
                                                                                       |#* ''Cheikh Gaafar se montra généreux pour la dot et les '''cadeaux'''. Il fit célébrer de belles fêtes pour le mariage. […]. Le village entier fut en liesse.'' {{source|{{w|Out-el-Kouloub}}, ''Zaheira'', dans "Trois contes de l'Amour et de la Mort", 1940}}
                                                                                       |#* ''J'ai reçu des sous-vêtements, des montres, même des étuis à crayon. Que des choses pratiques. Ce ne sont pas des '''cadeaux''', ça! Les '''cadeaux''', c'est le luxe, l'inattendu, le flafla. Ça ne sert à rien, un '''cadeau''' qui sert à quelque chose.'' {{source|{{nom w pc|David|Goudreault}}, ''La bête à sa mère'', Stanké, 2015, p. 152.}}
                                                                                       |
                                                                                       |==== {{S|synonymes}} ====
                                                                                       |* [[don]]
                                                                                       |* [[offrande]]
                                                                                       |* [[présent]]
                                                                                       |
                                                                                       |==== {{S|dérivés}} ====
                                                                                       |{{(}}
                                                                                       |* [[cadeau Bonux]]
                                                                                       |* [[cadeau-hameçon]]
                                                                                       |* [[cadeau empoisonné]]
                                                                                       |* [[cadeau publicitaire]]
                                                                                       |* {{lien|cadeauter|fr}}, {{lien|cadoter|fr}}
                                                                                       |* [[cadonner]] {{term|Afrique centrale}}
                                                                                       |* [[faire cadeau]]
                                                                                       |* [[papier cadeau]]
                                                                                       |* [[paquet-cadeau]], [[paquet cadeau]]
                                                                                       |{{)}}
                                                                                       |
                                                                                       |==== {{S|vocabulaire}} ====
                                                                                       |* {{voir thésaurus|fr|cadeau}}
                                                                                       |
                                                                                       |==== {{S|traductions}} ====
                                                                                       |{{trad-début|Présent, don|2}}
                                                                                       |* {{T|af}} : {{trad-|af|donasie}}, {{trad-|af|geskenk}}
                                                                                       |* {{T|de}} : {{trad+|de|Geschenk}} {{n}}, {{trad+|de|Präsent}} {{n}}, {{trad+|de|Angebinde}}, {{trad+|de|Gabe}}, {{trad+|de|Spende}}, {{trad+|de|Vermächtnis}}, {{trad+|de|Gift}}
                                                                                       |* {{T|en}} : {{trad+|en|present}}, {{trad+|en|gift}}, {{trad+|en|donation}}
                                                                                       |* {{T|ang}} : {{trad+|ang|gift}}
                                                                                       |* {{T|ar}} : {{trad-|ar|الهدية}}
                                                                                       |* {{T|arz}} : {{trad--|arz|کادو|tr=kâdo}}, {{trad--|arz|هدیه|tr=hediyé}}
                                                                                       |* {{T|br}} : {{trad-|br|prof|m}}
                                                                                       |* {{T|ca}} : {{trad-|ca|regal}}
                                                                                       |* {{T|shy}} : {{trad--|shy|aknwaw}}, {{trad--|shy|afexnin}}
                                                                                       |* {{T|zh}} : {{trad+|zh|礼物}}
                                                                                       |* {{T|ko}} : {{trad+|ko|선물|tr=seonmul|tradi=膳物}}
                                                                                       |* {{T|gcf}} : {{trad+|gcf|kado}}
                                                                                       |* {{T|hr}} : {{trad+|hr|dar}}, {{trad+|hr|poklon}}
                                                                                       |* {{T|da}} : {{trad+|da|gave}}
                                                                                       |* {{T|es}} : {{trad+|es|regalo}}, {{trad+|es|obsequio}}, {{trad+|es|presente}}
                                                                                       |* {{T|eo}} : {{trad-|eo|donaco}}
                                                                                       |* {{T|fo}} : {{trad-|fo|gáva}}
                                                                                       |* {{T|fi}} : {{trad+|fi|lahja}}
                                                                                       |* {{T|fy}} : {{trad-|fy|kado}}, {{trad-|fy|skinking}}
                                                                                       |* {{T|ga}} : {{trad-|ga|bronntanas}}
                                                                                       |* {{T|gl}} : {{trad+|gl|agasallo}}, {{trad-|gl|obsequio}}, {{trad+|gl|regalo}}
                                                                                       |* {{T|el}} : {{trad+|el|δώρο|R=dhóro}} {{n}}
                                                                                       |* {{T|hbo}} : {{trad--|hbo|זבד|dif=זֵבֶד}} {{m}}, {{trad--|hbo|מתן|dif=מַתָן}} {{m}}, {{trad--|hbo|מתנה|dif=מַתָנָה}} {{f}}, {{trad--|hbo|מגדנות|dif=מִגְדָּנות}} {{f}}
                                                                                       |* {{T|io}} : {{trad+|io|donacajo}}
                                                                                       |* {{T|is}} : {{trad+|is|gjöf}}
                                                                                       |* {{T|it}} : {{trad+|it|dono}}, {{trad+|it|presente}}, {{trad+|it|regalo}}
                                                                                       |* {{T|ja}} : {{trad+|ja|プレゼント}}, {{trad-|ja|贈り物|tr=okurimono}}
                                                                                       |* {{T|kk}} : {{trad-|kk|сыйлық|tr=sıylıq}}, {{trad-|kk|сый|tr=sıy}}, {{trad+|kk|тарту|tr=tartuw}}
                                                                                       |* {{T|la}} : {{trad+|la|donum}}
                                                                                       |* {{T|fsl}} : {{wikisign|cadeau}}
                                                                                       |* {{T|ms}} : {{trad-|ms|pemberian}}
                                                                                       |* {{T|yua}} : {{trad--|yua|siibil}}
                                                                                       |* {{T|nl}} : {{trad+|nl|cadeau}}, {{trad+|nl|donatie}}, {{trad+|nl|schenking}}, {{trad+|nl|geschenk}}, {{trad+|nl|gift}}
                                                                                       |* {{T|no}} : {{trad+|no|gave}}, {{trad+|no|presang}}
                                                                                       |* {{T|oc}} : {{trad-|oc|present}}, {{trad+|oc|dona}}
                                                                                       |* {{T|uz}} : {{trad-|uz|sovg‘a}}
                                                                                       |* {{T|pap}} : {{trad--|pap|opsekio}}, {{trad--|pap|regaldo}}
                                                                                       |* {{T|fa}} : {{trad+|fa|کادو}}, {{trad+|fa|پیشکش|tr=piš-kaš}}, {{trad+|fa|هدیه|tr=hadiyé}}
                                                                                       |* {{T|pl}} : {{trad+|pl|podarunek}}
                                                                                       |* {{T|pt}} : {{trad+|pt|brinde}}, {{trad+|pt|dádiva}}, {{trad+|pt|mimo}}, {{trad+|pt|presente}}
                                                                                       |* {{T|ro}} : {{trad+|ro|cadou|m}}
                                                                                       |* {{T|ru}} : {{trad+|ru|подарок}}, {{trad+|ru|дар|tr=dar}}, {{trad+|ru|презент|tr=prézent}}
                                                                                       |* {{T|se}} : {{trad--|se|skeaŋka}}
                                                                                       |* {{T|zdj}} : {{trad--|zdj|ɓambu}}
                                                                                       |* {{T|srn}} : {{trad--|srn|presenti}}
                                                                                       |* {{T|sv}} : {{trad+|sv|gåva}}, {{trad+|sv|present}}, {{trad+|sv|skänk}}
                                                                                       |* {{T|ta}} : {{trad+|ta|அன்பளிப்பு|tr=aṉpaḷippu}}
                                                                                       |* {{T|cs}} : {{trad+|cs|dar}}, {{trad+|cs|dárek}}, {{trad-|cs|dáreček}}
                                                                                       |* {{T|kim}} : {{trad--|kim|ӄуйулға}}
                                                                                       |* {{T|tyv}} : {{trad--|tyv|белек}}
                                                                                       |* {{T|tr}} : {{trad+|tr|armağan}}, {{trad+|tr|bağış}}, {{trad+|tr|hediye}}
                                                                                       |* {{T|vi}} : {{trad+|vi|quà}}
                                                                                       |* {{T|zu}} : {{trad-|zu|isipho}}
                                                                                       |{{trad-fin}}
                                                                                       |
                                                                                       |=== {{S|prononciation}} ===
                                                                                       |* {{écouter|lang=fr|France|œ̃ ka.do|titre=un cadeau|audio=Fr-cadeau.ogg}}
                                                                                       |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-Poslovitch-cadeau.wav}}
                                                                                       |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-cadeau.wav}}
                                                                                       |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-cadeau.wav}}
                                                                                       |
                                                                                       |=== {{S|anagrammes}} ===
                                                                                       |* [[audace]]
                                                                                       |
                                                                                       |=== {{S|voir aussi}} ===
                                                                                       |* {{WP}}
                                                                                       |
                                                                                       |=== {{S|références}} ===
                                                                                       |* {{Import:DAF8}}
                                                                                       |{{Références}}
                                                                                       |
                                                                                       |== {{langue|nl}} ==
                                                                                       |=== {{S|étymologie}} ===
                                                                                       |: Du {{étyl|fr|nl|mot=cadeau}}.
                                                                                       |
                                                                                       |=== {{S|nom|nl}} ===
                                                                                       |{{nl-nom|n.s=cadeau|n.pl=cadeaus|dim.s=cadeautje|dim.pl=cadeautjes}}
                                                                                       |'''cadeau''' {{n}}
                                                                                       |# [[#fr|Cadeau]], [[présent]], [[don]].
                                                                                       |#* ''iemand iets '''cadeau''' doen.''
                                                                                       |#*: offrir quelque chose à quelqu’un.
                                                                                       |#* ''ik geef het u '''cadeau'''.''
                                                                                       |#*: je vous en fais cadeau.
                                                                                       |#* '''''cadeau''' krijgen.''
                                                                                       |#*: recevoir en cadeau.
                                                                                       |#* ''dit heb ik '''cadeau''' gekregen.''
                                                                                       |#*: on me l’a offert, on m’en a fait cadeau, cela m’a été offert en cadeau.
                                                                                       |#* {{euphémisme|nl}} ''iets niet '''cadeau''' geven.''
                                                                                       |#*: vendre cher quelque chose.
                                                                                       |#* {{ironique|nl}} ''dat krijg je van me '''cadeau'''!''
                                                                                       |#*: tu peux le garder !, c’est cadeau.
                                                                                       |#* ''dat krijg je niet '''cadeau''''' : ça se mérite.
                                                                                       |#* ''je krijgt het niet '''cadeau''' in het leven.''
                                                                                       |#*: dans la vie on n’a rien pour rien.
                                                                                       |
                                                                                       |==== {{S|synonymes}} ====
                                                                                       |* [[geschenk]], [[present]], [[gift]], [[schenking]]
                                                                                       |
                                                                                       |=== {{S|taux de reconnaissance}} ===
                                                                                       |{{nl-taux|99,3|99,6|pourB=100|pourNL=99}}
                                                                                       |
                                                                                       |
                                                                                       |=== {{S|prononciation}} ===
                                                                                       |* {{écouter|||lang=nl|audio=Nl-cadeau.ogg}}
                                                                                       |* {{écouter|lang=nl|Pays-Bas (partie continentale) (Wijchen)|audio=LL-Q7411 (nld)-Robin van der Vliet-cadeau.wav}}
                                                                                       |
                                                                                       |=== {{S|Références}} ===
                                                                                       |{{Références}}""".stripMargin)
}
