/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data

object WikitextConne {
  val conne = """=== {{S|nom|fr|flexion}} ===
                |'''conne''' {{pron|kɔn|fr}}
                |# ''Féminin singulier de'' [[con#fr|con]].
                |#* ''Ce qui me rend malade ce n'est pas que tu puises te taper cette guenon, mais qu'un looser de ton espèce puisse ainsi me prendre pour une '''conne''' en draguant sans retenue n'importe quoi.'' {{source|Jean-Paul Donckèle, ''L’Amour, comment il vient comment il va... selon qui on est : La vie amoureuse des « profils »'', Société des Écrivains, 2010, p. 197}}
                |#* ''La souris gris’ se fâche et subito presto,<br />Entre la rue Didot et la rue de Vanves,<br />La '''conne''', la méchante,<br />Va d’mander ma tête à ses p’tits poteaux,<br />Entre la rue de Vanv’s et la rue Didot.'' {{source|{{w|Georges Brassens}}, ''Entre La Rue Didot Et La Rue De Vanves''}}
                |
                |=== {{S|adjectif|fr|flexion}} ===
                |'''conne''' {{pron|kɔn|fr}}
                |# ''Féminin singulier de'' [[con#fr|con]].
                |#* ''je réfléchis... je pense... l'idée me vient !... une '''idée conne'''...'' {{source|{{w|Louis-Ferdinand Céline}}, ''Féerie pour une autre fois'', Gallimard, 1952}}
                |
                |=== {{S|prononciation}} ===
                |* {{écouter|lang=fr|France (Brétigny-sur-Orge)||audio=LL-Q150 (fra)-Pamputt-conne.wav}}
                |
                |=== {{S|voir aussi}} ===
                |{{Autres projets
                ||w=Conne}}
                |""".stripMargin


  val conneRes = """== {{langue|fr}} ==
                   |=== {{S|nom|fr|flexion}} ===
                   |'''conne''' {{pron|kɔn|fr}}
                   |# ''Féminin singulier de'' [[con#fr|con]].
                   |#* ''Ce qui me rend malade ce n'est pas que tu puises te taper cette guenon, mais qu'un looser de ton espèce puisse ainsi me prendre pour une '''conne''' en draguant sans retenue n'importe quoi.'' {{source|Jean-Paul Donckèle, ''L’Amour, comment il vient comment il va... selon qui on est : La vie amoureuse des « profils »'', Société des Écrivains, 2010, p. 197}}
                   |#* ''La souris gris’ se fâche et subito presto,<br />Entre la rue Didot et la rue de Vanves,<br />La '''conne''', la méchante,<br />Va d’mander ma tête à ses p’tits poteaux,<br />Entre la rue de Vanv’s et la rue Didot.'' {{source|{{w|Georges Brassens}}, ''Entre La Rue Didot Et La Rue De Vanves''}}
                   |
                   |=== {{S|adjectif|fr|flexion}} ===
                   |'''conne''' {{pron|kɔn|fr}}
                   |# ''Féminin singulier de'' [[con#fr|con]].
                   |#* ''je réfléchis... je pense... l'idée me vient !... une '''idée conne'''...'' {{source|{{w|Louis-Ferdinand Céline}}, ''Féerie pour une autre fois'', Gallimard, 1952}}
                   |
                   |=== {{S|prononciation}} ===
                   |* {{écouter|lang=fr|France (Brétigny-sur-Orge)||audio=LL-Q150 (fra)-Pamputt-conne.wav}}
                   |
                   |=== {{S|voir aussi}} ===
                   |{{Autres projets
                   ||w=Conne}}""".stripMargin
}