/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Violette {
  def violette= """{{voir|Violette}}
                  |
                  |== {{langue|fr}} ==
                  |=== {{S|étymologie}} ===
                  |: Du {{étyl|la|fr|mot=viola}}, avec le suffixe diminutif ''[[-ette]]''.
                  |:Le nom donné dans un sens figuré à une personne timide et modeste vient du fait que cette fleur est cachée sous les feuilles.
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég|vjɔ.lɛt}}
                  |[[File:Viola odorata Garden 060402Aw.jpg|thumb|Des '''violettes''']]
                  |'''violette''' {{pron|vjɔ.lɛt|fr}} {{f}}
                  |# {{plantes|fr}} [[plante|Plante]] [[herbacé]]es vivaces de la famille des [[violacées]] appartenant au genre ''[[Viola]]''.
                  |#* {{exemple |Je plaçai mes fleurs dans un bouquetier de cristal, et je posai celui-ci sur un cahier de papier blanc étendu sur ma table. Le suave parfum des '''violettes''' se répandit dans ma chambre… je devins rêveur, […].'' |source=Jules Rengade, ''Promenades d'un naturaliste aux environs de Paris'', Librairie du petit journal, 1866, page 341 |lang=fr}}
                  |#* {{exemple |Donne-moi aussi le petit flacon de '''violette'''. Et quand je dis de '''violette'''… Il n’y a plus de vraie odeur de '''violette'''. Ils la font avec de l’iris. Et encore, la font-ils avec de l’iris ? Mais tu t’en moques, toi, Minet-Chéri, tu n’aimes pas l’essence de '''violette'''. Qu’ont donc nos filles, à ne plus aimer l’essence de '''violette''' ? |source={{w|Colette}}, ''La Maison de Claudine'', Hachette, 1922, collection Livre de Poche, 1960, page 116 |lang=fr}}
                  |# {{métonymie|fr}} {{fleurs|fr}} [[fleur|Fleur]] de cette plante.
                  |# {{poétique|fr}} Marques, taches [[violacé]]es.
                  |#* {{exemple |Nous y descendîmes, et un des premiers objets que j’aperçus sur le rivage fut le corps de Virginie; elle était à moitié couverte de sable, dans l’attitude où nous l’avions vue périr : ses traits n’étaient point sensiblement altérés; ses yeux étaient fermés, mais la sérénité était encore sur son front; seulement les pâles '''violettes''' de la mort se confondaient sur ses joues avec les roses de la pudeur. |source={{w|Jacques-Henri {{pc|Bernardin de Saint-Pierre}}}}, ''{{w|Paul et Virginie}}'', 1788 |lang=fr}}
                  |# {{figuré|fr}} Personne timide et modeste.
                  |# {{instruments|fr|de musique}} [[ancien|Ancien]] nom de l’[[alto]].
                  |
                  |==== {{S|dérivés}} ====
                  |{{(}}
                  |* [[bois de violette]] ''(sorte de bois, ainsi appelé parce que sa couleur ressemble à celle de la violette)
                  |* [[Cité des violettes]] ''(surnom de Toulouse)''
                  |* [[Père-la-Violette]] ''(surnom de Georges Clemenceau)''
                  |* [[pudeur de violette]]
                  |* [[se cacher comme une violette sous la mousse]]
                  |* [[violette de mars]]
                  |* [[violette de Toulouse]]
                  |* [[violette du Cap]]
                  |* [[violette odorante]]
                  |{{)}}
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début|traductions à trier}}
                  |* {{T|conv}} : {{trad+|conv|Viola}}
                  |* {{T|af}} : {{trad-|af|viooltjie}}
                  |* {{T|de}} : {{trad+|de|Veilchen}}
                  |* {{T|en}} : {{trad+|en|violet}}
                  |* {{T|ar}} : {{trad+|ar|بنفسج|m|dif=بِنَفْسَج|tr=binafsaj}}
                  |* {{T|ba}} : {{trad|ba|миләүшә}}
                  |* {{T|br}} : {{trad-|br|melionenn}}
                  |* {{T|ca}} : {{trad+|ca|violeta}}, {{trad+|ca|viola}}
                  |* {{T|co}} : {{trad-|co|viola}}
                  |* {{T|rcf}} : {{trad|rcf|violèt}}
                  |* {{T|hr}} : {{trad+|hr|ljubica}}
                  |* {{T|da}} : {{trad-|da|viol}}
                  |* {{T|es}} : {{trad+|es|violeta}}
                  |* {{T|eo}} : {{trad-|eo|violo}}
                  |* {{T|fo}} : {{trad-|fo|blákolla}}, {{trad-|fo|vióla}}
                  |* {{T|fi}} : {{trad+|fi|orvokki}}
                  |* {{T|fy}} : {{trad-|fy|fioeltsje}}
                  |* {{T|ka}} : {{trad-|ka|ია|tr=ia}}
                  |* {{T|el}} : {{trad+|el|βιολέτα|R=violéta}}, {{trad+|el|μενεξές|R=menexés}}, {{trad+|el|ίον|R=íon}}, {{trad+|el|γιούλι|R=yioúli}}
                  |* {{T|hu}} : {{trad+|hu|ibolya}}
                  |* {{T|io}} : {{trad+|io|violo}}
                  |* {{T|it}} : {{trad+|it|viola}}
                  |* {{T|ja}} : {{trad-|ja|菫|tr=sumire}}
                  |* {{T|kk}} : {{trad-|kk|шегіргүл|tr=şegirgül}}
                  |* {{T|kum}} : {{trad|kum|мелевше}}
                  |* {{T|ku}} : {{trad-|ku|benefşe}}
                  |* {{T|la}} : {{trad+|la|viola}}, {{trad-|la|ianthis}}
                  |* {{T|lez}} : {{trad|lez|беневша}}
                  |* {{T|nl}} : {{trad+|nl|viooltje}}
                  |* {{T|fa}} : {{trad+|fa|بنفشه}}
                  |* {{T|pl}} : {{trad+|pl|fiołek}}
                  |* {{T|pt}} : {{trad+|pt|violeta}}
                  |* {{T|qya}} : {{trad|qya|helin}}
                  |* {{T|ru}} : {{trad+|ru|фиалка|tr=fialka}}
                  |* {{T|sr}} : {{trad-|sr|ljubičica}}
                  |* {{T|sv}} : {{trad+|sv|viol}}
                  |* {{T|crh}} : {{trad|crh|melevşe}}
                  |* {{T|tt}} : {{trad-|tt|miläwşä}}, {{trad+|tt|миләүшә}}
                  |* {{T|cs}} : {{trad-|cs|fialka}}, {{trad-|cs|fiala}}
                  |* {{T|tr}} : {{trad+|tr|menekşe}}
                  |* {{T|tk}} : {{trad-|tk|benewşe}}
                  |{{trad-fin}}
                  |
                  |=== {{S|adjectif|fr|flexion}} ===
                  |{{fr-accord-et|vjɔ.l|ms=violet}}
                  |'''violette''' {{pron|vjɔ.lɛt|fr}}
                  |# ''Féminin singulier de'' [[violet]].
                  |
                  |=== {{S|verbe|fr|flexion}} ===
                  |{{fr-verbe-flexion|violeter|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
                  |'''violette''' {{pron|vjɔ.lɛt|fr}}
                  |# ''Première personne du singulier de l’indicatif présent de'' [[violeter]].
                  |# ''Troisième personne du singulier de l’indicatif présent de'' [[violeter]].
                  |# ''Première personne du singulier du subjonctif présent de'' [[violeter]].
                  |# ''Troisième personne du singulier du subjonctif présent de'' [[violeter]].
                  |# ''Deuxième personne du singulier de l’impératif de'' [[violeter]].
                  |
                  |=== {{S|prononciation}} ===
                  |* {{pron-rimes|vjɔ.lɛt|fr}}
                  |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->||audio=Fr-violette.ogg}}
                  |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines1-violette.wav}}
                  |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines2-violette.wav}}
                  |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines3-violette.wav}}
                  |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines5-violette.wav}}
                  |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Aemines4-violette.wav}}
                  |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines6-violette.wav}}
                  |* {{écouter|lang=fr|France||audio=LL-Q150 (fra)-Julien Baley-violette.wav}}
                  |* {{écouter|Cornimont (France)||lang=fr|audio=LL-Q150 (fra)-Poslovitch-violette.wav}}
                  |* {{écouter|Alsace (France)||lang=fr|audio=LL-Q150 (fra)-Le Commissaire-violette.wav}}
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{WP}}
                  |
                  |=== {{S|références}} ===
                  |* {{R:DAF8}}`
                  |
                  |== {{langue|it}} ==
                  |=== {{S|nom|it|flexion}} ===
                  |{{it-flexion|f=ae|vjo.ˈlet.t}}
                  |'''violette''' {{pron|vjo.ˈlet.te|it}} {{f}}
                  |# ''Pluriel de'' {{lien|violetta|it}}.
                  |
                  |=== {{S|adjectif|it|flexion}} ===
                  |{{it-flexion|f=oaie|vjo.ˈlet.t}}
                  |'''violette''' {{pron|vjo.ˈlet.te|it}}
                  |# ''Féminin pluriel de'' {{lien|violetto|it}}.
                  |
                  |== {{langue|sv}} ==
                  |=== {{S|adjectif|sv|flexion}} ===
                  |'''violette''' {{pron||sv}}
                  |# ''Forme dérivée de ''{{lien|violett|sv}}.
                  |""".stripMargin
}
