/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object noeud {
  val resource = Resource("noeud", "https://fr.wiktionary.org/wiki/","entry",
  """{{voir|noeud}}
    |
    |== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: De l’{{étyl|fro|fr|neu}}, du {{étyl|la|fr|mot=nodus|dif=nōdus|sens=nœud}}.
    |
    |=== {{S|nom|fr}} ===
    |{{fr-rég|nø}}
    |[[Image:CarricksBend HowTo.jpg|thumb|Exemple d’un '''nœud''' (1) de carrick.]]
    |'''nœud''' {{pron|nø|fr}} {{m}}
    |# [[enlacement|Enlacement]], [[entrecroisement]] serré de quelque chose de [[flexible]], ruban, soie, fil, corde, etc. que l’on fait soit à un bout pour l’[[arrêter]], soit avec deux bouts pour les [[unir]].
    |#* ''Le corsage est montant, à basque plate rouleautée au contour, et la manche a son parement orné d'un '''nœud''' en faille.'' {{source|« Explications de la gravure noire pages 174 et 175 », dans le ''Journal des demoiselles et Petit courrier des dames réunis'', n° 15 (2{{e}} semestre) du 13 novembre 1875, p. 171}}
    |#* ''Jusqu’aux cravates, au petit '''nœud''' suavement bloqué par une épingle dans l’échancrure du col, jusqu’au feu d’un vrai diam’ et au cuir mat du bracelet-montre, on sentait ces messieurs soucieux de leur mise.'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
    |#* ''Il se leva tout à fait, passa la main entre l’échancrure de son gilet et le plastron de sa chemise qui godait, tira les revers de son habit, et s’assura que le '''nœud''' de sa cravate n’avait pas été dérangé.'' {{source|{{w|Octave Mirbeau}}, ''Le colporteur''}}
    |## {{en particulier}} [[enchevêtrement|Enchevêtrement]] de [[cheveux]] ou de poils.
    |##* ''Si vous rencontrez des '''nœuds''', surtout ne forcez pas et finissez encore moins votre course en arrachant une partie des cheveux avec le '''nœud'''.'' {{source|Élodie-Joy Jaubert, ''J’aime mes cheveux - Tous les soins de beauté au naturel'', 2014, La Plage}}
    |# {{figuré|fr}} [[attachement|Attachement]], [[lien]] [[étroit]] entre des personnes.
    |#* ''Le '''nœud''' sacré du mariage.
    |#* ''Les '''nœuds''' de l’amitié.''
    |# Lieu où se rencontrent et s’unissent durablement certaines réalités.
    |#* ''Un village, au cours d’une guerre, n’est pas un '''nœud''' de traditions. Aux mains de l’ennemi, il n’est plus qu’un nid à rats.'' {{source|{{w|Antoine de Saint-Exupéry}}, [[w:Pilote de guerre|''Pilote de guerre'']], 1942, XIII}}
    |# {{lexique|marine|fr}} Unité de [[vitesse]] utilisée en navigation [[maritime]] et [[aérien]]ne, correspondante à 1 [[mille marin]] par heure, soit exactement 1,852 [[km/h]].
    |#* ''Sans cesse sur le pont, ajustant les écoutes de mes voiles pour en obtenir le meilleur rendement, utilisant chaque souffle de vent, j’avais réussi à conserver la vitesse d’à peu près un '''nœud'''.'' {{source|{{w|Alain Gerbault}}, ''À la poursuite du soleil'', tome 1 : ''De New-York à Tahiti'', 1929}}
    |#* ''Il y avait déjà deux jours que le bateau filait vers le grand Nord, à la vitesse maintenue de vingt '''nœuds'''.'' {{source|Jean Lhassa, ''Dernières nouvelles d'ailleurs'', Éditions Publibook, 2009, page 209}}
    |#* ''Que disait Joyon de cette trajectoire sur l’eau ? « ''Quand j’ai vu qu’il descendait le golfe de Gascogne deux '''nœuds''' plus vite que nous, avec de surcroît un bien meilleur angle au vent, j’ai compris que la messe était dite. ''»'' {{source|Jean-Louis Le Touzet, ''Franck Cammas, le sur-Rhum'', dans {{w|Libération (journal)|''Libération''}}, 10 novembre 2010}}
    |# {{argot|fr}} Extrémité du [[pénis]].
    |#* ''Elle a une manière tranquillement salingue de te regarder, qui te fait émerger la goutte de rosée au bout du '''nœud'''.'' {{source|''Maman, la dame fait rien qu’à me faire des choses !'', San Antonio, 2010, Fleuve noir}}
    |# {{argot|fr}} {{par ext}} {{injur|fr}} Personne [[stupide]], {{cf|lang=fr|tête de nœud}}.
    |#* ''Espèce de '''nœud''' : espèce d’imbécile.''
    |#* ''Tête de '''nœud''' !''
    |# [[articulation|Articulation]], [[jointure]] [[protubérant]]e des [[doigt]]s de la [[main]], [[nodosité]].
    |#* ''Un '''nœud''' au doigt du milieu.''
    |# [[vertèbre|Vertèbre]] qui forment la [[queue]] du [[cheval]], du [[chien]], du [[chat]] etc.
    |#* ''On a coupé à ce cheval deux '''nœuds''' de la queue.''
    |# {{lexique|botanique|fr}} Point d’[[attache]] des [[feuille]]s sur la [[tige]].
    |#* ''Il faut tailler la vigne au troisième '''nœud'''.''
    |#* ''Les éléments de balle retrouvés étaient constitués de glumes, bases de [[glume]], épillets mais aussi de '''nœuds''' de tige et de fragments de paille.''{{source|Lætitia Meurisse et al., [https://www.cairn.info/revue-du-nord-2014-5-page-109.htm Brillon (Nord), Parc d’activités de Sars-et-Rosières : un établissement gallo-romain original dans la vallée de la Scarpe] dans la ''Revue du Nord'', 2014}}
    |# {{lexique|botanique|foresterie|fr}} Déformation [[serré]]e et [[dur]]e de la substance ligneuse notamment du [[tronc]]&nbsp;; [[nodosité]] provoquée par la [[séparation]] d’une [[branche]] d’avec le tronc.
    |#* ''Je découvris, dans la planche du bout, un '''nœud''' qui cédait légèrement sous la pression ; je travaillai avec la plus grande peine, je finis par chasser le '''nœud''', et de l’autre côté, en enfonçant le doigt, je reconnus la terre, une terre grasse, argileuse et mouillée.'' {{source|{{w|Émile Zola}}, ''{{ws|La Mort d’Olivier Bécaille|La Mort d’Olivier Bécaille}}'', 1879}}
    |#* ''Martin se tenait à son côté sans rien dire et il avait les yeux rivés sur un '''nœud''' du parquet.'' {{source|{{ouvrage |prénom=Camilla |nom=Läckberg |trad=Lena Grumbach et Catherine Marcus |titre=Le Tailleur de pierre |éditeur=Actes Sud |lieu= |année=2009 |année première édition=2005}}, page 29}}
    |#* ''Les '''nœuds''' constituent le défaut majeur de la plupart des grumes de haute qualité, entraînant souvent leur déclassement. Ils sont exclus ou tolérés en très petit nombre et sains pour les hautes qualités. Un '''nœud''' pourri doit être purgé.'' {{source|Emmanuelle {{smcp|Brunin}}, Christophe {{smcp|Heyninck}} et Delphine {{smcp|Arnal}}, ''Carnet d’assistance pour l’évaluation qualitative des bois sur pied et abattus'', Forêt wallonne, 2012, {{ISBN|2-9600251-6-4}}}}
    |## {{particulier}} [[protubérance|Protubérance]] sur le tronc.
    |##* ''Le bois de cornouiller est plein de '''nœuds'''.''
    |# {{figuré|fr}} [[ornement|Ornement]] en forme de '''nœud''' sans en avoir l’utilité.
    |#* '''''Nœud''' de ruban,'' ornement en forme de '''nœud''' fait avec des rubans enlacés.
    |#* '''''Nœud''' d’épée,'' rosette de ruban dont on ornait la poignée d’une épée.
    |#* ''Collier orné de '''nœuds''' de diamant.''
    |# {{figuré|fr}} [[difficulté|Difficulté]], [[os]], point [[essentiel]] d’une [[affaire]], d’une [[question]].
    |#* ''Or, c’est bien ça le '''nœud''' du problème de la pédophilie dans l’Église catholique : ce n’est pas qu’il y ait des prêtres pédocriminels mais qu’ils aient été couverts continûment par leurs supérieurs.'' {{source|Josselin Tricou & Anthony Favier, « ''Pédocriminalité dans l'église catholique française : une commission parlementaire est indispensable'' », le 5 octobre 2018, dans ''[[w:Libération (journal)|Libération]]'' (www.liberation.fr)}}
    |#* ''Trancher le '''nœud''' de la question, de la difficulté.''
    |## {{lexique|littérature|fr}} {{particulier}} [[incident|Incident]] qui détermine la [[marche]] de la [[pièce de théâtre]], d’où [[dépendre|dépend]] l’[[intrigue]] d’une [[action]] [[dramatique]], {{cf|lang=fr|nœud de l’action}}.
    |##* ''Ce revirement fait le '''nœud''' de cette comédie.''
    |# {{lexique|physique|fr}} Point où l’[[amplitude]] de l’[[onde stationnaire]] est toujours nulle. [[Image:Standing wave.gif|vignette|Les points rouges représentent les '''nœuds''' (13) de l’onde stationnaire.]]
    |#* ''On construit des instruments de musique en suspendant sur des fils des lames de longueur différente ; on les dispose de manière que les lames posent par leurs '''nœuds'''. On frappe au milieu pour déterminer le mouvement vibratoire. Le glasschord, […], est un instrument de ce genre , fait avec des lames de verre ; le claquebois ou xylocordéon est fait avec des lames de bois.'' {{source|{{w|Eugène Soubeiran}}, ''Précis élémentaire de physique'', 2{{e}} édition, Paris: chez Fortin, Masson & C{{e|ie}}, 1844, p. 134}}
    |# {{lexique|électricité|fr}} Point de [[liaison]] d’au moins deux [[conducteur]]s dans un [[circuit électrique]]. [[Fichier:Trace junction.svg|vignette|droite|Symbole d’un '''nœud''' (14)]]
    |#* ''En outre, les jeux de barres sont susceptibles de constituer plusieurs '''nœuds''' électriques par l’ouverture de disjoncteurs ; on appelle alors ''sommet'' le jeu de barres ou le tronçon de jeu de barres ainsi constitué. Le nombre des sommets d’un poste caractérise ainsi son aptitude à former des '''nœuds''' électriques.'' {{source|Jean-Michel {{smcp|Delbarre}}, ''Postes à haute et très haute tension''|lien=https://www.cjoint.com/doc/16_04/FDrmSbMg6xd_d4570.pdf}}
    |# {{lexique|géologie|fr}} Point où des [[chaîne]]s de [[montagne]]s se [[réunir|réunissent]] en un [[système]].
    |# {{lexique|théorie des graphes|fr}} [[sommet|Sommet]] parcouru par plusieurs [[arc]]s.
    |# {{lexique|informatique|programmation|fr}} Élément d’une structure [[arborescent]]e, qui peut lui-même contenir un ou plusieurs nœuds enfants.
    |#* ''Chaque '''nœud''' a au plus deux fils, et le premier fils aura une clé inférieure au père, qui aura lui-même une clé.'' {{source|Yves Mettier, ''C en action'', page 167, Editions ENI, 2009}}
    |#* ''Ainsi, chaque élément HTML est représenté par un '''nœud''' ou une branche, accessible via le JavaScript.'' {{source|1=[http://books.google.fr/books?id=sjn4ES2EsrsC&pg=PA2&lpg=PA2&dq=%22n%C5%93ud%22+javascript&source=bl&ots=5Y2R_hG4_b&sig=cg7GM6dei8v0n2Ft7ZLC9EMokvw&hl=fr&sa=X&ei=H0o1UMrXO4eG0AWJnoD4BQ&ved=0CFQQ6AEwBg#v=onepage&q=%22n%C5%93ud%22%20javascript&f=false ''Ajax à 200 % - Page 2, Bruce W. Perry - 2006]}}
    |# {{lexique|informatique|fr}} Emplacement connectant plusieurs machines. {{?|la référence dit qu'un noeud est une machine|fr}}
    |#* ''La panne d’un '''nœud''' n’affecte pas le réseau.'' {{source|1=[//books.google.fr/books?id=eHmcFmf-E6MC&pg=PA11&lpg=PA11&dq=%22n%C5%93ud%22+r%C3%A9seau+%C3%A9thernet&source=bl&ots=XTIln7A4j9&sig=bM9XUJcu0fLPlFrQnrFkY0UiK3U&hl=fr&sa=X&ei=QUg1UMKcG-rG0QWjyIHYBQ&ved=0CEgQ6AEwAw#v=onepage&q=%22n%C5%93ud%22%20r%C3%A9seau%20%C3%A9thernet&f=false ''Transmission de Puissance Pneumatique - Page 11'', Daniel HUBERT]}}
    |# {{lexique|industrie pétrolière|fr}} Lieu d’interconnexion de réseaux de gaz naturel, destiné à optimiser les échanges gaziers à grande échelle.
    |# {{lexique|astronautique|fr}} L’un des points d’intersection d’une orbite avec le plan de référence qui, pour un satellite, est généralement le plan équatorial du corps autour duquel il orbite.
    |# Nom de la [[coiffure]] traditionnelle des [[alsacienne]]s.
    |
    |==== {{S|synonymes}} ====
    |* [[flot]] (d’un ruban pour retenir les cheveux)
    |* [[sommet]] (de graphe)
    |
    |==== {{S|antonymes}} ====
    |; Point où l’amplitude de l’onde stationnaire est toujours nulle :
    |* [[ventre]]
    |
    |==== {{S|apparentés}} ====
    |{{(}}
    |* [[neuneu]]
    |* [[nodal]]
    |* [[nodosité]]
    |* [[nodulaire]]
    |* [[nodule]]
    |* [[noduleux]]
    |* [[nodus]]
    |* [[nouage]]
    |* [[nouer]]
    |* [[noueux]]
    |{{)}}
    |
    |==== {{S|dérivés}} ====
    |{{(}}
    |* [[à la mord-moi-le-nœud]]
    |* [[corde à nœuds]]
    |* [[entre-nœud]]
    |* [[faire un nœud à son mouchoir]]
    |* [[multi-nœud]], [[multinœud]]
    |* [[nœud coulant]]
    |* [[nœud de Ranvier]]
    |* [[nœud baril]]
    |* [[nœud gordien]]
    |* [[nœud marin]]
    |* [[nœud-nœud]]
    |* [[nœud plat]]
    |* [[nœud sinusal]] : partie du cœur à l’origine du rythme cardiaque
    |* [[nœud vital]] : {{info lex|biologie}} point du bulbe rachidien qui gouverne tous les mouvements respiratoires de l’animal et dont la lésion suffit pour le tuer instantanément.
    |* [[pogner un nœud]]
    |* [[sac de nœuds]]
    |* [[se faire des nœuds au cerveau]]
    |* [[tête de nœud]]
    |* [[théorie des nœuds]] : branche de la topologie qui consiste en l’étude mathématique de bouts de ficelles idéalisés.
    |{{)}}
    |
    |==== {{S|vocabulaire}} ====
    |* [[bondage]]
    |
    |* {{voir thésaurus|fr|cadeau}}
    |
    |==== {{S|hyponymes}} ====
    |{{(}}
    |* [[nœud d’accroche]]
    |** [[nœud de cabestan]]
    |** [[nœud de demi-cabestan]]
    |** [[nœud en tête d’alouette]]
    |** [[nœud de taquet]]
    |** [[nœud de croc]]
    |** [[nœud de palan]]
    |** [[nœud de gueule de raie]]
    |** [[nœud d’élingue]]
    |** [[nœud de Prussik]]
    |** [[nœud de Machard]]
    |** [[nœud français]]
    |* [[nœud d’ajut]]
    |** [[nœud plat]]
    |** [[nœud de vache]] ou [[nœud en queue de cochon]] ou [[nœud de ménagère]]
    |** [[nœud de voleur]]
    |** [[nœud de carrick]]
    |** [[nœud de pêcheur]]
    |** [[nœud d’agui]]
    |** [[nœud de pêcheur double]]
    |** [[nœud d’écoute]]
    |** [[nœud d’écoute double]]
    |** [[nœud de sangle]]
    |** [[nœud de rosette]]
    |** [[nœud de chirurgien]]
    |** [[nœud de gabier]]
    |* [[nœud d’arrêt]]
    |** [[demi-nœud]]
    |** [[nœud de huit]]
    |** [[nœud de capucin]]
    |** [[nœud de franciscain]]
    |** [[nœud de lestage]]
    |** [[nœud de cul de porc]]
    |* [[nœud de boucle]]
    |** [[nœud de chaise]]
    |** [[nœud de calfat]]
    |** [[nœud de chaise double sur son double]]
    |** [[nœud de chaise triple]]
    |** [[nœud en double huit]]
    |** [[nœud de galère]] ou [[demi-nœud gansé]]
    |** [[nœud de jambe de chien]]
    |** [[nœud de laguis]]
    |** [[nœud de lapin]]
    |** [[nœud de menottes]]
    |** [[nœud de papillon]]
    |** [[nœud de pendu]]
    |** [[nœud de plein poing]]
    |* [[nœud décoratif]]
    |** [[nœud en tête de turc]]
    |** [[nœud de carrick multiple]]
    |** [[nœud de scoubidou]]
    |** [[nœud en pomme de touline]]
    |** [[nœud de gallon]]
    |** [[nœud de cravate simple]]
    |** [[nœud de cravate Windsor]] ou [[nœud Windsor]]
    |** [[nœud papillon]] ou [[nœud pap’]]
    |{{)}}
    |
    |==== {{S|traductions}} ====
    |{{trad-début|Enlacement|1}}
    |* {{T|de}} : {{trad+|de|Knoten|m}}, {{trad+|de|Schleife|f}}, {{trad+|de|Schlinge|f}}
    |* {{T|en}} : {{trad+|en|knot}} ; {{trad+|en|bow}}
    |* {{T|ar}} : {{trad-|ar|عقدة|tr='uqda}}
    |* {{T|eu}} : {{trad-|eu|korapilo}}
    |* {{T|br}} : {{trad+|br|koulm|m}}
    |* {{T|ca}} : {{trad+|ca|nus|m}}, {{trad+|ca|llaç}}
    |* {{T|zh}} : {{trad+|zh|结|tradi=結|tr=jié}}
    |* {{T|ko}} : {{trad+|ko|매듭|tr=maedeup}}
    |* {{T|kw}} : {{trad-|kw|kolm}}
    |* {{T|da}} : {{trad-|da|sløjfe}}, {{trad+|da|knob|n}}, {{trad-|da|stik|n}}
    |* {{T|eo}} : {{trad+|eo|nodo}}
    |* {{T|fi}} : {{trad+|fi|solmu}} (1), {{trad+|fi|liitto}} (2), {{trad-|fi|siteet}} (2)
    |* {{T|ga}} : {{trad+|ga|snaidhm|f}}
    |* {{T|gl}} : {{trad-|gl|nó|m}}
    |* {{T|cy}} : {{trad-|cy|cwlwm|m}}
    |* {{T|el}} : {{trad+|el|κόμπος|m|tr=kómbos}}
    |* {{T|griko}} : {{trad|griko|kombo}}, {{trad|griko|kumbo|m}}
    |* {{T|io}} : {{trad+|io|nodo}}
    |* {{T|it}} : {{trad+|it|nodo}}, {{trad+|it|groviglio}}
    |* {{T|ja}} : {{trad-|ja|結び目|tr=musubime}}
    |* {{T|avk}} : {{trad|avk|weboka}}
    |* {{T|nl}} : {{trad+|nl|knoop|m}}
    |* {{T|normand}} : {{trad|normand|nou}}
    |* {{T|no}} : {{trad-|no|knop}}
    |* {{T|oc}} : {{trad+|oc|nos}}, {{trad-|oc|nosèl}}
    |* {{T|pl}} : {{trad+|pl|węzeł}}
    |* {{T|ru}} : {{trad+|ru|узел}}
    |* {{T|se}} : {{trad|se|čuolbma}}
    |* {{T|zdj}} : {{trad|zdj|funɗo|tr=fundo}}
    |* {{T|sk}} : {{trad-|sk|uzol}}
    |* {{T|ses}} : {{trad|ses|guli}}
    |* {{T|cs}} : {{trad+|cs|uzel}}, {{trad-|cs|poutko}}, {{trad+|cs|mašle}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|marine}} Unité de mesure|3}}
    |* {{T|de}} : {{trad+|de|Knoten}}
    |* {{T|en}} : {{trad+|en|knot}}
    |* {{T|ca}} : {{trad+|ca|nus}}
    |* {{T|zh}} : {{trad+|zh|节|tr=jié|tradi=節}}
    |* {{T|da}} : {{trad+|da|knob|c}}
    |* {{T|fi}} : {{trad+|fi|solmu}}
    |* {{T|gl}} : {{trad-|gl|nó|m}}
    |* {{T|io}} : {{trad+|io|tubero}}
    |* {{T|ja}} : {{trad+|ja|ノット|tr=notto}}
    |* {{T|avk}} : {{trad|avk|knot kaliolk}}
    |* {{T|nl}} : {{trad+|nl|knoop|m}}
    |* {{T|nb}} : {{trad-|nb|knop|m}}
    |* {{T|cs}} : {{trad+|cs|uzel}}
    |{{trad-fin}}
    |
    |{{trad-début|Articulation, jointure protubérante des doigts de la main|7}}
    |* {{T|en}} : {{trad+|en|knuckle}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|botanique}} Point d’attache des feuilles sur la tige|9}}
    |* {{T|en}} : {{trad+|en|node}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|botanique}} Déformation serrée et dure de la substance ligneuse notamment du tronc|10}}
    |* {{T|en}} : {{trad+|en|knot}}
    |* {{T|ar}} : {{trad-|ar|كعبرة|tr=ku'bura}}
    |* {{T|avk}} : {{trad|avk|argist}}
    |* {{T|ru}} : {{trad+|ru|дупло|n|tr=dupló}}
    |{{trad-fin}}
    |
    |{{trad-début| Noeud ornemental|11}}
    |* {{T|de}} : {{trad-|de|Bandschleife}}
    |* {{T|en}} : {{trad+|en|bow}}
    |* {{T|eo}} : {{trad+|eo|banto}}
    |* {{T|pl}} : {{trad+|pl|kokarda}}
    |* {{T|ru}} : {{trad+|ru|бант|m|tr=bant}}
    |{{trad-fin}}
    |
    |{{trad-début|{{figuré|nocat=1}} Difficulté, point essentiel d’une affaire, d’une question|12}}
    |* {{T|en}} : {{trad+|en|nub}}
    |* {{T|zdj}} : {{trad|zdj|funɗo}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|physique}} Point où l’amplitude est toujours nulle|13}}
    |* {{T|en}} : {{trad+|en|node}}
    |* {{T|ja}} : {{trad+|ja|節|tr=setsu}}
    |* {{T|nl}} : {{trad+|nl|knoop|m}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|théorie des graphes}} Sommet parcouru par plusieurs arcs|16}}
    |* {{T|en}} : {{trad+|en|node}}
    |* {{T|ja}} : {{trad+|ja|ノード|tr=nōdo}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|informatique}} Emplacement connectant plusieurs machines|17}}
    |* {{T|en}} : {{trad+|en|node}}
    |{{trad-fin}}
    |
    |{{trad-début|{{info lex|programmation}} Élément désignant une partie de l’arborescence HTML|18}}
    |* {{T|en}} : {{trad+|en|node}}
    |{{trad-fin}}
    |
    |{{trad-début||19}}
    |* {{T|en}} : {{trad+|en|hub}}
    |{{trad-fin}}
    |
    |{{trad-début||20}}
    |* {{T|en}} : {{trad+|en|node}}
    |{{trad-fin}}
    |
    |===== {{S|traductions à trier}} =====
    |* {{T|af|trier}} : {{trad-|af|knoop}}
    |* {{T|ar|trier}} : {{trad-|ar|عقدة}}
    |* {{T|ca|trier}} : {{trad+|ca|node}}, {{trad+|ca|nus}}, {{trad+|ca|llaç}}
    |* {{T|da|trier}} : {{trad+|da|knude}}
    |* {{T|es|trier}} : {{trad+|es|nudo|m}}, {{trad+|es|lazo}}
    |* {{T|fo|trier}} : {{trad-|fo|knútur}}, {{trad-|fo|kvistur}}
    |* {{T|is|trier}} : {{trad-|is|hnútur}}
    |* {{T|nl|trier}} : {{trad+|nl|geleding}}, {{trad+|nl|knoest}}, {{trad+|nl|knoop}}, {{trad+|nl|knooppunt}}, {{trad+|nl|kwast}}, {{trad+|nl|lis}}, {{trad+|nl|lus}}, {{trad+|nl|strik}}
    |* {{T|pap|trier}} : {{trad|pap|kònòpi}}
    |* {{T|pl|trier}} : {{trad+|pl|węzeł}}
    |* {{T|pt|trier}} : {{trad+|pt|nodo}}, {{trad+|pt|nó}}, {{trad+|pt|encruzilhada}}, {{trad+|pt|laço}}, {{trad+|pt|vínculo}}
    |* {{T|sv|trier}} : {{trad+|sv|knut}}
    |* {{T|tl|trier}} : {{trad-|tl|buhól}}
    |* {{T|tr|trier}} : {{trad+|tr|boğum}}
    |* {{T|wo|trier}} : {{trad-|wo|pass}}
    |{{trad-fin}}
    |
    |=== {{S|prononciation}} ===
    |* {{écouter|France <!-- précisez svp la ville ou la région -->|nø|lang=fr|audio=Fr-noeud.ogg}}
    |* {{écouter|France <!-- précisez svp la ville ou la région -->|titre=un nœud|ɛ̃ nø|lang=fr|audio=Fr-nœud.ogg}}
    |* {{écouter|lang=fr|France (Toulouse)|nø|audio=LL-Q150 (fra)-Lepticed7-nœud.wav}}
    |* {{écouter|lang=fr|France (Muntzenheim)|nø|audio=LL-Q150 (fra)-0x010C-nœud.wav}}
    |* {{écouter|lang=fr|France (Vosges)|nø|audio=LL-Q150 (fra)-LoquaxFR-nœud.wav}}
    |* {{écouter|lang=fr|France (Lyon)|nø|audio=LL-Q150 (fra)-WikiLucas00-nœud.wav}}
    |
    |=== {{S|voir aussi}} ===
    |* {{WP}}
    |* {{WP|Nœud_(lien)|lang=fr|titre=Liste de nœuds}} (en français)
    |* {{WP|List_of_knots|lang=en|titre=Liste de nœuds}}
    |* {{WP|List_of_hitch_knots|lang=en|titre=Liste de nœuds d’accroche}}
    |
    |=== {{S|références}} ===
    |* {{R:Grand Robert}}
    |* {{R:Larousse}}
    |* {{R:TLFi}}
    |* {{R:DAF8}}
    |* {{R:FranceTerme}}
    |
    |[[Catégorie:Lexique en français de la navigation]]
    |[[Catégorie:œ en français]]
    |[[Catégorie:Unités de mesure en français]]
    |""".stripMargin)
}
