package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Humeur {
  val resource = Resource("humeur","https://fr.wiktionary.org/wiki/","entry","""== {{langue|fr}}==
                                                                                    |=== {{S|étymologie}} ===
                                                                                    |: ''([[#fr-nom-1|Nom commun 1]])''  Emprunté au {{étyl|la|fr}} ''[[humor]]'' (« [[liquide]], fait d’être « [[mouiller|mouillé]] ») {{cf|humide}}.
                                                                                    |: À l’origine, le mot ''humeur'' a un sens purement médical ; mais avec l’évolution de la médecine les chercheurs isolèrent et définirent les liquides du corps humain ; peu à peu le terme ''humeur'' tomba en désuétude. Le langage courant, petit à petit, conduit à l’utiliser pour évoquer des émotions. Ceci est essentiellement dû au manque d’éducation médicale populaire. Le peuple pensait encore au <small>XIX{{e}}</small> siècle que les troubles du comportement ou les tempéraments provenaient de certains fluides coulant dans les veines, d’où la dérive et l’amalgame qui se sont opérés sur le terme. {{cf|colère}}.
                                                                                    |: ''([[#fr-nom-2|Nom commun ]])'' De [[humer]].
                                                                                    |
                                                                                    |=== {{S|nom|fr|num=1}} ===
                                                                                    |{{fr-rég|y.mœʁ|préfpron={{h muet|fr}}}}
                                                                                    |'''humeur''' {{h muet|fr}}{{pron|y.mœʁ|fr}} {{f}}
                                                                                    |# {{vieilli|fr}} {{médecine non conv|fr}} Toute [[substance]] [[liquide]] qui se [[trouver|trouve]] dans un [[organisme]].
                                                                                    |#* '''''Humeur''' visqueuse.'' Il se disait surtout dans l’ancienne médecine.
                                                                                    |#* ''Les '''humeurs''' du corps humain sont la lymphe, le sang, le chyle, la bile, etc.''
                                                                                    |#* ''Cela vicie les '''humeurs'''.''
                                                                                    |#* ''Cela met les '''humeurs''' en mouvement.''
                                                                                    |#* ''L’altération des '''humeurs''' cause diverses maladies.''
                                                                                    |#* '''''Humeur''' maligne, '''Humeurs''' peccantes,'' Humeurs du corps que l’on croyait [[vicié]]es.
                                                                                    |#* '''''Humeur''' âcre.''
                                                                                    |#* '''''Humeur''' maligne.''
                                                                                    |#* ''Il invoqua les Indiens du Brésil qui gardent le cadavre du grand-père pendant plusieurs mois, pour envenimer la pointe de leurs armes avec les puantes '''humeurs''' de l’aïeul.'' {{source|{{nom w pc|Marcel|Pagnol}}, ''{{w|Le château de ma mère}}'', 1958, collection Le Livre de Poche, page 270}}
                                                                                    |#* '''''Humeurs''' froides,'' Nom vulgaire des [[dégénérescence]]s [[tuberculeuse]]s des [[glande]]s du [[cou]]. {{cf|écrouelles}}
                                                                                    |# {{figuré|fr}} [[état|État]] d’[[esprit]] plus ou moins [[durable]], particulièrement en ce qu’il est plutôt [[agréable]] ou non.
                                                                                    |#* ''Quoique très instruite, elle n'avoit ni les caprices, ni l’'''humeur''' qu'on attribue aux gens de lettres, qui tantôt se livrent à une loquacité importune, tantôt se renferment dans un silence méprisant.'' {{source|[[w:Étienne-François de Lantier|E.-F. Lantier]], ''Voyages d'Anténor en Grèce et en Asie'', Paris : chez Belin & chez Bernard, 2{{e}} édition revue, {{w|an VI}}, tome 1{{er}}, page 35}}
                                                                                    |#* ''[…] insensiblement la sombre '''humeur''' qu’on voyait sur leurs traits s’atténuait, s’effaçait. De menaçants, ils devenaient gouailleurs, puis doucereux, entreprenants.'' {{source|{{Citation/Francis Carco/Images cachées/1928}}}}
                                                                                    |#* ''Les cours de la graine de tournesol fluctuent en fonction de l’'''humeur''' des triturateurs et donc de la demande en huile.'' {{source|''ÉPIS-CENTRE Infos'', mai 2010}}
                                                                                    |#* ''Le premier passait son chemin en respirant une fleur et l’on se rendait compte qu’il était à cent lieues de partager l’'''humeur''' baroque et déconcertante du second.'' {{source|{{Citation/Francis Carco/L’Homme de minuit/1938}}}}
                                                                                    |#* ''Il a une '''humeur''' noire, une '''humeur''' atrabiliaire, une '''humeur''' mélancolique.''
                                                                                    |#* ''Être d’'''humeur''' douce, d’'''humeur''' fâcheuse, d’'''humeur''' égale, d’'''humeur''' inégale, d’'''humeur''' enjouée, d’'''humeur''' chagrine, d’'''humeur''' complaisante.''
                                                                                    |#* ''Il est aujourd’hui en bonne '''humeur''', en belle '''humeur''', de bonne '''humeur''', de mauvaise '''humeur''', d’une '''humeur''' agréable, d’une '''humeur''' sombre, d’une '''humeur''' chagrine, d’une '''humeur''' aigre, d’une '''humeur''' inquiète, d’une '''humeur''' bourrue, d’une '''humeur''' massacrante, etc.''
                                                                                    |#* ''Il est toujours de même '''humeur'''.''
                                                                                    |#* ''Il n’a point changé d’'''humeur'''.''
                                                                                    |#* ''De quelle '''humeur''' êtes-vous aujourd’hui ?''
                                                                                    |#* ''Ce sont deux '''humeurs''' bien différentes, bien incompatibles.''
                                                                                    |#* ''Incompatibilité d’'''humeurs'''.''
                                                                                    |#* ''Il a l’'''humeur''' impérieuse.''
                                                                                    |#* ''Être en '''humeur''' de faire quelque chose'' et ''Être d’'''humeur''' à faire quelque chose'' signifient être en disposition de le faire, avec cette différence qu’''Être en '''humeur''''' se dit toujours de la disposition [[actuel]]le, au lieu ''Qu’être d’'''humeur''''' se dit plus ordinairement d’une disposition [[habituel]]le.
                                                                                    |#* ''Il est en '''humeur''' de faire tout ce qu’on veut.''
                                                                                    |#* ''Êtes-vous en '''humeur''' d’aller vous promener, de travailler, de faire quelque chose ?''
                                                                                    |#* ''Il n’est pas d’'''humeur''' à se laisser gouverner.''
                                                                                    |#* ''Je ne suis pas d’'''humeur''' à souffrir vos injures.''
                                                                                    |#* ''Être en '''humeur''' de bien faire'' se dit particulièrement de l’heureuse disposition d’esprit où se trouvent quelquefois ceux qui travaillent d’[[imagination]] et de [[génie]], comme les [[poète]]s, les [[peintre]]s, les [[musicien]]s, etc.
                                                                                    |#* On dit, dans le sens contraire, ''N’être pas en '''humeur'''.'' Ces façons de parler ont vieilli.
                                                                                    |# {{absolument}} Une disposition [[chagrin]]e ; un [[mécontentement]].
                                                                                    |#* ''[…] : légère, étourdie, folle même, elle riait de tout, ne s'intéressait à rien ; confondait la tristesse avec l’'''humeur''', et ne voyait dans une personne affligée qu'une personne ennuyeuse.'' {{source|{{w|Marie-Jeanne Riccoboni}}, ''[[s:Histoire d’Ernestine|Histoire d’Ernestine]]'', 1762, édition ''Œuvres complètes de Mme Riccoboni'', tome I, Foucault, 1818}}
                                                                                    |#* ''On aurait dit qu’il en avait de l’'''humeur''', qu’il était mécontent.'' {{source|{{w|Georges Simenon}}, ''{{w|La Fuite de Monsieur Monde}}'', La Jeune Parque, 1945, ch. 9}}
                                                                                    |#* Par menace, ''Je lui ferai bien passer son '''humeur'''.''
                                                                                    |# [[fantaisie|Fantaisie]], [[caprice]].
                                                                                    |#* ''Alors que le romancier, riche de son don d’ubiquité, peut se dédoubler en autant de personnages que son '''humeur''' daigne susciter au fil des pages, le poète reste fixé à la finitude de son expérience, à la racine de son cri.'' {{source|Jean-Pol Madou, ''Édouard Glissant: de mémoire d’arbres'', 1996, p.16}}
                                                                                    |#* ''Chacun a ses '''humeurs'''.''
                                                                                    |#* ''Avoir des sautes d’'''humeur'''.''
                                                                                    |# {{familier|fr}} ''C’est un homme d’'''humeur''','' C’est un homme capricieux et d’'''humeur''' inégale.
                                                                                    |#* On dit, dans le sens contraire, ''C’est un homme qui n’a point d’'''humeur''', qui est sans '''humeur''', qui a une grande égalité d’'''humeur'''.''
                                                                                    |# {{familier|fr}} ''N’avoir ni '''humeur''' ni honneur,'' se dit d’une personne que les [[affront]]s ne [[toucher|touchent]] plus et qui a perdu tout [[sentiment]] d’honneur.
                                                                                    |# {{vieilli|fr}} Certain [[penchant]] à la [[plaisanterie]], d’une certaine [[originalité]] [[facétieux|facétieuse]]. Dans ce sens, on emploie [[maintenant]] plutôt le mot ''[[humour]]''.
                                                                                    |
                                                                                    |==== {{S|dérivés}} ====
                                                                                    |{{(}}
                                                                                    |* [[agir selon son humeur]]
                                                                                    |* [[bonne humeur]]
                                                                                    |* [[égalité d’humeur]]
                                                                                    |* [[être de mauvaise humeur]]
                                                                                    |* [[geste d’humeur]]
                                                                                    |* [[humeur âcre]]
                                                                                    |* [[humeur aqueuse]]
                                                                                    |* [[humeur bilieuse]]
                                                                                    |* [[humeur compatible]]
                                                                                    |* [[humeur de goutte]]
                                                                                    |* [[humeur flegmatique]]
                                                                                    |* [[humeur froide]]
                                                                                    |* [[humeur incompatible]]
                                                                                    |* [[humeur peccante]]
                                                                                    |* [[humeur vitrée]]
                                                                                    |* [[humoreux]]
                                                                                    |* [[incompatibilité d’humeur]]
                                                                                    |* [[mauvaise humeur]]
                                                                                    |* [[mouvement d’humeur]]
                                                                                    |* [[tempérer son humeur]]
                                                                                    |* [[trouble affectif de l’humeur]]
                                                                                    |* [[trouble de l’humeur]]
                                                                                    |* [[saute d’humeur]]
                                                                                    |{{)}}
                                                                                    |
                                                                                    |==== {{S|apparentés}} ====
                                                                                    |* [[humour]]
                                                                                    |
                                                                                    |==== {{S|hyponymes}} ====
                                                                                    |* ''(pour substance liquide)
                                                                                    |* [[bile]]
                                                                                    |* [[larme]]
                                                                                    |* [[lymphe]]
                                                                                    |* [[salive]]
                                                                                    |* [[sang]]
                                                                                    |* [[sperme]]
                                                                                    |* [[urine]]
                                                                                    |
                                                                                    |==== {{S|traductions}} ====
                                                                                    |{{trad-début|Etat d’esprit plus ou moins durable, particulièrement en ce qu’il est plutôt agréable ou non}}
                                                                                    |* {{T|de}} : {{trad+|de|Humor}}, {{trad+|de|Laune}}, {{trad+|de|Stimmung}}
                                                                                    |* {{T|en}} : {{trad+|en|humour}} {{Grande-Bretagne|nocat=1}} ou {{trad+|en|humor}} {{États-Unis|nocat=1}}, {{trad+|en|mood}}
                                                                                    |* {{T|bm}} : {{trad--|bm|nisɔn}}
                                                                                    |* {{T|ca}} : {{trad+|ca|humor}}
                                                                                    |* {{T|da}} : {{trad-|da|humør}}
                                                                                    |* {{T|es}} : {{trad+|es|humor}}
                                                                                    |* {{T|et}} : {{trad-|et|tuju}}
                                                                                    |* {{T|fi}} : {{trad+|fi|mielenlaatu}}
                                                                                    |* {{T|it}} : {{trad+|it|umore}}
                                                                                    |* {{T|kk}} : {{trad-|kk|көңіл-күй|tr=köñil-küy}}
                                                                                    |* {{T|la}} : {{trad-|la|humor}}
                                                                                    |* {{T|nl}} : {{trad-|nl|gemoedsgesteldheid}}, {{trad+|nl|humeur}}, {{trad+|nl|humor}}, {{trad+|nl|stemming}}
                                                                                    |* {{T|no}} : {{trad-|no|humør}}
                                                                                    |* {{T|pl}} : {{trad+|pl|humor}}, {{trad+|pl|nastrój}}
                                                                                    |* {{T|pt}} : {{trad+|pt|humor}}
                                                                                    |* {{T|ro}} : {{trad+|ro|umor|n}}
                                                                                    |* {{T|se}} : {{trad--|se|latnja}}, {{trad--|se|miella}}, {{trad--|se|miellaláhki}}, {{trad--|se|vuoigŋa}}
                                                                                    |* {{T|sv}} : {{trad+|sv|lynne}}, {{trad+|sv|sinnelag}}, {{trad+|sv|humör}}
                                                                                    |* {{T|tr}} : {{trad+|tr|huy}}
                                                                                    |{{trad-fin}}
                                                                                    |
                                                                                    |{{trad-début|Une disposition chagrine ; un mécontentement}}
                                                                                    |* {{T|en}} : {{trad+|en|temper}}, {{trad+|en|temperament}} ; [[bad]] [[temper]]
                                                                                    |* {{T|ar}} : {{trad-|ar|مزاج}}
                                                                                    |* {{T|sv}} : [[dålig|dåligt]] [[lynne#sv|lynne]]
                                                                                    |{{trad-fin}}
                                                                                    |
                                                                                    |===== {{S|traductions à trier}} =====
                                                                                    |{{trad-début}}
                                                                                    |* {{T|eo}} : {{trad-|eo|humoro}}
                                                                                    |* {{T|fo}} : {{trad-|fo|hýrur}}
                                                                                    |* {{T|io}} : {{trad+|io|humoro}}
                                                                                    |* {{T|is}} : {{trad+|is|skap}}
                                                                                    |* {{T|ja}} : {{trad-|ja|機嫌|R=きげん}} (kigen)
                                                                                    |* {{T|oc}} : {{trad-|oc|umor}}
                                                                                    |* {{T|pap}} : {{trad--|pap|humor}}
                                                                                    |{{trad-fin}}
                                                                                    |
                                                                                    |=== {{S|nom|fr|num=2}} ===
                                                                                    |{{fr-rég|y.mœʁ|préfpron={{h muet|fr}}}}
                                                                                    |'''humeur''' {{h aspiré}}{{pron|y.mœʁ|fr}} {{m}} {{équiv-pour|une femme|humeuse}}
                                                                                    |# Personne qui [[humer|hume]].
                                                                                    |
                                                                                    |==== {{S|traductions}} ====
                                                                                    |{{trad-début}}
                                                                                    |{{trad-fin}}
                                                                                    |
                                                                                    |=== {{S|références}} ===
                                                                                    |* {{Import:DAF8}}
                                                                                    |* Inspiré du ''Dictionnaire encyclopédique Quillet'', Paris, 1934.
                                                                                    |* Inspiré du ''Nouveau Larousse Illustré en 7 volumes'' Paris 1904
                                                                                    |
                                                                                    |=== {{S|prononciation}} ===
                                                                                    |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|y.mœʁ|audio=Fr-humeur.ogg}}
                                                                                    |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-humeur.wav}}
                                                                                    |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-humeur.wav}}
                                                                                    |
                                                                                    |=== {{S|voir aussi}} ===
                                                                                    |* {{WP}}
                                                                                    |
                                                                                    |== {{langue|nl}} ==
                                                                                    |=== {{S|étymologie}} ===
                                                                                    |: {{ébauche-étym|nl}}
                                                                                    |
                                                                                    |=== {{S|nom|nl}} ===
                                                                                    |'''humeur''' {{pron||nl}}
                                                                                    |# [[humeur#fr|Humeur]].
                                                                                    |
                                                                                    |==== {{S|synonymes}} ====
                                                                                    |* [[gemoedsgesteldheid]]
                                                                                    |* [[humor#nl|humot]]
                                                                                    |
                                                                                    |=== {{S|taux de reconnaissance}} ===
                                                                                    |{{nl-taux|100,0|99,5|pourB=100|pourNL=100}}
                                                                                    |
                                                                                    |=== {{S|prononciation}} ===
                                                                                    |* {{écouter|||lang=nl|audio=Nl-humeur.ogg}}
                                                                                    |
                                                                                    |=== {{S|Références}} ===
                                                                                    |{{Références}}""".stripMargin)
}
