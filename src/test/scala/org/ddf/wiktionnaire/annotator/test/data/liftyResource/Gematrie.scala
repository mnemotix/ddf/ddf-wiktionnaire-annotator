/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Gematrie {
  val gematrie = """== {{langue|fr}} ==
                   |=== {{S|étymologie}} ===
                   |: {{date|lang=fr}} {{refnec|lang=fr|Emprunté à l’{{étyl|he|fr|mot=גימטריה|tr={{smcp|gymtryh}}}}, lui même emprunté au {{étyl|grc|fr|mot=γεωμετρία|tr=geômetría|sens=géométrie}}.}}
                   |
                   |=== {{S|nom|fr}} ===
                   |{{fr-rég|ʒe.ma.tʁi}}
                   |'''gématrie''' {{pron|ʒe.ma.tʁi|fr}} {{f}}
                   |# {{lexique|occultisme|fr}} Branche de la [[cabale]] [[juif|juive]], qui s’occupe d’[[interpréter]] sur une base [[mathématique]] les données [[chiffré]]es du texte hébreu de la Bible (chaque lettre de l’alphabet hébreu ayant une valeur numérique).
                   |#* ''À propos de la '''gématrie''', qui est l’interprétation arithmétique de la Bible, le grand rabbin Rubinstein m’a fait observer, en passant, que les disciples du fondateur du christianisme pêchèrent, dans des filets qui ne se rompirent pas, cent cinquante-trois gros poissons. Or, ce nombre correspond au mot hébreu qui signifie « agneau pascal ».'' {{source|{{w|Roger Peyrefitte}}, ''Les Juifs'', quatrième partie, chapitre VI, Éditions Flammarion, Paris, 1965, page 441}}
                   |#* ''La valeur probante de la thèse se joue donc sur la '''gématrie'''. Et c’est là que le bât blesse.'' {{source|La Nouvelle revue française, 1990}}
                   |# {{par ext}} [[valeur|Valeur]] numérique de la somme des lettres d’un mot ou d’un groupe de mots hébreux.
                   |#* ''Sa théologie aussi semble tributaire de traditions retardataires : binitarisme, monarchianisme, '''gématrie''' du nom d’Adam, étymologie de celui de Moïse.'' {{source|''Histoire du christianisme des origines à nos jours'', tome I : ''Le nouveau peuple (des origines a 259)'', Desclée, 2000}}
                   |#* ''La numérologie est un ensemble de croyances et de pratiques fondées sur l’attribution de propriétés aux nombres. Liée à la '''gématrie''', à l’arithmancie, et à la kabbale, elle appartient à la constellation des pseudosciences de l’hermétisme dont se réclame Jacques Grimault, qu’il appelle la « Science des Anciens ».'' {{source|1=Acermendax, ''[http://menace-theoriste.fr/pyramides-et-numerologie/ Pyramides et Numérologie]'' le 22 janvier 2015 sur menace-theoriste.fr}}
                   |==== {{S|synonymes}} ====
                   |* [[guématrie]]
                   |
                   |==== {{S|dérivés}} ====
                   |* [[gématrique]]
                   |
                   |==== {{S|apparentés}} ====
                   |* [[géométrie]]
                   |
                   |==== {{S|traductions}} ====
                   |{{trad-début}}
                   |* {{T|it}} : {{trad+|it|gematria|f}}
                   |{{trad-fin}}
                   |
                   |=== {{S|anagrammes}} ===
                   |* [[égermait]] {{cf|lang=fr|égermer}}
                   |* [[ermitage]]
                   |* [[mègerait]], [[mégerait]] {{cf|lang=fr|méger}}
                   |* [[mergeait]] {{cf|lang=fr|merger}}
                   |
                   |=== {{S|voir aussi}} ===
                   |* {{WP}}""".stripMargin
}
