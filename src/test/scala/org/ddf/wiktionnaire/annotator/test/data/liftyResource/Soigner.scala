package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Soigner {
  val resource = Resource("soigner", "https://fr.wiktionary.org/wiki/", "entry", """== {{langue|fr}}==
                                                                                        |=== {{S|étymologie}} ===
                                                                                        |: Du {{étyl|frk|fr}}. Ou à rapprocher du latin ''[[somniare]]''. Voir ''[[songer]]''.
                                                                                        |
                                                                                        |=== {{S|verbe|fr}} ===
                                                                                        |'''soigner''' {{pron|swa.ɲe|fr}} {{t|fr}} {{conj|grp=1|fr}}
                                                                                        |# [[avoir|Avoir]] [[soin]] de [[quelqu’un]] ou de [[quelque chose]].
                                                                                        |#* ''Gasbieha et sa mère passaient le printemps au Caire, l’été à Alexandrie, et suivaient fréquemment le Pacha à Hélouan, où il '''soignait''' ses rhumatismes.'' {{source|{{w|Out-el-Kouloub}}, ''Zaheira'', dans ''Trois contes de l’Amour et de la Mort'', 1940}}
                                                                                        |#* ''Gravement blessés, tombés au pouvoir de l'adversaire, ils craignent pour leur vie mais '''sont soignés''' par des mains qui se révèlent fraternelles.'' {{source|''Ennemis fraternels, 1914-1915: Hans Rodewald, Antoine Bieisse, Fernand Tailhades : carnets de guerre et de captivité'', édité par Eckart Birnstiel & Rémy Cazals, Presses universitaires du Mirail, 2002, [[quatrième de couverture]].}}
                                                                                        |#* ''Il est '''soigné''' par tous ceux qui l’entourent.''
                                                                                        |#* '''''Soigner''' sa santé.''
                                                                                        |#* '''''Soigner''' un cheval.''
                                                                                        |#* '''''Soigner''' des rosiers.''
                                                                                        |#* '''''Soigner''' un malade''
                                                                                        |#* ''C’est le docteur un tel qui l’a '''soigné''' dans sa dernière maladie.''
                                                                                        |# {{familier|fr}} [[apporter|Apporter]] de l’[[intérêt]] à quelqu’un, dans l’[[espoir]] d’une [[contrepartie]].
                                                                                        |#* ''Il aura bientôt besoin de lui, alors il le '''soigne'''.''
                                                                                        |# [[apporter|Apporter]] de l’[[attention]], du [[soin]] à quelque chose.
                                                                                        |#* ''Il '''soigne''' beaucoup son style.''
                                                                                        |#* ''Ce peintre ne '''soigne''' pas assez les accessoires, les détails.''
                                                                                        |
                                                                                        |==== {{S|synonymes}} ====
                                                                                        |{{(}}
                                                                                        |* [[bichonner]]
                                                                                        |* [[cajoler]]
                                                                                        |* [[câliner]]
                                                                                        |* [[chérir]]
                                                                                        |* [[chouchouter]]
                                                                                        |* [[choyer]]
                                                                                        |* [[combler]]
                                                                                        |* [[coucounner]]
                                                                                        |* [[couver]]
                                                                                        |* [[dorloter]]
                                                                                        |* [[entourer]]
                                                                                        |* [[gâter]]
                                                                                        |* [[mignarder]]
                                                                                        |* [[mignoter]]
                                                                                        |* [[pouponner]]
                                                                                        |* prendre [[soin]] de
                                                                                        |* s’[[occuper]] de
                                                                                        |{{)}}
                                                                                        |
                                                                                        |==== {{S|dérivés}} ====
                                                                                        |*[[resoigner]]
                                                                                        |
                                                                                        |==== {{S|traductions}} ====
                                                                                        |{{trad-début}}
                                                                                        |* {{T|af}} : {{trad-|af|behartig}} ; {{trad-|af|behandel}} ; {{trad-|af|verpleeg}}
                                                                                        |* {{T|de}} : {{trad+|de|versorgen}} ; {{trad+|de|behandeln}} ; {{trad+|de|heilen}} ; {{trad-|de|kurieren}} ; {{trad+|de|pflegen}} ; {{trad+|de|warten}}
                                                                                        |* {{T|en}} : to {{trad+|en|look after}} ; to {{trad+|en|maintain}} ; to {{trad+|en|take care of}} ; to {{trad+|en|care for}} ; to {{trad+|en|attend}} ; to {{trad+|en|cure}} ; to {{trad+|en|treat}} ; to {{trad+|en|heal}} ; to {{trad+|en|remedy}} ; to {{trad+|en|nurse}} ; to {{trad-|en|tend to}} ; to {{trad+|en|care}}
                                                                                        |* {{T|ca}} : {{trad+|ca|curar}} ; {{trad-|ca|assistir malalts}} ; {{trad+|ca|gomboldar}}
                                                                                        |* {{T|hr}} : {{trad-|hr|skrbiti}}
                                                                                        |* {{T|da}} : {{trad-|da|behandle}} ; {{trad+|da|passe}}, {{trad-|da|pleje}}
                                                                                        |* {{T|es}} : {{trad+|es|cuidar}}, {{trad-|es|atender a}} ; {{trad-|es|cuidar de}}, {{trad+|es|curar}}, {{trad+|es|medicar}}
                                                                                        |* {{T|eo}} : {{trad+|eo|flegi}} (1) ; {{trad+|eo|kuraci}} (2) ; {{trad+|eo|prizorgi}} (3)
                                                                                        |* {{T|fo}} : {{trad-|fo|hava í hondum}} ; {{trad-|fo|síggja til}} ; {{trad-|fo|grøða}} ; {{trad-|fo|røkja}}
                                                                                        |* {{T|fi}} : {{trad+|fi|parantaa}} ; {{trad+|fi|hoitaa}}, {{trad+|fi|lääkitä}}
                                                                                        |* {{T|fy}} : {{trad-|fy|ferpleegje}}
                                                                                        |* {{T|igs-gls}} : {{trad--|igs-gls|kura}}
                                                                                        |* {{T|grc}} : {{trad--|grc|ἰατρεύω}}
                                                                                        |* {{T|io}} : {{trad+|io|sorgar}}
                                                                                        |* {{T|id}} : {{trad+|id|awat}} ; {{trad+|id|merawat}}
                                                                                        |* {{T|it}} : {{trad-|it|prendere a cuore}} ; {{trad-|it|guarire}} ; {{trad+|it|curare}}
                                                                                        |* {{T|kk}} : {{trad-|kk|күту|tr=kütüw (1)}}
                                                                                        |* {{T|avk}} : {{trad--|avk|koropé}} (3)
                                                                                        |* {{T|la}} : {{trad-|la|alere}}
                                                                                        |* {{T|yua}} : {{trad--|yua|ts’aakik}}
                                                                                        |* {{T|nl}} : {{trad+|nl|behartigen}} ; {{trad+|nl|verzorgen}} ; {{trad+|nl|behandelen}} ; {{trad+|nl|cureren}} ; {{trad+|nl|verplegen}} ; {{trad+|nl|zorgen}} voor
                                                                                        |* {{T|pap}} : {{trad--|pap|kura}} ; {{trad--|pap|lidia}}
                                                                                        |* {{T|pl}} : {{trad-|pl|zadbać}} ; {{trad+|pl|leczyć}} ; {{trad+|pl|pielęgnować}}
                                                                                        |* {{T|pt}} : {{trad+|pt|providenciar}} ; {{trad+|pt|curar}} ; {{trad-|pt|medicar}} ; {{trad+|pt|cuidar}} ; {{trad+|pt|tratar}}, {{trad+|pt|zelar}}
                                                                                        |* {{T|se}} : {{trad--|se|dikšut}}, {{trad--|se|buoridit}}, {{trad--|se|savvit}}, {{trad--|se|divššodit}}, {{trad--|se|dálkkastit}}, {{trad--|se|dearvvasmahttit}}
                                                                                        |* {{T|sv}} : {{trad+|sv|ansa}} ; {{trad+|sv|sköta}} ; {{trad+|sv|vårda}}
                                                                                        |* {{T|cs}} : {{trad-|cs|pečovat}}, {{trad+|cs|starat se}}, {{trad+|cs|léčit}}
                                                                                        |* {{T|cu}} : {{trad--|cu|врачєвати}}
                                                                                        |* {{T|zu}} : {{trad-|zu|-pholisa}}
                                                                                        |{{trad-fin}}
                                                                                        |{{trad-début|Avoir soin de quelqu’un ou de quelque chose.|1}}
                                                                                        |* {{T|en}} : {{trad+|en|treat}}
                                                                                        |* {{T|kk}} : {{trad-|kk|күту}}
                                                                                        |* {{T|avk}} : {{trad--|avk|ropé}}
                                                                                        |* {{T|pt}} : {{trad-|pt|tratar de}}, {{trad-|pt|cuidar de}}
                                                                                        |* {{T|zdj}} : {{trad--|zdj|-zihira|dif=uzihira}}
                                                                                        |{{trad-fin}}
                                                                                        |
                                                                                        |=== {{S|prononciation}} ===
                                                                                        |* {{pron|swa.ɲe|fr}}
                                                                                        |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|swa.ɲe|audio=Fr-soigner.ogg}}
                                                                                        |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-soigner.wav}}
                                                                                        |
                                                                                        |=== {{S|références}} ===
                                                                                        |* {{Import:DAF8}}""".stripMargin)
}