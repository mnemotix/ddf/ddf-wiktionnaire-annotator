/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object VaccinoSceptique {
  def vaccinoSceptique = """{{voir|vaccino-sceptique}}
                           |== {{langue|fr}} ==
                           |
                           |=== {{S|étymologie}} ===
                           |: {{composé de|vaccino-|sceptique|lang=fr|m=1}}
                           |
                           |=== {{S|adjectif|fr}} ===
                           |{{fr-rég|mf=oui|vak.si.nɔ.sɛp.tik}}
                           |'''vaccinosceptique''' {{pron|vak.si.nɔ.sɛp.tik|fr}} {{mf}} {{ortho1990}}
                           |# ''Variante orthographique de'' [[vaccino-sceptique]].
                           |#* ''Je ne sais pas si en Belgique c’est comme en France, mais on est le pays le pays le plus '''vaccinosceptique'''. Enfin, pas dans le bon sens du terme. Pour moi, le doute est une bonne chose. Mais douter des vaccins, ça ne veut pas dire avoir des soupçons sur le pourquoi on veut nous vacciner. Ce n’est pas la même chose.'' {{source|{{périodique|prénom1=Antonin|nom1=Marsac|titre=« Il est clair que ceux qui sont fortement dans la croyance détestent ce qu’on fait »|journal=La Libre|date=23 janvier 2021|url=https://www.lalibre.be/culture/medias-tele/thomas-durand-acermendax-la-tronche-en-biais-il-y-aura-plein-de-crises-a-l-avenir-qui-necessiteront-que-les-gens-reflechissent-avant-de-parler-600a0f759978e227dfc3ffa3}}}}
                           |
                           |=== {{S|nom|fr}} ===
                           |{{fr-rég|vak.si.nɔ.sɛp.tik}}
                           |'''vaccinosceptique''' {{pron|vak.si.nɔ.sɛp.tik|fr}} {{mf}} {{ortho1990}}
                           |# ''Variante orthographique de'' [[vaccino-sceptique]].
                           |#* {{exemple|lang=fr|Or, selon les prédictions de plusieurs spécialistes interrogés par la NZZ am Sonntag, les '''vaccinosceptiques''' seront probablement à l'origine de nouvelles vagues d'infections dans les prochains mois, provoquant à nouveau la surcharge des hôpitaux.}} {{source|LENA, «Covid-19: en Suisse, la pression sur les vaccinosceptiques s'accentue»,  26 mars2021 <!--
                           |https://www.lefigaro.fr/international/covid-19-en-suisse-la-pression-sur-les-vaccinosceptiques-s-accentue-20210326
                           |--> }}""".stripMargin
}
