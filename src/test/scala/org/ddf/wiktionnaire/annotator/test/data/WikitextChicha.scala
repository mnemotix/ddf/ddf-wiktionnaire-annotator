package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextChicha {

  val chicha = """=== {{S|étymologie}} ===
                 |: ([[#Nom commun 1|''Nom 1'']]) {{date|lang=fr}} De l’{{étyl|arz|fr|mot=شيشة|tr=šīša}}, issu du {{étyl|tr|fr|mot=şişe}}, lui-même issu du {{étyl|fa|fr|mot=شیشه|tr=šīša|sens=[[verre]]}}.
                 |: ([[#Nom commun 2|''Nom 2'']]) {{date|lang=fr}} De l’{{étyl|es|fr|mot=chicha|sens=[[pois chiche]]}}, lui-même du latin ''[[cicer#la|cicer]]''.
                 |
                 |=== {{S|nom|fr|num=1}} ===
                 |{{fr-rég|ʃi.ʃa}}
                 |[[Fichier:Hookah J1.jpg|vignette|Une '''chicha'''.]]
                 |'''chicha''' {{pron|ʃi.ʃa|fr}} {{f}}
                 |# [[pipe à eau|Pipe à eau]] très répandue dans les [[pays]] [[arabe]]s et récemment dans les pays occidentaux.
                 |#* ''Il a fumé la '''chicha''' toute la soirée.''
                 |#* ''Les saveurs sucrées qui émanaient de la fumée des '''chichas''' se mêlaient à celles du thé à la menthe, contribuant à créer une bulle olfactive harmonieuse et enveloppante.'' {{source|{{nom w pc|Guillaume|Musso}}, ''{{w|Central Park (roman)|Central Park}}'', deuxième partie, chapitre 13, {{w|XO éditions}}, 2014, page 208}}
                 |# {{métonymie|fr}} [[bar à chicha#fr|Bar à chicha]].
                 |#* ''Tu viens regarder le match à la '''chicha''' avec moi ce soir ?''
                 |
                 |==== {{S|variantes orthographiques}} ====
                 |* [[shisha]]
                 |
                 |==== {{S|synonymes}} ====
                 |* [[houka]]
                 |* [[narguilé]]
                 |
                 |==== {{S|dérivés}} ====
                 |* [[bar à chicha]]
                 |* [[chicher]]
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début|Pipe à eau|1}}
                 |* {{T|de}} : {{trad+|de|Schischa|f}}
                 |* {{T|es}} : {{trad+|es|narguile|m}}
                 |* {{T|nl}} : {{trad+|nl|waterpijp|mf}}
                 |{{trad-fin}}
                 |
                 |=== {{S|nom|fr|num=2}} ===
                 |{{fr-rég|ʃi.ʃa|pron2=tʃi.tʃa}}
                 |'''chicha''' {{pron|ʃi.ʃa|fr}} ou {{pron|tʃi.tʃa|fr}} {{f}}
                 |# [[boisson|Boisson]] d’Amérique du sud, souvent fermentée, préparée principalement à partir de [[maïs]].
                 |#* ''Et sur cette remarque, chacun de rire et de boire quelques gouttes de '''chicha''' à la santé d’Orellie-Antoine I{{er}}, ex-roi d’Araucanie.'' {{source|{{nom w pc|Jules|Verne}}, {{ws|Les Enfants du capitaine Grant/Partie 1/Chapitre XI|''Les Enfants du capitaine Grant'' - Partie 1 - Chapitre XI}}, 1868}}
                 |#* ''Madame Emilia ! Un litre de '''chicha''', de la bien forte pour nous, et un petit verre, de la doucette, pour le petit ami !'' {{source|{{nom w pc|Luis|Sepúlveda}}, ''Le Monde du bout du monde'', 1989 ; traduit de l’espagnol du Chili par {{nom w pc|François|Maspero}}, 1993, p. 27}}
                 |#* ''La '''chicha''' masticada est faite avec du maïs mastiqué puis recraché pour qu’il fermente.''
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début|Boisson|1}}
                 |{{trad-fin}}
                 |
                 |==== {{S|vocabulaire}} ====
                 |* {{voir thésaurus|fr|boisson}}
                 |
                 |=== {{S|verbe|fr|flexion}} ===
                 |{{fr-verbe-flexion|chicher|ind.ps.3s=oui}}
                 |'''chicha''' {{pron|ʃi.ʃa|fr}}
                 |# ''Troisième personne du singulier du passé simple de'' [[chicher]].
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=fr|Suisse (canton du Valais)|audio=LL-Q150 (fra)-DSwissK-chicha.wav}}
                 |* {{écouter|||lang=fr|audio=LL-Q150 (fra)-WikiLucas00-chicha.wav}}
                 |
                 |=== {{S|voir aussi}} ===
                 |* {{Wikipédia}}""".stripMargin

}
