/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Vacuite {
  def vacuite = """== {{langue|fr}} ==
                  |=== {{S|étymologie}} ===
                  |: {{date|lang=fr}} Du {{étyl|la|fr|vacuitas||[[espace]] [[vide]], [[vacance]] (d’un emploi), [[exemption]], [[absence]]}}.
                  |
                  |=== {{S|nom|fr}} ===
                  |{{fr-rég|va.kɥi.te}}
                  |'''vacuité''' {{pron|va.kɥi.te|fr}} {{f}}
                  |# [[état|État]] de ce qui est [[vide]].
                  |#* ''Dans l'état de '''vacuité''', la membrane muqueuse de l'estomac est d'une couleur grisâtre. Au moment où les aliments s'accumulent dans le réservoir gastrique, cette membrane devient rose, […].'' {{source|Jules Béclard, ''Traité élémentaire de physiologie humaine'', livre 1 : ''Fonctions de nutrition'' 4{{e}} éd., Paris : chez P. Asselin, 1862, p. 92}}
                  |#* ''Or, au lieu du vizir, les ouvriers, rassemblés pour l’accueillir, voient venir à sa place un chef de gendarmes, porteur de sa part d’un message lénifiant : les assurant de sa bonne volonté, le vizir, arguant de la quasi '''vacuité''' des silos de Karnak, promet aux ouvriers le versement prochain d’une quantité de grain correspondant à la moitié d’un salaire.'' {{source|Pierre Grandet, « ''Les grèves de Deîr el-Médînéh'' », dans ''Les régulations sociales dans l'Antiquité'', sous la direction de Michel Molin, Presses universitaires de Rennes, 2006, p. 93}}
                  |#* ''Comment la '''vacuité''' et les ignominies de mon quotidien professionnel faillirent me pousser au fond du gouffre et pourquoi néanmoins je refusai d’y sauter.'' {{source|Alain {{pc|Giraudo}}, ''Le respect qu’on se doit'', 2014.}}
                  |#* ''La tentation est alors grande de conclure à la '''vacuité''' de la classe {{nobr|<nowiki>{1, 1}</nowiki>}}, seuls objets tridimensionnels avec moins de quatre points d’équilibre.'' {{source|François {{pc|Lavallou}}, « Le Gömböc : Cet obscur objet du désir géométrique », ''{{w|Tangente (magazine)|Tangente}}'' {{n°|182}}, mai-juin 2018, page 46.}}
                  |# {{lexique|bouddhisme|fr}} Notion [[bouddhique]] stipulant que tous les phénomènes sont vides (exempts, dénués) d’une [[existence]] propre et autonome.
                  |#* ''Il s’agit en fait d’un ensemble de sutras, une quarantaine, qui traite de la réalisation de la sagesse (prajna) et développe la notion de '''vacuité'''.'' {{source|Pierre {{pc|Crépon}}, ''Les Fleurs de Bouddha: Anthologie du bouddhisme'', 1991.}}
                  |#* ''Ainsi, pour les courants du Grand Véhicule, puisque tout est '''vacuité''', il n’existe pas de différence fondamentale entre le samsâra et le nirvâna. Le nirvâna est, en effet, le samsâra évacué ; et le samsâra, le nirvâna occulté par le voile des apparences.'' {{source|Quentin {{pc|Ludwig}}, ''Le grand livre du bouddhisme'', Éditions Eyrolles, 2012, page 244.}}
                  |
                  |==== {{S|synonymes}} ====
                  |* [[vide]]
                  |
                  |==== {{S|traductions}} ====
                  |{{trad-début}}
                  |* {{T|en}} : {{trad+|en|vacuity}}
                  |* {{T|az}} : {{trad+|az|boşluq}}
                  |* {{T|br}} : {{trad-|br|goulloder|m}}, {{trad-|br|goulloded|m}}
                  |* {{T|oc}} : {{trad-|oc|vacuïtat|f}}
                  |* {{T|pl}} : {{trad+|pl|pustka}}
                  |{{trad-fin}}
                  |
                  |=== {{S|voir aussi}} ===
                  |* {{WP|Śūnyatā|La vacuité au sens bouddhique (Śūnyatā)}}
                  |""".stripMargin
}
