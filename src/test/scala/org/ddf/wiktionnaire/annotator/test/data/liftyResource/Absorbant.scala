/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource


object Absorbant {

  val resource = Resource("absorbant", "https://fr.wiktionary.org/wiki/", "entry",
  """== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: {{date|1740}} Du verbe ''absorber'' issu du latin ''absorbere'', « [[avaler]] ».
    |
    |=== {{S|adjectif|fr}} ===
    |{{fr-accord-cons|ap.sɔʁ.bɑ̃|t}}
    |'''absorbant''' {{pron|ap.sɔʁ.bɑ̃|fr}}
    |# Qui [[absorber|absorbe]].
    |#* ''Une terre '''absorbante'''.''
    |#* ''Des sables '''absorbants'''.''
    |# {{lexique|médecine|pharmacie|fr}} Se dit des [[substance]]s et des [[préparation]]s [[médicinal]]es [[ayant]] la [[propriété]] d’absorber les [[acide]]s qui se [[développer|développent]] [[spontanément]] dans l’[[estomac]]. [[Médicaments]] servant à [[absorber]] les [[liquides]] et les [[gaz]].
    |#* ''Substance, poudre '''absorbante'''.''
    |# {{figuré|fr}} Qui s’[[emparer|empare]] de l’[[esprit]], qui l’[[occuper|occupe]] tout [[entier]].
    |#* ''Pensées, occupations '''absorbantes'''.''
    |# {{figuré|fr}} Qui est [[absorbé]].
    |#* ''La normalienne donnait un simple exposé historique. Superficiellement, tous les enfants avaient l’air aussi '''absorbant''', aussi bénéficiant ; mais, à fixer mon attention, je voyais les phrases tomber différemment sur eux ; un dépit irrésistible me crispait : cette forme de parole ne s’adapte pas à cette forme de tête…'' {{source|{{w|Léon Frapié}}, ''{{ws|La maternelle/Texte entier|La maternelle}}'', Librairie Universelle, 1908}}
    |# {{figuré|fr}} {{rare|fr}} Qualifie une personne désireuse avant tout de faire connaître ses idées.
    |#* ''Samuel, comme on le voit, rentrait dans la classe des gens '''''absorbants''''', — des hommes insupportables et passionnés, chez qui le métier gâte la conversation, et à qui toute occasion est bonne, même une connaissance improvisée au coin d’un arbre ou d’une rue, — fût-ce d’un chiffonnier, — pour développer opiniâtrement leurs idées. — Il n’y a entre les commis voyageurs, les industriels errants, les allumeurs d’affaires en commandite et les poëtes '''''absorbants''''', que la différence de la réclame à la prédication ; le vice de ces derniers est tout à fait désintéressé.'' {{source|{{w|Charles Baudelaire}}, ''La Fanfarlo'', 1847 ; Gallimard, 2012, réédition Folio, page 20}}
    |# {{lexique|théorie des graphes|fr}} Qualifie un [[sous-graphe]] orienté pour lequel il n’existe aucun [[arc]] de son [[graphe]] qui ne lui soit pas un [[arc incident vers l’intérieur]].
    |#* {{exemple|lang=fr}}
    |# ''Élément '''absorbant''' (ou élément permis)'' : {{lexique|mathématiques|fr}} [[élément|Élément]] d’un [[ensemble]] qui, pour une loi de [[composition]] [[interne]] donnée, transforme tous les autres éléments en lui-même lorsqu’il est combiné avec eux par cette loi.
    |#* ''L’élément '''absorbant''' de la multiplication est le zéro.''
    |==== {{S|antonymes}} ====
    |*{{lien|hydrofuge|fr|adjectif}}
    |*{{lien|imperméable|fr|adjectif}}
    |==== {{S|traductions}} ====
    |===== {{S|traductions à trier}} =====
    |{{trad-début}}
    |* {{T|en}} : {{trad+|en|absorbent}}, {{trad+|en|absorbing}}, {{trad+|en|gripping}}
    |* {{T|hr}} : {{trad-|hr|upijajuċi}}, {{trad-|hr|potaknut interesom}}, {{trad-|hr|zanešen}}, {{trad-|hr|obuzet}}
    |* {{T|eo}} : {{trad-|eo|absorba}}
    |* {{T|fc}} : {{trad--|fc|aibchorbaint}}
    |* {{T|io}} : {{trad+|io|absorbiva}}
    |* {{T|id}} : {{trad+|id|penyerap}}
    |* {{T|is}} : {{trad-|is|gleypinn}}
    |* {{T|it}} : {{trad+|it|assorbente}}
    |* {{T|nl}} : {{trad+|nl|absorberend}}
    |* {{T|sv}} : {{trad-|sv|uppsugande}} (1, 2), {{trad+|sv|absorberande}} (1, 2), {{trad+|sv|fängslande}} (4)
    |{{trad-fin}}
    |
    |=== {{S|nom|fr}} ===
    |{{fr-rég|ap.sɔʁ.bɑ̃}}
    |'''absorbant''' {{pron|ap.sɔʁ.bɑ̃|fr}} {{m}}
    |# {{lexique|médecine|pharmacie|fr}} Substance absorbante.
    |#* ''On lui a donné des '''absorbants'''.''
    |# {{lexique|théorie des graphes|fr}} [[sous-graphe|Sous-graphe]] orienté pour lequel il n’existe aucun [[arc]] de son [[graphe]] qui ne lui soit pas un [[arc incident vers l’intérieur]].
    |#* {{exemple|lang=fr}}
    |
    |==== {{S|hyperonymes}} ====
    |* [[sous-graphe]]
    |
    |==== {{S|hyponymes}} ====
    |* [[noyau]]
    |* [[absorbant minimal]]
    |* [[absorbant minimum]]
    |
    |==== {{S|traductions}} ====
    |{{trad-début|Substance absorbante|1}}
    |* {{T|en}} : {{trad+|en|absorbent}}, {{trad+|en|absorbative}}
    |{{ébauche-trad|en}}
    |{{trad-fin}}
    |
    |{{trad-début|Sous-graphe|2}}
    |{{ébauche-trad|en}}
    |{{trad-fin}}
    |
    |=== {{S|verbe|fr|flexion}} ===
    |{{fr-verbe-flexion|absorber|ppr=oui}}
    |'''absorbant''' {{pron|ap.sɔʁ.bɑ̃|fr}}
    |# ''Participe présent du verbe'' [[absorber]].
    |#* ''Nous devons le concéder, c’est à Condillac, non à Locke, que Biran peut reprocher de vertdegriser le sujet en '''absorbant''' toutes ses facultés en une seule, car chez Locke, la séparation entre elles, ainsi que leur originarité, sont évidentes.'' {{source|Raphaël Badawi, mémoire de master 2, disponible sur studylibfr.com}}
    |
    |=== {{S|prononciation}} ===
    |* {{pron-rimes|ap.sɔʁ.bɑ̃|fr}}
    |* {{pron|ap.sɔʁ.bɑ̃|fr}}
    |** {{écouter|lang=fr|France (Paris)|ap.sɔʁ.bɑ̃|audio=Fr-absorbant.ogg}}
    |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-absorbant.wav}}
    |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-absorbant.wav}}
    |* {{écouter|lang=fr|France||audio=LL-Q150 (fra)-Mecanautes-absorbant.wav}}
    |
    |=== {{S|références}} ===
    |* {{Import:DAF8}}
    |
    |=== {{S|voir aussi}} ===
    |* {{Wikipédia}}
    |* {{w|Élément absorbant}}
    |
    |== {{langue|en}} ==
    |=== {{S|étymologie}} ===
    |: {{ébauche-étym|en}}
    |
    |=== {{S|adjectif|en}} ===
    |{{en-adj|pron=æb.ˈsɔɹ.bənt|pronGB=əb.ˈzɔː.bənt}}
    |'''absorbant''' {{pron|əb.ˈzɔː.bənt|en}} <small>ou</small> {{pron|əb.ˈsɔː.bənt|en}} <small>ou</small> {{pron|æb-|en}} {{États-Unis|nocat=1}}
    |# {{variante orthographique de|absorbent|en}}.
    |#* {{exemple|lang=en}}
    |
    |=== {{S|nom|en}} ===
    |{{en-nom-rég|əb.ˈzɔː.bənt|əb.ˈsɔː.bənt}}
    |'''absorbant''' {{pron|əb.ˈzɔː.bənt|en}} <small>ou</small> {{pron|əb.ˈsɔː.bənt|en}} <small>ou</small> {{pron|æb-|en}} {{États-Unis|nocat=1}}
    |# {{variante orthographique de|absorbent|en}}.
    |#* {{exemple|lang=en}}""".stripMargin
  )

}
