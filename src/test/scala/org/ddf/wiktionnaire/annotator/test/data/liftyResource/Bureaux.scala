/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Bureaux {
  val bureaux =
  """== {{langue|fr}} ==
    |=== {{S|nom|fr|flexion}} ===
    |{{fr-rég-x|s=bureau|by.ʁo}}
    |'''bureaux''' {{pron|by.ʁo|fr}} {{m}}
    |# ''Pluriel de'' [[bureau#fr|bureau]].
    |#* ''Il prend à l’avance la peine de prénoter son droit sur un registre spécial tenu dans les '''bureaux''' de la Commission foncière.'' {{source|''Bulletin de la Société de législation comparée'', 1878, volume 7, page 492}}
    |
    |=== {{S|prononciation}} ===
    |* {{écouter|lang=fr|France (Brétigny-sur-Orge)||audio=LL-Q150 (fra)-Pamputt-bureaux.wav}}
    |
    |=== {{S|anagrammes}} ===
    |{{voir anagrammes|fr}}
    |
    |== {{langue|en}} ==
    |=== {{S|nom|en|flexion}} ===
    |'''bureaux''' {{pron||en}}
    |# ''Pluriel de'' [[bureau]].
    |
    |[[Catégorie:Pluriels irréguliers en anglais]]""".stripMargin
}
