package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextAnanas {

  val ananas = """{{voir|Ananas|ananás}}
                 |== {{langue|fr}} ==
                 |=== {{S|étymologie}} ===
                 |: {{date|1578}} Du {{étyl|tpw|fr}}-{{étyl|gn|fr|naná}}. {{date|1554}} ''[[nana]]''.
                 |
                 |=== {{S|nom|fr}} ===
                 |{{fr-inv|a.na.nɑ|pron2=a.na.nas|sp=1}}
                 |[[Image:Pineapple Oahu.jpg|thumb|Un '''ananas'''.]]
                 |[[Image:Ananas comosus Victoria P1190421.jpg|thumb|Fruits d’'''ananas'''.]]
                 |'''ananas''' {{pron|a.na.nɑ|fr}} ou {{pron|a.na.nas|fr}} {{m}}, {{sp}}
                 |# {{plantes|fr}} Plante [[tropical]]e originaire de l’[[Amérique du Sud]], qu’on cultive en [[Europe]] dans des [[serre]]s chaudes, dont les [[tige]]s courtes portent une [[rosette]] de [[feuille]]s [[épaisse]]s, [[sessile]]s, étroitement [[imbriquer|imbriquées]] et munies de [[piquant]]s parfois [[acéré]]s.
                 |# [[faux-fruit|Faux-fruit]] [[comestible]] de cette plante de forme [[conique]] à la peau [[écailleux|écailleuse]] et épaisse et à la [[chair]] jaune et [[sucré]]e.
                 |#* ''S’il restait encore un doute sur la supériorité de l’Inde, par rapport à ses fruits, l’'''ananas''' seul ferait pencher la balance en faveur de son heureuse patrie : il réunit toutes les bonnes qualités et tous les agrémens épars çà et là dans les autres : élégance de la forme, parfum fin et délicat, saveur exquise, suc abondant et distribué également dans tout le fruit, la nature a versé sur lui tous ses trésors.'' {{source|Père M. Perrin, ''Voyage dans l’Indostan'', 1807}}
                 |#* ''L’'''ananas''' est essentiellement cultivé pour son fruit consommé au naturel ou mis en conserve (tranches, morceaux, jus). Les feuilles peuvent être utilisées pour leurs fibres et dans l’alimentation du bétail. La plante entière peut être réduite en farine pour le bétail. On en extrait de l’amidon et de la broméline, mélange d’enzymes utilisé dans l’industrie pharmaceutique.'' {{source|Cirad/Gret/MAE, ''Mémento de l’agronome'', Cirad/Gret/Ministère des Affaires étrangères, Paris, 2002, page 945}}
                 |# {{militaire|fr}} {{term|argot des tranchées}} [[mine|Mine]] explosive de fabrication britannique, pendant la Première Guerre mondiale.
                 |#* ''En réponse, je piquai dans la tranchée anglaise quelques « '''ananas''' », c’est-à-dire des mines qui faisaient songer par leur forme à ce fruit exquis.'' {{source|{{nom w pc|Ernst|Jünger}}, ''{{w|Orages d’acier}}'', 1961 ; traduit de l’allemand par {{nom w pc|Henri|Plard}}, 1970, p. 88}}
                 |
                 |==== {{S|dérivés}} ====
                 |{{(}}
                 |* [[ananas bouteille]]
                 |* [[ananas de mer]]
                 |* [[ananas des bois]]
                 |* [[ananas fossile]]
                 |* [[fraise ananas]]
                 |* [[ananas-bois]]
                 |* [[ananeraie]]
                 |{{)}}
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début|Plante}}
                 |* {{T|conv}} : ''{{trad+|conv|Ananas}}''
                 |* {{T|en}} : {{trad-|en|pineapple tree}}
                 |* {{T|eu}} : {{trad-|eu|anana}}
                 |* {{T|ca}} : {{trad-|ca|ananàs|m}}
                 |* {{T|zh}} : {{trad+|zh|菠萝蜜|tr=bōluómì|tradi=菠蘿蜜}}
                 |* {{T|es}} : {{trad+|es|ananás}}, {{trad+|es|piña|f}}
                 |* {{T|ja}} : {{trad-|ja|パイナップル|R=painappuru}}
                 |* {{T|avk}} : {{trad--|avk|ksago}}
                 |* {{T|nl}} : {{trad+|nl|ananas|m}}
                 |* {{T|pl}} : {{trad+|pl|ananas}}
                 |* {{T|pt}} : {{trad+|pt|abacaxi|m}}, {{trad+|pt|ananás|m}}
                 |* {{T|zdj}} : {{trad--|zdj|mnanasi}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Fruit}}
                 |* {{T|af}} : {{trad+|af|ananas}}, {{trad+|af|pynappel}}
                 |* {{T|sq}} : {{trad+|sq|ananas}}
                 |* {{T|de}} : {{trad+|de|Ananas|m}}
                 |* {{T|en}} : {{trad+|en|pineapple}}
                 |* {{T|ar}} : {{trad-|ar|اناناس|R={{transliterator|ar|اناناس}}}}
                 |* {{T|bci}} : {{trad--|bci|ablɛlɛ}}
                 |* {{T|eu}} : {{trad-|eu|anana}}
                 |* {{T|be}} : {{trad+|be|ананас|R=ananas}}
                 |* {{T|be-x-old}} : {{trad--|be-x-old|ананас|R=ananas}}
                 |* {{T|bs}} : {{trad+|bs|ananas}}
                 |* {{T|bg}} : {{trad+|bg|ананас|R=ananas}}
                 |* {{T|ca}} : {{trad-|ca|pinya}}
                 |* {{T|shy}} : {{trad--|shy|lananaṣ}}
                 |* {{T|zh}} : {{trad+|zh|菠萝|tradi=菠蘿|R=bōluó}}, {{trad+|zh|凤梨|tradi=鳳梨|R=fènglí}}
                 |* {{T|ko}} : {{trad+|ko|파인애플|R=painaepeul}}
                 |* {{T|hr}} : {{trad+|hr|ananas}}
                 |* {{T|da}} : {{trad+|da|ananas}}
                 |* {{T|es}} : {{trad+|es|ananás}}, {{trad+|es|piña}}
                 |* {{T|eo}} : {{trad+|eo|ananaso}}
                 |* {{T|et}} : {{trad+|et|ananass}}
                 |* {{T|fi}} : {{trad+|fi|ananas}}
                 |* {{T|el}} : {{trad+|el|ανανάς|R=ananás}}
                 |* {{T|hi}} : {{trad+|hi|अनन्नास|R=anannāsa}}
                 |* {{T|hu}} : {{trad+|hu|ananász}}
                 |* {{T|id}} : {{trad+|id|nanas}}
                 |* {{T|is}} : {{trad+|is|ananas}}
                 |* {{T|it}} : {{trad+|it|ananas}}
                 |* {{T|ja}} : {{trad-|ja|パイナップル|R=painappuru}}, {{trad-|ja|パイン|R=pain}}
                 |* {{T|lv}} : {{trad+|lv|ananass}}
                 |* {{T|ln}} : {{trad-|ln|ananasi}}
                 |* {{T|lt}} : {{trad+|lt|ananasas}}
                 |* {{T|mk}} : {{trad+|mk|ананас|R=ananas}}
                 |* {{T|ms}} : {{trad+|ms|nanas}}
                 |* {{T|gv}} : {{trad-|gv|annane}}
                 |* {{T|nl}} : {{trad+|nl|ananas|m}}
                 |* {{T|no}} : {{trad+|no|ananas}}
                 |* {{T|fa}} : {{trad+|fa|آناناس|R=ânânâs}}
                 |* {{T|pl}} : {{trad+|pl|ananas|m}}
                 |* {{T|pt}} : {{trad+|pt|abacaxi}}, {{trad+|pt|ananás}}
                 |* {{T|ro}} : {{trad+|ro|ananas}}
                 |* {{T|ru}} : {{trad+|ru|ананас|R=ananas}}
                 |* {{T|sr}} : {{trad+|ru|ананас|R=ananas}}
                 |* {{T|sh}} : {{trad+|sh|ananas}}
                 |* {{T|sk}} : {{trad+|sk|ananás}}
                 |* {{T|sl}} : {{trad+|sl|ananas}}
                 |* {{T|sv}} : {{trad+|sv|ananas}}
                 |* {{T|ta}} : {{trad-|ml|அன்னாசி|tr=aṉṉāçi}}
                 |* {{T|cs}} : {{trad+|cs|ananas}}
                 |* {{T|th}} : {{trad+|th|สับปะรด|R=sàpbpàrót}}
                 |* {{T|tr}} : {{trad+|tr|ananas}}
                 |* {{T|uk}} : {{trad+|uk|ананас|R=ananas}}
                 |* {{T|vi}} : {{trad+|vi|dứa}}
                 |* {{T|gdr}} : {{trad--|gdr|aitörang}}
                 |{{trad-fin}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{pron|a.na.nas|fr}} ou {{pron|a.na.na|fr}}
                 |** {{écouter|France|ɛ̃.n‿a.na.nas|titre=un ananas|audio=Fr-ananas.ogg|lang=fr}}
                 |** {{écouter|Canada {{soutenu|nocat=1}}|a.na.nɑ|lang=fr}}
                 |** {{écouter|Canada {{informel|nocat=1}}|a.na.nɔ|audio=Qc-ananas.ogg|lang=fr}}
                 |* {{écouter||a.na.nɑ|lang=fr|audio=LL-Q150 (fra)-Mathieu Denel WMFr-ananas.wav}}
                 |* {{note}} En France métropolitaine et en Suisse romande, le « S » de ''ananas'' est généralement prononcé, tandis qu’aux Antilles françaises, à la Réunion, en Belgique, en Haïti et au Québec, ce n’est pas le cas (information en provenance de {{WP}}).
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-PJLC-Wiki-ananas.wav}}
                 |* {{écouter|lang=fr|Suisse (Genève)|audio=LL-Q150 (fra)-Nattes à chat-ananas.wav}}
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines1-ananas.wav}}
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines2-ananas.wav}}
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines3-ananas.wav}}
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines5-ananas.wav}}
                 |* {{écouter|lang=fr||audio=LL-Q150 (fra)-Aemines4-ananas.wav}}
                 |* {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-Aemines6-ananas.wav}}
                 |
                 |=== {{S|voir aussi}} ===
                 |* {{WP}}
                 |
                 |=== {{S|références}} ===
                 |* {{R:TLFi}}
                 |* {{R:Meyer}}
                 |
                 |[[Catégorie:Fruits en français]]
                 |[[Catégorie:Broméliacées en français]]
                 |[[Catégorie:-s prononcés /s/ en français]]
                 |
                 |== {{langue|af}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|af|ananás}}.
                 |
                 |=== {{S|nom|af}} ===
                 |[[Image:Split ananas.jpg|thumb|ananas]]
                 |'''ananas''' {{pron||af}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |==== {{S|synonymes}} ====
                 |* {{lien|pynappel|af}}
                 |
                 |[[Catégorie:Fruits en afrikaans]]
                 |
                 |== {{langue|da}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|da|ananás}}.
                 |
                 |=== {{S|nom|da}} ===
                 |'''ananas''' {{pron||da}} {{m}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |[[Catégorie:Fruits en danois]]
                 |
                 |== {{langue|it}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|it|ananás}}.
                 |
                 |=== {{S|nom|it}} ===
                 |{{it-inv|a.na.ˈnas|pron2=a.na.ˈnas}}
                 |'''ananas''' {{pron|ˈa.na.nas|it}} {{m}} {{invar}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |==== {{S|synonymes}} ====
                 |* {{lien|ananasso|it}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{pron|ˈa.na.nas|it}}
                 |* {{pron|a.na.ˈnas|it}}
                 |
                 |[[Catégorie:Fruits en italien]]
                 |
                 |== {{langue|nl}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|nl|ananás}}.
                 |
                 |=== {{S|nom|nl}} ===
                 |'''ananas''' {{pron|ɑ.nɑ.nɑs|nl}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |==== {{S|variantes orthographiques}} ====
                 |* {{lien|annanas|nl}}
                 |
                 |=== {{S|taux de reconnaissance}} ===
                 |{{nl-taux|97,4|97,6|pourB=98|pourNL=97}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=nl|Pays-Bas|ɑ.nɑ.nɑs|audio=Nl-ananas.ogg}}
                 |
                 |=== {{S|Références}} ===
                 |{{Références}}
                 |
                 |
                 |[[Catégorie:Fruits en néerlandais]]
                 |
                 |== {{langue|no}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|no|ananás}}.
                 |
                 |=== {{S|nom|no}} ===
                 |'''ananas''' {{pron||no}} {{m}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |[[Catégorie:Fruits en norvégien]]
                 |
                 |== {{langue|pl}} ==
                 |=== {{S|étymologie}} ===
                 |: {{date|lang=pl}} Du {{étyl|pt|pl|mot=ananás}}.
                 |
                 |=== {{S|nom|pl}} ===
                 |{| class="wikitable" float="right" style="text-align:center; width:60%;"
                 ||+ Déclinaison de ananas (plante)
                 ||-
                 |! scope=row | Cas
                 ||'''Singulier'''
                 ||'''Pluriel'''
                 ||-
                 |! scope=row | Nominatif
                 ||ananas
                 ||[[ananasy]]
                 ||-
                 |! scope=row | Génitif
                 ||[[ananasa]]
                 ||[[ananasów]]
                 ||-
                 |! scope=row | Datif
                 ||[[ananasowi]]
                 ||[[ananasom]]
                 ||-
                 |! scope=row | Accusatif
                 ||ananas
                 ||[[ananasy]]
                 ||-
                 |! scope=row | Instrumental
                 ||[[ananasem]]
                 ||[[ananasami]]
                 ||-
                 |! scope=row | Locatif
                 ||[[ananasie]]
                 ||[[ananasach]]
                 ||-
                 |! scope=row | Vocatif
                 ||[[ananasie]]
                 ||[[ananasy]]
                 ||-
                 ||}
                 |{| class="wikitable" float="right" style="text-align:center; width:60%;"
                 ||+ Déclinaison de ananas (fruit)
                 ||-
                 |! scope=row | Cas
                 ||'''Singulier'''
                 ||'''Pluriel'''
                 ||-
                 |! scope=row | Nominatif
                 ||ananas
                 ||[[ananasy]]
                 ||-
                 |! scope=row | Génitif
                 ||[[ananasa]]
                 ||[[ananasów]]
                 ||-
                 |! scope=row | Datif
                 ||[[ananasowi]]
                 ||[[ananasom]]
                 ||-
                 |! scope=row | Accusatif
                 ||[[ananasa]], ananas
                 ||[[ananasy]]
                 ||-
                 |! scope=row | Instrumental
                 ||[[ananasem]]
                 ||[[ananasami]]
                 ||-
                 |! scope=row | Locatif
                 ||[[ananasie]]
                 ||[[ananasach]]
                 ||-
                 |! scope=row | Vocatif
                 ||[[ananasie]]
                 ||[[ananasy]]
                 ||-
                 ||}
                 |[[Image:Ananas comosus CB76 Trinidad 4412.JPG|thumb|'''ananas''' (plante)]]
                 |[[Image:Pina.jpg|thumb|'''ananas''' (fruit)]]
                 |'''ananas''' {{pron|a.na.nas|pl}} {{m}}
                 |# {{plantes|pl}} [[ananas#fr|Ananas]].
                 |#* ''[[ciocia|Ciocia]] Basia [[robi]] [[dobre]] [[ciasto]] [[z#pl|z]] '''ananasami'''.''
                 |#*: Tante Barbara fait un bon gâteau à l’ananas.
                 |
                 |==== {{S|dérivés}} ====
                 |* [[ananas jadalny]]
                 |* [[ananas siatkowany]]
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=pl|Pologne|a.na.nas|audio=Pl-ananas.ogg}}
                 |
                 |=== {{S|voir aussi}} ===
                 |* {{WP|lang=pl}}
                 |
                 |[[Catégorie:Fruits en polonais]]
                 |
                 |== {{langue|pt}} ==
                 |=== {{S|erreur|pt}} ===
                 |'''ananas''' {{pron|a.na.ˈnas|pt}}
                 |# {{cf|ananás|lang=pt}}.
                 |
                 |== {{langue|sl}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|sl|ananás}}.
                 |
                 |=== {{S|nom|sl}} ===
                 |{{sl-décl-nmd|ananas}}
                 |'''ananas''' {{pron||sl}} {{m|i}}
                 |# [[#fr|Ananas]].
                 |
                 |[[Catégorie:Fruits en slovène]]
                 |
                 |== {{langue|sv}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|sv|ananás}}.
                 |
                 |=== {{S|nom|sv}} ===
                 |{{sv-nom-c-er}}
                 |'''ananas''' {{pron||sv}} {{c}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |[[Catégorie:Fruits en suédois]]
                 |
                 |== {{langue|cs}} ==
                 |=== {{S|étymologie}} ===
                 |:Du {{étyl|pt|cs|ananás}}.
                 |
                 |=== {{S|nom|cs}} ===
                 |{{cs-décl-nom-mi-dur|rad=ananas}}
                 |'''ananas''' {{pron||cs}} {{m|i}}
                 |# [[ananas#fr|Ananas]].
                 |#* ''Jedlá část '''ananas'''u obsahuje průměrně 86% vody, 13% sacharidů, 0,5% vlákniny, 0,3% bílkovin a 0,1% tuků.''
                 |
                 |==== {{S|dérivés}} ====
                 |* {{lien|ananasový|cs}}
                 |* [[ananasovník]]
                 |
                 |=== {{S|voir aussi}} ===
                 |* {{WP|lang=cs}}
                 |
                 |[[Catégorie:Fruits en tchèque]]
                 |
                 |== {{langue|tr}} ==
                 |=== {{S|étymologie}} ===
                 |: {{ébauche-étym|tr}}
                 |
                 |=== {{S|nom|tr}} ===
                 |'''ananas''' {{pron|ɑ.nɑ.nɑs|tr}}
                 |# [[ananas#fr|Ananas]].
                 |
                 |[[Catégorie:Fruits en turc]]
                 |""".stripMargin

}
