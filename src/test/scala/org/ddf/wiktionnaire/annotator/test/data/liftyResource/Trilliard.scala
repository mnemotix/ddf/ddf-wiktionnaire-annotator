/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Trilliard {
  def trilliard = """== {{langue|fr}} ==
                    |{{numéraux_français}}
                    |
                    |=== {{S|étymologie}} ===
                    |: {{composé de|tri-|-illiard|lang=fr|m=1}}.
                    |
                    |=== {{S|nom|fr}} ===
                    |{{fr-rég|tʁi.ljaʁ}}
                    |'''trilliard''' {{pron|tʁi.ljaʁ|fr}} {{m}}, {{cardinal|fr}}
                    |# [[mille|Mille]] [[trillion]]s ou mille [[milliard]]s de milliards, soit 10<sup>21</sup>, c’est-à-dire {{formatnum:1000000000000000000000}}.
                    |
                    |==== {{S|synonymes}} ====
                    |* [[mille milliards de milliards]]
                    |* [[zetta-]]
                    |
                    |==== {{S|dérivés}} ====
                    |* [[trilliardième]]
                    |
                    |==== {{S|vocabulaire}} ====
                    |* [[trillion]]
                    |
                    |==== {{S|traductions}} ====
                    |{{trad-début}}
                    |* {{T|de}} : {{trad+|de|Trilliarde|f}}
                    |* {{T|en}} : {{trad+|en|sextillion}}
                    |* {{T|ja}} : {{trad-|ja|十垓|R=jūgai}}
                    |* {{T|avk}} : {{trad|avk|zungoy}}
                    |* {{T|oc}} : {{trad-|oc|triliard}}
                    |{{trad-fin}}
                    |
                    |=== {{S|prononciation}} ===
                    |* {{écouter|lang=fr|France (Toulouse)||audio=LL-Q150 (fra)-Lepticed7-trilliard.wav}}
                    |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-trilliard.wav}}
                    |
                    |=== {{S|voir aussi}} ===
                    |* {{Annexe|Nombres en français}}
                    |* {{WP|Échelles longue et courte|titre=Échelles longue et courte}}""".stripMargin
}
