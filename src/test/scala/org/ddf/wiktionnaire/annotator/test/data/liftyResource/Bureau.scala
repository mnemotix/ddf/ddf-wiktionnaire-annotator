/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

object Bureau {
  val bureau = """{{voir|Bureau}}
                 |
                 |== {{langue|fr}} ==
                 |=== {{S|étymologie}} ===
                 |: {{date|lang=fr|1150}} ''[[burel]]'' « étoffe grossière » ; {{date|lang=fr|1316}} ''bureau'' « tapis sur lequel on fait des comptes » d'où :
                 |:: {{date|lang=fr|1361}} « la table elle-même où l'on fait les comptes » ;
                 |:: {{date|lang=fr|1495}} « lieu où l'on fait les comptes »
                 |:: {{siècle|XV}} ''tenir bureau'', « tenir audience » ; « pièce, lieu de travail, cabinet »
                 |:: {{siècle|XVI}} « établissement ouvert au public où s'exécute un service d'intérêt collectif (débit, recette, etc.) »
                 |: Soit dérivé de ''[[bure]]'' avec le suffixe ''[[-el]]'' (devenu par la suite ''[[-eau]]'') ; soit dérivé en ''-ellus'' du {{étyl|la|fr|mot=bura}}.
                 |
                 |=== {{S|nom|fr|num=1}} ===
                 |{{fr-rég-x|by.ʁo}}
                 |[[Image:A Bureau From Chippendale's Director.jpg|thumb|'''Bureau''' par Thomas Chippendale.]]
                 |[[Image:Bureau du Connétable, Saint Brélade, Jersey.jpg|thumb|'''Bureau''' du Connétable de Jersey.]]
                 |'''bureau''' {{pron|by.ʁo|fr}} {{m}}
                 |# {{meubles|fr}} [[meuble|Meuble]] à [[tiroir]]s et à [[tablette]]s où l’on [[enfermer|enferme]] des [[papier]]s et sur [[lequel]] on [[écrit]].
                 |#* ''L’ameublement était sommaire : un '''bureau''', un fauteuil de cuir, des classeurs métalliques, deux petites tables dont l’une supportait une presse à copier.'' {{source|Claude Orval, ''Un Sursis pour Hilda'', Librairie des  Champs-Élysées, 1960, première partie, chapitre premier}}
                 |# {{par ext|fr}} [[table|Table]] [[destinée]] au [[travail]].
                 |#* ''J’ai mis ces papiers sur mon '''bureau'''.''
                 |# {{par ext|fr}} Tout [[endroit]] où [[travaillent]] [[habituellement]] des [[employé]]s, des [[commis]], des [[gens]] d’[[affaire]]s, etc.
                 |#* ''C'est plutôt le contraire qui est vrai ou qui du moins renferme la plus forte part de vérité : maintes fois l'aristocratie mérita le nom de « kakistocratie », dit-il, qu'il soit empereur ou chef de '''bureau''', dont Léopold de Ranke se sert dans son histoire.'' {{source|{{w|Elisée Reclus}}, ''L'évolution, la révolution et l'idéal anarchique'', Paris : chez P.-V. Stock, 6e édition 1906, page 69}}
                 |#* ''Depuis l’âge de seize ans, il avait été condamné à passer toute la journée dans une sorte de prison, et il n’avait cessé de maudire son sort. « Oh ! quelques années entre le '''bureau''' et la tombe ! gémissait-il. Un homme appelle son temps le temps dont il peut disposer comme il veut. Quand donc aurai-je du temps à moi ? »<br
                 |/>Et maintenant qu’il avait ce temps, il ne savait qu’en faire.'' {{source|{{w|Julien Green}}, ''Charles Lamb'', dans ''Suite anglaise'', 1927, Le Livre de Poche, page 72}}
                 |#* ''Le Père Directeur, qui était venu me rejoindre dans mon '''bureau''' de l’hôpital , ne me répondit rien et continua à tirer silencieusement sur son long fume-cigare.'' {{source|{{w|Albert Gervais}}, ''Æsculape dans la Chine en révolte'', Gallimard, 1953, page 31}}
                 |# [[établissement|Établissement]] ou [[succursale]] où des employés [[effectuent]] un [[travail]] plutôt [[intellectuel]].
                 |#* ''Le '''bureau''' d’Halifax de Atlas Shipping était chargé de noliser les bateaux par vingt ou quarante à la fois. La contrebande était transportée, sur ces navires, de Saint-Pierre au Canada et aux États-Unis.'' {{source|Jean-Pierre Andrieux, ''La Prohibition… Cap sur Saint-Pierre et Miquelon'', traduit de l’anglais par Georges Poulet, Ottawa : Éditions Leméac, 1983, page 171}}
                 |# {{vieilli|fr}} {{désuet|fr}} [[étoffe|Étoffe]] de [[bure]].
                 |#* ''Ils (les vilains) étaient vêtus de surcottes de gros '''bureau''', tombant à mi-jambes, et portaient un chapeau clabaud garni d’une Notre-Dame de plomb.'' {{source|Henri Troyat, ''Le jugement de Dieu'', Livre de Poche, page 83}}
                 |# {{vieilli|fr}} {{désuet|fr}} [[lieu|Lieu]] où se [[distribuent]] les [[billet]]s d’[[entrée]] pour un [[spectacle]] ou pour tout autre lieu dans lequel on n’est [[admis]] qu’en [[payant]]. On dit aujourd'hui « [[guichet]] ».
                 |#* ''Jouer à '''bureaux''' fermés. Prendre un billet au '''bureau'''. La foule assiégeait les '''bureaux'''. '''Bureau''' de location, '''bureau''' des suppléments, etc.''
                 |# {{par ext|fr}} L'[[ensemble]] des [[employé]]s [[eux-mêmes]] qui [[travaillent]] dans un '''bureau'''.
                 |#* ''Chaque '''bureau''' est composé d’un chef, d’un sous-chef, etc. Chef de '''bureau'''.''
                 |# [[établissement|Établissements]] qui [[dépendre|dépendent]], la plupart, de l’[[administration]] [[public|publique]], qui sont destinés à quelque [[service public]].
                 |#* ''Ce volume, petit in-32, semblable à un ''Annuaire du '''Bureau''' des Longitudes'', est recouvert de drap pelucheux comme un bréviaire de chanoine, […].'' {{source|{{Citation/Jules Verne/Claudius Bombarnac/1892|6}}}}
                 |# Certain nombre de personnes tirées d’une [[assemblée]] pour s’occuper [[spécialement]] d’une ou de [[plusieurs]] [[affaire]]s dont elles doivent ensuite rendre compte à l’[[assemblée générale]].
                 |#* ''L’assemblée fut partagée, divisée en tant de '''bureaux'''. Procéder à la formation des '''bureaux'''. Les membres de chaque '''bureau'''.''
                 |#* ''Le président, le secrétaire d’un '''bureau'''. Cette proposition fut renvoyée à l’examen des '''bureaux'''. Le rapporteur du premier '''bureau''', du deuxième '''bureau'''.''
                 |# Réunion du [[président]], des [[vice-président]]s, des [[secrétaire]]s et aussi, dans certains cas, du [[trésorier]], en parlant des [[assemblée]]s [[législative]]s ou [[électoral]]es, d’une [[académie]], du [[comité]], d’une [[société]], d’un [[syndicat]], d’une [[association]], etc.
                 |#* ''Il appartient au ministère principalement intéressé de préparer le projet de loi de ratification afin qu’il puisse être soumis au Conseil d’État puis inscrit à l’ordre du jour du conseil des ministres et enfin déposé sur le '''bureau''' de l’Assemblée nationale ou du Sénat dans le délai fixé par la loi d’habilitation.'' {{source|{{W|Secrétariat général du gouvernement}} et {{W|Conseil d’État (France)|Conseil d’État}}, ''Guide de légistique, 3<sup>e</sup> version'', {{W|La Documentation française}}, 2017, {{ISBN|978-2-11-145578-8}}|lien=https://www.legifrance.gouv.fr/Media/Droit-francais/Guide-legistique/Guide_Legistique_2017_PDF}}
                 |#* ''Les rouges étaient aux cent coups. Ils s'étaient réunis en comité restreint dans la cuisine du cafetier. Autour de la toile cirée, le '''bureau''' de la Libre-Pensée cogitait ferme.'' {{source|Arlette Aguillon, ''Le puits aux frelons'', Éditions de l'Archipel, 2009, chap. 20}}
                 |# {{lexique|informatique|fr}} Surface de travail des [[environnement graphique|environnements graphiques]], permettant d’accéder aux différentes ressources de l'appareil ([[logiciel]]s, [[fichier]]s, [[périphérique]]s...), et sur laquelle s’ouvrent les [[fenêtre]]s des applications.
                 |#* ''La disposition au sein du '''bureau''' des différentes icônes, raccourcis ou volets peut être gérée par le système d'exploitation ou bien prise en charge par l'utilisateur.'' {{source|Joël Green, ''J'apprends à me servir de Windows seven'', page 108, www.informatique-bureautique.com, 2010}}
                 |# {{lexique|audiovisuel|fr}} Délégation décentralisée d’une agence de presse, d’un journal ou d’un organisme de radiodiffusion ou de télévision.
                 |
                 |==== {{S|synonymes}} ====
                 |* [[burlingue]]
                 |
                 |==== {{S|dérivés}} ====
                 |{{(}}
                 |* [[avoir un deuxième bureau]]
                 |* [[avoir un second bureau]]
                 |* [[buraliste]]
                 |* [[bureau arabe]], {{info lex|histoire}} Juridiction militaire qui était établie en Algérie, dans certains districts habités par les indigènes
                 |* [[bureau-chef]] {{Canada|nocat=1}}
                 |* [[bureau d’esprit]] {{ironique|nocat=1}} {{vieilli|nocat=1}} Société où l’on s’occupe habituellement de littérature et d’ouvrages d’esprit
                 |* [[bureau de bienfaisance]]
                 |* [[bureau de change]]
                 |* [[bureau de nourrices]] : établissement où l’on se charge de procurer des nourrices.
                 |* [[bureau de placement]]
                 |* [[bureau de poste]]
                 |* [[bureau de tabac]]
                 |* [[bureau de vote]]
                 |* [[bureau des pleurs]]
                 |* [[bureaucrate]]
                 |* [[bureaucratie]]
                 |* [[bureaucratique]]
                 |* [[bureaucratisme]]
                 |* [[bureautique]]
                 |* [[burelain]]
                 |* [[burlingue]]
                 |* [[deuxième bureau]]
                 |* [[employé de bureau]]
                 |* [[être sur le bureau]]
                 |* [[garçon de bureau]]
                 |* [[homme de bureau]], celui qui se consacre tout entier et avec passion à sa vie de '''bureau'''
                 |* [[lampe de bureau]]
                 |* [[party de bureau]]
                 |* [[passer sous le bureau]]
                 |* [[sans bureau fixe]]
                 |{{)}}
                 |
                 |==== {{S|apparentés}} ====
                 |* [[bure]]
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début|Meuble à tiroirs et à tablettes où l’on enferme des papiers et sur lequel on écrit|1}}
                 |* {{T|de}} : {{trad+|de|Sekretär|m}}, {{trad+|de|Schreibtisch|m}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Table destinée au travail|2}}
                 |* {{T|de}} : {{trad+|de|Schreibtisch|m}}
                 |* {{T|en}} : {{trad+|en|desk}}
                 |* {{T|ar}} : {{trad+|ar|مَكْتَب|tr=maktab}}
                 |* {{T|br}} : {{trad+|br|taol-skrivañ}}
                 |* {{T|ca}} : {{trad-|ca|escriptori}}, {{trad+|ca|taula}}
                 |* {{T|zh}} : {{trad+|zh|书桌|tr=shūzhuō|tradi=書桌}}, {{trad-|zh|办公桌|tr=辦公桌|tradi=bàngōngzhuō}}
                 |* {{T|ko}} : {{trad+|ko|책상|tr=chaeg sang|tradi=冊床}}
                 |* {{T|es}} : {{trad+|es|despacho|m}}
                 |* {{T|eo}} : {{trad-|eo|skribotablo}}, {{trad-|eo|skribtablo}}
                 |* {{T|fc}} : {{trad|fc|poiye de traivaiye}}
                 |* {{T|el}} : {{trad+|el|γραφείο}}
                 |* {{T|crg}} : {{trad|crg|biiroo}}
                 |* {{T|nl}} : {{trad+|nl|bureau|n}}, {{trad+|nl|schrijftafel|f}}
                 |* {{T|pt}} : {{trad+|pt|escrivaninha|f}}
                 |* {{T|ru}} : {{trad+|ru|рабочий стол|m|tr=rabóc̆ij stol}}
                 |* {{T|se}} : {{trad|se|čállinbeavdi}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Tout endroit où travaillent habituellement des employés, des commis, des gens d’affaires, etc|3}}
                 |* {{T|de}} : {{trad+|de|Büro|n}}, {{trad+|de|Dienstraum|m}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Établissement ou succursale où des employés effectuent un travail plutôt intellectuel|4}}
                 |* {{T|de}} : {{trad+|de|Büro|n}}, {{trad+|de|Kontor|n}}
                 |* {{T|en}} : {{trad+|en|office}}
                 |* {{T|br}} : {{trad+|br|burev}}, {{trad+|br|gwazva}}
                 |* {{T|ca}} : {{trad+|ca|administració}}
                 |* {{T|zh}} : {{trad+|zh|办公室|tr=bàngōngshì|tradi=辦公室}}
                 |* {{T|fi}} : {{trad+|fi|toimisto}}
                 |* {{T|gallo}} : {{trad|gallo|buriao}}
                 |* {{T|el}} : {{trad+|el|γραφείο}}
                 |* {{T|id}} : {{trad+|id|penjabatan}}
                 |* {{T|kab}} : {{trad|kab|lbiru}}
                 |* {{T|lb}} : {{trad-|lb|Büro|n}}
                 |* {{T|nl}} : {{trad+|nl|kantoor|n}}
                 |* {{T|wes}} : {{trad|wes|biro}}
                 |* {{T|pt}} : {{trad+|pt|escritório|m}}
                 |* {{T|ru}} : {{trad+|ru|бюро}}, {{trad+|ru|офис|m|tr=ofis}}
                 |* {{T|se}} : {{trad|se|virgi}}, {{trad|se|doaimmahat}}, {{trad|se|juogus}}, {{trad|se|kantuvra}}, {{trad|se|konttor}}
                 |* {{T|cs}} : {{trad+|cs|úřad}}, {{trad-|cs|byro}}
                 |* {{T|tr}} : {{trad+|tr|ofis}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Étoffe de bure|5}}
                 |* {{T|de}} : {{trad+|de|Wollstoff|m}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Lieu où se distribuent les billets d’entrée pour un spectacle|6}}
                 |* {{T|de}} : {{trad+|de|Schalter|m}},
                 |{{trad-fin}}
                 |
                 |{{trad-début|L'ensemble des employés eux-mêmes qui travaillent dans un bureau|7}}
                 |* {{T|de}} : {{trad+|de|Bürobelegschaft|f}}, {{trad+|de|Büro|n}}, {{trad+|de|Belegschaft|n}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Établissements qui dépendent, la plupart, de l’administration publique, qui sont destinés à quelque service public|8}}
                 |* {{T|de}} : {{trad+|de|Büro|n}}, {{trad+|de|Amtsstube|f}}, {{trad+|de|Amt|n}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Certain nombre de personnes tirées d’une assemblée pour s’occuper spécialement d’une ou de plusieurs affaires dont elles doivent ensuite rendre compte à l’assemblée générale.|9}}
                 |* {{T|de}} : {{trad+|de|Ausschuss|m}}, {{trad+|de|Kommission|f}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Réunion du président, des vice-présidents, des secrétaires et aussi, dans certains cas, du trésorier, en parlant des assemblées législatives ou électorales...|11}}
                 |* {{T|de}} : {{trad+|de|Vorstand|m}}
                 |* {{T|en}} : {{trad+|en|board}}
                 |* {{T|ca}} : {{trad+|ca|mesa}}
                 |* {{T|kab}} : {{trad|kab|lbiru}}
                 |* {{T|lt}} : {{trad+|lt|taryba}}
                 |* {{T|nl}} : {{trad+|nl|bestuur|m}}
                 |* {{T|nb}} : {{trad+|nb|styre|m}}
                 |* {{T|zdj}} : {{trad|zdj|biro}}
                 |* {{T|sv}} : {{trad+|sv|styre}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|Surface de travail des environnements graphiques, permettant d’accéder aux différentes ressources de l'appareil (logiciels, fichiers, périphériques...)|11}}
                 |* {{T|de}} : {{trad+|de|Desktop|m}}
                 |* {{T|en}} : {{trad+|en|desktop}}
                 |{{trad-fin}}
                 |
                 |{{trad-début|traductions à trier}}
                 |* {{T|af}} : {{trad+|af|buro}}, {{trad+|af|kantoor}}
                 |* {{T|en}} : {{trad+|en|desk}} (1), {{trad+|en|office}} (3, 4, 5), {{trad-|en|law office}}, {{trad+|en|bureau}} (1,5, "marriage bureau", "bureau of …"), {{trad+|en|cabinet}} (7),
                 |* {{T|ar}} : {{trad+|ar|مكتب|m|tr=maktabe pour مكتب}}
                 |* {{T|br}} : {{trad+|br|taol-skrivañ}} (1, 2, 3), {{trad+|br|burev}} (4, 6, 7), {{trad+|br|poellgor}} (8, 9)
                 |* {{T|ca}} : {{trad-|ca|escriptori}}, {{trad+|ca|taula}} (3) ; {{trad+|ca|despatx}}, {{trad-|ca|oficina}} (4, 6) ; {{trad+|ca|mesa}} (8, 9)
                 |* {{T|shy}} : {{trad|shy|lbiru}}
                 |* {{T|da}} : {{trad+|da|kontor}}
                 |* {{T|es}} : {{trad+|es|escritorio|m}} (1), {{trad+|es|oficina|f}} (3)
                 |* {{T|eo}} : {{trad-|eo|advokatejo}} (4), {{trad-|eo|oficejo}} (4), {{trad+|eo|kontoro}} (5)
                 |* {{T|fo}} : {{trad-|fo|skrivstova}}
                 |* {{T|fc}} : {{trad|fc|poiye de traivaiye}} (3), {{trad|fc|tâle de traivaiye}} (4, 6, 7)
                 |* {{T|fy}} : {{trad-|fy|kantoar}}
                 |* {{T|ga}} : {{trad-|ga|oifig}}
                 |* {{T|gallo}} : {{trad|gallo|buriao}}
                 |* {{T|el}} : {{trad+|el|γραφείο}} (3, 4, 6, 8)
                 |* {{T|hu}} : {{trad+|hu|iroda}}
                 |* {{T|io}} : {{trad-|io|skribo-tablo}} (1), {{trad+|io|kontoro}} (3)
                 |* {{T|it}} : {{trad+|it|scrivania}}, {{trad+|it|studio}}, {{trad+|it|ufficio}}, {{trad-|it|uffizio|m}}, {{trad+|it|officio|m}}, {{trad-|it|offizio|m}}, {{trad-|it|ufizio|m}}, {{trad+|it|uficio|m}}, {{trad-|it|oficio|m}}, {{trad-|it|ofizio|m}}
                 |* {{T|kab}} : {{trad|kab|lbiru}}
                 |* {{T|fsl}} : {{wikisign|bureau}}
                 |* {{T|nl}} : {{trad+|nl|advocatenkantoor}}, {{trad+|nl|bureau}}, {{trad+|nl|bureel}}, {{trad+|nl|kantoor}}
                 |* {{T|no}} : {{trad+|no|kontor}}, {{trad+|no|skrivebord}} (3)
                 |* {{T|oc}} : {{trad-|oc|burèu}}, {{trad-|oc|gabinet}}, {{trad-|oc|escritòri}}
                 |* {{T|pap}} : {{trad|pap|kantor}}, {{trad|pap|ofisina}}
                 |* {{T|pl}} : {{trad+|pl|biuro}}, {{trad+|pl|urząd}}
                 |* {{T|pt}} : {{trad+|pt|repartição|f}}
                 |* {{T|ro}} : {{trad+|ro|birou}}
                 |* {{T|ru}} : {{trad+|ru|офис}}
                 |* {{T|srn}} : {{trad|srn|kantoro}}
                 |* {{T|sv}} : {{trad+|sv|byrå}}, {{trad+|sv|kontor}}
                 |* {{T|sw}} : {{trad+|sw|afisi}}
                 |* {{T|cs}} : {{trad+|cs|stůl}} (meuble)
                 |* {{T|tr}} : {{trad+|tr|büro}}
                 |* {{T|zu}} : {{trad-|zu|ihhovisi}}, {{trad-|zu|ilihhovisi}}
                 |{{trad-fin}}
                 |
                 |=== {{S|nom|fr|num=2}} ===
                 |{{fr-rég-x|by.ʁo}}
                 |'''bureau''' {{pron|by.ʁo|fr}} {{m}}
                 |# {{région}} {{désuet|fr}} [[petit|Petit]]e [[meule]].
                 |#* ''Lorsque le foin était sec, on le ramassait en petites meules hémisphériques, les "'''bureaux'''".'' {{source|Jacques Lambert, ''Campagnes et paysans des Ardennes - 1830-1914'', 1988, page 235}}
                 |# {{région}} {{désuet|fr}} [[hutte|Hutte]], [[cabane]].
                 |#* ''Lorsque le ver est prêt à donner sa soie, son appétit s'arrête. C'est l'instant de la montée. L'éleveur doit alors s'occuper à « cabaner », opération qui consiste à disposer en forme de '''bureaux''' ou cabanes, dans les intervalles compris entre les claies, des branches de genêts et de bruyères, dans lesquels les insectes montent et choisissent leur place pour commencer à filer leur cocon.'' {{source|D. de Prat, ''Nouveau manuel complet de filature; 1{{ère}} partie: Fibres animales & minérales'', Encyclopédie Roret, 1914}}
                 |
                 |==== {{S|synonymes}} ====
                 |{{(}}
                 |; meule
                 |* [[buriau]]
                 |* [[meulon]]
                 |* [[moyette]]
                 |
                 |; hutte
                 |* [[borie]]
                 |{{)}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{pron-rimes|by.ʁo|fr}}
                 |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|by.ʁo|audio=Fr-bureau.ogg}}
                 |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Lyokoï-bureau.wav}}
                 |* {{écouter|lang=fr|France (Toulouse)|audio=LL-Q150 (fra)-Lepticed7-bureau.wav}}
                 |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Ltrlg-bureau.wav}}
                 |* {{écouter|lang=fr|France (Muntzenheim)|audio=LL-Q150 (fra)-0x010C-bureau.wav}}
                 |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-bureau.wav}}
                 |* {{écouter|lang=fr|Suisse (canton du Valais)||audio=LL-Q150 (fra)-DSwissK-bureau.wav}}
                 |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-bureau.wav}}
                 |* {{écouter|France||lang=fr|audio=LL-Q150 (fra)-Mecanautes-bureau.wav}}
                 |
                 |=== {{S|adjectif|fr}} ===
                 |'''bureau''' {{pron|by.ʁo|fr}} {{invar}}
                 |# De la couleur de la [[bure]], [[marron]] foncé.
                 |#* ''Allons ! dit-elle d’un air tout fier et tout content, retourne de ce pas au bord de la rivière ; descends-la jusqu’à ce que tu entendes bêler ; et où tu verras un agneau '''bureau''', tu verras aussitôt ton frère : si cela n’arrive pas comme je te le dis, je te tiens quitte de ta parole.'' {{source|{{w|George Sand}}, ''{{w|La Petite Fadette}}'', chapitre IX, 1849.}}
                 |
                 |=== {{S|voir aussi}} ===
                 |* {{WP}}
                 |* {{Vikidia}}
                 |* {{Thésaurus|fr|couleur}}
                 |
                 |=== {{S|références}} ===
                 |* {{R:FranceTerme}}
                 |* {{R:TLFi}}
                 |* {{Import:DAF8}}
                 |
                 |== {{langue|en}} ==
                 |=== {{S|étymologie}} ===
                 |: Du {{étyl|fr|en|mot=bureau}}.
                 |
                 |=== {{S|nom|en}} ===
                 |{{en-nom-rég|ˈbjʊə.rəʊ|p2=bureaux}}
                 |'''bureau''' {{pron|ˈbjʊə.rəʊ|en}}
                 |# {{gall|en}} [[agence|Agence]], [[bureau#fr|bureau]].
                 |# {{gall|en}} [[bureau#fr|Bureau]] (meuble).
                 |# {{meubles|en}} [[commode|Commode]] {{États-Unis|en}}.
                 |
                 |==== {{S|apparentés}} ====
                 |* [[bureaucrat#Anglais|bureaucrat]]
                 |* [[bureaucracy#Anglais|bureaucracy]]
                 |* [[bureaucratic#Anglais|bureaucratic]]
                 |* [[bureaucratize#Anglais|bureaucratize]] {{États-Unis|nocat=1}}/{{Grande-Bretagne|nocat=1}} ou [[bureaucratise#Anglais|bureaucratise]] {{Grande-Bretagne|nocat=1}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=en|États-Unis <!-- précisez svp la ville ou la région -->|ˈbjʊə.rəʊ|audio=En-us-bureau.ogg}}
                 |
                 |== {{langue|nl}} ==
                 |=== {{S|étymologie}} ===
                 |: Du {{étyl|fr|nl|mot=bureau}}.
                 |
                 |=== {{S|nom|nl}} ===
                 |{{nl-nom|n.s=bureau|n.pl=bureaus|dim.s=bureautje|dim.pl=bureautjes}}
                 |'''bureau''' {{pron||nl}} {{n}}
                 |# {{meubles|nl}} [[bureau#fr|Bureau]].
                 |# {{term|lieu|nl}} [[bureau#fr|Bureau]], [[cabinet]].
                 |# [[poste de police|Poste de police]]), [[commissariat]].
                 |#* ''iemand naar het '''bureau''' brengen''
                 |#*: conduire quelqu’un au poste
                 |
                 |==== {{S|synonymes}} ====
                 |* [[bureel]] ''(inusité aux Pays-Bas)''
                 |* [[kantoor]]
                 |* [[schrijftafel]]
                 |
                 |==== {{S|dérivés}} ====
                 |{{(}}
                 |* [[adresbureau]]
                 |* [[advertentiebureau]]
                 |* [[adviesbureau]]
                 |* [[arbeidsbureau]]
                 |* [[architectenbureau]]
                 |* [[atoombureau]]
                 |* [[bagagebureau]]
                 |* [[bedrijfsbureau]]
                 |* [[bemiddelingsbureau]]
                 |* [[bevolkingsbureau]]
                 |* [[bureauagenda]]
                 |* [[bureaublad]]
                 |* [[bureaucraat]]
                 |* [[bureaucratie]]
                 |* [[bureauhengst]]
                 |* [[bureauhoofd]]
                 |* [[bureaukalender]]
                 |* [[bureaukast]]
                 |* [[bureaula]]
                 |* [[bureaulade]]
                 |* [[bureaulamp]]
                 |* [[bureaulandschap]]
                 |* [[bureauredacteur]]
                 |* [[bureausoftware]]
                 |* [[bureaustoel]]
                 |* [[castingbureau]]
                 |* [[cilinderbureau]]
                 |* [[classificatiebureau]]
                 |* [[consultancybureau]]
                 |* [[consultatiebureau]]
                 |* [[detacheringsbureau]]
                 |* [[detectivebureau]]
                 |* [[districtsbureau]]
                 |* [[escortbureau]]
                 |* [[evenementenbureau]]
                 |* [[expertisebureau]]
                 |* [[fotobureau]]
                 |* [[headhuntersbureau]]
                 |* [[hoofdbureau]]
                 |* [[huisvestingsbureau]]
                 |* [[huwelijksbureau]]
                 |* [[incassobureau]]
                 |* [[informatiebureau]]
                 |* [[ingenieursbureau]]
                 |* [[interimbureau]]
                 |* [[kamerverhuurbureau]]
                 |* [[klachtenbureau]]
                 |* [[kredietbureau]]
                 |* [[mailbureau]]
                 |* [[marktonderzoekbureau]]
                 |* [[marktonderzoeksbureau]]
                 |* [[mediabureau]]
                 |* [[merkenbureau]]
                 |* [[modellenbureau]]
                 |* [[octrooibureau]]
                 |* [[onderzoekbureau]]
                 |* [[onderzoeksbureau]]
                 |* [[outplacementbureau]]
                 |* [[passagebureau]]
                 |* [[patentbureau]]
                 |* [[persbureau]]
                 |* [[plaatsingsbureau]]
                 |* [[planbureau]]
                 |* [[politbureau]]
                 |* [[politiebureau]]
                 |* [[projectbureau]]
                 |* [[ratingbureau]]
                 |* [[reclamebureau]]
                 |* [[reisbureau]]
                 |* [[relatiebemiddelingsbureau]]
                 |* [[relatiebureau]]
                 |* [[rijksbureau]]
                 |* [[schrijfbureau]]
                 |* [[stembureau]]
                 |* [[studiebureau]]
                 |* [[terugkeerbureau]]
                 |* [[toeristenbureau]]
                 |* [[transferbureau]]
                 |* [[uitzendbureau]]
                 |* [[verkeersbureau]]
                 |* [[vertaalbureau]]
                 |* [[voorlichtingsbureau]]
                 |* [[woningbureau]]
                 |* [[zorgbureau]]
                 |{{)}}
                 |
                 |=== {{S|taux de reconnaissance}} ===
                 |{{nl-taux|98,6|98,0|pourB=98|pourNL=99}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=nl|Pays-Bas|audio=Nl-bureau.ogg}}
                 |* {{écouter|lang=nl|Pays-Bas (partie continentale) (Wijchen)|audio=LL-Q7411 (nld)-Robin van der Vliet-bureau.wav}}
                 |
                 |=== {{S|Références}} ===
                 |{{Références}}
                 |""".stripMargin
}
