package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Jeune {
  val resource = Resource("jeune","https://fr.wiktionary.org/wiki/","entry","""{{voir/jeune}}
                                                                    |=={{langue|fr}}==
                                                                    |=== {{S|étymologie}} ===
                                                                    |: {{date|1080}} Du {{étyl|frm|fr}}, de l’{{étyl|fro|fr|juefne}}, {{lien|''juene''|fro}}, {{lien|''joene''|fro}}, du {{étyl|fr|la}} populaire {{recons|jovene}}, altération du {{étyl|la|fr|mot=juvenis}} de même sens.
                                                                    |
                                                                    |=== {{S|adjectif|fr}} ===
                                                                    |{{fr-rég|ʒœn|mf=1}}
                                                                    |[[Image:Young Lion Mara.JPG|vignette|Un '''jeune''' lion. (1)]]
                                                                    |'''jeune''' {{pron|ʒœn|fr}} {{mf}}
                                                                    |# Qui est dans une [[phase]] au [[commencement]] de sa [[vie]] ou de son [[développement]] ; qui n’est guère [[avancé]] en [[âge]], en parlant des humains, des animaux ou des végétaux.
                                                                    |#* ''J’aurais pu […] accepter les offres engageantes des '''jeunes''' et jolies Mangavériennes, avoir là des enfants bronzés qui auraient grandi libres et heureux sous le chaud soleil de la Polynésie.'' {{source|{{w|Alain Gerbault}}, ''À la poursuite du soleil'', tome 1, ''De New-York à Tahiti'', 1929}}
                                                                    |#* ''Plus d’un '''jeune''' instituteur stagiaire a retrouvé dans le siècle celle pour qui s’échafaudaient ses odes et ses sonnets.'' {{source|{{Citation/Jean Rogissart/Passantes d’Octobre/1958|}}}}
                                                                    |# {{term|parfois}} Qui n’a pas l’âge [[habituel]] par rapport aux [[emploi]]s, aux [[dignité]]s qu’on ne donne [[ordinairement]] qu’à des hommes faits ou à des personnes [[déjà]] avancées en âge.
                                                                    |#* ''Il est trop '''jeune''' pour un emploi si important.''
                                                                    |#* ''Il a été élu académicien bien '''jeune'''.''
                                                                    |#* ''Il fut maréchal de France très '''jeune'''.''
                                                                    |# Qui [[posséder|possède]] des [[caractéristique|caractéristiques]] de la [[jeunesse]].
                                                                    |#* ''Malgré son âge, elle a su rester '''jeune'''.''
                                                                    |# Qui a encore quelque chose de l’[[ardeur]], de la [[vivacité]] et de l’[[agrément]] de la jeunesse.
                                                                    |# Qui est [[propre]] à une personne dans la jeunesse.
                                                                    |#* ''De '''jeunes''' désirs. — De '''jeunes''' ardeurs.''
                                                                    |#* ''Cette pensée enflammait son '''jeune''' courage.''
                                                                    |# {{familier|fr}} Qui ne [[seoir|sied]] qu’à des personnes jeunes.
                                                                    |#* ''Cette couleur est '''jeune'''. Cela fait '''jeune'''.''
                                                                    |#* ''Cette couleur est trop '''jeune''' pour moi.'',
                                                                    |# Qui est [[nouveau]], qui n’[[exister|existe]] que depuis [[récemment]].
                                                                    |#* ''Premier problème, le jeu vidéo reste un secteur '''jeune'''. Suffisamment '''jeune''', en tout cas, pour que les pionniers d’hier soient les dirigeants d’aujourd’hui.'' {{source|{{Lien web |auteur=William Audureau |url=https://www.lemonde.fr/pixels/article/2018/10/18/la-difficile-question-de-la-charge-de-travail-dans-l-industrie-du-jeu-video_5371361_4408996.html |titre=La difficile question de la charge de travail dans l’industrie du jeu vidéo |éditeur=''Le Monde'' |en ligne le=18 octobre 2018}}}}
                                                                    |# Qui [[manquer|manque]] d’[[expérience]], de [[maturité]] ; qui est [[étourdi]], [[évaporer|évaporé]].
                                                                    |#* ''Mon Dieu, qu’il est '''jeune'''!''
                                                                    |# {{spéc}} {{term|Eaux & Forêts}} Qualifie un [[taillis]], des [[baliveau]]x, d’un [[an]].
                                                                    |
                                                                    |==== {{S|synonymes}} ====
                                                                    |* [[enfantin]]
                                                                    |* [[juvénile]]
                                                                    |* [[nouveau]]
                                                                    |* [[petit]]
                                                                    |* [[récent]]
                                                                    |
                                                                    |==== {{S|antonymes}} ====
                                                                    |* [[âgé]]
                                                                    |* [[ancien]]
                                                                    |* [[vieux]]
                                                                    |
                                                                    |==== {{S|dérivés}} ====
                                                                    |{{(}}
                                                                    |* [[bec-jeune]]
                                                                    |* [[cantal jeune]]
                                                                    |* [[coup de jeune]]
                                                                    |* [[étoile jeune]]
                                                                    |* [[jeune âge]]
                                                                    |* [[jeune femme]]
                                                                    |* [[jeune fille]]
                                                                    |* [[jeune homme]]
                                                                    |* [[jeune lait]]
                                                                    |* [[jeune loup aux dents longues]]
                                                                    |* [[jeune marié]]
                                                                    |* [[jeune mariée]]
                                                                    |* [[jeune pousse]]
                                                                    |* [[jeune premier]]
                                                                    |* [[jeune saison]]
                                                                    |* [[jeunement]]
                                                                    |* [[jeunes ans]]
                                                                    |* [[jeunes années]]
                                                                    |* [[jeunes gens]]
                                                                    |* [[jeunesse]]
                                                                    |* [[jeunet]]
                                                                    |* [[jeuniser]]
                                                                    |* [[jeunisme]]
                                                                    |* [[jeuniste]]
                                                                    |* [[jeunot]]
                                                                    |* [[rajeunir]]
                                                                    |{{)}}
                                                                    |
                                                                    |==== {{S|phrases}} ====
                                                                    |* [[à jeune cheval, vieux cavalier]]
                                                                    |* [[si jeune ma buse]] ! {{plais|nocat=1}}
                                                                    |
                                                                    |==== {{S|traductions}} ====
                                                                    |{{trad-début}}
                                                                    |* {{T|af}} : {{trad+|af|jonk}}
                                                                    |* {{T|de}} : {{trad+|de|jung}}, {{trad+|de|jugendlich}}
                                                                    |* {{T|en}} : {{trad+|en|young}}
                                                                    |* {{T|ang}} : {{trad+|ang|geong}}
                                                                    |* {{T|ba}} : {{trad--|ba|йәш}}
                                                                    |* {{T|bs}} : {{trad-|bs|mlad}}
                                                                    |* {{T|br}} : {{trad+|br|yaouank}}
                                                                    |* {{T|ca}} : {{trad+|ca|jove}}
                                                                    |* {{T|zh}} : {{trad+|zh|年轻|tradi=年輕|tr=niánqīng}}
                                                                    |* {{T|ko}} : {{trad+|ko|젊다|tr=jeomda}}
                                                                    |* {{T|kw}} : {{trad-|kw|yowynk}}
                                                                    |* {{T|co}} : {{trad-|co|ghjovanu|m}}, {{trad-|co|ghjovana|f}}
                                                                    |* {{T|hr}} : {{trad+|hr|mlad}}
                                                                    |* {{T|da}} : {{trad+|da|ung}}
                                                                    |* {{T|es}} : {{trad+|es|joven}}
                                                                    |* {{T|eo}} : {{trad-|eo|juna}}
                                                                    |* {{T|fo}} : {{trad-|fo|ungur}}
                                                                    |* {{T|fi}} : {{trad+|fi|nuori}}
                                                                    |* {{T|gd}} : {{trad-|gd|òg}}
                                                                    |* {{T|ga}} : {{trad+|ga|óg}}
                                                                    |* {{T|gag}} : {{trad--|gag|genç}}
                                                                    |* {{T|cy}} : {{trad+|cy|ieuanc}}, {{trad+|cy|ifanc}}
                                                                    |* {{T|ka}} : {{trad-|ka|ახალგაზრდა|R=axalgazrda}}
                                                                    |* {{T|el}} : {{trad+|el|νέος|tr=nêoss}}, {{trad+|el|νεαρός}}, {{trad+|el|μικρός}}
                                                                    |* {{T|grc}} : {{trad--|grc|ἡβός|tr=hebos}}
                                                                    |* {{T|he}} : {{trad+|he|צעיר}}
                                                                    |* {{T|hbo}} : {{trad--|hbo|צעיר|dif=צָעִיר}} {{f}}
                                                                    |* {{T|hu}} : {{trad+|hu|fiatal}}
                                                                    |* {{T|sah}} : {{trad--|sah|эдэр}}
                                                                    |* {{T|io}} : {{trad-|io|-yun-}}
                                                                    |* {{T|id}} : {{trad+|id|muda}}, {{trad+|id|pemuda}}
                                                                    |* {{T|ia}} : {{trad-|ia|juvenes}}, {{trad-|ia|juvene}}
                                                                    |* {{T|is}} : {{trad+|is|ungur}}
                                                                    |* {{T|it}} : {{trad+|it|giovane}}
                                                                    |* {{T|ja}} : {{trad+|ja|若い|tr=wakai}}
                                                                    |* {{T|krc}} : {{trad--|krc|джаш}}
                                                                    |* {{T|kk}} : {{trad+|kk|жас|tr=jas}}
                                                                    |* {{T|kjh}} : {{trad--|kjh|час}}
                                                                    |* {{T|kg}} : {{trad--|kg|toko}}
                                                                    |* {{T|ky}} : {{trad+|ky|жаш}}
                                                                    |* {{T|kum}} : {{trad--|kum|яш}}
                                                                    |* {{T|ku}} : {{trad+|ku|ciwan}}
                                                                    |* {{T|lv}} : {{trad+|lv|jauns}}
                                                                    |* {{T|lt}} : {{trad+|lt|jaunas}}
                                                                    |* {{T|ms}} : {{trad+|ms|muda}}
                                                                    |* {{T|mn}} : {{trad+|mn|залуу}}
                                                                    |* {{T|nl}} : {{trad+|nl|jong}}, {{trad+|nl|jeugdig}}, {{trad+|nl|pril}}
                                                                    |* {{T|nog}} : {{trad--|nog|яс}}
                                                                    |* {{T|no}} : {{trad+|no|ung}}
                                                                    |* {{T|oc}} : {{trad+|oc|joen}}, {{trad+|oc|jove}}, {{trad-|oc|joine}}
                                                                    |* {{T|pap}} : {{trad--|pap|yòn}}, {{trad--|pap|hoben}}
                                                                    |* {{T|fa}} : {{trad+|fa|جوان|tr=javân}}
                                                                    |* {{T|pcd}} : {{trad--|pcd|jònne}}
                                                                    |* {{T|pl}} : {{trad+|pl|młody}}
                                                                    |* {{T|pt}} : {{trad+|pt|jovem}}
                                                                    |* {{T|ro}} : {{trad+|ro|tînăr}}, {{trad+|ro|june}}
                                                                    |* {{T|ru}} : {{trad+|ru|молодой}}
                                                                    |* {{T|se}} : {{trad--|se|nuorra}}
                                                                    |* {{T|ses}} : {{trad--|ses|saatara}}
                                                                    |* {{T|srn}} : {{trad--|srn|yongu}}
                                                                    |* {{T|sv}} : {{trad+|sv|ung}}, {{trad-|sv|ungdomlig}}
                                                                    |* {{T|tl}} : {{trad-|tl|batà}}
                                                                    |* {{T|crh}} : {{trad--|crh|yaş}}, {{trad--|crh|genç}}
                                                                    |* {{T|tt}} : {{trad+|tt|яшь}}
                                                                    |* {{T|cs}} : {{trad+|cs|mladý}} {{m}}
                                                                    |* {{T|cv}} : {{trad--|cv|çамрăк}}
                                                                    |* {{T|kim}} : {{trad--|kim|ниит}}
                                                                    |* {{T|tsolyáni}} : {{trad--|tsolyáni|láisan}}
                                                                    |* {{T|tr}} : {{trad+|tr|genç}}
                                                                    |* {{T|tk}} : {{trad-|tk|ýaş}}
                                                                    |* {{T|vi}} : {{trad+|vi|trẻ}}
                                                                    |* {{T|zu}} : {{trad+|zu|-sha}}
                                                                    |{{trad-fin}}
                                                                    |
                                                                    |=== {{S|nom|fr}} ===
                                                                    |{{fr-rég|ʒœn}}
                                                                    |'''jeune''' {{pron|ʒœn|fr}} {{mf}}
                                                                    |# Jeune [[personne]].
                                                                    |#* ''Le '''jeune''', viens par ici !''
                                                                    |# {{spéc}} Accompagné d'un article défini ou d'un adjectif possessif, sert à interpeller un jeune pour un interlocuteur d'une autre génération.
                                                                    |#* ''T'as encore l'air fatigué, le '''jeune'''. Veux-tu bien me dire ce que tu fais de tes nuits?'' {{source|{{nom w pc|David|Goudreault}}, ''La bête à sa mère'', Stanké, 2015, p. 152.}}
                                                                    |# {{spéc}} [[cadet|Cadet]] de sa [[lignée]].
                                                                    |#* ''Pline le '''jeune'''.''
                                                                    |#* ''Dubois '''jeune''', pharmacien.''
                                                                    |
                                                                    |==== {{S|synonymes}} ====
                                                                    |* [[jeunot]] {{péjoratif|nocat=1}}
                                                                    |
                                                                    |==== {{S|dérivés}} ====
                                                                    |* [[jeune de banlieue]]
                                                                    |
                                                                    |==== {{S|apparentés}} ====
                                                                    |* [[djeune]]
                                                                    |* [[djeun]]
                                                                    |
                                                                    |==== {{S|phrases}} ====
                                                                    |* [[six jeunes m’abusent]] {{plais|nocat=1}}
                                                                    |
                                                                    |==== {{S|traductions}} ====
                                                                    |{{trad-début}}
                                                                    |* {{T|ca}} : {{trad+|ca|jove}}
                                                                    |* {{T|zh}} : {{trad-|zh|年轻人|tradi=年輕人|R=nián qīng rén}}
                                                                    |* {{T|ko}} : {{trad+|ko|젊은이|tr=jeolmeuni}}
                                                                    |* {{T|es}} : {{trad+|es|joven}}, {{trad+|es|chaval}}, {{trad+|es|muchacho}}
                                                                    |* {{T|eo}} : {{trad-|eo|junulo}} ''(homme ou femme)'', {{trad-|eo|junulino}} ''(femme)''
                                                                    |* {{T|hbo}} : {{trad--|hbo|נער|dif=נַעַר}} {{m}}
                                                                    |* {{T|io}} : {{trad+|io|yuno}}
                                                                    |* {{T|ja}} : {{trad+|ja|若者|tr=wakamono}}
                                                                    |* {{T|oc}} : {{trad+|oc|jove}}, {{trad-|oc|jovent}}
                                                                    |* {{T|pcd}} : {{trad--|pcd|jòn·ne}}
                                                                    |* {{T|pt}} : {{trad+|pt|jovens}}
                                                                    |* {{T|se}} : {{trad--|se|nuorra}}
                                                                    |* {{T|zdj}} : {{trad--|zdj|shaɓaɓi}}, {{trad--|zdj|mwana}}
                                                                    |* {{T|tr}} : {{trad+|tr|genç}}
                                                                    |{{trad-fin}}
                                                                    |{{trad-début|Jeune personne|1}}
                                                                    |* {{T|en}} : {{trad+|en|youth}}
                                                                    |* {{T|zdj}} : {{trad--|zdj|shaɓaɓi}}, {{trad--|zdj|mwana}}
                                                                    |* {{T|tr}} : {{trad+|tr|genç}}
                                                                    |{{trad-fin}}
                                                                    |
                                                                    |=== {{S|verbe|fr|flexion}} ===
                                                                    |{{fr-verbe-flexion|jeuner|ind.p.1s=oui|ind.p.3s=oui|sub.p.1s=oui|sub.p.3s=oui|imp.p.2s=oui}}
                                                                    |'''jeune''' {{pron|ʒœn|fr}}
                                                                    |# ''Première personne du singulier de l’indicatif présent du verbe'' [[jeuner]].
                                                                    |# ''Troisième personne du singulier de l’indicatif présent du verbe'' [[jeuner]].
                                                                    |# ''Première personne du singulier du subjonctif présent du verbe'' [[jeuner]].
                                                                    |# ''Troisième personne du singulier du subjonctif présent du verbe'' [[jeuner]].
                                                                    |# ''Deuxième personne du singulier de l’impératif du verbe'' [[jeuner]].
                                                                    |
                                                                    |=== {{S|prononciation}} ===
                                                                    |* {{pron|ʒœn|fr}}
                                                                    |** {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->||audio=Fr-jeune.ogg}}* {{écouter||ʒœn|lang=fr|audio=LL-Q150 (fra)-Guilhelma-jeune.wav}}
                                                                    |** {{écouter||ʒœn|lang=fr|audio=LL-Q150 (fra)-Jules78120-jeune.wav}}
                                                                    |** {{écouter|lang=fr|France (Paris)|audio=LL-Q150 (fra)-GrandCelinien-jeune.wav}}
                                                                    |** {{écouter|lang=fr|France (Massy)|audio=LL-Q150 (fra)-X-Javier-jeune.wav}}
                                                                    |** {{écouter|lang=fr|France|audio=LL-Q150 (fra)-Fhala.K-jeune.wav}}
                                                                    |* Français méridional : {{pron|ˈʒœ.nə|fr}}
                                                                    |* {{écouter|lang=fr|Suisse (canton du Valais)|audio=LL-Q150 (fra)-DSwissK-jeune.wav}}
                                                                    |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Poslovitch-jeune.wav}}
                                                                    |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-jeune.wav}}
                                                                    |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-jeune.wav}}
                                                                    |
                                                                    |
                                                                    |
                                                                    |
                                                                    |
                                                                    |==== {{S|paronymes}} ====
                                                                    |* [[jeûne]] {{pron|ʒøn|fr}}
                                                                    |
                                                                    |=== {{S|anagrammes}} ===
                                                                    |* [[enjeu]]
                                                                    |
                                                                    |=== {{S|références}} ===
                                                                    |* {{R:DAF8}})""".stripMargin)
}
