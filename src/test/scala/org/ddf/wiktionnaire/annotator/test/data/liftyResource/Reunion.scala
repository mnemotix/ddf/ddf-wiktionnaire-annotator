/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Reunion {
  lazy val resource =  Resource("réunion", "https://fr.wiktionary.org/wiki/", "entry", content)

    val content = """{{voir/reunion}}
                    |== {{langue|fr}} ==
                    |=== {{S|étymologie}} ===
                    |: {{siècle|XV}} Attesté ''[[reunion]]'' en {{étyl|frm|fr}}, {{composé de|lang=fr|re-|union}} ou de ''[[réunir]]'' {{R|TLFi}}.
                    |
                    |=== {{S|nom|fr}} ===
                    |{{fr-rég|ʁe.y.njɔ̃}}
                    |[[Image:Staff meeting (3).jpg|vignette|Une '''réunion'''. (sens 5)]]
                    |'''réunion''' {{pron|ʁe.y.njɔ̃|fr}} {{f}}
                    |# [[action|Action]] de [[rapprocher]], de [[réunir]] des [[partie]]s qui avaient été [[diviser|divisées]], [[désunir|désunies]], [[isolé]]es, ou [[résultat]] de cette action.
                    |#* ''La '''réunion''' des lèvres d’une plaie.''
                    |# {{figuré|fr}} [[réconciliation|Réconciliation]].
                    |#* ''La '''réunion''' des deux partis.''
                    |#* ''Il voulut opérer la '''réunion''' de l’église grecque à l’église romaine.''
                    |# En parlant de [[fief]]s, de [[domaine]]s, etc., action de [[rejoindre]] une chose [[démembrer|démembrée]] au tout dont elle faisait [[partie]] ; ou action de [[joindre]] pour la [[premier|première]] fois une chose à une autre.
                    |#* ''La '''réunion''' de la Bourgogne, de la Normandie au domaine royal.''
                    |#* ''La '''réunion''' de la Lorraine à la France.''
                    |# Action de [[rassembler]] ce qui est [[épars]] ou le résultat de cette action.
                    |#* ''En effet, il n'y a pas si longtemps que la '''réunion''' des deux éléments de ce titre — préhistoire et Amérique — aurait suscité beaucoup de réticence chez plus d'un américaniste.'' {{source|Marc-R. Sauter, en préface de ''Préhistoire de l'Amérique'', par Salvator Canals Frau, Paris : Payot, 1953, page 5}}
                    |#* ''La '''réunion''' de tous ces petits ruisseaux forme une rivière.''
                    |#* ''La '''réunion''' de ces preuves, de ces faits établit son droit.''
                    |# [[assembler|Assemblée]] de personnes.
                    |#* ''Former une '''réunion'''.
                    |#* ''Une '''réunion''' de savants, de gens de lettres.''
                    |#* ''Il vient souvent à nos '''réunions'''.''
                    |#* ''Un lieu de '''réunion'''.'' ''Une salle de '''réunion'''.''
                    |#* '''''Réunion''' publique'' : Assemblée ouverte où l’on discute quelque question d’ordre politique, moral, économique, etc.
                    |#* '''''Réunion''' électorale'' : Assemblée de citoyens qui préparent une élection.
                    |#* ''Droit de '''réunion''''' : Droit accordé aux citoyens de se réunir pour traiter de matières politiques, économiques, sociales, etc.
                    |#* ''Le droit de '''réunion''' est souvent restreint par des lois ou des règlements de police.''
                    |# {{lexique|mathématiques|fr}} Réunion [[ensembliste]], ensemble contenant les éléments des ensembles considérés, synonyme de [[union]].
                    |#* ''L’ensemble des nombres réels est la '''réunion''' de l’ensemble des réels négatifs et de l’ensemble des réels positifs.''
                    |# {{lexique|sports hippiques|fr}} Ensemble des courses hippiques se déroulant sur un hippodrome au cours d'une même journée.
                    |
                    |==== {{S|hyponymes}} ====
                    |* [[Grenelle]]
                    |* [[lac-à-l’épaule]]
                    |
                    |==== {{S|dérivés}} ====
                    |{{(}}
                    |* [[e-réunion]]
                    |* [[liberté de réunion]]
                    |* [[réunion bilan]] ''ou'' [[réunion-bilan]]
                    |* [[réunion de bilan]]
                    |* [[réunion de cadrage]]
                    |* [[réunion de famille]]
                    |* [[réunion par téléphone]]
                    |* [[réunion préparatoire]]
                    |* [[réunion téléphonique]]
                    |* [[réunionisme]]
                    |* [[réunionite]]
                    |* [[réunionnel]]
                    |* [[réunionner]]
                    |* [[réunionnite]]
                    |* [[viol en réunion]]
                    |* [[vol en réunion]]
                    |{{)}}
                    |
                    |==== {{S|traductions}} ====
                    |{{trad-début}}
                    |* {{T|af}} : {{trad+|af|vergadering}}
                    |* {{T|de}} : {{trad+|de|Versammlung}}, {{trad+|de|Zusammenkunft}}, {{trad+|de|Besprechung}}
                    |* {{T|en}} : {{trad+|en|gathering}}, {{trad+|en|meeting}}, {{trad+|en|assembly}}, {{trad+|en|reunion}},{{trad+|en|union}}
                    |* {{T|eu}} : {{trad-|eu|bilerak}}
                    |* {{T|bs}} : {{trad-|bs|sastanka}}
                    |* {{T|br}} : {{trad+|br|bodadeg}}
                    |* {{T|ca}} : {{trad+|ca|reunió}}, {{trad+|ca|sessió}}
                    |* {{T|da}} : {{trad-|da|forsamling|c}}, {{trad+|da|møde|n}}
                    |* {{T|es}} : {{trad+|es|entrevista}}
                    |* {{T|eo}} : {{trad-|eo|kunveno}}
                    |* {{T|fi}} : {{trad+|fi|kokous}}
                    |* {{T|fy}} : {{trad-|fy|gearkomste}}
                    |* {{T|gd}} : {{trad-|gd|coinneamh}}
                    |* {{T|io}} : {{trad+|io|asemblo}}
                    |* {{T|it}} : {{trad+|it|convegno}}
                    |* {{T|cmn}} : {{trad-|zh|再联合|tr=zài lián hé|tradi=再聯合}}, {{trad-|zh|再结合|tr=zài jié hé|tradi=再結合}}, {{trad-|zh|再会合|tr=zài huì hé|tradi=再會合}}, {{trad+|zh|重聚|tr=chóng jù}}
                    |* {{T|nl}} : {{trad+|nl|bijeenkomst}}, {{trad+|nl|meeting}}, {{trad+|nl|vergadering}}, {{trad+|nl|samenkomst}}
                    |* {{T|no}} : {{trad+|no|møte}}
                    |* {{T|pl}} : {{trad+|pl|zebranie}}, {{trad+|pl|konwent}}
                    |* {{T|pt}} : {{trad+|pt|reunião|f}}, {{trad+|pt|assembleia|f}}
                    |* {{T|srn}} : {{trad--|srn|komakandra}}, {{trad--|srn|takmakandra}}
                    |* {{T|sv}} : {{trad+|sv|församling}}, {{trad+|sv|möte}}, {{trad+|sv|sammankomst}}, {{trad+|sv|sammanträde}}
                    |* {{T|tr}} : {{trad+|tr|toplantı}}
                    |{{trad-fin}}
                    |
                    |{{trad-début|Assemblée de personnes}}
                    |* {{T|de}} : {{trad+|de|Versammlung|f}}
                    |* {{T|en}} : {{trad+|en|meeting}}
                    |* {{T|ar}} : {{trad+|ar|اجتماع|tr=ijtimāʿ}}
                    |* {{T|es}} : {{trad+|es|reunión|f}}
                    |* {{T|gallo}} : {{trad--|gallo|assembllée}}
                    |* {{T|kk}} : {{trad-|kk|жиналыс|tr=jıynalıs}}
                    |* {{T|swb}} : {{trad--|swb|ɓundjilio|tr=bundjilio}}
                    |* {{T|cmn}} : {{trad+|zh|会议|tr=huì yì|tradi=會議}}
                    |* {{T|nl}} : {{trad+|nl|vergadering}}, {{trad+|nl|bijeenkomst}}, {{trad+|nl|samenkomst}}
                    |* {{T|pln}} : {{trad--|pln|reunión}}
                    |* {{T|pt}} : {{trad+|pt|reunião|f}}
                    |* {{T|ru}} : {{trad+|ru|собрание}}
                    |* {{T|se}} : {{trad--|se|čoahkkin}}, {{trad--|se|čoagganeapmi}}, {{trad--|se|čoakkán}}, {{trad--|se|deaivvadeapmi}}
                    |* {{T|zdj}} : {{trad--|zdj|mwonano}}, {{trad--|zdj|ɓundjilio|tr=bundjilio}}, {{trad--|zdj|mtsanganiho}}, {{trad--|zdj|trengwe}}
                    |* {{T|cs}} : {{trad+|cs|schůze}}
                    |{{trad-fin}}
                    |
                    |==== {{S|homophones|fr}} ====
                    |* [[Réunion]]
                    |
                    |=== {{S|prononciation}} ===
                    |* {{pron|ʁe.y.njɔ̃|fr}}
                    |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|ʁe.y.njɔ̃|audio=Fr-réunion.ogg}}
                    |* {{écouter|lang=fr|France (Toulouse)||audio=LL-Q150 (fra)-Lepticed7-réunion.wav}}
                    |
                    |=== {{S|références}} ===
                    |*{{Import:DAF8}}
                    |{{Références}}
                    |
                    |=== {{S|voir aussi}} ===
                    |* Traduction en langue des signes française : {{wikisign|réunion}}
                    |""".stripMargin
}
