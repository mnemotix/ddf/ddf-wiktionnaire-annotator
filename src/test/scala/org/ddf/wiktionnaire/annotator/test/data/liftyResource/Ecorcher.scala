package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Ecorcher {
 val resource = Resource("fermer","https://fr.wiktionary.org/wiki/","entry","""== {{langue|fr}}==
                                                                                   |=== {{S|étymologie}} ===
                                                                                   |: Du {{étyl|la|fr}} ''[[firmo#la|firmare]]'' (« [[affermir]], solidifier », d’où « [[fortifier]] »), le glissement de sens vers celui de « [[clore]] » a pu se faire dès le bas latin d’autant que son dérivé, ''[[confirmo#la|confirmare]]'', est en relation sémantique avec ''[[sera#la|sera]]'', ''[[vectis#la|vectis]] portarum'' (« les barres, les verrous des portes »). En latin médiéval, on note : ''[[archa]] [[firmata]]'' (« coffre fermé »). '''''Fermer''''' et ''clore'' ont coexisté jusqu’à ce que ce dernier entre, par certaines de ses formes, en collision homonymique avec ''[[clouer]]'' et se réduise progressivement à certains emplois techniques.
                                                                                   |: Ce, d’autant que le français connait un phénomène généralisé ({{cf|solutionner}}) et ancien ({{cf|feinter}}) de remplacement des verbes des second et troisième groupes par des équivalents ou des dérivés du premier groupe.
                                                                                   |: Pour certains étymologistes, c’est le sens de « ceindre une villa de murailles » ({{cf}} ''[[ferme#Nom commun|une ferme]]'') qui explique le glissement de sens.
                                                                                   |: D’autres enfin attribuent au rattachement à ''[[fer]]'' par étymologie populaire le glissement de sens de « rendre ferme » à « fermer » et donc l’éviction de ''clore'' par ''fermer'', mais elle est peu vraisemblable{{réf}} et le sentiment linguistique auquel elle fait appel serait aujourd’hui perdu.
                                                                                   |
                                                                                   |=== {{S|verbe|fr}} ===
                                                                                   |'''fermer''' {{pron|fɛʁ.me|fr}} {{t|fr}} ou {{i|fr}} ou {{prnl|fr}} {{conj|grp=1|fr}} {{lien pronominal}}
                                                                                   |# [[clore|Clore]] ce qui était ouvert, par une [[porte]], un [[couvercle]], une [[trappe]], etc.
                                                                                   |#* ''Elle vit plusieurs fois s’ouvrir et '''se fermer''' la porte, et à chaque fois, par l’entrebâillement, elle aperçut Catherine rajeunie par l’action, […].'' {{source|{{Citation/Alexandre Dumas/La Reine Margot/1886|I|X}}}}
                                                                                   |#* ''L’entrée '''était''' d’abord '''fermée''' par une chaîne dont les attaches sont encore à leur place […].'' {{source|{{w|Eugène Viollet-le-Duc}}, ''[[s:La Cité de Carcassonne/éd. 1888/Description des défenses|La Cité de Carcassonne]], 1888''}}
                                                                                   |#* '''''Fermer''' une chambre.''
                                                                                   |#* '''''Fermer''' une armoire, un secrétaire, une malle.''
                                                                                   |#* '''''Fermer''' une boîte.''
                                                                                   |#* '''''Fermer''' une cour.''
                                                                                   |# Clore un édifice, un établissement, un lieu de réunion, etc., en vue d'[[interrompre]] ce qui s'y faisait, [[momentanément]] ou [[définitivement]].
                                                                                   |#* '''''Fermer''' un théâtre.''
                                                                                   |#* '''''Fermer''' un bureau.''
                                                                                   |#* {{absolument}} ''Les maisons de commerce '''ferment''' les dimanches et les jours de fête.''
                                                                                   |# [[interdire|Interdire]], mettre en interdiction.
                                                                                   |#* ''Faire '''fermer''' une école pour cause d'épidémie.''
                                                                                   |#* ''Cette maison de jeu, cette salle de bal a été '''fermée''' par ordre supérieur.''
                                                                                   |# [[cesser|Cesser]] un commerce, une occupation.
                                                                                   |#* '''''Fermer''' boutique, cesser son commerce.''
                                                                                   |# [[boucher|Boucher]] une [[ouverture]], une [[entrée]], un [[passage]] et se dit en parlant des objets qui servent à la clôture.
                                                                                   |#* ''[…] il remarqua que la porte de dehors n’'''était fermée''' qu’au loquet alors que, la veille au soir, il était sûr d’avoir poussé le verrou.'' {{source|{{w|Louis Pergaud}}, ''[[s:La Vengeance du père Jourgeot|La Vengeance du père Jourgeot]]'', dans ''{{w|Les Rustiques, nouvelles villageoises}}'', 1921}}
                                                                                   |#* ''Elle disposa les fleurs au chevet, fit arrêter le balancier de l’horloge, voiler les glaces et les miroirs, '''fermer''' les fenêtres et cacher les portraits.'' {{source|{{Citation/Jean Rogissart/Hurtebise aux griottes/1954|21}}}}
                                                                                   |#* '''''Fermer''' la porte.''
                                                                                   |#* '''''Fermer''' une trappe, un judas.''
                                                                                   |#* '''''Fermer''' une écluse.''
                                                                                   |#* '''''Fermer''' la porte à clef, au verrou.''
                                                                                   |#* ''La porte n’était '''fermée''' qu’au loquet.''
                                                                                   |#* '''''Fermer''' la porte en dedans, en dehors.''
                                                                                   |#* '''''Fermer''' un robinet.''
                                                                                   |#* {{absolument}} ''On '''ferme''' !'' on va '''fermer''' les portes.
                                                                                   |#* '''''Fermer''' un tiroir,'' le faire rentrer dans le meuble où il est emboîté.
                                                                                   |#* '''''Fermer''' les rideaux,'' tirer les rideaux pour se garantir du froid, de la chaleur, du grand jour, etc.
                                                                                   |# [[interrompre|Interrompre]] un passage, le rendre impossible ou très difficile.
                                                                                   |#* '''''Fermer''' un chemin, une allée, une issue.''
                                                                                   |#* ''Faire '''fermer''' des fenêtres avec des grilles.''
                                                                                   |#* ''Des portes de bronze '''fermaient''' l’entrée du temple.''
                                                                                   |#* ''L’avenue est '''fermée''' à chaque extrémité par des barrières.''
                                                                                   |#* ''Des bancs de sable '''ferment''' l’entrée du port.''
                                                                                   |#* ''Des broussailles '''fermaient''' l’entrée de la grotte.''
                                                                                   |# {{par ext|fr}} [[empêcher|Empêcher]], par une résistance, par une défense quelconque, l’[[accès]], l’[[entrée]] ou la [[sortie]].
                                                                                   |#* ''Ce corps d'armée ferme le passage à l’ennemi.''
                                                                                   |#* '''''Fermer''' les ports, les mers, les chemins.''
                                                                                   |#* {{figuré|fr}} '''''Fermer''' à quelqu’un le chemin des honneurs.''
                                                                                   |#* ''Cette carrière lui est à jamais fermée.''
                                                                                   |# [[enclore|Enclore]].
                                                                                   |#* '''''Fermer''' une ville, un parc, un jardin,'' enclore de murailles, de haies, de fossés.''
                                                                                   |#* ''La grande muraille qui ferme la Chine au nord.''
                                                                                   |# [[rapprocher|Rapprocher]] l’une contre l'autre des parties dont l’[[écartement]] formait une [[ouverture]].
                                                                                   |#* '''''Fermer''' un sac, une bourse.''
                                                                                   |#* '''''Fermer''' la bouche.''
                                                                                   |#* '''''Fermer''' la main.''
                                                                                   |#* '''''Fermer''' un livre.''
                                                                                   |#* ''Cette plaie se '''fermera''' bientôt.''
                                                                                   |#* ''Les fleurs de cette plante se '''ferment''' dès que le soleil paraît.''
                                                                                   |#* '''''Fermer''' une lettre, un paquet, plier et cacheter une lettre, un paquet.''
                                                                                   |# Clore, [[arrêter]], [[terminer]].
                                                                                   |#* '''''Fermer''' un compte.''
                                                                                   |#* '''''Fermer''' une discussion.''
                                                                                   |#* '''''Fermer''' les débats.''
                                                                                   |#* '''''Fermer''' une liste, une souscription.''
                                                                                   |#* ''Son nom '''ferme''' la liste.''
                                                                                   |#* {{Vosges|fr}} '''''Fermer''' la lumière'' : arrêter l’éclairage.
                                                                                   |# {{intransitif|fr}} Être clos, se clore.
                                                                                   |#* ''Ce coffre '''ferme''' à clef.''
                                                                                   |#* ''Ces fenêtres ne '''ferment''' pas bien.''
                                                                                   |#* ''Cette porte '''ferme''' mal.''
                                                                                   |#* ''La Bourse a '''fermé''' à tel cours.''
                                                                                   |#* ''Cette valeur a '''fermé''' à tel cours,'' le cours des valeurs ou de cette valeur était tel à la fin de la Bourse.
                                                                                   |# {{Vosges|fr}} Éteindre.
                                                                                   |#* ''Tu pourrais '''fermer''' la lumière en sortant !''
                                                                                   |
                                                                                   |==== {{S|synonymes}} ====
                                                                                   |* [[clore]] {{vieilli|nocat=1}}
                                                                                   |
                                                                                   |==== {{S|antonymes}} ====
                                                                                   |* [[ouvrir]]
                                                                                   |
                                                                                   |==== {{S|dérivés}} ====
                                                                                   |{{(}}
                                                                                   |* [[autofermer]]
                                                                                   |* [[défermer]]
                                                                                   |* [[enfermer]]
                                                                                   |* [[fermage]]
                                                                                   |* [[fermail]]
                                                                                   |* [[fermant]]
                                                                                   |* [[ferme]] ''(nom féminin, l'adjectif est issu du latin)''
                                                                                   |* [[fermer boutique]]
                                                                                   |* [[fermer la marche]]
                                                                                   |* [[fermer la parenthèse]] ''(terminer une digression et revenir au sujet''
                                                                                   |* [[fermer la porte]]
                                                                                   |* [[fermer la porte au nez]] ''(pousser rudement la porte contre quelqu'un au moment où il se présente pour entrer)''
                                                                                   |* [[fermer les paupières]]
                                                                                   |* [[fermer les portes]]
                                                                                   |* [[fermer les yeux]]
                                                                                   |* [[fermer sa bouche]]
                                                                                   |* [[fermer sa gueule]]
                                                                                   |* [[fermer sa porte]] (à quelqu’un)
                                                                                   |* [[fermer son cœur]] : ne plus ressentir de sentiments
                                                                                   |* [[fermer toutes les portes]] à quelqu'un : empêcher toute opportunité
                                                                                   |* [[fermier]], [[fermière]]
                                                                                   |* [[fermoir]]
                                                                                   |* [[fermeture]]
                                                                                   |* [[la fermer]]
                                                                                   |* [[les yeux fermés]]
                                                                                   |* [[ne pas fermer l’œil]]
                                                                                   |* [[refermer]]
                                                                                   |{{)}}
                                                                                   |
                                                                                   |==== {{S|phrases}} ====
                                                                                   |* [[il faut qu’une porte soit ouverte ou fermée]]
                                                                                   |
                                                                                   |==== {{S|vocabulaire}} ====
                                                                                   |* {{voir thésaurus|fr|boutique}}
                                                                                   |
                                                                                   |==== {{S|traductions}} ====
                                                                                   |{{trad-début|Clore.}}
                                                                                   |* {{T|af}} : {{trad-|af|afsluit}}, {{trad-|af|toemaak}}
                                                                                   |* {{T|akz}} : {{trad--|akz|okbahli}}
                                                                                   |* {{T|de}} : {{trad+|de|schließen}}, {{trad+|de|verschließen}}, {{trad+|de|zuschließen}}, {{trad+|de|schließen}}, {{trad+|de|verschließen}}, {{trad+|de|zumachen}}
                                                                                   |* {{T|angevin}} : {{trad--|angevin|fremer}}
                                                                                   |* {{T|en}} : to {{trad+|en|close}}, to {{trad+|en|lock}}, to {{trad+|en|shut}}, to {{trad+|en|adjourn}}
                                                                                   |* {{T|ang}} : {{trad-|ang|fordyttan}}, {{trad-|ang|lucan}}
                                                                                   |* {{T|ar}} : {{trad-|ar|غلق}}
                                                                                   |* {{T|ba}} : {{trad--|ba|ябыу}}
                                                                                   |* {{T|bm}} : {{trad--|bm|tugu}}
                                                                                   |* {{T|eu}} : {{trad-|eu|itxi}}
                                                                                   |* {{T|br}} : {{trad+|br|serriñ}}
                                                                                   |* {{T|ca}} : {{trad-|ca|tancar amb clau}}, {{trad+|ca|tancar}}
                                                                                   |* {{T|shy}} : {{trad--|shy|yebleε}}, {{trad--|shy|isekkeṛ}}, {{trad--|shy|yeqqen}}
                                                                                   |* {{T|zh}} : {{trad+|zh|关|R=guān|tradi=關}}
                                                                                   |* {{T|da}} : {{trad-|da|låse}}, {{trad+|da|lukke}}
                                                                                   |* {{T|es}} : {{trad+|es|cerrar}}, {{trad-|es|cerrar con llave}}
                                                                                   |* {{T|eo}} : {{trad+|eo|fermi}}
                                                                                   |* {{T|fo}} : {{trad-|fo|læsa}}, {{trad-|fo|lata aftur}}
                                                                                   |* {{T|fi}} : {{trad+|fi|lukita}}, {{trad+|fi|sulkea}}
                                                                                   |* {{T|fy}} : {{trad-|fy|skoattelje}}, {{trad-|fy|ôfskoattelje}}, {{trad-|fy|tichtdwaan}}
                                                                                   |* {{T|gd}} : {{trad-|gd|glas}}
                                                                                   |* {{T|ga}} : {{trad-|ga|dún}}
                                                                                   |* {{T|gag}} : {{trad--|gag|kapamaa}}
                                                                                   |* {{T|got}} : {{trad--|got|𐌻𐌿𐌺𐌰𐌽|R=lukan}}
                                                                                   |* {{T|el}} : {{trad+|el|κλείνω|tr=cleínw}}
                                                                                   |* {{T|hbo}} : {{trad--|hbo|סגר}} {{m}}, {{trad--|hbo|סכר}} {{m}}
                                                                                   |* {{T|sah}} : {{trad--|sah|сап}}
                                                                                   |* {{T|io}} : {{trad+|io|klozar}}
                                                                                   |* {{T|ia}} : {{trad-|ia|clauder}}
                                                                                   |* {{T|it}} : {{trad+|it|chiudere}}, {{trad-|it|serrare a chiave}}
                                                                                   |* {{T|krc}} : {{trad--|krc|джабаргъа}}
                                                                                   |* {{T|kk}} : {{trad-|kk|жабу|tr=jabuw}}
                                                                                   |* {{T|ky}} : {{trad+|ky|жабуу}}
                                                                                   |* {{T|avk}} : {{trad--|avk|budé}}
                                                                                   |* {{T|kum}} : {{trad--|kum|япмакъ}}
                                                                                   |* {{T|ku}} : {{trad+|ku|girtin}}
                                                                                   |* {{T|arn}} : {{trad--|arn|nürüfün}}
                                                                                   |* {{T|yua}} : {{trad--|yua|k’alik}}
                                                                                   |* {{T|nl}} : {{trad-|nl|op slot doen}}, {{trad+|nl|sluiten}}, {{trad+|nl|afsluiten}}, {{trad+|nl|dichtdoen}}, {{trad+|nl|dichtmaken}}, {{trad+|nl|sluiten}}, {{trad+|nl|toedoen}}
                                                                                   |* {{T|fra-nor}} : {{trad--|fra-nor|froumaer}}
                                                                                   |* {{T|oc}} : {{trad+|oc|tancar}}, {{trad+|oc|barrar}}, {{trad+|oc|tapar}}, {{trad-|oc|claure}}
                                                                                   |* {{T|pap}} : {{trad--|pap|sera}}
                                                                                   |* {{T|myp}} : {{trad--|myp|gáabí}}
                                                                                   |* {{T|pl}} : {{trad+|pl|zamykać}}
                                                                                   |* {{T|pt}} : {{trad+|pt|fechar}}, {{trad+|pt|fechar}}
                                                                                   |* {{T|ro}} : {{trad+|ro|încuia}}, {{trad+|ro|închide}}
                                                                                   |* {{T|ru}} : {{trad+|ru|закрыть}} (zakryt'), {{trad+|ru|закрывать}} (zakryvat')
                                                                                   |* {{T|se}} : {{trad--|se|gokčat}}, {{trad--|se|giddet}}
                                                                                   |* {{T|sv}} : {{trad+|sv|låsa}}, {{trad+|sv|stänga}}
                                                                                   |* {{T|tl}} : {{trad-|tl|isará}}
                                                                                   |* {{T|tt}} : {{trad-|tt|ябу}}
                                                                                   |* {{T|cv}} : {{trad--|cv|хуп}}
                                                                                   |* {{T|tr}} : {{trad+|tr|kapamak}}
                                                                                   |* {{T|tk}} : {{trad-|tk|ýapmak}}
                                                                                   |* {{T|non}} : {{trad--|non|byrgja}}
                                                                                   |* {{T|zu}} : {{trad-|zu|-khiya}}, {{trad-|zu|-vala}}
                                                                                   |{{trad-fin}}
                                                                                   |{{trad-début|Boucher une ouverture, une entrée, un passage et se dit en parlant des objets qui servent à la clôture}}
                                                                                   |* {{T|ba}} : {{trad--|ba|бикләү}}
                                                                                   |* {{T|ga}} : {{trad-|ga|druid}}, {{trad-|ga|dún}}
                                                                                   |* {{T|gallo}} : {{trad--|gallo|crouiller}}
                                                                                   |* {{T|avk}} : {{trad--|avk|budé}}
                                                                                   |* {{T|swb}} : {{trad--|swb|uɓala|tr=ubala}}
                                                                                   |* {{T|se}} : {{trad--|se|gokčat}}
                                                                                   |* {{T|zdj}} : {{trad--|zdj|-ɓaya|dif=uɓaya}}
                                                                                   |{{trad-fin}}
                                                                                   |{{trad-début|Clore en rapprochant l'une contre l'autre des parties dont l'écartement formait une ouverture|9}}
                                                                                   |* {{T|de}} : {{trad+|de|schließen}}, {{trad+|de|zumachen}}
                                                                                   |* {{T|avk}} : {{trad--|avk|budé}}
                                                                                   |* {{T|zdj}} : {{trad--|zdj|-funga|dif=ufunga}}, {{trad--|zdj|-funya|dif=ufunya}}
                                                                                   |{{trad-fin}}
                                                                                   |
                                                                                   |=== {{S|prononciation}} ===
                                                                                   |* {{écouter|lang=fr|France <!-- précisez svp la ville ou la région -->|fɛʁ.me|audio=Fr-fermer.ogg}}
                                                                                   |* {{écouter|lang=fr|France (Vosges)|audio=LL-Q150 (fra)-Penegal-fermer.wav}}
                                                                                   |* {{écouter|lang=fr|France (Lyon)||audio=LL-Q150 (fra)-WikiLucas00-fermer.wav}}
                                                                                   |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-Poslovitch-fermer.wav}}
                                                                                   |* {{écouter|lang=fr|Suisse (canton du Valais)||audio=LL-Q150 (fra)-DSwissK-fermer.wav}}
                                                                                   |* {{écouter|lang=fr|France (Toulouse)||audio=LL-Q150 (fra)-Lepticed7-fermer.wav}}
                                                                                   |* {{écouter|lang=fr|France (Vosges)||audio=LL-Q150 (fra)-LoquaxFR-fermer.wav}}
                                                                                   |
                                                                                   |=== {{S|références}} ===
                                                                                   |* {{Import:DAF8}}
                                                                                   |* {{RÉF}} {{R:TLFi}}
                                                                                   |
                                                                                   |[[Catégorie:Verbes ergatifs en français]]
                                                                                   |
                                                                                   |== {{langue|fro}} ==
                                                                                   |=== {{S|étymologie}} ===
                                                                                   |: Du {{étyl|la|fro|fermo|dif=fermare}}.
                                                                                   |
                                                                                   |=== {{S|verbe|fro}} ===
                                                                                   |'''fermer''' {{pron||fro}}
                                                                                   |# [[fermer#fr|Fermer]].
                                                                                   |# [[attacher|Attacher]], [[fixer]].
                                                                                   |#* ''Esperons d’or a en ses piés '''fermés''''' {{source|1=''Hervis de Metz'', édition de {{pc|E. Stengel}}, [http://gallica.bnf.fr/ark:/12148/bpt6k6534759h/f406.image p. 380], 1200-25. ''Esperons'', éperons.}}
                                                                                   |#*:
                                                                                   |# [[bâtir|Bâtir]].
                                                                                   |
                                                                                   |=== {{S|références}} ===
                                                                                   |* {{R:Godefroy}}
                                                                                   |* {{R:Godefroy|complément=1}} ''(au complément)''""".stripMargin)
}
