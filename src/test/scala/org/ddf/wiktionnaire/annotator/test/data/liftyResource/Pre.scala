/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource


object Pre {
  val resTxt = """{{voir/pre}}
                 |
                 |== {{langue|fr}} ==
                 |=== {{S|étymologie}} ===
                 |: De l’adverbe et préfixe {{étyl|la|fr|mot=prae|sens=[[devant]], [[avant]]}}.
                 |
                 |=== {{S|préfixe|fr}} ===
                 |'''pré-''' {{pron|pʁe|fr}}
                 |# Marque l’[[antériorité]] dans le temps.
                 |#* '''''pré'''dire'' — ''Annoncer par avance.''
                 |#* '''''pré'''fabriqué'' — ''Fabriqué à l’avance et qu’il ne reste plus qu’à assembler.''
                 |#* '''''pré'''histoire'' — ''Période qui a précédé l’histoire contemporaine (écrite).''
                 |#* '''''pré'''venir'' - ''Aviser, mettre en garde.Satisfaire ou empêcher par prévention.''
                 |#* '''''pré'''lavage'' — ''Étape de lavage qui précède le nettoyage approfondi.''
                 |#* '''''pré'''retraite.'' — ''Statut d’inactivité qui précède l’âge de la retraite officielle.''
                 |#* ''Au-delà du contrôle du virus, il faudra vraisemblablement des années avant que l'économie américaine ne retrouve son rythme '''pré-'''pandémique.'' {{source|Richard Latendresse, « Malchanceux, ces démocrates! », dans ''Le journal de Montréal'', 1er novembre 2020}}
                 |# Qui [[précéder|précède]] dans l’espace.
                 |#* '''''Pré'''alpes'' — ''Massifs montagneux qui bordent les Alpes.''
                 |#* '''''pré'''cité'' — ''Cité plus haut dans le texte.''
                 |#* '''''pré'''fixe'' — ''Ensemble de lettres placé en tête de mot pour en modifier le sens.''
                 |#* '''''pré'''nom'' — ''Nom qui est situé avant le nom de famille.''
                 |#* '''''pré'''position'' — ''Mot placé en tête d’un groupe de mots.''
                 |
                 |==== {{S|synonymes}} ====
                 |* [[anté-]]
                 |* [[avant-]]
                 |* [[anti-]]
                 |* [[pro-]]
                 |
                 |==== {{S|antonymes}} ====
                 |* [[après-]]
                 |* [[arrière-]]
                 |* [[post-]]
                 |
                 |==== {{S|traductions}} ====
                 |{{trad-début}}
                 |* {{T|de}} : {{trad+|de|prä-}}, {{trad+|de|vor-}}, {{trad-|de|früh-}}
                 |* {{T|en}} : {{trad+|en|fore-}}, {{trad+|en|pre-}}
                 |* {{T|br}} : {{trad+|br|rak-}}, {{trad+|br|rag-}}, {{trad-|br|kent-}}, {{trad+|br|rag-}}, {{trad-|br|kent-}}
                 |* {{T|gallo}} : {{trad--|gallo|avant-}}
                 |* {{T|el}} : {{trad+|el|προ-}}
                 |* {{T|ia}} : {{trad-|ia|pre-}}
                 |* {{T|it}} : {{trad+|it|pre-}}
                 |* {{T|la}} : {{trad-|la|prae-}}
                 |* {{T|oc}} : {{trad-|oc|pre-}}
                 |{{trad-fin}}
                 |
                 |=== {{S|prononciation}} ===
                 |* {{écouter|lang=fr|Canada (Shawinigan)||audio=LL-Q150 (fra)-DenisdeShawi-pré-.wav}}
                 |
                 |{{clé de tri|pre}}
                 |
                 |[[Catégorie:Mots autologiques en français]]
                 |""".stripMargin

  val resource = Resource("pré-","https://fr.wiktionary.org/wiki/","entry",resTxt)

}
