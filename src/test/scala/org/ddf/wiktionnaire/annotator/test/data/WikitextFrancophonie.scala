package org.ddf.wiktionnaire.annotator.test.data

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WikitextFrancophonie {
  val francophonieMAJ = """{{voir|francophonie}}
                          |
                          |== {{langue|fr}} ==
                          |=== {{S|étymologie}} ===
                          |:{{ébauche-étym|fr}}
                          |
                          |=== {{S|nom|fr}} ===
                          |{{fr-inv|fʁɑ̃.kɔ.fɔ.ni}}
                          |'''Francophonie''' {{pron|fʁɑ̃.kɔ.fɔ.ni|fr}} {{f}}, {{au singulier uniquement|fr}}
                          |# [[organisation|Organisation]] internationale qui rassemble les [[pays]] parlant ou promouvant la langue française.
                          |#* ''Macron s’engage par rapport à la '''Francophonie''' : il veut faire du français la troisième langue du monde. Il crée un centre de la francophonie dans un ancien château de l’Aisne.'' {{source|Dominique {{pc|Lebel}}, ''L’entre-deux-mondes'', Montréal, Boréal, 2019, p. 326}}
                          |# Ensemble des [[peuple]]s, pays et territoires parlant français.
                          |
                          |==== {{S|vocabulaire}} ====
                          |* [[francophonie]]
                          |
                          |===== {{S|notes}} =====
                          |Le terme avec une minuscule initiale désigne l’ensemble des locuteurs tandis qu’avec une majuscule c’est le plus souvent une organisation politique, mais une certaine confusion règne entre ces deux usages.
                          |
                          |==== {{S|traductions}} ====
                          |{{trad-début}}
                          |* {{T|en}} : {{trad+|en|Francophonie}}
                          |{{trad-fin}}
                          |
                          |== {{langue|en}} ==
                          |=== {{S|étymologie}} ===
                          |: Emprunt au {{étyl|fr|en}}.
                          |
                          |=== {{S|nom propre|en}} ===
                          |'''Francophonie''' {{pron||en}}
                          |# La [[francophonie]].""".stripMargin
}
