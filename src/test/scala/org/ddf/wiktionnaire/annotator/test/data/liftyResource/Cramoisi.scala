/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Cramoisi {
  val resource = Resource("cramoisi", "https://fr.wiktionary.org/wiki/", "entry",
  """== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: {{siècle|XIII}}{{R|TLFi}} De l’{{étyl|es|fr|mot=carmesí}}{{R|Littré}} ou de l’{{étyl|it|fr|mot=chermisi}}{{R|Littré}}{{R|TLFi}}, ''{{lien|cremisi|it}}'', eux-mêmes de l’{{étyl|ar|fr}} {{ar-mot|qirmiziy²ũ}} : carmin, cramoisi, écarlate ; voir ''{{lien|kermès|fr}}''.
    |
    |=== {{S|adjectif|fr}} ===
    |{{fr-accord-rég|kʁa.mwa.zi}}
    |'''cramoisi''' {{pron|kʁa.mwa.zi|fr}}
    |# D’une [[couleur]] [[rouge]] [[foncé]], [[tirer|tirant]] sur le [[violet]].{{couleur|#DC143C}}
    |#* ''Cette robe était '''cramoisie''' et faite de la plus belle laine ; […].'' {{source|{{Citation/Walter Scott/Ivanhoé/1820}}}}
    |#* {{exemple|lang=fr|La maison, dans laquelle on ne pouvait pas dire que cette lumière brillait, car elle était tamisée par un double rideau '''cramoisi''' dont elle traversait mystérieusement l’épaisseur, était une grande maison qui n’avait qu’un étage, — mais placé très haut…|source={{w|Jules Barbey d'Aurevilly}}, ''Le Rideau '''cramoisi''''', 1874, réédition Gallimard, collection Folio Classique, page 34}}
    |#* ''Le soleil qui quelques minutes avant était rouge '''cramoisi''', était devenu d’un jaune vif, et les rayons d’or qu’il dardait étaient si flamboyants que l’œil en était ébloui ; […].'' {{source|Jules Leclercq, ''La Terre de glace, Féroë, Islande, les geysers, le mont Hékla'', Paris : E. Plon & Cie, 1883, page 68}}
    |#* ''Le drapeau cueilli à Saint Blaize (et dont les Parisiens admirent, à la fenêtre du ministère, la soie '''cramoisie''') présage toute une moisson d’étendards.'' {{source|{{w|Victor Margueritte}}}, ''Au bord du Gouffre'', 1919}}
    |#* ''Et pourtant les figuiers s’obstinent à pousser spontanément parmi les gravats et je me gorge, sans rien payer, de leurs fruits '''cramoisis'''.'' {{source|{{Citation/Ludovic Naudeau/La France se regarde/1931}}}}
    |#* ''Je connais une planète où il y a un monsieur '''cramoisi'''.'' {{source|{{w|Antoine de Saint-Exupéry}}, {{w|Le Petit Prince|''Le Petit Prince''}}, Éditions {{w|Gallimard}}, France, 2013, page 33}}
    |#* ''Il se penche par-dessus la rampe, le visage '''cramoisi''' d’indignation, et hurle vers tout le monde : […]. ''{{source|{{w|Patrick Senécal|Patrick Senécal}}, {{w|Aliss (roman)|''Aliss''}}, Éditions {{w|Alire}}, Québec, 2000, page 374}}
    |
    |==== {{S|traductions}} ====
    |{{trad-début}}
    |* {{T|en}} : {{trad+|en|crimson}}
    |* {{T|es}} : {{trad+|es|carmesí}}
    |* {{T|it}} : {{trad-|it|chermisi}}
    |* {{T|pt}} : {{trad+|pt|escarlate}}, {{trad+|pt|carmim}}
    |* {{T|ru}} : {{trad+|ru|пунцовый}}
    |{{trad-fin}}
    |
    |=== {{S|nom|fr}} ===
    |{{fr-rég|kʁa.mwa.zi}}
    |'''cramoisi''' {{pron|kʁa.mwa.zi|fr}} {{m}}
    |# [[quelque chose|Quelque chose]] ou [[quelqu’un]] qui est très [[rouge]].
    |# {{lexique|technique|fr}} [[procédé|Procédé]] de [[teinture]] qui rend les couleurs plus [[éclatante]]s, mais moins [[durable]] dans le temps.
    |#* ''Une étoffe teinte en '''cramoisi'''.''
    |
    |==== {{S|synonymes}} ====
    |* [[rouge]]
    |* [[carmin]]
    |
    |==== {{S|traductions}} ====
    |{{trad-début}}
    |* {{T|de}} : {{trad-|de|karmesinrot}}
    |* {{T|en}} : {{trad+|en|crimson}}
    |* {{T|ar}} : {{trad-|ar|قرمزي|tr=qirmizi}}
    |* {{T|ca}} : {{trad+|ca|carmesí|m}}
    |* {{T|es}} : {{trad+|es|carmesí}}, {{trad+|es|carmín}}
    |* {{T|io}} : {{trad+|io|karmezina}}
    |* {{T|it}} : {{trad+|it|cremisi}}
    |* {{T|nl}} : {{trad+|nl|karmijnrood}}
    |* {{T|pes}} : {{trad|pes|قرمز|tr=ghérméz}}
    |* {{T|pl}} : {{trad+|pl|karmazyn}}
    |* {{T|pt}} : {{trad+|pt|carmesim}}
    |* {{T|ro}} : {{trad+|ro|stacojiu}}, {{trad+|ro|carmin}}, {{trad+|ro|cârmâziu}} (pop.)
    |* {{T|sv}} : {{trad+|sv|karmosin}}
    |{{trad-fin}}
    |
    |=== {{S|verbe|fr|flexion}} ===
    |{{fr-verbe-flexion|cramoisir|pp=oui}}
    |'''cramoisi''' {{pron|kʁa.mwa.zi|fr}}
    |# ''Participe passé masculin singulier du verbe'' [[cramoisir]].
    |
    |=== {{S|prononciation}} ===
    |* {{pron-rimes|kʁa.mwa.zi|fr}}
    |* {{écouter|lang=fr|France (Brétigny-sur-Orge)|audio=LL-Q150 (fra)-Pamputt-cramoisi.wav}}
    |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Lyokoï-cramoisi.wav}}
    |* {{écouter|lang=fr|France (Lyon)|audio=LL-Q150 (fra)-Ltrlg-cramoisi.wav}}
    |
    |=== {{S|voir aussi}} ===
    |{{Autres projets|w=Cramoisi (homonymie)}}
    |* {{Thésaurus|fr|couleur}}
    |
    |[[Catégorie:Couleurs en français]]""".stripMargin)
}
