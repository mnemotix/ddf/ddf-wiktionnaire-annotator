/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.test.data.liftyResource

import org.ddf.wiktionnaire.annotator.model.Resource

object Scenario {
  val resource = Resource("scenarion", "https://fr.wiktionary.org/wiki/", "entry",
  """== {{langue|fr}} ==
    |=== {{S|étymologie}} ===
    |: {{date|lang=fr|1764}} De l’{{étyl|it|fr|mot=scenario|sens=[[décor]] de théâtre », puis « [[description]] de la [[mise en scène]]}}, dérivé de ''[[scena]]'' (« [[scène]] »). En français, le mot s’est d’abord utilisé sans accent comme en italien, mais cet usage est archaïque. Le mot désigne d’abord le [[canevas]], le [[schéma]] d’une [[pièce]] ; il est employé au <small>XIX</small>{{e}} [[siècle]] au sens de « mise en scène » et reste jusqu’au <small>XX</small>{{e}} siècle un [[terme]] [[technique]] de [[théâtre]]. L’emploi figuré pour « [[déroulement]] selon un [[plan]] préétabli » (av. 1850) ne s’est répandu qu’au <small>XX</small>{{e}} siècle, sous l’[[influence]] du sens devenu courant (1907, Méliès) qui désigne, au [[cinéma]], la description rédigée de l’[[action]] d’un [[film]].
    |
    |=== {{S|nom|fr}} ===
    |{{fr-rég|se.na.ʁjo}}
    |'''scénario''' {{pron|se.na.ʁjo|fr}} {{m}}
    |# {{lexique|cinéma|théâtre|fr}} [[description|Description]] [[détaillé]]e des [[différent]]es [[scène]]s d’un [[film]], d’une [[bande dessinée]] ; [[canevas]] d’une [[pièce de théâtre]].
    |#* ''Les frères Dardenne construisent d’abord la structure de leur '''scénario''' à deux, puis travaillent séparément, Luc Dardenne écrivant une première continuité dialoguée, qu’il soumet à Jean-Pierre… l’élaboration scénaristique s’organisant autour d’un “pas de deux”.'' {{source|{{nom w pc|Frédéric|Sojcher}}, ''La kermesse héroïque du cinéma belge: Le carrousel européen 1988-1996'', p. 93, L’Harmattan, 1999}}
    |#* ''Dans ''la Terrasse'', d’Ettore Scola, Trintignant joue le concocteur professionnel de '''scénarios''' hilarants et se passe — moins marrant — le doigt en entier dans le taille-crayon électrique que lui a offert son producteur-manager-pompeur de rigolo-esclavagiste.'' {{source|{{w|Sylvie Caster}}, « ''Rire, ça ne rend pas gai'' », le 10 décembre 1980, chronique en recueil dans ''La France fout le camp !'' (Recueil de textes extraits de "{{w|Charlie Hebdo}}", 1977-1981), préface de {{w|François Cavanna}}, Paris : Éditions BFB, 1982}}
    |# {{par extension}} [[déroulement|Déroulement]] [[préétabli]] ou [[concerté]] d’une [[action]].
    |#* ''Son manager ne venait que les samedis soir, depuis qu’il avait réglé, une fois pour toutes, les '''scénarios''' de l’arrivée triomphale et de la chasse aux autographes.'' {{source|Jo {{pc|Barnais}} [Georges Auguste Charles {{pc|Guibourg}}, dit {{nom w pc||Georgius}}], ''{{w|Mort aux ténors (roman)|Mort aux ténors}}'', ch. XI, {{w|Série noire}}, {{w|Gallimard}}, 1956, p. 96}}
    |#* ''Cette grossesse l’avait prise par surprise. Elle avait oublié le précieux comprimé rempart. C'était le même '''scénario''' que celui du moment : une soirée, du vin, une baise en bonne et due forme et l'oubli de la capsule jusqu'au lendemain.'' {{source|Aurélie Capobianco, ''Délivrez-nous du mal'', Éditions TDO, 2016, chap. 9}}
    |# {{par extension}} [[schéma|Schéma]] d’[[analyse]] d’[[action]] [[possible]].
    |#* ''Si l’on prend comme exemple l’approvisionnement alimentaire, on entend par autosuffisance une complète indépendance à l’égard des importations pour répondre aux besoins alimentaires d’une collectivité ou d’une ville, un objectif difficilement atteignable même en vertu du '''scénario''' le plus optimiste.'' {{source|Luc J. A. {{petites capitales|Mougeot}}, ''Cultiver de meilleures villes : agriculture urbaine et développement durable'', 2006, p. 96}}
    |#* ''Pour mieux faire saisir le principe, considérons deux '''scénarios'''. Soit un '''scénario''' où deux personnes travaillent sur l’ensemble de l’année et touchent une rémunération de 100. […]. Considérons maintenant un deuxième '''scénario''' où la rémunération horaire du travail et l’emploi global sont les mêmes.'' {{source|[[INSEE]], ''Partage de la valeur ajoutée, partage des profits et écarts de rémunérations en France'', mai 2009}}
    |#* ''Une caractérisation des risques est réalisée pour chaque nouveau '''scénario''' d’exposition conformément aux prescriptions de la section 6 de l’annexe I.'' {{source|RÈGLEMENT (CE) {{numéro}} 1907/2006 DU PARLEMENT EUROPÉEN ET DU CONSEIL du 18 décembre 2006 concernant l’enregistrement, l’évaluation et l’autorisation des substances chimiques, ainsi que les restrictions applicables à ces substances (REACH), instituant une agence européenne des produits chimiques, modifiant la directive 1999/45/CE et abrogeant le règlement (CEE) {{numéro}} 793/93 du Conseil et le règlement (CE) {{numéro}} 1488/94 de la Commission ainsi que la directive 76/769/CEE du Conseil et les directives 91/155/CEE, 93/67/CEE, 93/105/CE et 2000/21/CE de la Commission, p. 207}}
    |# {{lexique|psychologie|fr}} Processus mental récurrent aboutissant à un [[passage à l’acte]] ou à l’expression d’un état affectif particulier.
    |#* {{ébauche-exe|fr}}
    |# {{lexique|jeux|fr}} [[élément|Éléments]] utilisés pour préparer une partie de [[jeu de rôle]] : intrigue, personnages, lieux…
    |
    |==== {{S|notes}} ====
    |: ''[[scénarii#fr|Scénarii]]'', le pluriel italianisant est considéré comme [[affecté]]{{réf|1}}. Le Conseil supérieur de la langue française, via le journal officiel du 6 décembre 1990, a déconseillé l’usage dans les documents officiels de la République française.
    |
    |: Ni le pluriel italien archaïque ''scenarii'' (ancienne orthographe<ref>En italien moderne, le pluriel n’a qu’un seul ''i'' (''scenari'').</ref>), ni sa variante francisée ''scénarii'' (avec accent aigu) ne sont d’usage courant : le pluriel ''scénarios'' est le plus commun en français<ref>Voir l’entrée [http://www.cnrtl.fr/lexicographie/scenario Scénario], section « Prononciation et orthographe », du TLFi. — Le Petit Robert de 1987 marque le pluriel « scénarii » comme rare. Le Petit Larousse de 1990 indique, comme forme savante, la version sans accent ''scenario'', ''scenarii''.</ref>.
    |: Selon l’[[Académie française]], en effet, le mot ''scénario'' étant français (en italien, il n’aurait pas d’accent aigu), le pluriel « scénarios » s’impose — exactement comme pour ''lavabos'' ou ''pianos''<ref>[http://www.academie-francaise.fr/questions-de-langue#73_strong-em-rectifications-de-l-orthographe-em-strong ''Questions de langue'', Conseil supérieur de la langue française]</ref>.
    |: Quand un mot d’origine étrangère est intégré à la langue française — en particulier si le sens est différent de celui de la langue d’origine — il cesse d’être soumis aux règles grammaticales de sa langue d’origine pour être soumis aux règles grammaticales françaises (rapport du Conseil supérieur de la langue française publié dans les documents administratifs du Journal officiel du 6 décembre 1990). C’est donc cette règle qui est d’usage lorsqu’un mot de cette nature est utilisé dans une contextualisation triviale ou à caractère indifférent, tandis que le pluriel archaïque reste occasionnellement utilisé pour mettre en évidence une sophistication du contexte, en exploitant l’effet de contraste avec la norme par la manifestation de son [[Assonance|assonance harmonique]] particulière.""".stripMargin)
}
