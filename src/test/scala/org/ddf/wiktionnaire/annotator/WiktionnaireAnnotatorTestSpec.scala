package org.ddf.wiktionnaire.annotator

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.services.WiktionnaireRdfReader
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{ExecutionContext}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireAnnotatorTestSpec extends AnyFlatSpec with Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging {
  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext =  ExecutionContext.global

  lazy val wiktionnaireVocabularyLoader = new WiktionnaireRdfReader
  wiktionnaireVocabularyLoader.init

  val vocs = wiktionnaireVocabularyLoader.vocabulariesBlocked

  implicit val map = wiktionnaireVocabularyLoader.posTypeBlocked

  implicit val lexicalQualificationRecognizer = new VocsRecognizer(vocs)

  override protected def afterAll(): Unit = system.terminate()
}