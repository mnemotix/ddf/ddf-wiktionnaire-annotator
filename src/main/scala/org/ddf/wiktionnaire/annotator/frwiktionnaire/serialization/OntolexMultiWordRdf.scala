/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization.LexicalEntryRdf.extractClassForm
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.RegexExtractors
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.wiktionnaire.annotator.model.ddf.LexicalEntry
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

object OntolexMultiWordRdf {
  def serialize(model: ModelBuilder, lexicalEntry: LexicalEntry, lexicalEntryIRI: IRI)(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]) = {

    /**
     * Gender RDF
     */

    lexicalEntry.wordSectionContent.toList.foreach { wikitext =>
      val text = RegexExtractors.extractLexicalInfo(wikitext)
      text.toList.foreach { t =>
        val gender = lexicalQualificationRecog.recogFromVocab(t, "http://data.dictionnairedesfrancophones.org/authority/gender")
        gender.foreach { gdr =>
          model.subject(lexicalEntryIRI)
            .add(gdr._1, gdr._2)
        }
      }
    }

    /**
     * Number RDF
     */

    lexicalEntry.wordSectionContent.toList.foreach { wikitext =>
      val text = RegexExtractors.extractLexicalInfo(wikitext)
      text.toList.foreach { t =>
        val number = lexicalQualificationRecog.recogFromVocab(t, "http://data.dictionnairedesfrancophones.org/authority/number")
        number.foreach { nbr =>
          model.subject(lexicalEntryIRI)
            .add(nbr._1, nbr._2)
        }
      }
    }


    lexicalQualificationRecog.recogFromVocab(lexicalEntry.lexicalEntryType, "http://data.dictionnairedesfrancophones.org/authority/multiword-type").foreach { couple =>
      model.subject(lexicalEntryIRI)
        .add(couple._1, couple._2)
        .add(RDF.TYPE, extractClassForm(lexicalEntry.pageTitle))
    }
  }
}