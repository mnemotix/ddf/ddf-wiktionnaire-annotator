/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

object OntolexTermElementRdf {
  def serialize(model: ModelBuilder, lexicalEntryIRI: IRI)(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]) = {
    model.subject(lexicalEntryIRI)
      .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/ontolex#Afflix"))
  }
}