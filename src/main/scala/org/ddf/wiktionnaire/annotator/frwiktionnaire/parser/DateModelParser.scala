/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

class DateModelParser {
  // TODO :{{siècle|?}} Du {{étyl|la|fr|repraesento|dif=repraesentare}}. -> (Siècle à préciser) Du latin repraesentare.
  // {{siècle|lang=fr|?}} Du {{étyl|la|fr|mot=itinerans|sens=voyageant}} ; ''{{lien|itinérer|fr}}'' semble être un emprunt postérieur.

  def parser(char: String): String = {
    if (char.contains("{{siècle|lang=fr|?}}")) {
      val res = char.replaceAll("\\{\\{siècle\\|lang=fr\\|\\?}}", "").replaceAll("\\(\\)","").trim
      parser(res)
    }
    else if (char.contains("{{siècle|?}}")) {
      val res = char.replaceAll("\\{\\{siècle\\|\\?}}", "").replaceAll("\\(\\)","").trim
      parser(res)
    }
    else if (char.contains("{{date|")) {
      val res = date(char).replaceAll("\\(\\)","").trim
      parser(res)
    }
    else if (char.contains("{{siècle|") || char.contains("{{siècle2|")) {
      val res = siecle(char).replaceAll("\\(\\)","").trim
      parser(res)
    }
    else {
      char.replaceAll("\\(\\)","").trim
    }
  }

  def date(sentence: String): String = {
    val dateModel = "\\{\\{date\\|(.*?)\\}\\}".r.findFirstIn(sentence)
    if (dateModel.isDefined) {
      val dateModelArray = dateModel.get.split("\\|")
      val replacer = dateModelArray.map { splitted =>
        dateHelper(splitted)
      }.mkString(" ")
      date(sentence.replace(dateModel.get,s"(${replacer.trim})"))
    }
    else sentence.trim
  }

  def dateHelper(partString: String) = {
    partString.split("\\s").collect {
      case word if word.matches("lang\\=fr\\}\\}") => word.replace("lang=fr}}", "")
      case word if word.matches("lang\\=fr") => word.replace("lang=fr", "")
      case word if word.matches(".*?\\}\\}") => word.replace("}}","")
      case word if word.matches("\\{\\{date") => ""
      case word => word
    }.mkString(" ")
  }

  // {{siècle2|XII}} siècle

  def siecle(sentence: String): String = {
    val siecleModel = "\\{\\{siècle(2?)\\|(.*?)\\}\\}".r.findFirstIn(sentence)
    if (siecleModel.isDefined) {
      val siecleModelArray = siecleModel.get.split("\\|")
      val replacer = siecleModelArray.map { splitted =>
        siecleHelper(splitted)
      }.mkString(" ")
      siecle(sentence.replace(siecleModel.get,s"(${replacer.trim})"))
    }
    else sentence
  }

  def siecleHelper(partString: String): String  = {
    partString.split("\\s").collect {
      case word if word.matches("M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\}\\}") => s"${word.replace("}}","")}e siècle"
      case word if word.matches("doute\\=oui") => "?"
      case word if word.matches("doute\\=oui\\}\\}") => "?"
      case word if word.matches("M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})") => s"${word}e siècle"
      case word if word.matches("\\{\\{siècle") => ""
      case word if word.matches("\\{\\{siècle2") => ""
      case word if word.trim.matches("lang\\=fr") => ""
      case word if word.trim.matches("lang=fr}}") => ""
      case word if word.trim.matches("fr") => ""
      case word if word.matches("(.*?)\\}\\}") => word.replace("}}","")
      case word => word
    }.mkString(" ")
  }

}
