package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

import java.net.URL

import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class SourceModelParser extends LazyLogging{

  val wikitoHtml = new WikitoHtml
  val dateModelParser = new DateModelParser
  val simpleParser = new SimpleParser

  def parser(citation: String): String = {
    val citationParsed = citationModel(citation)
    val dateParsed = dateModelParser.parser(citationParsed)
    val simpleParsed = simpleParser.convertSimple(dateParsed)
    val legifranceParsed = legifrance(simpleParsed)
    val removed = legifranceParsed.replaceAll("\\{\\{source\\|", "").replaceAll("source\\=", "")
      .replaceAll("\\{\\{périodique\\|","")
      .replaceAll("\\{\\{nom\\sw\\spc\\|", "").replaceAll("\\{\\{ouvrage ", "").replaceAll("\\{\\{Lien web", "")
      .replaceAll("\\{\\{ère\\}\\}", "ère")
      .replaceAll("\\{\\{e}}", "<sup>e</sup>")
      .replaceAll("\\{\\{petites capitales\\|", "")
      .replaceAll("\\{\\{Citation/","")
      .replaceAll("\\{\\{pc\\|", "")
      .replaceAll("\\{\\{smcp\\|", "")
      .replaceAll("\\{\\{nobr\\|", "")
      .replaceAll("\\{\\{in\\|(\\d)\\}\\}", "<sub>$1</sub>")
      .replaceAll("\\{\\{numéro}}", "n°")
      .replaceAll("lien\\=", "")
      .replaceAll("\\{\\{lien web", "")
      //.replaceAll("1\\=", "")
      .replaceAll("\\{\\{ws\\|", "")
      .replaceAll("\\{\\{w\\|","").replaceAll("\\{\\{s\\|", "").
      replaceAll("\\{\\{n\\|", "").replaceAll("\\{\\{b\\|","")
      .replaceAll("\\[\\[ws\\:(.*?)\\|", "").replaceAll("\\[\\[w\\:(.*?)\\|","").replaceAll("\\[\\[s\\:(.*?)\\|", "").
      replaceAll("\\[\\[n\\:(.*?)\\|", "").replaceAll("\\[\\[b\\:(.*?)\\|","").replaceAll("}}", "")
     // replaceAll("\\]\\]","")
    //val sourceSplitted = removed.split("\\|").mkString(" ")
    val splitted = removed.split("\\|")
    val sourceSplitted = splitted.zipWithIndex.map { case valueS =>
      if (valueS._1.contains("auteur=")) s"${valueS._1.replaceAll("auteur=","").trim},"
      else if (valueS._1.contains("sous-titre=")) s": ${valueS._1.replaceAll("sous-titre=","").trim},"
      else if (valueS._1.contains("éditeur=")) s"${valueS._1.replaceAll("éditeur=","").trim}."
      else if (valueS._1.contains("journal=")) s"${valueS._1.replaceAll("journal=",", dans le ").trim}."
      else if (valueS._1.contains("volume=")) s"${valueS._1.replaceAll("volume=","vol.").trim}"
      else if (valueS._1.contains("numéro=")) s"${valueS._1.replaceAll("numéro=","n° ").trim}"
      else if (valueS._1.contains("pages=")) s"${valueS._1.replaceAll("pages=","p.").trim}."
      else if (valueS._1.contains("page=")) s"${valueS._1.replaceAll("page=","p.").trim}."


      else if (valueS._1.contains("en ligne le=")) s"${valueS._1.replaceAll("en ligne le=","Mis en ligne le ")}"
      else if (valueS._1.contains("prénom1=")) s"${valueS._1.replaceAll("prénom1=","")}"
      else if (valueS._1.contains("nom1=")) s"${valueS._1.replaceAll("nom1=","").trim},"

      else if (valueS._1.contains("prénom=")) s"${valueS._1.replaceAll("prénom=","")}"
      else if (valueS._1.contains("nom=")) s"${valueS._1.replaceAll("nom=","").trim},"
      else if (valueS._1.contains("{{ISBN")) s"${valueS._1.replaceAll("\\{\\{ISBN","ISBN").trim}"
      //else if (valueS._1.contains("{{Légifrance")) s"${valueS._1.replaceAll("\\{\\{Légifrance","Légifrance").trim}"
      else if (valueS._1.contains("base=")) s"${valueS._1.replaceAll("base=","").trim}"
      else if (valueS._1.contains("titre=")) {

        if (valueS._2 - 1 >= 0 && s"${splitted(valueS._2 - 1)}".contains("url=")) {
          s"""<a href="${splitted(valueS._2 - 1).replaceAll("url=","").trim}">${valueS._1.replaceAll("titre=","").trim}</a>"""
        }
        else if (splitted.filter(_.contains("url=")).size > 0) {
          val url = splitted.filter(_.contains("url=")).last
          s"""<a href="${url.replaceAll("url=","").trim}">${valueS._1.replaceAll("titre=","").trim}</a>"""
        }

        // <a href="https://example.com">Website</a>
        else s"${valueS._1.replaceAll("titre=","").trim}"
      }
      else if (valueS._1.contains("éditeur=")) s"${valueS._1.replaceAll("éditeur=","").trim},"
      else if (valueS._1.contains("lieu=")) s"${valueS._1.replaceAll("lieu=","").trim},"
      else if (valueS._1.contains("année=")) s"${valueS._1.replaceAll("année=","").trim}"
      else if (valueS._1.contains("date=")) s"${valueS._1.replaceAll("date=","").trim}"
      else if (valueS._1.contains("lang=")) s"${valueS._1.replaceAll("lang=.*","").trim}"
      else if (valueS._1.contains("site=")) s"${valueS._1.replaceAll("site=","sur ").trim}"
      else if (valueS._1.contains("consulté le=")) s"${valueS._1.replaceAll("consulté le=","consulté le ").trim}"
      else if (valueS._1.contains("texte=")) valueS._1.replaceAll(valueS._1, s"""<a href="${valueS._1.replaceAll("texte=", "")}">${"texte intégral"}</a>""".trim)
      /*
      s"${valueS._1.replaceAll(valueS._1,s"""<a href="${valueS._1.trim.replaceAll("\\$", "").replaceAll("texte=", "")}">${"texte intégal"}</a>""".trim)}"
      consulté le=
       */
      else if (valueS._1.contains("url=")) {
        if ((valueS._2 + 1) < splitted.size) {
          if (s"${splitted(valueS._2 + 1)}".contains("titre=")) {
            s"${valueS._1.replace(valueS._1, "").trim}"
          }
        }
        else if (splitted.filter(_.contains("titre=")).size > 0) {
          s"${valueS._1.replace(valueS._1, "").trim}"
        }
        else {
          val toReplace = s"""<a href="${valueS._1.trim.replaceAll("\\$", "").replaceAll("url=", "")}">${"texte intégal"}</a>""".trim
          s"${valueS._1.replace(valueS._1, toReplace).trim}"
        }
      }
      else s"${valueS._1}"
    }.mkString(" ").trim
    val htmled = wikitoHtml.toHtml(sourceSplitted).replaceAll("]]", "").replaceAll(" ,",",").replaceAll(",\\s{2,}", ", ").replaceAll("\\(\\)","").trim

    if ("\\[.*?\\]".r.findFirstIn(htmled).isDefined) {

      val withURL = "\\[.*?\\]".r.findFirstIn(htmled).get.replaceAll("\\[", "").replaceAll("]", "")
      val urls  = withURL.split("""\s+""").filter { case word => (Try(new URL(word)).isSuccess)}
      val noUrls = withURL.split("""\s+""").filter { case word => (Try(new URL(word)).isFailure)}.mkString(" ").replaceAll("\\$", "")
      //val noUrls = withURL.split("""\s+""").filter { case word => (Try(new URL(word)).isFailure)}.mkString(" ").replaceAll("\\$", "").replaceAll("/", "")
      val toreplace = if (urls.length > 0) {
        s"""<a href="${urls.mkString(" ").trim.replaceAll("\\$", "")}">${noUrls.trim}</a>""".trim
      }
      else {
        noUrls
      }
      val ress = Try {
        htmled.replaceFirst("\\[.*?]", toreplace).trim
      }
      ress match {
        case Success(value) => value
        case Failure(exception) => {
          logger.error(s"error in source model parser for ${citation}", exception)
          "pas de source"
        }
      }
    }
    else htmled
  }

  def removeLink(htmled: String):String = {
    if ("\\[.*?\\]".r.findFirstIn(htmled).isDefined) {
      removeLinkhelper(htmled)
      removeLink(htmled)
    }
    else htmled
  }

  def removeLinkhelper(htmled:String): String = {
    val withURL = "\\[.*?\\]".r.findFirstIn(htmled).get.replaceAll("\\[", "").replaceAll("\\]", "")
    val urls  = withURL.split("""\s+""").filter { case word => (Try(new URL(word)).isSuccess)}
    val noUrls = withURL.split("""\s+""").filter { case word => (Try(new URL(word)).isFailure)}.mkString(" ")
    val toreplace = if (urls.length > 0) {
      s"""<a href="${urls.mkString(" ").trim}">${noUrls.trim}</a>""".trim
    }
    else {
      noUrls
    }
    htmled.replaceFirst("\\[.*?\\]", toreplace).trim
  }

  def citationModel(sentence: String): String = {
    if (("\\{\\{source\\|\\{\\{Citation\\/.*\\}\\}\\}\\}").r.findFirstIn(sentence).isDefined) {
      val converted = citationParser(sentence)
      citationModel(converted)
    }
    else sentence
  }

  def citationParser(sentence: String): String = {
    val citation = ("\\{\\{source\\|\\{\\{Citation\\/.*\\}\\}\\}\\}").r.findFirstIn(sentence).get.replaceAll("\\{\\{source\\|\\{\\{Citation\\/", "").replaceAll("}","")
    val splitted = citation.split("\\/")
    val replaceWith = splitted.zipWithIndex.map { case valueS =>
      if (valueS._2 == 1) {
        s"${valueS._1.replace(valueS._1, s"<i>${valueS._1}</i>").trim}"
      }
      else if (valueS._1.contains("|")) {
        val s = valueS._1.split("\\|")
        s"${s.mkString(", ")}"
      }
      else s"${valueS._1},"
    }.mkString(" ")
    sentence.replaceFirst("\\{\\{source\\|\\{\\{Citation\\/.*\\}\\}\\}\\}", replaceWith.dropRight(1))
  }

  // "{{source|{{Légifrance|base=JORF|numéro=ACVX8900143L|texte=Art. 1{{er}} de la loi {{n°}} 89-1013}}
  def legifrance(sentence: String) = {
    if ("\\{\\{source\\|\\{\\{Légifrance\\|(.*?)}}(.*?)}}".r.findFirstIn(sentence).isDefined) {
      val removed = "\\{\\{source\\|\\{\\{Légifrance\\|(.*?)}}(.*?)}}".r.findFirstIn(sentence).get.replaceAll("\\{\\{source\\|", "").replaceAll("}}", "").replaceAll("\\{\\{", "")
      val splitted = removed.split("\\|")
      val sourceSplitted = splitted.zipWithIndex.map { case valueS =>
        if (valueS._1.contains("base=")) s"${valueS._1.replaceAll("base=", "").trim}"
        else if (valueS._1.contains("numéro=")) s"${valueS._1.replaceAll("numéro=", "n°").trim}"
        else if (valueS._1.contains("texte=")) s"${valueS._1.replaceAll("texte=", "").trim}"
        else s"${valueS._1}"
      }.mkString(" ").trim
      sourceSplitted
    }
    else sentence
  }
}