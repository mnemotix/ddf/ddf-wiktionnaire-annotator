package org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing

import com.typesafe.scalalogging.LazyLogging
import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.{AbreviationDeModelParser, Bibliographie, FormatNumParser, SimpleParser, SourceModelParser, WModelParser, WikitoHtml}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object TextCleaners extends LazyLogging{
  //"=== {{S|nom|fr|num=1}} ===" => nom
  // "=== {{S|verbe|fr}} ===" => verbe
  def cleanWordSectionTitle(title: String) = {
    "(===\\s\\{\\{S\\|)(.*)(\\|fr.*\\s===)".r.findFirstMatchIn(title).get.group(2)
  }

  def cleanWordTitle(title: String): String = {
    title.replaceAll("\\|num\\=\\d+", "")
      .replaceAll("\\|flexion", "")
  }

  def cleanCitation(citation: String): String = {
    citation.replaceAll("\\{\\{source\\|.*$", " ").trim
  }

  def cleanCitationExempleModel(citation: String) = {
    citation.replaceAll("\\|source\\=\\{\\{.*$", "").trim
  }

  def cleanEtymologiesStartOfText(etym: String) = {
    etym.toLowerCase.replaceAll("\\[\\[.*?\\|", " ").replace("'", " ").replace("(", " ").replace(")", " ").replace("[", " ")
      .replace("]", " ").replace("#", " ").replace("|", " ").replace(":", " ")
      .replace("commun", " ").replaceAll("\\s{2,}", " ").trim
    }

  def cleanSectionTitleForEtymology(sectionTitle: String) = {
    val res = "\\{\\{S\\|(.*?)\\|fr\\|?(.*?=[0-9])?".r.findFirstMatchIn(sectionTitle)
    val cleanNumber = (string: String) => if (string != null) ("[0-9]").r.findFirstIn(string).get.trim else ""
    if (res.isDefined) {
      Some(s"${res.get.group(1).trim} ${cleanNumber(res.get.group(2)).trim}".trim)
    }
    else None
  }

  def cleanDefinition(definitionTxt: String) = {
    val bibliographie = new Bibliographie
    val definitionAccol = definitionTxt.replaceAll("\\{\\{\\{", "{{").replaceAll("}}}", "}}")
    val d = bibliographie.parse(definitionAccol)
    val abbrev = new AbreviationDeModelParser
    val d2 = abbrev.parser(d)
    val format = new FormatNumParser
    val d4 = format.parser(d2)
    val wmodel = new WModelParser
    val d5 = wmodel.parser(d4)
    val simpleParser = new SimpleParser
    val d6 = simpleParser.convertSimple(d5).trim.replaceAll("\\#\\s", "")
      .replaceAll("\\#\\*", "")
      //.replaceAll("\\{\\{.*}}", "").trim
      .replaceAll("\\[\\[Fichier:.*?]]", "")
      .replaceAll("\\[\\[Image:.*?]]", "")
      .replaceAll("\\[\\[File:.*?]]", "")

    val d7 = TextCleaners.cleanStartOfString(d6).replaceAll("^[[\\.:;,!()?]*$]*$", "")
      .replaceAll("^#*", "")
    //logger.debug(d7)

    /**
     * Hack
     */

    val d8 = d7.replaceAll("\\*\\s\\{\\{siècle\\|(.*\\n)*\\|source.*\\}\\}", "")

    val d9 = d8.replaceAll("\\|source.*\\}\\}", "")

    val d10 = d9.replaceAll("\\{\\{source.*\\}\\}", "")

    val d11 = d10.replaceAll("\\{\\{refnec\\|.*\\}\\}", "")

    //    val d12 = d11.replaceAll("\\<\\!\\-\\-", "}}")
    /**
     * Hack
     */
    //  logger.info(d11)
    val replaced = TemplateReplacer.replaceTemplates(d11)
    val wikitoHtml = new WikitoHtml
    val sortie =   wikitoHtml.toHtml(replaced.replaceAll("\\!\\!undefined\\:.*?\\!\\!", ""))

    sortie
  }

  def cleanSource(sourceTxt: String): String = {
    val source = new SourceModelParser
    source.parser(sourceTxt)
  }

  def removeNonPrintableCharachter(str: String): String = {
    str.replaceAll("[\\p{C}]", "")
  }

  def cleanStartOfString(str: String): String = {
    str.replaceAll("(?m)^\\w?,\\w?", "").trim
  }

  def replaceApostrophe(str: String): String =(
    str.replaceAll("’", "'").trim
    )

  def removeHTMLTags(str: String) = {
    str.replaceAll("\\<.*?\\>", "")
  }
}