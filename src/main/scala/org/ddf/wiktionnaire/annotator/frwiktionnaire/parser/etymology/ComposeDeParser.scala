package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

import com.typesafe.scalalogging.LazyLogging
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo https://fr.wiktionary.org/wiki/Mod%C3%A8le:compos%C3%A9_de - 53,078


class ComposeDeParser extends LazyLogging {

  def parser(sentence: String): String = {
    if ("\\{\\{compos(é\\sde)?\\|(.*?)\\}\\}".r.findFirstIn(sentence).isDefined) {
      val convertComposeReplaced = convertComposeDeHelper(sentence)
      parser(convertComposeReplaced)
    }
    else sentence
  }

  // nocat=1
  def convertComposeDeHelper(sentence: String): String = {
    val composeDe = "\\{\\{compos(é\\sde)?\\|(.*?)\\}\\}".r.findFirstIn(sentence).get.replaceAll("\\{\\{", "").
      replaceAll("lang\\=(.*?)(?=\\||(\\}\\}))", "").replaceAll("m\\=(.*?)(?=\\||(\\}\\}))", "")
      .replaceAll("f\\=(.*?)(?=\\||(\\}\\}))", "").replaceAll("nocat\\=(.*?)(?=\\||(\\}\\}))", "").replaceAll("\\}\\}", "")

    val femMajOption = composeDeFemMaj(sentence)
    val deriveDeOption = deriveDe(sentence)

    val composeDeRemove = {
      if (femMajOption._1 && femMajOption._2 && deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "Dérivée de")
      else if (!femMajOption._1 && femMajOption._2 && deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "dérivée de")
      else if (!femMajOption._1 && !femMajOption._2 && deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "dérivé de")
      else if (femMajOption._1 && !femMajOption._2 && deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "Dérivé de")

      else if (femMajOption._1 && femMajOption._2 && !deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "Composée de")
      else if (!femMajOption._1 && femMajOption._2 && !deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "composée de")
      else if (femMajOption._1 && !femMajOption._2 && !deriveDeOption) composeDe.replaceAll("composé\\sde(?=\\|)", "Composé de")
      else composeDe.replaceAll("composé\\sde(?=\\|)", "composé de")
    }
    val composRemove: String = {
      if (femMajOption._1 && femMajOption._2 && deriveDe(sentence)) composeDe.replaceAll("compos(é\\sde)?\\|", "Dérivée de|")
      else if (!femMajOption._1 && femMajOption._2 && deriveDe(sentence)) composeDe.replaceAll("compos(é\\sde)?\\|", "dérivée de|")
      else if (!femMajOption._1 && !femMajOption._2 && deriveDeOption) composeDe.replaceAll("compos(é\\sde)?\\|", "dérivé de|")
      else if (femMajOption._1 && !femMajOption._2 && deriveDe(sentence)) composeDe.replaceAll("compos(é\\sde)?\\|", "Dérivé de|")

      else if (femMajOption._1 && femMajOption._2) composeDeRemove.replaceAll("compos(é\\sde)?\\|", "Composée de|")
      else if (!femMajOption._1 && femMajOption._2) composeDeRemove.replaceAll("compos(é\\sde)?\\|", "composée de|")
      else if (femMajOption._1 && !femMajOption._2) composeDeRemove.replaceAll("compos(é\\sde)?\\|", "Composé de|")
      else composeDeRemove.replaceAll("compos(é\\sde)?\\|", "composé de|")
    }


    val parserSensRemove = parserSens(composRemove)
    val parserTraductRemove = parserTraduct(parserSensRemove)
    val parserSuffixeRemove = parserSuffixe(parserTraductRemove)
    val parserPrefixeRemove = parserPrefixe(parserSuffixeRemove)

    val replacer = printout(parserPrefixeRemove)
    sentence.replaceFirst("\\{\\{compos(é\\sde)?\\|(.*?)\\}\\}", replacer)
  }

  def composeDeFemMaj(sentence: String): (Boolean, Boolean) = {
    val composeDe = "\\{\\{compos(é\\sde)?\\|(.*?)\\}\\}".r.findFirstIn(sentence).get
    val splitted = composeDe.split("\\|")

    val maj = splitted.filter(content => content.contains("m=1") || content.contains("m=oui"))
    val fem = splitted.filter(content => content.contains("f=1") || content.contains("f=oui"))
    (maj.size > 0, fem.size > 0)
  }

  def deriveDe(sentence: String) = {
    // if (sentence.contains("-|"))
    // if (sentence.contains("|-")) {
    sentence.contains("-|") ^ sentence.contains("|-")
  }

  def parserSens(sentence: String): String = {
    if ("(?<=\\|)(sens(\\d)?)\\=".r.findFirstIn(sentence).isDefined) {
      val convertedSens = sensHelper(sentence)
      parserSens(convertedSens)
    }
    else sentence
  }

  def sensHelper(sentence: String): String = {
    val regexMatch = ("(?<=\\|)(sens(\\d)?)\\=(.*?)(?=\\||\\}|$)").r.findFirstMatchIn(sentence).get
    val sens = regexMatch.group(3)
    val sensNbr = if (regexMatch.group(2) == null) "littéralement" else regexMatch.group(2)
    sentence.replaceFirst("(sens(\\d)?)\\=(.*?)(?=\\||\\}|$)", s"$sensNbr (« ${sens} »)")

  }

  def parserTraduct(sentence: String): String = {
    if ("(?<=\\|)(tr(\\d)?)\\=".r.findFirstIn(sentence).isDefined) {
      val convertedTraduct = traductHelper(sentence)
      parserTraduct(convertedTraduct)
    }
    else sentence
  }

  def traductHelper(sentence: String): String = {
    val regexMatch = ("(?<=\\|)(tr(\\d)?)\\=(.*?)(?=\\||\\}|$)").r.findFirstMatchIn(sentence).get
    val trad = regexMatch.group(3)
    val tradNbr = regexMatch.group(2)
    sentence.replaceFirst("(?<=\\|)(tr(\\d)?)\\=(.*?)(?=\\||\\}|$)", s"$tradNbr ''$trad''")
  }

  def parserSuffixe(sentence: String): String = {
    if (sentence.contains("|-")) {
      val convertedSuffixe = suffixeHelper(sentence)
      parserSuffixe(convertedSuffixe)
    }
    else sentence
  }

  def suffixeHelper(sentence: String): String = {
    sentence.replaceFirst("(?<=\\|)-", "avec le suffixe -")
  }

  def parserPrefixe(sentence: String): String = {
    if (sentence.contains("-|")) {
      val convertedPrefixe = prefixeHelper(sentence)
      parserPrefixe(convertedPrefixe)
    }
    else sentence
  }

  def prefixeHelper(sentence: String): String = sentence.replaceAll("-(?=\\|)", " avec le préfixe")

  // [dérivé de||méso avec le prefixe|xérophile]
  def printout(sentence: String): String = {
    val splitted = sentence.split("\\|").filterNot(_.isEmpty)
    val firstPart = splitted(0)
    val secondPart= splitted.filter(part => !part.contains("''")).filter(part => !part.contains("«")).filter(part => part != null).
      filter(part => !part.contains("avec le préfixe")).
      filter(part => part != "dérivé de").filter(part => part != "Dérivé de").filter(part => part != "Dérivée de").filter(part => part != "dérivée de").
      filter(part => part != "composé de").filter(part => part != "Composé de").filter(part => part != "Composée de").filter(part => part != "composée de").
      filter(part => part != "")

    /*
     || (part != "Composé de") ||  (part != "Composée de") || (part != "composée de")
     */
    val transThird = splitted.filter(part => part.contains("''"))
    val sensForth = splitted.filter(part => part.contains("«")).filter(part => !part.contains("littéralement "))
    val litteralement = splitted.filter(part => part.contains("littéralement "))
    val prefixe = prefixFix(splitted.filter(part => part.contains("avec le préfixe")))

    val middlePart =  secondPart.zipWithIndex.map { case sp =>
      if ((transThird.size > sp._2) && (sensForth.size > sp._2)) {
        if (sp._2 == secondPart.size - 1) s"${sp._1} ${transThird(sp._2).replaceAll("\\d", "")} ${sensForth(sp._2).replaceAll("\\d","")}"
        else s"${sp._1} ${transThird(sp._2).replaceAll("\\d", "")} ${sensForth(sp._2).replaceAll("\\d","")},"
      }
      else if (transThird.size > sp._2) {
        if (sp._2 == secondPart.size - 1) s"${sp._1}  ${transThird(sp._2).replaceAll("\\d", "")}"
        else s"${sp._1}  ${transThird(sp._2).replaceAll("\\d", "")},"
      }
      else if (sensForth.size > sp._2) {
        if (sp._2 == secondPart.size - 1) s"${sp._1} ${sensForth(sp._2).replaceAll("\\d", "")}"
        else s"${sp._1} ${sensForth(sp._2).replaceAll("\\d", "")},"
      }
      else if (sp._2 == secondPart.size - 1 && secondPart.size > 1 && !sp._1.contains("avec le suffixe")) s"et de ${sp._1.trim}"
      else s"${sp._1}"
    }.mkString(" ")
    s"$firstPart $middlePart $prefixe ${if (litteralement.size > 0) s", ${litteralement.last}" else ""}".trim.replaceAll(" +", " ").replaceAll(" ,", ",")
  }

  def prefixFix(prefix: Array[String]): String = {
    if (prefix.size > 0) {
     val regexM =  ("(.*?)\\s(avec le préfixe)").r.findFirstMatchIn(prefix.last).get
      s"${regexM.group(2)} ${regexM.group(1)}-"
    }
    else ""
  }
}