/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization.OntolexWordRdf.wordType
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.{RegexExtractors, SplitProcessing, TextCleaners}
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.{createIRI, createLiteral}
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model.ddf.{LexicalEntry, Sense}
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

object InflexionRdf {

  def serialize(model: ModelBuilder, sense: Sense, lentry: LexicalEntry, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]): Unit = {
    lentrySerialize(model, lentry, wikitext)
    LexicalSensRdf.senseSerialization(model, sense, lentry)

    if (TextCleaners.cleanWordTitle(lentry.lexicalEntryType).toLowerCase() == "verbe") {
      verbalInflection(model, createIRI(lentry.uri.toString), lentry.wordSectionContent.getOrElse(""), lentry.lexicalEntryType)
    }
    else {
      if (sense.definition.isDefined) {
        genderNumberTransitivity(model, createIRI(lentry.uri.toString), sense.definition.get, lentry.lexicalEntryType,  lentry.wordSectionContent.getOrElse(""))
      }
    }
  }

  def genderNumberTransitivity(model: ModelBuilder, lexicalEntryURI: IRI, definition: String, lexicalEntryType: String, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    val uriInflectablePos = infletionOfUri(definition, lexicalEntryType)

    uriInflectablePos.toList.foreach { iri =>
      model.subject(iri)
        .add("ddf:hasInflection", lexicalEntryURI)
        .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"))
      model.subject(lexicalEntryURI)
        .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#Inflection"))
        .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"))
    }

    val gender = lexicalQualificationRecog.recogFromVocab(wikitext, "http://data.dictionnairedesfrancophones.org/authority/gender")
    val number = lexicalQualificationRecog.recogFromVocab(wikitext, "http://data.dictionnairedesfrancophones.org/authority/number")

    val gender2 = lexicalQualificationRecog.recogFromVocabWithLabel(definition.toLowerCase(), "http://data.dictionnairedesfrancophones.org/authority/gender")
    val number2 = lexicalQualificationRecog.recogFromVocabWithLabel(definition.toLowerCase(), "http://data.dictionnairedesfrancophones.org/authority/number")

    gender.foreach(couple => model.subject(lexicalEntryURI).add("ddf:inflectionHasGender", couple._2))
    gender2.foreach(couple => model.subject(lexicalEntryURI).add("ddf:inflectionHasGender", couple._2))

    number.foreach(couple => model.subject(lexicalEntryURI).add("ddf:inflectionHasNumber", couple._2))
    number2.foreach(couple => model.subject(lexicalEntryURI).add("ddf:inflectionHasNumber", couple._2))
  }


  def verbalInflection(model: ModelBuilder, lexicalEntryURI: IRI, definition: String, lexicalEntryType: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    val mood = lexicalQualificationRecog.recogFromVocab(definition, "http://data.dictionnairedesfrancophones.org/authority/mood")
    val tense = lexicalQualificationRecog.recogFromVocab(definition, "http://data.dictionnairedesfrancophones.org/authority/tense")
    val person = lexicalQualificationRecog.recogFromVocab(definition, "http://data.dictionnairedesfrancophones.org/authority/person")

    mood.foreach(couple => model.subject(lexicalEntryURI).add(couple._1, couple._2))
    tense.foreach(couple => model.subject(lexicalEntryURI).add(couple._1, couple._2))
    person.foreach(couple => model.subject(lexicalEntryURI).add(couple._1, couple._2))

    val uriInflectablePos = infletionOfUri(definition, lexicalEntryType)
    uriInflectablePos.toList.foreach { iri =>
      model.subject(iri)
        .add("ddf:hasVerbalInflection", lexicalEntryURI)
        .add(RDF.TYPE, "ddf:Verb")
      model.subject(lexicalEntryURI)
        .add(RDF.TYPE, "ddf:VerbalInflection")
    }
  }

  def infletionOfUri(definition: String, lexicalEntryType: String): Option[IRI] = {
    val resourceTitles: Seq[String] = RegexExtractors.extractInflectionOf(definition).map { rgex =>
      rgex.group(2).split("\\|")
    }.flatten

    val resourceTitle = if (resourceTitles.size == 1) {
      Some(resourceTitles(0))
    }
    else if (resourceTitles.size == 2) {
      Some(resourceTitles(1))
    }
    else if (resourceTitles.size > 2) {
      None
    }
    else {
      None
    }

    if (resourceTitle.isDefined) {
      val ofMasculin = definition.toLowerCase.contains("féminin")
      if (ofMasculin) {
        Some(createIRI(WiktionnaireUriFactory.ontolexWord(resourceTitle.toString, TextCleaners.cleanWordSectionTitle(lexicalEntryType), Some("masculin"))))
      }
      else Some(createIRI(WiktionnaireUriFactory.ontolexWord(resourceTitle.toString, TextCleaners.cleanWordSectionTitle(lexicalEntryType), None)))
    }
    else None
  }

  def lentrySerialize(model: ModelBuilder, lexicalEntry: LexicalEntry, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]) = {
    val lexicalEntryIRI = createIRI(lexicalEntry.uri.toString)
    val formIRI = createIRI(WiktionnaireUriFactory.ontolexForm(lexicalEntry.pageTitle))

    model.subject(lexicalEntryIRI)
      .add("ontolex:canonicalForm", formIRI)
    model.subject(formIRI)
      .add(RDF.TYPE, createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"))
      .add("ontolex:writtenRep", createLiteral(TextCleaners.replaceApostrophe(lexicalEntry.pageTitle), "fr"))


    lexicalEntry.otherForms.toList.flatten.foreach { word =>
      val form = SplitProcessing.splitOtherForms(word)
      val otherFormIRI = createIRI(WiktionnaireUriFactory.ontolexForm(form))
      model.subject(lexicalEntryIRI)
        .add("ontolex:otherForm", otherFormIRI)
      model.subject(otherFormIRI)
        .add(RDF.TYPE, createIRI("ontolex:Form"))
        .add("ontolex:writtenRep", createLiteral(TextCleaners.replaceApostrophe(form), "fr"))
    }

    lexicalEntry.canonicalForm.toList.foreach { cf =>
      cf.phoneticRep.toList.flatten.foreach { phonen =>
        model.subject(formIRI)
          .add("ontolex:phoneticRep", phonen)
      }
    }

    val pos = lexicalQualificationRecog.recogFromVocab(lexicalEntry.lexicalEntryType, "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type")
    wordType(lexicalEntryIRI, pos, model)
    pos.foreach { couple =>
      model.subject(lexicalEntryIRI)
        .add(couple._1, couple._2)
    }

    RegexExtractors.extractCategories(wikitext).foreach { regex =>
      lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/glossary", s"${regex.group(0)}").foreach { vocab =>
        //(vocabMatch.property, createIRI(vocabMatch.concept))
        model.subject(lexicalEntryIRI)
          .add("ddf:entryHasGlossary", createIRI(vocab.concept))
      }
    }
  }
}
