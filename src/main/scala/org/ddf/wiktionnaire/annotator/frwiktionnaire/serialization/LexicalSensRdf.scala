/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model.ddf.{LexicalEntry, Sense}
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

object LexicalSensRdf {
  def serialize(model: ModelBuilder, lentry: LexicalEntry, lexicogEntryIRI: IRI)(implicit vocsRecognizer: VocsRecognizer) = {
    lentry.sense.toList.flatten.foreach { sense =>
      senseSerialization(model, sense, lentry)
    }

    senseRelation(model, lentry.sense.toList.flatten, lexicogEntryIRI, createIRI(lentry.uri.toString))
  }

  def senseSerialization(model: ModelBuilder, sense: Sense, lentry: LexicalEntry)(implicit vocsRecognizer: VocsRecognizer) = {
    if (sense.definition.isDefined) {
      val lexicalEntryIRI = createIRI(lentry.uri.toString)
      val usageExampleString = if (sense.usageExamples.isDefined) sense.usageExamples.get.mkString else ""
      val lexicalSensIRI = createIRI(WiktionnaireUriFactory.ontolexLexicalSense(s"${sense.definition.get} $usageExampleString", lentry.uri.toString))

      /**
       * lexical entry - lexical sense
       */

      model.subject(lexicalEntryIRI)
        .add("ontolex:sense", lexicalSensIRI)

      /**
       * lexical sense definition
       */

      model.subject(lexicalSensIRI)
        .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/ontolex#LexicalSense"))
        .add("skos:definition", TextCleaners.replaceApostrophe(TextCleaners.cleanDefinition(sense.definition.get)))

      sense.usageExamples.toList.flatten.foreach { usageExample =>
        val citationString = if (usageExample.bibliographicalCitation.isDefined) usageExample.bibliographicalCitation.get.mkString else ""
        val exampleIRI = createIRI(WiktionnaireUriFactory.lexicogUsageExample(s"${usageExample.example} $citationString"))

        /**
         * usage example
         */

        val exampleLiteral = TextCleaners.replaceApostrophe(TextCleaners.cleanDefinition(usageExample.example)).replaceAll("lang\\=fr\\|", "")

        if (!exampleLiteral.isEmpty) {

          model.subject(lexicalSensIRI)
            .add("lexicog:usageExample", exampleIRI)
          model.subject(exampleIRI)
            .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#UsageExample"))
            .add(createIRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#value"), exampleLiteral)
        }


        val citationLiteral = TextCleaners.cleanSource(citationString)

          if (!citationLiteral.isEmpty) {

            /**
             * citation
             */

            model.subject(exampleIRI)
            .add("http://purl.org/dc/terms/bibliographicalCitation", citationLiteral )

          }
      }
      vocsRecognizer.lexicalQualificationGenerate(sense.definition.get).foreach { couple =>
        model.subject(lexicalSensIRI)
          .add(couple._1, couple._2)
      }

    }
  }

  def senseRelation(model: ModelBuilder, sense: Seq[Sense], lexicogEntryIRI: IRI, lexicalEntryIRI: IRI)(implicit vocsRecognizer: VocsRecognizer) = {

    sense.zipWithIndex.foreach { case (sense, i) =>
      val definition = sense.definition
      if (definition.isDefined) {
        val vocabs = vocsRecognizer.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", definition.get)
        val usageExampleString = if (sense.usageExamples.isDefined) sense.usageExamples.get.mkString else ""
        val lexicalSensIRI = createIRI(WiktionnaireUriFactory.ontolexLexicalSense(s"${sense.definition.get} $usageExampleString", lexicalEntryIRI.toString))
        vocabs.foreach { vocab =>
          val uriRelation: IRI = createIRI(WiktionnaireUriFactory.ddfSemanticRelation(lexicogEntryIRI.toString, lexicalSensIRI.toString, vocab.concept))

          /**
           * semantic relation
           */

          model.subject(lexicalSensIRI)
            .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#lexicalSenseHasSemanticRelationWith", uriRelation)
          model.subject(uriRelation)
            .add(RDF.TYPE,createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"))
            .add(vocab.property, createIRI(vocab.concept))

        }
      }
    }
  }
}

