package org.ddf.wiktionnaire.annotator.frwiktionnaire.helper

import java.io.File

import org.ddf.wiktionnaire.annotator.helpers.{FileSystemToolKit, WiktionnaireAnnotatorConfig}

import scala.io.{BufferedSource, Source}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object EtymologyHelper {
  def loadCodesLang = {
    val file = new File(FileSystemToolKit.parsePath(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}codesDeLangues.tsv"))
    Source.fromFile(file, "UTF-8").getLines().map { line =>
      val tab = line.split("\t")
      (tab(1), tab(2))
    }.toMap
  }
}
