package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WModelParser {
  // {{w|Martin Luther}}
  // sentence.contains("{{w|")
  def parser(sentence: String): String = {
    if ("(\\{\\{w\\|)(.*?\\}\\})".r.findFirstIn(sentence).isDefined) {
      val converted = wModelHelper(sentence)
      parser(converted)
    }
    else sentence
  }

  def wModelHelper(sentence: String): String = {
     val wM = "(\\{\\{w\\|)(.*?\\}\\})".r.findFirstIn(sentence).get.replaceAll("\\{\\{w\\|", "[[").
      replaceAll("\\}\\}", "]]")
    sentence.replaceFirst("(\\{\\{w\\|)(.*?\\}\\})", s"$wM")
  }
}