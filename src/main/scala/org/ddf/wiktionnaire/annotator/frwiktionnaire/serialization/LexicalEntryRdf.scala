/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import com.typesafe.scalalogging.LazyLogging
import fr.mnemotix.wiktionnaire.templatereplacer.TemplateReplacer
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.{RegexExtractors, SplitProcessing, TextCleaners}
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.{createIRI, createLiteral}
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model.ddf.LexicalEntry
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.IRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

object LexicalEntryRdf extends LazyLogging {
  def serialize(model: ModelBuilder, lexicalEntry: LexicalEntry, lexicogEntryUri: IRI, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]): Unit = {
    val lexicalEntryIRI = createIRI(lexicalEntry.uri.toString)
    val formIRI = createIRI(WiktionnaireUriFactory.ontolexForm(lexicalEntry.pageTitle))
    val classForm = extractClassForm(lexicalEntry.pageTitle).stringValue()

    /**
     * lexicogEntry -> lexicalEntry RDF
     */

    model.subject(lexicogEntryUri)
      .add("lexicog:describes", lexicalEntryIRI)


    /**
     * lexical Entry Type
     */

    val termsElement = lexicalQualificationRecog.recogFromVocab(lexicalEntry.lexicalEntryType, "http://data.dictionnairedesfrancophones.org/authority/term-element")

    if(classForm == "http://www.w3.org/ns/lemon/ontolex#Word" && termsElement.isEmpty) {
      model.subject(lexicalEntryIRI)
        .add(RDF.TYPE, extractClassForm(lexicalEntry.pageTitle))


      OntolexWordRdf.serialize(model, lexicalEntry, lexicalEntryIRI)
    }
    else if(!termsElement.isEmpty) {
      OntolexTermElementRdf.serialize(model, lexicalEntryIRI)

      termsElement.map { tuple =>
        model.subject(lexicalEntryIRI)
          .add(tuple._1, tuple._2)
      }
    }
    else {
      OntolexMultiWordRdf.serialize(model, lexicalEntry, lexicalEntryIRI)
    }


    /**
     * form RDF
     */

    model.subject(lexicalEntryIRI)
      .add("ontolex:canonicalForm", formIRI)
    model.subject(formIRI)
      .add(RDF.TYPE, createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"))
      .add("ontolex:writtenRep", createLiteral(TextCleaners.replaceApostrophe(lexicalEntry.pageTitle), "fr"))

    /**
     * Other form RDF
     */

    lexicalEntry.otherForms.toList.flatten.foreach { word =>
      val form = SplitProcessing.splitOtherForms(word)
      val otherFormIRI = createIRI(WiktionnaireUriFactory.ontolexForm(form))
      model.subject(lexicalEntryIRI)
        .add("ontolex:otherForm", otherFormIRI)
      model.subject(otherFormIRI)
        .add(RDF.TYPE, createIRI("ontolex:Form"))
        .add("ontolex:writtenRep", createLiteral(TextCleaners.replaceApostrophe(form), "fr"))
    }

    /**
     * Etymology RDF
     */

    lexicalEntry.etymology.toList.foreach { etymologyText =>
      val etymologyIRI = createIRI(WiktionnaireUriFactory.siocPost(etymologyText))
      val cleaned = TemplateReplacer.replaceTemplates(TextCleaners.cleanDefinition(etymologyText))
      if (!cleaned.isEmpty) {
        model.subject(lexicalEntryIRI)
          .add("ddf:hasItemAboutEtymology", etymologyIRI)

        model.subject(etymologyIRI)
          .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#OnlineContribution"))
          .add("sioc:content", TextCleaners.replaceApostrophe(cleaned).replaceAll("^\\:","").trim)
          .add("ddf:aboutEtymology", lexicalEntryIRI)
      }

    }

    /**
     * Pronunciation RDF
     */

    lexicalEntry.canonicalForm.toList.foreach { cf =>
      cf.phoneticRep.toList.flatten.foreach { phonen =>
        model.subject(formIRI)
          .add("ontolex:phoneticRep", phonen)
      }
    }

    /**
     * lexical entry -> glossary
     */

    RegexExtractors.extractCategories(wikitext).foreach { regex =>
      lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/glossary", s"${regex.group(0)}").foreach { vocab =>
        //(vocabMatch.property, createIRI(vocabMatch.concept))
        model.subject(lexicalEntryIRI)
          .add("ddf:entryHasGlossary", createIRI(vocab.concept))
      }
    }
  }

  def extractClassForm(pageTitle: String) = {
    val split1 = pageTitle.trim.split(" ").size
    val split3 = pageTitle.trim.split("'").size
    if (split1 > 1 | split3 > 1) createIRI("http://www.w3.org/ns/lemon/ontolex#MultiWordExpression") else createIRI("http://www.w3.org/ns/lemon/ontolex#Word")
  }
}