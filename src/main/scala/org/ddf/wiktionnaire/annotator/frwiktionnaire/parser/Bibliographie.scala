package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Bibliographie {

  def parse(sentence: String): String = {
    sentence.replace("{{R:TLFi}}", "TLFi, Le Trésor de la langue française informatisé, 1971-1994.").
      replace("{{R|Darwin&Royer|{{w|Charles Darwin}}", "Darwin&Royer, Charles Darwin").
      replace("{{R:ADA}}", "Dr. Rüdiger Riehl et Hans A. Baensch, Atlas de l’aquarium, 2001.").
      replace("{{R:Riehl-Baensch}}", "Dr. Rüdiger Riehl et Hans A. Baensch, Atlas de l’aquarium, 2001.").
      replace("{{R:Alexandria}}", "Alexandria, Memodata, 2009, dictionnaire.sensagent.com, en ligne.").
      replace("{{R:Sensagent}}", "Alexandria, Memodata, 2009, dictionnaire.sensagent.com, en ligne.").
      replace("{{R:Babault}}", "Babault, Dictionnaire français et géographique, contenant, outre tous les mots de la Langue Française, des sciences et des arts, la nomenclature de toutes les communes de France et des villes les plus remarquables du Monde; etc., Paris, 1836").
      replace("{{R:BDLP}}", "Base de données lexicographiques panfrancophone, AUF et TLFQ, 2001-2014, en ligne.").
      replace("{{R:Bélisle}}", "Louis-Alexandre Bélisle, Dictionnaire Bélisle de la langue française au Canada, ca. 1958.").
      replace("{{R:Berhaut 1967}}", "J. Berhaut, Flore du Sénégal, Clairafrique, 1967.").
      replace("{{R:Bescherelle}}", "Louis-Nicolas Bescherelle (dit Bescherelle aîné), Dictionnaire national, ou dictionnaire universel de la langue française, 1856, quatrième édition, en deux tomes.").
      replace("{{R:Bescherelle1875}}", "Louis-Nicolas Bescherelle (dit Bescherelle aîné), Dictionnaire national, ou dictionnaire universel de la langue française, 1875, quinzième édition, en deux tomes.").
      replace("{{R:BHVF}}", "Base Historique du Vocabulaire Français, ATILF (Analyse et traitement informatique de la langue française), 2019, en ligne.").
      replace("{{R:BO106}}", "Bulletin officiel du Ministère des Affaires étrangères, n°106, Paris, janvier-mars 2009, en ligne.").
      replace("{{R:Boiste}}", "Pierre-Claude-Victor Boiste, Dictionnaire universel de la langue française, avec le latin et les étymologies, 1836.").
      replace("{{R:Chéruel}}", "Adolphe Chéruel, Dictionnaire historique des institutions, mœurs et coutumes de la France, 1899.").
      replace("{{R:Classification phylogénétique}}", "G. Lecointre & H. Leguyader, Classification phylogénétique du vivant, 2001.").
      replace("{{R:Corblet}}", "Jules Corblet, Glossaire étymologique et comparatif du patois picard, ancien et moderne, 1851.").
      replace("{{R:Couplan2006}}", "François Couplan, Dictionnaire étymologique de botanique, Delachaux et Niestlé, coll. « Les références du naturaliste » p.34, 2006, 238 p.").
      replace("{{R:CNRTL}}", "Centre national de ressources textuelles et lexicales, 2009.").
      replace("{{R:CRISCO}}", "Centre de Recherche Inter-langues sur la Signification en COntexte (CRISCO), Dictionnaire Électronique des Synonymes, Université de Caen, www.crisco.unicaen.fr/des/, 2008–2013.").
      replace("{{R:CrozesAv}}", "Daniel Crozes, Les 1001 mots de l’Aveyron, Éditions du Rouergue, 2010.").
      replace("{{R:DA}}", "Hassane Makki, Dictionnaire des arabismes, 2005.").
      replace("{{R:DAF1}}", "Dictionnaire de l’Académie française, première édition, 1694.").
      replace("{{R:DAF2}}", "Dictionnaire de l’Académie française, deuxième édition, 1718.").
      replace("{{R:DAF3}}", "Dictionnaire de l’Académie française, troisième édition, 1740.").
      replace("{{R:DAF4}}", "Dictionnaire de l’Académie française, quatrième édition, 1762.").
      replace("{{R:DAF5}}", "Dictionnaire de l’Académie française, cinquième édition, 1798.").
      replace("{{R:DAF6}}", "Dictionnaire de l’Académie française, sixième édition, 1832-1835.").
      replace("{{R:DAF7}}", "Dictionnaire de l’Académie française, septième édition, 1878.").
      replace("{{R:DAF8}}", "Dictionnaire de l’Académie française, huitième édition, 1932-1935.").
      replace("{{R:DAF9}}", "Dictionnaire de l’Académie française, neuvième édition, 1992–.").
      replace("{{R:DauzatRostaing}}", "Albert Dauzat et Charles Rostaing, Dictionnaire étymologique des noms de lieux en France, Librairie Guénégaud, 1978, 2e édition.").
      replace("{{R:DÉCT}}", "Dictionnaire Électronique de Chrétien de Troyes, en ligne.").
      replace("{{R:DEHLF}}", "Emmanuèle Baumgartner et Philippe Ménard, Dictionnaire étymologique et historique de la langue française, Paris, Livre de Poche, 1996.").
      replace("{{R:DEL}}", "Alain Rey et Sophie Chantreau, Dictionnaire d’expressions et locutions, 2003.").
      replace("{{R:DHLF1980}}", "Françoise Guérard (dir.) et al., Dictionnaire Hachette de la langue française, Hachette, 1980.").
      replace("{{R:Dico étymo Bonaventure}}", "Jean-Baptiste-Bonaventure de Roquefort-Flaméricourt, Dictionnaire étymologique de la langue françoise, où les mots sont classés par familles, 1829.").
      replace("{{R:DicoExcent}}", "Lorédan Larchey, Les Excentricités du langage, Cinquième édition, 1865.").
      replace("{{R:DicoHayard}}", "Napoléon Hayard, Dictionnaire Argot-Français, L. Hayard, Paris, 1907, en ligne.").
      replace("{{R:DLV2}}", "Alfred Delveau, Dictionnaire de la Langue Verte, E. Dentu, Paris, 2e édition, 1866, en ligne.").
      replace("{{R:DMM2007}}", "Jacques Quevauvilliers (dir.), Dictionnaire médical, Masson, 5e édition, 2007.").
      replace("{{R:DPC}}", "Albert Deshayes, Dictionnaire des prénoms celtiques, 2000.").
      replace("{{R:DRUHN1764}}", "Jacques-Christophe Valmont de Bomare, Dictionnaire raisonné universel d'histoire naturelle, 1764.").
      replace("{{R:DSR}}", "André Thibault, Pierre Knecht, Dictionnaire suisse romand, éditions Zoé, Genève, 1997 (1re édition), 2004 (2e édition).").
      replace("{{R:Dupré}}", "Paul Dupré, Encyclopédie du bon français dans l’usage contemporain, 1972.").
      replace("{{R:DUVF1}}", "Louis-Nicolas Bescherelle et Henri Bescherelle, Dictionnaire des verbes ou Dictionnaire usuel de tous les verbes français, tome 1, 1843.").
      replace("{{R:DUVF2}}", "Louis-Nicolas Bescherelle et Henri Bescherelle, Dictionnaire des verbes ou Dictionnaire usuel de tous les verbes français, tome 2, 1843.").
      replace("{{R:Féraud}}", "Jean-François Féraud, Dictionaire critique de la langue française, 1787-1788.").
      replace("{{R:FEW-abr}}", "Oscar Bloch, Walther von Wartburg, Dictionnaire étymologique de la langue française, 2e éd., Paris, Presses universitaires de France, 1950.").
      replace("{{R:FFF1989}}", "J. C. Rameau, D. Mansion, G. Dumé et al., Flore forestière française, 1989.").
      replace("{{R:FoucaultRaoult}}", "Alain Foucault et Jean-François Raoult, Dictionnaire de géologie, Dunod, 2005 (6e édition).").
      replace("{{R:FranceTerme}}", "Délégation générale à la langue française et aux langues de France, FranceTerme, en ligne.").
      replace("{{R:GDT}}", "Le Grand Dictionnaire terminologique, Office québécois de la langue française, en ligne.").
      replace("{{R:Grevisse}}", "Maurice Grevisse, Le Bon Usage, 1936.").
      replace("{{R:Guide pratique des champignons}}", "Cécile Lemoine et Georges Claustres, Guide pratique des champignons, Éditions Jean-Paul Gisserot, Paris, 1997.").
      replace("{{R:Hatzfeld Darmesteter}}", "Adolphe Hatzfeld, Arsène Darmesteter, Antoine Thomas, Dictionnaire général de la langue française du commencement du XVIIe siècle à nos jours, Delagrave, Paris, 1890-1893.").
      replace("{{R:Herbier méditerranéen 2007}}", "Claude Meslay, Marie-Françoise Delarozière, Herbier méditerranéen, Édisud, 2007.").
      replace("{{Import:CFC}}", "« Glossaire de cartographie », dans le Bulletin du Comité Français de Cartographie, mars-juin 1990, no 123-124, Paris (2e édition).").
      replace("{{R:IPLFM 1982}}", "Groupe de Travail IFM de l'AELIA, Inventaire des particularités lexicales du Français au Mali, Publications de l’Association d’études linguistiques interculturelles africaines, 1982.").
      replace("{{R:Jespers-noms de lieux}}", "Jean-Jacques Jespers, Dictionnaire des noms de lieux en Wallonie et à Bruxelles, Racine, 2005.").
      replace("{{R:L’Internaute}}", "L’Internaute.com : Encyclopédie, CCM Benchmark, 2000–2019.").
      replace("{{R:Landais}}", "Napoléon Landais, Dictionnaire général et grammatical des dictionnaires français, 1846, 9e édition.").
      replace("{{R:Larousse.fr}}", "Larousse.fr, Éditions Larousse 2009.").
      replace("{{R:Larousse}}", "Larousse.fr, Éditions Larousse 2009.").
      replace("{{R:LarousseXIXe}}", "Pierre Larousse, Grand Dictionnaire universel du XIXe siècle, en 15 volumes (1866-1877) et 2 volumes de suppléments (1878-1888).").
      replace("{{R:LarousseXXe}}", "Larousse du XXe siècle, 1928-1933.").
      replace("{{R:Larousse1898}}", "Claude Augé, Nouveau Larousse illustré - Dictionnaire universel encyclopédique, en 7 volumes et un supplément (1898), Première édition sous la Direction de Claude Augé. Première édition contenant des illustrations en couleurs.").
      replace("{{R:Larousse10vol1960}}", "Grand Larousse encyclopédique en 10 volumes, 1960-1964.").
      replace("{{R:Larousse2vol1922}}", "Larousse universel en 2 volumes, 1922.").
      replace("{{R:Lecomte}}", "Charles Lecomte, Le parler dolois, étude et glossaire des patois comparés de l'arrondissement de Saint-Malo suivi d'un relevé des locutions et dictons populaires, Honoré Champion, 1910, 264 p.").
      replace("{{R:Legoarant}}", "Benjamin Legoarant, Nouveau Dictionnaire critique de la langue française, Librairie de veuve Berger-Levrault et fils, Paris/Strasbourg, 1858).").
      replace("{{R:Leland}}", "Nouvelle Encyclopédie du monde (18 volumes), Éditions Leland / Librairie Aristide Quillet, 1962.").
      replace("{{R:Lexique typographique 1917}}", "Jean Dumont, Lexique typographique : complément du Vade-mecum du typographe, 1917.").
      replace("{{R:Linternaute}}", "Linternaute.com, Édité par CCM Benchmark Group, 2016, en ligne.").
      replace("{{R:Littré}}", "Émile Littré, Dictionnaire de la langue française, deuxième édition, 1872-1877.").
      replace("{{R:Littré 1863}}", "Émile Littré, Dictionnaire de la langue française, deuxième édition, 1872-1877.").
      replace("{{R:LPDL}}", "Gilbert Salmon, Le Parler du Lyonnais, Christine Bonneton Éditeur, Paris, 2010.").
      replace("{{R:Mediadico}}", "Dictionnaire Mediadico, 1987-2010, en ligne.").
      replace("{{R:Médical}}", "Dictionnaire de l’Académie nationale de médecine, en ligne.").
      replace("{{R:Ménage 1750}}", "Gilles Ménage, Dictionnaire étymologique de la Langue Française, Paris, 1750.").
      replace("{{R:Meyer}}", "Christian Meyer, éditeur scientifique, Dictionnaire des sciences animales, Cirad, Montpellier, France, 2019, en ligne.").
      replace("{{R:Mieux connaître les champignons}}", "Cécile Lemoine et Georges Claustres, Mieux connaître les champignons, Éditions Jean-Paul Gisserot, Paris, 1995.").
      replace("{{R:Moréri1759}}", "Louis Moréri, Grand Dictionnaire historique, 1759.").
      replace("{{R:Morisot1814}}", "Joseph-Marie-Rose Morisot, Vocabulaire des arts et métiers en ce qui concerne les constructions, volume V, Firmin Didot, Paris, 1814.").
      replace("{{R:Morisot}}", "Joseph-Marie-Rose Morisot, Vocabulaire des arts et métiers en ce qui concerne les constructions, volume V, Firmin Didot, Paris, 1814.").
      replace("{{R:Morphalou}}", "Morphalou, Centre national de ressources textuelles et lexicales, 2009, en ligne.").
      replace("{{R:MoureauBrace}}", "Magdeleine Moureau et ‎Gerald Brace, Dictionnaire des sciences de la Terre, Publications de l’Institut français du pétrole, Éditions Technip, Paris, 2000.").
      replace("{{R:Neyron}}", "Pierre Neyron, Nouveau dictionnaire étymologique (néologismes), Éditions de la Revue moderne, 1970.").
      replace("{{R:Nicot}}", "Jean Nicot, Thresor de la langue françoyse, 1606.").
      replace("{{R:Nicot1606}}", "Jean Nicot, Thresor de la langue françoyse, 1606.").
      replace("{{R:Nodier}}", "Charles Nodier, V. Verger, Dictionnaire universel de la langue française, Belin-Mandar, 1835, 7e édition.").
      replace("{{R:Noël 1839}}", "F. J. M. Noël, L. J. Carpentier, Dictionnaire étymologique, critique, historique, anecdotique et littéraire, 1839").
      replace("{{R:Nouveau Logos}}", "Jean Girodet, Le Nouveau Logos, Dictionnaire de la langue française, Encyclopédies Bordas, SGED, 1994.").
      replace("{{R:NPLarousse}}", "Nouveau Petit Larousse Illustré, Dictionnaire Encyclopédique, Librairie Larousse, Paris, 1952.").
      replace("{{R:Nysten}}", "Pierre Hubert Nysten, Dictionnaire de médecine, 1855.").
      replace("{{R:Nysten1855}}", "Pierre Hubert Nysten, Dictionnaire de médecine, 1855.").
      replace("{{R:Nysten1873}}", "Pierre Hubert Nysten, Dictionnaire de médecine, 1873.").
      replace("{{R:Obsolète}}", "Alain Duchesne et Thierry Leguay, L’Obsolète, dictionnaire des mots perdus, 1988 version originale, 1989.").
      replace("{{R:ODS5}}", "L’Officiel du jeu Scrabble, Éditions Larousse, 2007, 5e édition.").
      replace("{{R:Oudin}}", "Antoine Oudin, Dictionnaire italien et françois, 1663.").
      replace("{{R:Peigné}}", "M. A. Peigné, Nouveau Dictionnaire de poche de la langue française, Chabert, Paris, 1834.").
      replace("{{R:PLarousse}}", "Petit Larousse").
      replace("{{R:PLM1}}", "André Domart et Jacques Bourneuf, Petit Larousse de la Médecine tome 1, 1976.").
      replace("{{R:PLM2}}", "André Domart et Jacques Bourneuf, Petit Larousse de la Médecine tome 2, 1976.").
      replace("{{R:PRobert}}", "Petit Robert").
      replace("{{R:PRobertF}}", "Petit Robert").
      replace("{{R:Poissons de mer des pêches françaises}}", "Jean-Claude Quéro, Poissons de mer des pêches françaises, Delachaux et Niestlé, 1997.").
      replace("{{R:Quillet}}", "Raoul Mortier, Dictionnaire Quillet de la langue française, en 4 volumes, Éditions Quillet, 1re édition 1975.").
      replace("{{R:Quillet-Grolier1975}}", "Dictionnaire encyclopédique Quillet-Grolier (16 volumes), Grolier Limitée / Librairie Aristide Quillet, 1968–1975.").
      replace("{{R:Raynouard}}", "François Raynouard, Lexique roman ou Dictionnaire de la langue des troubadours, comparée avec les autres langues de l’Europe latine, 1838–1844.").
      replace("{{R:Reverso}}", "Reverso (dictionnaire.reverso.net), Synapse, Softissimo, 2007, en ligne.").
      replace("{{R:Rivarol}}", "Antoine de Rivarol, Dictionnaire classique de la langue française, 1827.").
      replace("{{R:RobertDHLF}}", "Alain Rey, Dictionnaire historique de la langue française, Dictionnaires LE ROBERT, Paris, 1992.").
      replace("{{R:SantéActionSociale}}", "Régine Barrès, Anne-Marie Henrich, Danièle Rivaud et Nicolas Tanti-Hardouin, Dictionnaire de la santé et de l’action sociale, La dicothèque Foucher, 2005.").
      replace("{{R:Scheler 1862}}", "Auguste Scheler, Dictionnaire d’étymologie française d’après les résultats de la science moderne, 1862.").
      replace("{{R:StarLing}}", "Sergei Starostin, G. Bronnikov, Phil Krylov, Bases de données étymologiques StarLing, 1998–2014.").
      replace("{{R:T&T}}", "Alain Duchesne et Thierry Leguay, Turlupinades & tricoteries, dictionnaire des mots obsolètes de la langue française, 2004.").
      replace("{{R:Termium}}", "Bureau de la traduction du Canada, Termium, 1976-, en ligne.").
      replace("{{R:TGF1}}", "Ernest Nègre, Toponymie générale de la France, volume Ier, 1990.").
      replace("{{R:TGF2}}", "Ernest Nègre, Toponymie générale de la France, volume II, 1990.").
      replace("{{R:TLFi2}}", "TLFi, Le Trésor de la langue française informatisé, 1971-1994.").
      replace("{{R:Traité complet de la prononciation française 1871}}", "M.-A. Lesaint, Traité complet de la prononciation française dans la seconde moitié du XIXe siècle, Wilhelm Mauke, Hambourg, 1871.").
      replace("{{R:Trévoux}}", "[Jésuites de] Trévoux, Dictionnaire universel françois et latin, 1704-1771.").
      replace("{{R:Van Daele}}", "Hilaire Van Daele, Petit Dictionnaire de l’ancien français, Garnier, 1940.").
      replace("{{R:Veneroni}}", "Giovanni Veneroni, Le Maître italien, 1729.").
      replace("{{R:Inventaire}}", "Équipe IFA, Inventaire des particularités lexicales du français en Afrique noire, EDICEF/AUF, 2004, première édition en 1983.").
      replace("{{R|Gd Robert}}", " Le Grand Robert de la langue française, 1989")
  }
}
