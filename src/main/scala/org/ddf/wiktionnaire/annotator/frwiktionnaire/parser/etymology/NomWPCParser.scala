package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// hobbesien = {{nom w pc|Thomas|<i>Hobbes</i>}}
// berzélianite = {{nom w pc|Jöns Jacob|Berzélius}}
// selin = {{nom w pc|Jean-Baptiste de|Lamarck}}
// ringwoodite = {{pc|Binns}}, {{pc|Davis}}
// thénardite = {{nom w pc|lang=es|José Luis|Casaseca}}
// brucite = (1824) Nommée par le minéralogiste français {{nom w pc|François Sulpice|Beudant}} en l’honneur du minéralogiste américain {{nom w pc|Archibald|<i>Bruce</i>|lang=en|Archibald Bruce (mineralogist)}} (1777–1818).
// {{pc|Barnais}} [Georges Auguste Charles {{pc|Guibourg}}, dit {{nom w pc||Georgius}}


class NomWPCParser {
  def parser(sentence: String):String = {
    if ("\\{\\{nom w pc\\|(.*?)\\}\\}".r.findFirstIn(sentence).isDefined) {
      val parsed = parserNomWPC(sentence).trim
      parser(parsed)
    }
    else if ("\\{\\{pc\\|(.*?)\\}\\}".r.findFirstIn(sentence).isDefined) {
      val parsed = parserPC(sentence).trim
      parser(parsed)
    }
    else sentence.trim
  }

  def parserPC(sentence: String): String = {
    val pc = "\\{\\{pc\\|(.*?)}}".r.findFirstIn(sentence).get.replaceAll("\\{\\{pc", "").replaceAll("}}", "")
    val splitted = pc.split("\\|")
    val converted = splitted.mkString(" ").trim
    sentence.replaceFirst("\\{\\{pc\\|(.*?)}}", converted)
  }

  def parserNomWPC(sentence: String): String = {
    val wpc = "\\{\\{nom w pc\\|(.*?)}}".r.findFirstIn(sentence).get.replaceAll("\\{\\{nom w pc", "").replaceAll("}}", "")
    val splitted = wpc.split("\\|")
    val filtered = splitted.filterNot(split => split.matches("lang=.*")).map(_.trim)
    val converted = filtered.mkString(" ").trim
    sentence.replaceFirst("\\{\\{nom w pc\\|(.*?)}}", converted)
  }
}