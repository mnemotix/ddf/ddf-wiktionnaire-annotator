package org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing

import com.typesafe.scalalogging.LazyLogging

import java.net.URI
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model.{LexicalEntrySection, Section, VocabularyEntity}
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer

import scala.util.matching.Regex

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object SplitProcessing extends LazyLogging {

  lazy val posTypeVoc = "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type"
  lazy val multiWordVoc = "http://data.dictionnairedesfrancophones.org/authority/multiword-type"
  lazy val termElementVoc = "http://data.dictionnairedesfrancophones.org/authority/term-element"

  def splitByLang(wikitext: String): String = {
    val frSection = wikitext.split(("==\\s?\\{\\{langue\\|"))
      .filter(text => text.startsWith("fr}} ==") || text.startsWith("fr}}=="))
    frSection(0).replaceFirst("fr\\}\\}(\\s)?=="," ").trim
  }

  def splitByH3(wikitext: String): Iterator[Regex.Match] = {
    ("(?m)(^===\\s\\{\\{S\\|(.*?)\\}\\}\\s===$)(\\n^.*?)*((?=(^===\\s\\{\\{S\\|(.*?)\\}\\}\\s===$))|\\n^.*)").r.findAllMatchIn(wikitext)
  }

  def splitByH(wikitext: String): Iterator[Regex.Match] = {
    "(?m)(^=*\\s\\{\\{S\\|(.*?)\\}\\}\\s=*$)(\\n^.*?)*((?=(^=*\\s\\{\\{S\\|(.*?)\\}\\}\\s=*$))|\\n^.*)".r.findAllMatchIn(wikitext)
  }

  def splitBySection(wikitext: String): Seq[Section] = {
    splitByH(wikitext).map { regex =>
      Section(RegexExtractors.extractSectionTitle(regex.toString()), RegexExtractors.extractSectionContent(regex.toString()))
    }.toSeq
  }

  def splitByLexicalEntry(wikitext: String, resourceTitle: String)(implicit lexicalQualificationRecog: VocsRecognizer): Seq[LexicalEntrySection] = {
    splitByH3(wikitext)
      .filter { regex =>
        val vocTypes = lexicalQualificationRecog.filterVocsByType(posTypeVoc) ++ lexicalQualificationRecog.filterVocsByType(multiWordVoc) ++ lexicalQualificationRecog.filterVocsByType(termElementVoc)
        isType(regex.group(1), vocTypes)
      }
      .map { regex =>
        val lexicalInfo = RegexExtractors.extractLexicalInfo(regex.toString())
        val uriEntry = if (lexicalInfo.isDefined) {
          val vocs = lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/gender", lexicalInfo.get)
          if (!vocs.isEmpty) {
            URI.create(WiktionnaireUriFactory.ontolexWord(resourceTitle.trim, TextCleaners.cleanWordSectionTitle(regex.group(1)), Some(vocs(0).conceptLabel)))
          }
          else {
            URI.create(WiktionnaireUriFactory.ontolexWord(resourceTitle.trim, TextCleaners.cleanWordSectionTitle(regex.group(1)), None))
          }
        }
        else {
          URI.create(WiktionnaireUriFactory.ontolexWord(resourceTitle.trim, TextCleaners.cleanWordSectionTitle(regex.group(1)), None))
        }
        LexicalEntrySection(uriEntry, regex.group(1), splitBySection(regex.toString()), isInflection(regex.group(1)))
      }.toSeq
  }

  def isInflection(header: String): Boolean = {
    header.contains("flexion")
  }

  def splitEtymologyByPost(etymologyText: Option[String], lexicalEntryType: String, lexicalEntrysections: Seq[Section])(implicit lexicalQualificationRecog: VocsRecognizer) = {
    if (!etymologyText.isDefined) None
    else {
      val etymMatches = RegexExtractors.extractEtymologyByPOS(etymologyText.get)
      val result: String =
        if (etymMatches.size < 2) {
        etymologyText.get
      }
      else {
        val mid = etymMatches.filter { m =>
          val etymPOS = TextCleaners.cleanEtymologiesStartOfText(m.group(2))
          val res = etymPOS.split(" ").map(part => lexicalEntryType.contains(part))
          res.forall(_ == true)
        }
        if (mid.size < 1) etymologyText.get
        else mid.map(m =>
          if(m.groupCount < 5) etymologyText.get
          else m.group(5).toString).last.trim
      }
      //logger.debug(s"etymology all text : $etymologyText \n lexicalEntryType : $lexicalEntryType \n resultat : $result")
      Some(result)
    }
  }

  def isType(title: String, vocabularyTypes: Seq[VocabularyEntity]) = {
    if (vocabularyTypes.map(vocab => vocab.notation.contains(TextCleaners.cleanWordTitle(title))).filter(_ == true).size > 0) true
    else false
  }

  def splitByDefinition(text: String): Array[String] = {
    text.split("^#{1}(?!\\*)\\s{0,}")
  }

  def splitOtherForms(otherForm: String) = {
    if (otherForm.contains("|")) {
      otherForm.split("\\|").last
    }
    else otherForm
  }
}