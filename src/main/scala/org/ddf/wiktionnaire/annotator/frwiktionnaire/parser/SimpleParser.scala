package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


class SimpleParser {
  def convertSimple(sentence:String):String = {
    sentence.replaceAll("\\{\\{déverbal de\\}\\}", "Déverbal de").
      replaceAll("\\{\\{aphérèse(.*?)\\}\\}", "aphérèse").
      replaceAll("\\{\\{antonomase(.*?)\\}\\}", "(Antonomase)").
      replaceAll("\\{\\{acronyme\\|fr\\}\\}", "(Acronyme)").
      replaceAll("\\{\\{agglutination(.*?)\\}\\}","agglutination").
      replaceAll("\\{\\{apocope\\|(.*?)\\}\\}", "apocope").
      replaceAll("\\{\\{abréviation\\|fr\\}\\}", "abréviation").
      replaceAll("\\{\\{contraction(.*?)\\}\\}", "contraction").
      replaceAll("\\{\\{cf\\}\\}", "-> voir").
      replaceAll("\\{\\{déverbal\\}\\}", "Déverbal de").
      replaceAll("\\{\\{dénominal de\\}\\}", "Dénominal de").
      replaceAll("\\{\\{e\\}\\}", "e").
      replaceAll("\\{\\{dénominal\\}\\}", "Dénominal de").
      replaceAll("\\{\\{déglutination(.*?)\\}\\}", "déglutination").
      replaceAll("\\{\\{diminutif(.*?)\\}\\}", "diminutif").
      replaceAll("\\{\\{ellipse(.*?)\\}\\}", "(Par ellipse)").
      replaceAll("\\{\\{ortho1990\\}\\}", "orthographe rectifiée de 1990").
      replaceAll("\\{\\{tradit\\}\\}", "orthographe traditionnelle").
      replaceAll("\\{\\{voir\\sthésaurus\\|fr\\|(.*?)\\}\\}", "").
      replaceAll("\\{\\{parataxe\\|fr\\}\\}", "parataxe").
      replaceAll("\\{\\{apocope\\|fr\\}\\}", "apocope").
      replaceAll("\\{\\{apocope(.*?)\\}\\}", "apocope").
      replaceAll("\\{\\{ébauche-étym\\|fr\\}\\}", "").
      replaceAll("\\{\\{ébauche-déf\\|fr\\}\\}", "").
      replaceAll("\\{\\{ébauche-exe\\|fr\\}\\}","").
      replaceAll("\\{\\{ébauche\\|fr\\}\\}", "").
      replaceAll("\\{\\{exemple\\|lang=fr\\}\\}", "").
      replaceAll("\\{\\{louchébem(.*?)\\}\\}","(louchébem)").
      replaceAll("\\{\\{note\\}\\}", "'''Note : '''").
      replaceAll("\\{\\{mot\\-valise(.*?)\\}\\}","mot-valise").
      replaceAll("\\{\\{refnec\\|lang\\=fr\\}\\}", "").
      replaceAll("\\{\\{réf\\}\\}", "").
      replaceAll("\\{\\{réf\\|(.*?)\\}\\}", "").
      replaceAll("\\{\\{unités\\|fr\\|de\\svolume\\}\\}", "(Métrologie)").
      replaceAll("\\{\\{unités\\|fr\\|de\\smasse\\}\\}", "(Métrologie)").
      replaceAll("\\{\\{parataxe(.*?)\\}\\}", "(parataxe)").
      replaceAll("\\{\\{reverlanisation(.*?)\\}\\}", "(reverlanisation)").
      replaceAll("\\{\\{recons\\}\\}", "<sup>*</sup>").
      replaceAll("\\{\\{siècle\\}\\}", "(Siècle à préciser)").
      replaceAll("\\{\\{sigle\\|fr\\}\\}", "(sigle)").
      replaceAll("\\{\\{syncope(.*?)\\}\\}", "(syncope)").
      replaceAll("\\{\\{verlan(.*?)\\}\\}", "(verlan)").
      replaceAll("\\{\\{univerbation(.*?)\\}\\}", "(univerbation)").
      replaceAll("\\{\\{\\?\\|la\\sdéfinition\\sest\\}\\}", "pas de définition").
      replaceAll("\\{\\{er\\}\\}", "<sup>er</sup>").
      replaceAll("\\{\\{in\\|(\\d)\\}\\}", "<sub>$1</sub>").
      replaceAll("\\{\\{anglicisme\\|fr\\}\\}", "(Anglicisme)").
      replaceAll("\\{\\{méton\\|fr\\}\\}", "").
      replaceAll("\\<ref\\>(.*?)\\<\\/ref\\>", "").
      replaceAll("\\{\\{exemple\\|", "").
      replaceAll("\\{\\{couleur\\|.*?\\}\\}","").
      replaceAll("\\{\\{particulier}}", "").
      replaceAll("\\{\\{lexique\\|.*?}}", "").
      replaceAll("\\{\\{n°\\}\\}", "n°").
      replaceAll("\\{\\{nobr\\|(.*?)\\}\\}", "$1").
      replaceAll("\\(\\(", "(").
      replaceAll("\\)\\)", ")").
      replaceAll("\\{\\{\\?\\|.*?\\}\\}", "")
  }
}