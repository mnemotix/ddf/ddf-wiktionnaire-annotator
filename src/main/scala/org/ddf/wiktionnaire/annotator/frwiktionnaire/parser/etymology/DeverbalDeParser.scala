package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DeverbalDeParser {

  def parser(sentence: String): String = {
    if ("\\{\\{déverbal\\s(de|(sans\\ssuffixe))\\|(.*?)\\}\\}".r.findFirstIn(sentence).isDefined) {
      val converted = parserDeverbalHelper(sentence)
      parser(converted)
    }
    else sentence
  }

  def parserDeverbalHelper(sentence: String): String = {
    val deverbal = "\\{\\{déverbal\\s(de|(sans\\ssuffixe))\\|(.*?)\\}\\}".r.findFirstIn(sentence).get.replaceAll("\\{\\{", "").
      replaceAll("\\|fr\\}\\}", "").
      replaceAll("\\|fr\\|", "").
      replaceAll("de\\=", "").
      replaceAll("lang\\=(.*?)(\\||\\}\\})", "").
      replaceAll("m\\=\\d", "").
      replaceAll("\\}\\}", "")
    val splitted = deverbal.split("\\|")

    val converted = splitted.zipWithIndex.map { case valueS =>
      if (valueS._1.contains("déverbal de")) valueS._1
      else if (valueS._1.contains("déverbal sans suffixe")) "déverbal de"
      else s"[[${valueS._1}]]"
    }.mkString(" ")
    sentence.replaceFirst("\\{\\{déverbal\\s(de|(sans\\ssuffixe))\\|(.*?)\\}\\}", converted)
  }
}