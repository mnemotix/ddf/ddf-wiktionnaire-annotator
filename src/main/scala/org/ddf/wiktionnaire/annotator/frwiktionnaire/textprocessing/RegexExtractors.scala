package org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing

import scala.util.matching.Regex

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object RegexExtractors {

  //def extractSKOSDefinition(content: String) = "(?m)^# (.*)$((\\n#\\*.*$))*".r.findAllMatchIn(content)

  def extractSKOSDefinition(content: String) = {
    val rm = content.replaceAll("<br\\n", "<br")
      .replaceAll("\\n\\|", "\\|")
      .replaceAll("\\|\\s", "\\|")
      .replaceAll("\\n\\}\\}", "}}")
      .replaceAll("\\#\\*\\:", "#*")
    "(?m)^(?!#*\\*)#(.*)$((\\n#*\\*.*$)*)".r.findAllMatchIn(rm)
  }

  def extractSubSKOSDefinition(content: String) = "(?m)^#{2,} (.*)$((\\n#{2,}\\*.*$)*)".r.findAllMatchIn(content)

  def extractCitation(citations: String) = ("(?m)^#\\*.*$").r.findAllMatchIn(citations)

  def extractSource(citation: String) = ("(?m)\\{\\{source\\|.*$").r.findAllMatchIn(citation)

  def extractSourceInExample(citation: String) = ("(?m)\\|source\\=\\{\\{.*$").r.findAllMatchIn(citation)

  def extractphonetique(senseText: String) = "(\\{\\{pron\\|)(.*?)(\\|fr\\}\\})".r.findAllMatchIn(senseText)

  def extractRelationWords(content: String) = ("(?m)^\\*\\s\\[\\[(.*?)\\]\\]").r.findAllMatchIn(content)

  def extractSectionTitle(section: String): String = {
    val reg = "={3,6}\\s\\{\\{S\\|.*\\}\\}\\s={3,6}".r.findFirstMatchIn(section)
    if (reg.isDefined) reg.get.toString() else "No title"
  }

  def extractSectionContent(section: String): String = section.replaceAll("={3,6}\\s\\{\\{S\\|.*\\}\\}\\s={3,6}", " ").trim
  // "(?m)((^: \\(''.*?''\\)|(^: ''\\(.*?\\)''))(.*)$)"
  // "(?m)((^: \\(.*?\\)|(^: ''\\(.*?\\)'')|(^: \\{\\{.*?\\}\\}))(.*)$)"
  //"(?m)((^: \\(.*?\\)|(^: ''\\(.*?\\)''))(.*)$)"
  def extractEtymologyByPOS(etymology: String): Seq[Regex.Match] = "(?m)(((^: \\(.*?\\))|(^: ''\\(.*?\\)''))(.*)$)".r.findAllMatchIn(etymology).toSeq

  def extractLexique(sentence: String) = "\\{\\{(lexique|info lex\\|)(.*?)}}".r.findAllMatchIn(sentence).toSeq

  def extractInflectionOf(sentence: String) = "(.*?)\\[\\[(.*?)\\]\\].*?".r.findAllMatchIn(sentence).toSeq
  //# ''Féminin pluriel de'' [[jaloux#fr|jaloux]].

  def extractLexicalInfo(wikitext: String) = {
    "(?m)^'''.*$".r.findFirstIn(wikitext)
  }

  def extractCategories(wikitext: String)={
    "\\[\\[Catégorie\\:.*?\\]\\]".r.findAllMatchIn(wikitext).toSeq
  }
}