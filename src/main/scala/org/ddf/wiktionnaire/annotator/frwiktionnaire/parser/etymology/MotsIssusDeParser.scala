package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class MotsIssusDeParser(codesLang: Map[String, String]) {
// {{subst:mots issus de|lang}}

  def parser(sentence: String): String = {
    if (sentence.contains("{{subst:mots issus de|")) {
      val replaced = motsIssusDeParserhelper(sentence)
      parser(replaced)
    }
    else sentence
  }

  def motsIssusDeParserhelper(sentence: String): String = {
    val motsIssusDe = ("(\\{\\{subst\\:mots\\sissus\\sde\\|)(.*)?\\}\\}").r.findFirstIn(sentence).get.replaceAll("\\{\\{", "").
      replaceAll("\\}\\}", "")
    val splitted = motsIssusDe.split("\\|")
    val lang = codesLang.get(splitted(1))
    if (lang.isDefined) {
      sentence.replaceFirst("(\\{\\{subst\\:mots\\sissus\\sde\\|)(.*)?\\}\\}", s"mots issus de ${lang.get}")
    }
    else sentence.replaceFirst("(\\{\\{subst\\:mots\\sissus\\sde\\|)(.*)?\\}\\}", s"mots issus de inconnus")
  }
}
