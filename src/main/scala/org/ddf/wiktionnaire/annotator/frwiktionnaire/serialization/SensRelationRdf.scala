/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization

import com.mnemotix.synaptix.cache.RocksDBStore
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF
import org.ddf.wiktionnaire.annotator.model.ddf.Entry
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer

import scala.concurrent.ExecutionContext

object SensRelationRdf {

  def serialize(model: ModelBuilder, entry: Entry, rocksDB: RocksDBStore)(implicit lexicalQualificationRecog: VocsRecognizer, ec: ExecutionContext): Unit = {

    entry.lentries.toList.flatten.foreach { lentry =>
      lentry.sensRelation.toList.flatten.foreach { sensRelations =>
        val vocabs = lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", sensRelations.notationSkos)
        sensRelations.relationWord.foreach { word =>

          val futget = rocksDB.get(word)
          val lexicalEntryIRI = createIRI(lentry.uri.toString)

          vocabs.foreach { vocab =>
            futget.toList.foreach { f =>
              val uriRelationsWith = new String(f)
              val lexicogEntryIRI = createIRI(entry.uri.toString)
              val uriRelation = WiktionnaireUriFactory.ddfSemanticRelation(lexicogEntryIRI.toString, lexicalEntryIRI.toString, vocab.concept)


              model.subject(lexicogEntryIRI)
                .add(createIRI("http://www.w3.org/ns/lemon/lexicog#describes"), lexicalEntryIRI)
                .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"))
                .add("ddf:hasLexicographicResource", createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))

              model.subject(lexicalEntryIRI)
                .add("ddf:lexicalEntryHasSemanticRelationWith", createIRI(uriRelation))

              model.subject(uriRelation)
                .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"))
                .add(vocab.property, createIRI(vocab.concept))
                .add("ddf:semanticRelationOfEntry", createIRI(uriRelationsWith))
            }
          }
        }
      }
    }
  }
}