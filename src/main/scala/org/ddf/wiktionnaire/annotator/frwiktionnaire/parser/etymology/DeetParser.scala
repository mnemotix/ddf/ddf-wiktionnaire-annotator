package org.ddf.wiktionnaire.annotator.frwiktionnaire.parser.etymology

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo https://fr.wiktionary.org/wiki/Mod%C3%A8le:deet ->  14,712

class DeetParser {

  def convert(sentence: String): String = {
    if ("\\{\\{deet\\|(.*?)\\}\\}".r.findFirstIn(sentence).isDefined) {
      convertDeetHelper(sentence)
    }
    else sentence
  }

  def convertDeetHelper(sentence:String): String = {
    val deet = "\\{\\{deet\\|(.*?)\\}\\}".r.findFirstIn(sentence)
    if (deet.isDefined) {
      val de = deet.get.replace("{{deet|", "De|").replaceAll("lang\\=(.*?)(\\||\\}\\})","")
        .replaceAll("issu\\=(.*?)(\\||\\}\\})","").replace("}}", "")
      val parserSuffixeRemove = parserSuffixe(de)
      val parserPrefixeRemove = parserPrefixe(parserSuffixeRemove)
      val converted = printout(parserPrefixeRemove)
      convertDeetHelper(sentence.replaceFirst("\\{\\{deet\\|(.*?)\\}\\}", converted))
    }
    else sentence
  }

  def parserSuffixe(sentence: String): String = {
    if (sentence.contains("|-")) {
      val convertedSuffixe = suffixeHelper(sentence)
      parserSuffixe(convertedSuffixe)
    }
    else sentence
  }

  def suffixeHelper(sentence: String): String = {
    sentence.replaceFirst("(?<=\\|)-", "avec le suffixe -")
  }

  def parserPrefixe(sentence: String): String = {
    if (sentence.contains("-|")) {
      val convertedPrefixe = prefixeHelper(sentence)
      parserPrefixe(convertedPrefixe)
    }
    else sentence
  }

  def prefixeHelper(sentence: String): String = sentence.replaceAll("-(?=\\|)", " avec le préfixe")

  def prefixFix(prefix: Array[String]): String = {
    if (prefix.size > 0) {
      val regexM =  ("(.*?)\\s(avec le préfixe)").r.findFirstMatchIn(prefix.last).get
      s"${regexM.group(2)} '''${regexM.group(1)}-'''"
    }
    else ""
  }

  def suffixFix(suffix: Array[String]): String = {
    if (suffix.size > 0) {
      val regexM =  ("(avec le suffixe)\\s(.*)").r.findFirstMatchIn(suffix.last).get
      s"${regexM.group(1)} '''${regexM.group(2)}'''"
    }
    else ""
  }

  def printout(sentence: String): String = {
    val splitted = sentence.split("\\|").filterNot(_.isEmpty)
    val firstPart = splitted(0)
    val secondPart = splitted.filter(part => part != "De").filterNot(part => part.contains("avec le suffixe")).filterNot(part => part.contains("avec le préfixe"))
    val suffixe = suffixFix(splitted.filter(part => part.contains("avec le suffixe")))
    val prefixe = prefixFix(splitted.filter(part => part.contains("avec le préfixe")))

    val middlePart =  secondPart.zipWithIndex.map { case sp =>
      if (sp._2 == secondPart.size - 1 && secondPart.size > 1) s"et '''${sp._1.trim}'''"
      else s"'''${sp._1}'''"
    }.mkString(" ")
    s"$firstPart $middlePart $prefixe $suffixe".trim.replaceAll(" +", " ").replaceAll(" ,", ",")
  }
}