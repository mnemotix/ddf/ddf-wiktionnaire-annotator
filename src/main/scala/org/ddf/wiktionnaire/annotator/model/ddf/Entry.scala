/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.model.ddf

import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.frwiktionnaire.serialization.{InflexionRdf, LexicalEntryRdf, LexicalSensRdf, SensRelationRdf, SubSenseRdf}
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.{createIRI, nameSpace}
import org.ddf.wiktionnaire.annotator.services.cache.MemoryCacheSystem
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.net.URI
import scala.collection.mutable
import scala.concurrent.ExecutionContext

case class Entry(uri: URI, form: String, text: String, lentries: Option[Seq[LexicalEntry]])

object Entry extends LazyLogging {
  implicit class toRDF(entry: Entry) {
    def toDDFModel()(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]): Model = {
      val builder = new ModelBuilder()
      val model = nameSpace(builder)
      val lexicogEntryUri = createIRI(entry.uri.toString)
      model.subject(lexicogEntryUri)
        .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"))
        .add("ddf:hasLexicographicResource", createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))

      entry.lentries.toList.flatten.filterNot(_.isInflection).foreach { lentry =>
        SubSenseRdf.serialize(model, lentry)
        LexicalEntryRdf.serialize(model, lentry, lexicogEntryUri, entry.text)
        LexicalSensRdf.serialize(model, lentry, lexicogEntryUri)
      }
      model.build()
    }

    def toInflexion()(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String]) = {
      val builder = new ModelBuilder()
      val model = nameSpace(builder)

      entry.lentries.toList.flatten.filter(_.isInflection).foreach { lentry =>

        val lexicogEntryUri = createIRI(entry.uri.toString)
        model.subject(lexicogEntryUri)
          .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"))
          .add("ddf:hasLexicographicResource", createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))
          .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))

        lentry.sense.toList.flatten.foreach { sense =>
         InflexionRdf.serialize(model, sense, lentry, entry.text)
        }
      }
      model.build()
    }

    def toSenseRelation()(implicit lexicalQualificationRecog: VocsRecognizer, posType: mutable.Map[String, String], ec: ExecutionContext) = {
      lazy val lexicogEntryCache = MemoryCacheSystem.lexicogEntryTmpDB
      val builder = new ModelBuilder()
      val model = nameSpace(builder)
      val lexicogEntryUri = createIRI(entry.uri.toString)
      model.subject(lexicogEntryUri)
        .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"))
        .add("ddf:hasLexicographicResource", createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"))

      SensRelationRdf.serialize(model, entry, lexicogEntryCache)
      model.build()
    }
  }
}

