/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.model.ddf


import java.net.URI

case class LexicalEntry(uri: URI,
                        pageTitle: String,
                        lexicalEntryType: String,
                        sense: Option[Seq[Sense]],
                        subSense: Option[Seq[SubSense]],
                        canonicalForm: Option[CanonicalForm],
                        etymology: Option[String],
                        otherForms: Option[Seq[String]],
                        sensRelation: Option[Seq[SenseRelation]],
                        wordSectionContent: Option[String],
                        isInflection: Boolean)
