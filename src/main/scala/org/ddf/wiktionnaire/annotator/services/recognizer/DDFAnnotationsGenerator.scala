package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
object DDFAnnotationsGenerator {

/*

  def generator(entry: Entry)(implicit lexicalQualificationRecog: VocsRecognizer, POSType: mutable.Map[String, String]): Seq[LiftyAnnotation] = {
    val lexicalEntry: Option[Seq[LiftyAnnotation]] = entry.lentries.map((lentries: Seq[LexicalEntry]) =>
      lentries.filterNot(lentry => lentry.isInflection).filter(_.canonicalForm.isDefined) flatMap { lentry: LexicalEntry =>
        val lexicogEntry: Seq[LiftyAnnotation] = lexicogEntryAnnotations(entry)
        val lexicalEntry: LiftyAnnotation = LexicalEntryAnnotationsRecogs.lexicalEntryAnnotations(entry.uri,lentry.uri)
        val form: Option[Seq[LiftyAnnotation]] = if (lentry.canonicalForm.isDefined) Some(LexicalEntryAnnotationsRecogs.formAnnotations(lentry.uri, lentry.canonicalForm.get.label)) else None
        val otherForm: Option[Seq[LiftyAnnotation]] = if (lentry.otherForms.isDefined) Some(LexicalEntryAnnotationsRecogs.otherFormAnnotations(lentry.otherForms.get, lentry.uri)) else None
        val pronounciation: Option[Seq[LiftyAnnotation]] = if (lentry.canonicalForm.isDefined) {
          if (lentry.canonicalForm.get.phoneticRep.isDefined) {
              Some(LexicalEntryAnnotationsRecogs.pronounciationAnnotations(lentry.canonicalForm.get.label, lentry.canonicalForm.get.phoneticRep.get))
            }
            else None
          }
          else None
          val etymology: Option[Seq[LiftyAnnotation]] = LexicalEntryAnnotationsRecogs.etymologyAnnotations(lentry.uri, lentry.etymology)

          val classForm = LexicalEntryAnnotationsRecogs.extractClassForm(entry.form).toString()
          val termElement = LexicalEntryAnnotationsRecogs.termElement(lentry.uri, lentry.lexicalEntryType)
          val annotationLexicalInfo = if (classForm == "http://www.w3.org/ns/lemon/ontolex#Word" && !termElement.isDefined) {
            val classFormAnnotation = LexicalEntryAnnotationsRecogs.classAnnotation(lentry.uri, lentry.canonicalForm.get.label)
            val genderNumberTransitivity: Option[Seq[LiftyAnnotation]] = if (lentry.wordSectionContent.isDefined) Some(LexicalEntryAnnotationsRecogs.genderNumberTransitivityAnnotations(lentry.uri, lentry.wordSectionContent.get)) else None
            val pos: Option[Seq[LiftyAnnotation]] = LexicalEntryAnnotationsRecogs.posAnnotations(lentry.uri, lentry.lexicalEntryType)
            val wordType: Option[Seq[LiftyAnnotation]] = if (pos.isDefined) Some(LexicalEntryAnnotationsRecogs.wordTypeAnnotations(lentry.uri, pos.get)) else None
            genderNumberTransitivity.toSeq.flatten ++ pos.toSeq.flatten ++ wordType.toSeq.flatten ++ Seq(classFormAnnotation)
          }
          else if (termElement.isDefined) {
            val termElementAnnotations = LexicalEntryAnnotationsRecogs.termElementAnnotations(lentry.uri)
            termElement.toSeq.flatten ++ Seq(termElementAnnotations)
          }
          else {
            val classFormAnnotation = LexicalEntryAnnotationsRecogs.classAnnotation(lentry.uri, lentry.canonicalForm.get.label)
            LexicalEntryAnnotationsRecogs.multiWordType(lentry.uri, lentry.lexicalEntryType).toSeq.flatten ++ Seq(classFormAnnotation)
          }

          val lexicalSens: Option[Seq[LiftyAnnotation]] = lexicalSensGenerator(entry, lentry)
          val subLexicalSense: Option[Seq[LiftyAnnotation]] = lexicalSubSensGenerator(entry, lentry)
          val annotationsGenerate = Seq(lexicalEntry) ++ form.toSeq.flatten ++ otherForm.toSeq.flatten ++ otherForm.toSeq.flatten ++ pronounciation.toSeq.flatten ++ etymology.toSeq.flatten ++ lexicalSens.toSeq.flatten ++ annotationLexicalInfo
          annotationsGenerate ++ lexicogEntry
      }
    )
    lexicalEntry.toSeq.flatten
  }

  def generatorFlexion(entry: Entry)(implicit lexicalQualificationRecog: VocsRecognizer, POSType: mutable.Map[String, String]): Seq[LiftyAnnotation] = {
    val lexicalEntry: Option[Seq[LiftyAnnotation]] = entry.lentries.map((lentries: Seq[LexicalEntry]) =>
      lentries.filter(lentry => lentry.isInflection) flatMap { lentry: LexicalEntry =>
        val lexicogEntry: Seq[LiftyAnnotation] = lexicogEntryAnnotations(entry)
        val lexicalEntry: LiftyAnnotation = LexicalEntryAnnotationsRecogs.lexicalEntryAnnotations(entry.uri, lentry.uri)
        val form: Option[Seq[LiftyAnnotation]] = if (lentry.canonicalForm.isDefined) Some(LexicalEntryAnnotationsRecogs.formAnnotations(lentry.uri, lentry.canonicalForm.get.label)) else None
        val otherForm: Option[Seq[LiftyAnnotation]] = if (lentry.otherForms.isDefined) Some(LexicalEntryAnnotationsRecogs.otherFormAnnotations(lentry.otherForms.get, lentry.uri)) else None
        val pronounciation: Option[Seq[LiftyAnnotation]] = if (lentry.canonicalForm.isDefined) {
          if (lentry.canonicalForm.get.phoneticRep.isDefined) {
            Some(LexicalEntryAnnotationsRecogs.pronounciationAnnotations(lentry.canonicalForm.get.label, lentry.canonicalForm.get.phoneticRep.get))
          }
          else None
        }
        else None
        val etymology: Option[Seq[LiftyAnnotation]] = LexicalEntryAnnotationsRecogs.etymologyAnnotations(lentry.uri, lentry.etymology)

        val classForm = LexicalEntryAnnotationsRecogs.extractClassForm(entry.form).toString()
        val termElement = LexicalEntryAnnotationsRecogs.termElement(lentry.uri, entry.form)
        val annotationLexicalInfo = if (classForm == "http://www.w3.org/ns/lemon/ontolex#Word" && !termElement.isDefined) {
          val pos: Option[Seq[LiftyAnnotation]] = LexicalEntryAnnotationsRecogs.posAnnotations(lentry.uri, lentry.lexicalEntryType)
          val wordType: Option[Seq[LiftyAnnotation]] = if (pos.isDefined) Some(LexicalEntryAnnotationsRecogs.wordTypeAnnotations(lentry.uri, pos.get)) else None
          pos.toSeq.flatten ++ wordType.toSeq.flatten
        }
        else if (termElement.isDefined) {
          val termElementAnnotations = LexicalEntryAnnotationsRecogs.termElementAnnotations(lentry.uri)
          termElement.toSeq.flatten ++ Seq(termElementAnnotations)
        }
        else LexicalEntryAnnotationsRecogs.multiWordType(lentry.uri, lentry.lexicalEntryType).toSeq.flatten


        val flexion: Option[Seq[LiftyAnnotation]] = if (lentry.sense.isDefined) {
          val res = lentry.sense.get.map { sense =>
            InflectionAnnotationsRecogs.inflexionGenerator(sense, lentry, entry.text).toSeq.flatten
          }
          Some(res.flatten)
        }
        else None

        val annotationsGenerate = Seq(lexicalEntry) ++ form.toSeq.flatten ++ otherForm.toSeq.flatten ++ pronounciation.toSeq.flatten ++ etymology.toSeq.flatten ++ annotationLexicalInfo  ++ flexion.toSeq.flatten


        //val annotationsGenerate: Option[Seq[LiftyAnnotation]] = None
        annotationsGenerate ++ lexicogEntry
      }
    )
      lexicalEntry.toSeq.flatten
  }

  def lexicalSensGenerator(entry: Entry, lentry: LexicalEntry)(implicit lexicalQualificationRecog: VocsRecognizer, POSType: mutable.Map[String, String]) = {
    if (lentry.sense.isDefined) {
      val sensRelation: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.sensRelationGenerate(lentry.sense.get.filter(_.definition.isDefined), entry.form, entry.uri, lentry.uri.toString)
      val senses: Seq[Seq[LiftyAnnotation]] = lentry.sense.get.map { sense =>
        val aSensAnnotation: Option[Seq[LiftyAnnotation]] = senseGenerator(sense, lentry)
        aSensAnnotation.toSeq.flatten
      }
      val lexicalSens = sensRelation ++ senses.flatten
      Some(lexicalSens)
    }
    else None
  }

  // dirty code
  def lexicalSubSensGenerator(entry: Entry, lentry: LexicalEntry)(implicit lexicalQualificationRecog: VocsRecognizer, POSType: mutable.Map[String, String]) = {
    if (lentry.subSense.isDefined) {
      val senses: Seq[Seq[LiftyAnnotation]] = lentry.subSense.get.map { subSense =>
        val sense = Sense(subSense.definition, subSense.usageExamples)
        val aSensAnnotation: Option[Seq[LiftyAnnotation]] = senseGenerator(sense, lentry)
        aSensAnnotation.toSeq.flatten
      }
      val lexicalSens =  senses.flatten
      Some(lexicalSens)
    }
    else None
  }

  def senseGenerator(sense: Sense, lentry: LexicalEntry)(implicit lexicalQualificationRecog: VocsRecognizer, POSType: mutable.Map[String, String]) = {
    if (sense.definition.isDefined) {
      val lexicalSensURI: URI = LexicalSensAnnotationsRecogs.lexicalSensURIGenerate(sense.definition.get, sense.usageExamples, lentry.uri.toString)
      val lexicalSens: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.lexicalSensGenerate(lentry.uri, lexicalSensURI)
      val skosDefinition: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.skosDefinitionGenerate(sense.definition.get, lexicalSensURI)
      val vocsLexical: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.vocsLexicalGenerate(lexicalSensURI, sense.definition.get)
      val usageExempleCitation: Option[Seq[LiftyAnnotation]] = usageExempleGenerator(sense.usageExamples, lexicalSensURI)
      val res = usageExempleCitation.toSeq.flatten ++ lexicalSens ++ skosDefinition ++ vocsLexical
      Some(res)
    }
    else None
  }

  def usageExempleGenerator(usageExamples: Option[Seq[UsageExample]], lexicalSensURI: URI) = {
    if (usageExamples.isDefined) {
      val res = usageExamples.get.map { usageExemple =>
        val exempleURI: URI = LexicalSensAnnotationsRecogs.exempleURIGenerate(usageExemple.example, usageExemple.bibliographicalCitation)
        val usageExample = LexicalSensAnnotationsRecogs.usageExampleGenerate(usageExemple.example, lexicalSensURI, exempleURI)
        val source = if (usageExemple.bibliographicalCitation.isDefined) Some(LexicalSensAnnotationsRecogs.bibliographicalCitationGenerate(exempleURI, usageExemple.bibliographicalCitation.get))  else None
        val concatenate = source.toSeq ++ usageExample
        concatenate
      }
      Some(res.flatten.toSeq)
    }
    else None
  }

  def lexicogEntryAnnotations(entry: Entry) = {
    Seq(LiftyAnnotation(entry.uri, "a", createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"), 1.0),
      LiftyAnnotation(entry.uri, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"), 1.0)
    )
  }

 */
}