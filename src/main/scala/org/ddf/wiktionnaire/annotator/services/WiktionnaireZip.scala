/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, IOResult, RestartSettings}
import akka.stream.alpakka.file.ArchiveMetadata
import akka.stream.alpakka.file.scaladsl.Archive
import akka.stream.scaladsl.{FileIO, Source}
import com.mnemotix.analytix.commons.utils.FileUtils
import com.mnemotix.synaptix.core.utils.{RandomNameGenerator, StringUtils}

import java.io.File
import java.nio.file.Paths
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

class WiktionnaireZip(dir: String)(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) {

  def zipFiles(): Iterator[IOResult] = {
    val zipped = zip()
    val fut = Future.sequence(zipped)
    Await.result(fut, 10.minutes)
  }

  private def zip() = {
    val path = StringUtils.removeTrailingSlashes(dir)
    val directory = new File(s"${path}/zip")
    if (!directory.exists()) directory.mkdir()

    val files = FileUtils.getListOfFiles(new File(s"${path}"), List("trig"))
    val toZip = files.map(_.getName)
    toZip.grouped(4).map { list =>
      val files = list.map(file => new File(s"${path}/$file"))
      val name = RandomNameGenerator.haiku
      archive(files, s"${path}/zip/$name.zip")
    }
  }

  private def archive(files: Seq[File], zipFile: String): Future[IOResult] = {
    val streams = files.map { file =>
      (ArchiveMetadata(file.getName), FileIO.fromPath(file.toPath))
    }
    Source(streams.toList).via(Archive.zip())
      .runWith(FileIO.toPath(Paths.get(zipFile)))
  }

  val settings = RestartSettings(
    minBackoff = 3.seconds,
    maxBackoff = 30.seconds,
    randomFactor = 0.2
  ).withMaxRestarts(20, 5.minutes)
}