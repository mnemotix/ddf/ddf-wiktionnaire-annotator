package org.ddf.wiktionnaire.annotator.services.recognizer



import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// rocksDB : word -> uri

@deprecated
object DDFSensRelationsAnnotationsGenerator {
  /*
  def generator(entry: Entry, rocksDB: RocksDBStore)(implicit lexicalQualificationRecog: VocsRecognizer, ec: ExecutionContext): Option[Seq[Seq[LiftyAnnotation]]] = {
    if (entry.lentries.isDefined) {
      Some(entry.lentries.get.flatMap { lentry =>
        if (lentry.sensRelation.isDefined) {
          Some(lentry.sensRelation.get.flatMap { sensRelations =>
            val vocabs = lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", sensRelations.notationSkos)
            sensRelations.relationWord.flatMap { word =>
                generateSensRelationAnnotations(rocksDB, word, entry.uri, lentry.uri, vocabs.last).toSeq.flatten
            }
          })
        }
        else None
      })
    }
    else None
  }

  def generateSensRelationAnnotations(rocksDBStore: RocksDBStore, word: String, lexicogEntryURI: URI, lexicalEntryURI: URI, vocab: VocabularyEntity)(implicit ec: ExecutionContext): Option[Seq[LiftyAnnotation]] = {
    val uri = filterWords(rocksDBStore, word)
    if (uri.isDefined) {
      val uriRelationsWith = uri.get.utf8String
      Some(LexicalEntryAnnotationsRecogs.sensRelationAnnotations(lexicogEntryURI, lexicalEntryURI,vocab, URI.create(uriRelationsWith)))
    }
    else None
  }

  def filterWords(rocksDBStore: RocksDBStore, word: String)(implicit ec: ExecutionContext): Option[ByteString] = {
    val futget = rocksDBStore.get(word.getBytes)
    Await.result(futget, Duration.Inf)
  }


   */
}