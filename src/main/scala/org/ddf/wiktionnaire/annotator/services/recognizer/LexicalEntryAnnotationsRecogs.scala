package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model.VocabularyEntity
import org.ddf.wiktionnaire.annotator.frwiktionnaire.parser._

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.{RegexExtractors, SplitProcessing}

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
object LexicalEntryAnnotationsRecogs {

/*

  def lexicalEntryAnnotations(lexicogEntryUri: URI, lexicalEntryURI: URI) = {
    LiftyAnnotation(lexicogEntryUri, "http://www.w3.org/ns/lemon/lexicog#describes", createIRI(lexicalEntryURI.toString), 1.0)
  }

  def formAnnotations(lexicalEntryURI:URI, pageTitle: String) = {
    Seq(LiftyAnnotation(lexicalEntryURI,
      "http://www.w3.org/ns/lemon/ontolex#canonicalForm",
      createIRI(WiktionnaireUriFactory.ontolexForm(pageTitle)), 1.0),
      LiftyAnnotation(new URI(WiktionnaireUriFactory.ontolexForm(pageTitle)),
        "a",
        createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"), 1.0),
      LiftyAnnotation(new URI(WiktionnaireUriFactory.ontolexForm(pageTitle)),
        "http://www.w3.org/ns/lemon/ontolex#writtenRep", createLiteral(s"${StringCleaner.replaceApostrophe(pageTitle)}", "fr"), 1.0)
    )
  }

  def extractClassForm(pageTitle: String) = {
    val split1 = pageTitle.trim.split(" ").size
    val split2 = pageTitle.trim.split("-").size
    val split3 = pageTitle.trim.split("'").size
    if (split1 > 1 | split2 > 1 | split3 > 1) createIRI("http://www.w3.org/ns/lemon/ontolex#MultiWordExpression") else createIRI("http://www.w3.org/ns/lemon/ontolex#Word")
  }

def classAnnotation(lexicalEntryURI:URI, pageTitle: String) = {
  LiftyAnnotation(lexicalEntryURI,
    "a",
    extractClassForm(pageTitle), 1.0)
}

  def otherFormAnnotations(words: Seq[String], lexicalEntryURI:URI): Seq[LiftyAnnotation] = {
    words.flatMap { word =>
      val form = SplitProcessing.splitOtherForms(word)
      val otherFormURI = WiktionnaireUriFactory.ontolexForm(form)
      Seq(LiftyAnnotation(lexicalEntryURI,  "http://www.w3.org/ns/lemon/ontolex#otherForm", createIRI(otherFormURI), 1.0),
        LiftyAnnotation(new URI(otherFormURI), "a", createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"), 1.0),
        LiftyAnnotation(new URI(otherFormURI), "http://www.w3.org/ns/lemon/ontolex#writtenRep", createLiteral(s"${StringCleaner.replaceApostrophe(form)}",  "fr"),  1.0))
    }
  }

  def pronounciationAnnotations(pageTitle: String, phonetiques: Seq[String]): Seq[LiftyAnnotation] = {
    val formURI = new URI(WiktionnaireUriFactory.ontolexForm(pageTitle))
    phonetiques.map { phonetique =>
      LiftyAnnotation(formURI, "http://www.w3.org/ns/lemon/ontolex#phoneticRep", createLiteral(phonetique,  PrimitiveDatatypesIRI.STRINGIRIDATATYPE) , 1.0)
    }
  }

  def etymologyAnnotations(lexicalEntryURI: URI, etymologyText:Option[String]) = {
    if (etymologyText.isDefined) {
      val uriEtymology: URI = URI.create(WiktionnaireUriFactory.siocPost(etymologyText.get))
      Some(Seq(LiftyAnnotation(lexicalEntryURI, s"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasItemAboutEtymology", createIRI(uriEtymology.toString) , 1.0),
        LiftyAnnotation(uriEtymology, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#OnlineContribution"), 1.0),
        LiftyAnnotation(uriEtymology, "http://rdfs.org/sioc/ns#content",  createLiteral(StringCleaner.removeHTMLTags(etymologyModelParser.convert(StringCleaner.replaceApostrophe(etymologyText.get))), PrimitiveDatatypesIRI.STRINGIRIDATATYPE) , 1.0),
        LiftyAnnotation(uriEtymology, "http://data.dictionnairedesfrancophones.org/ontology/ddf#aboutEtymology", createIRI(lexicalEntryURI.toString), 1.0)
      ))
    }
    else None
  }

  def genderNumberTransitivityAnnotations(lexicalEntryURI: URI, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    val text = RegexExtractors.extractLexicalInfo(wikitext)

    val gender: Option[Seq[LiftyAnnotation]] = if (text.isDefined) lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, text.get, "http://data.dictionnairedesfrancophones.org/authority/gender") else None
    val number: Option[Seq[LiftyAnnotation]] = if (text.isDefined) lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, text.get, "http://data.dictionnairedesfrancophones.org/authority/number") else None
    val transitivity: Option[Seq[LiftyAnnotation]] = if (text.isDefined) lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, text.get, "http://data.dictionnairedesfrancophones.org/authority/verb-frame") else None
    (gender ++ number ++ transitivity).flatten.toSeq ++ inflectablePOS(lexicalEntryURI, gender, number).toSeq.flatten ++ verb(lexicalEntryURI, transitivity).toSeq.flatten
  }

  def inflectablePOS(lexicalEntryURI: URI, gender: Option[Seq[LiftyAnnotation]], number: Option[Seq[LiftyAnnotation]]): Option[Seq[LiftyAnnotation]] = {
    if (gender.isDefined) {
      Some(Seq(LiftyAnnotation(lexicalEntryURI, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"), 1.0)))
    }
    else if (number.isDefined) {
      Some(Seq(LiftyAnnotation(lexicalEntryURI, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"), 1.0)))
    }
    else None
  }

  def verb(lexicalEntryURI: URI, transitivity: Option[Seq[LiftyAnnotation]]): Option[Seq[LiftyAnnotation]] = {
    if (transitivity.isDefined) {
      Some(Seq(LiftyAnnotation(lexicalEntryURI, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb"), 1.0)))
    }
    else None
  }



  def termElement(lexicalEntryURI: URI, sectionTitle: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, sectionTitle,"http://data.dictionnairedesfrancophones.org/authority/term-element")
  }

  def termElementAnnotations(lexicalEntryUri: URI) = {
    LiftyAnnotation(lexicalEntryUri, "a", createIRI("http://www.w3.org/ns/lemon/ontolex#Afflix"), 1.0)

  }

  def multiWordType(lexicalEntryURI: URI, sectionTitle: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, sectionTitle,"http://data.dictionnairedesfrancophones.org/authority/multiword-type")
  }

  def posAnnotations(lexicalEntryURI: URI, sectionTitle: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, sectionTitle,"http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type" )
  }

  def sensRelationAnnotations(lexicogEntryUri: URI, lexicalEntryURI: URI, vocab: VocabularyEntity, uriRelationsWith: URI) = {
    val lexicalEntryAnnot =  Seq(lexicalEntryAnnotations(lexicogEntryUri, lexicalEntryURI))
    val lexicogEntryAnnotations = Seq(LiftyAnnotation(lexicogEntryUri, "a", createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"), 1.0),
      LiftyAnnotation(lexicogEntryUri, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",createIRI("http://data.dictionnairedesfrancophones.org/resource/wiktionnaire"), 1.0)
    )

      val uriRelation = WiktionnaireUriFactory.ddfSemanticRelation(lexicogEntryUri.toString, lexicalEntryURI.toString, vocab.concept)

    lexicalEntryAnnot ++ lexicogEntryAnnotations ++ Seq(LiftyAnnotation(lexicalEntryURI, "http://data.dictionnairedesfrancophones.org/ontology/ddf#lexicalEntryHasSemanticRelationWith",
        createIRI(uriRelation), 1.0),
        LiftyAnnotation(new URI(uriRelation), "a", createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"), 1.0),
          LiftyAnnotation(new URI(uriRelation), vocab.property, createIRI(vocab.concept), 1.0),
        LiftyAnnotation(new URI(uriRelation), "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfEntry", createIRI(uriRelationsWith.toString), 1.0)
      )
  }

  def wordTypeAnnotations(lexicalEntryURI: URI, partOfSpeech: Seq[LiftyAnnotation])(implicit POSType: mutable.Map[String, String]): Seq[LiftyAnnotation] = {
    partOfSpeech.map { pos =>
      val postType = POSType.get(pos.obj.stringValue())
      if (postType.get == "variable") {
        LiftyAnnotation(lexicalEntryURI, "a",  createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"), 1.0)
      }
      else if (postType.get == "verbe") {
        LiftyAnnotation(lexicalEntryURI, "a",  createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb"), 1.0)
      }
      else {
        LiftyAnnotation(lexicalEntryURI, "a",  createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS"), 1.0)
      }
      //variable createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS")
      //verbe createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb")
      //invariable createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS")
    }
  }

 */
}