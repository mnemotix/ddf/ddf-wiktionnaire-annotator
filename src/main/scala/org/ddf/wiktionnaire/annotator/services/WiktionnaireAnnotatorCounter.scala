/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape, IOResult}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.synaptix.http.FileDownloader
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

@deprecated
class WiktionnaireAnnotatorCounter(filePath: String) extends LazyLogging  {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wiktionnaireAnnotationRelationSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val wiktionnaireRdfReader = new WiktionnaireRdfReader
  wiktionnaireRdfReader.init
  lazy val vocs = wiktionnaireRdfReader.vocabulariesBlocked
  implicit val map = wiktionnaireRdfReader.posTypeBlocked
  implicit val lexicalQualtification = new VocsRecognizer(vocs)
  implicit val htmlCrawler = new WiktionnaireHTMLCrawler()

  lazy val extractor = new WiktionnaireWikidataExtractor(filePath)
  lazy val ddfModelsFactory = new DDFModelsSetter()
  lazy val recognizer = new WiktionnaireRecognizer()
  lazy val cacheManager = new WiktionnaireDBCacheManager()
  lazy val wiktionnaireFileOutput = new WiktionnaireFileOutput()
  lazy val fileDownloader = new FileDownloader


  def init= {logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")}

  @deprecated
  def countSimple: (Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]], Future[immutable.Seq[Seq[String]]]) = {

    /*
    val lexicogEntrySink = Sink.seq[Seq[String]]
    val lexicalEntrySink = Sink.seq[Seq[String]]
    val lexicalSensSink = Sink.seq[Seq[String]]
    val canonicalFormSink = Sink.seq[Seq[String]]

    val inflectablePOSSink = Sink.seq[Seq[String]]
    val verbSink = Sink.seq[Seq[String]]
    val invariablePOSSink = Sink.seq[Seq[String]]


    RunnableGraph.fromGraph(GraphDSL.create(lexicogEntrySink, lexicalEntrySink, lexicalSensSink, canonicalFormSink, inflectablePOSSink, verbSink, invariablePOSSink)((_,_,_,_,_,_,_)) { implicit builder =>
      (lexicogES, lexicalES, lexicalSS, cononicalFS, inflectablePS, verbS, invariablePS) =>

      import GraphDSL.Implicits._


      val broadcast4 = builder.add(Broadcast[Seq[LiftyAnnotation]](7))
      val filterLexicogEntrySink: Flow[Seq[LiftyAnnotation], Seq[String], NotUsed] = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/lexicog#Entry").map(_.resource.toString))
      val filterLexicalEntrySink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.property == "http://www.w3.org/ns/lemon/ontolex#canonicalForm").map(_.resource.toString))
      val filterLexicalSensSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#LexicalSense").map(_.resource.toString))
      val filterCanonicalFormSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#Form").map(_.resource.toString))
      val filterInflectablePOSSink =  Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS").map(_.resource.toString))
      val filterVerbSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb").map(_.resource.toString))
      val filterInvariableSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS").map(_.resource.toString))

        extractor.extract ~> ddfModelsFactory.setter.filter(entry => entry.lentries.isDefined && entry.lentries.get.size > 0) ~> recognizer.recognize ~> broadcast4 ~> filterLexicogEntrySink ~> lexicogES
                                                                                                                                                         broadcast4 ~>  filterLexicalEntrySink ~> lexicalES
                                                                                                                                                         broadcast4 ~> filterLexicalSensSink ~> lexicalSS
                                                                                                                                                         broadcast4 ~> filterCanonicalFormSink ~> cononicalFS
                                                                                                                                                          broadcast4 ~>  filterInflectablePOSSink ~> inflectablePS
                                                                                                                                                          broadcast4 ~> filterVerbSink ~> verbS
                                                                                                                                                          broadcast4 ~> filterInvariableSink ~> invariablePS

        ClosedShape
    }).run()

     */
    ???
  }

  @deprecated
  def countFlexion  = {
/*
    val lexicogEntrySink = Sink.seq[Seq[String]]
    val lexicalEntrySink = Sink.seq[Seq[String]]
    val lexicalSensSink = Sink.seq[Seq[String]]
    val canonicalFormSink = Sink.seq[Seq[String]]

    val inflectablePOSSink = Sink.seq[Seq[String]]
    val verbSink = Sink.seq[Seq[String]]
    val invariablePOSSink = Sink.seq[Seq[String]]

    RunnableGraph.fromGraph(GraphDSL.create(lexicogEntrySink, lexicalEntrySink, lexicalSensSink, canonicalFormSink, inflectablePOSSink, verbSink, invariablePOSSink)((_,_,_,_,_,_,_)) { implicit builder =>
      (lexicogES, lexicalES, lexicalSS, cononicalFS, inflectablePS, verbS, invariablePS) =>

        import GraphDSL.Implicits._


        val broadcast4 = builder.add(Broadcast[Seq[LiftyAnnotation]](4))
        val filterLexicogEntrySink: Flow[Seq[LiftyAnnotation], Seq[String], NotUsed] = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/lexicog#Entry").map(_.resource.toString))
        val filterLexicalEntrySink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.property == "http://www.w3.org/ns/lemon/ontolex#canonicalForm").map(_.resource.toString))
        val filterLexicalSensSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#LexicalSense").map(_.resource.toString))
        val filterCanonicalFormSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#Form").map(_.resource.toString))
        val filterInflectablePOSSink =  Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS").map(_.resource.toString))
        val filterVerbSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb").map(_.resource.toString))
        val filterInvariableSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS").map(_.resource.toString))

        extractor.extract ~> ddfModelsFactory.setter.filter(entry => entry.lentries.isDefined && entry.lentries.get.size > 0) ~> recognizer.recognizeFlexions ~> broadcast4 ~> filterLexicogEntrySink ~> lexicogES
        broadcast4 ~>  filterLexicalEntrySink ~> lexicalES
        broadcast4 ~> filterLexicalSensSink ~> lexicalSS
        broadcast4 ~> filterCanonicalFormSink ~> cononicalFS
        broadcast4 ~>  filterInflectablePOSSink ~> inflectablePS
        broadcast4 ~> filterVerbSink ~> verbS
        broadcast4 ~> filterInvariableSink ~> invariablePS

        ClosedShape
    }).run()


 */
    ???
  }
}

