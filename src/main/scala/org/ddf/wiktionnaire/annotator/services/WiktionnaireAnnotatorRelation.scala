package org.ddf.wiktionnaire.annotator.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink}
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig
import org.ddf.wiktionnaire.annotator.model.ddf.Entry
import org.ddf.wiktionnaire.annotator.services.cache.MemoryCacheSystem
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer

import java.io.File
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireAnnotatorRelation(filePath: String) {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wiktionnaireAnnotationRelationSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val wiktionnaireRdfReader = new WiktionnaireRdfReader
  wiktionnaireRdfReader.init
  lazy val vocs = wiktionnaireRdfReader.vocabulariesBlocked
  implicit val map = wiktionnaireRdfReader.posTypeBlocked
  implicit val lexicalQualtification = new VocsRecognizer(vocs)
  implicit val htmlCrawler = new WiktionnaireHTMLCrawler()
  lazy val extractor = new WiktionnaireWikidataExtractor(filePath)
  lazy val ddfModelsFactory = new DDFModelsSetter()
  lazy val recognizer = new WiktionnaireRecognizer()
  lazy val cacheManager = new WiktionnaireDBCacheManager()
  lazy val wiktionnaireFileOutput = new WiktionnaireFileOutput()

  lazy val topHeadSink = Sink.ignore
  lazy val lexicogEntryCache = MemoryCacheSystem.lexicogEntryTmpDB

  def init: Boolean = {
    new File(WiktionnaireAnnotatorConfig.wiktionaryDir).mkdir()
    wiktionnaireFileOutput.init
  }

  def relation: Future[Done] = {

    lexicogEntryCache.init
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink){ implicit builder =>
      (sink) =>
        import GraphDSL.Implicits._
        extractor.extract ~> ddfModelsFactory.setter.filter(entry => entry.lentries.isDefined && entry.lentries.get.size > 0) ~> Flow[Entry].map(_.toSenseRelation()).grouped(100000) ~> wiktionnaireFileOutput.bulkDump ~> sink.in
        ClosedShape
    }).run()

  }

  def shutdown: Unit = {
    lexicogEntryCache.shutdown
    system.terminate()
    materializer.shutdown()
  }
}