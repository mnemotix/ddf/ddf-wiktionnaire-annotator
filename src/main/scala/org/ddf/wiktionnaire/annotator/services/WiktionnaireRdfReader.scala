package org.ddf.wiktionnaire.annotator.services

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{Done, RDFClientReadConnection, RDFClientWriteConnection}
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.TextCleaners
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig
import org.ddf.wiktionnaire.annotator.model.VocabularyEntity
import org.eclipse.rdf4j.query.BindingSet

import scala.collection.immutable.ListMap
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireRdfReader(implicit val ec: ExecutionContextExecutor) extends LazyLogging {

  implicit lazy val conn: RDFClientReadConnection = RDFClient.getReadConnection((WiktionnaireAnnotatorConfig.ddfRepoName))
  implicit lazy val writeConn: RDFClientWriteConnection = RDFClient.getWriteConnection((WiktionnaireAnnotatorConfig.ddfRepoName))

  lazy val vocabularySparqlQuery =
    """prefix skos: <http://www.w3.org/2004/02/skos/core#>
      |SELECT ?voc ?notation ?concept ?conceptLabel
      |WHERE {
      | ?voc a skos:ConceptScheme .
      | ?concept skos:inScheme ?voc .
      | ?concept skos:notation ?notation .
      | ?concept skos:prefLabel ?conceptLabel .
      | FILTER (datatype(?notation) = <http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>)
      |}
    """.stripMargin

  lazy val posTypeQuery =
    """PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      |PREFIX dct: <http://purl.org/dc/terms/>
      |SELECT ?concept ?type
      |WHERE {
      | <http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type> a skos:ConceptScheme .
      | ?concept skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type> .
      | ?concept dct:type ?type .
      |}
    """.stripMargin

  lazy val graphesSelectQuery = """SELECT DISTINCT ?g WHERE {
                                  |GRAPH ?g {?s ?p ?o}
                                  |}""".stripMargin

  lazy val deleteGraphQuery: String => String = (x: String) => s"DROP GRAPH <$x>"

  def init: Unit = {
    RDFClient.init()
  }

  def posTypeAsync: Future[mutable.Map[String, String]] = {
    var posType = scala.collection.mutable.Map[String, String]()
    val futureTypeAsync = RDFClient.select(posTypeQuery).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        posType += (bs.getValue("concept").stringValue() -> bs.getValue("type").stringValue())
      }
      posType
    }

    futureTypeAsync onComplete {
      case Success(i) => logger.info(s"The number of Pos Type : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureTypeAsync
  }

  def posTypeBlocked: mutable.Map[String, String] = {
    val futurePos = posTypeAsync
    Await.result(futurePos, Duration.Inf)
  }

  def listOfGraphesAsync: Future[ListBuffer[String]] = {
    var listOfGraphes = new ListBuffer[String]()
    val futureGrapheAsync = RDFClient.select(graphesSelectQuery).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        listOfGraphes += bs.getValue("g").stringValue()
      }
      listOfGraphes
    }

    futureGrapheAsync onComplete {
      case Success(i) => logger.info(s"The number of graphe : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureGrapheAsync
  }

  def listOfGraphesBlocked: ListBuffer[String] = {
    val futureListOfGraph: Future[ListBuffer[String]] = listOfGraphesAsync
    Await.result(futureListOfGraph, Duration.Inf)
  }

  def vocabulariesAsync: Future[ListBuffer[VocabularyEntity]] = {
    var vocabularies = new ListBuffer[VocabularyEntity]()
    val futureVocabulariesAsync = RDFClient.select(vocabularySparqlQuery).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        val vocEntity = vocabularyEntity(bs)
        vocabularies = vocabularies ++ vocEntity
      }
      vocabularies
    }

    futureVocabulariesAsync onComplete {
      case Success(i) => logger.info(s"The number of vocabulaire : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureVocabulariesAsync
  }

  def vocabulariesBlocked: Seq[VocabularyEntity] = {
    val futureVOC = vocabulariesAsync
    Await.result(futureVOC, Duration.Inf).toSeq
  }

  def vocabularyEntity(bs: BindingSet): Array[VocabularyEntity] = {
    val voc = bs.getValue("voc").stringValue()
    val notations = TextCleaners.removeNonPrintableCharachter(bs.getValue("notation").stringValue())
    val concept = TextCleaners.removeNonPrintableCharachter(bs.getValue("concept").stringValue())
    val conceptLabel = bs.getValue("conceptLabel").stringValue()

    notations.split(";").map { notation =>
      voc match {
        case "http://data.dictionnairedesfrancophones.org/authority/connotation" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasConnotation", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/domain" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasDomain", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/form-type" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasFormType", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/frequency" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasFrequency", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/gender" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGender", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/glossary" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGlossary", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/grammatical-constraint" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGrammaticalConstraint", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/mood" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasMood", conceptLabel)

        case "http://data.dictionnairedesfrancophones.org/authority/multiword-type" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#multiWordType", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/number" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasNumber", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPartOfSpeech", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/person" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPerson", conceptLabel)

        case "http://data.dictionnairedesfrancophones.org/authority/place" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/register" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasRegister", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/sense-relation" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSemanticRelationType", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/sociolect" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSociolect", conceptLabel)

        case "http://data.dictionnairedesfrancophones.org/authority/temporality" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTemporality", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/tense" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTense", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/term-element" => VocabularyEntity(voc, notation, concept, "http://www.lexinfo.net/ontology/2.0/lexinfo#termElement", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/textual-genre" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTextualGenre", conceptLabel)
        case "http://data.dictionnairedesfrancophones.org/authority/verb-frame" => VocabularyEntity(voc, notation, concept, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTransitivity", conceptLabel)
      }
    }
  }

  def deleteBeforeLastGraph: Option[Future[Done]] = {
    val listGraphes = listOfGraphesBlocked
    val sorted = sortByDate(listGraphes.toSeq)
    if (sorted.size > 1) {
      val graphName = sorted.toSeq(sorted.size-1)._1
      Some(RDFClient.update(deleteGraphQuery(graphName)))
    }
    else None
  }

  private def sortByDate(listGraphes: Seq[String]): Map[String, Long] = {
    val dates = listGraphes.filter(grapheName => grapheName.contains("http://data.dictionnairedesfrancophones.org/dict/wikt/graph/")).map { grapheName =>
      (grapheName, getDate(grapheName))
    }.toMap
    ListMap(dates.toSeq.sortBy(_._2):_*)
  }

  private def getDate(grapheName: String): Long = {
    val segments = grapheName.split("/")
    val dateStr = segments(segments.length-1)
    dateStr.toLong
  }

  def close: Unit = {
    conn.close()
    writeConn.close()
  }
}