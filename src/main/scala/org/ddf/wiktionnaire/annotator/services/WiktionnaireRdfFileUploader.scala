package org.ddf.wiktionnaire.annotator.services
import java.io.File
import java.net.URL

import collection.JavaConverters._
import com.mnemotix.synaptix.graphdb.GraphDbRestApiClient
import com.mnemotix.synaptix.graphdb.models.{EnableLiteralIndex, EnablePredicateList, GraphDBDataImportUrl, GraphDBEditRepository, GraphDBRepository, InMemoryLiteralProperties, Params, ParserSettings}
import com.mnemotix.synaptix.rdf.client.models.Done
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFFormats}
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
class WiktionnaireRdfFileUploader(implicit ec: ExecutionContext) extends LazyLogging {

  def init = RDFClient.init()

  def initRepository: Future[Seq[Done]] = {
    val creation = createRepo
    if (creation._1) {
      val models = WiktionnaireAnnotatorConfig.ddfModels.asScala.toList
      val vocabs = WiktionnaireAnnotatorConfig.ddfVocabulary.asScala.toList
      val result = loadModelsVocs(models, "http://data.dictionnairedesfrancophones.org/dict/ontologies/graph") ++ loadModelsVocs(vocabs,"http://data.dictionnairedesfrancophones.org/dict/inv/graph")
      Future.sequence(result)
    }
    else {
      logger.error(s"Problem with init ${creation._2}")
      throw new Exception(s"Problem with init ${creation._2}")
    }
  }

/*
    def loadRdfFiles(listOfFiles: List[File]): Seq[Future[Done]] = {
     val futureDone = listOfFiles.map { file =>
      implicit val writeConn = RDFClient.getWriteConnection(Some(WiktionnaireAnnotatorConfig.ddfRepoName))
       val fut = RDFClient.load(file, None, format(file.getName))(ec, writeConn)
       fut.onComplete {
         case Success(i) => {
           writeConn.close()
           logger.info(s"the upload of ${file.getName} finished with ${i.isSuccess}")
         }
         case Failure(t) => {
           writeConn.close()
           logger.error(s"there is a failure with this rdf file - ${file.getName} - during the upload ", t)
         }
       }
       fut
    }
      futureDone
  }
*/

  def editRepository(boolean: Boolean): (Boolean, String) = {
    val enableLiteralIndex = new EnableLiteralIndex(value = boolean)
    val inMemoryLiteralProperties = new InMemoryLiteralProperties(value = boolean)
    val enablePredicateList = new EnablePredicateList(value = boolean)
    val params = new Params(enablePredicateList,inMemoryLiteralProperties, enableLiteralIndex)
    val editRepository = new GraphDBEditRepository(WiktionnaireAnnotatorConfig.ddfRepoName, "", params, "Dictionnaire des francophones", "GRAPHDB-EE Master")
    GraphDbRestApiClient.editRepositoryConfig(
      WiktionnaireAnnotatorConfig.ddfRepoName,
      editRepository
    )
  }

  def loadRdfFiles(listOfFiles: List[File]): Seq[(Boolean, String)] = {
    listOfFiles.map { file =>
      val parserSettings = new ParserSettings
      val importURL = new GraphDBDataImportUrl(None, None, Some(file.toURI().toURL().toExternalForm),
        None, None, None, Some(file.getName), parserSettings, None, "PENDING", System.currentTimeMillis(), None, None)
      GraphDbRestApiClient.importDataByUrl(WiktionnaireAnnotatorConfig.ddfRepoName, importURL)
    }
  }

  def loadRdfFile(fileURL: String): (Boolean, String) = {
    val parserSettings = new ParserSettings
    val importURL = new GraphDBDataImportUrl(None, None, Some(fileURL),
      None, None, None, Some(fileURL), parserSettings, None, "PENDING", System.currentTimeMillis(), None, None)
    GraphDbRestApiClient.importDataByUrl(WiktionnaireAnnotatorConfig.ddfRepoName, importURL)
  }

  private def loadModelsVocs(listOfFiles: Seq[String], context:String): Seq[Future[Done]] = {
    listOfFiles.map { file =>
      implicit val writeConn = RDFClient.getWriteConnection(WiktionnaireAnnotatorConfig.ddfRepoName)
      RDFClient.load(new URL(s"${WiktionnaireAnnotatorConfig.ddfFilePath}${file}"), None, format(file), context)(ec, writeConn)
    }
  }

  private def createRepo: (Boolean, String) = {
    if (!exist) {
      ???
      //GraphDbRestApiClient.createRepository(WiktionnaireAnnotatorConfig.ddfRepoName, WiktionnaireAnnotatorConfig.ddfLabel, WiktionnaireAnnotatorConfig.ddfRepoBaseUrl)
    }
    else (true, "Done")
  }

  private def exist: Boolean = {
    //val parsing = Json.parse(GraphDbRestApiClient.listRepositories()).as[Seq[GraphDBRepository]]
    //parsing.map(_.id).contains("ddf")
    ???
  }

  private def format(filename: String): RDFFormats.RDFFormat = {
    filename match {
      case filename if filename.contains(".ttl") => RDFFormats.TURTLE
      case filename if filename.contains(".trig") => RDFFormats.TRIG
      case filename if filename.contains(".owl") => RDFFormats.RDFXML
      case filename if filename.contains(".rdf") => RDFFormats.RDFXML
    }
  }

  def close: Unit = {
  }
}

/*
  def loadFiles(listOfGzipFiles: List[File]): Future[List[Done]] = {
    rdf4jRDFClient.init()
    Future.sequence(listOfGzipFiles.map { file =>
      rdf4jRDFClient.load(WiktionnaireAnnotatorConfig.ddfRepoName, None, file.getAbsolutePath)
    })
  }

 */