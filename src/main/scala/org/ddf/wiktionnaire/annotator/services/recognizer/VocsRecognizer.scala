package org.ddf.wiktionnaire.annotator.services.recognizer

import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.RegexExtractors
import org.ddf.wiktionnaire.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.wiktionnaire.annotator.model.VocabularyEntity
import org.eclipse.rdf4j.model.IRI

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class VocsRecognizer(vocabularyEntity: Seq[VocabularyEntity]) {

  lazy val listOfVocab =
    Seq("http://data.dictionnairedesfrancophones.org/authority/place",
    "http://data.dictionnairedesfrancophones.org/authority/grammatical-constraint",
    "http://data.dictionnairedesfrancophones.org/authority/textual-genre",
    "http://data.dictionnairedesfrancophones.org/authority/register",
    "http://data.dictionnairedesfrancophones.org/authority/sociolect",
    "http://data.dictionnairedesfrancophones.org/authority/domain",
    "http://data.dictionnairedesfrancophones.org/authority/glossary",
    "http://data.dictionnairedesfrancophones.org/authority/connotation",
    "http://data.dictionnairedesfrancophones.org/authority/temporality",
    "http://data.dictionnairedesfrancophones.org/authority/frequency"
  )

  def recogFromVocab(content: String, vocabulary: String): Seq[(String, IRI)] = {
    val vocabs = extractFromVocabulary(vocabulary, content)
    vocabs.map { vocab =>
        (vocab.property, createIRI(vocab.concept))
    }.distinct
  }

  def recogFromVocabWithLabel(content: String, vocabulary: String): Seq[(String, IRI)] = {
    val vocabs = extractFromVocabularyLabelBased(vocabulary, content)
    vocabs.map { vocab =>
      (vocab.property, createIRI(vocab.concept))
    }.distinct
  }

  def lexicalQualificationGenerate(sentence: String): Seq[(String, IRI)] = {
      listOfVocab.map { vocabulary =>
        //http://data.dictionnairedesfrancophones.org/authority/domain
        //println(vocabulary)
        if (vocabulary == "http://data.dictionnairedesfrancophones.org/authority/domain") {
          domainFromText(sentence.trim).filter(_ != "fr").map { domain =>
            extractFromVocabulary(vocabulary, s"{{$domain|fr}}" ).map { vocabMatch =>
              //LiftyAnnotation(new URI(uri), vocabMatch.property, createIRI(vocabMatch.concept), 1.0)
              (vocabMatch.property, createIRI(vocabMatch.concept))
            }
          }.flatten.toSeq
        }
        else {
          extractFromVocabulary(vocabulary, sentence).map { vocabMatch =>
            (vocabMatch.property, createIRI(vocabMatch.concept))
          }
        }

      }.flatten.distinct
  }



  def domainFromText(sentence: String): Seq[String] = {
    RegexExtractors.extractLexique(sentence).map { matches =>
      matches.group(2).split("\\|")
    }.flatten
  }

  def extractFromDomainVocabulary(vocabulary: String, domain: String): Seq[VocabularyEntity] = {
    vocabularyEntity.filter(vocab => vocab.voc == vocabulary).filter { vocab =>
      domain == vocab.conceptLabel.toLowerCase
    }
  }

  def extractFromVocabulary(vocabulary: String, content: String): Seq[VocabularyEntity] = {
    vocabularyEntity.filter(vocab => vocab.voc == vocabulary).filter { vocab =>
      content.contains(vocab.notation)
    }
  }

  def extractFromVocabularyLabelBased(vocabulary: String, content: String): Seq[VocabularyEntity] = {
    vocabularyEntity.filter(vocab => vocab.voc == vocabulary).filter(vocab => content.contains(vocab.conceptLabel))
  }

  def filterVocsByType(vocType: String): Seq[VocabularyEntity] = {
    vocabularyEntity.filter(vocab => vocab.voc == vocType)
  }
}

