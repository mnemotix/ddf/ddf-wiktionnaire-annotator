package org.ddf.wiktionnaire.annotator.services.cache

import org.ddf.wiktionnaire.annotator.services.WiktionnaireDBCacheManager

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// TO TEST WITH TEMP DB

object MemoryCacheSystem {
  lazy val lexicogEntryTmpDB = WiktionnaireDBCacheManager.createCache("lexicogEntryTmpDB")
  //lazy val changedLexicogEntry = WiktionnaireDBCacheManager.initDB("changedLexicogEntry")
  //lazy val lexicogEntryWiktionnaire = WiktionnaireDBCacheManager.initDB("lexicogEntryDB")

  //lexicogEntryTmpDB.init
  //changedLexicogEntry.init
  //lexicogEntryWiktionnaire.init
}
