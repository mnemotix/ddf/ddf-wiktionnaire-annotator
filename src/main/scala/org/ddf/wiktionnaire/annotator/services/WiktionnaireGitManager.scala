package org.ddf.wiktionnaire.annotator.services

import java.io.File
import java.lang

import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireGitException
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig
import org.eclipse.jgit.api.{Git, PullResult}
import org.eclipse.jgit.dircache.DirCache
import org.eclipse.jgit.lib.RepositoryCache
import org.eclipse.jgit.transport.{PushResult, UsernamePasswordCredentialsProvider}
import org.eclipse.jgit.util.FS

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
class WiktionnaireGitManager extends LazyLogging {

  lazy val DEVTOOLS_PUSH_REFSPEC = "refs/notes/devtools/*:refs/notes/devtools/*"

  def init: Git = {
    if (new File(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}ddf-wiktionnaire-git/").exists()) {
      Git.open(new File(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}ddf-wiktionnaire-git/"))
    }
    else {
      Git.cloneRepository()
        .setURI("https://gitlab.com/mnemotix/ddf/ddf-wiktionnaire.git")
        .setDirectory(new File(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}ddf-wiktionnaire-git/"))
        .call()
    }
  }

  def gitAdd(git: Git): DirCache = {
    git.add().addFilepattern(".").call()
  }

  def gitCommit(git: Git, message: String) = {
    git.commit().setMessage(message).call()
  }

  def gitPull(git: Git): PullResult = {
    git.pull().call()
  }

  def gitPush(git: Git): lang.Iterable[PushResult] = {
    //val spec = new RefSpec(DEVTOOLS_PUSH_REFSPEC)
    try {
      git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(WiktionnaireAnnotatorConfig.gitUser, WiktionnaireAnnotatorConfig.gitPassword)).call()
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the git push", t)
        throw new WiktionnaireGitException("An error occured during the git push", Some(t))
    }
  }
}
