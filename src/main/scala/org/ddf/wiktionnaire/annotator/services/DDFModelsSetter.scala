package org.ddf.wiktionnaire.annotator.services

import java.net.URI
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Flow
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireModelSetterException
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireUriFactory
import org.ddf.wiktionnaire.annotator.model._
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.{RegexExtractors, TextCleaners}
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.SplitProcessing
import org.ddf.wiktionnaire.annotator.frwiktionnaire.textprocessing.SplitProcessing.{isType, multiWordVoc, posTypeVoc, splitByH3, splitBySection, termElementVoc}
import org.ddf.wiktionnaire.annotator.model.ddf.{CanonicalForm, Entry, LexicalEntry, Sense, SenseRelation, SubSense, UsageExample}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.matching.Regex

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFModelsSetter(implicit system: ActorSystem, ec: ExecutionContextExecutor, lexicalQualificationRecog: VocsRecognizer) extends LazyLogging{

  lazy val lexicalEntriesAllSection: Seq[LexicalEntrySection] => Seq[Section] = (lexicalEntriesSection: Seq[LexicalEntrySection]) => {
    lexicalEntriesSection.map { lexicalEntrieSection =>
      lexicalEntrieSection.sections
    }.flatten
  }

  def filteringNot(liftyResource: Resource): Boolean = {
    liftyResource.id.contains("Wiktionnaire:") || liftyResource.id.contains("Catégorie:")
  }

  def setter: Flow[Resource, Entry, NotUsed] = {
    try {
      // take(20000)
      Flow[Resource].filter(liftyResource => liftyResource.content.contains("== {{langue|fr}} ==")).filterNot(filteringNot(_)).mapAsync(1000) { liftyResource =>
        Future(entry(liftyResource))
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the parsing of the wiktionnaire page", t)
        throw new WiktionnaireModelSetterException("An error occured during the parsing of the wiktionnaire page", Some(t))
    }
  }

  def counter: Flow[Resource, Resource, NotUsed] = {
      try {
        Flow[Resource].filter(liftyResource => liftyResource.content.contains("== {{langue|fr}} =="))
      }
      catch {
        case t: Throwable =>
          logger.error("An error occured during the parsing of the wiktionnaire page", t)
          throw new WiktionnaireModelSetterException("An error occured during the parsing of the wiktionnaire page", Some(t))
      }
  }


  def entry(liftyResource: Resource): Entry = {
    new Entry(URI.create(WiktionnaireUriFactory.lexicogEntry(liftyResource.id)), liftyResource.id, liftyResource.content, lexicalEntry(SplitProcessing.splitByLang(liftyResource.content), liftyResource.id))
  }

  def lexicalEntry(frWikitext: String, title: String): Option[Seq[LexicalEntry]] = {
    val lexicalEntriesSection: Seq[LexicalEntrySection] = SplitProcessing.splitByLexicalEntry(frWikitext, title)
    val sectionsWordType = wordTypeSection(lexicalEntriesAllSection(lexicalEntriesSection))
    if (lexicalEntriesSection.size > 0) {
      Some(lexicalEntriesSection.map { lexicalEntrySection =>
        wordTypeSection(lexicalEntrySection.sections).foreach(s =>s.title)
        val wordSectionContent: Option[String] = if (lexicalEntrySection.sections.isEmpty || lexicalEntrySection.sections.size == 0 ) None else Some(wordTypeSection(lexicalEntrySection.sections)(0).content)
        val uri = lexicalEntrySection.uriLexicalEntry
        val lexicalEntryType = TextCleaners.cleanWordTitle(lexicalEntrySection.lexicalEntryType)
        val senseLexical = sense(wordSectionContent)
        val subSenseLexical = subSense(wordSectionContent)
        LexicalEntry(
          uri,
          title,
          lexicalEntryType,
          senseLexical,
          subSenseLexical,
         cannonicalForm(wordSectionContent, title),
         SplitProcessing.splitEtymologyByPost(if (etymologySection(frWikitext).isDefined) Some(etymologySection(frWikitext).get.content) else None, lexicalEntrySection.lexicalEntryType,sectionsWordType),
         otherForm(lexicalEntrySection),
         senseRelation(lexicalEntrySection),
         wordSectionContent,
         lexicalEntrySection.isInflection
       )
      })
    }
    else None
  }

  def sense(lexicalEntryText: Option[String]): Option[Seq[Sense]] = {
    if (lexicalEntryText.isDefined) {
      val skosDefCitations: Seq[Regex.Match] = RegexExtractors.extractSKOSDefinition(lexicalEntryText.get).toSeq
      if (skosDefCitations.size > 0) {
        Some( skosDefCitations.map { skosDefCitation =>
          val definition = if (skosDefCitation.group(1).trim == null || skosDefCitation.group(1).trim.isEmpty) None else Some(skosDefCitation.group(1))
          val citationsSources = skosDefCitation.group(2)
          Sense(definition, usageExample(citationsSources))
        })
      }
      else None
    }
    else None
  }

  def subSense(lexicalEntryText:  Option[String]): Option[Seq[SubSense]] = {
    if (lexicalEntryText.isDefined) {
      val subSkosDefCitations = RegexExtractors.extractSubSKOSDefinition(lexicalEntryText.get).toSeq
        if(subSkosDefCitations.size > 0) {
          Some(subSkosDefCitations.map { subSkosDefCitation =>
            val definition = if (subSkosDefCitation.group(1).trim == null || subSkosDefCitation.group(1).trim.isEmpty) None else Some(subSkosDefCitation.group(1))
            val citationsSources = subSkosDefCitation.group(2)
            SubSense(definition, usageExample(citationsSources))
          })
      }
      else None
    }
    else None
  }

  def cannonicalForm(lexicalEntryText: Option[String], title: String): Option[CanonicalForm] = {
    if (lexicalEntryText.isDefined) {
      val phonetiqueRegex = RegexExtractors.extractphonetique(lexicalEntryText.get).toSeq
      val phonetique: Option[Seq[String]] = if (phonetiqueRegex.size > 0) Some(phonetiqueRegex.map(_.group(2))) else None
      Some(CanonicalForm(title, phonetique))
    }
    else None
  }

  def otherForm(lexicalEntrySection: LexicalEntrySection): Option[Seq[String]] = {
    val res = lexicalEntrySection.sections.flatMap {
      case section if section.title.contains("==== {{S|variantes orthographiques}} ====") => {
       Some(RegexExtractors.extractRelationWords(section.content).map(regex => regex.group(1)).toSeq)
      }
      case section => None
    }
    if (res.flatten.toSeq.size > 0) Some(res.flatten) else None
  }

  def usageExample(citationsSources: String):  Option[Seq[UsageExample]] = {
      if (citationsSources.toString == null || citationsSources.trim.isEmpty){
        None
      }
      else if (citationsSources.contains("#* {{exemple|")) {
        Some(RegexExtractors.extractCitation(citationsSources).toSeq.map { citationSourceRegex =>
          val citationSource = citationSourceRegex.toString()
          val citation = TextCleaners.cleanCitationExempleModel(citationSource)
          val bibliographicalSourceRgx = RegexExtractors.extractSourceInExample(citationSource).toSeq
          val source = if (bibliographicalSourceRgx.size < 1) None else Some(bibliographicalSourceRgx.last.toString())
          UsageExample(citation, source)
        })
      }
      else {
        Some(RegexExtractors.extractCitation(citationsSources).toSeq.map { citationSourceRegex =>
          val citationSource = citationSourceRegex.toString()
          val citation = TextCleaners.cleanCitation(citationSource)
          val bibliographicalSourceRgx = RegexExtractors.extractSource(citationSource).toSeq
          val source =  if (bibliographicalSourceRgx.size < 1) None else Some(bibliographicalSourceRgx.last.toString())
          UsageExample(citation, source)
        })
      }
  }

  def senseRelation(lexicalEntrySection: LexicalEntrySection): Option[Seq[SenseRelation]] = {
    val vocabs: Seq[VocabularyEntity] = lexicalQualificationRecog.filterVocsByType("http://data.dictionnairedesfrancophones.org/authority/sense-relation")
    val notations = vocabs.map(vocab => vocab.notation)
    val res = lexicalEntrySection.sections.flatMap {
      case section if notations.contains(section.title) => {
        Some(SenseRelation(section.title ,RegexExtractors.extractRelationWords(section.content).map(regex => regex.group(1)).toSeq))
      }
      case section => None
    }
    if (res.toSeq.size > 0) Some(res) else None
  }

  def etymologySection(wikitext: String): Option[Section] = {
    val sections = splitByH3(wikitext) .filter { regex =>
        regex.group(1).contains("=== {{S|étymologie}} ===")
      }.toSeq
    if (sections.size < 1 || sections.isEmpty) {
      None
    }else {
      val res: Seq[Section] = sections.map { regex =>
        splitBySection(regex.toString())
      }.flatten.filter(_.title == "=== {{S|étymologie}} ===")
      if (res.size < 1 || res.isEmpty) None else Some(res.last)
    }
  }

  def wordTypeSection(sections: Seq[Section]): Seq[Section] = {
    sections.filter { section =>
      val vocTypes = lexicalQualificationRecog.filterVocsByType(posTypeVoc) ++ lexicalQualificationRecog.filterVocsByType(multiWordVoc) ++ lexicalQualificationRecog.filterVocsByType(termElementVoc)
      isType(section.title, vocTypes)
    }
  }
}