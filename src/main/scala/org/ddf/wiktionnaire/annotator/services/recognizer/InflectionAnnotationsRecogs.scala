/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.annotator.services.recognizer

@deprecated
object InflectionAnnotationsRecogs {

  /*
  def inflexionGenerator(sense: Sense, lentry: LexicalEntry, wikitext: String)(implicit lexicalQualificationRecog: VocsRecognizer): Option[Seq[LiftyAnnotation]] = {
    if (sense.definition.isDefined) {
      val lexicalSensURI: URI = LexicalSensAnnotationsRecogs.lexicalSensURIGenerate(sense.definition.get, sense.usageExamples, lentry.uri.toString)
      val lexicalSens: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.lexicalSensGenerate(lentry.uri, lexicalSensURI)
      val skosDefinition: Seq[LiftyAnnotation] = LexicalSensAnnotationsRecogs.skosDefinitionGenerate(sense.definition.get, lexicalSensURI)
      val usageExempleCitation: Option[Seq[LiftyAnnotation]] = DDFAnnotationsGenerator.usageExempleGenerator(sense.usageExamples, lexicalSensURI)

    val infection: Seq[LiftyAnnotation] = if (TextCleaners.cleanWordTitle(lentry.lexicalEntryType).toLowerCase == "verbe") {
      verbalInflection(lentry.uri, sense.definition.get, lentry.lexicalEntryType)
    }
    else {
      genderNumberTransitivityAnnotations(lentry.uri, sense.definition.get, wikitext, lentry.lexicalEntryType)
    }
    val res = usageExempleCitation.toSeq.flatten ++ lexicalSens ++ skosDefinition ++ infection
    Some(res)
  }
  else None
 }

  def genderNumberTransitivityAnnotations(lexicalEntryURI: URI, definition: String, wikitext: String, lexicalEntryType: String)(implicit lexicalQualificationRecog: VocsRecognizer): Seq[LiftyAnnotation] = {
    val uriInflectablePos = inflectionOfUri(definition, lexicalEntryType)

    val inflectablePos = if (uriInflectablePos.isDefined) {
      Some(Seq(LiftyAnnotation(uriInflectablePos.get, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasInflection", createIRI(lexicalEntryURI.toString) , 1.0),
        LiftyAnnotation(lexicalEntryURI, "a", createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#Inflection"), 1.0),
        LiftyAnnotation(uriInflectablePos.get, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"), 1.0),
        LiftyAnnotation(lexicalEntryURI, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"), 1.0)
      ))
    }
    else None

    val gender = lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, wikitext, "http://data.dictionnairedesfrancophones.org/authority/gender")
    val number = lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, wikitext, "http://data.dictionnairedesfrancophones.org/authority/number")

    val gender2 = lexicalQualificationRecog.recogFromVocabWithLabel(lexicalEntryURI, definition.toLowerCase(), "http://data.dictionnairedesfrancophones.org/authority/gender")
    val number2 = lexicalQualificationRecog.recogFromVocabWithLabel(lexicalEntryURI, definition.toLowerCase(), "http://data.dictionnairedesfrancophones.org/authority/number")

    gender2.toSeq.flatten.map { annotations =>
      LiftyAnnotation(annotations.resource, "http://data.dictionnairedesfrancophones.org/ontology/ddf#inflectionHasGender", annotations.obj, annotations.weight)
    } ++
      gender.toSeq.flatten.map { annotations =>
        LiftyAnnotation(annotations.resource, "http://data.dictionnairedesfrancophones.org/ontology/ddf#inflectionHasGender", annotations.obj, annotations.weight)
      } ++
      number2.toSeq.flatten.map { annotations =>
        LiftyAnnotation(annotations.resource, "http://data.dictionnairedesfrancophones.org/ontology/ddf#inflectionHasNumber", annotations.obj, annotations.weight)
      } ++
      number.toSeq.flatten.map { annotations =>
        LiftyAnnotation(annotations.resource, "http://data.dictionnairedesfrancophones.org/ontology/ddf#inflectionHasNumber", annotations.obj, annotations.weight)
      } ++ inflectablePos.toSeq.flatten
  }

  def verbalInflection(lexicalEntryURI: URI, definition: String, lexicalEntryType: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {

    val mood = lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, definition, "http://data.dictionnairedesfrancophones.org/authority/mood")
    val tense = lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, definition, "http://data.dictionnairedesfrancophones.org/authority/tense")
    val person = lexicalQualificationRecog.recogFromVocab(lexicalEntryURI, definition, "http://data.dictionnairedesfrancophones.org/authority/person")

    val uriInflectablePos = inflectionOfUri(definition, lexicalEntryType)

    val inflectablePos = if (uriInflectablePos.isDefined) {
      Some(Seq(LiftyAnnotation(uriInflectablePos.get, "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasVerbalInflection",createIRI(lexicalEntryURI.toString) , 1.0),
        LiftyAnnotation(lexicalEntryURI, "a",createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#VerbalInflection"), 1.0),
        LiftyAnnotation(uriInflectablePos.get, "a", createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb"), 1.0)
      ))
    }
    else None

    (mood ++ tense ++ person).flatten.toSeq ++ inflectablePos.toSeq.flatten
  }

  def inflectionOfUri(definition: String, lexicalEntryType: String) = {

    val resourceTitles: Seq[String] = RegexExtractors.extractInflectionOf(definition).map { rgex =>
      rgex.group(2).split("\\|")
    }.flatten

    //    RegexExtractors.extractLexique(sentence).map { matches =>
    //      matches.group(2).split("\\|")
    //    }.flatten
    val resourceTitle = if (resourceTitles.size == 1) {
      Some(resourceTitles(0))
    }
    else if (resourceTitles.size == 2) {
      Some(resourceTitles(1))
    }
    else if (resourceTitles.size > 2) {
      None
    }
    else {
      None
    }

    if (resourceTitle.isDefined) {
      val ofMasculin = definition.toLowerCase.contains("féminin")
      if (ofMasculin) {
        Some(URI.create(WiktionnaireUriFactory.ontolexWord(resourceTitle.toString, TextCleaners.cleanWordSectionTitle(lexicalEntryType), Some("masculin"))))
      }
      else Some(URI.create(WiktionnaireUriFactory.ontolexWord(resourceTitle.toString, TextCleaners.cleanWordSectionTitle(lexicalEntryType), None)))
    }
    else None
  }

   */
}