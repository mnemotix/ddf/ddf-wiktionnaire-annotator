package org.ddf.wiktionnaire.annotator.services


import java.io.File
import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.http.FileDownloader
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireRDFMapperException
import org.ddf.wiktionnaire.annotator.helpers.{RDFSerializationUtil, WiktionnaireAnnotatorConfig}
import org.ddf.wiktionnaire.annotator.model.ddf.Entry
import org.ddf.wiktionnaire.annotator.services.recognizer.VocsRecognizer

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import org.ddf.wiktionnaire.annotator.services.cache.MemoryCacheSystem
import org.eclipse.rdf4j.model.Model

import java.util.concurrent.atomic.AtomicInteger


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireAnnotator(filePath: String) extends LazyLogging {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wiktionnaireAnnotationSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val wiktionnaireRdfReader = new WiktionnaireRdfReader
  wiktionnaireRdfReader.init
  lazy val vocs = wiktionnaireRdfReader.vocabulariesBlocked
  implicit val map = wiktionnaireRdfReader.posTypeBlocked
  implicit val lexicalQualtification = new VocsRecognizer(vocs)
  implicit val htmlCrawler = new WiktionnaireHTMLCrawler()

  lazy val extractor = new WiktionnaireWikidataExtractor(filePath)
  lazy val ddfModelsFactory = new DDFModelsSetter()
  lazy val recognizer = new WiktionnaireRecognizer()
  lazy val cacheManager = new WiktionnaireDBCacheManager()
  lazy val wiktionnaireFileOutput = new WiktionnaireFileOutput()
  lazy val fileDownloader = new FileDownloader


  lazy val topHeadSink = Sink.ignore
  lazy val middle1HeadSink = Sink.ignore

  lazy val lexicogEntryCache = MemoryCacheSystem.lexicogEntryTmpDB



  def init: Boolean = {
    new File(WiktionnaireAnnotatorConfig.wiktionaryDir).mkdir()
    wiktionnaireFileOutput.init
  }

  def badPages(entry: Entry) = {
    entry.text.contains("}}===") || entry.text.contains("==={{") || entry.text.contains("<!--")
  }

  def entry: (Future[Done], Future[Done]) = {
    //lazy val pages = new RocksDBStore(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}pages")
    lazy val i = new AtomicInteger()
    //pages.init()

    lexicogEntryCache.init()

    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink, middle1HeadSink)((_,_)) { implicit builder =>
      (topHS, m1HS) =>
      import GraphDSL.Implicits._
      val bcast2 = builder.add(Broadcast[Entry](2))
      extractor.extract ~> ddfModelsFactory.setter.filter(entry => entry.lentries.isDefined && entry.lentries.get.size > 0 && entry.form != "vaccinosceptique" && entry.form != "latin" && entry.form != "astrométrie"/*&& !badPages(entry) && !pages.get(entry.form).isDefined*/) ~> bcast2 ~>  Flow[Entry].map{ entry =>
        //if (entry.form == "bureau" || entry.form == "tableau")
        var model = RDFSerializationUtil.emptyModel
        try {
          model = entry.toDDFModel()
        }
        catch {
          case e: StackOverflowError => {
            println("outch on" + entry.form)
            logger.error(s"==> ${entry.form}")
          }
        }
        //pages.put(entry.form, Array())
        if (i.addAndGet(1) % 10000 == 0) logger.info(s"==> We convert ${i.get()} words")
        model
      }.grouped(100000) ~> wiktionnaireFileOutput.bulkDump ~> topHS
        bcast2 ~> cacheManager.transform(WiktionnaireDBCacheManager.transformerLexicogEntry) ~> cacheManager.bulkDump(lexicogEntryCache) ~> m1HS.in

        ClosedShape
    }).run()
  }

  def flexion: (Future[Done], Future[Done]) = {
    lexicogEntryCache.init()
    lazy val i = new AtomicInteger()
    //lazy val flexion = new RocksDBStore(s"${WiktionnaireAnnotatorConfig.wiktionaryDir}flexions")
    //flexion.init()
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink, middle1HeadSink)((_,_)) { implicit builder =>
      (topHS, m1HS) =>
        import GraphDSL.Implicits._
        val bcast2 = builder.add(Broadcast[Entry](2))
        extractor.extract ~> ddfModelsFactory.setter.filter(entry => entry.lentries.isDefined && entry.lentries.get.size > 0  && entry.form != "vaccinosceptique" && entry.form != "latin" /*&& badPages(entry) && !flexion.get(entry.form).isDefined*/) ~> bcast2 ~>  Flow[Entry].map{ entry =>
          //if (entry.form == "bureaux") logger.debug(s"==> ${entry.form}")
          //if (i.addAndGet(1) % 100 == 0) logger.info(s"==> We convert ${i.get()} words")
          logger.debug(s"==> ${entry.form}")
          entry.toInflexion()
        }.grouped(100000) ~> wiktionnaireFileOutput.bulkDump ~> topHS
        bcast2 ~> cacheManager.transform(WiktionnaireDBCacheManager.transformerLexicogEntry) ~> cacheManager.bulkDump(lexicogEntryCache) ~> m1HS.in

        ClosedShape
    }).run()
  }

  def shutdown: Unit = {
    //lexicogEntryCache.shutdown()
    system.terminate()
    materializer.shutdown()
  }

  def writeDir = wiktionnaireFileOutput.dir
}