package org.ddf.wiktionnaire.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow

import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging

import org.ddf.wiktionnaire.annotator.services.recognizer.{DDFAnnotationsGenerator, DDFSensRelationsAnnotationsGenerator, VocsRecognizer}

import scala.collection.mutable
import scala.concurrent.{ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
class WiktionnaireRecognizer(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContextExecutor, lexicalQualificationRecog: VocsRecognizer,POSType: mutable.Map[String, String]) extends LazyLogging {
 /*
  def recognize: Flow[Entry, Seq[LiftyAnnotation], NotUsed] = {
    try {
      Flow[Entry].mapAsync(500) { entry =>
        Future(DDFAnnotationsGenerator.generator(entry))
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the recognition process", t)
        throw new LiftyRecognizeException("An error occured during the recognition process", Some(t))
    }
  }

  def recognizeFlexions: Flow[Entry, Seq[LiftyAnnotation], NotUsed] = {
    try {
      Flow[Entry].mapAsync(500) { entry =>
        Future(DDFAnnotationsGenerator.generatorFlexion(entry))
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the recognition process", t)
        throw new LiftyRecognizeException("An error occured during the recognition process", Some(t))
    }
  }

  def recognizeSensRelation(rocksDB: RocksDBStore): Flow[Entry, Seq[LiftyAnnotation], NotUsed] = {
    try {
      Flow[Entry].mapAsync(500) { entry =>
        Future(DDFSensRelationsAnnotationsGenerator.generator(entry, rocksDB).toSeq.flatten.flatten.toSeq)
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the sens relation recognition process", t)
        throw new LiftyRecognizeException("An error occured during the sens relation recognition process", Some(t))
    }
  }

  */
}