package org.ddf.wiktionnaire.annotator.services

import java.io.File

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Framing, Sink, Source}
import akka.util.ByteString
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireHTMLCrawler(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) {

  lazy val downloadURL = WiktionnaireAnnotatorConfig.wiktionnaireUrl
  lazy val serverFileName = WiktionnaireAnnotatorConfig.wiktionnaireFileName

  lazy val uri = Uri(s"$downloadURL$serverFileName")
  lazy val downloadDir = new File(WiktionnaireAnnotatorConfig.wiktionaryDir)

  def latestFileDate: Option[String] = {
    val pageContent = Await.result(wiktionnaryLatestPageContent, Duration.Inf)
    val linkAndDate = "(?m)^(\\<a\\shref\\=.*\\<\\/a\\>)\\s*(\\d\\d\\-\\w*\\-\\d\\d\\d\\d)".r.findAllMatchIn(pageContent).find {
      regexMatch => regexMatch.group(1).contains(serverFileName)
    }
    if (linkAndDate.isDefined) Some(linkAndDate.get.group(2))
    else None
  }

  def fileSize: Option[String] = {
    val pageContent = Await.result(wiktionnaryLatestPageContent, Duration.Inf)
    val linkAndDate = "(?m)^(\\<a\\shref\\=.*\\<\\/a\\>)\\s*(\\d\\d\\-\\w*\\-\\d\\d\\d\\d)\\s*(\\d\\d\\:\\d\\d)\\s*(\\d*)".r.findAllMatchIn(pageContent).find {
      regexMatch => regexMatch.group(1).contains(serverFileName)
    }
    if (linkAndDate.isDefined) Some(linkAndDate.get.group(4))
    else None
  }

  private def wiktionnaryLatestPageContent: Future[String] = {
    val delimiter: Flow[ByteString, ByteString, NotUsed] =
      Framing.delimiter(
        ByteString("\r\n"),
        maximumFrameLength = 100000,
        allowTruncation = true)

    val homeUri = Uri(s"$downloadURL")
    val req = HttpRequest(GET, uri = homeUri)

    val fut: Future[Source[String, _]] = Http().singleRequest(req).map { resp =>
      resp.entity.dataBytes.via(delimiter)
        .map(_.utf8String)
    }
    val resultat: Source[String, _] = Await.result(fut, Duration.Inf)
    val sink = Sink.fold[String, String]("")(_ + "\n" + _)
    resultat.runWith(sink)
  }
}