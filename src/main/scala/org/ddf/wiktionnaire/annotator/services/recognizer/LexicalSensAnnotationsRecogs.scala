package org.ddf.wiktionnaire.annotator.services.recognizer

import java.net.URI



/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
object LexicalSensAnnotationsRecogs {
  /*

  val definitionModelParsing = new DefinitionParser()
  val usageExampleParser = new UsageExampleParser()
  val sourceModelParser = new SourceModelParser

  /*
  def lexicalSensURIGenerate(definition: String, usageExamples: Option[Seq[UsageExample]], lexicalEntryUri: String) = {
    val usageExampleString = if (usageExamples.isDefined) usageExamples.get.mkString else ""
    URI.create(WiktionnaireUriFactory.ontolexLexicalSense(s"$definition ${usageExampleString}", lexicalEntryUri.toString))
  }

   */

  def exempleURIGenerate(example: String, bibliographicalCitation: Option[String]) = {
    val citationString = if (bibliographicalCitation.isDefined) bibliographicalCitation.get.mkString else ""
    URI.create(WiktionnaireUriFactory.lexicogUsageExample(s"$example ${citationString}"))
  }

  //createIRI(lexicalEntryURI.toString)
  // createLiteral(s"${StringCleaner.replaceApostrophe(pageTitle)}", PrimitiveDatatypesIRI.STRINGIRIDATATYPE)

  def lexicalSensGenerate(lexicalEntryUri: URI, lexicalSensUri: URI) = {
    Seq(
      LiftyAnnotation(lexicalEntryUri,
        "http://www.w3.org/ns/lemon/ontolex#sense",
        createIRI(lexicalSensUri.toString), 1.0),

      LiftyAnnotation(lexicalSensUri,
        "a",
        createIRI("http://www.w3.org/ns/lemon/ontolex#LexicalSense"), 1.0)
    )
  }

  def skosDefinitionGenerate(definition: String, lexicalSensUri: URI ) = {
    Seq(LiftyAnnotation(lexicalSensUri,
      "http://www.w3.org/2004/02/skos/core#definition",
      createLiteral(s"${StringCleaner.replaceApostrophe(definitionModelParsing.parser(definition))}", PrimitiveDatatypesIRI.STRINGIRIDATATYPE), 1.0))
  }

  def usageExampleGenerate(example: String, lexicalSensUri: URI, exempleURI: URI) = {
    Seq(LiftyAnnotation(lexicalSensUri, "http://www.w3.org/ns/lemon/lexicog#usageExample", createIRI(exempleURI.toString), 1.0),
      LiftyAnnotation(exempleURI,"a", createIRI("http://www.w3.org/ns/lemon/lexicog#UsageExample"), 1.0),
      LiftyAnnotation(exempleURI, "http://www.w3.org/1999/02/22-rdf-syntax-ns#value", createLiteral(StringCleaner.replaceApostrophe(usageExampleParser.parser(example)), PrimitiveDatatypesIRI.STRINGIRIDATATYPE), 1.0)
    )
  }

  def bibliographicalCitationGenerate(exempleURI: URI, source: String) = {
    LiftyAnnotation(exempleURI, "http://purl.org/dc/terms/bibliographicalCitation", createLiteral(StringCleaner.replaceApostrophe(sourceModelParser.parser(source)), PrimitiveDatatypesIRI.STRINGIRIDATATYPE) , 1.0)
  }

  def vocsLexicalGenerate(lexicalSensUri: URI, definition: String)(implicit vocsRecognizer: VocsRecognizer) = {
    vocsRecognizer.lexicalQualificationGenerate(lexicalSensUri.toString, definition)
  }

  /*
  def sensRelationGenerate(senses: Seq[Sense], resourceTitle: String, lexicogEntryUri: URI, lexicalURI: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {

    val rs = senses.zipWithIndex.map { case (sense, i) =>
      val definition = sense.definition
        if (definition.isDefined) {
          val vocabs = lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", definition.get)
          val uriLexicalSens = lexicalSensURIGenerate(definition.get, sense.usageExamples, lexicalURI)
          if (vocabs.size > 0) {
            println(
             s"""
                |BEFORE = ${senses(i-1).definition.get}
                |CURRENT = ${senses(i).definition.get}
                |""".stripMargin)
            val uriLexicalSensBefore = if (i > 1) {
              lexicalSensURIGenerate(senses(i-1).definition.get, senses(i-1).usageExamples,lexicalURI)
            }
            else lexicalSensURIGenerate(senses(i).definition.get, senses(i).usageExamples,lexicalURI)
            Some(vocabs.flatMap{ voc =>
              val uriRelation = WiktionnaireUriFactory.ddfSemanticRelation(lexicogEntryUri.toString, uriLexicalSens.toString, voc.concept)
              sensRelationHelper(uriLexicalSens, new URI(uriRelation), voc.property, voc.concept, uriLexicalSensBefore)
            })
          }
          else None
        }
        else None
    }
    rs.flatten.flatten.toSeq
  }
   */

  def sensRelationGenerate(senses: Seq[Sense], resourceTitle: String, lexicogEntryUri: URI, lexicalURI: String)(implicit lexicalQualificationRecog: VocsRecognizer) = {
    val rs: Seq[Option[Seq[LiftyAnnotation]]] = senses.zipWithIndex.map { case (sense, i) =>
      val definition = sense.definition
      if (definition.isDefined) {
        val vocabs = lexicalQualificationRecog.extractFromVocabulary("http://data.dictionnairedesfrancophones.org/authority/sense-relation", definition.get)
        val uriLexicalSens = lexicalSensURIGenerate(definition.get, sense.usageExamples, lexicalURI)
        if (vocabs.size > 0) {
          Some(vocabs.flatMap{ voc =>
            val uriRelation = WiktionnaireUriFactory.ddfSemanticRelation(lexicogEntryUri.toString, uriLexicalSens.toString, voc.concept)
            sensRelationHelper(uriLexicalSens, new URI(uriRelation), voc.property, voc.concept)
          })
        }
        else None
      }
      else None
    }
    rs.flatten.flatten.toSeq
  }

  def sensRelationHelper(uriLexicalSens: URI, uriRelation: URI, vocProperty: String, vocConcept: String): Seq[LiftyAnnotation] = {
    Seq(LiftyAnnotation(uriLexicalSens,
      "http://data.dictionnairedesfrancophones.org/ontology/ddf#lexicalSenseHasSemanticRelationWith",
      createIRI(s"$uriRelation"), 1.0),
      LiftyAnnotation(uriRelation,
        "a",
        createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"), 1.0
      ),
      LiftyAnnotation(uriRelation,
        vocProperty,
        createIRI(vocConcept), 1.0
      )
      /*,
      LiftyAnnotation(uriRelation,
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfLexicalSense",
        createIRI(uriRelationWith.toString), 1.0
      )*/
    )
  }


  def sensRelationHelper(uriLexicalSens: URI, uriRelation: URI, vocProperty: String, vocConcept: String, uriRelationWith: URI): Seq[LiftyAnnotation] = {
    Seq(LiftyAnnotation(uriLexicalSens,
      "http://data.dictionnairedesfrancophones.org/ontology/ddf#lexicalSenseHasSemanticRelationWith",
      createIRI(s"$uriRelation"), 1.0),
      LiftyAnnotation(uriRelation,
        "a",
        createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"), 1.0
      ),
      LiftyAnnotation(uriRelation,
        vocProperty,
        createIRI(vocConcept), 1.0
      ),
      LiftyAnnotation(uriRelation,
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfLexicalSense",
        createIRI(uriRelationWith.toString), 1.0
      )
    )
  }*/
}