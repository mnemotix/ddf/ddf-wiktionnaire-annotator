package org.ddf.wiktionnaire.annotator.services

import java.io.File

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.stream.{ActorMaterializer}
import com.mnemotix.synaptix.core.utils._
import com.mnemotix.synaptix.http.FileDownloader
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.helpers.WiktionnaireAnnotatorConfig

import scala.concurrent.{ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireDownloader(implicit val system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer ) extends LazyLogging {

  // download.url = "https://dumps.wikimedia.org/frwiktionary/latest/"
  // file.name = "frwiktionary-latest-pages-articles.xml.bz2"

  lazy val fileDownloader = new FileDownloader
  lazy val latest = s"${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionaryUnzipFile}"
  lazy val ancient = s"${WiktionnaireAnnotatorConfig.previousFileDir}${WiktionnaireAnnotatorConfig.previousFileName}"

  def init: Option[Boolean] = {
    logger.info(s"Moving ${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionaryUnzipFile} to ${WiktionnaireAnnotatorConfig.previousFileDir}${WiktionnaireAnnotatorConfig.previousFileName}")
    val latestDir = createDir(s"${WiktionnaireAnnotatorConfig.latestFileDir}").isDefined
    val ancientDir = createDir(s"${WiktionnaireAnnotatorConfig.previousFileDir}").isDefined
    if (!latestDir & !ancientDir) Some(FileUtils.copy(latest, ancient)) else None

    createDir(s"${WiktionnaireAnnotatorConfig.latestFileDir}")
    createDir(s"${WiktionnaireAnnotatorConfig.previousFileDir}")
  }

  def createDir(dirName: String): Option[Boolean] = {
    val directory =  new File(dirName)
    if (!directory.exists()) Some(directory.mkdir()) else None
  }

  def download(uri: Uri): Future[Done] = fileDownloader.downloadFileToDisk(uri.toString(), s"${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionnaireFileName}")

  def unzip: (Future[Int], String) = {
    logger.info(s"Unzipping ${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionnaireFileName}")
    val future = ZipUtils.unzipFileCMD(s"${WiktionnaireAnnotatorConfig.latestFileDir}${WiktionnaireAnnotatorConfig.wiktionnaireFileName}")
    val dir = latest
    (future, dir)
  }
}