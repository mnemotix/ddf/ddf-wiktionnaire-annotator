package org.ddf.wiktionnaire.annotator.services

import java.nio.file.Paths
import akka.NotUsed
import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.alpakka.xml.scaladsl.XmlParsing
import akka.stream.scaladsl.{FileIO, Flow, Source}
import com.sun.org.apache.xalan.internal.xsltc.trax.DOM2SAX
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireWikidateExtractor
import org.ddf.wiktionnaire.annotator.model.Resource

import scala.concurrent.Future
import scala.xml.parsing.NoBindingFactoryAdapter

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class WiktionnaireWikidataExtractor(filePath: String)(implicit mat: ActorMaterializer) extends LazyLogging {

  def extract: Source[Resource, Future[IOResult]] = {
    try {
      FileIO.fromPath(Paths.get(filePath))
        .via(XmlParsing.parser)
        .via(XmlParsing.subtree("mediawiki" :: "page" :: Nil)).map(asXml(_)).via(convert)
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the resource extraction process", t)
        throw new WiktionnaireWikidateExtractor("An error occured during the resource extraction process", Some(t))
    }
  }

  private def convert: Flow[scala.xml.Node, Resource, NotUsed] = Flow[scala.xml.Node].map[Resource](parse)

  private def parse(elem: scala.xml.Node): Resource = {
    val title = (elem \ "title").text
    val text = (elem \ "revision" \ "text").text
    Resource(title, "https://fr.wiktionary.org/wiki/", "entry", text)
  }

  private def asXml(dom: org.w3c.dom.Node): scala.xml.Node = {
    val dom2sax = new DOM2SAX(dom)
    val adapter = new NoBindingFactoryAdapter
    dom2sax.setContentHandler(adapter)
    dom2sax.parse()
    adapter.rootElem
  }
}