package org.ddf.wiktionnaire.annotator.services

import java.io.{File, FileOutputStream}
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, IOResult, RestartSettings}
import akka.stream.alpakka.file.ArchiveMetadata
import akka.stream.alpakka.file.scaladsl.Archive
import akka.stream.scaladsl.{FileIO, Flow, Source}
import com.mnemotix.analytix.commons.utils.FileUtils
import com.mnemotix.synaptix.core.utils.{RandomNameGenerator, StringUtils}
import com.mnemotix.synaptix.graphdb.GraphDbRestApiClient
import com.mnemotix.synaptix.graphdb.models.{GraphDBDataImportUrl, ParserSettings}
import com.mnemotix.synaptix.rdf.client.RDFFormats
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.annotator.WiktionnaireFileOutputException
import org.ddf.wiktionnaire.annotator.helpers.{RDFSerializationUtil, WiktionnaireAnnotatorConfig, WiktionnaireUriFactory}
import org.eclipse.rdf4j.model.{IRI, Model}
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.nio.file.Paths
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireFileOutput(implicit wiktionnaireHTMLCrawler: WiktionnaireHTMLCrawler, system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) extends LazyLogging {

  lazy val dateNow: String = LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMMyyyy"))
  lazy val contextIRI: IRI = WiktionnaireUriFactory.createContextIRI(wiktionnaireHTMLCrawler)
 //lazy val contextIRI: IRI =  SimpleValueFactory.getInstance().createIRI(s"${WiktionnaireAnnotatorConfig.wiktionnaireDataGraphUri}/sprint29")
  lazy val dateOfWiktionnaire: String = WiktionnaireUriFactory.wiktionnaireOnlineDate.getOrElse(dateNow)
  //lazy val dateOfWiktionnaire = "20Jul2021"
  val fileOutGenerator: String => FileOutputStream = (name:String) => new FileOutputStream(s"${WiktionnaireAnnotatorConfig.rdfDumpDir}${dateOfWiktionnaire}/$name.trig")
  var part = 0

  lazy val dir = s"${WiktionnaireAnnotatorConfig.rdfDumpDir}${dateOfWiktionnaire}"

  def init: Boolean = {
    try {
      logger.debug(s"Service ${this.getClass.getSimpleName} is initializing with ${dir}")
      new File(s"${dir}").mkdirs()
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the creation of the directory for file output", t)
        throw new WiktionnaireFileOutputException("An error occured during the creation of the directory for file output", Some(t))
    }
  }

  def dump: Flow[Model, Unit, NotUsed] = {
    Flow[Model].grouped(WiktionnaireAnnotatorConfig.triplesPerDumpFile).filter(p => p.size > 0).map { models =>
      logger.info(s"Dumping models file with: ${models.size}")
      try {
        val name = s"$part-${RandomNameGenerator.haiku}"
        logger.info(s"File name: ${name}")
        part = part + 1
        val file = fileOutGenerator(name)
        val model = RDFSerializationUtil.merge(models)

        Rio.write(model, file, RDFFormat.TRIG)
      }
      catch {
        case t: Throwable =>
          logger.error("An error occured during the output process", t)
          throw new WiktionnaireFileOutputException("An error occured during the output process", Some(t))
      }
    }
  }

  def bulkDump: Flow[Seq[Model], Unit, NotUsed] = {
    Flow[Seq[Model]].map { models =>
      logger.info(s"Dumping models file with: ${models.size}")
      try {
        val name = s"$part-${RandomNameGenerator.haiku}"
        logger.info(s"File name: ${name}")
        part = part + 1
        val file = fileOutGenerator(name)
        val model = RDFSerializationUtil.merge(models)
        Rio.write(model, file, RDFFormat.TRIG)
      }
      catch {
        case t: Throwable =>
          logger.error("An error occured during the output process", t)
          throw new WiktionnaireFileOutputException("An error occured during the output process", Some(t))
      }
    }
  }

  def loadRdfFile(fileURL: String): (Boolean, String) = {
    val parserSettings = new ParserSettings
    val importURL = new GraphDBDataImportUrl(None, None, Some(fileURL),
      None, None, None, Some(fileURL), parserSettings, None, "PENDING", System.currentTimeMillis(), None, None)
    GraphDbRestApiClient.importDataByUrl(WiktionnaireAnnotatorConfig.ddfRepoName, importURL)
  }

  private def format(filename: String): RDFFormats.RDFFormat = {
    filename match {
      case filename if filename.contains(".ttl") => RDFFormats.TURTLE
      case filename if filename.contains(".trig") => RDFFormats.TRIG
      case filename if filename.contains(".owl") => RDFFormats.RDFXML
      case filename if filename.contains(".rdf") => RDFFormats.RDFXML
    }
  }
}