package org.ddf.wiktionnaire.annotator.helpers

import com.typesafe.config.{Config, ConfigFactory}
import scala.jdk.CollectionConverters.CollectionHasAsScala

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WiktionnaireAnnotatorConfig {

  lazy val conf: Config = Option(ConfigFactory.load().getConfig("wiktionnaire")).getOrElse(ConfigFactory.empty())
  lazy val wiktionaryDir: String = conf.getString("dir")
  lazy val rdfDumpDir: String = conf.getString("rdf.dump.dir")
  lazy val latestFileDir: String = conf.getString("wiktionnaire.file.latest.dir")
  lazy val previousFileDir: String = conf.getString("wiktionnaire.file.previous.dir")
  lazy val previousFileName: String = conf.getString("wiktionnaire.file.previous.name")
  lazy val triplesPerDumpFile = conf.getInt("triples.per.file")
  lazy val wiktionnaireFileLocation = conf.getString("wiktionnaire.file.location")

  lazy val wiktionnaireNumberOfTask: String = conf.getString("task.number")

  lazy val ddfFilePath = conf.getString("data.path")
  lazy val ddfModels = conf.getStringList("data.model")
  lazy val ddfVocabulary = conf.getStringList("data.vocabulary")
  lazy val wiktionnaireCodeLang = conf.getString("data.codeLangues")

  lazy val wiktionnairePrefixUri: String = conf.getString("ontology.prefix")
  lazy val wiktionnaireGrapUri: String = conf.getString("ontology.graph.uri")
  lazy val wiktionnaireDataGraphUri = conf.getString("ontology.data.graph.uri")

  lazy val gitUser = conf.getString("git.user")
  lazy val gitPassword = conf.getString("git.password")

  //rdf
  lazy val rdfConf: Config = Option(ConfigFactory.load().getConfig("rdf")).getOrElse(ConfigFactory.empty())
  lazy val ontotextGraphDBhost = rdfConf.getString("root.uri")
  lazy val ontotextGraphDBUser = rdfConf.getString("user")
  lazy val ontotextGraphDBPassword = rdfConf.getString("password")
  lazy val ddfRepoName = rdfConf.getString("repository.name")
  lazy val ddfLabel = "Dictionnaire des francophones"
  lazy val ddfRepoBaseUrl = "http://data.dictionnairedesfrancophones.org/ontology/ddf#"
  lazy val ontologyNamespace = conf.getConfig("rdf.namespaces").entrySet().asScala.map(keys => keys.getKey -> keys.getValue.unwrapped().toString).toMap


  lazy val localDBlexicogEntry = conf.getString("localDB.lexicog.entry.db.name")

  // AMQP
  lazy val amqpConf: Config = Option(ConfigFactory.load().getConfig("amqp")).getOrElse(ConfigFactory.empty())
  lazy val amqpExchangeName = amqpConf.getString("exchange.name")

  // HTTP
  lazy val httpConf: Config = Option(ConfigFactory.load().getConfig("http")).getOrElse(ConfigFactory.empty())
  lazy val wiktionaryUnzipFile = httpConf.getString("unzipped.name")
  lazy val wiktionnaireUrl: String = httpConf.getString("download.url")
  lazy val wiktionnaireBaseUrl: String = httpConf.getString("base.url")
  lazy val wiktionnaireFileName: String = httpConf.getString("file.name")
}
