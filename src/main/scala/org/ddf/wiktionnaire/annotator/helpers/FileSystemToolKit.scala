package org.ddf.wiktionnaire.annotator.helpers

import java.io.File

import akka.stream.{ActorMaterializer, IOResult}
import com.mnemotix.synaptix.core.utils.ZipUtils
import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable.ListMap
import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process
import scala.sys.process._
import scala.sys.process.Process
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object FileSystemToolKit extends LazyLogging {

  lazy val fileCompressor = ZipUtils

  def parsePath(path: String)= {
    if (path.startsWith("~" + File.separator)) {
      System.getProperty("user.home") + path.substring(1)
    }
    else
      path
  }

  def getListOfFiles(dir: File, extensions: List[String]):List[File] ={
    dir.listFiles.filter(_.isFile).toList.filter { file =>
      extensions.exists(file.getName.endsWith(_))
    }
  }

  def listOfDir(dir: File): List[File] = {
    dir.listFiles.filter(_.isDirectory).toList
  }

  def lastAnnotationsDir(dir: List[File]) = {
    val dirMap = dir.map(file => (file.getName, WiktionnaireUriFactory.convertToTimeStamp(file.getName))).toMap
    val sorted: Map[String, String] = ListMap(dirMap.toSeq.sortBy(_._2):_*)
    sorted.last._1
  }

  def unbzipFile(filePath: String)(implicit ec: ExecutionContext) = {
    val result = Process(s"bzip2 -dkf $filePath")
    val future: Future[Int] = Future(result.run().exitValue())

    future onComplete {
      case Success(i) => {
        i match {
          case 0 => logger.info("The file was unzipped successfully.")
          case _ => logger.error("Some problem with the unzip")
        }
      }
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    future
  }

def zipFile(file: String, newfileName: String)(implicit ec: ExecutionContext): Future[Int] = {
  import scala.sys.process._

  val result: process.ProcessBuilder = Process(s"zip -rmj -T ${WiktionnaireAnnotatorConfig.wiktionaryDir}${newfileName} $file")
  val future: Future[Int] = Future(result.run().exitValue())

  future onComplete {
    case Success(i) => {
      i match {
        case 0 => println("The file was unzipped successfully.")
        case _ => println("Some problem with the unzip")
      }
    }
    case Failure(t) => println("An error has occurred: " + t.getMessage)
  }
  future
}

  def movefile(pathSource: String, pathTarget: String)(implicit ec: ExecutionContext): Future[Int] = {
    import scala.sys.process._

    val result: process.ProcessBuilder = Process(s"mv -f $pathSource $pathTarget")
    val future: Future[Int] = Future(result.run().exitValue())

    future onComplete {
      case Success(i) => {
        i match {
          case 0 => println("The file was moved successfully.")
          case _ => println("Some problem with the moving")
        }
      }
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    future
  }

  def gzipFiles(filesLocation: String)(implicit mat: ActorMaterializer, ex: ExecutionContext): Future[List[IOResult]] = {
    val listOfTrigFiles = getListOfFiles(new File(s"${filesLocation}"), List("trig"))
    val futureZip = Future.sequence(listOfTrigFiles.map { file =>
      fileCompressor.gzipFile(file.getAbsolutePath, s"${file.getAbsolutePath}.gzip")
    })

    futureZip onComplete {
      case Success(i) => logger.info(s"The files was zipped ${i.mkString(" ")}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureZip
  }
}