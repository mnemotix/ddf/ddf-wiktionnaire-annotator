package org.ddf.wiktionnaire.annotator.helpers

import java.math.BigInteger
import java.security.MessageDigest
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date

import org.ddf.wiktionnaire.annotator.services.WiktionnaireHTMLCrawler
import org.eclipse.rdf4j.model.impl.SimpleValueFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object WiktionnaireUriFactory {

  def md5sum(components:String*): String = {
    val sb: StringBuilder = new StringBuilder
    for (c <- components) {
      if (c != null && c.length > 0) sb.append(c.trim)
    }
    val number: BigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(sb.toString.getBytes("UTF-8")))
    number.toString(16)
  }

  def lexicogEntry(entry: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}entry/${md5sum(createWiktionnaireURL(entry))}"
  }

  def ontolexWord(formLabel: String, POSType: String, gender: Option[String]): String = {
    if (gender.isDefined) s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}word/${md5sum(formLabel, POSType, gender.get)}"
    else s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}word/${md5sum(formLabel, POSType)}"
  }

  def ddfInflectablePOS(formLabel: String, POSType: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}inflectable/${md5sum(formLabel, POSType)}"
  }

  def ddfInflectableVerb(lexicalUri: String, moodtenseperson: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}inflectable/${md5sum(lexicalUri, moodtenseperson)}"
  }

  def ddfVerb(formLabel: String, POSType: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}verb/${md5sum(formLabel, POSType)}"
  }

  def ddfInvariablePOS(formLabel: String, POSType: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}invariable/${md5sum(formLabel, POSType)}"
  }

  def ontolexAffix(formLabel: String, POSType: String)= {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}affix/${md5sum(formLabel, POSType)}"
  }

  def ontolexMultiWordExpression(formLabel: String, POSType: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}multi-word/${md5sum(formLabel, POSType)}"
  }

  def ontolexLexicalSense(skosDefinitionContent: String, uriEntry: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}sense/${md5sum(skosDefinitionContent, uriEntry)}"
  }

  def ontolexForm(ontolexFormLabel: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}form/${md5sum(ontolexFormLabel)}"
  }

  def siocPost(siocPostContent: String): String = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}sioc-post/${md5sum(siocPostContent)}"
  }

  // LexicogEntry URI + LexicalSense URI + Relation type URI
  def ddfSemanticRelation(lexicogEntryURI: String, lexicalSenseURI: String, relationTypeURI: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}relation/${md5sum(lexicogEntryURI,lexicalSenseURI, relationTypeURI)}"
  }

  def lexicogUsageExample(lexicogUsageExampleValue: String)= {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}example/${md5sum(lexicogUsageExampleValue)}"
  }

  def ddfInflection(ddfInflectionURI: String, genderTypenumberType: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}inflection/${md5sum(ddfInflectionURI, genderTypenumberType)}"
  }

  def ddfVerbalInflection(verbURI: String, moodType: String, tenseType: String, personType: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnairePrefixUri}verbal-inflection/${md5sum(verbURI, moodType, tenseType, personType)}"
  }

  def createWiktionnaireURL(entry: String) = {
    s"${WiktionnaireAnnotatorConfig.wiktionnaireBaseUrl}$entry"
  }

  def createContextIRI(implicit wiktionnaireHTMLCrawler: WiktionnaireHTMLCrawler) = {
    val dateNow = LocalDate.now().format(DateTimeFormatter.ofPattern("ddMMMyyyy"))
    val latestDate = wiktionnaireOnlineDate
    val date = if (latestDate.isDefined) latestDate.get else dateNow
    SimpleValueFactory.getInstance().createIRI(s"${WiktionnaireAnnotatorConfig.wiktionnaireDataGraphUri}/${convertToTimeStamp(date)}")
  }

  def convertToTimeStamp(inDdate: String) = {
    val df = new SimpleDateFormat("ddMMMyyyy")
    val date = df.parse(inDdate)
    val timestamp = new Timestamp(date.getTime())
    timestamp.toInstant.toEpochMilli.toString
  }

  def wiktionnaireOnlineDate(implicit wiktionnaireHTMLCrawler: WiktionnaireHTMLCrawler) = {
    val formatter = new SimpleDateFormat("dd-MMM-yyyy")
    val latestDate =  wiktionnaireHTMLCrawler.latestFileDate
    if (latestDate.isDefined) {
      val date2: Date = formatter.parse(latestDate.get)
      val dateFormat = new SimpleDateFormat("ddMMMyyyy")
      Some(dateFormat.format(date2))
    }
    else None
  }
}