  import sbt._

  object Version {
    lazy val scalaLogging = "3.9.4"
    lazy val scalaTest = "3.2.9"
    lazy val synaptix = "3.1.1-SNAPSHOT"
    lazy val scalaVersion = "2.13.8"

    lazy val akkaVersion = "2.6.15"
    lazy val alpakka = "3.0.4"
    lazy val akkaHTTP = "10.1.9"

    lazy val config = "1.4.1"
    lazy val scalaXml = "2.0.0-M2"

    lazy val commons = "2.6"
    lazy val commonsCodec = "1.10"
    lazy val commonsHTTPClient = "3.1"
    lazy val htmlCleaner = "2.22"
    lazy val jgit = "5.6.1.202002131546-r"
    lazy val logback = "1.2.5"
    lazy val playJson = "2.9.2"
    lazy val rdf4j = "3.7.2"
    lazy val rocksdb = "6.2.2"
    lazy val scalaParserCombinators = "2.1.1"
    lazy val analytix = "3.0-SNAPSHOT"

  }

  //"com.softwaremill.sttp" %% "akka-http-backend" %
  // lazy val amqpToolkit = "com.mnemotix" %% "synaptix-amqp-toolkit" % "0.1.6-SNAPSHOT"
  // "org.eclipse.jgit" % "org.eclipse.jgit" % "5.6.1.202002131546-r"

  object Library {
    lazy val alpakkaXml = "com.lightbend.akka" %% "akka-stream-alpakka-xml" % Version.alpakka
    lazy val config: ModuleID = "com.typesafe" % "config" % Version.config
    lazy val commons = "commons-io" % "commons-io" % Version.commons
    lazy val commonsCodec = "commons-codec" % "commons-codec" % Version.commonsCodec
    lazy val commonsHTTPClient = "commons-httpclient" % "commons-httpclient" % Version.commonsHTTPClient
    lazy val htmlCleaner = "net.sourceforge.htmlcleaner" % "htmlcleaner" % Version.htmlCleaner
    lazy val jgit = "org.eclipse.jgit" % "org.eclipse.jgit" % Version.jgit
    lazy val logbackClassic: ModuleID = "ch.qos.logback" % "logback-classic" % Version.logback
    lazy val playJson ="com.typesafe.play" %% "play-json" % Version.playJson

    lazy val rdf4j = "org.eclipse.rdf4j" % "rdf4j-runtime" % Version.rdf4j
    lazy val scalaLangMod = "org.scala-lang.modules" %% "scala-xml" % Version.scalaXml
    lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
    lazy val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % Version.scalaTest

    lazy val synaptixCore = "com.mnemotix" % "synaptix-core_2.13" % Version.synaptix
    lazy val synaptixHtmlToolkit = "com.mnemotix" % "synaptix-http-toolkit_2.13" % Version.synaptix
    lazy val synaptixRdfToolKit = "com.mnemotix" % "synaptix-rdf-toolkit_2.13" % Version.synaptix
    lazy val synaptixCacheToolkit = "com.mnemotix" % "synaptix-cache-toolkit_2.13" % Version.synaptix
    lazy val synaptixJobToolkit = "com.mnemotix" % "synaptix-jobs-toolkit_2.13" % Version.synaptix

    lazy val wiktionnaireTemplateReplacer = "com.mnemotix" % "ddf-wiktionnaire-template-replacer_2.13" % "0.2.4-SNAPSHOT"
    lazy val scalaParserCombinators = "org.scala-lang.modules" %% "scala-parser-combinators" % Version.scalaParserCombinators
    lazy val analytixCore = "com.mnemotix" % "analytix-commons_2.13" % Version.analytix
    lazy val alpakkaFile = "com.lightbend.akka" %% "akka-stream-alpakka-file" % Version.alpakka


  }

  object Dependencies {

    import Library._

    val alpakka = List(alpakkaXml, alpakkaFile)
    val conf = List(config)
    val common = List(commons, commonsCodec, commonsHTTPClient)
    val log = List(logbackClassic)
    val git = List(jgit)
    val scala = List(scalaLangMod)
    val synaptix = List(synaptixCore, synaptixRdfToolKit, synaptixHtmlToolkit, synaptixCacheToolkit, synaptixJobToolkit, wiktionnaireTemplateReplacer, scalaParserCombinators, analytixCore)
    val test = List(scalaTest)

    val compileScope = conf  ++ scala ++ alpakka ++ common ++ git

    val testScope = (test ++ log).map(_ % "test")

    val providedScope = (
        synaptix
      ).map(_ % "provided")

    // Projects
    val core = compileScope ++ testScope ++ providedScope
  }